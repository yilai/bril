#ifndef _dtdemosource_Application_h_
#define _dtdemosource_Application_h_

#include "bril/histogramsource/generic/Application.h"
#include "interface/bril/HistogramDtDemoTopics.hh"

namespace bril{
    namespace histogramsourcedtdemo {

        class Application: public bril::histogramsourcegeneric::Application {

            public:
                // basic
                XDAQ_INSTANTIATOR();
                Application(xdaq::ApplicationStub * s);

            protected:
                // these methods are detector specific
                std::string getApplicationSummary() { return std::string("Histogram Source DT Demonstrator"); }
                uint32_t getHistogramMaxsize() { return interface::bril::dtdemo_rawhistT::maxsize(); }
                std::string getHistogramTopicname() { return interface::bril::dtdemo_rawhistT::topicname(); }
                std::string getHistogramPayloadDict() { return interface::bril::dtdemo_rawhistT::payloaddict(); }
                void ConfigureHardware();

        };

    }
}

#endif