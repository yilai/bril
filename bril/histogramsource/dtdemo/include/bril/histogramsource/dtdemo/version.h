#ifndef _bril_histogramsourcedtdemo_version_h_
#define _bril_histogramsourcedtdemo_version_h_

#include "config/PackageInfo.h"

#define BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MAJOR 1
#define BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MINOR 0
#define BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_PATCH 3
#undef BRIL_BRILHISTOGRAMSOURCEDTDEMO_PREVIOUS_VERSIONS
#define BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MAJOR, BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MINOR, BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_PATCH)
#ifndef BRIL_BRILHISTOGRAMSOURCEDTDEMO_PREVIOUS_VERSIONS
#define BRIL_BRILHISTOGRAMSOURCEDTDEMO_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MAJOR, BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MINOR, BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_PATCH)
#else
#define BRIL_BRILHISTOGRAMSOURCEDTDEMO_FULL_VERSION_LIST BRIL_BRILHISTOGRAMSOURCEDTDEMO_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MAJOR, BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_MINOR, BRIL_BRILHISTOGRAMSOURCEDTDEMO_VERSION_PATCH)
#endif

namespace histogramsourcedtdemo
{
    const std::string project = "bril";
    const std::string package = "brilhistogramsourcedtdemo";
    const std::string versions = BRIL_BRILHISTOGRAMSOURCEDTDEMO_FULL_VERSION_LIST;
    const std::string summary = "DT Demonstrator (Slice Test) Source";
    const std::string description = "A generic application to read based on generichistogramsource to read bril_histogram module";
    const std::string authors = "Mykyta Haranko";
    const std::string link = "";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
