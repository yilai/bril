#ifndef _histogramsourcegeneric_ReadoutInterface_h
#define _histogramsourcegeneric_ReadoutInterface_h

#include <string>
#include <vector>

namespace bril{
    namespace histogramsourcegeneric {
        class ReadoutInterface {
            public:
                // constructor
                ReadoutInterface(uint32_t p_histogram_readout_interval_usec) : m_histogram_readout_interval_usec(p_histogram_readout_interval_usec) {};
                virtual ~ReadoutInterface() = 0;
                // read write methods
                virtual uint32_t read(std::string reg_name) = 0;
                virtual std::vector<uint32_t> readBlock(std::string reg_name, uint32_t size) = 0;
                virtual void write(std::string reg_name, uint32_t data) = 0;
                virtual void writeBlock(std::string reg_name, std::vector<uint32_t> data) = 0;
                // dedicated reset and read histogram methods
                virtual void resetHistogram(std::string parent_node, uint8_t value);
                virtual std::vector<uint32_t> readHistogram(std::string parent_node);
                // config status
                virtual std::vector<std::pair<std::string, std::string>> getConfiguration() = 0;
                // config status fixed
                bool isInitialized() { return m_initialized; }
                bool isConfigured() { return m_configured; }

            protected:
                // connection
                bool m_initialized;
                bool m_configured;
                // constants
                uint32_t m_histogram_readout_interval_usec;
        };
    }
}

#endif