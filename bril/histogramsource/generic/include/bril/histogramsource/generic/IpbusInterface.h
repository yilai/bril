#ifndef _histogramsourcegeneric_IpbusInterface_h
#define _histogramsourcegeneric_IpbusInterface_h

#include "bril/histogramsource/generic/ReadoutInterface.h"
#include "uhal/uhal.hpp"
#include <string>
#include <vector>

namespace bril{
    namespace histogramsourcegeneric {
        class IpbusInterface : public ReadoutInterface {
            public:
                // base
                IpbusInterface(uhal::HwInterface hw_interface, uint32_t histogram_readout_interval_usec);
                ~IpbusInterface();
                // read write methods
                virtual uint32_t read(std::string reg_name);
                virtual std::vector<uint32_t> readBlock(std::string reg_name, uint32_t size);
                virtual void write(std::string reg_name, uint32_t data);
                virtual void writeBlock(std::string reg_name, std::vector<uint32_t> data);
                // parameter list
                virtual  std::vector<std::pair<std::string, std::string>> getConfiguration();
                // local methods
                bool checkConnection();

            private:
                // connection
                uhal::HwInterface m_hw_interface;

        };
    }
}

#endif