#ifndef _histogramsourcegeneric_Application_h_
#define _histogramsourcegeneric_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xgi/exception/Exception.h"
#include "eventing/api/Member.h"
#include "toolbox/Runtime.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "toolbox/ActionListener.h"
#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedInteger.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/shared/CompoundDataStreamer.h"
#include "interface/bril/HistogramGenericTopics.hh"

namespace bril{
    namespace histogramsourcegeneric {

        class ReadoutInterface;

        struct readout_status_t {
            uint32_t nhistograms;
            uint32_t nstablehistograms;
            uint32_t nskipped;
            uint32_t nduplicate;
            uint32_t nunknown;
            uint32_t current_fill;
            uint32_t current_run;
            uint32_t current_section;
            uint32_t current_nibble;
            uint32_t current_section_internal;
            uint32_t current_nibble_internal;
            uint32_t npulls_current;
            uint32_t npulls_static;
        };
        readout_status_t readout_status_init = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        struct tcds_external_t {
            uint32_t current_fill;
            uint32_t current_run;
            uint32_t current_section;
            uint32_t current_nibble;
        };
        tcds_external_t tcds_external_init = {0, 0, 0, 0};

        class Application:  public xdaq::Application,
                            public xdata::ActionListener,
                            public toolbox::ActionListener,
                            public xgi::framework::UIManager,
                            public eventing::api::Member {
            public:
                // basic
                XDAQ_INSTANTIATOR();
                Application(xdaq::ApplicationStub * s);
                ~Application();
                // xgi(web) callback
                void Default(xgi::Input * in, xgi::Output * out);
                void actionPerformed( xdata::Event& ); // infospace event callback
                void actionPerformed( toolbox::Event& ); // toolbox event callback

            private:
                bool m_shutdown_request;
                toolbox::mem::Pool * m_memory_pool;

            protected:
                // eventing config
                xdata::Vector<xdata::Properties> m_config_eventing_input;
                xdata::Vector<xdata::Properties> m_config_eventing_output;
                xdata::Boolean m_do_histogram_reset;
                xdata::UnsignedInteger m_config_backend_poll_interval;
                xdata::UnsignedInteger m_config_push_interval;
                xdata::UnsignedInteger m_config_frequency_nb;
                // eventing topics
                std::set<std::string> m_unready_buses;
                std::map<std::string, std::set<std::string>> m_topic_bus_map;
                typedef std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* >::iterator QueueStoreIt;
                std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* > m_topicoutqueues;
                // readout config
                xdata::String m_config_interface_type;
                xdata::Vector<xdata::Properties> m_config_tcp_boards;
                xdata::Properties m_config_ipbus_general;
                xdata::Vector<xdata::Properties> m_config_ipbus_boards;
                // external tcds
                xdata::Boolean m_config_use_external_tcds_info;
                std::map<std::pair<uint16_t, uint16_t>, toolbox::squeue< tcds_external_t >*> m_tcds_external_queue;
                void onMessage( toolbox::mem::Reference*, xdata::Properties& );
            protected:
                // readout interface and worksloops
                std::vector<ReadoutInterface*> m_backend_interface;
                std::map<uint16_t, std::vector<std::string>> m_backend_board_channel_prefix_map;
                std::map<uint16_t, std::vector<std::pair<uint16_t, uint16_t>>> m_backend_board_channel_algo_map;
                std::vector<std::pair<uint16_t, uint16_t>> m_backend_channel_algo_vec;
            private:
                toolbox::task::WorkLoop* m_readhist_wl;
                toolbox::task::WorkLoop* m_publishing_wl;
                void resetHistograms();
                void resetAndStartWorkLoops();
                void resynchroniseHistograms();
                // readout methods
                bool readHistogram(toolbox::task::WorkLoop* wl);
                bool processHistogram(std::pair<uint16_t, uint16_t> ch_algo, std::vector<uint32_t> histogram, interface::bril::shared::DatumHead* histogram_header);
                std::map<std::pair<uint16_t, uint16_t>, bril::histogramsourcegeneric::readout_status_t> m_readout_status;
                std::map<std::string, std::vector<std::string>> getReadoutStatus();
                // publishing
                bool publishing(toolbox::task::WorkLoop* wl);

            protected:
                // these methods are detector specific
                virtual std::string getApplicationSummary() { return std::string("Histogram Source Generic"); }
                virtual uint32_t getHistogramMaxsize() { return interface::bril::histogramgeneric_rawhistT::maxsize();}
                virtual std::string getHistogramTopicname() { return interface::bril::histogramgeneric_rawhistT::topicname();}
                virtual std::string getHistogramPayloadDict() { return interface::bril::histogramgeneric_rawhistT::payloaddict();}
                virtual void ConfigureHardware() {}; // not required but can be implemented

            protected:
                Application(const Application&);
                Application& operator=(const Application&);

        };

    }
}

#endif