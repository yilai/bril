#include <bril/histogramsource/generic/ReadoutInterface.h>

#include <cstring>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

namespace bril{
    namespace histogramsourcegeneric {

        void ReadoutInterface::resetHistogram(std::string parent_node, uint8_t value) {
            this->write(parent_node + ".user_reset", value);
        }

        std::vector<uint32_t> ReadoutInterface::readHistogram(std::string parent_node) {
            // return
            std::vector<uint32_t> data_histogram;

            // define fifo status veriables
            uint32_t fifo_stat = this->read(parent_node + ".fifo_stat");
            uint8_t fifo_empty = (fifo_stat >> 31) & 0x1;
            uint8_t fifo_valid = (fifo_stat >> 30) & 0x1;
            uint32_t fifo_words_cnt = (fifo_stat >> 0) & 0xfffffff;

            // wait for fifo to be ready
            if (!(fifo_empty || (!fifo_valid) || fifo_words_cnt == 0)) {
                // read one word and get data size
                uint32_t data_first_word = this->read(parent_node + ".fifo_data");
                if (((data_first_word >> 24) & 0xff) != 0xff) {
                    std::cout << "ERROR histogram header data is corrupted" << std::endl;
                }
                uint32_t data_number_of_words = (data_first_word >> 0) & 0xffff;

                // now wait enough words in fifo and read
                fifo_words_cnt = this->read(parent_node + ".fifo_stat.words_cnt");
                while (fifo_words_cnt < data_number_of_words-1) {
                    fifo_words_cnt = read(parent_node + ".fifo_stat.words_cnt");
                    usleep(m_histogram_readout_interval_usec);
                }

                // finally read the histogram
                data_histogram = this->readBlock(parent_node + ".fifo_data", data_number_of_words-1);

                // append first header word
                data_histogram.insert(data_histogram.begin(), data_first_word);
            }

            // return
            return data_histogram;

        }

    }
}