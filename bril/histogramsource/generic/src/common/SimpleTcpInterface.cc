#include <bril/histogramsource/generic/SimpleTcpInterface.h>

#include <cstring>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <zmq.hpp>
#include <unistd.h>

namespace bril{
    namespace histogramsourcegeneric {
        SimpleTcpInterface::SimpleTcpInterface(std::string host, std::string port, uint32_t histogram_readout_interval_usec) : ReadoutInterface(histogram_readout_interval_usec), m_host(host), m_port(port) {
            //m_socket = new zmq::socket_t(context, ZMQ_REQ);
            m_server_address = "tcp://" + m_host + ":" + m_port;
            m_initialized = true;
            m_configured = checkConnection();
        }

        SimpleTcpInterface::~SimpleTcpInterface() {

        }

        std::vector<char> SimpleTcpInterface::uint32_to_bytearray(uint32_t value) {
            // simple convert int into a four byte array
            std::vector<char> res;
            for(uint8_t i = 0; i < 4; i++) res.push_back((value >> ((3-i)*8)) & 0xff);
            return res;
        }

        bool SimpleTcpInterface::checkConnection() {
            //
            bool result;
            // open
            try {
                // dummy command
                std::vector<uint32_t> data;
                send_command(IPBUS_COMMAND_DUMMY, "", data);
                result = true;
            } catch (const std::exception& e) {
                throw("Failed to establish connection");
                result = false;
            }
            return result;
        }

        std::vector<uint32_t> SimpleTcpInterface::send_command(uint32_t command, std::string reg_name, std::vector<uint32_t> data) {
            // initialise socket
            zmq::context_t context (1);
            m_socket = new zmq::socket_t(context, ZMQ_REQ);
            m_socket->connect (m_server_address.c_str());

            // message that will be sent
            std::vector<char> message;

            // first append command
            uint32_t encoded_command = (command | (data.size() & 0x00ffffff));
            std::vector<char> encoded_command_bytearray = uint32_to_bytearray(encoded_command);
            message.insert(message.end(), encoded_command_bytearray.begin(), encoded_command_bytearray.end());

            // now append reg name length and reg name itself
            std::vector<char> encoded_reg_name_size_bytearray = uint32_to_bytearray(reg_name.length());
            message.insert(message.end(), encoded_reg_name_size_bytearray.begin(), encoded_reg_name_size_bytearray.end());
            message.insert(message.end(), reg_name.c_str(), reg_name.c_str()+reg_name.length());

            // append data
            std::vector<char> encoded_data_word;
            for(auto data_word : data) {
                encoded_data_word = uint32_to_bytearray(data_word);
                message.insert(message.end(), encoded_data_word.begin(), encoded_data_word.end());
            }

            // convert message into request
            zmq::message_t request (message.size());
            memcpy (request.data (), &(*message.begin()), message.size());
            m_socket->send(request);

            // response
            zmq::message_t response;
            m_socket->recv(&response);
            char *response_bytearray = static_cast<char*>(response.data());
            size_t response_size = response.size();
            std::vector<uint32_t> response_decoded;
            uint32_t res = 0;
            uint8_t counter = 3;
            for (uint32_t i = 0; i < response_size; i++) {
                res += ((uint32_t(response_bytearray[i]) & 0xff) << counter*8);
                if (counter == 0) {
                    response_decoded.push_back(res & 0xffffffff);
                    res = 0;
                    counter = 3;
                } else counter--;
            }

            // close socket
            m_socket->close();
            delete m_socket;

            return response_decoded;
        }

        uint32_t SimpleTcpInterface::read(std::string reg_name) {
            std::vector<uint32_t> data;
            return send_command(IPBUS_COMMAND_READ, reg_name, data).at(0);
        }

        std::vector<uint32_t> SimpleTcpInterface::readBlock(std::string reg_name, uint32_t size) {
            std::vector<uint32_t> data;
            data.push_back(size);
            return send_command(IPBUS_COMMAND_READ_BLOCK, reg_name, data);
        }

        void SimpleTcpInterface::write(std::string reg_name, uint32_t data) {
            std::vector<uint32_t> data_vec;
            data_vec.push_back(data);
            send_command(IPBUS_COMMAND_WRITE, reg_name, data_vec);
        }

        void SimpleTcpInterface::writeBlock(std::string reg_name, std::vector<uint32_t> data) {
            send_command(IPBUS_COMMAND_WRITE_BLOCK, reg_name, data);
        }

        void SimpleTcpInterface::global_reset() {
            std::vector<uint32_t> data;
            send_command(IPBUS_COMMAND_GLOBAL_RESET, "", data);
        }

        std::vector<std::pair<std::string, std::string>> SimpleTcpInterface::getConfiguration() {
            std::vector<std::pair<std::string, std::string>> result;
            result.push_back(std::make_pair("Interface type", "TCP"));
            result.push_back(std::make_pair("TCP server hostname", m_host));
            result.push_back(std::make_pair("TCP server port", m_port));
            result.push_back(std::make_pair("Full server address", m_server_address));
            result.push_back(std::make_pair("Histogram readout interval (us)", std::to_string(m_histogram_readout_interval_usec)));
            return result;
        }

    }
}