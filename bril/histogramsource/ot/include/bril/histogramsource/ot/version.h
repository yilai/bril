#ifndef _bril_histogramsourceot_version_h_
#define _bril_histogramsourceot_version_h_

#include "config/PackageInfo.h"

#define BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MAJOR 1
#define BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MINOR 0
#define BRIL_BRILHISTOGRAMSOURCEOT_VERSION_PATCH 0
#undef BRIL_BRILHISTOGRAMSOURCEOT_PREVIOUS_VERSIONS
#define BRIL_BRILHISTOGRAMSOURCEOT_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MAJOR, BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MINOR, BRIL_BRILHISTOGRAMSOURCEOT_VERSION_PATCH)
#ifndef BRIL_BRILHISTOGRAMSOURCEOT_PREVIOUS_VERSIONS
#define BRIL_BRILHISTOGRAMSOURCEOT_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MAJOR, BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MINOR, BRIL_BRILHISTOGRAMSOURCEOT_VERSION_PATCH)
#else
#define BRIL_BRILHISTOGRAMSOURCEOT_FULL_VERSION_LIST BRIL_BRILHISTOGRAMSOURCEOT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MAJOR, BRIL_BRILHISTOGRAMSOURCEOT_VERSION_MINOR, BRIL_BRILHISTOGRAMSOURCEOT_VERSION_PATCH)
#endif

namespace histogramsourceot
{
    const std::string project = "bril";
    const std::string package = "brilhistogramsourceot";
    const std::string versions = BRIL_BRILHISTOGRAMSOURCEOT_FULL_VERSION_LIST;
    const std::string summary = "OT Source";
    const std::string description = "A generic application to read based on generichistogramsource to read bril_histogram module";
    const std::string authors = "Mykyta Haranko";
    const std::string link = "";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
