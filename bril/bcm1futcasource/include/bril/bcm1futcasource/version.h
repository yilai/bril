// $Id$
#ifndef _bril_bcm1futcasource_version_h_
#define _bril_bcm1futcasource_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILBCM1FUTCASOURCE_VERSION_MAJOR 4
#define BRIL_BRILBCM1FUTCASOURCE_VERSION_MINOR 0
#define BRIL_BRILBCM1FUTCASOURCE_VERSION_PATCH 10
#define BRILBCM1FUTCASOURCE_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILBCM1FUTCASOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILBCM1FUTCASOURCE_VERSION_MAJOR,BRIL_BRILBCM1FUTCASOURCE_VERSION_MINOR,BRIL_BRILBCM1FUTCASOURCE_VERSION_PATCH)
#ifndef BRIL_BRILBCM1FUTCASOURCE_PREVIOUS_VERSIONS
#define BRIL_BRILBCM1FUTCASOURCE_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILBCM1FUTCASOURCE_VERSION_MAJOR,BRIL_BRILBCM1FUTCASOURCE_VERSION_MINOR,BRIL_BRILBCM1FUTCASOURCE_VERSION_PATCH)
#else
#define BRIL_BRILBCM1FUTCASOURCE_FULL_VERSION_LIST BRIL_BRILBCM1FUTCASOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILBCM1FUTCASOURCE_VERSION_MAJOR,BRIL_BRILBCM1FUTCASOURCE_VERSION_MINOR,BRIL_BRILBCM1FUTCASOURCE_VERSION_PATCH)
#endif
namespace brilbcm1futcasource{
  const std::string project= "bril";
  const std::string package = "brilbcm1futcasource";
  const std::string versions = BRIL_BRILBCM1FUTCASOURCE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bcm1futcasource";
  const std::string description = "collect histogram and orbit raw data";
  const std::string authors = "Spandan";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
