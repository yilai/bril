// $Id$
#ifndef _BRIL_BCM1FUTCASOURCE_BACKENDACQ_H_
#define _BRIL_BCM1FUTCASOURCE_BACKENDACQ_H_
#include <map>
#include <string>
#include <mutex>
#include "uhal/ConnectionManager.hpp"
#include "bril/bcm1futcasource/exception/Exception.h"
#include "bril/bcm1futcasource/BackEndConfig.h"
#include "bril/bcm1futcasource/Monitoring.h"
#include <boost/circular_buffer.hpp>
#include "bril/webutils/WebUtils.h"

namespace bril { namespace bcm1futcasource {

class Application;

class UbcmBcm1fSource : public utca::uBCM {

friend class BackEndAcq;

 public:
    UbcmBcm1fSource(const utca::ipmi & i1, const uhal::HwInterface& i2) : utca::uBCM(i1, i2), _ro_time_amp(200), _ro_time_occ(200) {
        //
        _tcds = utca::GLIBv3::tcds_info_t{0,0,0,0}; _ready = false;

        _numberOfDiscardedHistograms     = 0LL;

        _numberOfMissedHistograms        = 0LL;

        _numberOfMismatchedOrbitCounters = 0LL;

        _numberOfReadOccHistograms       = 0LL;

        _numberOfReadAmpHistograms       = 0LL;

        _numberOfOrbitLengthErrors       = 0LL;
    }

    virtual ~UbcmBcm1fSource() {};

    bool isReady() const { return _ready; }

    void setReady( bool ready, const char * exception_what = NULL )
    {
    	_ready = ready;

    	if ( ready ) _exception_what = "";

    	else
    	{
    		if ( exception_what ) _exception_what = exception_what;
    	}

    }

    const std::string & getLastExceptionMessage() const { return _exception_what; }

    utca::GLIBv3::tcds_info_t getTcdsHeader() const { return _tcds; }    

    void setTcdsHeader( utca::GLIBv3::tcds_info_t tcds ) { _tcds = tcds; }

    const std::map<std::string, std::string> & getInfo() const { return _info; }

    std::shared_ptr<webutils::WebChart> getOccupancyChart() const { return _occupancy_chart; };
    std::shared_ptr<webutils::WebChart> getAmplitudeChart() const { return _amplitude_chart; };
    std::shared_ptr<webutils::WebChart> getRawOrbitChart() const { return _raw_orbit_chart; };

    void writeDataAsJSON(const std::string & device, const cgicc::Cgicc& query, xgi::Output* out);

 protected:

    void storeOrbitData();

 protected:

    bool _ready; // true if connected and configured

    // TCDS header of the last published  histograms

    utca::GLIBv3::tcds_info_t  _tcds;

    // Hardware info from  utca

    utca::uBCM::info_t  _hwinfo;

    // Data to Highcharts

    std::map<std::string, std::vector<float>> _occupancy_data;
    std::map<std::string, std::vector<float>> _amplitude_data;
    std::map<std::string, std::vector<float>> _raw_orbit_data;

    std::shared_ptr<webutils::WebChart> _occupancy_chart;
    std::shared_ptr<webutils::WebChart> _amplitude_chart;
    std::shared_ptr<webutils::WebChart> _raw_orbit_chart;

    std::map<std::string, std::string> _info;

    std::string _exception_what;

    long long _numberOfDiscardedHistograms;

    long long _numberOfMissedHistograms;

    long long _numberOfMismatchedOrbitCounters;

    long long _numberOfReadOccHistograms;

    long long _numberOfReadAmpHistograms;

    long long _numberOfOrbitLengthErrors;

    boost::circular_buffer<float> _ro_time_amp;

    boost::circular_buffer<float> _ro_time_occ;

    std::ostringstream _oss;

    std::chrono::time_point<std::chrono::high_resolution_clock> _tp_amp;
    std::chrono::time_point<std::chrono::high_resolution_clock> _tp_occ;
    std::chrono::time_point<std::chrono::high_resolution_clock> _tp_info;
};

class BackEndAcq {
 public:
    BackEndAcq(bril::bcm1futcasource::Application * application, const BackEndConfig & cfg);

    void connect();
 
    void acquireOrbit();  // read-out all raw data in all channels;

    // read one FMC histograms from a workloop;
    void resetHistograms();
    void acquireHistograms(const std::string & device);  

    std::shared_ptr<UbcmBcm1fSource> getUBCM(const std::string & device);

    const std::map<std::string, std::string> & getInfo(const std::string & device) const;

    unsigned int getNumberOfReadyDevices () const;

    void updateInfo();

    void initWebPlotting(const std::string & url);

 protected:
    //
    Application * _application;    //  To bring logger into the class

    const BackEndConfig & _cfg;

    //  IPbus manager - singleton - ownership is shared with all uTCA boards
    //
    std::shared_ptr<uhal::ConnectionManager>                  _ipbus_cmg;

    std::shared_ptr<utca::ipmi_base>                          _ipmi_cmg;

    std::map<std::string, std::shared_ptr<UbcmBcm1fSource>  > _ubcm;

 protected: 

    void logAcqError(unsigned int channelid, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const;

    void logAcqError(const std::string & device, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const;

    void logAcqWarning(const std::string & device, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const;

    void logAcqDebug(const std::string & device, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const;

    bool rawDataAcquisitionPermitted() const;

    bool reboot(const std::string & device);
};
}  // namespace bcm1futcasource
}  // namespace bril

#endif
