// $Id$
#include <typeinfo>
#include <thread>
#include <numeric>
#include <chrono>
#include <iomanip>
#include "bril/bcm1futcasource/BackEndAcq.h"
#include "xcept/tools.h"
#include "bril/bcm1futcasource/Application.h"
#include <boost/filesystem.hpp>

bril::bcm1futcasource::BackEndAcq::BackEndAcq(bril::bcm1futcasource::Application * application, const BackEndConfig & cfg) : _application(application), _cfg(cfg){
    /**
    */
    uhal::disableLogging();

    _ipmi_cmg = std::make_shared<utca::ipmi_base>();
}

void bril::bcm1futcasource::BackEndAcq::connect() {
    /**
    */
    try {
        _ipbus_cmg = std::make_shared<uhal::ConnectionManager>(_cfg.getIPbusConfig());
        //
    }catch(uhal::exception::exception & e) {
        //
        std::string msg("uhal::ioManager failed. "); msg += e.what(); boost::trim_right(msg);

        XCEPT_RAISE(bril::bcm1futcasource::exception::ubcmException, msg);
    }

    std::vector<std::string> devices = _cfg.getListOfDevices();

    size_t nbMalfunctioningBoards = 0;

    for ( std::vector<std::string>::const_iterator it = devices.begin(); it != devices.end(); ++it ) {
        //
        std::string host = _cfg.getIpmiHostname(*it);

        if ( host.size() == 0 ) {
            //
            std::string msg("Not found ipmi hostname of a device "); msg += *it;

            XCEPT_RAISE(bril::bcm1futcasource::exception::Exception, msg);
        }

        std::string slot = _cfg.getAmcSlotNumber(*it);

        if ( slot.size() == 0 ) {
            //
            std::string msg("Not found AMC slot number of a device "); msg += *it;

            XCEPT_RAISE(bril::bcm1futcasource::exception::Exception, msg);
        }
        int amc = 0;

        try {
            amc = std::stoi(slot);
            //
        } catch(std::invalid_argument & e ) {
            //
            std::string msg("Invalid AMC slot number of a device "); msg += *it;

            XCEPT_RAISE(bril::bcm1futcasource::exception::Exception, msg);
            //
        } catch(std::out_of_range & e) {
            //
            std::string msg("Out of range AMC slot number of a device "); msg += *it;

            XCEPT_RAISE(bril::bcm1futcasource::exception::Exception, msg);
        }

        utca::ipmi ipmi_if(_ipmi_cmg, host, amc);

        std::map<std::string, std::shared_ptr<UbcmBcm1fSource>  >::iterator x;

        bool y;

        try {
            uhal::HwInterface ipbus_if = _ipbus_cmg->getDevice(*it);

            std::tie(x, y) = _ubcm.emplace(*it, std::make_shared<UbcmBcm1fSource>(ipmi_if, ipbus_if));

            LOG4CPLUS_INFO(_application->getApplicationLogger(), "Initialization of the device " << (*it));
            //
        } catch( uhal::exception::ConnectionUIDDoesNotExist & e ) {
            //
            std::string msg("IPbus "); msg += e.what(); boost::trim_right(msg);

            XCEPT_RAISE(bril::bcm1futcasource::exception::ubcmException, msg);
            //
        }
        try {
        	LOG4CPLUS_INFO(_application->getApplicationLogger(),  "init(utca::fw_BCM1F_FMC125, utca::quad_30spbx)");

            (*x).second->init(utca::fw_BCM1F_FMC125, utca::quad_30spbx, _cfg.getOrbitOffsetCoarse(*it), _cfg.getOrbitOffsetFine(*it), _cfg.getOrbitOffsetUltraFine(*it), false);
            //
        } catch( utca::configuration_error & e ) {
            //
            LOG4CPLUS_ERROR(_application->getApplicationLogger(), e.what());

            nbMalfunctioningBoards++; continue;
            //
        } catch( utca::communication_error & e ) {
            //
            LOG4CPLUS_ERROR(_application->getApplicationLogger(),  e.what());

            (*x).second->setReady(false,e.what()); nbMalfunctioningBoards++; continue;
        }
        LOG4CPLUS_INFO(_application->getApplicationLogger(),  "orbit_phase_set()");

        (*x).second->orbit_phase_set(_cfg.getOrbitPhase(*it));

        LOG4CPLUS_INFO(_application->getApplicationLogger(),  "adc_bit_align()");

        (*x).second->adc_bit_align();

        LOG4CPLUS_INFO(_application->getApplicationLogger(),  "adc_test_ramp()");

        (*x).second->adc_test_ramp();

        LOG4CPLUS_INFO(_application->getApplicationLogger(),  "cdc_align()");

        (*x).second->cdc_align();

        LOG4CPLUS_INFO(_application->getApplicationLogger(),  "orbit_length_error_reset()");

        (*x).second->orbit_length_error_reset();

        try {
        	LOG4CPLUS_INFO(_application->getApplicationLogger(),  "configure_raw_data");
        	if (_cfg.isRawDataSelfTrigger()) LOG4CPLUS_INFO(_application->getApplicationLogger(),  "NOTE: Running in raw data self-trigger mode");
        	if (_cfg.isRawDataPeakfinderTrigger()) LOG4CPLUS_INFO(_application->getApplicationLogger(),  "NOTE: Running in rraw data peakfinder-trigger mode");
        	if (_cfg.isRawDataSelfTrigger() && _cfg.isRawDataPeakfinderTrigger()) LOG4CPLUS_WARN(_application->getApplicationLogger(),  "You should not enable both self and raw data trigger");
        	uint8_t c_threshold = _cfg.getRawDataSelfTriggerThreshold();
            utca::GLIBv3::thresholds_t t{ c_threshold, c_threshold, c_threshold, c_threshold };
            (*x).second->configure_raw_data(false, t, _cfg.isRawDataSelfTrigger(), _cfg.isRawDataPeakfinderTrigger());
            //
        } catch( utca::communication_error & e){
            //
            std::string msg("IPbus "); msg += e.what(); boost::trim_right(msg);

            LOG4CPLUS_ERROR(_application->getApplicationLogger(), msg);

            (*x).second->setReady(false,e.what()); nbMalfunctioningBoards++; continue;
        }
        try {
            // basic peak finder (not used anymore)
            //LOG4CPLUS_INFO(_application->getApplicationLogger(),  "configure_threshold_finder");
            //(*x).second->configure_threshold_finder( _cfg.getBasicAmplitudeThresholds(*it), _cfg.getTimeOverThresholds(*it));

        	LOG4CPLUS_INFO(_application->getApplicationLogger(),  "configure_peak_finder");

            // peakfinder trigger threshold
            uint16_t c_threshold_low = _cfg.getPeakfinderTriggerThresholdLow();
            uint16_t c_threshold_high = _cfg.getPeakfinderTriggerThresholdHigh();

            (*x).second->configure_peak_finder(_cfg.getDerivativeDerivativeThresholds(*it),	_cfg.getDerivativeIsolationThresholds(*it), _cfg.getDerivativeAmplitudeThresholds(*it), c_threshold_low, c_threshold_high);

        	LOG4CPLUS_INFO(_application->getApplicationLogger(),  "configure_histograms");

            (*x).second->configure_histograms(4);

            LOG4CPLUS_INFO(_application->getApplicationLogger(),  "check_system()");

            std::string status;
            uint32_t ntries_left = 3;
            while(ntries_left > 0) {
                status = (*x).second->check_system();
                if (status.empty()) break;
                else {
                    ntries_left--;
                    LOG4CPLUS_WARN(_application->getApplicationLogger(),  "check_system() : failed, retrying");
                }
            }

            if ( !status.empty() ) {
                //
                std::vector<std::string> errors; boost::algorithm::split(errors, status, boost::is_any_of("\n"));

                for ( auto && s : errors ) {
                    //
                    if ( s.empty() )  s = "!!! Failed to configure !!!";

                    LOG4CPLUS_ERROR(_application->getApplicationLogger(), s); 
                }
                (*x).second->setReady(false,"Failed system check"); nbMalfunctioningBoards++; continue;
            }
            //
        } catch( utca::communication_error & e){
            //
            std::string msg("IPbus "); msg += e.what(); boost::trim_right(msg);

            LOG4CPLUS_ERROR(_application->getApplicationLogger(), msg);

            (*x).second->setReady(false,e.what());   nbMalfunctioningBoards++; continue;
        }

        (*x).second->setReady(true);

        {
			std::lock_guard<std::mutex> lock(_application->_mutex);

			(*x).second->_info.clear();

			for( auto topic : (*x).second->info() ){

				std::string msg = topic.first; msg += " = " + topic.second;

				LOG4CPLUS_INFO(_application->getApplicationLogger(),  msg);

				(*x).second->_info.insert(topic);
			}
        }

        LOG4CPLUS_INFO(_application->getApplicationLogger(), "Done. The device is ready.");
        LOG4CPLUS_INFO(_application->getApplicationLogger(), "");

    }
    LOG4CPLUS_INFO(_application->getApplicationLogger(), "Status: " << (devices.size() - nbMalfunctioningBoards) << " of " << devices.size() << " initialized.");
}

std::shared_ptr<bril::bcm1futcasource::UbcmBcm1fSource> bril::bcm1futcasource::BackEndAcq::getUBCM(const std::string & device) {
    /**
    */
    for( auto && hw : _ubcm ) {
        //
        if ( hw.first == device ) return hw.second;
    }
    return nullptr;
}

void bril::bcm1futcasource::BackEndAcq::acquireOrbit(){
//     /**
//     Orbit raw data have to be acquired only when histograms are already published for the given TCSD slot
//     */

    unsigned int nbOfFinishedDevices = 0;
    
    for ( auto u : _ubcm )
    {
        try {
            // check orbit length
            auto timing = u.second->timing();
            if ( timing.orbit_length_errors > u.second->_numberOfOrbitLengthErrors ) {
                u.second->_numberOfOrbitLengthErrors = timing.orbit_length_errors;
                logAcqWarning(u.first,timing.tcds, " Orbit length error detected, now counter is " + std::to_string(u.second->_numberOfOrbitLengthErrors));
            }

            //
            std::unique_ptr<utca::GLIBv3::raw_data_t> raw_orbit = u.second->read_raw_data();

            if ( raw_orbit == nullptr )
            {
                std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );

                raw_orbit = u.second->read_raw_data();

                if ( raw_orbit == nullptr ) 
                {
                    logAcqDebug(u.first,_application->_tcds,"Orbit acquisition timeout. Skipped."); continue;
                }
            }

            if ( raw_orbit->tcds.lhc_fill + raw_orbit->tcds.cms_run + raw_orbit->tcds.lumi_section + raw_orbit->tcds.lumi_nibble  == 0 )
            {
                // Use software TCDS info from BRILDAQ eventing

                std::lock_guard<std::mutex> lock(_application->_mutex);

                raw_orbit->tcds.lhc_fill     = _application->_tcds.lhc_fill;
                raw_orbit->tcds.cms_run      = _application->_tcds.cms_run;
                raw_orbit->tcds.lumi_section = _application->_tcds.lumi_section;
                raw_orbit->tcds.lumi_nibble  = _application->_tcds.lumi_nibble;

                if ( raw_orbit->tcds.lhc_fill + raw_orbit->tcds.cms_run + raw_orbit->tcds.lumi_section + raw_orbit->tcds.lumi_nibble  == 0 )
                {
                    // beam topic is not yet received. No TCDS information. The acquisition is skipped.

                    return;
                }

                logAcqDebug(u.first,raw_orbit->tcds,"orbit acq : software TCDS");
            }
            else
            {
            	logAcqDebug(u.first,raw_orbit->tcds,"orbit acq : hardware TCDS");
            }
            // Print readout duration and collect statistics

            std::string msg("Readout orbit time: "); msg += std::to_string(raw_orbit->ro_time.count())  + " us";

            logAcqDebug(u.first,raw_orbit->tcds,msg);

            for( unsigned int channel =0; channel < utca::N_MAX_CH; channel++ ) {
                //
                toolbox::mem::Reference * rawrefraw = nullptr;

                if ( _cfg.getChannelId(u.first,channel) == 0 ) continue;

                try
                {
                    {
                        std::lock_guard<std::mutex> lock(_application->_mutex);

                        rawrefraw = _application->_poolFactory->getFrame(_application->_memPool,interface::bril::bcm1futcarawdataT::maxsize());
                    }
                    rawrefraw->setDataSize(interface::bril::bcm1futcarawdataT::maxsize());

                    interface::bril::shared::DatumHead * rawheaderraw = (interface::bril::shared::DatumHead*)(rawrefraw->getDataLocation());

                    uint8_t * dataraw = reinterpret_cast<uint8_t *>(rawheaderraw->payloadanchor);

                    toolbox::TimeVal tv = toolbox::TimeVal::gettimeofday();

                    rawheaderraw->setTime(raw_orbit->tcds.lhc_fill, raw_orbit->tcds.cms_run,raw_orbit->tcds.lumi_section,raw_orbit->tcds.lumi_nibble,tv.sec(),tv.millisec());

                    rawheaderraw->setResource(interface::bril::shared::DataSource::BCM1F,_cfg.getAlgoId(u.first,channel),_cfg.getChannelId(u.first,channel),interface::bril::shared::StorageType::UINT8);

                    if ( _cfg.isOrbitRequestedEveryNB1() ) 
                    {
                        rawheaderraw->setFrequency(interface::bril::shared::FrequencyType::NB1);
                    }
                    else 
                    {
                        rawheaderraw->setFrequency(interface::bril::shared::FrequencyType::NB4);
                    }

                    rawheaderraw->setTotalsize(interface::bril::bcm1futcarawdataT::maxsize());

                    if ( raw_orbit->data[channel].size() < interface::bril::bcm1futcarawdataT::n() ) {
                        //
                        std::ostringstream oss; oss << u.first << " : fewer number of samples in orbit data " <<  interface::bril::bcm1futcarawdataT::n();

                        oss << " (topic) / " << raw_orbit->data[channel].size() << " (acquired)"; rawrefraw->release();

                        logAcqError(_cfg.getChannelId(u.first,channel),raw_orbit->tcds, oss.str());           continue;
                    }

                    std::copy(raw_orbit->data[channel].begin(), raw_orbit->data[channel].begin()+interface::bril::bcm1futcarawdataT::n(),dataraw);

                    // ============================================================================
                    //
                    // Data for Highcharts plots in Hyperdaq
                    //
                    std::vector<float> V(interface::bril::bcm1futcarawdataT::n());

                    for ( unsigned int iV=0; iV < interface::bril::bcm1futcarawdataT::n(); iV++ )
                    {
                    	V[iV] = static_cast<float>(dataraw[iV]);
                    }
                    u.second->_raw_orbit_data[_cfg.getPlotLegend(u.first,channel)] = std::move(V);

                    if(u.second->_raw_orbit_chart) u.second->_raw_orbit_chart->newDataReady();
                    // ============================================================================


                    std::set<std::string> bus = _cfg.getListOfPublicationBuses(interface::bril::bcm1futcarawdataT::topicname());

                    for (std::set<std::string>::const_iterator it = bus.begin(); it != bus.end(); ++it) {
                        //
                        toolbox::mem::Reference * rawref = nullptr;

                        // clone the frame to publish in more than one eventing bus

                        if ( std::next(it,1) != bus.end() )
                        {
                            try
                            {
                                rawref = _application->_poolFactory->getFrame(_application->_memPool,rawrefraw->getDataSize());

                                memcpy(rawref->getDataLocation(),rawrefraw->getDataLocation(),rawrefraw->getDataSize());
                            }
                            catch(toolbox::mem::exception::Exception &ex)
                            {
                              std::string msg("Failed to allocate an extra memory frame to publish raw orbit data into the bus: ");

                              msg += *it; logAcqError(_cfg.getChannelId(u.first,channel),raw_orbit->tcds,msg); continue;
                            }
                        }
                        else
                        {
                            rawref = rawrefraw; rawrefraw = nullptr;
                        }
                        xdata::Properties plistraw;

                        plistraw.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);

                        std::string payloaddict = interface::bril::bcm1futcarawdataT::payloaddict();

                        plistraw.setProperty("PAYLOAD_DICT",payloaddict);

                        if ( _application->_terminate )
                        {
			  if ( rawrefraw ) {
			    rawrefraw->release();
			  }
			  return;
                        }
                        std::lock_guard<std::mutex> lock(_application->_mutex);

                        if ( !_cfg.isCommissioningMode() )
                        {
                            if ( !_application->_do_store ) plistraw.setProperty("NOSTORE","1");
                        }
                        try
                        {
                            _application->getEventingBus(*it).publish(interface::bril::bcm1futcarawdataT::topicname(),rawref,plistraw);
                        }
                        catch(std::exception & ex)
                        {
			  std::string msg("Failed to publish raw orbit data into the bus: ");
			  msg += *it;

			  logAcqError(_cfg.getChannelId(u.first,channel),raw_orbit->tcds,msg);

			  if ( rawref ) {
			    rawref->release();
			  }
			  continue;
                        }
                    }
                }
                catch(toolbox::mem::exception::Exception &ex)
		 {
		    std::string msg("Failed to allocate memory frame to publish orbit raw data");

                  logAcqError(_cfg.getChannelId(u.first,channel),raw_orbit->tcds,msg);

                  if ( rawrefraw ) {
		    rawrefraw->release();
		  }
		  continue;
                }
            }
        }
        catch( std::exception & e) {
            //
            std::lock_guard<std::mutex> lock(_application->_mutex);

            // u.second->setReady(false);  // Re-enable when the hardware reset is done

            logAcqError(u.first,_application->_tcds,e.what());
        }
        nbOfFinishedDevices++;
    }
}

void bril::bcm1futcasource::BackEndAcq::resetHistograms() {
    LOG4CPLUS_INFO(_application->getApplicationLogger(),"Resetting Histograms");
    for ( auto u : _ubcm )
    {
        u.second->reset_occupancy();
        u.second->reset_amplitude();
    }
}

void bril::bcm1futcasource::BackEndAcq::acquireHistograms(const std::string & device) {
    /**
    */
    bool isDeviceReady = false;
    
    {
        std::lock_guard<std::mutex> lock(_application->_mutex); 

        if ( _ubcm.find(device) != _ubcm.end() ) isDeviceReady = _ubcm[device]->isReady();
    }

    if ( !isDeviceReady ) {
        //
        std::this_thread::sleep_for( std::chrono::milliseconds( 600 ) ); return;
    }

    std::shared_ptr<UbcmBcm1fSource> hw = getUBCM(device);
    
    std::chrono::time_point<std::chrono::high_resolution_clock> readout_time_occ,   readout_time_amp, readout_time_info;

    std::chrono::time_point<std::chrono::high_resolution_clock> readout_start_occ,  readout_start_amp, readout_start_info;

    try {     
        //
        if ( _application->_terminate ) return;

        // Reading occupancy histograms =====================================================
        readout_start_occ = std::chrono::high_resolution_clock::now();

        // the thing is that when CMS is reconfigured it may cause a timeout - lets give it a bit of slack
        std::unique_ptr<utca::GLIBv3::occ_histogram_t> occ_histograms;
        uint8_t ntries_left = 3;
        while (ntries_left > 0) {
            try {
                occ_histograms = hw->read_occupancy();
                ntries_left = 0;
            } catch (utca::histogram_timeout_error &e) {
                std::ostringstream oss;
                oss << device << " : " << e.what() << " - TIMEOUT IS NORMAL WHEN CMS RECONFIGURES, REPEATING";
                LOG4CPLUS_INFO(_application->getApplicationLogger(),oss.str());
                if (--ntries_left == 0) throw;
            }
        }

        readout_time_occ = std::chrono::high_resolution_clock::now();
        // ==================================================================================

        // Reading amplitude histograms =====================================================
        readout_start_amp = std::chrono::high_resolution_clock::now();

        // the thing is that when CMS is reconfigured it may cause a timeout - lets give it a bit of slack
        std::unique_ptr<utca::GLIBv3::amp_histogram_t> amp_histograms;
        ntries_left = 3;
        while (ntries_left > 0) {
            try {
                amp_histograms = hw->read_amplitude();
                ntries_left = 0;
            } catch (utca::histogram_timeout_error &e) {
                std::ostringstream oss;
                oss << device << " : " << e.what() << " - TIMEOUT IS NORMAL WHEN CMS RECONFIGURES, REPEATING";
                LOG4CPLUS_INFO(_application->getApplicationLogger(),oss.str());
                if (--ntries_left == 0) throw;
            }
        }

        readout_time_amp = std::chrono::high_resolution_clock::now();
        // ==================================================================================

        // MH: why would this be zero pointer?
        if ( occ_histograms != nullptr ) {
            if ( occ_histograms->tcds.lumi_nibble % 4 != 0 ) logAcqWarning(device, occ_histograms->tcds, "Occupancy: Odd TCDS nibble");
        }
        if ( amp_histograms != nullptr ) {
            if ( amp_histograms->tcds.lumi_nibble % 4 != 0 ) logAcqWarning(device, amp_histograms->tcds, "Amplitude: Odd TCDS nibble");
        }
        if ( _application->_terminate ) return;

        // MH : not sure this should every happen
        if ( amp_histograms && !occ_histograms )
        {
            occ_histograms = hw->read_occupancy();
        }
        else if ( !amp_histograms && occ_histograms )
        {
            amp_histograms = hw->read_amplitude();
        }
        if ( _application->_terminate ) return;

        // =============================================================================
        // Read and store hardware info

        readout_start_info = std::chrono::high_resolution_clock::now();

        hw->_hwinfo.clear();   for( auto topic : hw->info() ) hw->_hwinfo.insert(topic);

        readout_time_info = std::chrono::high_resolution_clock::now();
        // =============================================================================

        if ( occ_histograms == nullptr ) // no luck :-(
        {
            if ( _application->_tcds.lhc_fill + _application->_tcds.cms_run + _application->_tcds.lumi_section + _application->_tcds.lumi_nibble  == 0 ) return;

            std::string msg("Failed reading occupancy histograms"); logAcqError(device,_application->_tcds,msg); return;
        }

        if ( _cfg.isCommissioningMode() )
        {
        	//  Printing information about time points of histogram readout

        	hw->_oss << "occ, " ;

        	std::chrono::duration<double> diff;

        	diff = readout_time_info-readout_start_info;

        	hw->_oss << std::setw(12) << diff.count() << ", ";

        	hw->_tp_info = readout_time_info;

        	diff = readout_time_occ-hw->_tp_occ;

        	hw->_oss << std::setw(12) << diff.count() << ", ";

        	diff = readout_time_occ-readout_start_occ;

        	hw->_oss << std::setw(12) << diff.count() << ", ";

        	hw->_tp_occ = readout_time_occ;

			hw->_oss << occ_histograms->tcds.lhc_fill << ", " << occ_histograms->tcds.cms_run << ", ";

			hw->_oss << occ_histograms->tcds.lumi_section << ", " << occ_histograms->tcds.lumi_nibble << ", ";

			hw->_oss << occ_histograms->counter << ", " << occ_histograms->normalization;

			if ( amp_histograms != nullptr )
			{
	        	hw->_oss << ", amp, ";

	        	diff = readout_time_amp-hw->_tp_amp;

	        	hw->_oss << std::setw(12) << diff.count() << ", ";

	        	diff = readout_time_amp-readout_start_amp;

	        	hw->_oss << std::setw(12) << diff.count() << ", ";

	        	hw->_tp_amp = readout_time_amp;

				hw->_oss << amp_histograms->tcds.lhc_fill << ", " << amp_histograms->tcds.cms_run << ", ";

				hw->_oss << amp_histograms->tcds.lumi_section << ", " << amp_histograms->tcds.lumi_nibble << ", ";

				hw->_oss << amp_histograms->counter << ", " << amp_histograms->normalization << std::endl;
			}
			else
			{
				hw->_oss << std::endl;
			}

			if ( hw->_numberOfReadOccHistograms % 128LL == 0LL )
			{
				const std::string folder = "/globalscratch/brilpro/bcm1futca";

				boost::filesystem::create_directories(folder);

				std::ofstream ofs;

				ofs.open(folder + "/" + device+".csv",std::ios::out | std::ios::app);

				if ( ofs.is_open() )
				{
					ofs << hw->_oss.str();  ofs.close();
				}
				hw->_oss.str("") ;
			}
        }
        hw->_numberOfReadOccHistograms++; if ( amp_histograms != nullptr ) hw->_numberOfReadAmpHistograms++;

        if ( occ_histograms->tcds.lhc_fill + occ_histograms->tcds.cms_run + occ_histograms->tcds.lumi_section + occ_histograms->tcds.lumi_nibble  == 0 )
        {
            // Use software TCDS info from BRILDAQ eventing

            std::lock_guard<std::mutex> lock(_application->_mutex);

            occ_histograms->tcds.lhc_fill     = _application->_tcds.lhc_fill;
            occ_histograms->tcds.cms_run      = _application->_tcds.cms_run;
            occ_histograms->tcds.lumi_section = _application->_tcds.lumi_section;
            occ_histograms->tcds.lumi_nibble  = _application->_tcds.lumi_nibble;

            // We assume if the occupancy histogram doesn't have TCDS counters, the amplitude one either

            if ( amp_histograms != nullptr )
            {
                amp_histograms->tcds.lhc_fill     = _application->_tcds.lhc_fill;
                amp_histograms->tcds.cms_run      = _application->_tcds.cms_run;
                amp_histograms->tcds.lumi_section = _application->_tcds.lumi_section;
                amp_histograms->tcds.lumi_nibble  = _application->_tcds.lumi_nibble;
            }
            // else { just ignore. We have to publish occupancy histograms in any case }

            if ( occ_histograms->tcds.lhc_fill + occ_histograms->tcds.cms_run + occ_histograms->tcds.lumi_section + occ_histograms->tcds.lumi_nibble  == 0 )
            {
                // beam topic is not yet received. No TCDS information. The acquisition is skipped.

                return;
            }
            logAcqDebug(device,occ_histograms->tcds,"histograms: software TCDS");
        }
        else
        {
        	// ================================================================================
        	// Validate TCDS counters to confirm no duplication or missed NB4

        	// 1. Check if amplitude and occupancy histograms both have the same TCDS counters

        	if ( amp_histograms && occ_histograms )
        	{
        		if ( occ_histograms->tcds.lhc_fill != amp_histograms->tcds.lhc_fill )
        		{
        			std::stringstream ss;

        			ss << "Fill number mismatches in occupancy and amplitude histograms: (amp) fill #  " << amp_histograms->tcds.lhc_fill;

        			logAcqDebug(device,occ_histograms->tcds,ss.str());

        			amp_histograms->tcds.lhc_fill = occ_histograms->tcds.lhc_fill;
         		}
        		if ( occ_histograms->tcds.cms_run != amp_histograms->tcds.cms_run )
        		{
        			std::stringstream ss;

        			ss << "CMS run number mismatches in occupancy and amplitude histograms: (amp) run #  " << amp_histograms->tcds.cms_run;

        			logAcqDebug(device,occ_histograms->tcds,ss.str());

        			amp_histograms->tcds.cms_run = occ_histograms->tcds.cms_run;
         		}
        		if ( occ_histograms->tcds.lumi_section != amp_histograms->tcds.lumi_section )
        		{
        			std::stringstream ss;

        			ss << "Lumi section number mismatches in occupancy and amplitude histograms: (amp) LS #  " << amp_histograms->tcds.lumi_section;

        			logAcqDebug(device,occ_histograms->tcds,ss.str());

        			amp_histograms->tcds.lumi_section = occ_histograms->tcds.lumi_section;
         		}
        		if ( occ_histograms->tcds.lumi_nibble != amp_histograms->tcds.lumi_nibble )
        		{
        			std::stringstream ss;

        			ss << "Lumi nibbble counter mismatches in occupancy and amplitude histograms: (amp) NB #  " << amp_histograms->tcds.lumi_nibble;

        			logAcqDebug(device,occ_histograms->tcds,ss.str());

        			amp_histograms->tcds.lumi_nibble = occ_histograms->tcds.lumi_nibble;
         		}
        		if ( occ_histograms->counter != amp_histograms->counter )
        		{
        			hw->_numberOfMismatchedOrbitCounters++;

        			std::stringstream ss;

        			ss << "Histogram counter mismatches in occupancy and amplitude histograms: occ #  " << occ_histograms->counter;

        			ss << ", amp # " << amp_histograms->counter; logAcqWarning(device,occ_histograms->tcds,ss.str());
         		}
        	}

        	// 2. Check for duplication

        	if ( occ_histograms->tcds.lhc_fill == hw->_tcds.lhc_fill         &&  occ_histograms->tcds.cms_run == hw->_tcds.cms_run
        	  && occ_histograms->tcds.lumi_section == hw->_tcds.lumi_section &&  occ_histograms->tcds.lumi_nibble == hw->_tcds.lumi_nibble )
        	{
        		if ( occ_histograms->tcds.lhc_fill != 0 )
        		{
        			hw->_numberOfDiscardedHistograms++;

        			logAcqWarning(device,occ_histograms->tcds,"Duplication of histograms. Discarded."); return;
        		}
        	}

        	// 2. Check for missed histograms

        	if ( hw->_tcds.lumi_section > 0 &&  occ_histograms->tcds.lumi_section > 0 && hw->_tcds.cms_run == occ_histograms->tcds.cms_run )
			{
				long NB1 = hw->_tcds.lumi_section           * 64 + hw->_tcds.lumi_nibble;

				long NB2 = occ_histograms->tcds.lumi_section* 64 + occ_histograms->tcds.lumi_nibble;

				if ( NB2 > NB1 && (NB2-NB1)/4 > 1 && NB1 > 0 )
				{
					hw->_numberOfMissedHistograms += (NB2-NB1)/4 - 1;

					std::stringstream ss;

					ss << "There is a gap of " << (NB2-NB1)/4 - 1 << " NB4 in time series of histogram acquisition.";

					logAcqWarning(device,occ_histograms->tcds,ss.str());

					if ( _cfg.isCommissioningMode() )
					{
						const std::string folder = "/globalscratch/brilpro/bcm1futca";

						boost::filesystem::create_directories(folder);

						std::ofstream ofs;

						ofs.open(folder + "/" + device+"-missedhistograms.csv",std::ios::out | std::ios::app);

						if ( ofs.is_open() )
						{
							ofs << device << ", " << hw->_tcds.lhc_fill << ", " << hw->_tcds.cms_run << ", ";

							ofs << hw->_tcds.lumi_section << ", " << hw->_tcds.lumi_nibble << ", ";

							ofs << occ_histograms->tcds.lumi_section << ", " << occ_histograms->tcds.lumi_nibble;

							for( auto topic : hw->info() )
							{
								if ( topic.first.find("TCDS") != std::string::npos )
								{
									ofs << std::endl << topic.first << ", " << topic.second;
								}
							}
							ofs << std::endl;  ofs.close();
						}
						hw->_oss.str("") ;
					}
				}
				else if ( NB2 < NB1 )
				{
					std::stringstream ss;

					hw->_numberOfDiscardedHistograms++;

					ss << "TCDS counters are older than ones already published: fill # " << hw->_tcds.lhc_fill;

					ss << " run # " << hw->_tcds.cms_run << " LS # " << hw->_tcds.lumi_section;

					ss << " NB # " << hw->_tcds.lumi_nibble;

					hw->setTcdsHeader(occ_histograms->tcds);

					logAcqError(device,occ_histograms->tcds,ss.str()); return;

					if ( _cfg.isCommissioningMode() )
					{

					}
				}
        	}
        	// ================================================================================
        }
        if ( _application->_terminate ) return;

        // Check for orbit lengths errors
        if ( occ_histograms->orbit_error_counter > 0 || amp_histograms->orbit_error_counter > 0) {
                std::stringstream ss;
                ss << "Orbit length error counter non-zero: occ " << occ_histograms->orbit_error_counter << ", amp " << amp_histograms->orbit_error_counter;
                logAcqWarning( device, occ_histograms->tcds, ss.str() );
        }

        // Check normalisation
        if (occ_histograms->normalization != 16384) {
            std::stringstream ss;
            ss << "Normalisation counter is: occ " << occ_histograms->normalization << ", amp " << amp_histograms->normalization;
            if (occ_histograms->normalization == 16385) logAcqDebug( device, occ_histograms->tcds, ss.str() ); // this is quite frequent when CMS is running at local clock, so not interesting
            else logAcqWarning( device, occ_histograms->tcds, ss.str() );
        }

        // Print readout duration and collect statistics

        std::string msg("Readout occ histogram time: "); msg += std::to_string(occ_histograms->ro_time.count())  + " us";

        hw->_ro_time_occ.push_back(occ_histograms->ro_time.count());

        this->logAcqDebug(device,occ_histograms->tcds,msg);

        if ( amp_histograms != nullptr )
        {
        	hw->_ro_time_amp.push_back(amp_histograms->ro_time.count());

            msg = "Readout amp histogram time: "; msg += std::to_string(amp_histograms->ro_time.count())  + " us";

            logAcqDebug(device,amp_histograms->tcds,msg);
        }

        // Publishing occupancy histograms

        for( unsigned int channel =0; channel < utca::N_MAX_CH; channel++ ) {
            //
            toolbox::mem::Reference * rawrefocc = nullptr;

            if ( _cfg.getChannelId(device,channel) == 0 ) continue;

            try
            {

                rawrefocc = _application->_poolFactory->getFrame(_application->_memPool,interface::bril::bcm1futcaocchistT::maxsize());

                rawrefocc->setDataSize(interface::bril::bcm1futcaocchistT::maxsize());

                interface::bril::shared::DatumHead * rawheaderocc = (interface::bril::shared::DatumHead*)(rawrefocc->getDataLocation());

                uint16_t * dataocc = reinterpret_cast<uint16_t *>(rawheaderocc->payloadanchor);

                toolbox::TimeVal tv = toolbox::TimeVal::gettimeofday();

                rawheaderocc->setTime(occ_histograms->tcds.lhc_fill, occ_histograms->tcds.cms_run,occ_histograms->tcds.lumi_section,occ_histograms->tcds.lumi_nibble,tv.sec(),tv.millisec());

                rawheaderocc->setResource(interface::bril::shared::DataSource::BCM1F,_cfg.getAlgoId(device,channel),_cfg.getChannelId(device,channel),interface::bril::shared::StorageType::UINT16);

                rawheaderocc->setFrequency(interface::bril::shared::FrequencyType::NB4);

                rawheaderocc->setTotalsize(interface::bril::bcm1futcaocchistT::maxsize());

                if ( occ_histograms->data[channel].size() != interface::bril::bcm1futcaocchistT::n() ) {
                    //
                    std::ostringstream oss; oss << device << " : missmatch number of bins in occupancy histogram " <<  interface::bril::bcm1futcaocchistT::n();

                    oss << " (topic) / " << occ_histograms->data[channel].size() << " (acquired)"; rawrefocc->release();

                    XCEPT_RAISE(bril::bcm1futcasource::exception::ubcmException, oss.str());
                }

                std::copy( occ_histograms->data[channel].begin(), occ_histograms->data[channel].begin()+interface::bril::bcm1futcaocchistT::n(),dataocc);

                // ============================================================================
                //
                // Data for Highcharts plots in Hyperdaq
                //
                std::vector<float> V(interface::bril::bcm1futcaocchistT::n());

				for ( unsigned int iV=0; iV < interface::bril::bcm1futcaocchistT::n(); iV++ )
				{
					V[iV] = static_cast<float>(dataocc[iV]);
				}
				{
					std::lock_guard<std::mutex> lock(_application->_mutex);

					hw->_occupancy_data[_cfg.getPlotLegend(device,channel)] = std::move(V);

					if(hw->_occupancy_chart) hw->_occupancy_chart->newDataReady();
                }
                // ============================================================================

                std::set<std::string> bus = _cfg.getListOfPublicationBuses(interface::bril::bcm1futcaocchistT::topicname());

                for (std::set<std::string>::const_iterator it = bus.begin(); it != bus.end(); ++it) {
                    //
                    toolbox::mem::Reference * rawref = nullptr;

                    // clone the frame to publish in more than one eventing bus

                    if ( std::next(it,1) != bus.end() )
                    {
                        try
                        {
                            rawref = _application->_poolFactory->getFrame(_application->_memPool,rawrefocc->getDataSize());

                            memcpy(rawref->getDataLocation(),rawrefocc->getDataLocation(),rawrefocc->getDataSize());
                        }
                        catch(toolbox::mem::exception::Exception &ex)
                        {
                          std::string msg("Failed to allocate an extra memory frame to publish occupancy histogram into the bus: ");

                          msg += *it; logAcqError(_cfg.getChannelId(device,channel),occ_histograms->tcds,msg); continue;
                        }
                    }
                    else
                    {
                        rawref = rawrefocc; rawrefocc = nullptr;
                    }

                    xdata::Properties plistocc;

                    plistocc.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);

                    std::string payloaddict = interface::bril::bcm1futcaocchistT::payloaddict();

                    plistocc.setProperty("PAYLOAD_DICT",payloaddict);

                    if ( _application->_terminate )
                    {
                        if ( rawrefocc ) rawrefocc->release();

                        if ( rawref ) rawref->release();

                        return;
                    }
                    std::lock_guard<std::mutex> lock(_application->_mutex);



                    if ( !_cfg.isCommissioningMode() )
                    {
                        if ( !_application->_do_store ) plistocc.setProperty("NOSTORE","1");
                    }
                    try
                    {
                        _application->getEventingBus(*it).publish(interface::bril::bcm1futcaocchistT::topicname(),rawref,plistocc);
                    }
                    catch(std::exception & ex)
                    {
                        std::string msg("Failed to publish occupancy histogram into the bus: ");
			msg += *it;

                        logAcqError(_cfg.getChannelId(device,channel),occ_histograms->tcds,msg);

                        if ( rawref ) {
			  rawref->release();
			}
			continue;
                    }
                }
            }
            catch(toolbox::mem::exception::Exception &ex)
            {
              std::string msg("Failed to allocate memory frame to publish occupancy histogram");

              logAcqError(_cfg.getChannelId(device,channel),occ_histograms->tcds,msg);

              if ( rawrefocc ) {
		rawrefocc->release();
	      }
	      continue;
            }
        }

        if ( _application->_terminate ) return;

        // Publishing amplitude histograms

        if ( amp_histograms == nullptr )
        {
            logAcqWarning(device,_application->_tcds,"Failed reading amplitude histograms.");
        }
        else
        {
            for( unsigned int channel =0; channel < utca::N_MAX_CH; channel++ ) {
                //
                toolbox::mem::Reference * rawrefamp = nullptr;

                if ( _cfg.getChannelId(device,channel) == 0 ) continue;

                try
                {
                    {
                        std::lock_guard<std::mutex> lock(_application->_mutex);

                        rawrefamp = _application->_poolFactory->getFrame(_application->_memPool,interface::bril::bcm1futcaamphistT::maxsize());
                    }
                    rawrefamp->setDataSize(interface::bril::bcm1futcaamphistT::maxsize());

                    interface::bril::shared::DatumHead * rawheaderamp = (interface::bril::shared::DatumHead*)(rawrefamp->getDataLocation());

                    uint32_t * dataamp = reinterpret_cast<uint32_t *>(rawheaderamp->payloadanchor);

                    toolbox::TimeVal tv = toolbox::TimeVal::gettimeofday();

                    rawheaderamp->setTime(amp_histograms->tcds.lhc_fill, amp_histograms->tcds.cms_run,amp_histograms->tcds.lumi_section,amp_histograms->tcds.lumi_nibble,tv.sec(),tv.millisec());

                    rawheaderamp->setResource(interface::bril::shared::DataSource::BCM1F,_cfg.getAlgoId(device,channel),_cfg.getChannelId(device,channel),interface::bril::shared::StorageType::UINT32);

                    rawheaderamp->setFrequency(interface::bril::shared::FrequencyType::NB4);

                    rawheaderamp->setTotalsize(interface::bril::bcm1futcaamphistT::maxsize());

                    if ( amp_histograms->data[channel].size() < interface::bril::bcm1futcaamphistT::n() ) {
                        //
                        std::ostringstream oss; oss << device << " : fewer number of bins in amplitude histogram " <<  interface::bril::bcm1futcaamphistT::n();

                        oss << " (topic) / " << amp_histograms->data[channel].size() << " (acquired)"; rawrefamp->release();

                        logAcqError(_cfg.getChannelId(device,channel),amp_histograms->tcds,oss.str());

                        continue;
                    }
                    std::copy( amp_histograms->data[channel].begin(), amp_histograms->data[channel].begin() + interface::bril::bcm1futcaamphistT::n(), dataamp);

                    // ============================================================================
                    //
                    // Data for Highcharts plots in Hyperdaq
                    //
                    std::vector<float> V(interface::bril::bcm1futcaamphistT::n());

                    for ( unsigned int iV=0; iV < interface::bril::bcm1futcaamphistT::n(); iV++ )
                    {
                    	V[iV] = static_cast<float>(dataamp[iV]);
                    }
    				{
    					std::lock_guard<std::mutex> lock(_application->_mutex);

                        hw->_amplitude_data[_cfg.getPlotLegend(device,channel)] = std::move(V);

                        if(hw->_amplitude_chart) hw->_amplitude_chart->newDataReady();
                    }
                    // ============================================================================


                    std::set<std::string> bus = _cfg.getListOfPublicationBuses(interface::bril::bcm1futcaamphistT::topicname());

                    for (std::set<std::string>::const_iterator it = bus.begin(); it != bus.end(); ++it) {
                        //
                        toolbox::mem::Reference * rawref = nullptr;

                        // clone the frame to publish in more than one eventing bus

                        if ( std::next(it,1) != bus.end() )
                        {
                            try
                            {
                                rawref = _application->_poolFactory->getFrame(_application->_memPool,rawrefamp->getDataSize());

                                memcpy(rawref->getDataLocation(),rawrefamp->getDataLocation(),rawrefamp->getDataSize());
                            }
                            catch(toolbox::mem::exception::Exception &ex)
                            {
                              std::string msg("Failed to allocate an extra memory frame to publish amplitude histogram into the bus: ");

                              msg += *it; logAcqError(_cfg.getChannelId(device,channel),amp_histograms->tcds,msg); continue;
                            }
                        }
                        else
                        {
                            rawref = rawrefamp; rawrefamp = nullptr;
                        }

                        xdata::Properties plistamp;

                        plistamp.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);

                        std::string payloaddict = interface::bril::bcm1futcaamphistT::payloaddict();

                        plistamp.setProperty("PAYLOAD_DICT",payloaddict);

                        if ( _application->_terminate )
                        {
                             if ( rawrefamp ) rawrefamp->release();

                             if ( rawref ) rawrefamp->release();

                            return;
                        }
                        std::lock_guard<std::mutex> lock(_application->_mutex);



                        if ( !_cfg.isCommissioningMode() )
                        {
                            if ( !_application->_do_store ) plistamp.setProperty("NOSTORE","1");
                        }
                        try
                        {
                            _application->getEventingBus(*it).publish(interface::bril::bcm1futcaamphistT::topicname(),rawref,plistamp);
                        }
                        catch(std::exception & ex)
                        {
			  std::string msg("Failed to publish amplitude histogram into the bus: ");
			  msg += *it;

			  logAcqError(_cfg.getChannelId(device,channel),amp_histograms->tcds,msg);

			  if ( rawref ) {
			    rawref->release();
			  }
			  continue;
                        }
                    }
                }
                catch(toolbox::mem::exception::Exception &ex)
                {
                  std::string msg("Failed to allocate memory frame to publish amplitude histogram");

                  logAcqError(_cfg.getChannelId(device,channel),amp_histograms->tcds,msg);

                  if ( rawrefamp ) {
		    rawrefamp->release();
		  }
		  continue;
                }
            }
        }
        // Update the last publication TCDS info (only if published )
        // =================================================================================================
        {
            std::lock_guard<std::mutex> lock(_application->_mutex); hw->setTcdsHeader(occ_histograms->tcds);
        }
        // =================================================================================================
        //
    } catch( std::exception & e) {
        //
        std::lock_guard<std::mutex> lock(_application->_mutex); 

        // _ubcm[device]->setReady(false);  // Re-enable when the hardware reset is done

        std::string msg(" Calling read_occupancy failed. "); 

        msg += device + " : " + e.what(); boost::trim_right(msg);

        XCEPT_RAISE(bril::bcm1futcasource::exception::ubcmException, msg);
    }
    return;
}


void bril::bcm1futcasource::BackEndAcq::logAcqError(unsigned int channelid, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const {
    /**
    */
    std::ostringstream oss;

    oss << "ID # " << channelid << " : FILL " << tcds.lhc_fill << " RUN " << tcds.cms_run << " LS " << tcds.lumi_section;

    oss << " NB " << tcds.lumi_nibble;  oss << " skipped.";

    LOG4CPLUS_ERROR(_application->getApplicationLogger(),oss.str()); LOG4CPLUS_ERROR(_application->getApplicationLogger(),msg);
}

void bril::bcm1futcasource::BackEndAcq::logAcqError(const std::string &device, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const {
    /**
    */
    std::ostringstream oss;

    oss << device << " : FILL " << tcds.lhc_fill << " RUN " << tcds.cms_run << " LS " << tcds.lumi_section;

    oss << " NB " << tcds.lumi_nibble;  oss << " skipped.";

    LOG4CPLUS_ERROR(_application->getApplicationLogger(),oss.str()); LOG4CPLUS_ERROR(_application->getApplicationLogger(),msg);
}

void bril::bcm1futcasource::BackEndAcq::logAcqWarning(const std::string &device, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const {
    /**
    */
    std::ostringstream oss;

    oss << device << " : FILL " << tcds.lhc_fill << " RUN " << tcds.cms_run << " LS " << tcds.lumi_section;

    oss << " NB " << tcds.lumi_nibble;  oss << " : " << msg;

    LOG4CPLUS_WARN(_application->getApplicationLogger(),oss.str());
}

void bril::bcm1futcasource::BackEndAcq::logAcqDebug(const std::string &device, const utca::GLIBv3::tcds_info_t &tcds, const std::string & msg) const {
    /**
    */
    std::ostringstream oss;

    oss << device << " : FILL " << tcds.lhc_fill << " RUN " << tcds.cms_run << " LS " << tcds.lumi_section;

    oss << " NB " << tcds.lumi_nibble;  oss << " : " << msg;

    LOG4CPLUS_DEBUG(_application->getApplicationLogger(),oss.str());
}


unsigned int bril::bcm1futcasource::BackEndAcq::getNumberOfReadyDevices () const
{
    /**
    */
    unsigned int n = 0;

    for ( auto u : _ubcm )if ( u.second->isReady() ) n++;

    return n;
}

bool bril::bcm1futcasource::BackEndAcq::rawDataAcquisitionPermitted() const {
    /**
    */
    utca::GLIBv3::tcds_info_t  tcds1 = _ubcm.begin()->second->getTcdsHeader(); 

    std::lock_guard<std::mutex> lock(_application->_mutex);

    logAcqDebug(_ubcm.begin()->first,tcds1,"tcds1");

    for ( auto u : _ubcm ){
        //
        utca::GLIBv3::tcds_info_t  tcds2 = u.second->getTcdsHeader();

        logAcqDebug(_ubcm.begin()->first,tcds2,"tcds2");

        if ( tcds1.lumi_section != tcds2.lumi_section || tcds1.lumi_nibble != tcds2.lumi_nibble ) return false;
    }
    return true;
}
    

bool bril::bcm1futcasource::BackEndAcq::reboot(const std::string & device) {

	return true;
}


void bril::bcm1futcasource::BackEndAcq::updateInfo() {
	//
	if ( _application->_terminate ) return;

	// Readout hardware status and update internal Info map

    std::vector<std::string> devices = _cfg.getListOfDevices();

    for ( std::vector<std::string>::const_iterator it = devices.begin(); it != devices.end(); ++it ) {
    	//
		// Taking timestamp

		std::time_t t = std::time(nullptr); std::string ascitime = std::ctime(&t); ascitime.pop_back();

    	std::shared_ptr<UbcmBcm1fSource> ubcm = getUBCM(*it);

    	if ( !ubcm )
    	{
    		std::string msg = *it; msg += " : devices is not in the collection.";

    		LOG4CPLUS_ERROR(_application->getApplicationLogger(), msg);

    		return;
    	}

    	if ( _application->_terminate ) return;

    	std::lock_guard<std::mutex> lock(_application->_mutex);

    	ubcm->_info.clear();

    	if ( ! ubcm->isReady() )
    	{
    		ubcm->_info.insert(std::make_pair("Failed",ubcm->getLastExceptionMessage()));

    		continue;
    	}
    	ubcm->_info.insert(std::make_pair("Last readout time",ascitime));

    	try{
    		for( auto topic : ubcm->info() ) ubcm->_info.insert(topic);

    		ubcm->_info.insert(std::make_pair("Number of duplicated histograms", std::to_string(ubcm->_numberOfDiscardedHistograms)));

    		ubcm->_info.insert(std::make_pair("Number of missed histograms", std::to_string(ubcm->_numberOfMissedHistograms)));

    		ubcm->_info.insert(std::make_pair("Number of mismatched orbit counters", std::to_string(ubcm->_numberOfMismatchedOrbitCounters)));

    		std::string s = "amp # "; s += std::to_string(ubcm->_numberOfReadOccHistograms);

    		s += " occ # " + std::to_string(ubcm->_numberOfReadAmpHistograms);

    		ubcm->_info.insert(std::make_pair("Number of read histograms",s));

			double average = std::accumulate(ubcm->_ro_time_amp.begin(), ubcm->_ro_time_amp.end(), 0.0) / ubcm->_ro_time_amp.size();

			double accum = 0.0;

			std::for_each(ubcm->_ro_time_amp.begin(), ubcm->_ro_time_amp.end(), [&](const double d) {
				//
				accum  += (d - average) * (d - average);
			});

			double stdev = std::sqrt(accum / (ubcm->_ro_time_amp.size()-1));

			std::string ro_time=std::to_string(average); ro_time += " +/- " + std::to_string(stdev) + " us";

    		ubcm->_info.insert(std::make_pair("Amplitude histogram readout time", ro_time));

			average = std::accumulate(ubcm->_ro_time_occ.begin(), ubcm->_ro_time_occ.end(), 0.0) / ubcm->_ro_time_occ.size();

			accum = 0.0;

			std::for_each(ubcm->_ro_time_occ.begin(), ubcm->_ro_time_occ.end(), [&](const double d) {
				//
				accum  += (d - average) * (d - average);
			});

			stdev = std::sqrt(accum / (ubcm->_ro_time_occ.size()-1));

			ro_time=std::to_string(average); ro_time += " +/- " + std::to_string(stdev) + " us";

    		ubcm->_info.insert(std::make_pair("Occupancy histogram readout time", ro_time));

    	}
    	catch(std::exception & ex) {
    		//
    		std::string msg = *it; msg += " : "; msg += ex.what();

    		LOG4CPLUS_ERROR(_application->getApplicationLogger(), msg);
    	}
    }
}

void bril::bcm1futcasource::BackEndAcq::initWebPlotting(const std::string & url)
{
	std::vector<std::string> devices = _cfg.getListOfDevices();

	const char * basic_options_amp = "type: 'column', zoomType: 'x', animation: false";

	const char * plot_options_amp  = "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'BCM1F uTCA Amplitude Histograms' }, tooltip: { enable : false }";

	const char * basic_options_occ = "type: 'column', zoomType: 'x', animation: false";

	const char * plot_options_occ  = "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'BCM1F uTCA Occupancy Histograms' }, tooltip: { enable : false }";

	const char * basic_options_raw = "type: 'column', zoomType: 'x', animation: false";

	const char * plot_options_raw  = "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'BCM1F uTCA Raw Orbit Samples' }, tooltip: { enable : false }";

	for ( std::vector<std::string>::const_iterator it = devices.begin(); it != devices.end(); ++it ) {

		if ( _application->_terminate ) return;

    	std::shared_ptr<UbcmBcm1fSource> ubcm = getUBCM(*it);

    	if ( ubcm->_amplitude_chart == nullptr )
    	{
    		std::lock_guard<std::mutex> lock(_application->_mutex);

    		webutils::WebChart::property_map pm;

    		pm[_cfg.getPlotLegend(*it,0)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,1)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,2)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,3)] = webutils::WebChart::series_properties();

    		std::string chartid = *it; chartid.erase(std::remove(chartid.begin(), chartid.end(), '.'), chartid.end());

    		chartid += "_amp";

    		ubcm->_amplitude_chart = std::make_shared<webutils::WebChart>(chartid,url,basic_options_amp,plot_options_amp,pm);
    	}
    	if ( ubcm->_occupancy_chart == nullptr )
    	{
    		std::lock_guard<std::mutex> lock(_application->_mutex);

    		webutils::WebChart::property_map pm;

    		pm[_cfg.getPlotLegend(*it,0)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,1)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,2)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,3)] = webutils::WebChart::series_properties();

    		std::string chartid = *it; chartid.erase(std::remove(chartid.begin(), chartid.end(), '.'), chartid.end());

    		chartid += "_occ";

    		ubcm->_occupancy_chart = std::make_shared<webutils::WebChart>(chartid,url,basic_options_occ,plot_options_occ,pm);
    	}
    	if ( ubcm->_raw_orbit_chart == nullptr )
    	{
    		std::lock_guard<std::mutex> lock(_application->_mutex);

    		webutils::WebChart::property_map pm;

    		pm[_cfg.getPlotLegend(*it,0)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,1)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,2)] = webutils::WebChart::series_properties();
    		pm[_cfg.getPlotLegend(*it,3)] = webutils::WebChart::series_properties();

    		std::string chartid = *it; chartid.erase(std::remove(chartid.begin(), chartid.end(), '.'), chartid.end());

    		chartid += "_raw";

    		ubcm->_raw_orbit_chart = std::make_shared<webutils::WebChart>(chartid,url,basic_options_raw,plot_options_raw,pm);
    	}
	}
}


void bril::bcm1futcasource::UbcmBcm1fSource::writeDataAsJSON(const std::string & device, const cgicc::Cgicc& query, xgi::Output* out)
{
	std::string chartid;

	chartid=device; chartid.erase(std::remove(chartid.begin(), chartid.end(), '.'), chartid.end());

	chartid += "_amp"; _amplitude_chart->writeDataForQuery(query,out,_amplitude_data);

	chartid=device; chartid.erase(std::remove(chartid.begin(), chartid.end(), '.'), chartid.end());

	chartid += "_occ"; _occupancy_chart->writeDataForQuery(query,out,_occupancy_data);

	chartid=device; chartid.erase(std::remove(chartid.begin(), chartid.end(), '.'), chartid.end());

	chartid += "_raw"; _raw_orbit_chart->writeDataForQuery(query,out,_raw_orbit_data);
}
