#include "bril/dipprocessor/Magnet.h"

bril::dip::Magnet::Magnet(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
  try{
    //if need to read configuraiton, listen also to defaultparameters
    getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
    getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  }catch( xcept::Exception & e){
    LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
  } 
  m_dipRoot="dip/CMS/";
  m_current=0.0;
}

bril::dip::Magnet::~Magnet(){
}

void bril::dip::Magnet::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    xdata::String* dipRoot = 
    dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
    if(dipRoot)m_dipRoot= dipRoot->value_;
    m_dipsubs.insert(std::make_pair("dip/CMS/MCS/Current",(DipSubscription*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot+"BRIL/Magnet",(DipPublication*)0 ));
  }
  if( e.type()== "ItemChangedEvent" ){
    std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
    if(item =="nbnum" ){
      std::stringstream ss;
      ss<<"Magnet publish to dip"<<std::endl;
      xdata::Serializable* currentrunnum = getOwnerApplication()->getApplicationInfoSpace()->find("runnum");
      xdata::Serializable* currentlsnum = getOwnerApplication()->getApplicationInfoSpace()->find("lsnum");
      xdata::Serializable* currentnbnum = getOwnerApplication()->getApplicationInfoSpace()->find("nbnum");
      
      if(currentrunnum && currentlsnum && currentnbnum){
	ss<<" run "<<dynamic_cast<xdata::UnsignedInteger*>(currentrunnum)->value_ ;
	ss<<" ls "<<dynamic_cast<xdata::UnsignedInteger*>(currentlsnum)->value_ ;
	ss<<" nb "<<dynamic_cast<xdata::UnsignedInteger*>(currentnbnum)->value_ ;
      }
     
      LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
      publish_to_dip();
    }
  }
}

void bril::dip::Magnet::actionPerformed(toolbox::Event& e){
}


void bril::dip::Magnet::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"Magnet::handleMessage from "+subname);
  if(message.size()==0) return;
  if(subname=="dip/CMS/MCS/Current"){
     m_current = message.extractFloat("__DIP_DEFAULT__");
   }
}

void bril::dip::Magnet::publish_to_dip(){
  DipData* dipdata = m_dip->createDipData();
  std::string diptopicname(m_dipRoot+"BRIL/Magnet");
  DipTimestamp t;
  float field=2.084287e-4 * m_current +1.704418E-2;
  dipdata -> insert(m_current,"Amps");
  dipdata -> insert(field,"Tesla");
  dipdata -> insert("Field data valid in range 1 to 4T","Message");
  m_dippubs[diptopicname]->send(*dipdata,t);
  delete dipdata;
}
