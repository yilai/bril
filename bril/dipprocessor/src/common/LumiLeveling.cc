#include "xcept/tools.h"
#include "xgi/framework/Method.h"
#include "toolbox/net/UUID.h"
#include "xdata/InfoSpaceFactory.h"
#include "b2in/nub/Method.h"
#include "dip/DipData.h"
#include "dip/Dip.h"

// Webpage
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "bril/dipprocessor/LumiLeveling.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "interface/bril/shared/LUMITopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"

#include <cmath>
#include <cstdio>

XDAQ_INSTANTIATOR_IMPL (bril::dip::LumiLeveling)

namespace bril {
namespace dip {

LumiLeveling::LumiLeveling(xdaq::ApplicationStub* s) : xdaq::Application(s)
                                                     , xgi::framework::UIManager(this)
                                                     , eventing::api::Member(this)
                                                     , m_applock(toolbox::BSem::FULL) {
    
    xgi::framework::deferredbind(this,this,&bril::dip::LumiLeveling::Default, "Default");
    xgi::framework::deferredbind(this,this,&bril::dip::LumiLeveling::setParameter, "setParameter");
    xgi::framework::deferredbind(this,this,&bril::dip::LumiLeveling::logOut, "logOut");

    b2in::nub::bind(this, &bril::dip::LumiLeveling::onMessage);
    m_bus.fromString("brildata");
    m_dipRoot.fromString("dip/CMSTEST/");
    
    m_mode="UNKNOWN";
    m_source="UNKNOWN";
    m_targetLumiQuantity = "UNKNOWN";
    m_last_user = "UNKNOWN";

    m_lumi = -1; m_last_lumi_time_sec = 0; m_run=0; m_lastrun=0; m_levelStepSigma=0; m_targetLumi=0;

    m_isfirsttcds = true; m_enable = false; m_AbortLeveling = false; m_StatusFlag = false;

    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRoot);
    getApplicationInfoSpace()->fireItemAvailable("targetLumiQuantity",&M_targetLumiQuantity);
    getApplicationInfoSpace()->fireItemAvailable("targetLumi",&M_targetLumi);
    getApplicationInfoSpace()->fireItemAvailable("lumiStep",&M_levelStepSigma);
    getApplicationInfoSpace()->fireItemAvailable("enableLeveling",&M_enable);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    m_wl_publishing_todip = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_dippublish","waiting");
    m_as_publishing_todip = toolbox::task::bind(this,&bril::dip::LumiLeveling::publishing_todip,"dippublish");

}

LumiLeveling::~LumiLeveling(){}

void LumiLeveling::Default(xgi::Input * in, xgi::Output * out) {

    // Start building HTML page
    try {
        cgicc::Cgicc cgi(in);

        const cgicc::CgiEnvironment& env = cgi.getEnvironment();
        std::string remoteuser = env.getRemoteUser();
        std::string serversw = env.getServerSoftware();
        std::string clientsw = env.getUserAgent();
        std::string authtype = env.getAuthType();

        std::vector<std::string> allowed_user{"bril:run4cms", "gnegro:run4cms", "amassiro:run4cms", "masettig:run4cms"};

        if(remoteuser.empty() || !(std::find(std::begin(allowed_user), std::end(allowed_user), remoteuser) != std::end(allowed_user)))
        {
            out->getHTTPResponseHeader().getStatusCode(401);
            out->getHTTPResponseHeader().getReasonPhrase("Unauthorized");
            out->getHTTPResponseHeader().addHeader("WWW-Authenticate", "Basic realm=\"cgicc\"");

            *out << cgicc::HTMLDoctype( cgicc::HTMLDoctype::eStrict) << std::endl;
            *out << cgicc::html().set("lang", "EN").set("dir", "LTR") << std::endl;
            *out << cgicc::head() << std::endl;

            *out << cgicc::title("401 Authorization Required")  << std::endl;
            *out << cgicc::head() << std::endl;
            *out << cgicc::body() << std::endl;

            *out << cgicc::h1("401 Authorization Required") << std::endl;
            *out << cgicc::p() << "This server could not verify that you are "
                << "authorized to access the document requested. Either you "
                << "supplied the wrong credentials (e.g., bad password), or "
                << "your browser doesn't understand how to supply the "
                << "credentials required." << cgicc::p();
            *out << cgicc::hr() << std::endl;
            *out << cgicc::address() << "GNU cgicc \"server\" version " << cgi.getVersion()
            <<  cgicc::address() << std::endl;
            return;
        }

        // Save login
        std::string delimiter = ":";
        m_user = remoteuser.substr(0, remoteuser.find(delimiter));

    } catch (const std::exception & e) {
        XCEPT_RAISE(xgi::exception::Exception, e.what());
    }

    // TEST
    out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("CMS Leveling Tool") << std::endl;

    // TODO: develop proper application page for improved monitoring and data quility checks
    //*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::table().set("class", "xdaq-table-vertical").set("style", "display: inline-block") << std::endl;
    *out << cgicc::tbody() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("TargetLumiQuantity");
    *out << cgicc::td();
    *out << m_targetLumiQuantity;
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Lumi source");
    *out << cgicc::td();
    *out << m_source;
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Target Pileup");
    *out << cgicc::td();
    *out << M_targetLumi;
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Current Pileup");
    *out << cgicc::td();
    *out << m_lumi;
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Last value provided by");
    *out << cgicc::td();
    *out << m_last_user.c_str();
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Automatic leveling enabled");
    *out << cgicc::td();
    if (m_enable) *out << cgicc::p().set( "style", "color: green")  << "True" << cgicc::p();
                else *out << cgicc::p().set( "style", "color: red")  << "False" << cgicc::p();

    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tbody() << std::endl;
    *out << cgicc::table();
    *out << cgicc::br() << std::endl;

	// This method can be invoked using Linux 'wget' command

	std::string method = toolbox::toString("/%s/setParameter",getApplicationDescriptor()->getURN().c_str());
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
    *out << cgicc::legend("Set the value of target Pileup (integer number 0 > x < 100)") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","GET").set("action", method) << std::endl;
    // Set the floating-point number format to fixed-point notation
    *out << std::fixed;
    // Display the input field with the fixed-point number
    *out << cgicc::input().set("type","text").set("name","value").set("value", std::to_string(M_targetLumi.value_)) << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Apply")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset();

    std::string reset = toolbox::toString("/%s/logOut",getApplicationDescriptor()->getURN().c_str());
	
	*out << cgicc::form().set("method","GET").set("action", reset) << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Logout")  << std::endl;
	*out << cgicc::form() << std::endl;
}

void LumiLeveling::setParameter(xgi::Input * in, xgi::Output * out ) 
{	
	try
        {
            cgicc::Cgicc cgi(in);
		
            // retrieve value and update exported variable
            float value = cgi["value"]->getIntegerValue();
            
            // Sanity check
            if (value > 0 && value < 100) {
                M_targetLumi = value;
                m_last_user = m_user;
            }

            // re-display form page 
            this->Default(in,out);

        }
        catch (const std::exception & e)
        {
            XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void LumiLeveling::logOut(xgi::Input * in, xgi::Output * out ) 
{	
    try {
        cgicc::Cgicc cgi(in);

        out->getHTTPResponseHeader().getStatusCode(401);
        out->getHTTPResponseHeader().getReasonPhrase("Unauthorized");
        out->getHTTPResponseHeader().addHeader("WWW-Authenticate", "Basic realm=\"cgicc\"");
        
    } catch (const std::exception & e) {
        XCEPT_RAISE(xgi::exception::Exception, e.what());
    }
}

void LumiLeveling::actionPerformed(xdata::Event& e){
    if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        try{
            LOG4CPLUS_DEBUG(getApplicationLogger(),"trying to connect to topics");

            this->getEventingBus(m_bus.value_).subscribe(interface::bril::beamT::topicname());
            this->getEventingBus(m_bus.value_).subscribe(interface::bril::shared::bestlumiT::topicname());
            this->getEventingBus(m_bus.value_).subscribe(interface::bril::tcdsT::topicname());

        }catch(eventing::api::exception::Exception& e){
            std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
            XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);
        }
        toolbox::net::UUID uuid;
        std::string processuuid = uuid.toString();

        m_dip = Dip::create(( std::string("brilLumiTopicPublisher_")+processuuid).c_str() );
        m_puberrorhandler = new PubErrorHandler;
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"LHC/LumiLeveling",(DipPublication*)0 ));  //Luminosity
        
        createDipPublication();
        
        m_wl_publishing_todip->activate();

        m_enable=M_enable;
        m_targetLumi=M_targetLumi;
        m_levelStepSigma=M_levelStepSigma;
        m_targetLumiQuantity=M_targetLumiQuantity;
    }
}

void LumiLeveling::actionPerformed(toolbox::Event& e){
    delete m_dip;
    if(m_puberrorhandler){
        delete m_puberrorhandler; m_puberrorhandler = 0;
    }
}

void LumiLeveling::createDipPublication(){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"createDipPublication: ");
    for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
        DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
        if(!p){
            LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
            m_dippubs.erase(it); // remove from registry
        }else{
            it->second=p;
        }
    }
}

void LumiLeveling::publishToDip(){
    DipTimestamp dip_timestamp_now;
    long now = dip_timestamp_now.getAsMillis()/1000;
    std::stringstream ss;
    ss<<"Entering publishToDip.  Lumi value: "<< m_lumi << ", time: " << m_last_lumi_time_sec;
    bool lumiStatus = true;
    
    if(m_lumi < 10){
        ss << ". Lumi value bellow 1000 => StatusFlag=false";
        lumiStatus=false;
    }

    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    if (now - m_last_lumi_time_sec > 60) {
        ss.str(""); ss.clear();
        ss<<"Lumi value is too old => StatusFlag=false. Data time: " << m_last_lumi_time_sec << ", now: " << now;
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
        lumiStatus=false;
    }

    DipData* dipdata = m_dip->createDipData();
    std::string diptopicname(m_dipRoot.value_+"LHC/LumiLeveling");
    
    dipdata -> insert(m_targetLumiQuantity,"TargetLumiQuantity");
    dipdata -> insert(m_lumi,"CurrentLumi");
    dipdata -> insert(m_targetLumi,"TargetLumi");
    dipdata -> insert(m_levelStepSigma,"LevelStepSigma");
    dipdata -> insert(m_enable,"Enable");
    dipdata -> insert(lumiStatus,"StatusFlag");

    m_lumi=-1;

    DipTimestamp t;
    m_dippubs[diptopicname]->send(*dipdata,t);
    delete dipdata;

    //reset variables already sent
    m_applock.take();
    m_source ="UNKNOWN";
    m_lumi = -1;
    m_applock.give();
}


void LumiLeveling::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        std::string data_version  = plist.getProperty("DATA_VERSION");
        if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
            std::string msg("Received brildaq message has no or wrong version, do not process.");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            ref->release(); ref=0;
            return;
        }

        if(ref!=0){
            interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
            std::string detpayload;

            std::cout << topic << std::endl;

            if (topic == interface::bril::tcdsT::topicname()){
                //Note: the time header of detector messages can NOT be trusted therefore should NOT be written into the common time infornation because for example HF nbnum, lsnum are guaranteed to be different from others. We use tcds's time as the common.
                m_applock.take();
                m_run = thead->runnum;
                m_ls = thead->lsnum;
                m_nb = thead->nbnum;
                m_ts = thead->timestampsec;
                m_tms = thead->timestampmsec;
                m_applock.give();
                if( m_run!=m_lastrun ){
                    m_isfirsttcds = true;
                    m_lastrun = m_run;
                } else {
                    m_isfirsttcds = false;
                }
                if( !m_isfirsttcds ){
                    //On start of process, incomplete set of messages are useless, do not process
                    m_wl_publishing_todip->submit(m_as_publishing_todip);
                }
            } else if(topic == interface::bril::shared::bestlumiT::topicname()){
                float _delivered = 0;
                float _pileup = 0;
                char _provider[50];
                interface::bril::shared::CompoundDataStreamer tc(interface::bril::shared::bestlumiT::payloaddict());
                tc.extract_field(&_delivered, "delivered",  thead->payloadanchor);
                tc.extract_field(&_pileup, "avgpu",  thead->payloadanchor);
                
                std::cout << "IN BESTLUMI TOPIC" << std::endl;

                if( isnan(_delivered) || isinf(_delivered) ){
                    LOG4CPLUS_ERROR(getApplicationLogger(),"onMessage: received delivered NaN or Inf from bestlumi");
                    _delivered = 0;
                }

                if( _delivered<1E-8 ){
                    _delivered = 0;
                }

                if( isnan(_pileup) || isinf(_pileup) ){
                    LOG4CPLUS_ERROR(getApplicationLogger(),"onMessage: received pileup NaN or Inf from bestlumi");
                    _pileup = 0;
                }

                m_applock.take();

                if (m_targetLumiQuantity == "LUMINOSITY") {
                    m_lumi = _delivered;
                } else if (m_targetLumiQuantity == "PILEUP") {
                    m_lumi = _pileup;//_pileup;
                }

                m_last_lumi_time_sec = thead->timestampsec;
                
                tc.extract_field(_provider, "provider",  thead->payloadanchor);
                m_source=std::string(_provider);
                m_applock.give();


            } else if(topic == interface::bril::beamT::topicname()){
                char machinemode[50];
                interface::bril::shared::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());
                tc.extract_field(machinemode, "status",  thead->payloadanchor);
                m_applock.take();
                m_mode=std::string(machinemode);
                m_applock.give();

            }else if(topic == interface::bril::tcdsT::topicname()){
                interface::bril::shared::CompoundDataStreamer tc(interface::bril::tcdsT::payloaddict());
                //submit workloops
                std::stringstream ss;
            }
        }
    }
    if(ref!=0){
        ref->release();
        ref=0;
    }
}

bool LumiLeveling::publishing_todip(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing_todip");
    usleep(400000);

    m_targetLumi=M_targetLumi;

    publishToDip();
    return false;
}

}
}