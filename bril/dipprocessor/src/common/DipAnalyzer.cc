#include "bril/dipprocessor/DipAnalyzer.h"
#include "bril/dipprocessor/DipErrorHandlers.h"

bril::dip::DipAnalyzer::DipAnalyzer(const std::string& name, unsigned int dipfrequency,const std::string& dipServiceName,xdaq::Application* owner):eventing::api::Member(owner),m_name(name),m_dipfrequency(dipfrequency){
  std::string dipsubscriber(dipServiceName);
  m_dip = Dip::create(dipsubscriber.c_str());
  m_servererrorhandler=new bril::dip::ServerErrorHandler;
  Dip::addDipDimErrorHandler(m_servererrorhandler);
  m_puberrorhandler = new PubErrorHandler;
}

void bril::dip::DipAnalyzer::connected(DipSubscription* dipsub){
  std::cout<<"connected to dip publication source "<<dipsub->getTopicName()<<std::endl;
}

void bril::dip::DipAnalyzer::disconnected(DipSubscription* dipsub,char *reason){
  std::cout<<"Publication source "<<dipsub->getTopicName()<<" unavailable"<<std::endl;
}

void bril::dip::DipAnalyzer::handleException(DipSubscription* dipsub, DipException& ex){
  std::cout<<"Publication source "<<dipsub->getTopicName()<<" exception "<<ex.what()<<std::endl;
}

void bril::dip::DipAnalyzer::subscribeToDip(){
  LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"subscribeToDip ");
  m_dip->setTimeout(3);
  for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
    DipSubscription* s = 0;
    s=m_dip->createDipSubscription(it->first.c_str(),this);
    if(!s){
      m_dipsubs.erase(it); // remove from registry
    }else{
      it->second=s;
    }
  }
}

void bril::dip::DipAnalyzer::createDipPublication(){
  for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
    DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
    if(!p){
      LOG4CPLUS_FATAL(getOwnerApplication()->getApplicationLogger(),"Failed to create dip topic "+it->first);
      m_dippubs.erase(it); // remove from registry
    }else{
      it->second=p;
    }
  }
}

bril::dip::DipAnalyzer::~DipAnalyzer(){
  for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
    m_dip->destroyDipSubscription(it->second);
    it->second = 0;
  }
  m_dipsubs.clear();
  for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
    m_dip->destroyDipPublication(it->second);
    it->second = 0;
  }
  m_dippubs.clear();
  delete m_dip;
  delete m_puberrorhandler;
  delete m_servererrorhandler;
}
