#include "bril/dipprocessor/VacSummary.h"

bril::dip::VacSummary::VacSummary(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
  try{
    //if need to read configuraiton, listen also to defaultparameters
    getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
    getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  }catch( xcept::Exception & e){
    LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
  } 
 
  m_dipRoot="dip/CMS/";
  // the list of vacuum gauges to monitor
  m_gaugeValues.insert(std::make_pair("VGPB_7_4L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGI_220_1L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_220_1L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGI_183_1L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_183_1L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_148_1L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_147_1L5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_147_1R5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_148_1R5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_183_1R5",0.0));
  m_gaugeValues.insert(std::make_pair("VGI_183_1R5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_220_1R5",0.0));
  m_gaugeValues.insert(std::make_pair("VGI_220_1R5",0.0));
  m_gaugeValues.insert(std::make_pair("VGPB_7_4R5",0.0));

  m_gaugePositions.insert(std::make_pair("VGPB_7_4L5",-58.0));
  m_gaugePositions.insert(std::make_pair("VGI_220_1L5",-22.0));
  m_gaugePositions.insert(std::make_pair("VGPB_220_1L5",-22.0));
  m_gaugePositions.insert(std::make_pair("VGI_183_1L5",-18.3));
  m_gaugePositions.insert(std::make_pair("VGPB_183_1L5",-18.3));
  m_gaugePositions.insert(std::make_pair("VGPB_148_1L5",-14.8));
  m_gaugePositions.insert(std::make_pair("VGPB_147_1L5",-14.7));
  m_gaugePositions.insert(std::make_pair("VGPB_147_1R5",14.7));
  m_gaugePositions.insert(std::make_pair("VGPB_148_1R5",14.8)); 
  m_gaugePositions.insert(std::make_pair("VGPB_183_1R5",18.3)); 
  m_gaugePositions.insert(std::make_pair("VGI_183_1R5",18.3));
  m_gaugePositions.insert(std::make_pair("VGPB_220_1R5",22.0));
  m_gaugePositions.insert(std::make_pair("VGI_220_1R5",22.0));
  m_gaugePositions.insert(std::make_pair("VGPB_7_4R5",58.0));
  
  m_dipHeader="dip/VAC/LHC/";
  m_dipTrailer="_X.PR";
}

bril::dip::VacSummary::~VacSummary(){
}

void bril::dip::VacSummary::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    xdata::String* dipRoot = 
    dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
    if(dipRoot)m_dipRoot= dipRoot->value_;
    std::map<std::string,float>::iterator iter;
    for (iter=m_gaugeValues.begin(); iter != m_gaugeValues.end(); iter++){
      m_dipsubs.insert(std::make_pair(m_dipHeader+iter->first+m_dipTrailer,(DipSubscription*)0 ));
      //std::cout<<"VACsummary subscribed to "<<m_dipHeader+iter->first+m_dipTrailer<<std::endl;
    }
    m_dippubs.insert(std::make_pair(m_dipRoot+"BRIL/VacSummary",(DipPublication*)0 ));
  }
  if( e.type()== "ItemChangedEvent" ){
    std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
    if(item =="nbnum" ){
      std::stringstream ss;
      ss<<"VacSummary publish to dip"<<std::endl;
      xdata::Serializable* currentrunnum = getOwnerApplication()->getApplicationInfoSpace()->find("runnum");
      xdata::Serializable* currentlsnum = getOwnerApplication()->getApplicationInfoSpace()->find("lsnum");
      xdata::Serializable* currentnbnum = getOwnerApplication()->getApplicationInfoSpace()->find("nbnum");
      
      if(currentrunnum && currentlsnum && currentnbnum){
	ss<<" run "<<dynamic_cast<xdata::UnsignedInteger*>(currentrunnum)->value_ ;
	ss<<" ls "<<dynamic_cast<xdata::UnsignedInteger*>(currentlsnum)->value_ ;
	ss<<" nb "<<dynamic_cast<xdata::UnsignedInteger*>(currentnbnum)->value_ ;
      }
     
      LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
      publish_to_dip();
    }
  }
}

void bril::dip::VacSummary::actionPerformed(toolbox::Event& e){
}


void bril::dip::VacSummary::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"VacSummary::handleMessage from "+subname);
  if(message.size()==0) return;
  std::map<std::string,float>::iterator iter;
  std::string dp;
  for (iter=m_gaugeValues.begin(); iter != m_gaugeValues.end(); iter++){
    dp=m_dipHeader+iter->first+m_dipTrailer;
    if(subname==dp){
      float vac=message.extractFloat("__DIP_DEFAULT__");
      iter->second=vac;
      //std::cout<<"Pressure for "<<iter->first<<" is "<<iter->second<<std::endl;
      break;
     }
   }
}

void bril::dip::VacSummary::publish_to_dip(){
  DipData* dipdata = m_dip->createDipData();
  std::string diptopicname(m_dipRoot+"BRIL/VacSummary");
  DipTimestamp t;
  const int nGauges=m_gaugeValues.size();
  float vacHisto[nGauges];
  int i=0;
  std::map<std::string,float>::iterator iter;
  for (iter=m_gaugeValues.begin(); iter != m_gaugeValues.end(); iter++){
    dipdata -> insert(iter->second,iter->first.c_str());
    vacHisto[i]=iter->second *1E12;
    i++;
  }
  dipdata -> insert(vacHisto,nGauges,"VacArray");
  m_dippubs[diptopicname]->send(*dipdata,t);
  //std::cout<<"VACHisto ";
  for (i=0;i<nGauges;i++){
    //std::cout<<vacHisto[i]<<", ";
  }
  //std::cout<<std::endl;
  delete dipdata;


}
