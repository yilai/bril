#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"
#include "toolbox/BSem.h"

#include "b2in/nub/Method.h"
#include "bril/dipprocessor/BHMdipprocessor.h"
#include "bril/dipprocessor/BHMDipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"

#include "interface/bril/TCDSTopics.hh"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/BHMTopics.hh"

XDAQ_INSTANTIATOR_IMPL( bril::bhmdipprocessor::BHMdipprocessor )

namespace bril
 {

  namespace bhmdipprocessor
   {

     BHMdipprocessor::BHMdipprocessor( xdaq::ApplicationStub* stub ):
      xdaq::Application( stub ),
      xgi::framework::UIManager( this ),
      eventing::api::Member( this ),
      m_app_lock( toolbox::BSem::FULL ),
      m_app_infospace( getApplicationInfoSpace() ),
      m_service_name( "briltestdipbridge" ),
      m_dip_root( "dip/CMS/" ),
      m_config_key( "none" )
      {
       xgi::framework::deferredbind( this, this, &BHMdipprocessor::Default, "Default" );
       b2in::nub::bind( this, &BHMdipprocessor::onMessage );
       try
        {
         m_app_infospace->fireItemAvailable( "dipServiceName", &m_service_name );
         m_app_infospace->fireItemAvailable( "dipRoot", &m_dip_root );
         m_app_infospace->fireItemAvailable( "configKey", &m_config_key );
         m_app_infospace->addListener( this, "urn:xdaq-event:setDefaultValues" );
        }
       catch ( xdata::exception::Exception& e )
        {
         LOG4CPLUS_FATAL( getApplicationLogger(), "Failed to setup appInfoSpace " << stdformat_exception_history( e ) ); 
         XCEPT_RETHROW( bril::dip::exception::Exception, "Failed to setup appInfoSpace ", e );
        }
      }

    BHMdipprocessor::~BHMdipprocessor()
     {
      m_dip->destroyDipSubscription( m_scalers[0] );
      m_dip->destroyDipSubscription( m_scalers[1] );
      m_dip->destroyDipSubscription( m_scalers[2] );
      m_dip->destroyDipSubscription( m_beam_1_intensity );
      m_dip->destroyDipSubscription( m_beam_2_intensity );
      m_dip->destroyDipPublication( m_output );
      Dip::shutdown();
     }

    void BHMdipprocessor::Default( xgi::Input* in, xgi::Output* out )
     {
      *out << "BHM VME Scaler Data Processor." << cgicc::br();
      *out << getApplicationContext()->getContextDescriptor()->getURL() << cgicc::br();
      *out << getApplicationDescriptor()->getURN() << cgicc::br();
      *out << "Config key: " << m_config_key.toString() << cgicc::br() << cgicc::br();

      *out << "Current background rates: " << cgicc::br();
      *out << cgicc::table().set( "class", "xdaq-table-vertical" );
      *out << cgicc::tbody();

      char buf[100];
      sprintf( buf, "%f Hz/cm^2/10^11", m_beam_1_rate );
      *out << cgicc::tr();
      *out << cgicc::th( "Beam 1" );
      *out << cgicc::td( buf );
      *out << cgicc::tr();

      sprintf( buf, "%f Hz/cm^2/10^11", m_beam_2_rate );
      *out << cgicc::tr();
      *out << cgicc::th( "Beam 2" );
      *out << cgicc::td( buf );
      *out << cgicc::tr();

      *out << cgicc::tbody();
      *out << cgicc::table();
      *out << cgicc::br();
     }

    void BHMdipprocessor::actionPerformed( xdata::Event& e )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), "Received xdata event " << e.type() );
      if( e.type() == "urn:xdaq-event:setDefaultValues" )
       {
        read_config( m_config_key.toString() );
        m_dip = Dip::create( m_service_name.toString().c_str() );
        if ( !m_dip )
         {
          LOG4CPLUS_FATAL( getApplicationLogger(), "Failed to setup DIP" );
          XCEPT_RAISE( bril::dip::exception::Exception, "Failed to setup DIP" );
         }
        m_servererrorhandler = new ServerErrorHandler;
        Dip::addDipDimErrorHandler( m_servererrorhandler );
        m_puberrorhandler = new PubErrorHandler;
        LOG4CPLUS_INFO( getApplicationLogger(), "Created DIP Factory: " << m_service_name.toString() );
        m_dip->setTimeout( 3 );
        m_scalers[0] = m_dip->createDipSubscription( ( m_dip_root.toString() + "BHM/scaler_rates/scaler0" ).c_str(), this );
        LOG4CPLUS_INFO( getApplicationLogger(), "Subscribed to: " << m_scalers[0]->getTopicName() );
        m_scalers[1] = m_dip->createDipSubscription( ( m_dip_root.toString() + "BHM/scaler_rates/scaler1" ).c_str(), this );
        LOG4CPLUS_INFO( getApplicationLogger(), "Subscribed to: " << m_scalers[1]->getTopicName() );
        m_scalers[2] = m_dip->createDipSubscription( ( m_dip_root.toString() + "BHM/scaler_rates/scaler2" ).c_str(), this );
        LOG4CPLUS_INFO( getApplicationLogger(), "Subscribed to: " << m_scalers[2]->getTopicName() );
        m_beam_1_intensity = m_dip->createDipSubscription( "dip/acc/LHC/Beam/Intensity/Beam1", this );
        LOG4CPLUS_INFO( getApplicationLogger(), "Subscribed to: " << m_beam_1_intensity->getTopicName() );
        m_beam_2_intensity = m_dip->createDipSubscription( "dip/acc/LHC/Beam/Intensity/Beam2", this );
        LOG4CPLUS_INFO( getApplicationLogger(), "Subscribed to: " << m_beam_2_intensity->getTopicName() );
        m_output = m_dip->createDipPublication( ( m_dip_root.toString() + "BHM/rates" ).c_str(), m_puberrorhandler );
        LOG4CPLUS_INFO( getApplicationLogger(), "Publishing to: " << m_output->getTopicName() );
        if ( !( m_scalers[0] && m_scalers[1] && m_scalers[2] && m_output ) )
         {
          LOG4CPLUS_FATAL( getApplicationLogger(), "Failed to setup DIP" ); 
          XCEPT_RAISE( bril::dip::exception::Exception, "Failed to setup DIP" );
         }
       }
     }

    void BHMdipprocessor::actionPerformed( toolbox::Event& e )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), "Received toolbox event " << e.type() );
     }

    void BHMdipprocessor::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist )
     {
      if ( ref )
        ref->release();
     }

    void BHMdipprocessor::connected( DipSubscription* dipsub )
     {
      LOG4CPLUS_INFO( getOwnerApplication()->getApplicationLogger(), "connected to " << dipsub->getTopicName() );
     }

    void BHMdipprocessor::disconnected( DipSubscription* dipsub, char* reason )
     {
      LOG4CPLUS_INFO( getOwnerApplication()->getApplicationLogger(), "disconnected from " << dipsub->getTopicName() );
     }

    void BHMdipprocessor::handleException( DipSubscription* dipsub, DipException& ex )
     {
      LOG4CPLUS_INFO( getOwnerApplication()->getApplicationLogger(), "exception from " << dipsub->getTopicName() << ": " << ex.what() );
     }

    void BHMdipprocessor::handleMessage( DipSubscription* dipsub, DipData& message )
     {
      std::string subname = dipsub->getTopicName();
      LOG4CPLUS_INFO( getOwnerApplication()->getApplicationLogger(), "handleMessage from " << subname );
      if ( message.size() == 0 )
        return;
      if ( subname.find( "BHM/scaler_rates/scaler" ) == std::string::npos )
       {
        if ( subname.find( "Intensity/Beam" ) == std::string::npos )
          return;
        if ( *( subname.end() - 1 ) == '1' )
          m_beam_1_current = message.extractDouble( "totalIntensity" ) / 1.0e11;
        else if ( *( subname.end() - 1 ) == '2' )
          m_beam_2_current = message.extractDouble( "totalIntensity" ) / 1.0e11;
       }
      scaler_set::iterator it = m_scaler_info.find( subname );
      if ( it == m_scaler_info.end() )
       {
        LOG4CPLUS_ERROR( getOwnerApplication()->getApplicationLogger(), subname + " unrecognized publication." );
        return;
       }

      //extract raw rates
      int nch = -1;
      const DipInt* rsd = message.extractIntArray( nch, "RawScalerData" );
      if ( nch != 16 )
       {
        LOG4CPLUS_ERROR( getOwnerApplication()->getApplicationLogger(), subname + " does not contain valid data." );
        return;
       }
      std::copy( rsd, rsd + 16, it->second.m_raw_rate );

      //extract rates
      nch = -1;
      const DipFloat* sd = message.extractFloatArray( nch, "ScalerRates" );
      if ( nch != 16 )
       {
        LOG4CPLUS_ERROR( getOwnerApplication()->getApplicationLogger(), subname + " does not contain valid data." );
        return;
       }
      std::copy( sd, sd + 16, it->second.m_rate );

      //update publication
      publish_to_dip();
     }

    void BHMdipprocessor::publish_to_dip()
     {
      LOG4CPLUS_INFO( getOwnerApplication()->getApplicationLogger(), "publish_to_dip" );
      DipFloat zplus = 0.0;
      int zplus_norm = 0;
      DipFloat zminus = 0.0;
      int zminus_norm = 0;
      for ( scaler_set::iterator it = m_scaler_info.begin(); it != m_scaler_info.end(); ++it )
       {
        const channel_map& map = it->second;
        for ( int ch = 0; ch != 16; ++ch )
         {
          if ( map.m_ch_id[ch] < 0 )
            continue;
          if ( map.m_ch_id[ch] <= 20 )
           {
            if ( map.m_ch_mask[ch] )
             {
              zplus += map.m_rate[ch];
              zplus_norm++;
             }
           }
          else
           {
            if ( map.m_ch_mask[ch] )
             {
              zminus += map.m_rate[ch];
              zminus_norm++;
             }
           }
         }
       }
      float charea = 3.1415 * 2.6 * 2.6;
      zplus = zplus_norm ? zplus / ( charea * zplus_norm ) : -1.0; //normalizes to Hz/cm2
      zminus = zminus_norm ? zminus / ( charea * zminus_norm ) : -1.0; //normalizes to Hz/cm2
      zplus /= m_beam_1_current;
      zminus /= m_beam_2_current;
      DipTimestamp t;
      DipData* dipdata = m_dip->createDipData();
      dipdata->insert( zplus, "ZplusHaloRate" );
      dipdata->insert( zminus, "ZminusHaloRate" );
      dipdata->insert( zplus_norm, "ZplusValidChannels" );
      dipdata->insert( zminus_norm, "ZminusValidChannels" );
      m_output->send( *dipdata, t );
      delete dipdata;
      m_beam_1_rate = zplus;
      m_beam_2_rate = zminus;
    }

    void BHMdipprocessor::read_config( const std::string& key )
     {
      //TODO: actually use key and remove hardcoded config
      //scaler configuration
      channel_map cm0;
      channel_map cm1;
      channel_map cm2;
      cm0.m_ch_id[0] = 11;
      cm0.m_ch_id[1] = 12;
      cm0.m_ch_id[2] = 13;
      cm0.m_ch_id[3] = 14;
      cm0.m_ch_id[4] = 1;
      cm0.m_ch_id[5] = 2;
      cm0.m_ch_id[6] = 3;
      cm0.m_ch_id[7] = 4;
      cm0.m_ch_id[8] = 31;
      cm0.m_ch_id[9] = 32;
      cm0.m_ch_id[10] = 33;
      cm0.m_ch_id[11] = 34;
      cm0.m_ch_id[12] = 21;
      cm0.m_ch_id[13] = 22;
      cm0.m_ch_id[14] = 23;
      cm0.m_ch_id[15] = -1;
      cm1.m_ch_id[0] = 15;
      cm1.m_ch_id[1] = 16;
      cm1.m_ch_id[2] = 17;
      cm1.m_ch_id[3] = 18;
      cm1.m_ch_id[4] = 5;
      cm1.m_ch_id[5] = 6;
      cm1.m_ch_id[6] = 7;
      cm1.m_ch_id[7] = 8;
      cm1.m_ch_id[8] = 35;
      cm1.m_ch_id[9] = 36;
      cm1.m_ch_id[10] = 37;
      cm1.m_ch_id[11] = 38;
      cm1.m_ch_id[12] = 25;
      cm1.m_ch_id[13] = 26;
      cm1.m_ch_id[14] = 27;
      cm1.m_ch_id[15] = -1;
      cm2.m_ch_id[0] = 19;
      cm2.m_ch_id[1] = 20;
      cm2.m_ch_id[2] = -1;
      cm2.m_ch_id[3] = -1;
      cm2.m_ch_id[4] = 9;
      cm2.m_ch_id[5] = 10;
      cm2.m_ch_id[6] = -1;
      cm2.m_ch_id[7] = 28;
      cm2.m_ch_id[8] = 39;
      cm2.m_ch_id[9] = 40;
      cm2.m_ch_id[10] = -1;
      cm2.m_ch_id[11] = 24;
      cm2.m_ch_id[12] = 29;
      cm2.m_ch_id[13] = 30;
      cm2.m_ch_id[14] = -1;
      cm2.m_ch_id[15] = -1;
      for ( int i = 0; i != 16; ++i )
       {
        cm0.m_ch_mask[i] = true;
        cm1.m_ch_mask[i] = true;
        cm2.m_ch_mask[i] = ( i % 4 ) < 2;
       }
      m_scaler_info[m_dip_root.toString() + "BHM/scaler_rates/scaler0"] = cm0;
      m_scaler_info[m_dip_root.toString() + "BHM/scaler_rates/scaler1"] = cm1;
      m_scaler_info[m_dip_root.toString() + "BHM/scaler_rates/scaler2"] = cm2;
     }

   }

 }
