#include "bril/dipprocessor/BLMSummary.h"

bril::dip::BLMSummary::BLMSummary(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
  try{
    //if need to read configuraiton, listen also to defaultparameters
    getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
    getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  }catch( xcept::Exception & e){
    LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
  } 
 
  m_dipRoot="dip/CMS/";
  // the list of BLM Monitors and their positions to monitor
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B2I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B1E10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B2I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B1E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01R5.B1E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B2I21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B2I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02R5.B1E30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B2I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B1E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B2I10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03R5.B1E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E10_TANC.4R5",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B2I10_TCTVA.4R5.B2",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B2I10_TCTH.4R5.B2",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E10_TCLP.4R5.B1",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E10_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04R5.B1E20_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B2I30_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B1E10_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B2I20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B1E20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B2I10_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04R5.B1E30_MQY",0.0));

  m_blmValues.insert(std::make_pair("BLMQI.01L5.B1I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B2E10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B1I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B2E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.01L5.B2E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E22_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B1I21_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E23_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B1I30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.02L5.B2E30_MQXB",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B1I20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B2E20_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B1I10_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.03L5.B2E30_MQXA",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B2E10_TANC.4L5",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B1I10_TCTVA.4L5.B1",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B1I10_TCTH.4L5.B1",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B2E10_TCLP.4L5.B2",0.0));
  m_blmValues.insert(std::make_pair("BLMEI.04L5.B2E10_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B1I30_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B2E10_MQY_XRP",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B1I20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B2E20_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B1I10_MQY",0.0));
  m_blmValues.insert(std::make_pair("BLMQI.04L5.B2E30_MQY",0.0)); 

  m_blmValues.insert(std::make_pair("CMS.BCML1.1L5",0.0));
  m_blmValues.insert(std::make_pair("CMS.BCML2.1L5",0.0));
  m_blmValues.insert(std::make_pair("CMS.BCML1.1R5",0.0));
  m_blmValues.insert(std::make_pair("CMS.BCML2.1R5",0.0)); 

  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B2I30_MQXA",21.4013));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B1E10_MQXA",23.5113));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B2I20_MQXA",25.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B1E20_MQXA",26.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I30_MQXB",29.5305));
  m_blmPositions.insert(std::make_pair("BLMQI.01R5.B1E30_MQXA",32.3705));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I23_MQXB",34.8173));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E21_MQXB",35.3673));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I22_MQXB",37.4663));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E22_MQXB",38.6413));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B2I21_MQXB",40.8793));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E23_MQXB",41.8793));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B2I30_MQXA",43.8205));
  m_blmPositions.insert(std::make_pair("BLMQI.02R5.B1E30_MQXB",46.7633));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B2I20_MQXA",49.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B1E20_MQXA",50.7213));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B2I10_MQXA",53.8837));
  m_blmPositions.insert(std::make_pair("BLMQI.03R5.B1E30_MQXA",59.0833));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E10_TANC.4R5",140.2613));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B2I10_TCTVA.4R5.B2",144.6653));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B2I10_TCTH.4R5.B2",146.7513));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E10_TCLP.4R5.B1",151.0813));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E10_XRP",153.4297));
  m_blmPositions.insert(std::make_pair("BLMEI.04R5.B1E20_XRP",162.1713));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B2I30_MQY",162.1713));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B1E10_MQY",164.6713));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B2I20_MQY",166.9943));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B1E20_MQY",168.1943));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B2I10_MQY",170.1897));
  m_blmPositions.insert(std::make_pair("BLMQI.04R5.B1E30_MQY",174.6643));

  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B1I30_MQXA",-21.4013));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B2E10_MQXA",-23.3587));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B1I20_MQXA",-25.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B2E20_MQXA",-26.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I30_MQXB",-29.4775));
  m_blmPositions.insert(std::make_pair("BLMQI.01L5.B2E30_MQXA",-32.2875));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I23_MQXB",-34.8137));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E21_MQXB",-35.2047));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I22_MQXB",-37.0927));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E22_MQXB",-38.6287));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B1I21_MQXB",-40.3017));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E23_MQXB",-41.7707));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B1I30_MQXA",-43.4727));
  m_blmPositions.insert(std::make_pair("BLMQI.02L5.B2E30_MQXB",-46.4667));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B1I20_MQXA",-49.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B2E20_MQXA",-50.5787));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B1I10_MQXA",-53.3897));
  m_blmPositions.insert(std::make_pair("BLMQI.03L5.B2E30_MQXA",-58.1397));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B2E10_TANC.4L5",-138.6703));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B1I10_TCTVA.4L5.B1",-144.5227));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B1I10_TCTH.4L5.B1",-146.6087));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B2E10_TCLP.4L5.B2",-150.9387));
  m_blmPositions.insert(std::make_pair("BLMEI.04L5.B2E10_XRP",-153.4303));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B1I30_MQY",-162.0287));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B2E10_MQY_XRP",-164.5287));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B1I20_MQY",-166.8467));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B2E20_MQY",-168.0467));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B1I10_MQY",-170.5437));
  m_blmPositions.insert(std::make_pair("BLMQI.04L5.B2E30_MQY",-174.5217));

  m_blmPositions.insert(std::make_pair("CMS.BCML1.1L5",-1.8));
  m_blmPositions.insert(std::make_pair("CMS.BCML2.1L5",-14.4));
  m_blmPositions.insert(std::make_pair("CMS.BCML1.1R5",1.8));
  m_blmPositions.insert(std::make_pair("CMS.BCML2.1R5",14.4));  

  
  for (int i=0;i<NBIN;i++){
    m_histo[i]=1;
  } 
  
}

bril::dip::BLMSummary::~BLMSummary(){
}

void bril::dip::BLMSummary::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    xdata::String* dipRoot = 
    dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
    if(dipRoot)m_dipRoot= dipRoot->value_;
    m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/BLM/LSS5R",(DipSubscription*)0 ));
    m_dipsubs.insert(std::make_pair("dip/acc/LHC/Beam/BLM/LSS5L",(DipSubscription*)0 ));
    m_dipsubs.insert(std::make_pair(m_dipRoot+"BRIL/BCMAnalysis/MonitorRS9",(DipSubscription*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot+"BRIL/BLMSummary",(DipPublication*)0 ));
    m_dippubs.insert(std::make_pair(m_dipRoot+"BRIL/BLMSummary/Histo",(DipPublication*)0 ));
  }
  if( e.type()== "ItemChangedEvent" ){
    std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
    if(item =="nbnum" ){
      std::stringstream ss;
      ss<<"BLMSummary publish to dip"<<std::endl;
      xdata::Serializable* currentrunnum = getOwnerApplication()->getApplicationInfoSpace()->find("runnum");
      xdata::Serializable* currentlsnum = getOwnerApplication()->getApplicationInfoSpace()->find("lsnum");
      xdata::Serializable* currentnbnum = getOwnerApplication()->getApplicationInfoSpace()->find("nbnum");
      
      if(currentrunnum && currentlsnum && currentnbnum){
	ss<<" run "<<dynamic_cast<xdata::UnsignedInteger*>(currentrunnum)->value_ ;
	ss<<" ls "<<dynamic_cast<xdata::UnsignedInteger*>(currentlsnum)->value_ ;
	ss<<" nb "<<dynamic_cast<xdata::UnsignedInteger*>(currentnbnum)->value_ ;
      }
     
      LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
      publish_to_dip();
    }
  }
}

void bril::dip::BLMSummary::actionPerformed(toolbox::Event& e){
}


void bril::dip::BLMSummary::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"BLMSummary::handleMessage from "+subname);
  if(message.size()==0) return;
  if(subname=="dip/acc/LHC/Beam/BLM/LSS5R" or subname == "dip/acc/LHC/Beam/BLM/LSS5L"){
    int sizeNames;
    const std::string * BLMNames = message.extractStringArray(sizeNames,"names");
    //std::cout << "BLMSummary "<< subname<<" names" << sizeNames << " values" <<std::endl; 
    int sizeValues;
    const DipDouble * BLMRS9 = message.extractDoubleArray(sizeValues,"RS9_1310ms");
    //std::cout << "BLMSummary "<<subname<<" RS9_1310ms" << sizeValues << " values" <<std::endl; 
    int loop=sizeNames;
    if(sizeValues<sizeNames) loop=sizeValues;
    for(int i=0; i<loop;i++){
      std::map<std::string,double>::iterator iter1 = m_blmValues.find(BLMNames[i]);
      if(iter1!=m_blmValues.end()){
        iter1->second=BLMRS9[i];
      }
    }

  }
  if (subname==m_dipRoot+"BRIL/BCMAnalysis/MonitorRS9"){
    std::map<std::string,double>::iterator iter1 = m_blmValues.find("CMS.BCML1.1L5");
    if(iter1!=m_blmValues.end()){
      iter1->second=(double)message.extractFloat("RS9L1MinusGy");
    }
    iter1 = m_blmValues.find("CMS.BCML1.1R5");
    if(iter1!=m_blmValues.end()){
      iter1->second=(double)message.extractFloat("RS9L1PlusGy");
    }
    iter1 = m_blmValues.find("CMS.BCML2.1L5");
    if(iter1!=m_blmValues.end()){
      iter1->second=(double)message.extractFloat("RS9L2MinusGy");
    }
    iter1 = m_blmValues.find("CMS.BCML2.1R5");
    if(iter1!=m_blmValues.end()){
      iter1->second=(double)message.extractFloat("RS9L2PlusGy");
    }
  }
}
void bril::dip::BLMSummary::publish_to_dip(){
  DipData* dipdata = m_dip->createDipData();
  std::string diptopicname(m_dipRoot+"BRIL/BLMSummary");
  DipTimestamp t;


  std::map<std::string,double>::iterator iter;
  for (iter=m_blmValues.begin(); iter != m_blmValues.end(); iter++){
    dipdata -> insert(iter->second,iter->first.c_str());
    std::map<std::string,float>::iterator iter1 = m_blmPositions.find(iter->first);
      if(iter1!=m_blmPositions.end()){
        int z;
        if(iter1->second<0){
          z=iter1->second-0.5;
        }else {
          z=iter1->second+0.5;
        }
        int bin=(NBIN/2)+ z;
        if(bin>NBIN-1)bin=NBIN-1;
        if(bin<0)bin=0;
        m_histo[bin]=iter->second*1e9;
        //std::cout<<iter->first<<","<<iter->second<<","<<iter1->second<<","<<bin<<std::endl;
      }    
  }

  m_dippubs[diptopicname]->send(*dipdata,t);
  delete dipdata;

  DipData* dipdataH = m_dip->createDipData();
  std::string diptopicnameH(m_dipRoot+"BRIL/BLMSummary/Histo");
  dipdataH -> insert(m_histo,NBIN,"BLMHisto");
  m_dippubs[diptopicnameH]->send(*dipdataH,t);
  delete dipdataH;
 


}
