#include "bril/dipprocessor/Bkg3Analyzer.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "xcept/tools.h"

bril::dip::Bkg3Analyzer::Bkg3Analyzer(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
  m_fill = 0;
  m_run = 0;
  m_ls = 0;
  m_nb = 0;
  
  m_bkgd1 = -1;
  m_bkgd2 = -1;
  m_bkgd3 = -1;
  
  m_vme_old1 = -1.; m_vme_old2 = -1.;
  m_vme_nc1 = -1.; m_vme_nc2 = -1.;
  m_vme_lead1 = -1.; m_vme_lead2 = -1.;
    
  // BCM1F UTCA
  m_utca_nc1 = -1.; m_utca_nc2 = -1.;
  m_utca_lead1 = -1.; m_utca_lead2 = -1.;
    
  // PLT
  m_plt_nc1 = -1.; m_plt_nc2 = -1.;
  m_plt_lead1 = -1.; m_plt_lead2 = -1.;
        
  // BCML
  m_bcml = -1.;
  
  // By default assume only Leading bunches
  m_non_colliding = false;

  try {
    //if need to read configuraiton, listen also to defaultparameters
    getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
    getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  } catch( xcept::Exception & e) {
    LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
  }
 
  m_dipRoot="dip/CMS/";
}

bril::dip::Bkg3Analyzer::~Bkg3Analyzer(){
}

void bril::dip::Bkg3Analyzer::actionPerformed(xdata::Event& e){
    if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        try{
            xdata::String* dipRoot = dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
            if (dipRoot) m_dipRoot= dipRoot->value_;
        } catch(xdata::exception::Exception& e){
            std::string msg("Failed to find dipRoot in ApplicationInfoSpace ");
            LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
        }
        
        m_dippubs.insert(std::make_pair(m_dipRoot+"LHC/BKGD",(DipPublication*)0 ));
    }
    
    if( e.type()== "ItemChangedEvent" ) {
        std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
        if(item =="nbnum" ) {
            std::stringstream ss;
            try{
                m_fill = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("fillnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find fillnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_run = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("runnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find runnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_ls = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("lsnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find lsnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_nb = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("nbnum"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find nbnum in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
      
            // BCM1F VME
            try{
                m_vme_old1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_old1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_old1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_old2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_old2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_old2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_nc1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_nc1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_nc2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_nc2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_lead1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_lead1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_lead1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_vme_lead2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_vme_lead2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_vme_lead2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // BCM1F UTCA
            try{
                m_utca_nc1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_nc1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_utca_nc2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_nc2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_utca_lead1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_lead1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_lead1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_utca_lead2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_utca_lead2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_utca_lead2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // PLT
            try{
                m_plt_nc1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_nc1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_nc1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_plt_nc2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_nc2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_nc2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_plt_lead1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_lead1"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_lead1 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_plt_lead2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_plt_lead2"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_plt_lead2 in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            // BCML
            try{
                m_bcml = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd_bcml"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find bkgd_bcml in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
      
            // Background source
            try{
                m_detector = dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("detector"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find detector in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            try{
                m_algo = dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("algo"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find algo in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }
            
            try{
                m_non_colliding = dynamic_cast<xdata::Boolean*>(getOwnerApplication()->getApplicationInfoSpace()->find("non_colliding_present"))->value_;
            }catch(xdata::exception::Exception& e){
                std::string msg("Failed to find non_colliding present in ApplicationInfoSpace ");
                LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
            }

            ss << "bril::dip::Bkg3Analyzer "
               << "Fill: " << m_fill << ", "
               << "Run: "  << m_run << ", "
               << "LS: "   << m_ls << ", "
               << "NB4: "  << m_nb << ", "
               << " source of best background: " << m_detector << " "
               << " algo: " << m_algo << ", "
               << " non_colliding: " << m_non_colliding;
            LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
            publish_to_dip();
        }
    }
}

void bril::dip::Bkg3Analyzer::actionPerformed(toolbox::Event& e){
}


void bril::dip::Bkg3Analyzer::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(),"Bkg3Analyzer::handleMessage from "+subname);
  if(message.size()==0) return; 
}

void bril::dip::Bkg3Analyzer::publish_to_dip(){
    // you can use common data such as beamegev in calculation from infospace.
    LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"Bkg3Analyzer::publish_to_dip");
    
    DipData* dipdata = m_dip->createDipData();
    std::string diptopicname(m_dipRoot+"LHC/BKGD");
    
    DipFloat bkgd1(-1), bkgd2(-1);
    std::string source;
    
    // Define Best Background
    if (m_detector == "bcm1f_vme") {
        if (m_algo == "old") {
            bkgd1 = m_vme_old1.value_;
            bkgd2 = m_vme_old2.value_;
            source = "BCM1F_VME_OLD";
        } else if (m_algo == "nc" && m_non_colliding) { // use non-colliding algo only if they injected
            bkgd1 = m_vme_nc1.value_;
            bkgd2 = m_vme_nc2.value_;
            source = "BCM1F_VME_NC";
        } else if (m_algo == "lead" || (m_algo == "nc" && !m_non_colliding)) {
            bkgd1 = m_vme_lead1.value_;
            bkgd2 = m_vme_lead2.value_;
            source = "BCM1F_VME_LEAD";
        }
    } else if (m_detector == "bcm1f_utca") {
        if (m_algo == "nc" && m_non_colliding) {
            bkgd1 = m_utca_nc1.value_;
            bkgd2 = m_utca_nc2.value_;
            source = "BCM1F_UTCA_NC";
        } else if (m_algo == "lead" || (m_algo == "nc" && !m_non_colliding)) {
            bkgd1 = m_utca_lead1.value_;
            bkgd2 = m_utca_lead2.value_;
            source = "BCM1F_UTCA_LEAD";
        }
    } else if (m_detector == "plt") {
        if (m_algo == "nc" && m_non_colliding) {
            bkgd1 = m_plt_nc1.value_;
            bkgd2 = m_plt_nc2.value_;
            source = "PLT_NC";
        } else if (m_algo == "lead" || (m_algo == "nc" && !m_non_colliding)) {
            bkgd1 = m_plt_lead1.value_;
            bkgd2 = m_plt_lead2.value_;
            source = "PLT_LEAD";
        }
    } else {
        LOG4CPLUS_FATAL(getOwnerApplication()->getApplicationLogger(), "Incorrect Luminometer provided, choose bcm1f_vme, bcm1f_utca or plt");
        bkgd1 = -1;
        bkgd2 = -1;
        source = "wrong_source";
    }
    
    DipFloat bkgd3 = m_bcml.value_;
    
    dipdata->insert(bkgd1,"BKGD1");
    dipdata->insert(source.c_str(), "BKGD1_source");
    dipdata->insert(bkgd2,"BKGD2");
    dipdata->insert(source.c_str(), "BKGD2_source");
    dipdata->insert(bkgd3,"BKGD3");
    dipdata->insert("AbortPercent_BCML","BKGD3_Source");
    
    DipTimestamp t;
    m_dippubs[diptopicname]->send(*dipdata,t);
    delete dipdata;
    
    m_bkgd1 = -1;
    m_bkgd2 = -1;
    m_bkgd3 = -1;
}
