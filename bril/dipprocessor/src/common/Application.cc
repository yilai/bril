#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"
#include "toolbox/BSem.h"

#include "b2in/nub/Method.h"
#include "bril/dipprocessor/Application.h"
#include "bril/dipprocessor/DipAnalyzer.h"
#include "bril/dipprocessor/DipAnalyzerFactory.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"

#include "interface/bril/TCDSTopics.hh"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/BCM1FUTCATopics.hh"
#include "interface/bril/BHMTopics.hh"
#include "interface/bril/BCMTopics.hh"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BEAMTopics.hh"

XDAQ_INSTANTIATOR_IMPL (bril::dip::Application)

bril::dip::Application::Application (xdaq::ApplicationStub* s): xdaq::Application(s)
                                                              , xgi::framework::UIManager(this)
                                                              , eventing::api::Member(this)
                                                              , m_applock(toolbox::BSem::FULL) {
                                                              
    xgi::framework::deferredbind(this,this,&bril::dip::Application::Default, "Default");
    b2in::nub::bind(this, &bril::dip::Application::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    
    m_runnum = 0;
    m_lsnum = 0;
    m_nbnum = 0;
    
    m_dipRoot = "dip/CMS/";
    m_busName = "brildata";
    m_signalTopic = "NB4";//no need to configure, we always listen to NB4
    
    m_dipconnected = false;
        
    // Assign new variables for subsystems
    // BCM1F VME
    m_vme_old1 = -1.; m_vme_old2 = -1.;
    m_vme_nc1 = -1.; m_vme_nc2 = -1.;
    m_vme_lead1 = -1.; m_vme_lead2 = -1.;
    
    // BCM1F UTCA
    m_utca_nc1 = -1.; m_utca_nc2 = -1.;
    m_utca_lead1 = -1.; m_utca_lead2 = -1.;
    
    // PLT
    m_plt_nc1 = -1.; m_plt_nc2 = -1.;
    m_plt_lead1 = -1.; m_plt_lead2 = -1.;
    
    // BHM
    m_bhm1 = -1.; m_bhm2 = -1.;
    // BCML
    m_bcml = -1.;
    
    // Bunch mask
    memset (m_iscolliding1, false, sizeof (m_iscolliding1) );
    memset (m_iscolliding2, false, sizeof (m_iscolliding2) );
    m_non_colliding = false;
 
    toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
    m_lastreceive_beam_sec = now.sec();
    m_lastreceive_bcm1f_vme_sec = now.sec();
    m_lastreceive_bcm1f_utca_sec = now.sec();
    m_lastreceive_plt_nc_sec = now.sec();
    m_lastreceive_plt_lead_sec = now.sec();
    m_lastreceive_bcml_sec = now.sec();
    m_lastreceive_bhm_sec = now.sec();
    
    try {
    
        m_appInfoSpace->fireItemAvailable("dipAnalyzers",&m_analyzerconf);
        m_appInfoSpace->fireItemAvailable("fillnum",&m_fillnum);
        m_appInfoSpace->fireItemAvailable("runnum",&m_runnum);
        m_appInfoSpace->fireItemAvailable("lsnum",&m_lsnum);
        m_appInfoSpace->fireItemAvailable("nbnum",&m_nbnum);
        m_appInfoSpace->fireItemAvailable("dipServiceName",&m_dipServiceName);
        m_appInfoSpace->fireItemAvailable("dipRoot",&m_dipRoot);
        m_appInfoSpace->fireItemAvailable("busName",&m_busName);
                
        // TODO: Check additional topics
        m_appInfoSpace->fireItemAvailable("bkgd_vme_old1",&m_vme_old1);
        m_appInfoSpace->fireItemAvailable("bkgd_vme_old2",&m_vme_old2);
        m_appInfoSpace->fireItemAvailable("bkgd_vme_nc1",&m_vme_nc1);
        m_appInfoSpace->fireItemAvailable("bkgd_vme_nc2",&m_vme_nc2);
        m_appInfoSpace->fireItemAvailable("bkgd_vme_lead1",&m_vme_lead1);
        m_appInfoSpace->fireItemAvailable("bkgd_vme_lead2",&m_vme_lead2);
        m_appInfoSpace->fireItemAvailable("bkgd_utca_nc1",&m_utca_nc1);
        m_appInfoSpace->fireItemAvailable("bkgd_utca_nc2",&m_utca_nc2);
        m_appInfoSpace->fireItemAvailable("bkgd_utca_lead1",&m_utca_lead1);
        m_appInfoSpace->fireItemAvailable("bkgd_utca_lead2",&m_utca_lead2);
        m_appInfoSpace->fireItemAvailable("bkgd_plt_nc1",&m_plt_nc1);
        m_appInfoSpace->fireItemAvailable("bkgd_plt_nc2",&m_plt_nc2);
        m_appInfoSpace->fireItemAvailable("bkgd_plt_lead1",&m_plt_lead1);
        m_appInfoSpace->fireItemAvailable("bkgd_plt_lead2",&m_plt_lead2);
        m_appInfoSpace->fireItemAvailable("bkgd_bhm1",&m_bhm1);
        m_appInfoSpace->fireItemAvailable("bkgd_bhm2",&m_bhm2);
        m_appInfoSpace->fireItemAvailable("bkgd_bcml",&m_bcml);
        
        // Define primary background (BKGD1/2) provider and algorithm
        m_appInfoSpace->fireItemAvailable("detector",&m_detector);
        m_appInfoSpace->fireItemAvailable("algo",&m_algo);
        
        m_appInfoSpace->fireItemAvailable("non_colliding_present",&m_non_colliding);
        
        // ...
        m_appInfoSpace->addListener(this, "urn:xdaq-event:setDefaultValues");
  
    } catch(xdata::exception::Exception& e) {
        std::string msg("Failed to setup appInfoSpace ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);
    }
  
    toolbox::net::UUID uuid;
    std::string processuuid = uuid.toString();
    std::string bkgd12_timerurn = "urn:dipApplication_bkg_timer"+processuuid;
    
    // Set timer for flashlist received
    toolbox::task::Timer* t = toolbox::task::TimerFactory::getInstance()->createTimer( bkgd12_timerurn );
    const std::string taskname("bkgd_timer");
    toolbox::TimeInterval vsec(5,0);//start timer 5 sec later
    toolbox::TimeInterval checkinterval (4, 0); // check every 4 seconds
    toolbox::TimeVal startT = now + vsec;
    t->scheduleAtFixedRate (startT, this, checkinterval, 0, taskname);
}

bril::dip::Application::~Application () {
    destroyAnalyzers();
}

void bril::dip::Application::Default (xgi::Input * in, xgi::Output * out) {
}

void bril::dip::Application::timeExpired( toolbox::task::TimerEvent& e ) {
    
    toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

    xdata::UnsignedInteger timeelapsed_beam = now.sec() - m_lastreceive_beam_sec;

    xdata::UnsignedInteger timeelapsed_vme = now.sec() - m_lastreceive_bcm1f_vme_sec;
    xdata::UnsignedInteger timeelapsed_utca = now.sec() - m_lastreceive_bcm1f_utca_sec;
    xdata::UnsignedInteger timeelapsed_plt_nc = now.sec() - m_lastreceive_plt_nc_sec;
    xdata::UnsignedInteger timeelapsed_plt_lead = now.sec() - m_lastreceive_plt_lead_sec;
    xdata::UnsignedInteger timeelapsed_bcml = now.sec() - m_lastreceive_bcml_sec;
    xdata::UnsignedInteger timeelapsed_bhm = now.sec() - m_lastreceive_bhm_sec;

    // Check logic. If no update of flashlist, non of the values could be trusted
    // However if they are already assigned to -1 skip
    if( timeelapsed_beam.value_ > 60 ) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_vme.toString()+" sec passed with no beam mask, resetting them to initial value");
        
        memset (m_iscolliding1, false, sizeof (m_iscolliding1) );
        memset (m_iscolliding2, false, sizeof (m_iscolliding2) );
        m_non_colliding = false;
    } else if ( timeelapsed_beam.value_ > 5 ) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_vme.toString()+" sec passed creating mask");
        
        bool non_coll = false;                
        for (unsigned int icol = 0; icol < NBX; icol++) {
            if ((m_iscolliding1[icol] && !m_iscolliding2[icol]) || ((!m_iscolliding1[icol] && m_iscolliding2[icol]))) {
               non_coll = true;
            }
        }        
        m_non_colliding = non_coll;
    }    
    
    if( timeelapsed_vme.value_ > 5 && m_vme_old1.value_ != -1 && m_vme_old2.value_ != -1 && m_vme_nc1.value_ != -1 && m_vme_nc2.value_ != -1 && m_vme_lead1.value_ != -1 && m_vme_lead1.value_ != -1) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_vme.toString()+" sec passed with no bcm1f_vme, resetting them to initial value");
        m_vme_old1 = -1.; m_vme_old2 = -1.;
        m_vme_nc1 = -1.; m_vme_nc2 = -1.;
        m_vme_lead1 = -1.; m_vme_lead2 = -1.;
    }
    
    if( timeelapsed_utca.value_ > 5 && m_utca_nc1.value_ != -1 && m_utca_nc2.value_ != -1 && m_utca_lead1.value_ != -1 && m_utca_lead2.value_ != -1) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_utca.toString()+" sec passed with no bcm1f_utca, resetting them to initial value");
        m_utca_nc1 = -1.; m_utca_nc2 = -1.;
        m_utca_lead1 = -1.; m_utca_lead2 = -1.;
    }
    
    if( timeelapsed_plt_nc.value_ > 5 && m_plt_nc1.value_ != -1 && m_plt_nc2.value_ != -1) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_plt_nc.toString()+" sec passed with no plt nc, resetting them to initial value");
        m_plt_nc1 = -1.; m_plt_nc2 = -1.;
    }
    
    if( timeelapsed_plt_lead.value_ > 5 && m_plt_lead1.value_ != -1 && m_plt_lead2.value_ != -1) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_plt_lead.toString()+" sec passed with no plt lead, resetting them to initial value");
        m_plt_lead1 = -1.; m_plt_lead2 = -1.;
    }
    
    if( timeelapsed_bcml.value_ > 5 && m_bcml.value_ != -1) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_bcml.toString()+" sec passed with no bcml, resetting them to initial value");
        m_bcml = -1.;
    }
    
    if( timeelapsed_bhm.value_ > 5 && m_bhm1.value_ != -1 && m_bhm2.value_ != -1) {
        LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed_bhm.toString()+" sec passed with no bhm, resetting them to initial value");
        m_bhm1 = -1.; m_bhm2 = -1.;
    }
}

void bril::dip::Application::actionPerformed(xdata::Event& e){
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
        if( !m_dipRoot.value_.empty() ){
            char lastChar = *(m_dipRoot.value_.rbegin());
            if( lastChar!='/' ){
                m_dipRoot.value_ += '/';
            }
        }
        
        std::stringstream ss;
        this->getEventingBus(m_busName.value_).addActionListener(this);
        size_t nanalyzers = m_analyzerconf.elements();
        ss << "Analyzers: ";
        for(size_t i=0; i<nanalyzers; ++i){
            xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_analyzerconf.elementAt(i));
            if(p){
                std::string analyzername = p->getProperty("analyzerName");
                ss << analyzername << " ";
                std::string dipFrequencyStr = p->getProperty("dipFrequency");
                xdata::UnsignedInteger dipFrequency;
                dipFrequency.fromString(dipFrequencyStr);
                createAnalyzer(analyzername,dipFrequency.value_,m_dipServiceName.value_);
            }
        }
        LOG4CPLUS_INFO(getApplicationLogger(), ss.str() );
        
        try {
            this->getEventingBus(m_busName.value_).subscribe(m_signalTopic);
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::bcm1fbkgT::topicname());
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::bcm1futca_backgroundT::topicname());
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::bhmbkgT::topicname());
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::bcmT::topicname());
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::beamT::topicname());
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::pltbkgAT::topicname());
            this->getEventingBus(m_busName.value_).subscribe(interface::bril::pltbkgBT::topicname());
        } catch(eventing::api::exception::Exception& e) {
            std::string msg("Failed to subscribe to eventing bus brildata");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
            XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);
        }
        
    }
}

void bril::dip::Application::createAnalyzer( const std::string& analyzername
                                           , unsigned int dipFrequency
                                           , const std::string& dipServiceName) {
    
    DipAnalyzer* a = DipAnalyzerFactory::create( analyzername, dipFrequency, dipServiceName, this);
    if (!a) {
        LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to create analyzer " + analyzername);
    } else {
        m_analyzers.push_back(a);
    }
}

void bril::dip::Application::destroyAnalyzers(){
  for(std::vector<DipAnalyzer*>::iterator it=m_analyzers.begin();it!=m_analyzers.end();++it){
    delete *it;
  }
}

void bril::dip::Application::actionPerformed(toolbox::Event& e) {
}

void bril::dip::Application::onMessage( toolbox::mem::Reference * ref, xdata::Properties & plist ) {
  
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify") {
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from (IN APPLICATION) "+topic);
        std::string data_version  = plist.getProperty("DATA_VERSION");
        std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
        if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION) {
            std::string msg("Received brildaq message has no or wrong version, do not process.");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            ref->release(); ref=0;
            return;
        }

        // TCDS Topics
        if (topic == m_signalTopic.toString()) {
            if (ref != 0) {
                //parse timing signal message header to get timing info
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                unsigned int fillnum = thead->fillnum;
                unsigned int runnum = thead->runnum;
                unsigned int lsnum = thead->lsnum;
                unsigned int nbnum = thead->nbnum;
                
                m_nbnum = nbnum;
                m_lsnum = lsnum;
                m_runnum = runnum;
                m_fillnum = fillnum;
                
                LOG4CPLUS_INFO(getApplicationLogger(), "Application fire nbnum changed");
                
                // Reconnect to dip if needed
                if( m_dipconnected==false ) {
                    for ( std::vector<DipAnalyzer*>::iterator it=m_analyzers.begin(); it!=m_analyzers.end(); ++it ) {
                        (*it)->subscribeToDip();
                        (*it)->createDipPublication();
                    }
                    m_dipconnected = true;
                }
                m_appInfoSpace->fireItemValueChanged("nbnum",this);
            }
        }
        
        // Beam TOPIC, comming from CommonDataExchange (should be aware of dependency)
        if (topic == interface::bril::beamT::topicname() ) {
            if (ref != 0) {
                // For timer
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_beam_sec = now.sec();
            
                //parse timing signal message header to get timing info
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                
                if( payload_dict.empty() ) {
                    std::string msg("Received beam topic message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                
                tc.extract_field(m_iscolliding1, "bxconfig1", thead->payloadanchor);
                tc.extract_field(m_iscolliding2, "bxconfig2", thead->payloadanchor);
            }
        }
        
        // BCM1F Topic
        if(topic == interface::bril::bcm1fbkgT::topicname()) {
            if (ref != 0) {
                // For timer
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_bcm1f_vme_sec = now.sec();
                
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                if( payload_dict.empty() ) {
                    std::string msg("Received bcm1fbkg message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                float bkgd_vme_old1 = -1.;
                float bkgd_vme_old2 = -1.;
                float bkgd_vme_nc1 = -1.;
                float bkgd_vme_nc2 = -1.;
                float bkgd_vme_lead1 = -1.;
                float bkgd_vme_lead2 = -1.;
                
                // TODO: for now simply assign variables. Once new fields will be added, extract from message
                tc.extract_field( &bkgd_vme_old1, "plusz", thead->payloadanchor );
                tc.extract_field( &bkgd_vme_old2, "minusz", thead->payloadanchor );
                tc.extract_field( &bkgd_vme_nc1, "nc1", thead->payloadanchor );
                tc.extract_field( &bkgd_vme_nc2, "nc2", thead->payloadanchor );
                tc.extract_field( &bkgd_vme_lead1, "lead1", thead->payloadanchor );
                tc.extract_field( &bkgd_vme_lead2, "lead2", thead->payloadanchor );
                
                m_vme_old1 = bkgd_vme_old1;
                m_vme_old2 = bkgd_vme_old2;
                m_vme_nc1 = bkgd_vme_nc1;
                m_vme_nc2 = bkgd_vme_nc2;
                m_vme_lead1 = bkgd_vme_lead1;
                m_vme_lead2 = bkgd_vme_lead2;
            }
        }
        
        // BCM1F UTCA Topic
        if(topic == interface::bril::bcm1futca_backgroundT::topicname()) {
            if (ref != 0) {
                // For timer. Now for both VME/UTCA
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_bcm1f_utca_sec = now.sec();
                
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                if( payload_dict.empty() ) {
                    std::string msg("Received bcm1futca_background message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                float bkgd_utca_nc1 = -1.;
                float bkgd_utca_nc2 = -1.;
                float bkgd_utca_lead1 = -1.;
                float bkgd_utca_lead2 = -1.;
                
                tc.extract_field( &bkgd_utca_nc1, "bkgd1_nc", thead->payloadanchor );
                tc.extract_field( &bkgd_utca_nc2, "bkgd2_nc", thead->payloadanchor );
                tc.extract_field( &bkgd_utca_lead1, "bkgd1", thead->payloadanchor );
                tc.extract_field( &bkgd_utca_lead2, "bkgd2", thead->payloadanchor );
                
                m_utca_nc1 = bkgd_utca_nc1;
                m_utca_nc2 = bkgd_utca_nc2;
                m_utca_lead1 = bkgd_utca_lead1;
                m_utca_lead2 = bkgd_utca_lead2;
            
            }
        }
        
        // BCML Topic
        if (topic == interface::bril::bcmT::topicname()) {
            if (ref != 0) {
                // For timer
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_bcml_sec = now.sec();
            
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                if( payload_dict.empty() ) {
                    std::string msg("Received bcm message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                float bkgd_bcml = -1.;
                tc.extract_field( &bkgd_bcml, "pa1max", thead->payloadanchor );
                m_bcml = bkgd_bcml;
            }
        }
        
        // BHM Topic
        if (topic == interface::bril::bhmbkgT::topicname()) {
            if (ref != 0) {
                // For timer
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_bhm_sec = now.sec();
            
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                if( payload_dict.empty() ) {
                    std::string msg("Received bhmbkg message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                float bkgd_bhm1 = -1.;
                float bkgd_bhm2 = -1.;
                tc.extract_field( &bkgd_bhm1, "beam1", thead->payloadanchor );
                tc.extract_field( &bkgd_bhm2, "beam2", thead->payloadanchor );
                m_bhm1 = bkgd_bhm1;
                m_bhm2 = bkgd_bhm2;
            }
        }
        
        // PLT non-colliding topic
        if (topic == interface::bril::pltbkgAT::topicname()) {
            if (ref != 0) {
                // For timer
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_plt_nc_sec = now.sec();
            
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                if( payload_dict.empty() ) {
                    std::string msg("Received pltbkgA message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                float bkgd_plt_nc1 = -1.;
                float bkgd_plt_nc2 = -1.;
                
                tc.extract_field( &bkgd_plt_nc1, "beam1bkg", thead->payloadanchor );
                tc.extract_field( &bkgd_plt_nc2, "beam2bkg", thead->payloadanchor );
                
                m_plt_nc1 = bkgd_plt_nc1;
                m_plt_nc2 = bkgd_plt_nc2;
                
            }
        }
            
        // PLT leading topic
        if(topic == interface::bril::pltbkgBT::topicname()) {
            if (ref != 0) {
                toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
                m_lastreceive_plt_lead_sec = now.sec();
            
                interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
                if( payload_dict.empty() ) {
                    std::string msg("Received pltbkgB message has no dictionary, do not process.");
                    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
                    ref->release(); ref=0;
                    return;
                }
                interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                float bkgd_plt_lead1 = -1.;
                float bkgd_plt_lead2 = -1.;
                    
                tc.extract_field( &bkgd_plt_lead1, "beam1bkg", thead->payloadanchor );
                tc.extract_field( &bkgd_plt_lead2, "beam2bkg", thead->payloadanchor );
                    
                m_plt_lead1 = bkgd_plt_lead1;
                m_plt_lead2 = bkgd_plt_lead2;
                    
            }
        }
    }
  
    // If non of them release message
    if ( ref!=0 ) {
        ref->release();
        ref=0;
    }
}
