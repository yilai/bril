#ifndef _bril_dipprocessor_BRILAlarms_h_
#define _bril_dipprocessor_BRILAlarms_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/Float.h"
#include "xdata/UnsignedInteger32.h"
#include "toolbox/BSem.h"
#include "toolbox/task/TimerListener.h"

#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"

#include "bril/dipprocessor/exception/Exception.h"
#include <string>

namespace toolbox{
  namespace task{
    class Timer;
  }
}

namespace bril{
  namespace dip{
    class PubErrorHandler;
    class ServerErrorHandler;

    class BRILAlarms: public xdaq::Application,public xdata::ActionListener, public DipSubscriptionListener, public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      BRILAlarms(xdaq::ApplicationStub* s);
      ~BRILAlarms();
      virtual void actionPerformed(xdata::Event& e); 
      virtual void timeExpired(toolbox::task::TimerEvent& e);    
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);
      virtual void connected(DipSubscription* dipsub);
      virtual void disconnected(DipSubscription* dipsub, char *reason);   
      virtual void handleException(DipSubscription* subscription, DipException& ex);
      
    private:      
      xdata::String m_dipInRoot;
      xdata::String m_dipOutRoot;
      xdata::Float m_checkIntervalSec;
      toolbox::task::Timer* m_timer; 
      std::string m_timername;   
      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;
      ServerErrorHandler* m_servererrorhandler;      
      std::map< std::string,DipSubscription* > m_dipsubs;
      std::map< std::string,DipPublication* > m_dippubs; 
      toolbox::BSem m_applock;

      std::string m_machineMode;
      bool m_plcCrate, m_CAEN, m_bkgd, m_bcml,m_bcmLHV, m_PLTCAEN;
      bool m_plcCrateValid;
      float m_bkgd1_local,m_bkgd2_local,m_bkgd3_local;
      float l_bhm1, l_bhm2;
      float l_bkgd1,l_bkgd2,l_bkgd3;
      bool m_beamdump;
      long m_beamdump_ts;
      long m_plcCrate_ts, m_CAEN_ts, m_bkgd_ts, m_tcds_ts,m_bcmLHV_ts, m_PLTCAEN_ts;
      int m_bcm1LHV,m_bcm2LHV;
      std::map< std::string,bool > m_1FmHV;
      std::map< std::string,bool > m_1FpHV;
      std::map< std::string,bool > m_1FmLV;
      std::map< std::string,bool > m_1FpLV;      
      std::map< std::string,bool > m_1LmHV;
      std::map< std::string,bool > m_1LpHV;
      std::map< std::string,bool > m_2LmHV; 
      std::map< std::string,bool > m_2LpHV; 
      std::map< std::string,bool > m_2LmLV; 
      std::map< std::string,bool > m_2LpLV; 
      std::map< std::string,bool > m_BHMmHV; 
      std::map< std::string,bool > m_BHMpHV; 
      std::map< std::string,bool > m_BHMLV;

      std::map< std::string,bool > m_PLTHV;
      std::map< std::string,bool > m_PLTLV;
      long m_bptxScaler_ts;
      long m_bptxScopeMon_ts;
      long m_bptxLHCTiming_ts; 
      long m_hflumi_ts, m_hflumiet_ts, m_pltzlumi_ts, m_bcm1flumi_ts, m_bcm1futcalumi_ts, m_bestlumi_ts, m_dtlumi_ts, m_remuslumi_ts, m_bhm_ts;
      long m_RHUAnalysis_ts, m_RHUHistos_ts;
      
      int m_hflumi_nb4, m_hflumi_ls, m_hflumi_run;
      bool m_hflumi_live, m_hflumi_insync;
      
      int m_hflumiet_nb4, m_hflumiet_ls, m_hflumiet_run;
      bool m_hflumiet_live, m_hflumiet_insync;
      
      int m_pltzlumi_nb4, m_pltzlumi_ls, m_pltzlumi_run;
      bool m_pltzlumi_live, m_pltzlumi_insync;
      
      int m_bcm1flumi_nb4, m_bcm1flumi_ls, m_bcm1flumi_run;
      bool m_bcm1flumi_live, m_bcm1flumi_insync;
      
      int m_bcm1futcalumi_nb4, m_bcm1futcalumi_ls, m_bcm1futcalumi_run;
      bool m_bcm1futcalumi_live, m_bcm1futcalumi_insync;
      
      int m_remuslumi_nb4, m_remuslumi_ls, m_remuslumi_run;
      bool m_remuslumi_live, m_remuslumi_insync;
      
      bool m_bhm, m_bhm_live;
      
      // No nb4 for DT
      int m_dtlumi_ls, m_dtlumi_run;
      bool m_dtlumi_live, m_dtlumi_insync;
      
      int m_bestlumi_nb4, m_bestlumi_ls, m_bestlumi_run;
      bool m_bestlumi_live, m_bestlumi_insync;
      
      long m_b1current_ts, m_b2current_ts;
      bool m_b1_in, m_b2_in;
      int m_hf_BX1, m_hfet_BX1,m_pltz_BX1, m_bcm1f_BX1, m_bcm1futca_BX1;  
     
      void subscribeToDip();
      void createDipPublication();
      void publish_to_dip();

      // nodefaut constructor
      BRILAlarms();
      // nocopy protection
      BRILAlarms(const BRILAlarms&);
      BRILAlarms& operator=(const BRILAlarms&);
    };
  }
}
#endif
