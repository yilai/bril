#ifndef _bril_dip_Application_h_
#define _bril_dip_Application_h_
#include <string>
#include <map>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/BSem.h"


namespace bril{
  namespace dip{
    class DipAnalyzer;

    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      // timer callback
      void timeExpired( toolbox::task::TimerEvent& e );
    protected:
      
      static const unsigned int NBX = 3564;
      
      std::vector<DipAnalyzer*> m_analyzers;
      
      xdata::UnsignedInteger m_fillnum;
      xdata::UnsignedInteger m_runnum;
      xdata::UnsignedInteger m_lsnum;
      xdata::UnsignedInteger m_nbnum;
      
      xdata::String m_busName;
      xdata::String m_dipRoot;
      xdata::String m_dipServiceName;
      
      // Colliding mask
      bool m_iscolliding1[NBX];
	  bool m_iscolliding2[NBX];
      xdata::Boolean m_non_colliding;
      
      xdata::String m_detector;
      xdata::String m_algo;
      
      xdata::Float m_vme_old1;
      xdata::Float m_vme_old2;
      xdata::Float m_vme_nc1;
      xdata::Float m_vme_nc2;
      xdata::Float m_vme_lead1;
      xdata::Float m_vme_lead2;
      xdata::Float m_utca_nc1;
      xdata::Float m_utca_nc2;
      xdata::Float m_utca_lead1;
      xdata::Float m_utca_lead2;
      xdata::Float m_plt_nc1;
      xdata::Float m_plt_nc2;
      xdata::Float m_plt_lead1;
      xdata::Float m_plt_lead2;
      xdata::Float m_bhm1;
      xdata::Float m_bhm2;
      xdata::Float m_bcml;
      
      unsigned int m_lastreceive_beam_sec;
      unsigned int m_lastreceive_bcm1f_vme_sec;
      unsigned int m_lastreceive_bcm1f_utca_sec;
      unsigned int m_lastreceive_plt_nc_sec;
      unsigned int m_lastreceive_plt_lead_sec;
      unsigned int m_lastreceive_bcml_sec;
      unsigned int m_lastreceive_bhm_sec;
      
    private:
      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      xdaq::ApplicationDescriptor *m_appDescriptor;
  
      // configuration parameters
      xdata::String m_signalTopic;
      xdata::Vector<xdata::Properties> m_analyzerconf;

      toolbox::BSem m_applock;
      bool m_dipconnected;
      void createAnalyzer(const std::string& analyzername,unsigned int dipfrequency,
        const std::string& dipServiceName);
      void destroyAnalyzers();

      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
