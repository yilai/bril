#ifndef _bril_dipprocessor_BkgdPublisher_h_
#define _bril_dipprocessor_BkgdPublisher_h_

#include <map>
#include "xdata/Float.h"
#include "xdata/String.h"
#include "bril/dipprocessor/DipAnalyzer.h"


namespace bril{
  namespace dip{

    class BkgdPublisher: public DipAnalyzer{
    public:
      BkgdPublisher(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName, xdaq::Application* owner);
      // destructor
      ~BkgdPublisher();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // concrete implementation
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);      
    private:
      unsigned int m_fill;
      unsigned int m_run;
      unsigned int m_ls;
      unsigned int m_nb;
      
      std::string m_dipRoot;
      
      xdata::Float m_vme_old1;
      xdata::Float m_vme_old2;
      xdata::Float m_vme_nc1;
      xdata::Float m_vme_nc2;
      xdata::Float m_vme_lead1;
      xdata::Float m_vme_lead2;
      xdata::Float m_utca_nc1;
      xdata::Float m_utca_nc2;
      xdata::Float m_utca_lead1;
      xdata::Float m_utca_lead2;
      xdata::Float m_plt_nc1;
      xdata::Float m_plt_nc2;
      xdata::Float m_plt_lead1;
      xdata::Float m_plt_lead2;
      xdata::Float m_bhm1;
      xdata::Float m_bhm2;
      xdata::Float m_bcml;
      
    private:    
      void publish_to_dip();
      // nodefaut constructor
      BkgdPublisher();
      // nocopy protection
      BkgdPublisher(const BkgdPublisher&);
      BkgdPublisher& operator=(const BkgdPublisher&);
    };
  }
}
#endif
