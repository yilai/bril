#ifndef _bril_dip_LumiTopicsPublisher_h_
#define _bril_dip_LumiTopicsPublisher_h_
#include <string>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/framework/UIManager.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
namespace bril{
  namespace dip{
    class PubErrorHandler;

    class LumiTopicsPublisher : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      LumiTopicsPublisher(xdaq::ApplicationStub* s);
      // destructor
      ~LumiTopicsPublisher ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      
      void createDipPublication();
      void publishToDip() ;
      void publishLumiToDip() ;
      void publishLumiSection() ;
      void accumulateLumiSection(const std::string & lumidet, const float lumi, const float * bxlumi);

    protected:
      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;

      //registries
      std::map< std::string,DipPublication* > m_dippubs; 
            
      // configuration parameters
      xdata::String m_bus;     
      xdata::String m_dipRoot;
      
      // local variables
      toolbox::TimeVal m_last_publishing_todip ;
      toolbox::TimeVal m_last_publishing_todipls ;
      bool m_isfirsttcds;
      
      float m_remuslumi;
      float m_remuslumiRaw;
      
      float m_dtlumi;
      float m_dtlumiRaw;
      
      float m_hflumi;
      float m_hflumiRaw;
      float m_maxhf;
      float m_hflumiet;
      float m_hflumietRaw;
      float m_maxhfet;
      int m_binhf;
      int m_binhfet;
      
      float m_bcmflumi;
      float m_bcmflumiRaw;
      float m_maxbcmf;
      int m_binbcmf;
      
      float m_bcmfutcalumi;
      float m_bcmfutcalumiRaw;
      float m_maxbcmfutca;
      int m_binbcmfutca;
      
      float m_pltlumi;
      float m_pltlumiRaw;
      float m_maxplt;
      int m_binplt;
      float m_pltlumizero;
      float m_pltlumizeroRaw;
      float m_maxpltzero;
      int m_binpltzero;
      int m_fill;
      int m_run;
      int m_lastrun;
      int m_ls;
      int m_nb;
      int m_bestfill;
      int m_bestrun;
      int m_bestls;
      int m_bestnb;  
      int m_ts;
      int m_tms;
      int m_nColliding;
      int m_bestlumits;
      float m_deadfrac;
      float m_avgPileup;
      float m_rmsPileup;
      float m_lumiError;
      std::string m_mode;
      float m_lumi;
      std::string m_source;
      float m_bxdelivered[3564];
      bool m_collmask[3564];
      float m_collidingCurrents[3564];
      float m_specificLumi[3564];

      float ls_hflumi;
      float ls_hflumiet;
      float ls_bcmflumi;
      float ls_bcmfutcalumi;
      float ls_pltlumi;
      float ls_pltzclumi;
      float ls_bxhflumi[3564];
      float ls_bxhflumiet[3564];
      float ls_bxbcmflumi[3564];
      float ls_bxbcmfutcalumi[3564];
      float ls_bxpltlumi[3564];
      float ls_bxpltzclumi[3564];
      int ls_hfCount;
      int ls_hfetCount;
      int ls_bcmfCount;
      int ls_bcmfutcaCount;
      int ls_pltCount;
      int ls_pltzcCount;
      int ls_lastPublication;
      int ls_lastrun;
      int ls_lastls;
      toolbox::BSem m_applock;
      //workloops
      toolbox::task::WorkLoop* m_wl_publishing_todip;
      toolbox::task::WorkLoop* m_wl_publishing_todipls;
      toolbox::task::ActionSignature* m_as_publishing_todip;
      toolbox::task::ActionSignature* m_as_publishing_todipls;                  
      bool publishing_todip(toolbox::task::WorkLoop* wl);
      bool publishing_todipls(toolbox::task::WorkLoop* wl);

    private:
      std::string m_preferredScanSource;
      
    private:      
      // nocopy protection
      LumiTopicsPublisher (const LumiTopicsPublisher&);
      LumiTopicsPublisher & operator=(const LumiTopicsPublisher&);

    };
  }
}
#endif
