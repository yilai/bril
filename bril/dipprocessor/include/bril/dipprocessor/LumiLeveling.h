#ifndef _bril_dip_LumiLeveling_h_
#define _bril_dip_LumiLeveling_h_
#include <string>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/framework/UIManager.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Float.h"
#include "xdata/Boolean.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

namespace bril{
namespace dip{
class PubErrorHandler;

class LumiLeveling : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener{
public:
    XDAQ_INSTANTIATOR();
    // constructor
    LumiLeveling(xdaq::ApplicationStub* s);
    // destructor
    ~LumiLeveling ();
    // xgi(web) callback
    void Default(xgi::Input * in, xgi::Output * out);
    // web setter
    void setParameter(xgi::Input * in, xgi::Output * out);
    // web setter
    void logOut(xgi::Input * in, xgi::Output * out);
    // infospace event callback
    virtual void actionPerformed(xdata::Event& e);
    // toolbox event callback
    virtual void actionPerformed(toolbox::Event& e);
    // b2in message callback
    void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

    void createDipPublication();
    void publishToDip() ;

protected:
    DipFactory* m_dip;
    PubErrorHandler* m_puberrorhandler;

    //registries
    std::map< std::string,DipPublication* > m_dippubs;

    // configuration parameters
    xdata::String m_bus;
    xdata::String m_dipRoot;
    xdata::String M_targetLumiQuantity;
    xdata::Boolean M_enable;
    xdata::Float M_targetLumi;
    xdata::Float M_levelStepSigma;

    std::string m_user;
    xdata::String m_last_user;

    // local variables
    bool m_isfirsttcds;
    int m_run;
    int m_lastrun;
    bool m_AbortLeveling,m_StatusFlag;
    bool m_enable;
    float m_targetLumi;
    float m_levelStepSigma;
    std::string m_targetLumiQuantity;

    int m_ls;
    int m_nb ;
    int m_ts;
    int m_tms;
    int m_last_lumi_time_sec;

    std::string m_mode;
    float m_lumi;
    std::string m_source;

    toolbox::BSem m_applock;
    //workloops
    toolbox::task::WorkLoop* m_wl_publishing_todip;
    toolbox::task::ActionSignature* m_as_publishing_todip;
    bool publishing_todip(toolbox::task::WorkLoop* wl);

private:
    // nocopy protection
    LumiLeveling (const LumiLeveling&);
    LumiLeveling & operator=(const LumiLeveling&);

};
}
}
#endif
