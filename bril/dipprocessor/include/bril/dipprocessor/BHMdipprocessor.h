/*
 * BHM DIP Processor xdaq Application.
 * 
 * This Application processes VME scaler data from DIP and publishes a per beam rate.
 * 
 */

#ifndef _bril_dip_BHMDipProcessor_h_
#define _bril_dip_BHMDipProcessor_h_

#include "b2in/nub/exception/Exception.h"

#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"

#include "eventing/api/Member.h"

#include "log4cplus/logger.h"

#include "toolbox/ActionListener.h"
#include "toolbox/BSem.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"

#include "xdaq/Application.h"

#include "xdata/ActionListener.h"
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"

#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xgi/Method.h"
#include "xgi/Output.h"


namespace bril
 {

  namespace bhmdipprocessor
   {

    class PubErrorHandler;
    class ServerErrorHandler;

    class BHMdipprocessor : public xdaq::Application,
                            public xgi::framework::UIManager,
                            public eventing::api::Member,
                            public xdata::ActionListener,
                            public toolbox::ActionListener,
                            public DipSubscriptionListener
     {

      private:
      struct channel_map
       {
        int m_raw_rate[16];
        float m_rate[16];
        int m_ch_id[16];
        bool m_ch_mask[16];
       };

      typedef std::map<std::string, channel_map> scaler_set;

      public:
      XDAQ_INSTANTIATOR();

      BHMdipprocessor( xdaq::ApplicationStub* );

      ~BHMdipprocessor();

      virtual void Default( xgi::Input* in, xgi::Output* out); // xgi(web) callback

      virtual void actionPerformed( xdata::Event& ); // infospace event callback

      virtual void actionPerformed( toolbox::Event& ); // toolbox event callback

      virtual void onMessage( toolbox::mem::Reference*, xdata::Properties& ); // b2in message callback

      virtual void handleMessage( DipSubscription*, DipData& ); // dip message callback

      virtual void connected( DipSubscription* );

      virtual void disconnected( DipSubscription*, char* );

      virtual void handleException( DipSubscription*, DipException& );

      private:
      void publish_to_dip();
      void read_config( const std::string& );

      private:
      toolbox::BSem m_app_lock;
      xdata::InfoSpace* m_app_infospace;

      xdata::String m_service_name;
      xdata::String m_dip_root;
      xdata::String m_config_key;

      scaler_set m_scaler_info;

      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;
      ServerErrorHandler* m_servererrorhandler;
      DipSubscription* m_scalers[3];
      DipSubscription* m_beam_1_intensity;
      DipSubscription* m_beam_2_intensity;
      DipPublication* m_output;

      float m_beam_1_rate;
      float m_beam_2_rate;
      float m_beam_1_current;
      float m_beam_2_current;

      private:
      // nodefault constructor
      BHMdipprocessor();
      // nocopy protection
      BHMdipprocessor( const BHMdipprocessor& );
      BHMdipprocessor& operator = ( const BHMdipprocessor& );

     };

   }

 }

#endif
