#ifndef _bril_dip_PLTAnalyzer_h_
#define _bril_dip_PLTAnalyzer_h_
#include <string>
#include <map>
#include <list>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"

#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"
#include "bril/dipprocessor/exception/Exception.h"
#include <fstream>

namespace bril{
  namespace dip{
    class PubErrorHandler;
    class ServerErrorHandler;

    class PLTAnalyzer : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener, public DipSubscriptionListener {
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      PLTAnalyzer(xdaq::ApplicationStub* s);
      // destructor
      ~PLTAnalyzer ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

      // dip message handler
      void handleMessage(DipSubscription* dipsub, DipData& message);

      // DipSubscriptionListener methods
      void connected(DipSubscription* dipsub);
      void disconnected(DipSubscription* subscription,char *reason);
      void handleException(DipSubscription* subscription, DipException& ex);
      void subscribeToDip();
      void createDipPublication();
      void publishToDip() ;
      void publishHistosToDip() ;
      void lumiCalc() ;

    protected:
      xdata::UnsignedInteger m_fillnum;
      xdata::UnsignedInteger m_runnum;
      xdata::UnsignedInteger m_lsnum;
      xdata::UnsignedInteger m_nbnum;
      xdata::UnsignedInteger m_tssec;
      xdata::UnsignedInteger m_tsmsec;

      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;
      ServerErrorHandler* m_servererrorhandler;

      //registries
      std::map< std::string,DipSubscription* > m_dipsubs;
      std::map< std::string,DipPublication* > m_dippubs; 
            
      // configuration parameters
      xdata::String m_signalTopic; 
      xdata::String m_inTopicStr; 
      xdata::String m_bus;     
      //xdata::String m_outTopicStr;
      xdata::String m_dipSubStr;
      xdata::String m_dipPubStr;  
      xdata::String m_dipRootArg;
      xdata::String m_dipServiceName;     
      toolbox::BSem m_applock;

      //monitoring
      xdata::InfoSpace *m_monInfoSpace;
      std::list<std::string> m_monItemList;

      xdata::Vector<xdata::UnsignedInteger> m_mon_lumivec;

      //      unsigned char chId;                                                                                                        
      double _sumIntegrationTime;
      float _avgLuminosity;
      float _avgLuminosityError;

      int binSize, abortGapSize;
      //binSize = 3564
      //abortGapSize = 120
      float _bx[3564];
      float _bxtail[120];
      float _bxSum;

      float _avgraw, _avgrawError;

      float _bxraw[3564];
      float _bxrawtail[120];
      float _bxrawSum;

      static const int chCount = 16;
      unsigned int _chHits[chCount];
      unsigned int _chHitsT[chCount];
      unsigned int _chHitsZ[chCount];
      unsigned int _chHitsSum[chCount];
      unsigned int _lumi_track_telescope[chCount];
      unsigned int _lumi_zero_telescope[chCount];

      //      std::vector < std::vector< int > > _chLS(10, std::vector < int > (10) );

      std::vector<std::vector<int> > _chLS;

      unsigned int _chTripCounter[chCount];

      double _chAvgCoincs[chCount];
      double _pOFAvg[chCount];

      std::string m_diplocation;
      std::string _beamstatus;

      int currNb, prevNb, NbDiff, tripCounter, nLS, nLS_max, LastRun;

      bool bkgd, badTCDS, runChange;

      double allChAvg;

    private:
      int64_t m_dipDataReceived;
      std::string m_dipRoot;
      bool m_busready;

    private:      
      // nocopy protection
      PLTAnalyzer (const PLTAnalyzer&);
      PLTAnalyzer & operator=(const PLTAnalyzer&);
    private: 
      //ostreams
      std::ofstream m_vdmfile;      
      std::ofstream m_commondatafile;
      std::string m_processuuid;        

    };
  }
}
#endif
