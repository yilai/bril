#ifndef _bril_dip_BCMFAnalyzer_h_
#define _bril_dip_BCMFAnalyzer_h_
#include <string>
#include <map>
#include <list>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"

#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"
#include "bril/dipprocessor/exception/Exception.h"

namespace bril{
  namespace dip{
    class PubErrorHandler;
    class ServerErrorHandler;

    class BCMFAnalyzer : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener {
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      BCMFAnalyzer(xdaq::ApplicationStub* s);
      // destructor
      ~BCMFAnalyzer ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

      // dip message handler
      void handleMessage(DipSubscription* dipsub, DipData& message);

      // DipSubscriptionListener methods
      void connected(DipSubscription* dipsub);
      void disconnected(DipSubscription* subscription,char *reason);
      void handleException(DipSubscription* subscription, DipException& ex);
      void subscribeToDip();
      void createDipPublication();
      void publishToDip() ;
      void publishHistosToDip() ;

    protected:
      
      unsigned int k_algoid;
      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;
      ServerErrorHandler* m_servererrorhandler;

      //registries
      std::map< std::string,DipPublication* > m_dippubs; 
            
      // configuration parameters
      xdata::String m_bus;     
      xdata::String m_dipRoot;  

    private:
      int64_t m_integrals[64];
      DipInt  m_histosSection[64][3564];
      DipInt  m_histosNibble[64][3564];
      unsigned int m_nbcount;

    private:      
      // nocopy protection
      BCMFAnalyzer (const BCMFAnalyzer&);
      BCMFAnalyzer & operator=(const BCMFAnalyzer&);
    private: 
      //ostreams
    };
  }
}
#endif
