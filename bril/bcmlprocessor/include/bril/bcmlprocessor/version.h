// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_bcmlprocessor_version_h_
#define _bril_bcmlprocessor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILBCMLPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILBCMLPROCESSOR_VERSION_MINOR 0
#define BRIL_BRILBCMLPROCESSOR_VERSION_PATCH 6
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILBCMLPROCESSOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILBCMLPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILBCMLPROCESSOR_VERSION_MAJOR,BRIL_BRILBCMLPROCESSOR_VERSION_MINOR,BRIL_BRILBCMLPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILBCMLPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILBCMLPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILBCMLPROCESSOR_VERSION_MAJOR,BRIL_BRILBCMLPROCESSOR_VERSION_MINOR,BRIL_BRILBCMLPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILBCMLPROCESSOR_FULL_VERSION_LIST BRIL_BRILBCMLPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILBCMLPROCESSOR_VERSION_MAJOR,BRIL_BRILBCMLPROCESSOR_VERSION_MINOR,BRIL_BRILBCMLPROCESSOR_VERSION_PATCH)
#endif

namespace brilbcmlprocessor
{
        const std::string project = "bril";
	const std::string package = "brilbcmlprocessor";
	const std::string versions = BRIL_BRILBCMLPROCESSOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bcmlprocessor";
	const std::string description = "BCML processor";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
