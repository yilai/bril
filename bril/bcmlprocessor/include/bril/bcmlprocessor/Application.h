#ifndef _bril_bcmlprocessor_Application_h_
#define _bril_bcmlprocessor_Application_h_
#include <string>
#include <map>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/TimeVal.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "bril/bcmlprocessor/exception/Exception.h"

#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "dip/DipData.h"

namespace bril{

  namespace bcmlprocessor{
    const int MAX_CARDS = 4;
    //BCM specifics
    const int MAX_CHANNELS = 16;
    const int NUMBER_RSUMS = 12;
    const int NUMBERTOEVENTING = 576;

    class PubErrorHandler;
    class ServerErrorHandler;

    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public DipSubscriptionListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      // dip message handler
      void handleMessage(DipSubscription* dipsub, DipData& message);
      // DipSubscriptionListener methods
      void connected(DipSubscription* dipsub);
      void disconnected(DipSubscription* subscription,char *reason);
      void handleException(DipSubscription* subscription, DipException& ex);
      void subscribeToDip();
      void createDipPublication();

    protected:
      
      xdata::UnsignedInteger32 m_fillnum;
      xdata::UnsignedInteger32 m_runnum;
      xdata::UnsignedInteger32 m_lsnum;
      xdata::UnsignedInteger32 m_nbnum;      
      xdata::UnsignedInteger32 m_sec;
      xdata::UnsignedInteger32 m_msec;
      xdata::String m_busName;
      xdata::String m_dipRoot;
      xdata::String m_signalTopic;
      xdata::String m_dipServiceName;
      DipFactory* m_dip;
      PubErrorHandler* m_puberrorhandler;
      ServerErrorHandler* m_servererrorhandler;
      //registries
      std::map< std::string,DipSubscription* > m_dipsubs;
      std::map< std::string,DipPublication* > m_dippubs; 

    private:
      // members
      // configuration parameters
      toolbox::BSem m_applock;
      
      //bool m_busready;
      //bool m_clockready;

      //Variables associated with the BCM task
    
      int64_t m_acqArray[MAX_CARDS][MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_acqEventing[NUMBERTOEVENTING];
      int64_t m_thresholdArray[MAX_CARDS][MAX_CHANNELS][NUMBER_RSUMS];
      bool m_abortChannelsArray[MAX_CARDS][MAX_CHANNELS];
      int64_t m_card2[MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_card3[MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_card4[MAX_CHANNELS][NUMBER_RSUMS];
      int64_t m_dipDataReceived;
      int64_t m_bcmDipTime;
      int m_liveBCM1LChannels;
      int m_liveBCM2LChannels;
      int m_liveRS1Thresholds;
      int m_liveRS12Thresholds;      

      bool m_BeamPermit;
      float m_RS1AbortFrac[MAX_CARDS][MAX_CHANNELS];
      float m_RS12AbortFrac[MAX_CARDS][MAX_CHANNELS];
      float m_bkgd1LminusRS1, m_bkgd1LplusRS1, m_bkgd2LminusRS1,m_bkgd2LplusRS1;
      float m_bkgd1LminusRS12, m_bkgd1LplusRS12, m_bkgd2LminusRS12,m_bkgd2LplusRS12;
      int64_t m_RS9L1Minus, m_RS9L1Plus, m_RS9L2Minus,m_RS9L2Plus;
      // PA are PercentageAbort variable, RS is the Unnormalized Running Sums
      float m_PA1_NB4_Filtered[48],m_PA1_NB64_Filtered[48];
      float m_PA1_NB4[48],m_PA1_NB64[48];
      int64_t m_RS9_NB4[48],m_RS9_NB64[48];
      float m_PA12_NB4[48],m_PA12_NB64[48];
      bool m_lostFrames,m_thresholdsOK;
      int64_t m_timeLastDump;

            //
      // variables to send to eventing for offline analysis
      unsigned int m_acq[576];
      unsigned int m_acqtsarray[3];
      int m_LostFrames[12];
      int m_nDump[6];
      bool m_BlecsDump;
      bool m_inAbort[48];
      

      xdata::InfoSpace *m_monInfoSpace;           
      xdata::Vector< xdata::Float > m_PA1;
      xdata::Float m_PA1_max;
      xdata::Vector< xdata::Float > m_PA1_Filt;
      xdata::Vector< xdata::Float > m_PA12;
      xdata::Vector< xdata::Float > m_RS9;
      xdata::Boolean m_lF;
      xdata::Boolean m_tOK;
      xdata::UnsignedInteger m_tLD;
      xdata::TimeVal m_acqTime;

      std::list<std::string> m_monItemList; 

      toolbox::mem::MemoryPoolFactory* m_poolFactory;
      toolbox::mem::Pool* m_memPool;

      void publish_to_dip();
      void publish_to_eventing();
      //
      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
