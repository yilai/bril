//TODO:
//take only odd bins for the albedo computation?? WRONG, check timing diagram where we expect collision products and MIB and take all others
//for bkg_nc take all non collding bunches and integrate over full 25ns DONE
//colhist: use only luminous (3,4,5,6) bins of colliding bunches DONE
//bkghist: use only MIB bins(1,2) of non colliding bunches according to bunch mask DONE

#ifndef _bril_bcm1f_utcaprocessor_backgroundalgorithm_h_
#define _bril_bcm1f_utcaprocessor_backgroundalgorithm_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"
#include <deque>

namespace bril {

    namespace bcm1futcaprocessor {

        //Zero Counting Lumi Algorithm
        class DefaultBackground : public BackgroundAlgorithm
        {
          public:
            DefaultBackground (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool )
            {
                m_channelInfo = channels;
                m_memPoolFactory = factory;
                m_memPool = pool;

                m_albedoAboveBkg = 0;
            }

	    ~DefaultBackground() = default;

            void compute (  OccupancyHistogramBuffer* histogramBuffer, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>& publishQueue, MonitoringVariables& monitoring)
            {
                //first, loop the Histogram Vector
                //for (auto& histBuffer : histogramVector)
                if(histogramBuffer != nullptr)
                {

                    //For the background topic
                    std::string pdict = interface::bril::bcm1futca_backgroundT::payloaddict();
                    xdata::Properties plist;
                    plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                    plist.setProperty ("PAYLOAD_DICT", pdict);

                    //first, the References for the lumi numbers
                    toolbox::mem::Reference* background = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futcalumiT::maxsize() );
                    interface::bril::bcm1futca_backgroundT* backgroundhist = this->getHistPtr<interface::bril::bcm1futca_backgroundT> (background, histogramBuffer->m_nb4Identifier, SensorType::All, 0, m_channelInfo->getAlgo());

                    ////now I have a histogram buffer object
                    ////histBuffer.first is the NB4 identifier in case I need it later
                    ////histBuffer.second is the Map<HistogramIdentifier, Histogram>

                    ////here i should instantiate the various variables for bkg calculations
                    ////to be fair, I need these split up by AlgoIDs too

                    float bkg1sum = 0;
                    float bkg2sum = 0;

                    //these are for the bkg_nc12
                    float bkg1sum_nc = 0;
                    float bkg2sum_nc = 0;

                    //these come from the beam topic, so compute them in the Beam object
                    float bunchCurrent1Sum = beam.getBunchCurrentSumBkg (1);
                    float bunchCurrent2Sum = beam.getBunchCurrentSumBkg (2);

                    //these I need to map to algo ID, otherwise i count doulbe
                    float totalActiveArea1 = 0;
                    float totalActiveArea2 = 0;

                    float bkg_plus = 0;
                    float bkg_minus = 0;

                    unsigned int nActiveChannels1 = 0;
                    unsigned int nActiveChannels2 = 0;

                    //for the per BX background
                    std::vector<float> bkg1_bx(interface::bril::shared::MAX_NBX, 0.0) ;
                    std::vector<float> bkg2_bx(interface::bril::shared::MAX_NBX, 0.0) ;

                    for (auto& hist : histogramBuffer->m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);

                        ////now looping the acutal channel histograms
                        ////hist.first is the Histogram Identifier
                        ////hist.second is the histogram object

                        //prepare and get the colhist
                        std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_col_histT::topicname(), this->createColHist (hist.second, beam, histogramBuffer->m_nb4Identifier, hist.first) );
                        publishQueue.push (t_pair);

                        //prepare and get the bkg hist
                        t_pair = std::make_pair (interface::bril::bcm1futca_bkg_histT::topicname(), this->createBkgHist (hist.second, beam, histogramBuffer->m_nb4Identifier, hist.first) );
                        publishQueue.push (t_pair);

                        hist.second.maskAbortGap();//maks the abort gap first before integrating for total rate

                        ////now test if we want this channel for bkg calculations
                        if (m_channelInfo->useBkg (t_channel) )
                        {
                            SensorPosition t_pos = m_channelInfo->getSensorPosition (t_channel);

                            // get the total active area and number of sensors
                            // this needs to be mapped to the algo ID otherwise I would count twice
                            if (t_pos == SensorPosition::plus)
                            {
                                nActiveChannels1++;

                                totalActiveArea1 += bril::bcm1futcaprocessor::s_areaSilicon * m_channelInfo->getFactor (hist.first);
                            }
                            else if (t_pos == SensorPosition::minus)
                            {
                                nActiveChannels2++;

                                totalActiveArea2 += bril::bcm1futcaprocessor::s_areaSilicon * m_channelInfo->getFactor (hist.first);
                            }

                            //now loop the uTCA histogram bins
                            //float binsum_nc = 0;

                            for (size_t iBin = 0; iBin < hist.second.size(); iBin++ )
                            {
                                unsigned int t_beam = 0;

                                if (t_pos == SensorPosition::plus) t_beam = 1;
                                else if (t_pos == SensorPosition::minus) t_beam = 2;

                                //first, check if we want to use this bunch for BKG
                                size_t iBX = iBin / interface::bril::BCM1FUTCA_BINPERBX_OCC;

                                if (beam.test_bkg (t_beam, static_cast<unsigned int> (iBX) ) )
                                {
                                    if (iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC == 0) // this is a MIB BIN
                                    {
                                        //calculate albedo as average over the last 20 bins - this will have to change; TODO
                                        float albedo = 0;
                                        float n_albedoBins = 0;

                                        for (size_t i = 0; i < 20; i++)
                                        {
                                            size_t albedoBin = iBin - i - 1;
                                            if (albedoBin >= interface::bril::BCM1FUTCA_BINPERBX_OCC * interface::bril::shared::MAX_NBX){
						 albedoBin = interface::bril::BCM1FUTCA_BINPERBX_OCC * interface::bril::shared::MAX_NBX + iBin - i - 1;
						}


                                            if (this->isAlbedoBin (albedoBin) )
                                            {
                                                albedo += static_cast<float> ( hist.second[albedoBin]) / 4.; //normalize by the number of nibbles
                                                n_albedoBins++;
                                            }
                                        }

                                        albedo /= n_albedoBins;
                                        //compute the initial number for the background
                                        float t_bkg = static_cast<float> (hist.second[iBin]) / m_channelInfo->getFactor (hist.first) / 4.;
                                        //subtract the albedo
                                        t_bkg -= albedo;

                                        if (t_bkg < 0)
                                        {
                                            t_bkg = 0;
                                            m_albedoAboveBkg++;
                                        }

                                        //now check if the bunch is not colliding - this is an additional restriction as some colliding bunches are used for BKG if they are the first in a train
                                        if (!beam.isColliding (iBX) )
                                        {
                                            if (t_pos == SensorPosition::plus)
                                                bkg1sum_nc += t_bkg;
                                            else if (t_pos == SensorPosition::minus)
                                                bkg2sum_nc += t_bkg;
                                        }

                                        if (t_pos == SensorPosition::plus)
                                        {
                                            bkg1sum += t_bkg;
                                            bkg1_bx.at (iBX) = t_bkg;
                                        }
                                        else if (t_pos == SensorPosition::minus)
                                        {
                                            bkg2sum += t_bkg;
                                            bkg2_bx.at (iBX) = t_bkg;
                                        }

                                    } //end of check if first bin in BX
                                } //end of bkg bin check

                                //TODO: alternative version of treating nc background but I guess this is wrong
                                //if (!beam.isColliding (iBX) )
                                //{
                                ////we need to aggregate into bxsum_nc and reset every 6 bins
                                //float t_bkg = static_cast<float> (hist.second[iBin]) / m_channelInfo->getFactor (hist.first) / 4.;
                                //binsum_nc += t_bkg;

                                //if (iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC == 0  && iBin != 0) //condition that we are in a new BX
                                //{
                                //if (t_pos == SensorPosition::plus)
                                //bkg1sum_nc[t_algo] += binsum_nc;
                                //else if (t_pos == SensorPosition::minus)
                                //bkg2sum_nc[t_algo] += binsum_nc;

                                ////reset the binsum
                                //binsum_nc = 0;
                                //}
                                //}
                            }// end of bin loop
                        }
                    }// end of channel / algo loop

                    //now need to normalize to to Hz/cm^2
                    //1 nibble = ~0.3s; t = 4096 orbits * 3564 BX * 25ns/BX = 0.365s

                    //normalize the BKG sum
                    if (totalActiveArea1 != 0)
                    {
                        bkg_plus = bkg1sum / (0.365 * 4. * totalActiveArea1);
                        bkg1sum_nc /= (0.365 * 4. * totalActiveArea1);
                    }

                    if (totalActiveArea2 != 0)
                    {
                        bkg_minus = bkg2sum / (0.365 * 4. * totalActiveArea2);
                        bkg2sum_nc /= (0.365 * 4. * totalActiveArea2);
                    }

                    //normalize the per BX background
                    //1 nibble: t = 4096 orbits * 25ns = 0.00009988776655s
                    for (size_t i = 0; i < interface::bril::shared::MAX_NBX; i++)
                    {
                        if (totalActiveArea1 != 0) bkg1_bx.at (i) /= (0.00009988776655 * 4 * totalActiveArea1);

                        if (totalActiveArea2 != 0) bkg2_bx.at (i) /= (0.00009988776655 * 4 * totalActiveArea2);

                        //TODO: need to normalize by bunch Current?
                        //
                        //attention, be sure to not sum all bunches but check in VME CODE TODO
                        float b1bxcurrent = beam.getBunchCurrent (1, i);
                        float b2bxcurrent = beam.getBunchCurrent (2, i);

                        if (b1bxcurrent != 0)
                            bkg1_bx.at (i) /= b1bxcurrent;

                        if (b2bxcurrent != 0)
                            bkg2_bx.at (i) /= b2bxcurrent;

                        //check for division by 0 TODO
                    }

                    if (bunchCurrent1Sum > 0.01)
                    {
                        bkg_plus /= bunchCurrent1Sum;
                        bkg1sum_nc /= bunchCurrent1Sum;
                    }
                    else
                    {
                        bkg_plus = 0;
                        bkg1sum_nc = 0;
                    }

                    if (bunchCurrent2Sum > 0.01)
                    {
                        bkg_minus /= bunchCurrent2Sum;
                        bkg2sum_nc /= bunchCurrent2Sum;
                    }
                    else
                    {
                        bkg_minus = 0;
                        bkg2sum_nc = 0;
                    }

                    //bkg_plus = bkg1sum;
                    //bkg_minus = bkg2sum;


                    //publish the bkg12 topic
                    std::pair<std::string, toolbox::mem::Reference*> t_pair_12 = std::make_pair (interface::bril::bcm1futca_bkg12T::topicname(), this->createBKG12Topic (histogramBuffer->m_nb4Identifier, bkg_plus, bkg_minus) );
                    publishQueue.push (t_pair_12);

                    //copy the per BX vectors to arrays
                    float bkg1_bx_array[interface::bril::shared::MAX_NBX];
                    std::copy (bkg1_bx.begin(), bkg1_bx.end(), bkg1_bx_array);
                    float bkg2_bx_array[interface::bril::shared::MAX_NBX];
                    std::copy (bkg2_bx.begin(), bkg2_bx.end(), bkg2_bx_array);

                    //here i have processed a complete NB4 and calculated the background
                    interface::bril::shared::CompoundDataStreamer tc (pdict);
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd1", &bkg_plus );
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd2", &bkg_minus );
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd1_nc", &bkg1sum_nc);
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd2_nc", &bkg2sum_nc);
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd1histsum", &bkg1sum );
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd2histsum", &bkg2sum );
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd1hist", bkg1_bx_array );
                    tc.insert_field (backgroundhist->payloadanchor, "bkgd2hist", bkg2_bx_array );

                    ////put into publishing queueu
                    std::pair<std::string, toolbox::mem::Reference*> t_pair_bkg = std::make_pair (interface::bril::bcm1futca_backgroundT::topicname(), background);
                    publishQueue.push (t_pair_bkg);

                    //now still need to fill the monitoring variables
                    monitoring.m_bg1Plus = bkg_plus;
                    monitoring.m_bg2Minus = bkg_minus;
                } // end of histogrambuffer vector loop
            }

            std::string name() const
            {
                return "DefaultBackground";
            }

          private:

            toolbox::mem::Reference* createBKG12Topic (NB4Identifier nb4, float bkg1, float bkg2)
            {
                toolbox::mem::Reference* ref = 0;
                ref = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_bkg12T::maxsize() );
                ref->setDataSize (interface::bril::bcm1futca_bkg12T::maxsize() );
                interface::bril::bcm1futca_bkg12T* bkg = (interface::bril::bcm1futca_bkg12T*) (ref->getDataLocation() );
                bkg->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                bkg->setFrequency (4);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                bkg->setResource (bestsource, m_channelInfo->getAlgo(), 0, interface::bril::shared::StorageType::COMPOUND);
                bkg->setTotalsize (interface::bril::bcm1futca_bkg12T::maxsize() );

                std::string pdict = interface::bril::bcm1futca_bkg12T::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field (bkg->payloadanchor, "bkgd1", &bkg1 );
                tc.insert_field (bkg->payloadanchor, "bkgd2", &bkg2 );

                return ref;
            }

            toolbox::mem::Reference* createColHist (bril::bcm1futcaprocessor::Histogram < uint16_t, interface::bril::shared::MAX_NBX* interface::bril::BCM1FUTCA_BINPERBX_OCC> hist, const Beam& beam, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);


                //now loop over the histogram and mask the MIB bins (0,1)
                for (size_t iBin = 0; iBin < hist.size(); iBin++)
                {
                    size_t iBX = iBin / interface::bril::BCM1FUTCA_BINPERBX_OCC;
                    size_t subBX = iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC;

                    if (!beam.isColliding (iBX) ) hist.at (iBin) = 0;
                    else
                    {
                        //so it acutally is colliding
                        if (!this->isLuminousBin (subBX) ) hist.at (iBin) = 0;
                    }

                }

                //first, get a rebinned histogram per bx that I can publish as agghist
                Histogram < uint16_t, interface::bril::shared::MAX_NBX> col_hist = hist.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                //not sure if this is explicitly needed after the previous treatment
                col_hist.maskAbortGap();

                //compute total rates??
                uint32_t totalrate = col_hist.integral();

                toolbox::mem::Reference* t_colhist = 0;
                t_colhist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_col_histT::maxsize() );
                t_colhist->setDataSize (interface::bril::bcm1futca_col_histT::maxsize() );
                interface::bril::bcm1futca_col_histT* colhist = (interface::bril::bcm1futca_col_histT*) (t_colhist->getDataLocation() );
                colhist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                colhist->setFrequency (4);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                colhist->setResource (bestsource, t_algo, t_channel, interface::bril::shared::StorageType::COMPOUND);
                colhist->setTotalsize (interface::bril::bcm1futca_col_histT::maxsize() );

                std::string pdict = interface::bril::bcm1futca_col_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field (colhist->payloadanchor, "totalhits", &totalrate );
                tc.insert_field (colhist->payloadanchor, "colhist", col_hist.data() );

                return t_colhist;
            }

            toolbox::mem::Reference* createBkgHist (bril::bcm1futcaprocessor::Histogram < uint16_t, interface::bril::shared::MAX_NBX* interface::bril::BCM1FUTCA_BINPERBX_OCC> hist, const Beam& beam, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);

                //now loop over the histogram and mask the Lumi bins (0,1)
                for (size_t iBin = 0; iBin < hist.size(); iBin++)
                {
                    size_t iBX = iBin / interface::bril::BCM1FUTCA_BINPERBX_OCC;
                    size_t subBX = iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC;

                    if (beam.isColliding (iBX) ) hist.at (iBin) = 0;
                    else
                    {
                        //so it acutally is not colliding
                        if (this->isLuminousBin (subBX) ) hist.at (iBin) = 0;
                    }
                }

                //first, get a rebinned histogram per bx that I can publish as agghist
                Histogram < uint16_t, interface::bril::shared::MAX_NBX> bkg_hist = hist.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                //not sure if this is explicitly needed after the previous treatment
                bkg_hist.maskAbortGap();

                //compute total rates??
                uint32_t totalrate = bkg_hist.integral();

                toolbox::mem::Reference* t_bkghist = 0;
                t_bkghist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_bkg_histT::maxsize() );
                t_bkghist->setDataSize (interface::bril::bcm1futca_bkg_histT::maxsize() );
                interface::bril::bcm1futca_bkg_histT* bkghist = (interface::bril::bcm1futca_bkg_histT*) (t_bkghist->getDataLocation() );
                bkghist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                bkghist->setFrequency (4);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                bkghist->setResource (bestsource, t_algo, t_channel, interface::bril::shared::StorageType::COMPOUND);
                bkghist->setTotalsize (interface::bril::bcm1futca_bkg_histT::maxsize() );

                std::string pdict = interface::bril::bcm1futca_bkg_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field (bkghist->payloadanchor, "totalhits", &totalrate );
                tc.insert_field (bkghist->payloadanchor, "bkghist", bkg_hist.data() );

                return t_bkghist;
            }

            template<class DataT>
            DataT* getHistPtr (toolbox::mem::Reference* ref, NB4Identifier nb4, SensorType source, unsigned int channel, unsigned int algo)
            {
                //ref = m_memPoolFactory->getFrame (m_memPool, DataT::maxsize() );
                ref->setDataSize (DataT::maxsize() );
                DataT* hist = (DataT*) (ref->getDataLocation() );
                //std::cout << "inmethod:" << ref->getDataLocation() << " hist " << hist << " maxsize " << DataT::maxsize() << " ref " << ref << std::endl;

                hist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                hist->setFrequency (4);

                int bestsource = interface::bril::shared::DataSource::BCM1FUTCA;

                hist->setResource (bestsource, algo, channel, interface::bril::shared::StorageType::COMPOUND);
                hist->setTotalsize (DataT::maxsize() );

                return hist;
            }

            inline bool isLuminousBin (size_t subBXbin)
            {
                if (subBXbin < 2) return false;
                else if (subBXbin > 1 && subBXbin < 6) return true;
                else return false;
            }

            inline bool isAlbedoBin (size_t bin)
            {
                if (bin % 6 == 1) return true;
                else if (bin % 6 == 5) return true;
                else return false;
            }

          private:
            ChannelInfo* m_channelInfo;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;
            unsigned int m_albedoAboveBkg;
        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
