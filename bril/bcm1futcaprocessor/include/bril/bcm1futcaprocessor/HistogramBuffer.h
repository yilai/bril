#ifndef _bril_bcm1f_utcaprocessor_histogrambuffer_h_
#define _bril_bcm1f_utcaprocessor_histogrambuffer_h_

//std c++
#include <algorithm>
#include <numeric>

//BRIL
#include "interface/bril/shared/CommonDataFormat.h"

//XDAQ
//#include "toolbox/mem/Reference.h"
//#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
//#include "toolbox/task/Guard.h"

//mine
#include "bril/bcm1futcaprocessor/Histogram.h"
#include "bril/bcm1futcaprocessor/Utils/ConsoleColor.h"

namespace bril {

    namespace bcm1futcaprocessor {

        struct HistogramIdentifier
        {
          public:
            HistogramIdentifier (const unsigned int algoID, const unsigned int channelID) : 
		m_identifier (0),
		m_algo(algoID),
		m_channel(channelID)
            {
                m_identifier |= ( (algoID & 0xFF) << 8) | (channelID & 0xFF);
            }
            void decode (unsigned int& algoID, unsigned int& channelID) const
            {
                algoID = (m_identifier & 0xFF00) >> 8;
                channelID = (m_identifier & 0x00FF);
            }
	    
	    const uint16_t& getChannel() const
	    {
		return m_channel;
	    }

	    const uint16_t& getAlgo() const
	    {
		return m_algo;
	    }

            bool operator < (const HistogramIdentifier& rhs) const
            {
                return std::tie(this->m_channel, this->m_algo) < std::tie(rhs.m_channel, rhs.m_algo);
            }

            bool operator == (const HistogramIdentifier& rhs) const
            {
                if (this->m_channel == rhs.m_channel && this->m_algo == rhs.m_algo) return true;
                else return false;
            }

	    //copy assignment
	    HistogramIdentifier& operator = (HistogramIdentifier const & rhs) 
	    {
		this->m_channel = rhs.m_channel;
		this->m_algo = rhs.m_algo;
		this->m_identifier = rhs.m_identifier; 
		return *this;	
	    }

            void print() const
            {
                std::cout << "Hist identifier::print Channel " << m_channel << " Algo " << m_algo << std::endl;
            }


          private:
            uint16_t m_identifier;
	    uint16_t m_algo;
	    uint16_t m_channel;
        }; // struct HistogramIdentifier


        struct NB4Identifier
        {
            unsigned int m_fillnum;
            unsigned int m_runnum;
            unsigned int m_lsnum;
            unsigned int m_nbnum;
            unsigned int m_timestampsec;
            unsigned int m_timestampmsec;

            NB4Identifier (unsigned int fillnum, unsigned int runnum, unsigned int lsnum, unsigned int nbnum, unsigned int timestampsec, unsigned int timestampmsec) :
                m_fillnum (fillnum),
                m_runnum (runnum),
                m_lsnum (lsnum),
                m_nbnum (nbnum),
                m_timestampsec (timestampsec),
                m_timestampmsec (timestampmsec)
            {}

            NB4Identifier() :
                m_fillnum (0),
                m_runnum (0),
                m_lsnum (0),
                m_nbnum (0),
                m_timestampsec (0),
                m_timestampmsec (0)
            {}

            bool operator < (const NB4Identifier& rhs) const
            {
                return std::tie (this->m_fillnum, this->m_runnum, this->m_lsnum, this->m_nbnum) < std::tie (rhs.m_fillnum, rhs.m_runnum, rhs.m_lsnum, rhs.m_nbnum);
            }

            void print() const
            {
                std::cout << "NB4 identifier::print Fill " << m_fillnum << " Run " << m_runnum << " LS " << m_lsnum << " NB4 " << m_nbnum << std::endl;
            }
        }; // struct NB4Identifier

        struct LSIdentifier
        {
            unsigned int m_fillnum;
            unsigned int m_runnum;
            unsigned int m_lsnum;

            LSIdentifier (unsigned int fillnum, unsigned int runnum, unsigned int lsnum) :
                m_fillnum (fillnum),
                m_runnum (runnum),
                m_lsnum (lsnum)
            {}

            LSIdentifier() :
                m_fillnum (0),
                m_runnum (0),
                m_lsnum (0)
            {}

            bool operator < (const LSIdentifier& rhs) const
            {
                return std::tie (this->m_fillnum, this->m_runnum, this->m_lsnum) <  std::tie (rhs.m_fillnum, rhs.m_runnum, rhs.m_lsnum);
            }
        }; // struct LSIdentifier

        //forwared declaration of NB4Buffer needed for aggregate
        //template<typename DataT, size_t nBin>
        //class NB4Buffer;//: public HistogramBuffer<DataT, nBin>

        template<typename DataT, size_t nBin>
        class HistogramBuffer
        {

          protected:
            bool m_isComplete;
            size_t m_defaultSize;
            //needed for use as LS buffer
            std::map<HistogramIdentifier, size_t> m_aggregateCounter{};
            //needed for use as NB4 buffer
            unsigned int m_timeout;
          public:
            std::string m_histType;
            std::map<HistogramIdentifier, Histogram<DataT, nBin>> m_channelMap{};
            NB4Identifier m_nb4Identifier;
            unsigned int m_lastAdditionTime;

	    HistogramBuffer() : 
                m_isComplete (false),
                m_defaultSize (0),
                m_timeout (2),
                m_histType (""),
                m_nb4Identifier ()
            {
                this->m_aggregateCounter.clear();
                m_channelMap.clear();

                //set the last received Time to construction time
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                m_lastAdditionTime = t.sec();
            }
		

	    //standard constructor
            HistogramBuffer (size_t defaultSize, std::string histType, NB4Identifier nb4, unsigned int timeout = 2) :
                m_isComplete (false),
                m_defaultSize (defaultSize),
                m_timeout (timeout),
                m_histType (histType),
                m_nb4Identifier (nb4)
            {
                this->m_aggregateCounter.clear();
                m_channelMap.clear();

                //set the last received Time to construction time
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                m_lastAdditionTime = t.sec();
            }

	    ~HistogramBuffer()
	    {
            	m_aggregateCounter.clear();
            	m_channelMap.clear();
	    }

	    //copy C'tor
            HistogramBuffer (HistogramBuffer const & rhs) = delete;

	    //move C'tor
            HistogramBuffer (HistogramBuffer && rhs) noexcept =  default;

	    //copy assignment	    
	    HistogramBuffer& operator = (HistogramBuffer const & rhs) = delete;

	    //move assignment	    
	    HistogramBuffer& operator = (HistogramBuffer && rhs) noexcept= default;

	    bool operator < ( HistogramBuffer const & rhs)
	    {
                return std::tie (this->m_histType, this->m_nb4Identifier, this->m_lastAdditionTime, this->m_isComplete, this->m_defaultSize) <  std::tie (rhs.m_histType, rhs.m_nb4Identifier, rhs.m_lastAdditionTime, rhs.m_isComplete, rhs.m_defaultSize);
	    }

            //generic methods shared between NB4 and LS (used to be base class)
            const bool is_complete() const
            {
                return m_isComplete;
            }

            const size_t size() const 
            {
                return m_channelMap.size();
            }

            const std::string getHistType() const
            {
                return m_histType;
            }

            const NB4Identifier getNB4Identifier() const
            {
                return m_nb4Identifier;
            }

            //NB4 specific methods (used to be NB4Buffer specialisation)
            void check_complete(std::ostream& logstream = std::cout) &
            {
                //if the size equals the calculated default size then we're good so we set isComplete tor true and return
                if (this->m_channelMap.size() == this->m_defaultSize )
                {
                    this->m_isComplete = true;
                    return;
                }
                //if this is not the case then we check if now-last addition time is greater timeout
                else
                {
                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    unsigned int now = t.sec();

                    if (now - this->m_lastAdditionTime > this->m_timeout)
		    {
                        //we haven't had an update in a while
                        //so we dicide to screw it and move on with our lives
                        this->m_isComplete = true;
	        	logstream << "WARNING; Check-complete (NB4 specialization): the buffer timed out (timeout setting = " << this->m_timeout << ") only "<< this->m_channelMap.size() << " channels out of " << this->m_defaultSize << " were received!" << std::endl;
		    }

                }
            }

            void add (HistogramIdentifier& histIdentifier, Histogram<DataT, nBin>& hist) &
            {
                //check if this element already exists
                auto pos = this->m_channelMap.find (histIdentifier);

                if (pos != std::end (this->m_channelMap) )
                    std::cout << RED << "ERROR: a histogram for this channel and algo exists already!" << RESET << std::endl;
                else
                {
                    this->m_channelMap.insert (std::make_pair (histIdentifier, hist) );
                    //set the last addition time to now
                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    this->m_lastAdditionTime = t.sec();
                }

                this->check_complete();
            }

            //LS Buffer specific methods (used to be LSBuffer specialisation)
            const size_t getAggregateOperations (const HistogramIdentifier& id) const 
            {
                //std::map<HistogramIdentifier, size_t>::iterator it = this->m_aggregateCounter.find (id);
                const auto & it = this->m_aggregateCounter.find (id);

                if (it != this->m_aggregateCounter.end() )
                    return it->second;
                else
                    return 0;
            }

	    //The Lumisection buffer version
            void check_complete (unsigned int timeout_sec, std::ostream& logstream = std::cout ) &
            {
                //this is complete under two conditions:
                //if the size is the channell map size and every channel has been aggregated 16 times - that is the easy one
                //we don't even have the correct number of histograms yet so we return and leave m_isComplete false
                if (this->m_channelMap.size() == this->m_defaultSize )
                {
                    unsigned int true_count = 0;

                    //so the map size is ok, now let's check we have 16 aggregate operations
                    for (auto aggIt : this->m_aggregateCounter)

                        //if the aggregate ops for each channel is 16 increment the true count
                        if (aggIt.second == 16) true_count++;

                    if (true_count == this->m_defaultSize)
                    {
                        this->m_isComplete = true;
                        return;
                    }
		    //else
		    //{

		    //}
                }
                else
                {
                    //if the channel map size is not the default size but all present channels have 16 aggregations, chances are high we won't get the missing channels
                    unsigned int true_count = 0;

                    //so the map size is ok, now let's check we have 16 aggregate operations
                    for (auto aggIt : this->m_aggregateCounter)

                        //if the aggregate ops for each channel is 16 increment the true count
                        if (aggIt.second == 16) true_count++;

                    if (true_count == this->m_channelMap.size() )
                    {
			logstream << "WARNING; check_complete (LS version): not all channels have been received for this LS " << this->m_nb4Identifier.m_lsnum << " (got " << this->m_channelMap.size() << " out of " << this->m_defaultSize << " but all have 16 aggregate operations" << std::endl;
                        this->m_isComplete = true;
                        return;
                    }
                }

                //now, let's assume neither the channel map has the correct size, nor all channels have enough aggregations
                //in this case we check if it's timed out
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                unsigned int now = t.sec();

                if (now - this->m_lastAdditionTime > timeout_sec)
		{
                    this->m_isComplete = true;
		    logstream << "WARNING; check_complete (LS version): not all channels have been received for this LS "<< this->m_nb4Identifier.m_lsnum << " (got " << this->m_channelMap.size() << " out of " << this->m_defaultSize << " and there are not enough aggregate operatoins - this buffer timed out in " << timeout_sec << "seconds! Number of aggregation counts: " << std::endl;
                    for (auto aggIt : this->m_aggregateCounter)
		    {
			unsigned int channel;
			unsigned int algo;
			aggIt.first.decode(algo, channel);
			logstream << "Channel " << channel << " (" << algo << "): " << aggIt.second << std::endl;
                    }

		}
            }

	    template<typename bintype>
            void aggregate (bril::bcm1futcaprocessor::HistogramBuffer<bintype, nBin>& aBuf) &
            {
                for (auto aBufIt : aBuf.m_channelMap)
                {
                    //iterate the incoming and check
                    auto histIt = this->m_channelMap.find (aBufIt.first);

                    if (histIt != std::end (this->m_channelMap) )
                    {
                        //the incoming histogram exists already so I add and increment the aggregage counter
                        histIt->second += aBufIt.second.template convert<uint32_t>();
                        this->m_aggregateCounter[aBufIt.first]++;
                    }
                    else
                    {
                        //the incoming channel is not yet part of the LSBuffer so I'm adding it and set the aggregate Counter to 1
                        this->m_channelMap[aBufIt.first] = aBufIt.second.template convert<uint32_t>();
                        this->m_aggregateCounter[aBufIt.first] = 1;
                    }

                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    this->m_lastAdditionTime = t.sec();
                }

            }


            void reScale() &
            {
                //this is intended to re-scale a LS buffer once it is marked complete
                for (auto mapIt : this->m_channelMap)
                {
                    //auto countIt = this->m_aggregateCounter.find (mapIt.first);
                    double factor = 16. / this->getAggregateOperations (mapIt.first);
                    mapIt.second.scale (factor);
                }
            }
        };

    }
}
#endif
