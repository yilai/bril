#ifndef _bril_bcm1f_utcaprocessor_histogram_h_
#define _bril_bcm1f_utcaprocessor_histogram_h_

#include <algorithm>
#include <numeric>
#include <math.h>

#include "interface/bril/shared/CommonDataFormat.h"

namespace bril {

    namespace bcm1futcaprocessor {

        template<typename DataT, size_t nBin>
        class Histogram
        {
          public:
            //Data types and iterators
            typedef DataT value_type;
            typedef DataT* iterator;
            typedef const DataT* const_iterator;

          private:
            //underlying C-style data container
            //DataT m_data[nBin];
            std::array<DataT, nBin> m_data{};

          public:
            //Constructors
            Histogram()
            {
		m_data.fill(0);
	    }

            //Default constructor
            Histogram ( DataT init)
            {
                //clear (init);
                m_data.fill(init);
            }

            //Constructor from Toolbox::Mem::Reference??
            //Histogram()
            template<typename DatumT>
            Histogram (const interface::bril::shared::Datum<DatumT>& aDatum)
            {
                //this const cast is necessary because Datum does not have a const-qualified accessor ...
                interface::bril::shared::Datum<DatumT>& bDatum = const_cast<interface::bril::shared::Datum<DatumT>&> (aDatum);

                for (size_t idx = 0; idx != this->size(); ++idx)
                    m_data[idx] = bDatum.payload() [idx];
            }

	    ~Histogram() = default;

            //Utility Members
            size_t size() const
            {
                //return nBin;
                return m_data.size();
            }

            void clear (DataT value = DataT (0) )
            {
                //for (iterator it = begin(); it != end(); ++it)
                //    *it = value;
                m_data.fill(value);
            }

            //iterator positions & accessors
            const_iterator cbegin() const
            {
                //return m_data;
                return m_data.cbegin();
            }

            iterator begin()
            {
                //return m_data;
                return m_data.begin();
            }

            iterator rbegin()
            {
                //return m_data + nBin;
                return m_data.rbegin();
            }
            iterator rend()
            {
                //return m_data;
                return m_data.rend();
            }

            const_iterator cend() const
            {
                //return m_data + nBin;
                return m_data.cend();
            }

            iterator end()
            {
                //return m_data + nBin;
                return m_data.end();
            }

            value_type& operator [] (size_t index)
            {
                return m_data[index];
            }

            value_type operator [] (size_t index) const
            {
                return m_data[index];
            }

            value_type& at (size_t index)
            {
                return m_data[index];
            }

            const value_type& at (size_t index) const
            {
                return m_data[index];
            }

            //+ & += operators
            Histogram<DataT, nBin> operator += (const Histogram<DataT, nBin>& aHist) //const
            {
                const_iterator aHistIt = aHist.cbegin();

                for (iterator it = begin(); it != end(); ++it)
                {
                    *it += *aHistIt;
                    aHistIt++;
                }

                return *this;
            }

	    template<typename T>
            Histogram<T, nBin> convert()
            {
                Histogram<T, nBin> returnHist;

                for (size_t i = 0; i < this->size(); i++)
                    returnHist.at (i) = static_cast<T> (this->at (i) );

                return returnHist;
            }

            Histogram<DataT, nBin> operator + (const Histogram<DataT, nBin>& aHist)
            {
                Histogram<DataT, nBin> returnHist (*this);
                return returnHist += aHist;
            }

            //operators to add to histogram from bril::Datum base class
            template<typename DatumT>
            Histogram<DataT, nBin>& operator += (const interface::bril::shared::Datum<DatumT>& aDatum)
            {
                //this const cast is necessary because Datum does not have a const-qualified accessor ...
                interface::bril::shared::Datum<DatumT>& bDatum = const_cast<interface::bril::shared::Datum<DatumT>&> (aDatum);

                for (size_t idx = 0; idx != this->size(); ++idx)
                    m_data[idx] += bDatum.payload() [idx];

                return *this;
            }

            template<typename DatumT>
            Histogram<DataT, nBin>& operator + (const interface::bril::shared::Datum<DatumT>& aDatum) const
            {
                Histogram<DataT, nBin> returnHist (*this);
                return returnHist += aDatum;
            }

            void shift ( const int shift)
            {
                int s = shift % (int) nBin;

                if (s < 0) s += nBin;

                std::reverse (this->begin(), this->end() );
                std::reverse (this->begin(), this->begin() + s);
                std::reverse (this->begin() + s, this->end() );

            }

            //rebinning
            template<size_t factor>
            Histogram < DataT, nBin / factor > rebin() const
            {
                static_assert (nBin % factor == 0, "Rebinning requires exact division");
                Histogram < DataT, nBin / factor > ret{};

                for (size_t idx = 0; idx != ret.size(); idx++)
                    for (size_t jdx = 0; jdx != factor; jdx++)
                        ret[idx] += m_data[idx * factor + jdx];

                return ret;
            }


            void scale (double factor)
            {
                for (size_t idx = 0; idx != this->size(); ++idx)
                {
                    double value = (double) m_data[idx] * factor;
                    m_data[idx] = static_cast<DataT> (round (value) );
                }
            }

            void maskAbortGap()
            {
                //we're using a running average algorithm to find the test pulse, so running average over 3*nBinsperBX bins is constantly calculated
                //and we mask all bins that exceed the running sum using the previous window_size bins by more than 30%
                //actually by masking, we set it to the running average
                unsigned int window_size = 3;
                double running_average = 0;

                std::deque<DataT> sum_deque;

                //abort gap is BX 3534 to 3564
                if (nBin == interface::bril::shared::MAX_NBX) // we are dealing with a per BX histogram already
                {
                    for (size_t iBX = 3534; iBX < interface::bril::shared::MAX_NBX; iBX++)
                    {
                        //if we are in the first iteration  we can't do much
                        if (iBX == 3534)
                        {
                            sum_deque.push_back (m_data[iBX]);
                            running_average = m_data[iBX];
                            continue;
                        }

                        if (m_data[iBX] > 1.3 * running_average) m_data[iBX] = running_average;
                        else
                        {
                            sum_deque.push_back (m_data[iBX]);

                            while (sum_deque.size() > window_size) sum_deque.pop_front();

                            int sum = 0;

                            for (auto it : sum_deque)
                                sum += it;

                            running_average = (double) sum / (double) sum_deque.size();
                        }
                    }

                }
                else
                {
                    size_t nBinsperBX = nBin / interface::bril::shared::MAX_NBX;
                    window_size = 3 * nBinsperBX;
                    //use a larger window size since we have way more bins

                    for (size_t iBin = nBinsperBX * 3534; iBin <= nBinsperBX * 3564; iBin++)
                    {
                        //if we are in the first iteration  we can't do much
                        if (iBin == 3534 * nBinsperBX)
                        {
                            sum_deque.push_back (m_data[iBin]);
                            running_average = m_data[iBin];
                            continue;
                        }

                        if (m_data[iBin] > 1.3 * running_average) m_data[iBin] = running_average;
                        else
                        {
                            sum_deque.push_back (m_data[iBin]);

                            while (sum_deque.size() > window_size) sum_deque.pop_front();

                            int sum = 0;

                            for (auto it : sum_deque)
                                sum += it;

                            running_average = (double) sum / (double) sum_deque.size();
                        }
                    }
                }
            }

            uint32_t integral()
            {
                uint32_t integral = 0;

                for (size_t iBin = 0; iBin < nBin; iBin++)
                    integral += static_cast<uint32_t> (m_data[iBin]);

                return integral;
            }

            DataT* data()
            {
                //return m_data;
                return m_data.data();
            }


            //Stream operator
            friend std::ostream& operator << (std::ostream& out, const Histogram<DataT, nBin>& aHist )
            {
                out << "Histogram with " << nBin << " Entries: " << std::endl;
                auto it = aHist.cbegin();

                while ( it != aHist.cend() - 1 )
                    out << *it++ << " ";

                out << *it << std::endl;
                return out;
            }

        }; //class Histogram

    } //namespace bcm1futcaprocessor
} //namespace bril

#endif
