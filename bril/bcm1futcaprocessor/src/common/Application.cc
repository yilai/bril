//TODO:
//remove message counter and output eventually
#include "bril/bcm1futcaprocessor/Application.h"

using namespace cgicc;

struct RefDestroyer  ////RAII exception safe guard object
{
    toolbox::mem::Reference* m_ref;
    RefDestroyer ( toolbox::mem::Reference* ref ) : m_ref ( ref ) {}
    ~RefDestroyer()
    {
        if ( m_ref ) m_ref->release();
    }
};

XDAQ_INSTANTIATOR_IMPL (bril::bcm1futcaprocessor::Application)

//C'tor
bril::bcm1futcaprocessor::Application::Application (xdaq::ApplicationStub* s ) :
    xdaq::Application (s),
    xgi::framework::UIManager (this),
    eventing::api::Member (this),
    m_algorithm (0),
    m_webCharts (false),
    m_useAlbedo (true),
    m_appLock (toolbox::BSem::FULL),
    m_incomingCounter(0),
    m_outgoingCounter(0),
    m_occQueue (500, 400),
    m_rawQueue (200, 180),
    m_occQueueLS (100, 80),
    m_ampQueueLS (100, 80),
    m_lumiAlgorithm (NULL),
    m_aggregateAlgorithm (NULL),
    m_amplitudeAlgorithm (NULL),
    m_backgroundAlgorithm (NULL),
    m_albedoAlgorithm (NULL)
{
    try
    {
        //bind default callback
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::Default, "Default");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::OverviewPage, "OverviewPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::ChannelPage, "ChannelPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::AlbedoPage, "AlbedoPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::InputDataPage, "InputDataPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::ConfigPage, "ConfigPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::MonitoringPage, "MonitoringPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::BeamPage, "BeamPage");
        //bind callback for b2in messages (eventing)
        b2in::nub::bind (this, &bril::bcm1futcaprocessor::Application::onMessage);
        //bind callback for webchars
        xgi::deferredbind ( this, this, &Application::requestRawData, "requestRawData" );
        xgi::deferredbind ( this, this, &Application::requestOverviewData, "requestOverviewData" );
        xgi::deferredbind ( this, this, &Application::requestChannelData, "requestChannelData" );
        xgi::deferredbind ( this, this, &Application::requestAlbedoData, "requestAlbedoData" );

        // setting shutdown handler to stop all workloops
        //toolbox::getRuntime()->addShutdownListener (this);

        //initialize all member variables
        m_appInfoSpace = this->getApplicationInfoSpace();
        m_appDescriptor = this->getApplicationDescriptor();
        m_className = m_appDescriptor->getClassName();
        m_appURL = m_appDescriptor->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
        m_logger = this->getApplicationLogger();

        //add a listener for xdata::Event::setDefault values
        m_appInfoSpace->addListener (this, "urn:xdaq-event:setDefaultValues");

        //now set the inputs from the config files
        m_appInfoSpace->fireItemAvailable ("eventinginput", &m_dataSources);
        m_appInfoSpace->fireItemAvailable ("eventingoutput", &m_dataSinks);
        m_appInfoSpace->fireItemAvailable ("channels", &m_channelConfig);
        m_appInfoSpace->fireItemAvailable ("lumialgorithm", &m_lumiProperties);
        m_appInfoSpace->fireItemAvailable ("aggregatealgorithm", &m_aggregateProperties);
        m_appInfoSpace->fireItemAvailable ("amplitudealgorithm", &m_amplitudeProperties);
        m_appInfoSpace->fireItemAvailable ("backgroundalgorithm", &m_backgroundProperties);
        m_appInfoSpace->fireItemAvailable ("albedoalgorithm", &m_albedoProperties);
        m_appInfoSpace->fireItemAvailable ("timeout", &m_timeout);
        m_appInfoSpace->fireItemAvailable ("expected_algorithm", &m_algorithm);
        m_appInfoSpace->fireItemAvailable ("webcharts", &m_webCharts);
        m_appInfoSpace->fireItemAvailable ("webchart_algo", &m_AlgoToPlot);
        m_appInfoSpace->fireItemAvailable ("useAlbedo", &m_useAlbedo);

        //memory Pools for outgoing messages
        m_memPoolFactory = toolbox::mem::getMemoryPoolFactory();
        //use the URN instead of the class name, this way it's unambiguous
        toolbox::net::URN t_memURN ("toolbox-mem-pool", m_appDescriptor->getURN() );
        m_memPool = m_memPoolFactory->createPool (t_memURN, new toolbox::mem::HeapAllocator() );

        m_beamMode = "";

        //set up the monitoring already now
        this->setupMonitoring();

        //GUI
        m_tab = Tab::OVERVIEW;

        //init the algorithm pointers to null
        m_aggregateAlgorithm = nullptr;
        m_backgroundAlgorithm = nullptr;
        m_lumiAlgorithm = nullptr;
        m_albedoAlgorithm = nullptr;
    }
    catch (xcept::Exception& e)
    {
        std::stringstream msg;
        msg << RED << " : Caught Exception " << RESET;
        LOG4CPLUS_FATAL (m_logger, __func__ << msg.str() << e.what() );
        //notify_qualified ("error", e);
        throw; // we don't want to continue in case we already have an exception in the constructor
    }

    LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Finished constructor, point your browser to " << BLUE << m_appURL << RESET);
}

//D'tor
bril::bcm1futcaprocessor::Application::~Application()
{
    //Never called when Ctrl-C The code is moved to actionPerformed on toolbox::EventShutdown
}

void bril::bcm1futcaprocessor::Application::Default (xgi::Input* in, xgi::Output* out)
{
    this->ConfigPage (in, out);
}

// we only care for the setDefaultValues event here
void bril::bcm1futcaprocessor::Application::actionPerformed (xdata::Event& e)
{
    LOG4CPLUS_DEBUG (m_logger, __func__ << "Received xdata::Event: " << e.type() );

    if ( e.type() == "urn:xdaq-event:setDefaultValues" )
    {
        try
        {
            //set the list of good channels and algorithms
            this->setupChannels();

            //set up the eventing: subscribe to topics and add action listeners for output buses
            this->setupEventing();

            //set up the webcharts
            if (m_webCharts)
                this->setupCharts();

            //set up the algorithm
            this->setupAlgorithms();

        }
        catch ( xcept::Exception& e )
        {
            std::stringstream msg;
            msg << RED << "Exception on setDefaultValues: " << e.what() << RESET;
            LOG4CPLUS_ERROR ( getApplicationLogger(), msg.str() );
            notifyQualified ( "error", e );
        }

        try
        {
            toolbox::TimeInterval checkinterval (10, 0); // check every 10 seconds
            std::string timername ("stalecachecheck_timer");
            toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer (timername );
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate (start, this, checkinterval, 0, timername);
        }
        catch (toolbox::exception::Exception& e)
        {
            std::stringstream msg;
            msg << RED << "failed to start timer :" << e.what() << RESET;
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str()  );
            XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::SetupException, msg.str(), e);
        } //end of set default values xdata::Event type
    }

    else if ( e.type() == "ItemChangedEvent" )
    {
        std::string item = static_cast<xdata::ItemEvent&> ( e ).itemName();
        LOG4CPLUS_INFO ( getApplicationLogger(), BOLDBLUE << __func__ << ": Parameter " << item << " changed."  << RESET);
        //do something here if necessary!
    }
}

//llisten for toolbox::Event which is the case when buses are ready to publish
//so start the publishing workloop here
void bril::bcm1futcaprocessor::Application::actionPerformed (toolbox::Event& e)
{
    LOG4CPLUS_DEBUG (m_logger, __func__ << "Received xdata::Event: " << e.type() );

    if (e.type() == "eventing::api::BusReadyToPublish")
    {
        try
        {
            std::string t_busName = (static_cast<eventing::api::Bus*> (e.originator() ) )->getBusName();
            LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Bus: " << t_busName << " is ready to publish!" << RESET);
            m_unreadyBuses.erase (t_busName);

            if (!m_unreadyBuses.empty() ) return;

            LOG4CPLUS_INFO (m_logger, BOLDGREEN << std::endl << "All output buses ready to publish!" << RESET);

            //start the workloops
            this->setupWorkloops();
        }
        catch (xcept::Exception& e)
        {
            std::stringstream msg;
            msg << RED << " : Caught Exception on toolbox::Event::BusReadyToPublish" << RESET;
            LOG4CPLUS_ERROR (m_logger, __func__ << msg.str() << e.what() );
            XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::EventingException, msg.str(), e);
        }
    }

    if (e.type() == "Shutdown")
    {
        //first, clean up
        if (m_lumiAlgorithm != nullptr) delete m_lumiAlgorithm;

        if (m_backgroundAlgorithm != nullptr) delete m_backgroundAlgorithm;

        if (m_albedoAlgorithm != nullptr) delete m_albedoAlgorithm;

        if (m_aggregateAlgorithm != nullptr) delete m_aggregateAlgorithm;

        //shutdown all workloops
        try
        {
            this->m_occWorkloop->cancel();
            this->m_ampWorkloop->cancel();
            this->m_rawWorkloop->cancel();
            this->m_publishWorkloop->cancel();
            this->m_albedoWorkloop->cancel();
        }
        catch (std::exception&) {}

        LOG4CPLUS_INFO (m_logger, GREEN << "Exited gracefully!" << RESET);
    }
}

//calback if message is received on the eventing bus
void bril::bcm1futcaprocessor::Application::onMessage (toolbox::mem::Reference* ref, xdata::Properties& plist)
{
    //make sure we release this reference once done with it
    toolbox::mem::AutoReference t_refGuard (ref);

    std::string t_action = plist.getProperty ("urn:b2in-eventing:action");

    //if action not notify or if reference is 0
    if (t_action != "notify" )
        return;

    if ( ref == 0)
        return;

    std::string t_topic = plist.getProperty ("urn:b2in-eventing:topic");

    //check version header in message
    std::string t_version = plist.getProperty ("DATA_VERSION");

    if (t_version.empty()  || t_version != interface::bril::shared::DATA_VERSION)
    {
        LOG4CPLUS_ERROR (m_logger, __func__ << RED << "Received message on topic " << t_topic << " without or with wrong version header, doing nothing!" << RESET);
        return;
    }
    else
    {
    	try
    	{
		//increment the incoming counter
		m_incomingCounter++;

    	        //get the input header so I can save the last location
    	        interface::bril::shared::DatumHead* t_inHeader = (interface::bril::shared::DatumHead*) (ref->getDataLocation() );

    	        if (t_topic== interface::bril::bcm1futcaocchistT::topicname() )
    	        {
    	            m_occCache.add (ref, t_topic);
    	            m_lastOccHeader = *t_inHeader;
    	        }

    	        if (t_topic == interface::bril::bcm1futcaamphistT::topicname() )
    	        {
    	            m_ampCache.add (ref, t_topic);
    	            m_lastAmpHeader = *t_inHeader;
    	        }

    	        if (t_topic == interface::bril::bcm1futcarawdataT::topicname() )
    	        {
    	            m_rawCache.add (ref, t_topic);
    	            m_lastRawHeader = *t_inHeader;
    	        }

    	        if (t_topic == interface::bril::beamT::topicname() )
    	            this->onMessageTopicBeam (ref);

    	        if (t_topic == interface::bril::vdmflagT::topicname() )
    	            this->onMessageTopicVdmFlag (ref);

    	        if (t_topic == interface::bril::vdmscanT::topicname() )
    	            this->onMessageTopicVdmScan (ref);

    	        //ref->release();

    	    	this->enqueueComplete();
    	}
    	catch ( xcept::Exception& e )
    	{
    	    LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " << e.what() << RESET );
    	    notifyQualified ( "error", e );
    	}
    }
}

void bril::bcm1futcaprocessor::Application::timeExpired (toolbox::task::TimerEvent& e)
{
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    unsigned int nowsec = t.sec();
    std::stringstream msg;

    std::stringstream occLog;

    if (this->is_subscribed (interface::bril::bcm1futcaocchistT::topicname() ) )
    {
        if (nowsec - m_occCache.getLastUpdate() > m_timeout)
        {
            msg << YELLOW << " No addition to occupancy Cache in last " << m_timeout << " seconds - runnumber: " << m_lastOccHeader.runnum << " lumisection: " << m_lastOccHeader.lsnum << " nibble: " << m_lastOccHeader.nbnum << RESET << std::endl;
        }
    }

    if (this->is_subscribed (interface::bril::bcm1futcaamphistT::topicname() ) )
    {
        if (nowsec - m_ampCache.getLastUpdate() > m_timeout)
        {
            msg << YELLOW << " No addition to amplitude Cache in last " << m_timeout << " seconds - runnumber: " << m_lastAmpHeader.runnum << " lumisection: " << m_lastAmpHeader.lsnum << " nibble: " << m_lastAmpHeader.nbnum << RESET << std::endl;
        }
    }

    if (this->is_subscribed (interface::bril::bcm1futcarawdataT::topicname() ) )
    {
        if (nowsec - m_rawCache.getLastUpdate() > m_timeout )
        {
            msg << YELLOW << " No addition to raw data Cache in last " << m_timeout << " seconds - runnumber: " << m_lastRawHeader.runnum << " lumisection: " << m_lastRawHeader.lsnum << " nibble: " << m_lastRawHeader.nbnum << RESET << std::endl;
        }
    }

    //let's have a look what's going on with our toolbox::mem::Pool
    //toolbox::mem::Usage use = m_memPool->getMemoryUsage();
    //size_t committed, used, max, allocated, cached;
    msg << "DEBUG: toolbox::mem::MemPool using " << std::endl;
	msg << "committed: "<<  m_memPool->getMemoryUsage().getCommitted() << " B" << std::endl; 
	msg << "used: "<<  m_memPool->getMemoryUsage().getUsed() << " B" << std::endl; 
	msg << "max: "<<  m_memPool->getMemoryUsage().getMax() << " B" << std::endl; 
	//msg << "allocated blocks: " << m_memPool->getMemoryUsage().getAllocatedBlocks() << std::endl; 
	//msg << "cached blocks: " << m_memPool->getMemoryUsage().getCachedBlocks() << std::endl; 
	msg << std::endl;

    //queue size:
    msg << "Publish queue size: " << m_publishQueue.elements() << std::endl;
    msg << "Occupancy NB4 queue size: " << m_occQueue.elements() << std::endl;
    msg << "Occupancy LS queue size: " << m_occQueueLS.elements() << std::endl;
    msg << "Amplitude LS queue size: " << m_ampQueueLS.elements() << std::endl;

    //print how many messages were received and published in the last timeout interval
    auto incoming = m_incomingCounter.exchange(0);
    auto outgoing = m_outgoingCounter.exchange(0);

    msg << BOLDBLUE << "In the last " << RED << m_timeout << BOLDBLUE << " seconds I " << std::endl;
    msg << "received " << incoming << " Messages" << std::endl;
    msg << "published " << outgoing << " Messages" << RESET << std::endl;

    //printCacheStatus();
    //unsigned int last_occ_hist = nowsec - m_occCache.getLastUpdate();
    //unsigned int last_amp_hist = nowsec - m_ampCache.getLastUpdate();

    //if (last_occ_hist > 20) std::cout << "Last occupancy histogram: " << last_occ_hist << std::endl;

    //if (last_amp_hist > 20) std::cout << "Last amplitude histogram: " << last_occ_hist << std::endl;

    //std::cout << "Last raw histogram: " << nowsec - m_rawCache.getLastUpdate() << std::endl;
    if (!msg.str().empty() )
        LOG4CPLUS_DEBUG (m_logger, msg.str() );

    //because it can't hurt
    malloc_trim(0);

}

////////////////////////////////////////////////////////////////////////////////
//PRIVATE METHODS
////////////////////////////////////////////////////////////////////////////////

void bril::bcm1futcaprocessor::Application::setupChannels()
{
    std::stringstream msg;
    m_channelInfo.setupChannels (m_channelConfig, m_algorithm, msg);

    if (!msg.str().empty() )
        LOG4CPLUS_INFO (m_logger, msg.str() );

    //Now I need to actually construct the Cache objects - since these are unique managers, I need to std::move
    m_occCache = OccupancyCache(m_channelInfo.getChannelVector(), m_algorithm, true); 
    m_ampCache = AmplitudeCache(m_channelInfo.getChannelVector(), m_algorithm, true); 
    m_rawCache = RawdataCache(m_channelInfo.getChannelVector(), m_algorithm, false); 

    // fill the total channel rates with the correct size and initialize to 0
    m_monVariables.m_totalChannelRate.resize (interface::bril::BCM1FUTCA_NCHANNELS,  0.);
    // amplitude analysis fields too
    m_monVariables.m_monMPV.resize (interface::bril::BCM1FUTCA_NCHANNELS,  0.);
    m_monVariables.m_monTP.resize (interface::bril::BCM1FUTCA_NCHANNELS,  0.);
    m_monVariables.m_monTotal.resize (interface::bril::BCM1FUTCA_NCHANNELS,  0.);
    m_monVariables.m_monMinBin.resize (interface::bril::BCM1FUTCA_NCHANNELS,  0.);
// ; ; ;  

}

void bril::bcm1futcaprocessor::Application::setupEventing()
{
    try
    {
        //first the input
        for (size_t source = 0; source < m_dataSources.elements(); source++)
        {
            std::string t_bus = m_dataSources[source].getProperty ("bus");
            std::string t_topics = m_dataSources[source].getProperty ("topics");
            //separate the topic list into unique elements
            const auto  & t_topicNames = toolbox::parseTokenSet (t_topics, ",");

            for (const auto & topic : t_topicNames)
            {
                LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Subscribing to " << BLUE << topic << GREEN << " on bus " << BLUE << t_bus << RESET);
                this->getEventingBus (t_bus).subscribe (topic);

                if (m_inputTopics.find (topic) == m_inputTopics.end() )
                    m_inputTopics.insert (topic);
            }
        }
    }
    catch (eventing::api::exception::Exception& e)
    {
        std::stringstream msg;
        msg << RED << " : Failed to subscribe to one or more topics! - " << RESET;
        LOG4CPLUS_ERROR (m_logger, __func__ << msg.str() << e.what() );
        //topic = t_topicNames.erase (topic);
        //raise my exception
        //XCEPT_RAISE (bril::bcm1futcaprocessor::exception::EventingException, msg.str() );
        //rethrow as my type
        //XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::EventingException, msg.str(), dynamic_cast<bril::bcm1futcaprocessor::EventingException&>(e).exp_);

        //do not throw but notify error collector
        XCEPT_DECLARE ( bril::bcm1futcaprocessor::exception::EventingException, myerrorobj, msg.str() );
        this->notifyQualified ("error", myerrorobj);
    }

    //now lets handle the output topics
    m_outTopicMap.clear();
    m_unreadyBuses.clear();

    for (size_t sink = 0; sink < m_dataSinks.elements(); sink++)
    {
        std::string t_topic = m_dataSinks[sink].getProperty ("topic");
        std::string t_buses = m_dataSinks[sink].getProperty ("buses");
        BusSet t_set = toolbox::parseTokenSet (t_buses, ",");
        m_outTopicMap[t_topic] = t_set;
        m_unreadyBuses.insert (t_set.begin(), t_set.end() );
    }

    //now parsed the configuration, let's wait for the buses
    for (auto bus = m_unreadyBuses.begin(); bus != m_unreadyBuses.end(); bus++)
    {
        try
        {
            LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Waiting for bus: " << BLUE << *bus << RED << " to be ready!" << RESET);
            getEventingBus (*bus).addActionListener (this);
        }
        catch (eventing::api::exception::Exception& e)
        {
            std::stringstream msg;
            msg << RED << " : Failed to add action listener on bus " << BLUE << *bus << RESET << " ";
            LOG4CPLUS_ERROR (m_logger, __func__ << msg.str() << e.what() );
            //raise my exception
            //XCEPT_RAISE (bril::bcm1futcaprocessor::exception::EventingException, msg.str() );
            //rethrow as my type
            //XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::EventingException, msg.str(), dynamic_cast<bril::bcm1futcaprocessor::EventingException&>(e).exp_);

            //do not throw but notify error collector
            XCEPT_DECLARE ( bril::bcm1futcaprocessor::exception::EventingException, myerrorobj, msg.str() );
            this->notifyQualified ("error", myerrorobj);
        }
    }
}

void bril::bcm1futcaprocessor::Application::setupWorkloops()
{
    //now the processing workloops for the histograms
    if (this->is_subscribed (interface::bril::bcm1futcaocchistT::topicname() ) )
    {
        this->m_occWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processOccupancy", "waiting" );
        toolbox::task::ActionSignature* as_processingOcc = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::occProcessJob, "processOccupancy" );
        this->m_occWorkloop->activate();
        this->m_occWorkloop->submit ( as_processingOcc );
        LOG4CPLUS_INFO (m_logger, BOLDYELLOW << std::endl << "Starting occupancy processing workloop!" << RESET);
    }

    if (this->is_subscribed (interface::bril::bcm1futcaamphistT::topicname() ) )
    {
        this->m_ampWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processAmplitude", "waiting" );
        toolbox::task::ActionSignature* as_processingAmp = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::ampProcessJob, "processAmplitude" );
        this->m_ampWorkloop->activate();
        this->m_ampWorkloop->submit ( as_processingAmp );
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << std::endl << "Starting amplitude processing workloop!" << RESET);
    }

    if (this->is_subscribed (interface::bril::bcm1futcarawdataT::topicname() ) )
    {
        this->m_rawWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processRawData", "waiting" );
        toolbox::task::ActionSignature* as_processingRaw = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::rawProcessJob, "processRawData" );
        this->m_rawWorkloop->activate();
        this->m_rawWorkloop->submit ( as_processingRaw );
        LOG4CPLUS_INFO (m_logger, BOLDMAGENTA << std::endl << "Starting raw data processing workloop!" << RESET);
    }

    //and finally the publishing workloop
    this->m_publishWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_publishHistograms", "waiting" );
    toolbox::task::ActionSignature* as_publishing = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::publishJob, "publishHistograms" );
    this->m_publishWorkloop->activate();
    this->m_publishWorkloop->submit ( as_publishing );
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << std::endl << "Starting publishing workloop!" << RESET);

    //the albedo workloop
    if (this->m_useAlbedo)
    {
        this->m_albedoWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_computeAlbedo", "waiting" );
        toolbox::task::ActionSignature* as_computingAlbedo = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::albedoJob, "computeAlbedo" );
        this->m_albedoWorkloop->activate();
        this->m_albedoWorkloop->submit ( as_computingAlbedo );
        LOG4CPLUS_INFO (m_logger, BOLDBLUE << std::endl << "Starting albedo workloop!" << RESET);
    }
}

void bril::bcm1futcaprocessor::Application::onMessageTopicBeam ( toolbox::mem::Reference* ref)
{
    //LOG4CPLUS_INFO (m_logger, GREEN << "Received message on topic beam!" << RESET);
    //from here on need to protect the beamInfo object with a BSEM
    m_appLock.take();
    m_beamInfo.update (ref);

    if (m_beamMode != m_beamInfo.getBeamMode() )
    {
        m_beamMode = m_beamInfo.getBeamMode();
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << "Beam Mode changed to " << m_beamMode << RESET);
    }

    std::stringstream msg;
    m_beamInfo.printStatus (msg);
    m_appLock.give();
    LOG4CPLUS_DEBUG (m_logger, msg.str() );

}

void bril::bcm1futcaprocessor::Application::onMessageTopicVdmFlag ( toolbox::mem::Reference* ref )
{
    m_appLock.take();
    bool t_vdmFlag = m_beamInfo.m_vdmFlag;

    m_beamInfo.updateVdmFlag (ref);

    if (t_vdmFlag != m_beamInfo.m_vdmFlag)
    {
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << "VDM Flag changed to " << m_beamInfo.m_vdmFlag << RESET);
    }

    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::onMessageTopicVdmScan ( toolbox::mem::Reference* ref )
{
    m_appLock.take();
    m_beamInfo.updateVdmIP (ref);

    if (m_beamInfo.m_vdmIP5)
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << "VDM IP changed to IP 5!" << RESET);

    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::printCacheStatus()
{
    std::stringstream msg;
    msg << std::endl << BOLDYELLOW << "Occupancy Cache Status: " << RESET << std::endl;
    m_occCache.print (msg);
    msg << BOLDYELLOW << "Occupancy Cache Status[LS]: " << RESET << std::endl;
    m_occCache.printLS (msg);

    msg << BOLDCYAN << "Amplitude Cache Status: " << RESET << std::endl;
    m_ampCache.print (msg);
    msg << BOLDCYAN << "Amplitude Cache Status[LS]: " << RESET << std::endl;
    m_ampCache.printLS (msg);

    //msg << BOLDMAGENTA << "RawData Cache Status: " << RESET << std::endl;
    //m_rawCache.print (msg);
    LOG4CPLUS_INFO (m_logger, __func__ << msg.str() );
}

void bril::bcm1futcaprocessor::Application::enqueueComplete()
{
    unsigned int timeout_sec_nb4 = 5;
    unsigned int timeout_sec_LS = 60;
    std::stringstream msg;
    std::stringstream occLog;

    OccupancyHistogramVector t_occVec;
    m_occCache.get_completedNB4 (t_occVec,timeout_sec_nb4, occLog);

    OccupancyHistogramVectorLS t_occVecLS;
    m_occCache.get_completedLS (t_occVecLS, timeout_sec_LS, msg);

    if (!occLog.str().empty() )
    {
        LOG4CPLUS_INFO (m_logger, "Enqueue complete: " << occLog.str() );
        m_missingHistogramLog += occLog.str();
        occLog.str ("");
    }

    if(t_occVec.size())
    {
	for(auto& element : t_occVec)
	{
	    OccupancyHistogramBuffer* ptr = new OccupancyHistogramBuffer(std::move(element));
	    m_occQueue.push(ptr);
	}
	t_occVec.clear();
    }


    if (t_occVecLS.size() )
    {
	for(auto& element : t_occVecLS)
	{
	    OccupancyHistogramBufferLS* ptr = new OccupancyHistogramBufferLS(std::move(element));
	    m_occQueueLS.push(ptr);
	}
	t_occVecLS.clear();
    }

    AmplitudeHistogramVector t_ampVec;
    this->m_ampCache.get_completedNB4 (t_ampVec, timeout_sec_nb4, msg);
    t_ampVec.clear();

    AmplitudeHistogramVectorLS t_ampVecLS;
    this->m_ampCache.get_completedLS (t_ampVecLS, timeout_sec_LS, msg);

    if (t_ampVecLS.size() )
    {
	for(auto& element : t_ampVecLS)
	{
	    AmplitudeHistogramBufferLS* ptr = new AmplitudeHistogramBufferLS(std::move(element));
	    m_ampQueueLS.push(ptr);
	}
	t_ampVecLS.clear();
    }


    RawHistogramVector t_rawVec;
    this->m_rawCache.get_completedNB4 (t_rawVec, timeout_sec_nb4, msg);

    //this should just go out of scope
    RawHistogramVectorLS t_rawVecLS; 
    this->m_rawCache.get_completedLS (t_rawVecLS, timeout_sec_LS, msg);
    t_rawVecLS.clear();

    if (t_rawVec.size() )
    {
	for(auto& element : t_rawVec)
	{
	    RawHistogramBuffer* ptr = new RawHistogramBuffer(std::move(element));
	    m_rawQueue.push(ptr);
	}
	t_rawVec.clear();
    }

    if (msg.str().size() )
        LOG4CPLUS_INFO (m_logger, msg.str() );

    msg.str ("");
    msg << std::endl;
}

void bril::bcm1futcaprocessor::Application::setupCharts()
{
    //Vector with Colors
    std::vector<std::string> t_colors = {"#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                                         "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                                         "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                                         "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                                         "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                                         "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                                         "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                                         "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

                                         "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                                         "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                                         "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                                         "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                                         "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
                                         "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
                                         "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
                                         "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379"
                                        };

    std::vector<std::string> t_colors_overview = {"#e6194b", "#3cb44b", "#ffe119", "#0082c8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#d2f53c", "#fabebe", "#800000", "#008080", "#000000"};

    //property map for per channel data
    bril::webutils::WebChart::property_map t_ChartMapChannel;
    bril::webutils::WebChart::property_map t_ChartMapAlbedo;

    std::string callbackRaw = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestRawData";
    std::string callbackOverview = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestOverviewData";
    std::string callbackChannel = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestChannelData";
    std::string callbackAlbedo = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestAlbedoData";

    for (size_t chan = 0; chan < m_channelInfo.getChannelVector().size(); chan++)
    {
        size_t color = chan;
        size_t color_uncorr = chan * 2;
        size_t color_corr = chan * 2 + 1;
        std::stringstream ss;
        ss << "Channel_" << m_channelInfo.getChannelVector().at (chan);
        t_ChartMapChannel[ss.str()] = bril::webutils::WebChart::series_properties (t_colors.at (color) );
        std::stringstream albedo_corr;
        albedo_corr << "Channel_" << m_channelInfo.getChannelVector().at (chan) << "_corrected";
        std::stringstream albedo_uncorr;
        albedo_uncorr << "Channel_" << m_channelInfo.getChannelVector().at (chan) << "_uncorrected";
        t_ChartMapAlbedo[albedo_corr.str()] = bril::webutils::WebChart::series_properties (t_colors.at (color_corr) );
        t_ChartMapAlbedo[albedo_uncorr.str()] = bril::webutils::WebChart::series_properties (t_colors.at (color_uncorr) );
    }

    //InputCharts
    m_occupancyChart = new bril::webutils::WebChart ("chart_occupancy",
            callbackRaw,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Occupancy Data [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX*6]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_amplitudeChart= new bril::webutils::WebChart ("chart_amplitude",
            callbackRaw,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Amplitude Data [LS]' }, tooltip: { enable : false }, xAxis: {title: {text: 'ADC', useHTML: true}}, yAxis: { min: 0, title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_rawChart= new bril::webutils::WebChart ("chart_rawdata",
            callbackRaw,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Raw Data [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX*6]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    //Agghist, Colhist and Bkghist
    m_agghistChart= new bril::webutils::WebChart ("chart_agghist",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Agghist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_colhistChart= new bril::webutils::WebChart ("chart_colhist",
            callbackChannel,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Colhist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: { min: 0, title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_bkghistChart= new bril::webutils::WebChart ("chart_bkghst",
            callbackChannel,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Bkgd Hist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    //property map for Lumi data
    bril::webutils::WebChart::property_map t_ChartMapLumi;
    t_ChartMapLumi["lumi"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (0) );
    t_ChartMapLumi["lumi_raw"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (1) );

    //Lumi Charts
    m_lumiBXChart= new bril::webutils::WebChart ("chart_lumiBX",
            callbackOverview,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Luminosity per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Lumi', useHTML: true } }", //other options
            t_ChartMapLumi);

    m_lumiChart= new bril::webutils::WebChart ( "chart_lumi",
            callbackOverview,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Luminosity' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Luminosity [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapLumi );

    // per channel lumi
    m_lumiBXChannelChart= new bril::webutils::WebChart ("chart_lumiBXChannel",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Luminosity per Channel per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Lumi', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_lumiChannelChart= new bril::webutils::WebChart ( "chart_lumiChannel",
            callbackChannel,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Luminosity per Channel' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Luminosity [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapChannel );

    // raw lumi
    m_rawlumiBXChannelChart= new bril::webutils::WebChart ("chart_rawlumiBXChannel",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Mu per Channel per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Mu', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_rawlumiChannelChart= new bril::webutils::WebChart ( "chart_rawlumiChannel",
            callbackChannel,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Mu per Channel' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Mu [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapChannel );

    //property map for BKG data
    bril::webutils::WebChart::property_map t_ChartMapBkg;
    t_ChartMapBkg["bkg1"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (0) );
    t_ChartMapBkg["bkg2"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (1) );
    t_ChartMapBkg["bkg1_nc"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (2) );
    t_ChartMapBkg["bkg2_nc"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (3) );

    bril::webutils::WebChart::property_map t_ChartMapBkgBX;
    t_ChartMapBkgBX["bkg1"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (0) );
    t_ChartMapBkgBX["bkg2"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (1) );

    //BKG Charts
    m_bkgBXChart= new bril::webutils::WebChart ("chart_bkgBX",
            callbackOverview,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Background per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'BKG', useHTML: true } }", //other options
            t_ChartMapBkgBX);
    m_bkgChart= new bril::webutils::WebChart ( "chart_bkg",
            callbackOverview,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Background' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Background [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapBkg );

    //albedo charts
    m_albedoChart= new bril::webutils::WebChart ( "chart_albedo",
            callbackAlbedo,
            "type: 'line', zoomType: 'xy', animation: false",
            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
            "title: { text: 'Albedo Queue' }, tooltip: { enable : false }", t_ChartMapAlbedo );
    m_fractionChart= new bril::webutils::WebChart ( "chart_fraction",
            callbackAlbedo,
            "type: 'line', zoomType: 'xy', animation: false",
            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
            "title: { text: 'Albedo Fraction' }, tooltip: { enable : false }", t_ChartMapChannel );

}

void bril::bcm1futcaprocessor::Application::setupAlgorithms()
{
    if (m_aggregateAlgorithm != NULL)
        delete m_aggregateAlgorithm;

    m_aggregateAlgorithm = AlgorithmFactory::aggregator (m_aggregateProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Aggrgator Algorithm " << RESET);

    if (m_backgroundAlgorithm)
        delete m_backgroundAlgorithm;

    m_backgroundAlgorithm = AlgorithmFactory::background (m_backgroundProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Background Algorithm " << RESET);

    if (m_lumiAlgorithm)
        delete m_lumiAlgorithm;

    m_lumiAlgorithm = AlgorithmFactory::lumi (m_lumiProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Luminosity Algorithm " << RESET);

    if (m_albedoAlgorithm)
        delete m_albedoAlgorithm;

    if (m_useAlbedo)
    {
        m_albedoAlgorithm = AlgorithmFactory::albedo (m_albedoProperties, &m_channelInfo);
        LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Albedo Algorithm " << RESET);
    }
    else
        LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Running without Albedo Algorithm " << RESET);

    if (m_amplitudeAlgorithm != NULL)
        delete m_amplitudeAlgorithm;

    m_amplitudeAlgorithm = AlgorithmFactory::amplitude (m_amplitudeProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Amplitude Algorithm " << RESET);


}

void bril::bcm1futcaprocessor::Application::setupMonitoring()
{
    //LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
    std::string monurn = createQualifiedInfoSpace ( "bcm1futcaprocessorMon" ).toString();
    m_monInfospace = xdata::getInfoSpaceFactory()->get ( monurn );
    const int nvars = 14;
    const char* names[nvars] = { "beammode", "fill", "run", "ls", "nb", "timestamp", "background1", "background2", "luminosity", "channelrates", "MPV", "testpulse", "totalhits", "minbin" };
    xdata::Serializable* vars[nvars] = { &m_monVariables.m_monBeammode, &m_monVariables.m_monFill, &m_monVariables.m_monRun, &m_monVariables.m_monLs, &m_monVariables.m_monNb, &m_monVariables.m_monTimestamp, &m_monVariables.m_bg1Plus, &m_monVariables.m_bg2Minus, &m_monVariables.m_lumiAvg, &m_monVariables.m_totalChannelRate, &m_monVariables.m_monMPV, &m_monVariables.m_monTP, &m_monVariables.m_monTotal, &m_monVariables.m_monMinBin };

    for ( int i = 0; i != nvars; ++i )
    {
        m_monVariables.m_monVarlist.push_back ( names[i] );
        m_monVariables.m_monVars.push_back ( vars[i] );
        m_monInfospace->fireItemAvailable ( names[i], vars[i] );
    }
}

////////////////////////////////////////////////////////////////////////////////
//WORKLOOPS
////////////////////////////////////////////////////////////////////////////////

bool bril::bcm1futcaprocessor::Application::occProcessJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //first, pop whatever is in the queue
        if (!m_occQueue.empty() )
        {
            OccupancyHistogramBuffer* ptr = m_occQueue.pop();
	    if(ptr == nullptr) std::cout << "ERROR, popped nullptr" << std::endl;
	    else
	    {
            	this->doTimeAlignment < uint16_t, s_nBinOcc > (ptr);

            	if (m_webCharts)
                	this->fillChartData<uint16_t, s_nBinOcc> (m_occupancyChart, m_occ_data, ptr);

            	m_lumiAlgorithm->compute (ptr, m_beamInfo, m_publishQueue, m_albedoQueue, m_monVariables, m_useAlbedo);
            	m_aggregateAlgorithm->compute (ptr, m_beamInfo, m_publishQueue, m_monVariables);
            	m_backgroundAlgorithm->compute (ptr, m_beamInfo, m_publishQueue, m_monVariables);
            	m_monInfospace->fireItemGroupChanged (m_monVariables.m_monVarlist, this);

	    	// now need to be SURE to manually delete pointer
  	   }
	   delete ptr;
        }

        //now check if I have to process an aggregated LS histogram
        if (!m_occQueueLS.empty() )
        {
            OccupancyHistogramBufferLS* ptr_LS = m_occQueueLS.pop();
	    if(ptr_LS  == nullptr) std::cout << "Error: popped nullptr LS" << std::endl;
	    else
	    {
            	this->doTimeAlignment (ptr_LS);
           	m_aggregateAlgorithm->compute (ptr_LS, m_beamInfo, m_publishQueue, m_monVariables);

                LOG4CPLUS_DEBUG (m_logger, BOLDBLUE << __func__ << " occupancy workloop popped a OccupancyHistogramBufferLS " << " from occupancy queue" << RESET);
            }
 	    delete ptr_LS;
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " << e.what() << RESET );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

bool bril::bcm1futcaprocessor::Application::ampProcessJob (toolbox::task::WorkLoop* wl)
{
    try
    {

        //now check if I have to process an aggregated LS histogram
        if (m_ampQueueLS.elements() )
        {
            AmplitudeHistogramBufferLS* ptr = m_ampQueueLS.pop();
	    if(ptr == nullptr)  std::cout << "ERROR, popping nullptr" << std::endl;
	    else
	    {
 
            	if (m_webCharts)
            	  	this->fillChartData<uint32_t, s_nBinAmp> (m_amplitudeChart, m_amp_data, ptr);

            	m_amplitudeAlgorithm->compute (ptr, m_beamInfo, m_publishQueue, m_monVariables);

                LOG4CPLUS_DEBUG (m_logger, BOLDBLUE << __func__ << " amplitude workloop popped a OccupancyHistogramBufferLS " << " from amplitude queue" << RESET);

	    }
	    delete ptr;
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

bool bril::bcm1futcaprocessor::Application::rawProcessJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //first, pop whatever is in the queue

        if (!m_rawQueue.empty() )
        {
            RawHistogramBuffer* ptr = m_rawQueue.pop();
	    if(ptr == nullptr)  std::cout << "ERROR, popping nullptr" << std::endl;
	    else
	    {
            	if (m_webCharts)
            	   	this->fillChartData<uint8_t, s_nBinRaw> (m_rawChart, m_raw_data, ptr);
	    }
 	    delete ptr;
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

bool bril::bcm1futcaprocessor::Application::publishJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        while (!m_publishQueue.empty() )
            //if (m_publishQueue.elements() )
        {
            std::pair<std::string, toolbox::mem::Reference*> t_pair = std::move(m_publishQueue.pop());
	    //I put a refGuard on t_pair second here so I can be sure it gets released when the guard goes out of scope at this->publish
	    //this->publish internally duplicates the reference to publish it and releases it if publish fails
	    //so this should be safe
	    toolbox::mem::AutoReference t_refGuard (t_pair.second);

            if (m_webCharts)
            {
                if (t_pair.first.find (interface::bril::bcm1futcalumiT::topicname() ) != std::string::npos   ||
                    //t_pair.first.find (interface::bril::bcm1futca_pcvdlumiT::topicname() ) != std::string::npos ||
                    t_pair.first.find (interface::bril::bcm1futca_silumiT::topicname() ) != std::string::npos)
                    this->fillChartDataLumi (t_pair);

                else if (t_pair.first == interface::bril::bcm1futca_backgroundT::topicname() )
                    this->fillChartDataBKG (t_pair);

                else if (t_pair.first == interface::bril::bcm1futca_agg_histT::topicname() ||
                         t_pair.first == interface::bril::bcm1futca_col_histT::topicname() ||
                         t_pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
                    this->fillChartDataHist (t_pair);
            }

            //now still need to publish
            //std::cout << BLUE << "Publishing topic: " << t_pair.first << RESET << std::endl;
            this->publish (t_pair);
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

void bril::bcm1futcaprocessor::Application::publish (std::pair<std::string, toolbox::mem::Reference*>& pair)
{
    std::string payloaddict;

    if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_agg_histT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_col_histT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_bkg_histT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_bkg12T::topicname() )
        payloaddict = interface::bril::bcm1futca_bkg12T::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_backgroundT::topicname() )
        payloaddict = interface::bril::bcm1futca_backgroundT::payloaddict();

    else if (pair.first == interface::bril::bcm1futcalumiT::topicname() )
        payloaddict = interface::bril::bcm1futcalumiT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_occ_section_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_occ_section_histT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_amp_analysisT::topicname() )
        payloaddict = interface::bril::bcm1futca_amp_analysisT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_amp_section_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_amp_section_histT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_baseline_tp_infoT::topicname() )
        payloaddict = interface::bril::bcm1futca_baseline_tp_infoT::payloaddict();

    else if (pair.first == interface::bril::bcm1futca_ADC_peaksT::topicname() )
        payloaddict = interface::bril::bcm1futca_ADC_peaksT::payloaddict();

    //this is needed for the per channel publication or if the algo ID is appended to the topicname for overall lumi
    //if the algo ID is appended to the channel lumi this does not make a differecne as it is the same payloaddict
    else if (pair.first.find ("bcm1futcalumi") != std::string::npos)
        payloaddict = interface::bril::bcm1futcalumiT::payloaddict();

    xdata::Properties plist;
    const std::string dv = interface::bril::shared::DATA_VERSION;
    plist.setProperty ("DATA_VERSION", dv);

    if (!payloaddict.empty() )
        plist.setProperty ("PAYLOAD_DICT", payloaddict);

    std::string t_beamMode = m_beamInfo.getBeamMode();

    if (t_beamMode == "SETUP" || t_beamMode == "ABORT" || t_beamMode == "BEAM DUMP" || t_beamMode == "RAMP DOWN" || t_beamMode == "CYCLING" || t_beamMode == "RECOVERY" || t_beamMode == "NO BEAM")
        plist.setProperty ("NOSTORE", "1");

    //this is a workaround needed to send per channel data - if the topicname is bcm1futcalumiX where
    //X is the channel number, we need to treat it as standard lumi topic from the xml
    std::string t_topicname = pair.first;

    if (pair.first.find (interface::bril::bcm1futcalumiT::topicname() ) != std::string::npos)
        t_topicname = interface::bril::bcm1futcalumiT::topicname();

    auto topicIt = m_outTopicMap.find (t_topicname);

    if (topicIt != std::end (m_outTopicMap) )
    {
        for (auto & busIt : topicIt->second)
        {
            toolbox::mem::Reference* ref = pair.second->duplicate();

            try
            {
                this->getEventingBus (busIt).publish (pair.first, ref, plist);
		this->m_outgoingCounter++;
                //std::cout << "Publishing " << pair.first << " (" << topicIt->first << ") on " << busIt << std::endl;
            }
            catch (xcept::Exception& e)
            {
                LOG4CPLUS_ERROR (m_logger, RED << "Failed to publish topic " << pair.first << " on bus " << busIt << RESET);

                if (ref)
                {
                    ref->release();
                    ref = 0;
                }
            }
        }
    }
    else
        LOG4CPLUS_INFO (m_logger, RED << "Error, topic: " << pair.first << " is not found in the map of output topics - not publishing!" << RESET);
}

bool bril::bcm1futcaprocessor::Application::albedoJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        if (m_albedoAlgorithm != nullptr)
        {
            m_albedoAlgorithm->compute (m_beamInfo, m_albedoQueue, m_appLock, m_albedo_data, m_fraction_data);

            ////now pick apart the albedo data and fill the chart
            if (m_webCharts)
            {
                m_albedoChart->newDataReady();
                m_fractionChart->newDataReady();
            }
        }
        else
        {
            m_appLock.take();
            m_albedoQueue.clear();
            m_albedoQueue.shrink_to_fit();
            m_appLock.give();
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

void bril::bcm1futcaprocessor::Application::requestRawData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    //Raw input data
    	m_occupancyChart->writeDataForQuery ( cgi, out, m_occ_data );
    	m_amplitudeChart->writeDataForQuery ( cgi, out, m_amp_data );
    	m_rawChart->writeDataForQuery ( cgi, out, m_raw_data );
    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::requestOverviewData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    //Lumi charts
    	m_lumiBXChart->writeDataForQuery ( cgi, out, m_lumiBXdata );
    	m_lumiChart->writeDataForQuery (cgi, out, m_lumiData);
    	//BKG charts
    	m_bkgBXChart->writeDataForQuery ( cgi, out, m_bkgBXdata );
    	m_bkgChart->writeDataForQuery (cgi, out, m_bkgData);
    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::requestChannelData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    	//Lumi charts
    	m_lumiBXChannelChart->writeDataForQuery ( cgi, out, m_lumiBXChanneldata );
    	m_lumiChannelChart->writeDataForQuery (cgi, out, m_lumiChannelData);
    	m_rawlumiBXChannelChart->writeDataForQuery ( cgi, out, m_rawlumiBXChanneldata );
    	m_rawlumiChannelChart->writeDataForQuery (cgi, out, m_rawlumiChannelData);

    	m_agghistChart->writeDataForQuery (cgi, out, m_agghist_data);
    	m_colhistChart->writeDataForQuery (cgi, out, m_colhist_data);
    	m_bkghistChart->writeDataForQuery (cgi, out, m_bkghist_data);
    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::requestAlbedoData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    	//Raw input data
    	m_albedoChart->writeDataForQuery ( cgi, out, m_albedo_data );
    	m_fractionChart->writeDataForQuery ( cgi, out, m_fraction_data );
    m_appLock.give();
}

template<typename DataT, size_t nBin> void bril::bcm1futcaprocessor::Application::fillChartData (webutils::WebChart* chart, std::map<std::string, std::vector<DataT>>& chartMap, HistogramBuffer<DataT, nBin>* buf)
{
    //loop the vector of HistogramBuffers
    //for (const auto &buf : bufVec)
    if(buf != nullptr)
    {
        std::string histtype = buf->getHistType();

        for (const auto & hist : buf->m_channelMap)
        {
            unsigned int t_chan;
            unsigned int t_algo;
            hist.first.decode (t_algo, t_chan);

            std::stringstream ss;
            ss << "Channel_" << t_chan;

            m_appLock.take();

            std::vector<DataT> t_tmpVec (hist.second.cbegin(), hist.second.cend() );

            chartMap[ss.str()] = t_tmpVec;
            chart->newDataReady();

            m_appLock.give();
        }
    }
}


void bril::bcm1futcaprocessor::Application::fillChartDataLumi (std::pair<std::string, toolbox::mem::Reference*>& pair)
{
    size_t queulenght = 1500;
    //get the Datum Header from the reference
    float lumi_bx[interface::bril::shared::MAX_NBX];

    float lumi_bx_raw[interface::bril::shared::MAX_NBX];
    float lumi;// = 0;
    float lumi_raw;// = 0;

    memset (lumi_bx, 0, sizeof (lumi_bx) );
    memset (lumi_bx_raw, 0, sizeof (lumi_bx_raw) );

    interface::bril::shared::DatumHead* t_inHeader = (interface::bril::shared::DatumHead*) (pair.second->getDataLocation() );
    unsigned int channelID = t_inHeader->getChannelID();

    std::string sensorstring;

    if (pair.first.find (interface::bril::bcm1futcalumiT::topicname() ) != std::string::npos)
    {
        sensorstring = "";
        interface::bril::shared::CompoundDataStreamer t_streamer (interface::bril::bcm1futcalumiT::payloaddict() );
        //decode the info
        t_streamer.extract_field (lumi_bx, "bx", t_inHeader->payloadanchor);
        t_streamer.extract_field (lumi_bx_raw, "bxraw", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi, "avg", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi_raw, "avgraw", t_inHeader->payloadanchor);
    }

    //create a compound data streamer to extrat the beam payload

    std::vector<float> t_tmpVec (lumi_bx, lumi_bx + interface::bril::shared::MAX_NBX );

    std::vector<float> t_tmpVec_raw (lumi_bx_raw, lumi_bx_raw + interface::bril::shared::MAX_NBX );

    toolbox::TimeVal t_time = toolbox::TimeVal::gettimeofday().sec() * 1000;

    m_appLock.take();

    if (channelID == 0)
    {
        std::stringstream ss;
        ss << "lumi" << sensorstring;
        std::stringstream ss_raw;
        ss_raw << "lumi_raw" << sensorstring;

        m_lumiBXdata[ss.str()] = t_tmpVec;
        m_lumiBXdata[ss_raw.str()] = t_tmpVec_raw;

        m_lumiData[ss.str()].push_back (std::make_pair (t_time, lumi) );
        int overflow = m_lumiData[ss.str()].size() - queulenght;

        if (overflow > 0) m_lumiData[ss.str()].erase (m_lumiData[ss.str()].begin(), m_lumiData[ss.str()].begin() + overflow);

        m_lumiData[ss_raw.str()].push_back (std::make_pair (t_time, lumi_raw) );
        overflow = m_lumiData[ss_raw.str()].size() - queulenght;

        if (overflow > 0) m_lumiData[ss_raw.str()].erase (m_lumiData[ss_raw.str()].begin(), m_lumiData[ss_raw.str()].begin() + overflow);


        m_lumiBXChart->newDataReady();
        m_lumiChart->newDataReady();
    }
    else
    {
        std::stringstream ss;
        ss << "Channel_" << channelID;

        // this is lumi
        m_lumiBXChanneldata[ss.str()] = t_tmpVec;
        // this is mu
        m_rawlumiBXChanneldata[ss.str()] = t_tmpVec_raw;

        // lumi
        m_lumiChannelData[ss.str()].push_back (std::make_pair (t_time, lumi) );
        int overflow = m_lumiChannelData[ss.str()].size() - queulenght;

        if (overflow > 0) m_lumiChannelData[ss.str()].erase (m_lumiChannelData[ss.str()].begin(), m_lumiChannelData[ss.str()].begin() + overflow);

        // mu
        m_rawlumiChannelData[ss.str()].push_back (std::make_pair (t_time, lumi_raw) );
        overflow = m_rawlumiChannelData[ss.str()].size() - queulenght;

        if (overflow > 0) m_rawlumiChannelData[ss.str()].erase (m_rawlumiChannelData[ss.str()].begin(), m_rawlumiChannelData[ss.str()].begin() + overflow);

        m_lumiBXChannelChart->newDataReady();
        m_lumiChannelChart->newDataReady();
        m_rawlumiBXChannelChart->newDataReady();
        m_rawlumiChannelChart->newDataReady();
    }

    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::fillChartDataBKG (std::pair<std::string, toolbox::mem::Reference*>& pair)
{
    size_t queulenght = 1500;
    //get the Datum Header from the reference
    float bkg1_bx[interface::bril::shared::MAX_NBX];
    float bkg2_bx[interface::bril::shared::MAX_NBX];
    float bkg1;// = 0;
    float bkg1_nc;// = 0;
    float bkg2;// = 0;
    float bkg2_nc;// = 0;

    memset (bkg1_bx, 0, sizeof (bkg1_bx) );
    memset (bkg2_bx, 0, sizeof (bkg2_bx) );

    interface::bril::shared::DatumHead* t_inHeader = (interface::bril::shared::DatumHead*) (pair.second->getDataLocation() );
    unsigned int channelID = t_inHeader->getChannelID();

    if (channelID == 0)
    {
        //create a compound data streamer to extrat the beam payload
        interface::bril::shared::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_backgroundT::payloaddict() );
        //decode the info
        //t_streamer.extract_field (&bkg1_bx, "bkgd1hist", t_inHeader->payloadanchor);
        //t_streamer.extract_field (&bkg2_bx, "bkgd2hist", t_inHeader->payloadanchor);
        t_streamer.extract_field (bkg1_bx, "bkgd1hist", t_inHeader->payloadanchor);
        t_streamer.extract_field (bkg2_bx, "bkgd2hist", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg1, "bkgd1", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg1_nc, "bkgd1_nc", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg2, "bkgd2", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg2_nc, "bkgd2_nc", t_inHeader->payloadanchor);

        std::vector<float> t_tmpVec1 (bkg1_bx, bkg1_bx + interface::bril::shared::MAX_NBX );
        std::vector<float> t_tmpVec2 (bkg2_bx, bkg2_bx + interface::bril::shared::MAX_NBX );

        toolbox::TimeVal t_time = toolbox::TimeVal::gettimeofday().sec() * 1000;

        std::stringstream ss1;
        ss1 << "bkg1";
        std::stringstream ss2;
        ss2 << "bkg2";
        std::stringstream ss1nc;
        ss1nc << "bkg1_nc";
        std::stringstream ss2nc;
        ss2nc << "bkg2_nc";

        m_appLock.take();

        m_bkgBXdata[ss1.str()] = t_tmpVec1;
        m_bkgBXdata[ss2.str()] = t_tmpVec2;

        m_bkgData[ss1.str()].push_back (std::make_pair (t_time, bkg1) );
        int overflow = m_bkgData[ss1.str()].size() - queulenght;

        if (overflow > 0) m_bkgData[ss1.str()].erase (m_bkgData[ss1.str()].begin(), m_bkgData[ss1.str()].begin() + overflow);

        m_bkgData[ss1nc.str()].push_back (std::make_pair (t_time, bkg1_nc) );
        overflow = m_bkgData[ss1nc.str()].size() - queulenght;

        if (overflow > 0) m_bkgData[ss1nc.str()].erase (m_bkgData[ss1nc.str()].begin(), m_bkgData[ss1nc.str()].begin() + overflow);

        m_bkgData[ss2.str()].push_back (std::make_pair (t_time, bkg2) );
        overflow = m_bkgData[ss2.str()].size() - queulenght;

        if (overflow > 0) m_bkgData[ss2.str()].erase (m_bkgData[ss2.str()].begin(), m_bkgData[ss2.str()].begin() + overflow);

        m_bkgData[ss2nc.str()].push_back (std::make_pair (t_time, bkg2_nc) );
        overflow = m_bkgData[ss2nc.str()].size() - queulenght;

        if (overflow > 0) m_bkgData[ss2nc.str()].erase (m_bkgData[ss2nc.str()].begin(), m_bkgData[ss2nc.str()].begin() + overflow);

        m_bkgBXChart->newDataReady();
        m_bkgChart->newDataReady();

        m_appLock.give();
    }
}

void bril::bcm1futcaprocessor::Application::fillChartDataHist (std::pair<std::string, toolbox::mem::Reference*>& pair)
{
    //get the Datum Header from the reference
    uint16_t data[interface::bril::shared::MAX_NBX];

    memset (data, 0, sizeof (data) );

    interface::bril::shared::DatumHead* t_inHeader = (interface::bril::shared::DatumHead*) (pair.second->getDataLocation() );
    unsigned int channelID = t_inHeader->getChannelID();

    //create a compound data streamer to extrat the beam payload
    if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
    {
        interface::bril::shared::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_agg_histT::payloaddict() );
        //t_streamer.extract_field (&data, "agghist", t_inHeader->payloadanchor);
        t_streamer.extract_field (data, "agghist", t_inHeader->payloadanchor);
    }
    else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
    {
        interface::bril::shared::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_col_histT::payloaddict() );
        t_streamer.extract_field (data, "colhist", t_inHeader->payloadanchor);
    }
    else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
    {
        interface::bril::shared::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_bkg_histT::payloaddict() );
        t_streamer.extract_field (data, "bkghist", t_inHeader->payloadanchor);
    }

    std::vector<uint16_t> t_tmpVec (data, data + interface::bril::shared::MAX_NBX );

    std::stringstream ss;
    ss << "Channel_" << channelID;

    m_appLock.take();

    if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
    {
        m_agghist_data[ss.str()] = t_tmpVec;
        m_agghistChart->newDataReady();
    }
    else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
    {
        m_colhist_data[ss.str()] = t_tmpVec;
        m_colhistChart->newDataReady();
    }
    else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
    {
        m_bkghist_data[ss.str()] = t_tmpVec;
        m_bkghistChart->newDataReady();
    }

    m_appLock.give();
}

template<typename DataT, size_t nBin> void bril::bcm1futcaprocessor::Application::doTimeAlignment (HistogramBuffer<DataT, nBin>* buf)
{
	if(buf != nullptr)
	{
            for (auto& hist : buf->m_channelMap)
            {
                unsigned int t_chan;
                unsigned int t_algo;
                hist.first.decode (t_algo, t_chan);
                int t_delay = m_channelInfo.getChannelDelay (hist.first);

                if (t_delay != 0)
                    hist.second.shift (t_delay);
            }
	}
}

////////////////////////////////////////////////////////////////////////////////
//WEB CALLBACKS
////////////////////////////////////////////////////////////////////////////////
void bril::bcm1futcaprocessor::Application::createTabBar (/*xgi::Input* in,*/ xgi::Output* out, bril::bcm1futcaprocessor::Tab tab)
{
    std::string t_urn = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/";
    //draw the xdaq header
    //this->getHTMLHeader (in, out);
    // Create the Title, Tab bar

    //Style this thing
    *out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/bril/bcm1futcaprocessor/html/stylesheet.css\" media=\"screen\" />";
    //Load the local highcharts.src.js
    //*out << "<script src=\"/bril/bcm1futcaprocessor/html/highcharts.src.js\"></script>";
    *out << "<script src=\"/bril/bcm1futcaprocessor/html/highcharts.js\"></script>";
    *out << "<script src=\"/bril/bcm1futcaprocessor/html/modules/boost.js\"></script>";

    std::ostringstream cTabBarString;

    // switch to show the current tab
    switch (tab)
    {
        case Tab::OVERVIEW:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button active") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button") << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::CHANNELS:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button active") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::ALBEDO:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button active") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::INPUT:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button active") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::CONFIG:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button active") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button") << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::MONITORING:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button active")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::BEAM:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button active") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;
    }


    *out << cgicc::div ().set ("class", "title").add (h2 (a ("BCM1F uTCA Processor" ).set ("href", t_urn + "Overview").set ("style", "float: left") ) ).add (img().set ("src", "/bril/bcm1futcaprocessor/html/bril.png" ).set ("alt", "brillogo").set ("height", "50").set ("style", "float: right") ) << std::endl;
    *out << cgicc::div (cTabBarString.str() ).set ("class", "tab") << std::endl;
}

void bril::bcm1futcaprocessor::Application::OverviewPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::OVERVIEW;
    //bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Luminosity & Background</h3><br/>";
    //*out << "<div class=\"plots-contentleft\"><h3>Luminosity & Background</h3><br/>";

    if (m_webCharts) m_lumiChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_lumiBXChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_bkgChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_bkgBXChart->writeChart (out);

    //*out << "</div>";

    *out << "</div></div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::ChannelPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::CHANNELS;
    //bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Luminosity & Hists</h3><br/>";
    //*out << "<div class=\"plots-contentleft\"><h3>Luminosity & Hists</h3><br/>";

    if (m_webCharts) m_lumiChannelChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_lumiBXChannelChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_agghistChart->writeChart (out);

    *out << "<br/>";
    //*out << "<div class=\"plots-contentleft\">";

    if (m_webCharts) m_colhistChart->writeChart (out);

    *out << "<br/>";

    //*out << "</div>";
    *out << "</div></div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::AlbedoPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::ALBEDO;
    //bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Corrected & Uncorrected</h3><br/>";
    //*out << "<div class=\"plots-contentleft\"><h3>Corrected & Uncorrected</h3><br/>";

    if (m_webCharts) m_albedoChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_fractionChart->writeChart (out);

    //*out << "</div>";
    *out << "</div></div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::InputDataPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::INPUT;
    //bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Input Data</h3><br/>";
    //*out << "<div class=\"plots-contentleft\"><h3>Input Data</h3><br/>";

    if (m_webCharts) m_occupancyChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_amplitudeChart->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_rawChart->writeChart (out);

    //*out << "</div>";
    *out << "</div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::ConfigPage (xgi::Input* in, xgi::Output* out)
{
    std::string webchartstring = (m_webCharts) ? "1" : "0";
    std::stringstream timeout;
    timeout << m_timeout;
    std::stringstream webchart_algo_string;
    webchart_algo_string << m_AlgoToPlot;
    m_tab = Tab::CONFIG;
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Configuration Parameters</h3><br/>";
    *out << "<div class=\"plots-contentleft\">";
    *out << "<h3>Eventing Config</h3><br/>";
    *out << "<table class=\"xdaq-table\" ><thead>";
    *out << tr().add (th ("") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";
    *out << tr().add (td ("") ).add (td ("timeout") ).add (td (timeout.str() ) );
    *out << tr().add (td ("") ).add (td ("webcharts") ).add (td (webchartstring) );
    *out << tr().add (td ("") ).add (td ("webchart_algo") ).add (td (webchart_algo_string.str()) );
    *out << "</tbody><thead>";

    *out << tr().add (th ("IO") ).add (th ("Bus") ).add (th ("Topic") ) << "</thead><tbody>";

    for (size_t source = 0; source < m_dataSources.elements(); source++)
    {
        std::string t_bus = m_dataSources[source].getProperty ("bus");
        std::string t_topics = m_dataSources[source].getProperty ("topics");
        *out << tr().add (td ("Input") ).add (td (t_bus) ).add (td (t_topics) );
    }

    *out << "</tbody><thead>" << tr().add (th ("IO") ).add (th ("Topic") ).add (th ("Buses") ) << "</thead><tbody>";

    for (size_t sink = 0; sink < m_dataSinks.elements(); sink++)
    {
        std::string t_topic = m_dataSinks[sink].getProperty ("topic");
        std::string t_buses = m_dataSinks[sink].getProperty ("buses");
        *out << tr().add (td ("Output") ).add (td (t_topic) ).add (td (t_buses) );
    }

    *out << "</tbody></table><br/>";
    *out << "<h3>Algorithm Config</h3><br/>";
    *out << "<table class=\"xdaq-table\" >";
    //*out << tr().add (th ("Lumi Algorithm") ).set ("colspan", "2" );
    *out << "<thead>" <<  tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_LumiProperties{"class", "calibtag", "commissioningmode", "automasking"};

    for (const auto& prop : t_LumiProperties)
        *out << tr().add (td ("Lumi") ).add (td (prop) ).add (td (m_lumiProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_AggProperties{"class"};

    for (const auto & prop : t_AggProperties)
        *out << tr().add (td ("Aggregator") ).add (td (prop) ).add (td (m_aggregateProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_AmplProperties{"class"};

    for (const auto & prop : t_AmplProperties)
        *out << tr().add (td ("Amplitude") ).add (td (prop) ).add (td (m_amplitudeProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";


    std::vector<std::string> t_BkgProperties{"class", "bestAlgo"};

    for (const auto & prop : t_BkgProperties)
        *out << tr().add (td ("Background") ).add (td (prop) ).add (td (m_backgroundProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_AlbedoProperties{"class", "modelFilePath", "albedoQueueLength", "albedoCalculationTime"};

    for (const auto & prop : t_AlbedoProperties)
        *out << tr().add (td ("Albedo") ).add (td (prop) ).add (td (m_albedoProperties.getProperty (prop) ) );

    *out << "</tbody></table><br/>";
    *out << "</div>";
    *out << "<div class=\"plots-contentright\">";
    *out << "<h3>Channel Config</h3><br/>";
    *out << "<table class=\"xdaq-table\" >";
    *out << "<thead>" << tr().add (th ("Channel") ).add (th ("delay") ).add (th ("uselumi") ).add (th ("usebkg") ).add (th ("sigmavis") ).add (th ("calibSBIL") ).add (th ("nonlinearity") ).add (th ("factor") ) << "</thead><tbody>";

    for (size_t channel = 0; channel < m_channelConfig.elements(); channel++)
    {
        std::string channelID = m_channelConfig[channel].getProperty ("channel");
        *out << tr().add (td (channelID) ).add (td (m_channelConfig[channel].getProperty ("delay") ) ).add (td (m_channelConfig[channel].getProperty ("uselumi") ) ).add (td (m_channelConfig[channel].getProperty ("usebkg") ) ).add (td (m_channelConfig[channel].getProperty ("sigmavis") ) ).add (td (m_channelConfig[channel].getProperty ("calibSBIL") ) ).add (td (m_channelConfig[channel].getProperty ("nonlinearity") ) ).add (td (m_channelConfig[channel].getProperty ("factor") ) );
    }

    *out << "</tbody></table><br/>";
    *out << "</div></div>";
}

void bril::bcm1futcaprocessor::Application::MonitoringPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::MONITORING;
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Monitoring Parameters</h3><br/>";
    *out << "<table class=\"xdaq-table\" border=\"1\">";
    *out << "<thead>" << tr().add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";
    m_appLock.take();

    int index = 0;

    for (const auto  & iVar : m_monVariables.m_monVarlist)
    {
        *out << tr().add (td (iVar) ).add (td (m_monVariables.m_monVars[index]->toString() ).set ("style", "word-break:break-all;") );
        index++;
    }

    m_appLock.give();
    *out << "</tbody></table>";
    *out << "</div>";
    *out << textarea (m_missingHistogramLog).set ("rows", "10").set ("columns", "100");
}

void bril::bcm1futcaprocessor::Application::BeamPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::BEAM;
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Beam Info</h3><br/>";
    *out << "<table class=\"xdaq-table\" border=\"1\">";
    m_appLock.take();
    std::string collstring = (m_beamInfo.m_beamPresent) ? "true" : "false";
    *out << tr().add (td ("MachineMode") ).add (td (m_beamInfo.m_machineMode) );
    *out << tr().add (td ("BeamMode") ).add (td (m_beamInfo.m_beamMode) );
    *out << tr().add (td ("Total Current Beam 1") ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_b1Itot) ) );
    *out << tr().add (td ("Total Current Beam 2") ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_b2Itot) ) );
    *out << tr().add (td ("Collidable Beams") ).add (td (collstring) );
    *out << "</table><br/>";

    *out << "<table class=\"xdaq-table\" >";

    *out << "<thead>" << tr().add (th ("BX") ).add (th ("B1 Intensity") ).add (th ("B2 Intensity") ).add (th ("B1 Config") ).add (th ("B2 Config") ).add (th ("Collision Mask") ).add (th ("BKG1 Mask") ).add (th ("BKG2 Mask") ) << "</thead><tbody>";

    for (size_t iBX = 0; iBX < interface::bril::shared::MAX_NBX; iBX++)
    {
        std::string colstring = (m_beamInfo.m_collidingMask.test (iBX) ) ? "1" : "0";
        std::string colclass = (m_beamInfo.m_collidingMask.test (iBX) ) ? "on" : "off";
        std::string bkg1string = (m_beamInfo.m_bkg1Mask.test (iBX) ) ? "1" : "0";
        std::string bkg1class = (m_beamInfo.m_bkg1Mask.test (iBX) ) ? "on" : "off";
        std::string bkg2string = (m_beamInfo.m_bkg2Mask.test (iBX) ) ? "1" : "0";
        std::string bkg2class = (m_beamInfo.m_bkg2Mask.test (iBX) ) ? "on" : "off";
        *out << tr().add (td (bril::bcm1futcaprocessor::convert (iBX) ) ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx1Intensity[iBX] ) ) )
             .add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx2Intensity[iBX] ) ) ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx1Conf[iBX] ) ) )
             .add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx2Conf[iBX] ) ) ).add (td (colstring).set ("class", colclass) ).add (td (bkg1string ).set ("class", bkg1class) ).add (td (bkg2string).set ("class", bkg2class) );
    }

    m_appLock.give();
    *out << "</tbody></table>";
    *out << "</div>";
}
