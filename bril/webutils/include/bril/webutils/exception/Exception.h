#ifndef _bril_webutils_exception_Exception_h_
#define _bril_webutils_exception_Exception_h_

#include "xcept/Exception.h"

namespace bril
 {

  namespace webutils
   {

    namespace exception
     {

      class DataMismatchError : public xcept::Exception
       {

        public:
        DataMismatchError( std::string name, std::string message, std::string module, int line, std::string function ) :
         xcept::Exception( name, message, module, line, function )
         {}

        DataMismatchError( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
         xcept::Exception( name, message, module, line, function, e )
         {}

       };

     }

   }

 }

#endif

