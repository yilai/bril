/**
 * @brief Webpage utility functions and highcharts based monitoring.
 *
 * @author N. Tosi \<nicolo.tosi@cern.ch\>
 *
 * This package includes various utility functions to create tables and plots in xdaq webpages.
 */

#ifndef _bril_webutils_WebHelpers_h_
#define _bril_webutils_WebHelpers_h_

#include <cstdio>
#include <iterator>
#include <limits>
#include <string>
#include "xdata/Serializable.h"

namespace bril
 {

  namespace webutils
   {

    /**
     * The family of to_string functions are needed because of the incomplete support for the
     * c++11 std::to_string function in gcc 4.4.7 (the current cmsonline compiler).
     * Once that is upgraded to a compiler that fully supports c++11, these can be removed.
     */

    inline std::string to_string( int x )
     {
      return std::to_string( (long long)x );
     }

    inline std::string to_string( unsigned int x )
     {
      return std::to_string( (unsigned long long)x );
     }

    inline std::string to_string( long x )
     {
      return std::to_string( (long long)x );
     }

    inline std::string to_string( unsigned long x )
     {
      return std::to_string( (unsigned long long)x );
     }

    inline std::string to_string( double x )
     {
      return std::to_string( (long double)x );
     }

    inline std::string to_string( std::string x )
     {
      return x;
     }

    inline std::string to_string( const xdata::Serializable* x )
     {
      return x->toString();
     }

    template <typename T, typename U>
    inline std::string to_string( const typename std::pair<T, U>& pair )
     {
      return "[" + to_string( pair.first ) + ", " + to_string( pair.second ) + "]";
     }

    /**
     * An x,y,z triplet to be used when producing Heatmap Charts.
     */
    template <typename X, typename Y, typename Z>
    struct heatmap_point
     {

      heatmap_point( X x, Y y, Z z ) :
       m_x( x ), m_y( y ), m_z( z )
       {}

      X m_x;
      Y m_y;
      Z m_z;

     };

    template <typename X, typename Y, typename Z>
    inline heatmap_point<X, Y, Z> make_heatmap_point( X x, Y y, Z z )
     {
      return heatmap_point<X, Y, Z>( x, y, z );
     }

    template <typename X, typename Y, typename Z>
    inline std::string to_string( const heatmap_point<X, Y, Z>& hp )
     {
      return '[' + to_string( hp.m_x ) + ',' + to_string( hp.m_y ) + ',' + to_string( hp.m_z ) + ']';
     }

    namespace
     {

      template <typename T>
      struct cvt_code
       {
        static const char* value;
        static const std::size_t size = 0;
       };

#define SPECIFY_FOR_TYPE(T,CODE,SIZE)\
      template <> struct cvt_code<T> { static const char* value; static const std::size_t size = SIZE; }; const char* cvt_code<T>::value = CODE; inline void dummy_##T(){(void)cvt_code<T>::value;}

//I could have used std::numeric_limits<T>::digits10 for the integers, which would have been close enough with some added margin and technically portable... meh...
      SPECIFY_FOR_TYPE(bool,"%hhu",1)
      SPECIFY_FOR_TYPE(char,"%hhi",3)
      SPECIFY_FOR_TYPE(int8_t,"%hhi",3)
      SPECIFY_FOR_TYPE(uint8_t,"%hhu",3)
      SPECIFY_FOR_TYPE(int16_t,"%hi",6)
      SPECIFY_FOR_TYPE(uint16_t,"%hu",5)
      SPECIFY_FOR_TYPE(int32_t,"%i",11)
      SPECIFY_FOR_TYPE(uint32_t,"%u",10)
      SPECIFY_FOR_TYPE(int64_t,"%lli",20)
      SPECIFY_FOR_TYPE(uint64_t,"%llu",20)
      SPECIFY_FOR_TYPE(float,"%.6g",15)
      SPECIFY_FOR_TYPE(double,"%.12g",20)
//this one is explicit because ## can't paste 'long double' with a space
      template <> struct cvt_code<long double> { static const char* value; static const std::size_t size = 25; }; const char* cvt_code<long double>::value = "%.16g"; inline void dummy_longdouble(){(void)cvt_code<long double>::value;}

      template <typename T, typename = void>
      struct to_string_fast;

      //base template takes care of standard case
      template <typename T>
      struct to_string_fast <T, typename std::enable_if<cvt_code<T>::size == 0, void>::type>
       {
        template <typename Iter>
        static std::string run( Iter first, Iter last, std::input_iterator_tag dummy )
         {
          if ( std::distance( first, last ) == 1 )
            return to_string( *first );
          return to_string( *first ) + ", " + run( ++first, last, dummy );
         };

        template <typename Iter>
        static std::string run( Iter first, Iter last, std::bidirectional_iterator_tag dummy )
         {
          if ( std::distance( first, last ) == 1 )
            return to_string( *first );
          --last;
          return run( first, last, dummy ) + ", " + to_string( *last ); //it's more efficient to add a smaller string at the back of a bigger one
         };
       };

      template <typename T>
      struct to_string_fast <T, typename std::enable_if<cvt_code<T>::size >= 1, void>::type>
       {
        template <typename Iter>
        static std::string run( Iter first, Iter last, std::input_iterator_tag )
         {
          auto n = std::distance( first, last );
          auto maxsz = cvt_code<T>::size + 1; //for the comma...
          char* ptr = new char[maxsz * n];
          char* it = ptr;
          for ( ; first != last; ++first )
           {
            std::size_t offset = std::snprintf( it, maxsz, cvt_code<T>::value, T( *first ) );
            it += offset;
            *it = ',';
            ++it;
           }
          *--it = '\0';
          std::string ret( ptr, it - ptr );
          delete[] ptr;
          return ret;
         };
       };
     }

    template <typename Iter>
    inline std::string to_string( Iter first, Iter last )
     {
      if ( first == last )
        return std::string();
      typedef typename std::iterator_traits<Iter> traits;
      return to_string_fast<typename traits::value_type>::run( first, last, typename traits::iterator_category() );
     }

    /**
     * Provides a minimal interface to a sequential container, avoinding unnecessary copies.
     */
    template <typename Iterator>
    struct Range
     {

      Iterator begin() const { return first; }
      Iterator end() const { return last; }

      typename std::iterator_traits<Iterator>::difference_type size() const { return last - first; }
      bool empty() const { return first == last; }

      Range() {last=NULL;first = last;}

      Range( Iterator f, Iterator l ) : first( f ), last( l ) {}

      Iterator first;
      Iterator last;

     };

    template <typename T>
    typename std::enable_if<sizeof(typename T::const_iterator), Range<typename T::const_iterator>>::type make_range( const T& cont ) 
     {
      return Range<typename T::const_iterator>( cont.begin(), cont.end() );
     }

    template <typename T, std::size_t N>
    Range<const T*> make_range( const T (& arr)[N] )
     {
      return Range<const T*>( arr, arr + N );
     }

    template <typename T>
    auto make_range_map( std::string key, T&& value ) -> typename std::map<std::string, decltype( make_range( value ) )>
     {
      typename std::map<std::string, decltype( make_range( value ) )> ret;
      ret[key] = make_range( std::forward<T>( value ) );
      return ret;
     }

    template <typename T, typename... Others>
    auto make_range_map( std::string key, T&& value, Others&&... others ) -> typename std::map<std::string, decltype( make_range( value ) )>
     {
      auto ret = make_range_map( std::forward<Others>( others )... );
      ret[key] = make_range( std::forward<T>( value ) );
      return ret;
     }

    template <typename StrIter, typename TIter>
    auto make_range_map( StrIter firstk, StrIter lastk, TIter firstv, TIter lastv ) -> typename std::map<std::string, decltype( make_range( *firstv ) )>
     {
      typename std::map<std::string, decltype( make_range( *firstv ) )> ret;
      while ( ( firstk != lastk ) && ( firstv != lastv ) )
        ret[*firstk++] = make_range( *firstv++ );
      return ret;
     }

   }

 }

#endif

