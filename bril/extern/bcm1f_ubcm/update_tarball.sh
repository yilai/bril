# select version tag
TAG="v1.4"
# script
if [ -d "ubcm" ]; then echo "First store all changes in ubcm folder and remove it" && return 1; fi
mkdir ubcm
cd ubcm/
git init
git remote add origin ssh://git@gitlab.cern.ch:7999/bril/ubcm.git
git config core.sparsecheckout true
echo "sw/src" >> .git/info/sparse-checkout
echo "sw/include" >> .git/info/sparse-checkout
echo "sw/cfg" >> .git/info/sparse-checkout
git pull origin tag ${TAG} --depth=1
git checkout ${TAG}
cd ..
tar -czf ubcm.tar.gz ubcm
rm -rf ubcm
