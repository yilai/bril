#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Guard.h"
#include "toolbox/TimeVal.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Integer.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"
#include "b2in/nub/Method.h"
#include "interface/bril/BEAMTopics.hh"
#include "bril/supervisor/WebUtils.h"
#include "bril/supervisor/BunchPatternProcessor.h"
#include "bril/supervisor/utils.h"
#include "bril/supervisor/exception/Exception.h"
#include <algorithm>
#include <utility>

XDAQ_INSTANTIATOR_IMPL(bril::supervisor::BunchPatternProcessor)

namespace bril{ 
  namespace supervisor {
    static int maxnbx=3564;
    
    BunchPatternProcessor::BunchPatternProcessor(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
      xgi::framework::deferredbind(this,this,&bril::supervisor::BunchPatternProcessor::Default, "Default");
      b2in::nub::bind(this, &bril::supervisor::BunchPatternProcessor::onMessage);
      m_bus.fromString("brildata");//default            
      try{
	getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
	getApplicationInfoSpace()->fireItemAvailable("bxconfigPriority",&m_bxconfigPriorityStr);
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	toolbox::net::URN memurn("toolbox-mem-pool","brilBunchPatternProcessor_mem"); 
	toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
	m_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);
	m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_publishing","waiting");    
	m_cleaning = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_cleaning","waiting");    
	m_as_publishing = toolbox::task::bind(this,&bril::supervisor::BunchPatternProcessor::publishing,"publishing");
	m_as_cleaning = toolbox::task::bind(this,&bril::supervisor::BunchPatternProcessor::cleaning,"cleaning");
	m_cutoffmsec = 600000;//600ms
	m_canpublish = false;
	m_flashIS = 0;
	m_evtpublishcount = 0;
	// all recognised providers
	m_providernametoid.insert( std::make_pair("bptx_rhu",0) );
	m_providernametoid.insert( std::make_pair("bptx_scope",1) );
	m_providernametoid.insert( std::make_pair("fbct",2) );
	m_providernametoid.insert( std::make_pair("bptx_utca",3) );
	m_providernametoid.insert( std::make_pair("lhcconfig",4) );
	m_providernametoid.insert( std::make_pair("other",9) );	 
	m_provideridtoname.insert( std::make_pair(0,"bptx_rhu") );
	m_provideridtoname.insert( std::make_pair(1,"bptx_scope") );
	m_provideridtoname.insert( std::make_pair(2,"fbct") );
	m_provideridtoname.insert( std::make_pair(3,"bptx_utca") );
	m_provideridtoname.insert( std::make_pair(4,"lhcconfig") );
	m_provideridtoname.insert( std::make_pair(9,"other") );

      }catch(xcept::Exception & e){
	XCEPT_RETHROW(xdaq::exception::Exception, "Failed to set up infospace or memory pool", e);
      }    
     
    }
    
    BunchPatternProcessor::~BunchPatternProcessor(){  
      cleanFlashlist();
    }
    
    void BunchPatternProcessor::Default(xgi::Input * in, xgi::Output * out){      
    }
    
    void BunchPatternProcessor::actionPerformed(xdata::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
      if( e.type() == "urn:xdaq-event:setDefaultValues" ){     
	try{	  	 
	  this->getEventingBus(m_bus.value_).addActionListener(this);  
	  this->getEventingBus(m_bus.value_).subscribe("bxconfig");
	  m_publishing->activate();	  
	  m_cleaning->activate();
	}catch(eventing::api::exception::Exception& e){
	  std::string msg("Failed to listen to eventing bus "+m_bus.value_);
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	  XCEPT_RETHROW(exception::Exception,msg,e);     
	}

	std::list<std::string> p = toolbox::parseTokenList(m_bxconfigPriorityStr.value_,",");
	size_t nps = p.size();
	std::list<std::string> ::iterator pit;       //the first element has the highest priority
	for( pit=p.begin(); pit!=p.end(); ++pit ){
	  std::map< std::string,short >::iterator i=m_providernametoid.find(*pit);
	  if( i!=m_providernametoid.end() ){
	    m_providerpriorities.insert( std::make_pair(i->second,nps) );
	  }else{
	    LOG4CPLUS_ERROR(getApplicationLogger(),"bxconfig provider "+*pit+" not recognised");	    
	  }	  
	  --nps;
	}
	
	// declare flashlist
	std::string urn = createQualifiedInfoSpace("lumimon_bxconfig").toString();
	m_flashIS = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(urn));  
	init_bxconfigFlash();
      }
    }

    void BunchPatternProcessor::init_bxconfigFlash(){
      std::string fieldname;

      fieldname = "fillnum";
      m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "runnum";
      m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "lsnum";
      m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "timestamp";
      m_flashvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "nbx";
      m_flashvalues.push_back( new xdata::Integer(0) );
      m_flashfields.push_back( fieldname );

      fieldname = "rank";
      m_flashvalues.push_back( new xdata::Integer(-1) );
      m_flashfields.push_back( fieldname );

      fieldname = "providers";
      m_flashvalues.push_back( new xdata::Vector<xdata::Integer>() );
      m_flashfields.push_back( fieldname );

      fieldname = "b1";
      m_flashvalues.push_back( new xdata::Vector<xdata::Integer>() );
      m_flashfields.push_back( fieldname );

      fieldname = "b2";
      m_flashvalues.push_back( new xdata::Vector<xdata::Integer>() );
      m_flashfields.push_back( fieldname );
            
      try{
	std::vector<xdata::Serializable*>::const_iterator vIt = m_flashvalues.begin();
	std::list<std::string>::const_iterator nameIt=m_flashfields.begin(); 
	for(; nameIt!=m_flashfields.end(); ++nameIt, ++vIt ){
	  m_flashIS->fireItemAvailable(*nameIt,*vIt);
	}
      }catch(xdata::exception::Exception& e){
	std::string msg( "Failed to declare flashlist lumimon_bxconfig" );
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
	XCEPT_RETHROW(xdaq::exception::Exception, msg, e);
      }
    }
    
    void BunchPatternProcessor::actionPerformed(toolbox::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
      if ( e.type() == "eventing::api::BusReadyToPublish" ){
	m_canpublish = true;
      }
    }

    void BunchPatternProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) {
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if (action == "notify"){      
	std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	if(ref!=0){
	  std::stringstream msg;
	  std::string data_version = plist.getProperty("DATA_VERSION");
	  std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
	  if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
	    std::string msg("Received brildaq message "+topic+" with no or wrong version, do not process.");
	    LOG4CPLUS_ERROR(getApplicationLogger(),msg);
	    ref->release(); ref = 0;
	    return;
	  }
	  if(payloaddict.empty()){
	    msg<<"Received "<<topic<<" with no dictionary, do not process";
	    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
	    ref->release(); ref = 0;
	    return;
	  }
	  interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());		  	  
	  bool b1config[maxnbx];
	  bool b2config[maxnbx];
	  unsigned short datasourceid = 0;
	  interface::bril::shared::CompoundDataStreamer tc(payloaddict);
	  tc.extract_field( &datasourceid, "datasource", thead->payloadanchor );
	  if( thead->getDataSourceID()==9 ){
	    datasourceid = 9; //temporary: bptx processor sets datasourceid for bxconfig always to 0.
	  }
	  msg<<"received "<<topic<<" "<<thead->fillnum<<" "<<thead->runnum<<" "<<thead->lsnum<<" " <<" nb "<<thead->nbnum<<" from "<<datasourceid;
	  LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
	  msg.str("");msg.clear();	 

	  std::map<short, std::string>::iterator providerit = m_provideridtoname.find( datasourceid );
	  Provider pv;
	  short prid = -1;
	  if(providerit != m_provideridtoname.end()){	
	    prid = providerit->first;
	    std::string prname = providerit->second;
	    std::map<short, short>::iterator providerPriorityit = m_providerpriorities.find(prid);
	    if( providerPriorityit == m_providerpriorities.end() ){
	      msg<<"Provider "<<prname<<" is not in the priority list, ignore its message";
	      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
	      msg.str(""); msg.clear();
	      ref->release(); ref = 0;
	      return;
	    }
	    pv.name = prname;
	    pv.id = prid;	    
	    pv.priority = m_providerpriorities[prid];
	  }
	  tc.extract_field( b1config, "beam1", thead->payloadanchor );
	  tc.extract_field( b2config, "beam2", thead->payloadanchor );

	  BunchPattern bp; 	  
	  bp.fillnum = thead->fillnum; //in the case of same bx pattern, the time header is taken from the first message provider. Time info is not guaranteed to be correct
	  bp.runnum = thead->runnum;
	  bp.lsnum = thead->lsnum;
	  bp.nbnum = thead->nbnum;
	  bp.tssec = thead->timestampsec;
	  bp.tsmsec = thead->timestampmsec;	  
	  bp.providerpriorities.push_back(pv);
	  memcpy(bp.bxpattern, b1config, sizeof(b1config) );
	  memcpy(bp.bxpattern+maxnbx, b2config, sizeof(b2config) );
	  if( m_bxconfigCache.empty() ){
	    toolbox::task::Guard<toolbox::BSem> guard(m_applock);
	    m_bxconfigCache.push_back(bp);
	    if( m_canpublish ){
	      //if need to publish, do publish
	      m_publishing->submit(m_as_publishing);
	    }else{
	      //if no need to publish, clean cache 
	      m_cleaning->submit(m_as_cleaning);
	    }
	  }else{ 
	    bool asbefore = false;
	    for( std::vector<BunchPattern>::iterator bpit = m_bxconfigCache.begin(); bpit!=m_bxconfigCache.end(); ++bpit){
	      if( (*bpit)==bp ){
		asbefore = true;
		if( !bpit->providerExists(prid) ){
		  toolbox::task::Guard<toolbox::BSem> guard(m_applock);		
		  bpit->providerpriorities.push_back(pv);
		  break;
		}else{
		  msg<<"Duplicated Provider for the same bxpattern id="<<prid;
		  LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
		  msg.str(""); msg.clear();
		}		
	      }else{
		msg<<"Mismatched bxpattern "<<bpit->providerpriorities.size()<<" vs "<<bp.providerpriorities.size()<<" : ";
		msg<<*bpit<<" vs "<<bp;
		LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
		msg.str(""); msg.clear();		
	      }
	    }		
	    if( !asbefore ) {
	      toolbox::task::Guard<toolbox::BSem> guard(m_applock);
	      m_bxconfigCache.push_back(bp);
	    }	  	    
	  }
	}//end if(ref!=0)
      }
      if(ref){
	ref->release();
	ref = 0;
      }
    }
    
    bool BunchPatternProcessor::publishing(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing: Entering");      
      usleep(m_cutoffmsec.value_);     
      {
	toolbox::task::Guard<toolbox::BSem> guard(m_applock);
	std::sort(m_bxconfigCache.begin(),m_bxconfigCache.end(),BunchPattern_IsGreater());
      }
      do_publish_bxconfig();
      {
	toolbox::task::Guard<toolbox::BSem> guard(m_applock);
	m_bxconfigCache.clear();
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing: Leaving");  
      return false;
    }    
    
    bool BunchPatternProcessor::cleaning(toolbox::task::WorkLoop* wl){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "cleaning: Entering");      
      usleep(m_cutoffmsec.value_);
      {
	toolbox::task::Guard<toolbox::BSem> guard(m_applock);
	m_bxconfigCache.clear();
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(), "cleaning: Leaving"); 
      return false;
    }    
    
    void BunchPatternProcessor::do_publish_bxconfig(){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "do_publish_bxconfig: Entering");      
      //assume m_bxconfigCache is already sorted
      std::stringstream msg;
      bool fireflash = false;
      if( m_flashIS && m_evtpublishcount>15 ) {
	fireflash=true;
      }

      for( size_t pos=0; pos!=m_bxconfigCache.size(); ++pos ){
	bool b1[maxnbx];
	bool b2[maxnbx];
	memcpy(b1,m_bxconfigCache[pos].bxpattern,sizeof(b1));
	memcpy(b2,m_bxconfigCache[pos].bxpattern+maxnbx,sizeof(b2));
	if( pos==0 ){	  // pos 0 is the bestbxconfig, bestbxconfig to eventing
	  interface::bril::shared::CompoundDataStreamer tc(interface::bril::bestbxconfigT::payloaddict());
	  toolbox::mem::Reference* r = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,interface::bril::bestbxconfigT::maxsize());
	  r->setDataSize( interface::bril::bestbxconfigT::maxsize() );    
	  interface::bril::shared::DatumHead* h = (interface::bril::shared::DatumHead*)(r->getDataLocation());  
	  h->setTime(m_bxconfigCache[pos].fillnum,m_bxconfigCache[pos].runnum,m_bxconfigCache[pos].lsnum,m_bxconfigCache[pos].nbnum,m_bxconfigCache[pos].tssec,m_bxconfigCache[pos].tsmsec);
	  h->setResource(interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND);
	  h->setTotalsize( interface::bril::bestbxconfigT::maxsize() );
	  unsigned short ncollidingbx = m_bxconfigCache[pos].ncollidingbx();
	  tc.insert_field( h->payloadanchor, "ncollidingbx", &ncollidingbx );
	  tc.insert_field( h->payloadanchor, "beam1", b1 );
	  tc.insert_field( h->payloadanchor, "beam2", b2 );
	  xdata::Properties plist;
	  plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
	  plist.setProperty("PAYLOAD_DICT",interface::bril::bestbxconfigT::payloaddict());
	  try{
	    msg<<"Publish bestbxconfig "<<m_bxconfigCache[pos].fillnum<<" "<<m_bxconfigCache[pos].runnum<<" "<<m_bxconfigCache[pos].lsnum<<" "<<m_bxconfigCache[pos].nbnum<<" to "<<m_bus.toString()<<" nbx "<<ncollidingbx<<" nproviders "<<m_bxconfigCache[pos].providerpriorities.size();
	    LOG4CPLUS_INFO( getApplicationLogger(), msg.str() );
	    msg.str(""); msg.clear();
	    getEventingBus(m_bus.value_).publish(interface::bril::bestbxconfigT::topicname(),r,plist);
	  }catch(xcept::Exception& e){
	    msg<<"Failed to publish bestbxconfig to "<<m_bus.value_;
	    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	    if(r){ r->release(); r = 0; }   
	    XCEPT_DECLARE_NESTED(exception::Exception,myerrorobj,msg.str(),e);
	    this->notifyQualified("error",myerrorobj);
	  }
	  m_evtpublishcount++;
	}//end pos==0
	if( fireflash ){// fire to flashlist only every 16 times of firing to eventing	  	
	  try{
	    xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_flashIS->find("fillnum") );
	    p_fillnum->value_ = m_bxconfigCache[pos].fillnum; 

	    xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_flashIS->find("runnum") );
	    p_runnum->value_ = m_bxconfigCache[pos].runnum; 

	    xdata::UnsignedInteger32* p_lsnum = dynamic_cast< xdata::UnsignedInteger32* >( m_flashIS->find("lsnum") );
	    p_lsnum->value_ = m_bxconfigCache[pos].lsnum;

	    xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( m_flashIS->find("timestamp") );
	    p_timestamp->value_.sec( m_bxconfigCache[pos].tssec );
	    p_timestamp->value_.usec( m_bxconfigCache[pos].tsmsec*1000 );

	    xdata::Integer* p_nbx = dynamic_cast<xdata::Integer* >( m_flashIS->find("nbx") );	    
	    xdata::Vector<xdata::Integer>* p_b1 =  dynamic_cast< xdata::Vector<xdata::Integer>* >( m_flashIS->find("b1") );
	    xdata::Vector<xdata::Integer>* p_b2 =  dynamic_cast< xdata::Vector<xdata::Integer>* >( m_flashIS->find("b2") );
	    p_b1->clear();
	    p_b2->clear();
	    int n = 0;
	    for(int i=0; i<maxnbx; ++i){
	      if( b1[i]&&b2[i] ){
		++n;
	      } 
	      p_b1->push_back( xdata::Integer(b1[i]) );
	      p_b2->push_back( xdata::Integer(b2[i]) );
	    }
	    p_nbx->value_ = n;	    
	    
	    xdata::Vector<xdata::Integer>* p_providers =  dynamic_cast< xdata::Vector<xdata::Integer>* >( m_flashIS->find("providers") );
	    p_providers->clear();
	    for( std::vector<Provider>::const_iterator it=m_bxconfigCache[pos].providerpriorities.begin(); it!=m_bxconfigCache[pos].providerpriorities.end(); ++it){
	      p_providers->push_back( xdata::Integer(it->id) );
	    }

	    xdata::Integer* p_rank =  dynamic_cast< xdata::Integer* >( m_flashIS->find("rank") );	    
	    p_rank->value_ = pos;
	    msg<<"flashlist firing "<<p_fillnum->toString()<<" "<<p_runnum->toString()<<" "<<p_lsnum->toString()<<" rank "<<pos<<" nbx "<<n<<" providers "<<p_providers->toString();
	    LOG4CPLUS_INFO(getApplicationLogger(), msg.str());      
	    m_flashIS->fireItemGroupChanged(m_flashfields,this);
	  }catch(xdata::exception::Exception& e){
	    std::string msg("Failed to fire flashlist bxconfig");
	    LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
	    XCEPT_DECLARE_NESTED(bril::supervisor::exception::Exception,myerrorobj,msg,e);
	    notifyQualified("error",myerrorobj);
	  }
	}
      }
      if(fireflash){
	m_evtpublishcount = 0;
	fireflash = false;
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(), "do_publish_bxconfig: Leaving");      
    }

    void BunchPatternProcessor::cleanFlashlist(){
      for(std::vector< xdata::Serializable* >::iterator it=m_flashvalues.begin(); it!=m_flashvalues.end(); ++it){
	delete *it;
      }
      m_flashvalues.clear();
    }
}}//ns bril::supervisor
