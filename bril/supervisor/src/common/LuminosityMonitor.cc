#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"
#include "b2in/nub/Method.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/ActionWithArg.h"
#include "toolbox/task/Guard.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"
#include "xdata/TimeVal.h"
#include "xdata/Vector.h"
#include "bril/supervisor/LuminosityMonitor.h"
#include "bril/supervisor/exception/Exception.h"
#include "bril/supervisor/Events.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/shared/CompoundDataStreamer.h"

XDAQ_INSTANTIATOR_IMPL (bril::supervisor::LuminosityMonitor)

bril::supervisor::LuminosityMonitor::LuminosityMonitor(xdaq::ApplicationStub* s) : xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this),m_applock(toolbox::BSem::FULL) {

    xgi::framework::deferredbind(this,this,&bril::supervisor::LuminosityMonitor::Default, "Default");
    b2in::nub::bind(this, &bril::supervisor::LuminosityMonitor::onMessage);    

    toolbox::net::UUID u=getApplicationDescriptor()->getUUID();
    m_uniqueid = u.toString();

    getApplicationInfoSpace()->fireItemAvailable("lumitopics",&m_lumitopicsStr);
    getApplicationInfoSpace()->fireItemAvailable("fastlumitopics",&m_fastlumitopicsStr);
    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    
    //prepare detluminosity_mon infospace, publication on LS changed event
    std::string nid("detluminosity_mon");
    std::string monurn = createQualifiedInfoSpace(nid).toString();
    m_luminosityInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));      

    //prepare fastbestlumi_mon infospace, immediate publication on message
    std::string fastbestnid("fastbestlumi_mon");
    std::string fastbestmonurn = createQualifiedInfoSpace(fastbestnid).toString();
    m_fastbestlumiInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(fastbestmonurn));  
    
    //prepare fastdetluminosity_mon infospace, immediate publication on message
    std::string fastnid("fastdetluminosity_mon");
    std::string fastmonurn = createQualifiedInfoSpace(fastnid).toString();
    m_fastlumiInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(fastmonurn));  

    init_luminosityFlashlist();
    declare_luminosityFlashlist();         
    init_fastlumiFlashlist();
    declare_fastlumiFlashlist();         

}

bril::supervisor::LuminosityMonitor::~LuminosityMonitor(){
}

void bril::supervisor::LuminosityMonitor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if ( action == "notify" && ref!=0 ){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    LOG4CPLUS_DEBUG(getApplicationLogger(),"received topic "+topic);
    std::string payloaddict = plist.getProperty("PAYLOAD_DICT");   
    interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 

    std::map< std::string, SimpleLuminosityData >::iterator sIt=m_fastlumidata.find(topic);
    if( sIt!=m_fastlumidata.end() ){
      std::string detectorname = topic;
      float fastavg = 0;
      {//begin guard
      toolbox::task::Guard<toolbox::BSem> g(m_applock);
      (sIt->second).fillnum = thead->fillnum;
      (sIt->second).runnum = thead->runnum;
      (sIt->second).lsnum = thead->lsnum;
      (sIt->second).nbnum = thead->nbnum;
      (sIt->second).timestampsec = thead->timestampsec;      
      (sIt->second).timestampmsec = thead->timestampmsec;     
      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
      if( topic.find("bestlumi")!=std::string::npos ){
	char provider[20];
	tc.extract_field( provider, "provider", thead->payloadanchor );	
	detectorname = std::string(provider);
	tc.extract_field( &fastavg, "delivered", thead->payloadanchor );
      }else{
	tc.extract_field( &fastavg, "avg", thead->payloadanchor );
      }
      (sIt->second).avg = fastavg;
      (sIt->second).detectorname = detectorname;
      }//end guard
      std::map< std::string, std::pair< toolbox::task::WorkLoop*, toolbox::task::ActionSignature* > >::iterator wlit = m_fastlumi_wlstore.find(topic);
      if( wlit!=m_fastlumi_wlstore.end() ){
	(wlit->second).first->submit( (wlit->second).second );
      }
    }

    std::map< std::string, DetLuminosityData >::iterator it=m_lumidata.find(topic);
    if( it!=m_lumidata.end() ){  

      unsigned int previous_runnum = (it->second).runnum;
      unsigned int previous_lsnum = (it->second).lsnum;
      
      unsigned int runnum = thead->runnum;
      unsigned int lsnum = thead->lsnum;

      if( runnum!=previous_runnum || lsnum!=previous_lsnum ){	
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Before enter guard-onMessage ");
	interface::bril::shared::CompoundDataStreamer tc(payloaddict);
	{//begin guard
	toolbox::task::Guard<toolbox::BSem> g(m_applock);
	(it->second).fillnum = thead->fillnum;
	(it->second).runnum = runnum;
	(it->second).lsnum = lsnum;
	(it->second).timestampsec = thead->timestampsec;
	(it->second).timestampmsec = thead->timestampmsec;
	if(payloaddict.find("mask")!=std::string::npos){
	  tc.extract_field( &(it->second).masklow , "masklow", thead->payloadanchor );
	  tc.extract_field( &(it->second).maskhigh , "maskhigh", thead->payloadanchor );
	}
	tc.extract_field( &(it->second).avg, "avg", thead->payloadanchor );
	char calibtag[20];
	tc.extract_field( calibtag, "calibtag", thead->payloadanchor );
	it->second.calibtag = std::string(calibtag);
	if( payloaddict.find("bx")!=std::string::npos ){
	  tc.extract_field( it->second.bx, "bx", thead->payloadanchor );
	}
	}//end guard
	LOG4CPLUS_DEBUG(getApplicationLogger(),"Left guard-OnMessage");
	bril::supervisor::TopicLumiSectionChangedEvent lse(topic);
	this->fireEvent( lse );
      }
    }
  }
  if(ref!=0){
    ref->release(); ref=0;
  }
}

void bril::supervisor::LuminosityMonitor::Default(xgi::Input * in, xgi::Output * out){
}

void bril::supervisor::LuminosityMonitor::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " +e.type() );
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){ 
    std::set< std::string > alltopics;
    m_fastlumitopics = toolbox::parseTokenSet(m_fastlumitopicsStr.value_, ",");
    for(std::set<std::string>::iterator it=m_fastlumitopics.begin(); it!=m_fastlumitopics.end(); ++it ){
      std::string topicname = *it;
      alltopics.insert(topicname);
      SimpleLuminosityData fd;      
      const std::string fastwlname = m_uniqueid+"_publishing_fast"+topicname;		
      toolbox::task::WorkLoop* fastwl = toolbox::task::getWorkLoopFactory()->getWorkLoop(fastwlname, "waiting");
      toolbox::task::ActionSignature* fastas = toolbox::task::bindwitharg(this,&bril::supervisor::LuminosityMonitor::publishing_fastlumi, fastwlname, topicname);
      
      m_fastlumidata.insert( std::make_pair(topicname,fd) );
      m_fastlumi_wlstore.insert( std::make_pair(topicname, std::make_pair(fastwl, fastas) ) );
      fastwl->activate();      
    }
    
    m_lumitopics = toolbox::parseTokenSet(m_lumitopicsStr.value_, ",");
    for(std::set<std::string>::iterator it=m_lumitopics.begin(); it!=m_lumitopics.end(); ++it ){
      std::string topicname = *it;
      alltopics.insert(topicname);
      const std::string wlname = m_uniqueid+"_publishing_"+topicname;		
      toolbox::task::WorkLoop* wl = toolbox::task::getWorkLoopFactory()->getWorkLoop(wlname, "waiting");
      toolbox::task::ActionSignature* as = toolbox::task::bindwitharg(this,&bril::supervisor::LuminosityMonitor::publishing_detlumi, wlname, topicname);
      DetLuminosityData d;
      m_lumidata.insert( std::make_pair(topicname,d) );      
      m_wlstore.insert( std::make_pair(*it, std::make_pair(wl, as) ) );
      wl->activate();
      
    }
    
    for(std::set<std::string>::iterator it=alltopics.begin(); it!=alltopics.end(); ++it ){
      this->getEventingBus(m_bus.value_).subscribe( *it );      
    }

    addActionListener(this);    
  }

}

void bril::supervisor::LuminosityMonitor::actionPerformed( toolbox::Event& e ){
  LOG4CPLUS_DEBUG( this->getApplicationLogger(), "Received toolbox event " + e.type() );   
  if( e.type() == "urn:bril-supervisor-event:LumiSectionChanged" ){
    bril::supervisor::TopicLumiSectionChangedEvent* et = dynamic_cast< bril::supervisor::TopicLumiSectionChangedEvent* >(&e);
    std::string topicname = et->topicname_;
    std::map< std::string, std::pair< toolbox::task::WorkLoop*, toolbox::task::ActionSignature* > >::iterator it = m_wlstore.find(topicname);
    if( it!=m_wlstore.end() ){
      (it->second).first->submit( (it->second).second );
    }
  }
}

bool bril::supervisor::LuminosityMonitor::publishing_detlumi( toolbox::task::WorkLoop* wl, std::string topicname ){      
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Entering publishing "+topicname);
  std::stringstream ss;
  std::map< std::string, DetLuminosityData >::iterator it=m_lumidata.find(topicname);
  if( it!=m_lumidata.end() ){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Before enter guard-publishing ");
    {//begin guard
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    DetLuminosityData& d=it->second;
    xdata::String* p_detectorname = dynamic_cast< xdata::String* >( m_luminosityInfoSpace->find("detectorname") );
    p_detectorname->value_ = topicname;
    xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_luminosityInfoSpace->find("fillnum") );
    p_fillnum->value_ = d.fillnum;
    xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_luminosityInfoSpace->find("runnum") );
    p_runnum->value_ = d.runnum;
    xdata::UnsignedInteger32* p_lsnum = dynamic_cast< xdata::UnsignedInteger32* >( m_luminosityInfoSpace->find("lsnum") );
    p_lsnum->value_ = d.lsnum;
    xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( m_luminosityInfoSpace->find("timestamp") );
    p_timestamp->value_.sec( d.timestampsec ) ;
    p_timestamp->value_.usec( d.timestampmsec*1000 ) ;
    xdata::String* p_calibtag = dynamic_cast< xdata::String* >( m_luminosityInfoSpace->find("calibtag") );
    p_calibtag->value_ = d.calibtag;
    xdata::Float* p_avg = dynamic_cast< xdata::Float* >( m_luminosityInfoSpace->find("avg") );
    p_avg->value_ = d.avg;
    xdata::UnsignedInteger32* p_masklow = dynamic_cast< xdata::UnsignedInteger32* >( m_luminosityInfoSpace->find("masklow") );
    p_masklow->value_ = d.masklow;
    xdata::UnsignedInteger32* p_maskhigh = dynamic_cast< xdata::UnsignedInteger32* >( m_luminosityInfoSpace->find("maskhigh") );
    p_maskhigh->value_ = d.maskhigh;
    xdata::Vector< xdata::Float >* p_bx = dynamic_cast< xdata::Vector< xdata::Float>* >( m_luminosityInfoSpace->find("bx") );
    p_bx->clear();
    
    for( size_t idx=0; idx<3564; ++idx){
      p_bx->push_back( d.bx[idx] );
    }
    ss.clear();
    ss<<"publish LS "<<topicname<<" run "<<d.runnum<<" ls "<<d.lsnum<<" avg "<<d.avg;
    LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str() );
    }//end guard
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Left guard-publishing ");
    try{
      m_luminosityInfoSpace->fireItemGroupChanged( m_flashfields, this);
    }catch(xdata::exception::Exception& e){
      std::string msg("Failed to fire luminosity flashlist for "+topicname);
      LOG4CPLUS_ERROR(getApplicationLogger(), msg+xcept::stdformat_exception_history(e));
      XCEPT_DECLARE_NESTED(bril::supervisor::exception::Exception,myerrorobj,msg,e);
      notifyQualified("error",myerrorobj);
    }
  }
  return false;
}

bool bril::supervisor::LuminosityMonitor::publishing_fastlumi( toolbox::task::WorkLoop* wl, std::string topicname ){      
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Entering publishing "+topicname);

  std::stringstream ss;
  std::map< std::string, SimpleLuminosityData >::iterator it=m_fastlumidata.find(topicname);
  if( it!=m_fastlumidata.end() ){
    xdata::InfoSpace * is = 0;
    if( topicname.find("bestlumi")!=std::string::npos ){
      is = m_fastbestlumiInfoSpace;
    }else{
      is = m_fastlumiInfoSpace;
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Before enter guard-publishing ");
    {//begin guard
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    SimpleLuminosityData& d=it->second;   
    xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( is->find("fillnum") );
    p_fillnum->value_ = d.fillnum;
    xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( is->find("runnum") );
    p_runnum->value_ = d.runnum;
    xdata::UnsignedInteger32* p_lsnum = dynamic_cast< xdata::UnsignedInteger32* >( is->find("lsnum") );
    p_lsnum->value_ = d.lsnum;
    xdata::UnsignedInteger32* p_nbnum = dynamic_cast< xdata::UnsignedInteger32* >( is->find("nbnum") );
    p_nbnum->value_ = d.nbnum;    
    xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( is->find("timestamp") );
    p_timestamp->value_.sec( d.timestampsec ) ;
    p_timestamp->value_.usec( d.timestampmsec*1000 ) ;

    xdata::String* p_detectorname = dynamic_cast< xdata::String* >( is->find("detectorname") );
    p_detectorname->value_ = d.detectorname;
    xdata::Float* p_avg = dynamic_cast< xdata::Float* >( is->find("avg") );
    p_avg->value_ = d.avg;
    
    ss.clear();ss.str("");
    ss<<"publish fast "<<topicname<<" run "<<d.runnum<<" ls "<<d.lsnum<<" nb "<<d.nbnum<<" avg "<<d.avg;
    LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str() );
    }//end guard
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Left guard-publishing ");
    try{
      is->fireItemGroupChanged( m_fastflashfields, this);
    }catch(xdata::exception::Exception& e){
      std::string msg("Failed to fire fast flashlist for "+topicname);
      LOG4CPLUS_ERROR(getApplicationLogger(), msg+xcept::stdformat_exception_history(e));
      XCEPT_DECLARE_NESTED(bril::supervisor::exception::Exception,myerrorobj,msg,e);
      notifyQualified("error",myerrorobj);
    }
  }
  return false;
}

void  bril::supervisor::LuminosityMonitor::init_luminosityFlashlist(){
  std::string fieldname;
  fieldname = "detectorname";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::String("") );

  fieldname = "fillnum";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "runnum";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "lsnum";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "timestamp";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::TimeVal(toolbox::TimeVal()) );
  
  fieldname = "calibtag";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::String("") );

  fieldname = "avg";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::Float(0) );

  fieldname = "masklow";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "maskhigh";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "bx";
  m_flashfields.push_back( fieldname );
  m_flashcontents.push_back( new xdata::Vector< xdata::Float > );
}

void bril::supervisor::LuminosityMonitor::declare_luminosityFlashlist(){
  std::list<std::string>::const_iterator it = m_flashfields.begin();
  std::vector<xdata::Serializable*>::const_iterator valit = m_flashcontents.begin();
  for(;  it!=m_flashfields.end() && valit!=m_flashcontents.end(); ++it, ++valit){
    m_luminosityInfoSpace->fireItemAvailable(*it,*valit);	
  }            
}
    
void  bril::supervisor::LuminosityMonitor::init_fastlumiFlashlist(){
  std::string fieldname;
  fieldname = "detectorname";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::String("") );

  fieldname = "fillnum";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "runnum";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "lsnum";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "nbnum";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "timestamp";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::TimeVal(toolbox::TimeVal()) );

  fieldname = "avg";
  m_fastflashfields.push_back( fieldname );
  m_fastflashcontents.push_back( new xdata::Float(0) );
  
}

void bril::supervisor::LuminosityMonitor::declare_fastlumiFlashlist(){  
  std::list<std::string>::const_iterator it = m_fastflashfields.begin();
  std::vector<xdata::Serializable*>::const_iterator valit = m_fastflashcontents.begin();
  for(;  it!=m_fastflashfields.end() && valit!=m_fastflashcontents.end(); ++it, ++valit){
    m_fastbestlumiInfoSpace->fireItemAvailable(*it,*valit);
    m_fastlumiInfoSpace->fireItemAvailable(*it,*valit);
  }            
}
