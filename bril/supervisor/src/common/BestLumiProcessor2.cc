#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/AutoReference.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/TimeVal.h"
#include "b2in/nub/Method.h"
#include "interface/bril/shared/LUMITopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/HFTopics.hh"
#include "bril/supervisor/WebUtils.h"
#include "bril/supervisor/BestLumiProcessor2.h"
#include "bril/supervisor/exception/Exception.h"

#include <cstdio>
#include <cmath>

XDAQ_INSTANTIATOR_IMPL(bril::supervisor::BestLumiProcessor2)

namespace bril{
namespace supervisor {
static int maxnbx=3564;

BestLumiProcessor2::BestLumiProcessor2(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL),m_plock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::supervisor::BestLumiProcessor2::Default, "Default");
    b2in::nub::bind(this, &bril::supervisor::BestLumiProcessor2::onMessage);
    m_minbias = 78400;
    m_bus.fromString("brildata");//default
    m_tcdstopic = interface::bril::tcdsT::topicname();
    m_medianband_upper = 3;
    m_medianband_lower = 0.33;
    m_excludeband_lower = 0;
    m_excludeband_upper = 5;
    
    m_fillnum = 0;
    m_runnum = 0;
    m_lsnum = 0;
    m_nbnum = 0;
    m_tssec = 0;
    m_tsmsec = 0;
    //m_lastnbnum = 0;
    m_lasttcdscount = m_tcdscount = 0;
    m_deadtimefrac = 1.0;
    m_cmson = false;
    m_norb = 0;
    m_nbperls = 0;
    m_ncollidingbx = 0;
    m_delivered = -99;
    m_recorded = -99;
    m_avgpu = 0; 
    m_storable = false;
    m_instanceid = 0;
    m_lumistorethreshold = 1.0e-4;
    if( getApplicationDescriptor()->hasInstanceNumber() ){
        m_instanceid = getApplicationDescriptor()->getInstance();
    }

    defineLumiMonTable();

    try{
        getApplicationInfoSpace()->fireItemAvailable("nbnum",&m_nbnum);
        getApplicationInfoSpace()->fireItemAvailable("minbias",&m_minbias);
        getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
        getApplicationInfoSpace()->fireItemAvailable("inTopics",&m_inTopicStr);
        getApplicationInfoSpace()->fireItemAvailable("lumiPriority",&m_lumiPriorityStr);
        getApplicationInfoSpace()->fireItemAvailable("lumistorethreshold",&m_lumistorethreshold);
	getApplicationInfoSpace()->fireItemAvailable("medianbandUpper",&m_medianband_upper);
	getApplicationInfoSpace()->fireItemAvailable("medianbandLower",&m_medianband_lower);
	getApplicationInfoSpace()->fireItemAvailable("excludebandUpper",&m_excludeband_upper);
	getApplicationInfoSpace()->fireItemAvailable("excludebandLower",&m_excludeband_lower);
        getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
        getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
        toolbox::net::URN memurn("toolbox-mem-pool","brilBestLumiProcessor2_mem");
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_instanceid.toString()+"_publishing","waiting");
        m_as_publishing = toolbox::task::bind(this,&bril::supervisor::BestLumiProcessor2::publishing,"publishing");

    }catch(xcept::Exception & e){
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to set up infospace or memory pool", e);
    }
    try{
        std::string nid("lumiMon");
        std::string monurn = createQualifiedInfoSpace(nid).toString();
        m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));
        m_monInfoSpace->fireItemAvailable("nbnum",&m_nbnum);
        m_monItemList.push_back("nbnum");
        m_monInfoSpace->fireItemAvailable("lsnum",&m_lsnum);
        m_monItemList.push_back("lsnum");
        m_monInfoSpace->fireItemAvailable("runnum",&m_runnum);
        m_monItemList.push_back("runnum");
        m_monInfoSpace->fireItemAvailable("fillnum",&m_fillnum);
        m_monItemList.push_back("fillnum");
        m_monInfoSpace->fireItemAvailable("inTopics",&m_inTopicStr);
        m_monItemList.push_back("inTopics");
        m_monInfoSpace->fireItemAvailable("lumiPriority",&m_lumiPriorityStr);
        m_monItemList.push_back("lumiPriority");
        m_monInfoSpace->fireItemAvailable("minbias",&m_minbias);
        m_monItemList.push_back("minbias");
        m_monInfoSpace->fireItemAvailable("deadtimefrac",&m_deadtimefrac);
        m_monItemList.push_back("deadtimefrac");
        m_monInfoSpace->fireItemAvailable("cmson",&m_cmson);
        m_monItemList.push_back("cmson");
        m_monInfoSpace->fireItemAvailable("norb",&m_norb);
        m_monItemList.push_back("norb");
        m_monInfoSpace->fireItemAvailable("nbperls",&m_nbperls);
        m_monItemList.push_back("nbperls");
        m_monInfoSpace->fireItemAvailable("ncollidingbx",&m_ncollidingbx);
        m_monItemList.push_back("ncollidingbx");
        m_monInfoSpace->fireItemAvailable("delivered",&m_delivered);
        m_monItemList.push_back("delivered");
        m_monInfoSpace->fireItemAvailable("recorded",&m_recorded);
        m_monItemList.push_back("recorded");
        m_monInfoSpace->fireItemAvailable("lumiprovider",&m_lumiprovider);
        m_monItemList.push_back("lumiprovider");
        m_monInfoSpace->fireItemAvailable("avgpu",&m_avgpu);
        m_monItemList.push_back("avgpu");
        m_monInfoSpace->fireItemAvailable("lumimonTable",&m_mon_lumitable);
        m_monItemList.push_back("lumimonTable");

        m_mon_configtable.insert(std::make_pair("inTopics",&m_inTopicStr));
        m_mon_configtable.insert(std::make_pair("lumiPriority",&m_lumiPriorityStr));
        m_mon_configtable.insert(std::make_pair("minBias",&m_minbias));

        m_mon_runtable.insert(std::make_pair("fill",&m_fillnum));
        m_mon_runtable.insert(std::make_pair("run",&m_runnum));
        m_mon_runtable.insert(std::make_pair("lsnum",&m_lsnum));
        m_mon_runtable.insert(std::make_pair("nbnum",&m_nbnum));

        m_mon_tcdstable.insert(std::make_pair("deadtimefrac",&m_deadtimefrac));
        m_mon_tcdstable.insert(std::make_pair("cmson",&m_cmson));
        m_mon_tcdstable.insert(std::make_pair("norb",&m_norb));
        m_mon_tcdstable.insert(std::make_pair("nbperls",&m_nbperls));
        m_mon_tcdstable.insert(std::make_pair("ncollidingbx",&m_ncollidingbx));

        m_mon_besttable.insert(std::make_pair("lumiprovider",&m_lumiprovider));
        m_mon_besttable.insert(std::make_pair("delivered",&m_delivered));
        m_mon_besttable.insert(std::make_pair("recorded",&m_recorded));
        m_mon_besttable.insert(std::make_pair("avgpu",&m_avgpu));

        m_monInfoSpace->addGroupRetrieveListener(this);
    }catch(xdata::exception::Exception& e){
        std::stringstream msg;
        msg<<"Failed to fire monitoring items";
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        XCEPT_RETHROW(bril::supervisor::exception::Exception,msg.str(),e);
    }catch(xdaq::exception::Exception& e){
        std::stringstream msg;
        msg<<"Failed to create infospace ";
        LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
        XCEPT_RETHROW(bril::supervisor::exception::Exception,stdformat_exception_history(e),e);
    }catch(std::exception& e){
        LOG4CPLUS_ERROR(getApplicationLogger(),"std error "+std::string(e.what()));
        XCEPT_RAISE(bril::supervisor::exception::Exception,e.what());
    }
}

BestLumiProcessor2::~BestLumiProcessor2(){
  //    do_clearpriority();
}

void BestLumiProcessor2::Default(xgi::Input * in, xgi::Output * out){
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+getApplicationDescriptor()->getURN();
    *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
    *out << "<p> Eventing Status</p>";
    *out <<busesToHTML();
    m_monInfoSpace->lock();
    m_monInfoSpace->fireItemGroupRetrieve(m_monItemList,this);
    m_monInfoSpace->unlock();
    *out << bril::supervisor::WebUtils::mapToHTML("Configuration",m_mon_configtable,"",false);
    *out << bril::supervisor::WebUtils::mapToHTML("Run Status",m_mon_runtable,"",false);
    *out << bril::supervisor::WebUtils::mapToHTML("TCDS Status",m_mon_tcdstable,"",false);
    *out << bril::supervisor::WebUtils::mapToHTML("Best Lumi",m_mon_besttable,"",false);
    *out << "<br>";
    *out << "<p> Incoming Lumi Data</p>";
    *out << bril::supervisor::WebUtils::xdatatableToHTML(&m_mon_lumitable);
}

void BestLumiProcessor2::actionPerformed(xdata::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
      if( m_excludeband_lower.value_>=m_excludeband_upper.value_ ){
	XCEPT_RAISE(bril::supervisor::exception::BestLumiProcessor,"excludebandLower>=excludebandUpper");
      }
      if( m_medianband_lower.value_>=m_medianband_upper.value_ ){
	XCEPT_RAISE(bril::supervisor::exception::BestLumiProcessor,"medianbandLower>=medianbandUpper" );
      }
      try{	  
	if(!m_inTopicStr.value_.empty()){
	  m_intopics = toolbox::parseTokenSet(m_inTopicStr.value_,",");
	}
        
	init_bestlumiFlash();
        
	if(!m_lumiPriorityStr.value_.empty()){
	  std::list<std::string> li = toolbox::parseTokenList(m_lumiPriorityStr.value_,",");
	  std::copy(li.begin(), li.end(), std::back_inserter(m_lumipriority) );
	}
	initializeLumiMonTable();
	this->getEventingBus(m_bus.value_).addActionListener(this);
	
	//activate workloop
	m_publishing->activate();
      }catch(eventing::api::exception::Exception& e){
	std::string msg("Failed to listen to eventing bus "+m_bus.value_);
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	XCEPT_RETHROW(exception::Exception,msg,e);
      }catch(xcept::Exception& e){
	std::string msg("Failed to setup infospaces for flashlists");
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	XCEPT_RETHROW(exception::Exception,msg,e);
      }
    }else if ( e.type() == "ItemChangedEvent"){
      std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
      if(item=="nbnum"){
	//submit workloop
	//LOG4CPLUS_INFO(getApplicationLogger(),"start publishing workloop");
	m_publishing->submit(m_as_publishing);
      }
    }
}

void BestLumiProcessor2::actionPerformed(toolbox::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
    if ( e.type() == "eventing::api::BusReadyToPublish" ){
        std::set<std::string>::iterator it;
        for(it=m_intopics.begin(); it!=m_intopics.end(); ++it){
            this->getEventingBus(m_bus.value_).subscribe(*it);
        }
    }
}

void BestLumiProcessor2::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify" && ref!=0){
    std::stringstream msg;
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");   
    std::string ignore = plist.getProperty("IGNORE");
    if( !ignore.empty() ){
      LOG4CPLUS_INFO(getApplicationLogger(),"IGNORE message from "+topic);
      return;
    }
    std::string data_version = plist.getProperty("DATA_VERSION");
    std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
    if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
      std::string msg("Received brildaq message "+topic+" with no or wrong version, do not process.");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      return;
    }
    if(payloaddict.empty()){
      msg<<"Received "<<topic<<" with no dictionary, do not process";
      LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
      return;
    }
    interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
    msg<<"received "<<topic<<": fill "<<thead->fillnum<<", run "<<thead->runnum<<", ls "<<thead->lsnum<<", " <<" nb "<<thead->nbnum;
    LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
    msg.str("");msg.clear();
    std::vector<std::string>::iterator lit = std::find(m_lumipriority.begin(),m_lumipriority.end(),topic);
    if( topic == m_tcdstopic ){
      m_applock.take();
      m_fillnum.value_ = thead->fillnum;
      m_nbnum.value_ = thead->nbnum;
      m_lsnum.value_ = thead->lsnum;
      m_runnum.value_ = thead->runnum;
      m_tssec.value_ = thead->timestampsec;
      m_tsmsec.value_ = thead->timestampmsec;
      m_deadtimefrac.value_ = -1.;
      m_tcdscount++;
      m_applock.give();
      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
      unsigned int norb = 0;
      unsigned int nbperls = 0;
      bool cmson = false;
      unsigned int ncollidingbx = 0;
      float dtfrac = 1.;
      tc.extract_field( &norb, "norb", thead->payloadanchor );
      tc.extract_field( &nbperls, "nbperls", thead->payloadanchor );
      tc.extract_field( &cmson, "cmson", thead->payloadanchor );
      tc.extract_field( &dtfrac, "deadfrac", thead->payloadanchor );
      tc.extract_field( &ncollidingbx, "ncollidingbx", thead->payloadanchor );
      if( isnan(dtfrac)||isinf(dtfrac) ){
	LOG4CPLUS_ERROR(getApplicationLogger(), "deadfrac is Nan or Inf, reset to 1.");
	dtfrac = 1.;
      }
      msg.str("");
      msg<<"norb "<<norb<<" nbperls "<<nbperls<<" cmson "<<cmson<<" deadtime "<<dtfrac<<" ncollidingbx "<<ncollidingbx;
      m_applock.take();
      m_deadtimefrac.value_ = dtfrac;
      m_cmson.value_ = cmson;
      m_norb.value_ = norb;
      m_nbperls.value_ = nbperls;
      m_ncollidingbx.value_ = ncollidingbx;
      m_applock.give();
      LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
    }else if( topic == interface::bril::beamT::topicname() ){
      char beamstatus[50];
      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
      tc.extract_field( beamstatus, "status", thead->payloadanchor );
      m_beamstatus.value_ = std::string(beamstatus);
      m_storable = isDataStorable( std::string(beamstatus) );
    }else if( lit!=m_lumipriority.end() ){
      // intercept lumi message for monitoring
      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
      interface::bril::shared::DatumHead * mhead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());  
      LumiTopicItem thislumi(*lit,m_lumipriority);      
      thislumi.fillnum = thead->fillnum;
      thislumi.runnum = thead->runnum;
      thislumi.lsnum = thead->lsnum;
      thislumi.nbnum = thead->nbnum;
      thislumi.timestampsec = thead->timestampsec;
      thislumi.timestampmsec = thead->timestampmsec;
      thislumi.avg = -1;
      thislumi.avgraw = -1;     
      for(int i=0;i<bril::supervisor::maxnbx;++i) {
	thislumi.bx[i]=0;
	thislumi.bxraw[i]=0;
      }
      thislumi.maskhigh = 0;
      thislumi.masklow = 0;
      thislumi.calibtag = "";

      if(thislumi.fillnum<=0 || thislumi.fillnum>9999 || thislumi.runnum<=0 || thislumi.runnum>999999 || thislumi.lsnum<=0 || thislumi.lsnum>999999 ){
	//filter out out of range time headers
	LOG4CPLUS_INFO(getApplicationLogger(), "discard time header out of range from "+topic);
	return;
      }      
      tc.extract_field( &thislumi.avg, "avg", mhead->payloadanchor );
      tc.extract_field( &thislumi.avgraw, "avgraw", mhead->payloadanchor );
      if( payloaddict.find("bx")!=std::string::npos ){
	tc.extract_field( thislumi.bx, "bx", mhead->payloadanchor );
	tc.extract_field( thislumi.bxraw, "bxraw", mhead->payloadanchor );
      }
      tc.extract_field( &thislumi.masklow, "masklow", mhead->payloadanchor );
      tc.extract_field( &thislumi.maskhigh, "maskhigh", mhead->payloadanchor );
      char calibtag[50];
      tc.extract_field( calibtag, "calibtag", mhead->payloadanchor );
      thislumi.calibtag = std::string(calibtag);

      if( isnan(thislumi.avg)||isinf(thislumi.avg) ){
	LOG4CPLUS_ERROR(getApplicationLogger(), topic+": avg is Nan or Inf, discarding");
	return;
      }
      if( isnan(thislumi.avgraw)||isinf(thislumi.avgraw) ){
	LOG4CPLUS_ERROR(getApplicationLogger(), topic+": avgraw is Nan or Inf, reset to 0.");
	thislumi.avgraw = 0.;
      }
      bool bxbroke = false;
      bool bxrawbroke = false;
      for( int i=0; i<bril::supervisor::maxnbx; ++i){
	if( isnan(thislumi.bx[i])||isinf(thislumi.bx[i]) ){ //if found one corrupt bunch, reset the whole array
	  bxbroke = true;	  
	}
	if( isnan(thislumi.bxraw[i])||isinf(thislumi.bxraw[i]) ){ //if found one corrupt bunch, reset the whole array
	  bxrawbroke = true;
	}
      }
      if(bxbroke){
	LOG4CPLUS_ERROR(getApplicationLogger(), topic+": bx contains Nan or Inf, reset all to 0.");	
	memset(thislumi.bx,0,sizeof(float)*bril::supervisor::maxnbx);
      }
      if(bxrawbroke){
	LOG4CPLUS_ERROR(getApplicationLogger(), topic+": bxraw contains Nan or Inf, reset all to 0.");
	memset(thislumi.bxraw,0,sizeof(float)*bril::supervisor::maxnbx);
      }
      msg.clear(); msg.str("");
      msg<<"calibtag "<<thislumi.calibtag<<" avg "<<thislumi.avg<<" avgraw "<<thislumi.avgraw;
      LOG4CPLUS_INFO( getApplicationLogger(), msg.str() );
      // hyperdaq mon
      xdata::Float xavg(thislumi.avg);
      xdata::Float xavgraw(thislumi.avgraw);
      xdata::String xcalibtag;
      xcalibtag.fromString(thislumi.calibtag);
      size_t rowid = std::distance(m_lumipriority.begin(),lit);
      xdata::String xts;
      msg.clear();msg.str("");
      msg<<thislumi.fillnum<<" "<<thislumi.runnum<<" "<<thislumi.lsnum<<" "<<thislumi.nbnum<<" "<<thislumi.timestampsec<<"."<<thislumi.timestampmsec;
      xts.fromString(msg.str());
      xdata::String xtopic; xtopic.fromString(topic);
      m_mon_lumitable.setValueAt(rowid,"Topic",xtopic);
      m_mon_lumitable.setValueAt(rowid,"Avg",xavg);
      m_mon_lumitable.setValueAt(rowid,"AvgRaw",xavgraw);
      m_mon_lumitable.setValueAt(rowid,"Calibtag",xcalibtag);
      m_mon_lumitable.setValueAt(rowid,"TimeInfo",xts);
      
      std::vector<int> nonzerobx;
      float maxlumi = -99;
      unsigned int maxbx = 0;
      for(int i=0; i<bril::supervisor::maxnbx;++i ){
	if(thislumi.bx[i]>0) {
	  nonzerobx.push_back(i);
	}
	if(thislumi.bx[i]>maxlumi){
	  maxbx = i;
	  maxlumi = thislumi.bx[i];
	}
      }
      xdata::UnsignedInteger nlumibx(nonzerobx.size());
      xdata::UnsignedInteger xmaxbx(maxbx);
      m_mon_lumitable.setValueAt(rowid,"NFilledBX",nlumibx);
      m_mon_lumitable.setValueAt(rowid,"MaxBXId",xmaxbx);
      
      // push thislumi into priority queue      
      m_plock.take();
      m_pq.enqueue(thislumi);
      m_plock.give();
    }
    //if( m_nbnum.value_ != m_lastnbnum ){ //use nbnum change as signal does not work if someone screw up the nb number
    if( m_lasttcdscount!=m_tcdscount ){//here we require only tcds signal. we don't look into nbnum values
      getApplicationInfoSpace()->fireItemValueChanged("nbnum",this);//first (4) nbnum change
      m_lasttcdscount = m_tcdscount;
    }
  }
}

void BestLumiProcessor2::do_toppriority(){
  LumiTopicItem t = m_pq.top();
  do_publish_bestlumi( t );
  while( !m_pq.empty() ){
    m_pq.pop();
  }
}
bool BestLumiProcessor2::publishing(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering publishing");
    usleep(700000);    
    m_plock.take();
    if( m_pq.empty() ){
      LOG4CPLUS_INFO(getApplicationLogger(), "Nothing to publish");
      m_plock.give();
      return false;
    }
    if( m_beamstatus.value_!="STABLE BEAMS" ){
      LOG4CPLUS_INFO(getApplicationLogger(), "Non STABLE BEAMS, keep top priority");
    }else if(m_pq.allinrange(m_excludeband_lower.value_,m_excludeband_upper.value_) ){
      LOG4CPLUS_INFO(getApplicationLogger(), "All in exclude band, keep top priority");
    }else if( m_pq.size()<2 ){
      LOG4CPLUS_INFO(getApplicationLogger(), "Non majority, keep top priority");
    }else{
      std::stringstream ss;
      float medianavg = 0;
      try{
	medianavg = m_pq.median_avglumi();
      }catch( const bril::supervisor::exception::MedianOnEmptyContainer& er){
	LOG4CPLUS_ERROR(getApplicationLogger(), "Median on empty container, do nothing");
	m_plock.give();
	return false;
      }
      float upper = medianavg*(1.+m_medianband_upper.value_);
      float lower = medianavg*m_medianband_lower.value_;
      ss<<"Medianband "<<lower<<" "<<medianavg<<" "<<upper;
      LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
      while( !m_pq.empty() ){
	if( m_pq.top().avg>upper || m_pq.top().avg<lower ){
	  ss.clear();ss.str("");
	  ss<<"Reject "<<m_pq.top().topicname()<<" "<<m_pq.top().avg<<" out of median band";
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  m_pq.pop();
	  continue;
	}
	break;
      }      
    }
    do_toppriority();      
    m_plock.give();
    return false;
}

bool BestLumiProcessor2::do_publish_bestlumi( const LumiTopicItem& lumi ){
    std::stringstream msg;
    // default values
    std::string lumiprovider = lumi.topicname();
    float delivered = 0.;
    float recorded = 0.;
    float avgpu = 0.;
    float bxdelivered[bril::supervisor::maxnbx];
    for(int i=0; i<bril::supervisor::maxnbx; ++i) bxdelivered[i]=0.;
    
    //hack for DB
    if ( lumiprovider.find("pltlumizero")!=std::string::npos ){
        lumiprovider = "PLTZERO";
    }else if ( lumiprovider.find("pltlumi")!=std::string::npos ){
        lumiprovider = "PLT";
    }else if ( lumiprovider.find("bcm1flumi")!=std::string::npos ){
        lumiprovider = "BCM1F";
    }else if( lumiprovider.find("hfoclumi")!=std::string::npos||lumiprovider.find("hflumi")!=std::string::npos ){
        lumiprovider = "HFOC";
    }else if( lumiprovider.find("hfetlumi")!=std::string::npos ){
        lumiprovider = "HFET";
    }else if( lumiprovider.find("bcm1futca")!=std::string::npos ){
        lumiprovider = "BCM1FUT";
    }
   
    //if(m_ncollidingbx.value_>0){
    if( m_beamstatus.value_=="STABLE BEAMS" || m_beamstatus.value_=="ADJUST" ){
        float rotationrate = 11245.613;
	if( m_ncollidingbx.value_>0 ){
	  avgpu = (lumi.avg/m_ncollidingbx.value_)*float(m_minbias.value_)/rotationrate;
	}
    }else{
      avgpu = 0;
    }
    delivered = lumi.avg;
    recorded = lumi.avg*(1.-m_deadtimefrac.value_);
    for(int i=0; i<bril::supervisor::maxnbx; ++i){
      bxdelivered[i] = lumi.bx[i];
    }
    m_applock.take();
    m_delivered.value_ = delivered;
    m_recorded.value_ = recorded;
    m_avgpu.value_ = avgpu;
    m_lumiprovider.value_ = lumiprovider;
    m_applock.give();

    // build bestlumi
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",interface::bril::shared::bestlumiT::payloaddict());
    if( !m_storable || delivered<m_lumistorethreshold.value_ ){
        plist.setProperty("NOSTORE","1");
        LOG4CPLUS_DEBUG(getApplicationLogger(),"NOSTORE bestlumi");
    }
    toolbox::mem::Reference* bestlumiRef = 0;
    size_t totalsize = interface::bril::shared::bestlumiT::maxsize();
    std::string pdict = interface::bril::shared::bestlumiT::payloaddict();
    //std::cout<<"bestlumidict "<<pdict<<" totalsize "<<totalsize<<std::endl;
    bestlumiRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bestlumiRef->setDataSize(totalsize);
    interface::bril::shared::DatumHead* bestlumiheader = (interface::bril::shared::DatumHead*)(bestlumiRef->getDataLocation());
    bestlumiheader->setTime(lumi.fillnum,lumi.runnum,lumi.lsnum,lumi.nbnum,lumi.timestampsec,lumi.timestampmsec);
    bestlumiheader->setResource(interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND);
    bestlumiheader->setFrequency(4);
    bestlumiheader->setTotalsize(totalsize);
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    tc.insert_field( bestlumiheader->payloadanchor, "provider", lumiprovider.c_str() );
    tc.insert_field( bestlumiheader->payloadanchor, "calibtag", lumi.calibtag.c_str() );
    tc.insert_field( bestlumiheader->payloadanchor, "delivered", &delivered );
    tc.insert_field( bestlumiheader->payloadanchor, "recorded", &recorded );
    tc.insert_field( bestlumiheader->payloadanchor, "avgpu", &avgpu );
    tc.insert_field( bestlumiheader->payloadanchor, "bxdelivered", bxdelivered );   
    
    try{
      xdata::UnsignedInteger32* p_runnum = dynamic_cast<xdata::UnsignedInteger32* >( m_bestlumiIS->find("runnum") );
      xdata::UnsignedInteger32* p_lsnum = dynamic_cast<xdata::UnsignedInteger32* >( m_bestlumiIS->find("lsnum") );
      if( (p_lsnum->value_!=lumi.lsnum) || (p_runnum->value_!=lumi.runnum) ){
	p_runnum->value_ = lumi.runnum;
	p_lsnum->value_ = lumi.lsnum;
	xdata::UnsignedInteger32* p_fillnum = dynamic_cast<xdata::UnsignedInteger32* >( m_bestlumiIS->find("fillnum") );
	p_fillnum->value_ = lumi.fillnum;
	xdata::TimeVal* p_timestamp = dynamic_cast<xdata::TimeVal* >( m_bestlumiIS->find("timestamp") );
	p_timestamp->value_.sec(lumi.timestampsec);
	p_timestamp->value_.usec(lumi.timestampmsec*1000);
	xdata::String* p_provider = dynamic_cast<xdata::String* >( m_bestlumiIS->find("provider") );
	p_provider->value_ = lumiprovider;
	xdata::Integer* p_ncollidingbx = dynamic_cast<xdata::Integer* >( m_bestlumiIS->find("ncollidingbx") );
	p_ncollidingbx->value_ = (int)m_ncollidingbx.value_;
	xdata::Float* p_avg = dynamic_cast<xdata::Float* >( m_bestlumiIS->find("avg") );
	p_avg ->value_ = lumi.avg;
	xdata::Float* p_avgpu = dynamic_cast<xdata::Float* >( m_bestlumiIS->find("avgpu") );
	p_avgpu->value_ = avgpu;
	xdata::Float* p_deadtimefrac = dynamic_cast<xdata::Float* >( m_bestlumiIS->find("deadtimefrac") );
	p_deadtimefrac->value_ = m_deadtimefrac.value_;
	xdata::String* p_beamstatus = dynamic_cast<xdata::String* >( m_bestlumiIS->find("beamstatus") );
	p_beamstatus->value_ = m_beamstatus.value_;
	msg.clear(); msg.str("");
	msg<<"Firing bestlumi flashlist provider "<<p_fillnum->toString()<<" fill "<<p_fillnum->toString()<<" run "<<p_runnum->toString()<<" ls "<<p_lsnum->toString()<<" beamstatus "<<p_beamstatus->toString()<<" avg "<<p_avg->toString()<<" avgpu "<<p_avgpu->toString()<<" deadfrac "<<p_deadtimefrac->toString()<<" nbx "<<p_ncollidingbx->toString();
	LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
	m_bestlumiIS->fireItemGroupChanged(m_bestlumiFlashFields,this);
      }      
    }catch(xdata::exception::Exception& e){
      std::string msg("Failed to find item in infospace for flashlist of bestlumi");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
      this->notifyQualified("error",myerrorobj);
    }      
  
    msg.clear();msg.str("");
    msg<<"Publishing Bestlumi provider "<<lumiprovider<<" calibtag "<<lumi.calibtag<<" delivered(Hz/ub) "<<delivered<<" recorded(Hz/ub) "<<recorded<<" avgpu "<<avgpu <<" fill "<<lumi.fillnum<<" run "<<lumi.runnum<<" ls "<<lumi.lsnum<<" nbnum "<<lumi.nbnum<<" tssec "<<lumi.timestampsec;
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    try{
      getEventingBus(m_bus.value_).publish(interface::bril::shared::bestlumiT::topicname(),bestlumiRef ,plist);
    }catch(xcept::Exception& e){
      msg<<"Failed to publish bestlumi to "<<m_bus.value_;
      LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
      if(bestlumiRef) bestlumiRef->release();
      XCEPT_DECLARE_NESTED(exception::Exception,myerrorobj,msg.str(),e);
      this->notifyQualified("fatal",myerrorobj);
      return false;
    }

    return true;
}

void BestLumiProcessor2::defineLumiMonTable(){
    m_mon_lumitable.addColumn("Topic","string");
    m_mon_lumitable.addColumn("Calibtag","string");
    m_mon_lumitable.addColumn("Avg","float");
    m_mon_lumitable.addColumn("AvgRaw","float");
    m_mon_lumitable.addColumn("NFilledBX","unsigned int");
    m_mon_lumitable.addColumn("MaxBXId","unsigned int");
    m_mon_lumitable.addColumn("TimeInfo","string");
}

void BestLumiProcessor2::initializeLumiMonTable(){
    for(std::vector<std::string>::iterator it=m_lumipriority.begin(); it!=m_lumipriority.end(); ++it){
        size_t row = std::distance(m_lumipriority.begin(),it);
        xdata::String lumitopic;lumitopic.fromString(*it);
        m_mon_lumitable.setValueAt(row,"Topic",lumitopic);
        xdata::String calib; calib.fromString("UNKNOWN");
        m_mon_lumitable.setValueAt(row,"Calibtag",calib);
        xdata::Float avglumi(-1);
        m_mon_lumitable.setValueAt(row,"Avg",avglumi);
        xdata::Float avgrawlumi(-1);
        m_mon_lumitable.setValueAt(row,"AvgRaw",avgrawlumi);
        xdata::UnsignedInteger nbx(0);
        m_mon_lumitable.setValueAt(row,"NFilledBX",nbx);
        xdata::UnsignedInteger maxbx(0);
        m_mon_lumitable.setValueAt(row,"MaxBXId",maxbx);
    }
}

bool BestLumiProcessor2::isDataStorable(const std::string& beamstatus) const{
    if(beamstatus.empty()) return true;
    if(beamstatus!="STABLE BEAMS" && beamstatus!="SQUEEZE" && beamstatus!="ADJUST" && beamstatus!="FLAT TOP"){
        return false;
    }
    return true;
}

void BestLumiProcessor2::init_bestlumiFlash(){
  std::string urn = createQualifiedInfoSpace("lumimon_bestlumi").toString();          
  m_bestlumiIS = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(urn));
  std::vector< xdata::Serializable*> fieldvalues;
  std::string fieldname;

  fieldname = "fillnum";
  fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
  m_bestlumiFlashFields.push_back( fieldname );

  fieldname = "runnum";
  fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "lsnum";
  fieldvalues.push_back( new xdata::UnsignedInteger32(0) );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "timestamp";
  fieldvalues.push_back( new xdata::TimeVal( toolbox::TimeVal() ) );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "provider";
  fieldvalues.push_back( new xdata::String("") );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "ncollidingbx";
  fieldvalues.push_back( new xdata::Integer(0) );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "avg";
  fieldvalues.push_back( new xdata::Float(0) );
  m_bestlumiFlashFields.push_back( fieldname );

  fieldname = "avgpu";
  fieldvalues.push_back( new xdata::Float(0.) );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "deadtimefrac";
  fieldvalues.push_back( new xdata::Float(0.) );
  m_bestlumiFlashFields.push_back( fieldname );
  
  fieldname = "beamstatus";
  fieldvalues.push_back( new xdata::String("") );
  m_bestlumiFlashFields.push_back( fieldname );
 
  std::list<std::string>::const_iterator nIt = m_bestlumiFlashFields.begin();
  std::vector<xdata::Serializable*>::const_iterator vIt = fieldvalues.begin();
  for(; nIt!=m_bestlumiFlashFields.end() && vIt!=fieldvalues.end(); ++nIt, ++vIt){
    m_bestlumiIS->fireItemAvailable(*nIt,*vIt);
  }
}

}}//ns bril::supervisor
