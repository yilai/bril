#ifndef _bril_BestLumiProcessor_h_
#define _bril_BestLumiProcessor_h_
#include <string>
#include <set>
#include <queue>
#include <vector>
#include <algorithm>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "toolbox/task/TimerListener.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/Boolean.h"
#include "xdata/Table.h"
#include "xdata/Vector.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/Reference.h"

namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{ namespace supervisor {    
    
    class TopicItem{
    public:
    TopicItem(const std::string& name,
	      const std::string& payloaddict,
	      toolbox::mem::Reference* ref,
	      const std::vector<std::string>& prioritylist):m_name(name),m_payloaddict(payloaddict),m_ref(ref){
	std::vector<std::string>::const_iterator it = std::find(prioritylist.begin(),prioritylist.end(),name);
	m_pos = it - prioritylist.begin();
      }   
      bool operator>(const TopicItem& rhs)const{
	return m_pos>rhs.m_pos;
      }
      friend std::ostream& operator<<(std::ostream& os, const TopicItem& ti){
	return os << ti.m_name <<" : "<< ti.m_pos;
      }
      toolbox::mem::Reference* ref(){return m_ref;}
      std::string name()const{return m_name;}
      std::string payloaddict()const{return m_payloaddict;}
      size_t priority()const{return m_pos;}
      void setRef(toolbox::mem::Reference* ref){m_ref=ref;}
      void releaseRef(){
	if(m_ref){
	  m_ref->release();
	  m_ref = 0;
	}
      }
    private:
      std::string m_name;
      std::string m_payloaddict;
      size_t m_pos;
      toolbox::mem::Reference* m_ref;
    };    

    class BestLumiProcessor : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      BestLumiProcessor(xdaq::ApplicationStub* s);
      // destructor
      ~BestLumiProcessor();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      //workloop 
      bool publishing(toolbox::task::WorkLoop* wl);
    protected:      
      // configuration parameters
      xdata::UnsignedInteger32 m_minbias;
      xdata::String m_bus;
      std::string m_tcdstopic;
      xdata::String m_lumiPriorityStr;
      std::vector<std::string> m_lumipriority;
      xdata::String m_inTopicStr;
      std::set<std::string> m_intopics;
      
      // caches
      xdata::UnsignedInteger32 m_fillnum;
      xdata::UnsignedInteger32 m_runnum;
      xdata::UnsignedInteger32 m_lsnum;
      xdata::UnsignedInteger32 m_nbnum;
      xdata::UnsignedInteger32 m_tssec;
      xdata::UnsignedInteger32 m_tsmsec;      
      xdata::UnsignedInteger m_instanceid;
      xdata::Float m_lumistorethreshold;
      std::priority_queue< TopicItem, std::vector<TopicItem>, std::greater<TopicItem> > m_pq;
      toolbox::mem::Pool* m_memPool;
      toolbox::BSem m_applock;
      toolbox::BSem m_plock;
      toolbox::task::ActionSignature* m_as_publishing;
      toolbox::task::WorkLoop* m_publishing;
      unsigned int m_lasttcdscount;
      unsigned int m_tcdscount;
      // monitoring
      xdata::InfoSpace *m_monInfoSpace;
      std::list<std::string> m_monItemList;
      std::map<std::string, xdata::Serializable*> m_mon_configtable;
      std::map<std::string, xdata::Serializable*> m_mon_runtable;
      std::map<std::string, xdata::Serializable*> m_mon_besttable;
      std::map<std::string, xdata::Serializable*> m_mon_tcdstable;
      xdata::Table m_mon_lumitable;

      xdata::Float m_deadtimefrac;
      xdata::Boolean m_cmson;
      xdata::UnsignedInteger32 m_norb;
      xdata::UnsignedInteger32 m_nbperls;
      xdata::UnsignedInteger32 m_ncollidingbx;
      xdata::Float m_delivered;
      xdata::Float m_recorded;
      xdata::String m_lumiprovider;
      xdata::Float m_avgpu;
      xdata::String m_beamstatus;
      bool m_storable;
      
      xdata::Properties m_topicToflash; //topic name to flash name
      std::map< std::string,xdata::InfoSpace* > m_flashinfostore; //topic name to flash infospace  
      std::map< xdata::InfoSpace*, std::list<std::string> > m_flashfields;//p_infospace, field names
      std::map< xdata::InfoSpace*, std::vector< xdata::Serializable*> > m_flashcontent; //p_infospace, field values
      
    private: 

      bool do_publish_bestlumi(const std::string& lumitopic, const std::string& payloaddict, toolbox::mem::Reference* bufRef);
      void do_clearpriority();
      void defineLumiMonTable();
      void initializeLumiMonTable();
      bool isDataStorable(const std::string& beamstatus) const;
      void init_lumiFlash(xdata::InfoSpace* is, const std::string& topicname);
      void init_detlumiFlash(xdata::InfoSpace* is);
      void init_bestlumiFlash(xdata::InfoSpace* is);
      void fireFlashlist(const std::string& flashname);
      void declareFlashlists();
      void cleanFlashlists();
      // nocopy protection
      BestLumiProcessor (const BestLumiProcessor&);
      BestLumiProcessor & operator=(const BestLumiProcessor&);
  };
  }}
#endif
