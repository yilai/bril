#ifndef _bril_BunchPatternProcessor_h_
#define _bril_BunchPatternProcessor_h_
#include <string>
#include <list>
#include <vector>
#include <map>
#include <iterator>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "toolbox/task/TimerListener.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/Reference.h"

namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{ namespace supervisor {

    struct Provider{
      std::string name;
      short id;
      short priority;//larger numbers have higher priority  
      Provider():id(-1),priority(-1){}
      friend std::ostream&  operator<<(std::ostream& o, Provider p){
	o<<" Provider:"<<p.name<<"-"<<p.id<<"-"<<p.priority;
	return o;
      }
      bool operator==(const Provider& y) const{
	return (name==y.name && id==y.id && priority==y.priority);
      }
      bool operator>(const Provider& y)const{
	return  priority>y.priority;
      }
    };    
    
    struct Priority_IsLesser{
      bool operator()(const Provider& x, const Provider& y) const{
	return x.priority<y.priority;
      }
    };
      
    struct Priority_IsGreater{
      bool operator()(const Provider& x, const Provider& y) const{
	return x.priority>y.priority;
      }
    };

    struct BunchPattern{
    BunchPattern():fillnum(0),runnum(0),lsnum(0),nbnum(0),tssec(0),tsmsec(0){
	std::fill_n( bxpattern, 7128, false );	
      }
      std::vector<Provider> providerpriorities;
      bool bxpattern[7128];
      int fillnum;
      int runnum;
      int lsnum;
      int nbnum;
      int tssec;
      int tsmsec;

      inline bool providerExists( short pid ) const{
	for(std::vector<Provider>::const_iterator it=providerpriorities.begin(); it!=providerpriorities.end(); ++it){
	  if( pid == it->id ){
	    return true;
	  }
	}
	return false;
      }

      inline unsigned short ncollidingbx(){
	unsigned short result=0;
	for(int i=0; i<7128; ++i){
	  int j = i+3564;
	  if( bxpattern[i] && bxpattern[j]){
	    ++result;
	  }
	}
	return result;
      }

      bool operator==(const BunchPattern& a) const{	
	return !memcmp(bxpattern, a.bxpattern, sizeof(bxpattern) );
      }

      friend std::ostream& operator<<(std::ostream& o, BunchPattern a){
	o<<a.fillnum<<","<<a.runnum<<","<<a.lsnum<<","<<a.nbnum<<" "; 
	for(std::vector<Provider>::iterator it=a.providerpriorities.begin(); it!=a.providerpriorities.end();++it){
	  o<<*it;
	}
	return o;      
      }
      
    };   
    
    struct BunchPattern_HasNoBestProvider{
      bool operator()(const BunchPattern& x, const BunchPattern& y) const{	
	return (x.providerpriorities == y.providerpriorities);	    
      }
    };
        
    struct BunchPattern_IsGreater{
      bool operator()(const BunchPattern& x, const BunchPattern& y) const{
	if( x.providerpriorities.size()>y.providerpriorities.size() ){
	  return true; //compare number of providers
	}
	std::vector<Provider>::const_iterator xIt = std::max_element( x.providerpriorities.begin(), x.providerpriorities.end(), Priority_IsLesser() );
	std::vector<Provider>::const_iterator yIt = std::max_element( y.providerpriorities.begin(), y.providerpriorities.end(), Priority_IsLesser() );	
	if( yIt==y.providerpriorities.end() ) return true;
	if( xIt!=x.providerpriorities.end() ){
	  return *xIt>*yIt;	  
	}
	return false;
      }
    };
    
    
    class BunchPatternProcessor : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      BunchPatternProcessor(xdaq::ApplicationStub* s);
      // destructor
      ~BunchPatternProcessor();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      //workloop 
      bool publishing(toolbox::task::WorkLoop* wl);
      bool cleaning(toolbox::task::WorkLoop* wl);
    protected:      
      // configuration parameters
      xdata::String m_bus;
      xdata::String m_bxconfigPriorityStr;
      xdata::UnsignedInteger32 m_cutoffmsec;
      bool m_canpublish;
      // cache    
      std::map<std::string,short> m_providernametoid;
      std::map<short,std::string> m_provideridtoname;
      std::map<short,short> m_providerpriorities;
      std::vector<BunchPattern> m_bxconfigCache;
      // flashlist
      xdata::InfoSpace* m_flashIS;
      int m_evtpublishcount;
      std::list< std::string > m_flashfields;
      std::vector< xdata::Serializable* > m_flashvalues;
      //workloops
      toolbox::mem::Pool* m_memPool;
      toolbox::BSem m_applock;
      toolbox::task::ActionSignature* m_as_publishing;
      toolbox::task::ActionSignature* m_as_cleaning;
      toolbox::task::WorkLoop* m_publishing;
      toolbox::task::WorkLoop* m_cleaning;
    private:       
      void do_publish_bxconfig();
      void init_bxconfigFlash();
      void cleanFlashlist();
      // nocopy protection
      BunchPatternProcessor (const BunchPatternProcessor&);
      BunchPatternProcessor & operator=(const BunchPatternProcessor&);
    };
  }}
#endif
