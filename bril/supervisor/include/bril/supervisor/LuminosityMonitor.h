#ifndef _bril_LuminosityMonitor_h_
#define _bril_LuminosityMonitor_h_
#include <string>
#include <set>
#include <vector>
#include <list>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Serializable.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "toolbox/ActionListener.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/ActionWithArg.h"
#include "toolbox/EventDispatcher.h"
#include "string.h"

namespace bril{ namespace supervisor { 

    class DetLuminosityData{
    public:
    DetLuminosityData():fillnum(0),runnum(0),lsnum(0),timestampsec(0),masklow(0),maskhigh(0),avg(0){ memset(bx,0.0,sizeof(float)*3564); }
      unsigned int fillnum;
      unsigned int runnum;
      unsigned int lsnum;
      unsigned int timestampsec;
      unsigned int timestampmsec;
      unsigned int masklow;
      unsigned int maskhigh;
      float avg;
      std::string calibtag;
      float bx[3564];
      };
   
    class SimpleLuminosityData{
    public:
    SimpleLuminosityData():fillnum(0),runnum(0),lsnum(0),nbnum(0),timestampsec(0),avg(0){ }
      unsigned int fillnum;
      unsigned int runnum;
      unsigned int lsnum;
      unsigned int nbnum;
      unsigned int timestampsec;
      unsigned int timestampmsec;
      float avg;
      std::string detectorname;
    };

    class LuminosityMonitor : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,toolbox::EventDispatcher{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      LuminosityMonitor(xdaq::ApplicationStub* s);
      // destructor
      ~LuminosityMonitor();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      //workloop 
      bool publishing_detlumi(toolbox::task::WorkLoop* wl, std::string topicname);
      bool publishing_fastlumi(toolbox::task::WorkLoop* wl, std::string topicname);
    protected:      
      // configuration parameters
      xdata::String m_bus;
      xdata::String m_lumitopicsStr;
      xdata::String m_fastlumitopicsStr;
      std::set<std::string> m_lumitopics;
      std::set<std::string> m_fastlumitopics;
      std::string m_uniqueid;

      // caches            
      std::map< std::string,DetLuminosityData > m_lumidata;
      std::map< std::string,SimpleLuminosityData > m_fastlumidata;

      std::map< std::string, std::pair< toolbox::task::WorkLoop*, toolbox::task::ActionSignature* > > m_wlstore;
      std::map< std::string, std::pair< toolbox::task::WorkLoop*, toolbox::task::ActionSignature* > > m_fastlumi_wlstore;

      // monitoring
      xdata::InfoSpace *m_luminosityInfoSpace; //per ls det lumi
      xdata::InfoSpace *m_fastlumiInfoSpace; //fast detlumi
      xdata::InfoSpace *m_fastbestlumiInfoSpace; //fast bestlumi

      std::list<std::string> m_flashfields;
      std::vector<xdata::Serializable*> m_flashcontents;
      
      std::list<std::string> m_fastflashfields;
      std::vector<xdata::Serializable*> m_fastflashcontents;

    private: 
      toolbox::BSem m_applock;
      void init_luminosityFlashlist();
      void declare_luminosityFlashlist();
      void clean_luminosityFlashlist();

      void init_fastlumiFlashlist();
      void declare_fastlumiFlashlist();
      void clean_fastlumiFlashlist();

      // nocopy protection
      LuminosityMonitor (const LuminosityMonitor&);
      LuminosityMonitor & operator=(const LuminosityMonitor&);
  };
  }}
#endif
