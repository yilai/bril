#ifndef _bril_supervisor_DTLumiProcessor_h_
#define _bril_supervisor_DTLumiProcessor_h_
#include <string>
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/String.h"
#include "xdata/Properties.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/BSem.h"
namespace toolbox
 {
  namespace task
   {
    class WorkLoop;
    class ActionSignature;
   }
 }

namespace bril{
  namespace supervisor{

    class DTLumiProcessor :
    public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::EventDispatcher{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      DTLumiProcessor(xdaq::ApplicationStub* s);
      // destructor
      ~DTLumiProcessor();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      // infospace callback
      virtual void actionPerformed( xdata::Event& e );
      // toolbox event callback
      virtual void actionPerformed( toolbox::Event& e ); 
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
    private:

      // memory pool
      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;

      // configuration
      xdata::Vector< xdata::Properties > m_eventinginput;
      xdata::String m_outbus;
      xdata::String m_outtopic;
      xdata::String m_calibtag;
      xdata::Float m_background;
      xdata::Float m_sigmaVis;
      xdata::Float m_scalefactor;
      //cache
      std::map< std::string , std::set<std::string> > m_in_busTotopics;
      std::map< uint64_t, uint32_t, std::less<uint64_t> > m_runlstotimestamp;
      std::map< uint64_t, float, std::less<uint64_t> > m_runlstorate;
      char m_beamstatus[32];    
      float m_bxbeamintensity1[3564];
      float m_bxbeamintensity2[3564];
      std::string m_uniqueid;
      unsigned int m_fillnum;
      unsigned int m_runnum;
      unsigned int m_lsnum;
      unsigned int m_nbnum;
      unsigned int m_timestampsec;
      unsigned int m_timestampmsec;
      bool m_canpublish;
      // cache values from trigger flashlist
      unsigned int m_RunNumber;
      unsigned int m_algo_lumiSection;
      float m_bmtfSortedRate;
      //workloop
      toolbox::task::WorkLoop* m_publishing_wl;
      toolbox::task::ActionSignature* m_as_publishing_wl;     
      toolbox::BSem m_applock;
    private:
      bool publishing(toolbox::task::WorkLoop* wl);

    private:
      DTLumiProcessor(const DTLumiProcessor&);
      DTLumiProcessor& operator=(const DTLumiProcessor&);
    };
  }// end ns supervisor
}// end ns bril
#endif
