#ifndef _bril_LumiTopicItem_h_
#define _bril_LumiTopicItem_h_
#include <string>
#include <vector>
#include <ostream>
#include <algorithm>

namespace bril{ namespace supervisor{
    class LumiTopicItem{
    public:
      LumiTopicItem(const std::string& topicname,
		    const std::vector<std::string>& prioritylist) :m_topicname(topicname){
	std::vector<std::string>::const_iterator it = std::find(prioritylist.begin(),prioritylist.end(),topicname);
	m_pos = it - prioritylist.begin();
      }
      bool operator>(const LumiTopicItem& rhs)const{
	return m_pos>rhs.m_pos;
      }
      friend std::ostream& operator<<(std::ostream& os, const LumiTopicItem& ti){
	return os << ti.m_topicname <<":"<< ti.m_pos<<":"<<ti.avg;
      }
      std::string topicname() const{ return m_topicname; }
      size_t priority() const{ return m_pos; }
    public:
      unsigned int timestampsec;
      unsigned int timestampmsec;
      unsigned int fillnum;
      unsigned int runnum;
      unsigned int lsnum;
      unsigned int nbnum;
      float avg;
      float avgraw;
      float bx[3564];
      float bxraw[3564];       
      std::string calibtag;
      unsigned int maskhigh;
      unsigned int masklow;
      //std::string payloaddict;
    private:
      std::string m_topicname;
      size_t m_pos;      	
    };

  }
}
#endif
