#include <iostream>
#include "bril/supervisor/LumiTopicPriorityQueue.h"

using namespace bril::supervisor;

int main(){
  std::vector<std::string> priority;
  priority.push_back("cd");
  priority.push_back("ab");
  priority.push_back("ef");
  priority.push_back("gh");
  priority.push_back("dup");

  LumiTopicItem item_gh("gh",priority);
  LumiTopicItem item_ab("ab",priority);
  LumiTopicItem item_cd("cd",priority);
  LumiTopicItem item_ef("ef",priority);

  item_gh.avg = 5;
  item_ab.avg = 4;
  item_cd.avg = 0.5;
  item_ef.avg = 0.006;

  std::cout<<item_ab<<" "<<item_gh<<" "<<item_ef.avg<<std::endl;
  LumiTopicPriorityQueue q;
  q.enqueue(item_gh);
  q.enqueue(item_ab);
  q.enqueue(item_cd);
  q.enqueue(item_ef);
  std::cout<<"q.size() "<<q.size()<<std::endl;
  LumiTopicItem t=q.top();
  std::cout<<"top is "<<t<<std::endl;
  std::cout<<"median "<<q.median_avglumi()<<std::endl;
  
  //simulation
  if( q.empty() ){
    std::cout<<"should return "<<std::endl;
  }
  const float lowerthreshould = 0;
  const float threshould = 0.5;
  if( q.allinrange(lowerthreshould,threshould) ){
    std::cout<<" queue all in range 0 "<<threshould<<" publish top priority"<<std::endl;
    return 0;
  }
  if( q.size()<=2 ){
    std::cout<<" queue size is <=2, publish top priority "<<std::endl;
  }else{
    float medianavg = q.median_avglumi();
    float upper = medianavg*3;
    float lower = medianavg*0.33;
    std::cout<<"band lower "<<lower<<" median "<<medianavg<<" upper "<<upper<<std::endl;
    while( !q.empty() ){
      if( q.top().avg>upper || q.top().avg<lower ){
	std::cout<<" discarded "<<q.top()<<std::endl;
	q.pop();
	continue;
      }      
      break;
    }
    std::cout<<"publish queue top "<<q.top()<<" avg "<<q.top().avg<<std::endl;

    LumiTopicItem item_dupa("dup",priority);
    item_dupa.avg = 0.5;
    LumiTopicItem item_dupb("dup",priority);
    item_dupb.avg = 0.8;
    
    q.enqueue(item_dupa);
    std::cout<<q<<std::endl;
    q.enqueue(item_dupb);
    std::cout<<q<<std::endl;
    
  }
}
