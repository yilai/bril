class IOVPayloadItem(object): # to be replaced by cython!!
    def __init__(self,nfields=1):
        self.fields = [None]*nfields
    def nfields(self):
        return len(self.fields)
    def setfield(self,fieldidx,v):
        self.fields[fieldidx] = v
    def getfield(self,fieldidx=0):
        return self.fields[fieldidx]
    
class BCM1FChannelmaskItem(IOVPayloadItem): 
    def __init__(self):
        super(BCM1FChannelmaskItem, self).__init__()
        self.setfield(0,[1]*48)
    def maskchannels(self,channelids):
        for cid in channelids:
            self.fields[0][cid] = 0
            
class BHMLutItem(IOVPayloadItem):
    def __init__(self):
        super(BHMLutItem, self).__init__(2)
    def setlookup(self,key,val):
        self.fields[0] = key
        self.fields[1] = val
    
if __name__=='__main__':
    m = BCM1FChannelmaskItem()
    m.maskchannels([1,3,5])
    print m.getfield()
    bhmluts = [] 
    for i in xrange(40):
        l = BHMLutItem()
        l.setlookup(str(i),hex(i*100))
        bhmluts.append(l)
    print bhmluts
