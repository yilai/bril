from sqlalchemy import *
from sqlalchemy import exc
import objdef,utils
from datetime import datetime
from copy import deepcopy

db = create_engine('sqlite:////home/zhen/work/bril/trunk/daq/bril/lumistore/scripts/testiov.db')
metadata = MetaData(db)

iovtags = Table('IOVTAGS', metadata,
                Column('TAGID', BIGINT, primary_key=True),
                Column('TAGNAME', TEXT() ),
                Column('CREATIONUTC', TEXT(), ),
                Column('DATADICT', TEXT() ),
                Column('MAXNITEMS', INT, default=1),
                Column('IOVDATAVERSION', TEXT(),default='1.0.0'),
                Column('DATASOURCE', TEXT() ),
                Column('APPLYTO', TEXT() ),
                Column('ISDEFAULT', SMALLINT,default=0 ),
                Column('COMMENT', TEXT(),default=u'')
)
tagdata = Table('IOVTAGDATA', metadata,
                Column('TAGID', BIGINT, primary_key=True),
                Column('SINCE', INT , primary_key=True),
                Column('PAYLOADID', BIGINT ),
                Column('COMMENT', TEXT(),default=u'' )
)

iovp_uint8 = Table('IOVP_UINT8', metadata,
                Column('PAYLOADID', BIGINT, primary_key=True),
                Column('IFIELD', INT, primary_key=True),
                Column('IITEM', INT, primary_key=True),
                Column('IPOS', INT, primary_key=True),
                Column('VAL', SMALLINT, ),
)

iovp_string = Table('IOVP_STRING', metadata,
                Column('PAYLOADID', BIGINT, primary_key=True),
                Column('IFIELD', INT, primary_key=True),
                Column('IITEM', INT, primary_key=True),
                Column('IPOS', INT, primary_key=True),
                Column('VAL', TEXT(), ),
)

payloadtablenames = utils.getPtablenames()
print payloadtablenames
payloadtables = {}
payloadtables['IOVP_UINT8'] = iovp_uint8
payloadtables['IOVP_STRING'] = iovp_string

datadesc = "UINT8:48"
datadict = utils.parsedatadict(datadesc)
    
tagid = next(utils.nonsequential_key(7))
tagname = u'bcm1fchannelmask_v1'
nowstr = datetime.utcnow().strftime("%Y-%m-%d %H:%M")
sinces = [1,123450,123460,123480]

connection = db.connect()

nitems = 1
m = objdef.BCM1FChannelmaskItem()
m.maskchannels([1,3,5])

trans = connection.begin()
try:
    print "creating tag %s"%tagname
    i = iovtags.insert(
        values=dict(TAGID=tagid,TAGNAME=tagname,CREATIONUTC=nowstr,DATADICT=datadesc,MAXNITEMS=nitems,DATASOURCE='BCM1F',APPLYTO='LUMI',ISDEFAULT=1,COMMENT=u'test')
    )
    i.execute()
    print i
    print "done"

    for since in sinces:
        payloadid = next(utils.nonsequential_key(2))
        print '    since %d, payload %d'%(since,payloadid)
        t = tagdata.insert(
            values=dict(TAGID=tagid,SINCE=since,PAYLOADID=payloadid)
        )
        t.execute()
        
        rowcache = {}
        print 'bulk inserting payload BCM1FChannelmask'
        for item_idx in xrange(nitems):
            for fieldidx,(tablesuffix,varlen,alias) in enumerate(datadict):
                tname = utils.getPtablename(tablesuffix)
                npos = 1
                if tablesuffix.find('STRING')<0: #if is not string
                    npos = int(varlen)
                v = m.getfield(fieldidx)
                if npos==1 :
                    rowcache.setdefault(tname,[]).append({'PAYLOADID':payloadid,'IITEM':item_idx,'IFIELD':fieldidx,'IPOS':0,'VAL':v})
                else:
                    for ipos_idx in xrange(npos):
                        rowcache.setdefault(tname,[]).append({'PAYLOADID':payloadid,'IITEM':item_idx,'IFIELD':fieldidx,'IPOS':ipos_idx,'VAL':v[ipos_idx]})
                    
        for k,bulkrows in rowcache.items():
            if len(bulkrows)==0: continue
            print '  inserting to %s '%k
            ptable = payloadtables[k]                
            ptable.insert().execute(bulkrows)
    print "done"
    trans.commit()
#except SQLAlchemyError:
except:
    trans.rollback()
    raise

#
# Try another class
#

datadesc = "STRING256:1:KEY,STRING1024:1:VAL"
datadict = utils.parsedatadict(datadesc)
print datadict

bhmluts = [] 
nitems = 40
for i in xrange(nitems):
    l = objdef.BHMLutItem()
    l.setlookup(str(i), hex(i*100))
    bhmluts.append(l)
tagname = u'bhmlut_v1'
tagid = next(utils.nonsequential_key(7))

trans = connection.begin()
try:
    print "creating tag %s"%tagname
    i = iovtags.insert(
        values=dict(TAGID=tagid,TAGNAME=tagname,CREATIONUTC=nowstr,DATADICT=datadesc,MAXNITEMS=nitems,DATASOURCE='BHM',APPLYTO='DAQ',ISDEFAULT=1,COMMENT=u'test')
    )
    i.execute()
    print i
    print "done"
    
    print "inserting iovs to %s"%tagname
    for since in sinces:
        payloadid = next(utils.nonsequential_key(7))
        print '    since %d, payload %d'%(since,payloadid)
        t = tagdata.insert(
            values=dict(TAGID=tagid,SINCE=since,PAYLOADID=payloadid)
        )
        print t
        t.execute()
        rowcache = {}
        print 'bulk inserting payload BHMLut'
        for item_idx in xrange(len(bhmluts)):#outermode container
            m = bhmluts[item_idx]
            for fieldidx,(tablesuffix,varlen,alias) in enumerate(datadict):#loop over data members
                tname = utils.getPtablename(tablesuffix)
                npos = 1
                if tablesuffix.find('STRING')<0: #if is not string
                    npos = int(varlen)
                v = m.getfield(fieldidx)
                if npos==1 :
                    rowcache.setdefault(tname,[]).append({'PAYLOADID':payloadid,'IITEM':item_idx,'IFIELD':fieldidx,'IPOS':0,'VAL':v})
                else:
                    for ipos_idx in xrange(npos):
                        rowcache.setdefault(tname,[]).append({'PAYLOADID':payloadid,'IITEM':item_idx,'IFIELD':fieldidx,'IPOS':ipos_idx,'VAL':v[ipos_idx]})
        for k,bulkrows in rowcache.items():
            if len(bulkrows)==0: continue
            ptable = payloadtables[k]                
            ptable.insert().execute(bulkrows) 
    print "done"
    trans.commit()
except exc.SQLAlchemyError, e:
    print e
    trans.rollback()
    raise

connection.close()
db.dispose()
