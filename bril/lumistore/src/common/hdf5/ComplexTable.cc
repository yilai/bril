#include "bril/lumistore/hdf5/File.h"
#include "bril/lumistore/hdf5/ComplexTable.h"
#include "bril/lumistore/hdf5/H5RowDef.h"
#include "bril/lumistore/hdf5/H5Utils.h"
//#include <cstdio>
#include <string.h>
#include <iostream>
namespace bril{ namespace lumistore { namespace hdf5{
      ComplexTable::ComplexTable(const std::string& parentpath, hid_t parentid, const std::string& name, File* file,const std::string& dictionary, size_t nrows, bool shrink):Node(parentpath,parentid,name,file),m_nrows(nrows),m_shrink(shrink){
	m_rowdef = new H5RowDef(dictionary.c_str());	
	m_rowtype = 0;
	m_wbuf = malloc(m_rowdef->rowsize()*m_nrows);
	//memset(m_wbuf, 0xff, sizeof(m_wbuf));
        memset(m_wbuf, 0xff,m_rowdef->rowsize()*m_nrows);
	m_current = 0;
	m_rowsinbuffer = 0;
	m_committedrows = 0;
	m_committedtimes = 0;
	m_allocatedrows = 0;	
	m_id = create_table();
	m_isopen = true;
      }
      ComplexTable::~ComplexTable(){
	close();
	delete m_rowdef ; m_rowdef = 0;
      }
      
      void ComplexTable::append(const void* payloadaddress){
	char* buffc = (char*)m_wbuf;
	m_current = (char*)(buffc+m_rowdef->rowsize()*m_rowsinbuffer);	
	memcpy(m_current,payloadaddress,m_rowdef->rowsize());
	m_rowsinbuffer++;
	if(m_rowsinbuffer>=m_nrows){ 
	  flush();
	}
      }
      
      void ComplexTable::close(){
	if(!m_isopen) return;
	if(m_rowsinbuffer>0) flush();
	if(m_shrink){
	  hsize_t dims[1];
	  dims[0] = m_committedrows ;
	  H5Dset_extent(m_id,dims);
	}
	H5Dclose(m_id);
	free(m_wbuf);
	m_isopen = false;
      }

      hid_t ComplexTable::create_table(){	
	m_rowtype = H5Tcreate(H5T_COMPOUND,m_rowdef->rowsize());
	m_rowdef->H5_definerow(m_rowtype);	
	hid_t tableid = H5TBOmake_table(m_parentid,m_name.c_str(),m_rowtype,0,m_nrows,0,0);
	if(tableid<0) {
	  std::cout<<"failed to create datatable"<<std::endl;
	  return 0;
	}
	m_rowdef->H5_setfieldnames(tableid,m_pathname.c_str(),m_name.c_str());
	return tableid; 
      }

      void ComplexTable::flush(){
	// signature: H5TBOappend_records( hid_t dataset_id, hid_t mem_type_id,hsize_t nrecords,hsize_t nrecords_orig,const void *data )
	size_t previouscommitted = m_committedtimes*m_nrows;
	herr_t status = H5TBOappend_records(m_id,m_rowtype,m_rowsinbuffer,previouscommitted,m_wbuf);
	if(status<0){
	  std::cout<<"failed to write to disk"<<std::endl;
	  return;
	}
	m_allocatedrows += m_nrows;
	m_committedrows += m_rowsinbuffer;
	m_committedtimes ++;
	m_rowsinbuffer = 0;
      }
}}}
