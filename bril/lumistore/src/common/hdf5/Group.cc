#include "bril/lumistore/hdf5/Group.h"
#include "bril/lumistore/hdf5/File.h"
#include <iostream>
namespace bril{namespace lumistore{ namespace hdf5{

    Group::Group(const std::string& parentpath, hid_t parentid, const std::string& name,File* file):Node(parentpath,parentid,name,file){
      if(parentid!=0){
	m_id = create_group();
	if(m_id<=0){
	  std::cout<<"failed to create group "<<std::endl;
	}
	m_isopen = true;
      }else{
	m_id = file->id();//this is RootGroup
      }
    }

    Group::~Group(){
      if(m_isopen){
	this->close();
      }
    }

    //void Group::open(){
    //  m_id = H5Gopen(m_parentid,m_name,H5P_DEFAULT);
    //  m_isopen = True;
    //}

    void Group::close(){
      if(!m_isopen) return;
      if(m_id>0){
	H5Gclose(m_id);
	m_id = 0;
      }
      m_isopen = false;
    }

    hid_t Group::create_group(){
      m_id = H5Gcreate(m_parentid,m_pathname.c_str(),H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);      
      return m_id;
    }
    
      RootGroup::RootGroup(File* file):Group("",0,"",file){
      }
      RootGroup::~RootGroup(){}   
  }}}
