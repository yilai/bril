#include "bril/lumistore/hdf5/File.h"
#include "bril/lumistore/hdf5/Group.h"
#include "bril/lumistore/hdf5/Table.h"
#include "bril/lumistore/hdf5/ComplexTable.h"
#include "hdf5_hl.h"
#include <iostream>
namespace bril{ namespace lumistore { namespace hdf5{
      File::File(const std::string& name,bool compressed):m_filename(name),m_compressed(compressed),m_isopen(false){
	m_fileid = H5Fcreate(m_filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	if(m_fileid<0) {
	  std::cout<<"failed to create file"<<std::endl;	
	}
	m_root = new RootGroup(const_cast<File*>(this));
	H5LTset_attribute_string(m_fileid,"/","CLASS","GROUP");
	H5LTset_attribute_string(m_fileid,"/","PYTABLES_FORMAT_VERSION","2.1");
	H5LTset_attribute_string(m_fileid,"/","TITLE","");
	H5LTset_attribute_string(m_fileid,"/","VERSION","1.0");
	m_isopen = true;
	
      }
      File::~File(){
	close();
	delete m_root;
	m_root = 0;
      }

      Group* File::create_group(const std::string& parentpath,const std::string& name){
	Node* parentnode = get_node(parentpath);
	if(!parentnode){
	  std::cout<<"Parent node nonexisting"<<std::endl;
	  return 0; 
	}
	Group* g = new Group(parentpath,parentnode->id(),name,const_cast<File*>(this));	
	m_noderegistry.insert(std::make_pair(g->pathname(),g));	
	return g;
      }
      
      Node* File::create_table(const std::string& parentpath,const std::string& name,size_t datacolumnlen,hid_t datacolumntype,size_t rowsperblock,bool shrink){
	Node* parentnode = get_node(parentpath);
	if(!parentnode){
	  std::cout<<"Parent node nonexisting"<<std::endl;
	  return 0; 
	}
	Table* t = new Table(parentpath,parentnode->id(),name,const_cast<File*>(this),datacolumnlen,datacolumntype,rowsperblock,shrink);
	m_noderegistry.insert(std::make_pair(t->pathname(),t));
	return t;
      }

      Node* File::create_complextable(const std::string& parentpath,const std::string& name,const std::string& dictionary,size_t rowsperblock,bool shrink){
	Node* parentnode = get_node(parentpath);
	if(!parentnode){
	  std::cout<<"Parent node nonexisting"<<std::endl;
	  return 0; 
	}
	ComplexTable* t = new ComplexTable(parentpath,parentnode->id(),name,const_cast<File*>(this),dictionary,rowsperblock,shrink);
	m_noderegistry.insert(std::make_pair(t->pathname(),t));
	return t;
      }


      Node* File::get_node(const std::string& path){
	if(path=="/" || path.empty()) return get_rootnode();
	std::map<std::string,Node*>::iterator it = m_noderegistry.find(path);
	if(it!=m_noderegistry.end()){
	  return it->second;
	}else{
	  return 0;
	}
      }

      Node* File::get_rootnode(){
	return m_root;
      }
      
      void File::set_attrstr(const std::string& path,const std::string& attrname,const std::string& attrvalue){
	Node* t = get_node(path);
	if(t){
	  H5LTset_attribute_string(m_fileid,path.c_str(),attrname.c_str(),attrvalue.c_str());
	}else{
	  std::cout<<path<<" not found, cannot add attribute"<<std::endl;
	}
      }

      hid_t File::id()const{ 
	return m_fileid;
      }

      bool File::isopen() const{
	return m_isopen;
      }

      size_t File::get_filesize() const {
	/**
	std::cout<<"in get_filesize "<<std::endl;
	if(!m_isopen) return 0;
	herr_t err = 0;
	hsize_t size = 0;
	hid_t newid = H5Freopen(m_fileid);
	std::cout<<"about to H5Fget_filesize fileid "<<m_fileid<<std::endl;
	//	err = H5Fget_filesize(m_fileid, &size);
	err = H5Fget_filesize(newid, &size);
	H5Fclose(newid);
	std::cout<<"done"<<std::endl;
	if (err < 0 ){
	  std::cout<<"Unable to retrieve the HDF5 file size"<<std::endl;
	}
	**/
	size_t size = 0;
	return size;	
      }

      void File::close(){	
	if(!m_isopen) return;
	for(std::map<std::string, Node* >::iterator it=m_noderegistry.begin(); it!=m_noderegistry.end(); ++it){
	  it->second->close();
	  delete it->second;
	  it->second = 0;
	}
	m_noderegistry.clear();
	H5Fclose(m_fileid);
	m_fileid = 0;
	m_isopen = false;
      }
    }}}
