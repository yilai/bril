#include "bril/lumistore/H5OutputFile.h"
#include "bril/lumistore/StorageUnit.h"
#include "bril/lumistore/OutputFileConfig.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include "bril/lumistore/hdf5/Table.h"
#include "bril/lumistore/hdf5/ComplexTable.h"
#include "bril/lumistore/hdf5/File.h"
#include "bril/lumistore/hdf5/Group.h"
#include "toolbox/TimeVal.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
namespace bril{ namespace lumistore{
    H5OutputFile::H5OutputFile(const std::string& basename, const OutputFileConfig* fconfig,OutputFileStatus* fstatus):OutputFile(basename,fconfig,fstatus),m_file(0){
      m_file = new hdf5::File(fstatus->filename,m_fconfig->compressed);
    }
    H5OutputFile::~H5OutputFile(){
      if(m_file){
	delete m_file; 
	m_file=0;
      }
    }
    void H5OutputFile::do_writeStorageUnit(const StorageUnit* u){
      size_t nitems = u->dataheader()->nitems();
      if(nitems==0){
	std::cout<<"empty payload, store nothing"<<std::endl;
	return;
      }
      std::string topicname = u->topicname();
      std::string tablepath = "/"+topicname;
      hdf5::Node* t = m_file->get_node(tablepath);
      std::string payloaddict = u->payloadDict();
      unsigned int storagetype = u->dataheader()->getStorageTypeID();
      std::string dict ;
      if( !payloaddict.empty() && nitems!=0 ){
	dict = interface::bril::shared::DatumHead::headdict() + " " + payloaddict; 
      }
      if(!t){// create new table
	if(payloaddict.empty()){ //simple table
	  hid_t h5type = 0;
	  if(storagetype==interface::bril::shared::StorageType::FLOAT){
	    h5type = H5T_NATIVE_FLOAT;
	  }else if(storagetype==interface::bril::shared::StorageType::DOUBLE){
	    h5type = H5T_NATIVE_DOUBLE;
	  }else if(storagetype==interface::bril::shared::StorageType::UINT32){
	    h5type = H5T_NATIVE_UINT;
	  }else if(storagetype==interface::bril::shared::StorageType::INT32){
	    h5type = H5T_NATIVE_INT;
	  }else if(storagetype==interface::bril::shared::StorageType::UINT16){
	    h5type = H5T_NATIVE_USHORT;
	  }else if(storagetype==interface::bril::shared::StorageType::INT16){
	    h5type = H5T_NATIVE_SHORT;
	  }else if(storagetype==interface::bril::shared::StorageType::UINT64){
	    h5type = H5T_NATIVE_ULLONG;
	  }else if(storagetype==interface::bril::shared::StorageType::INT64){
	    h5type = H5T_NATIVE_LLONG;
	  }else if(storagetype==interface::bril::shared::StorageType::UINT8){
	    h5type = H5T_NATIVE_UINT8;
	  }else if(storagetype==interface::bril::shared::StorageType::INT8){
	    h5type = H5T_NATIVE_INT8;
	  }else if(storagetype==interface::bril::shared::StorageType::COMPOUND){
	    std::cout<<"missing dictionary for compound payload"<<std::endl;
	  }else{
	    std::cout<<"undefined type"<<std::endl;
	  }
	  t = m_file->create_table(m_file->get_rootnode()->pathname(),topicname,nitems,h5type,m_fconfig->nrowsperwbuf,m_fconfig->h5shrink);
	}else{
	  t = m_file->create_complextable(m_file->get_rootnode()->pathname(),topicname,dict,m_fconfig->nrowsperwbuf,m_fconfig->h5shrink);  
	}
      }
      if(dict.empty()){
	(dynamic_cast<hdf5::Table*>(t))->append(u->dataheader(),u->payloadbuffer());      
      }else{
	unsigned char* ref = (unsigned char*)(u->dataref()->getDataLocation());	
	(dynamic_cast<hdf5::ComplexTable*>(t))->append(ref);      
      }
    }

    void H5OutputFile::do_close(){
      if(!m_file->isopen()) return;
      m_file->close();
    }
    size_t H5OutputFile::do_getfilesize() const{      
      return m_file->get_filesize();
    }
  }}
