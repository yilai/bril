#include "bril/lumistore/RAWOutputFile.h"
#include "bril/lumistore/StorageUnit.h"
#include "bril/lumistore/OutputFileConfig.h"
#include "toolbox/TimeVal.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
namespace bril{ namespace lumistore{
    RAWOutputFile::RAWOutputFile(const std::string& basename, const OutputFileConfig* fconfig,OutputFileStatus* status):OutputFile(basename,fconfig,status),p_file(0){
    }
    RAWOutputFile::~RAWOutputFile(){
    }
    void RAWOutputFile::do_writeStorageUnit(const StorageUnit* u){
      void* dataAddress = u->dataref()->getDataLocation();     
      interface::bril::shared::DatumHead* data = (interface::bril::shared::DatumHead*)dataAddress; 
      if(p_file==0){
	p_file = fopen((m_status->filename).c_str(),"wb");
	if(!p_file){
	  std::cout<<"throw unable to open file"<<std::endl;
	}	
      }      
      size_t unitsize = data->totalsize();
      fwrite(dataAddress,unitsize,1,p_file);
    }
    void RAWOutputFile::do_close(){
      if(p_file!=0){
	fclose(p_file);
	p_file=0;
      }
    }
    size_t RAWOutputFile::do_getfilesize() const{      
      struct stat buf;
      int fd = fileno(p_file);
      fstat(fd, &buf);
      size_t fsz = buf.st_size;
      return fsz;
    }
  }}
