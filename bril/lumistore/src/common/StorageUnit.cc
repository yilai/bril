#include "toolbox/TimeVal.h"
#include "bril/lumistore/StorageUnit.h"

//#include <sstream>
namespace bril{
  namespace lumistore{
    StorageUnit::StorageUnit(const std::string& topicname,toolbox::mem::Reference* ptr):m_topicname(topicname),m_ref(ptr){
      m_creationtime = toolbox::TimeVal::gettimeofday().sec();
    }
    StorageUnit::~StorageUnit(){}
    void StorageUnit::payloadDict(const std::string& pdict){
      m_payloaddict = pdict;
    }
    std::string StorageUnit::payloadDict()const{
      return m_payloaddict;
    }
    unsigned int StorageUnit::creationTime() const{ 
      return m_creationtime; 
    }
    toolbox::mem::Reference*  StorageUnit::dataref() const{
      return m_ref;
    }
    interface::bril::shared::DatumHead* StorageUnit::dataheader() const{
      if(m_ref==0) return 0;
      return (interface::bril::shared::DatumHead*)(m_ref->getDataLocation());  
    }
    unsigned char* StorageUnit::payloadbuffer() const{
      if(m_ref==0) return 0;
      unsigned char* result = dataheader()->payloadanchor;
      return result;
    }
    unsigned int StorageUnit::totalDataSize() const{
      if(m_ref==0) return 0;
      return ((interface::bril::shared::DatumHead*)(m_ref->getDataLocation()))->totalsize();
    }
    size_t StorageUnit::age() const { 
      return toolbox::TimeVal::gettimeofday().sec()-m_creationtime; 
    }
    void StorageUnit::release(){ 
      if(m_ref){m_ref->release(); m_ref=0;} 
    }    
  }}//ns 
