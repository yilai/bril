#ifndef _bril_lumistore_TimeUtils_h_
#define _bril_lumistore_TimeUtils_h_

#include "toolbox/TimeVal.h"
#include <sstream>

namespace bril{ namespace lumistore {
    struct TimeUtils{
      TimeUtils(unsigned int sec,unsigned int msec):tv(sec,msec*1000){}
      inline std::string toString() const{
	return tv.toString(toolbox::TimeVal::gmt);
      }
      inline unsigned short weekofyear() const{
	std::string weekofyear = tv.toString("%V",toolbox::TimeVal::gmt);
	weekofyear.erase(0,weekofyear.find_first_not_of('0'));
	unsigned short s;
	std::istringstream buf(weekofyear);
	buf >> s;
	return s;
      }
      inline unsigned short dayofyear() const{
	std::string dayofyear = tv.toString("%j",toolbox::TimeVal::gmt);
	dayofyear.erase(0,dayofyear.find_first_not_of('0'));
	unsigned short s;
	std::istringstream buf(dayofyear);
	buf >> s;
	return s;
      }
      inline unsigned short dayofweek() const{
	std::string dayofweek = tv.toString("%w",toolbox::TimeVal::gmt);
	unsigned short s;
	std::istringstream buf(dayofweek);
	buf >> s;
	return s;
      }
      inline unsigned short hourofday() const{
	std::string hour = tv.toString("%H",toolbox::TimeVal::gmt);
	if(hour=="00") return 0;
	hour.erase(0,hour.find_first_not_of('0'));
	unsigned short s;
	std::istringstream buf(hour);
	buf >> s;
	return s;
      }
      inline unsigned short year() const{
	std::string year = tv.toString("%Y",toolbox::TimeVal::gmt);
	unsigned short s;
	std::istringstream buf(year);
	buf >> s;
	return s;
      }
      inline unsigned short month() const{
	std::string month = tv.toString("%m",toolbox::TimeVal::gmt);
	month.erase(0,month.find_first_not_of('0'));
	unsigned short s;
	std::istringstream buf(month);
	buf >> s;
	return s;
      }
      toolbox::TimeVal tv;
    };
    
    }}
#endif
