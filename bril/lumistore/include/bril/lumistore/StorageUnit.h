#ifndef _bril_lumistore_StorageUnit_h
#define _bril_lumistore_StorageUnit_h
#include "toolbox/mem/Reference.h"
#include "interface/bril/shared/CommonDataFormat.h"
#include <string>
namespace bril{
  namespace lumistore{
    /**
       Class hold datum with extra information such as age
    */
    class StorageUnit{
    public:
      StorageUnit(const std::string& topicname,toolbox::mem::Reference* ptr);
      ~StorageUnit();
      void payloadDict(const std::string& payloadDict);
      std::string payloadDict()const;
      unsigned int creationTime() const;
      toolbox::mem::Reference* dataref() const;
      interface::bril::shared::DatumHead* dataheader() const;
      unsigned char* payloadbuffer() const;
      bool empty() const{ return m_ref==0; }
      unsigned int totalDataSize()const;
      size_t age() const;
      void release();  
      std::string topicname()const{return m_topicname;}
    private:
      std::string m_topicname;
      std::string m_payloaddict;
      unsigned int m_creationtime;
      toolbox::mem::Reference* m_ref;
    };
  }}
#endif
