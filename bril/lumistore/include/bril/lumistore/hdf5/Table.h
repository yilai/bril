#ifndef _bril_lumistore_hdf5_Table_h
#define _bril_lumistore_hdf5_Table_h
#include "interface/bril/shared/CommonDataFormat.h"
#include "bril/lumistore/hdf5/Node.h"
#include "hdf5.h"
#include <string>
namespace bril{ namespace lumistore { namespace hdf5{
 class Row;
 class Table : public Node{
   public:
     Table(const std::string& parentpath, hid_t parentid, const std::string& name,File* file,size_t datacolumnlen,hid_t datacolumnhtype,size_t nrows, bool shrink=false);  
     ~Table();
     void append(const interface::bril::shared::DatumHead* datahead, unsigned char* payloadaddress);
     virtual void close();
   private:
     hid_t create_table();
     void flush();
   private:
     size_t m_datacolumnlen;
     hid_t m_datacolumnhtype;
     size_t m_nrows;
     bool m_shrink;
     size_t m_datacolumnsize;
     size_t m_rowsize;
     hid_t m_rowtype;
     void* m_wbuf; 
     Row* m_current;
     size_t m_rowsinbuffer;
     size_t m_committedrows;
     size_t m_committedtimes;
     size_t m_allocatedrows;
     hid_t m_tableid;
 };

}}}

#endif
