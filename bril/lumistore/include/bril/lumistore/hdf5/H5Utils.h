#ifndef _bril_lumistore_hdf5_H5Utils_h
#define _bril_lumistore_hdf5_H5Utils_h
#include "hdf5.h"

namespace bril{ 
  namespace lumistore{ 
    namespace hdf5{       
      herr_t H5TBOmake_table( hid_t loc_id,
			      const char* dset_name,
			      hid_t type_id,
			      hsize_t nrecords,
			      hsize_t chunk_size,
			      void  *fill_data,
			      const void *data );
      herr_t H5TBOappend_records( hid_t dataset_id,
				  hid_t mem_type_id,
				  hsize_t nrecords,
				  hsize_t nrecords_orig,
				  const void *data );      
    }
  }
}

#endif
