#ifndef _bril_lumistore_Application_h_
#define _bril_lumistore_Application_h_
#include <string>
#include <map>
#include <set>
#include "toolbox/squeue.h"
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/BSem.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Properties.h"
#include "bril/lumistore/exception/Exception.h"
#include "log4cplus/logger.h"
#include "toolbox/task/TimerListener.h"
namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{ namespace lumistore {
    class OutputFile;
    class OutputFileConfig;
    class OutputFileStatus;
    class StorageUnit;

    class Application : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      Application (xdaq::ApplicationStub* s);
      ~Application ();
      // hyperdaq callback
      void Default (xgi::Input * in, xgi::Output * out);
      // b2in callback
      void onB2INMessage (toolbox::mem::Reference * ref, xdata::Properties & plist);
      // xdata event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // timer callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
    private:
      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
            
      bool writing(toolbox::task::WorkLoop* wl);
      void writedata();
      void checkForFileTimeOuts();
      //void unsubscribeall();
      void subscribeall();
      void createfile();
    private:
      std::string m_classname;
      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool *m_memPool;
      toolbox::BSem m_applock;

      xdata::InfoSpace *m_appInfoSpace;
      xdata::UnsignedInteger32 m_instance;
      
      xdata::Vector<xdata::Properties> m_datasources;
      xdata::UnsignedInteger32 m_maxstalesec;
      xdata::UnsignedInteger32 m_checkagesec;
      xdata::UnsignedInteger32 m_maxsizeMB;
      xdata::UnsignedInteger32 m_nrowsperwbuf;
      xdata::Boolean m_h5shrink;
      xdata::Boolean m_compressed;
      xdata::String m_filepath;
      xdata::String m_fileformat;
      xdata::UnsignedInteger32 m_workinterval;
      std::string m_monurn;
      xdata::InfoSpace* m_monInfoSpace;
      xdata::UnsignedInteger64 m_totalsizeByte;
      xdata::UnsignedInteger32 m_totaltimeSec;
      xdata::Float m_throughput;
      xdata::UnsignedInteger32 m_dataqueuesize;
      xdata::UnsignedInteger32 m_lastupdatesec;
      xdata::UnsignedInteger32 m_firstupdatesec;
      xdata::UnsignedInteger32 m_lastFileTimeoutCheckTime;
      std::list<std::string> m_monItemList;
      
      //std::map<std::string, std::vector< std::pair<std::string, std::string> > > m_topicproperties;
      std::map<std::string, std::set<std::string> > m_bustopics;

      std::string m_processuuid;
      OutputFile* m_file;
      OutputFileConfig* m_fconfig;
      OutputFileStatus* m_fstatus;
      toolbox::squeue<StorageUnit*> m_dataqueue;
      toolbox::task::WorkLoop* m_writingwl;
      toolbox::task::ActionSignature* m_as_writingwl;

      bool m_shuttingdown;
      bool m_filetimedout;  
      
      std::string m_filechecktimername;
      toolbox::task::Timer* m_filechecktimer;       
    };//cls Application
  }//ns lumistore
}//ns bril	
#endif
