#include "bril/lumistore/hdf5/H5Utils.h"
#include "hdf5.h"
#include "hdf5_hl.h"
#include <stdlib.h>
#include <string.h>
#include <iostream>

using namespace bril::lumistore::hdf5;

const size_t NUMBER_OF_BLOCKS = 100;
const size_t ROWS_PER_BLOCK = 1024;
const unsigned int CHUNK_DIMS = 1024;

struct row{
  int var1;
  char str[25];
  double d1;
  int var2;
  int var4;
  long long var3;
  double d2;
  float lumis[3564];
} ;


/**
void write_block(hid_t dataset, hid_t rowType,hsize_t blocknum)
{
  ROWDATA item[ROWS_PER_BLOCK];
  ROWDATA* current;
  char strs[ROWS_PER_BLOCK][25];
  int i;
  herr_t status;
  hsize_t current_dims[1], dataspace;
  hsize_t offsets[1], select_dims[1];
  hid_t filespace;
  static int n;

  memset(item, 0xff, sizeof(item));
  for (i = 0; i < ROWS_PER_BLOCK; ++i){
    n = (607479349 * n + 307491661) & 0x7fffffff;
    current = &item[i];
    current->var1 = -(((int)blocknum) * 100 + i);
    sprintf(strs[i], "%10d", n);
    current->str = strs[i];
    current->d1 = ((int)blocknum) * 1000.0 + i;
    current->var2 = -(((int)blocknum) * 10000 + i);
    current->var3 = -(((int)blocknum) * 11111 + i);
    current->var4 = -(((int)blocknum) * 22222 + i);
    current->d2 = ((int)blocknum) * 2000.0 + i;
  }

  current_dims[0] = (blocknum + 1) * ROWS_PER_BLOCK;
  status = H5Dset_extent(dataset, current_dims);
  
  select_dims[0] = ROWS_PER_BLOCK;
  dataspace = H5Screate_simple(1, select_dims, NULL);
  filespace = H5Dget_space(dataset);
  
  offsets[0] = blocknum * ROWS_PER_BLOCK;
  select_dims[0] = ROWS_PER_BLOCK;
  status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET,
			       offsets, NULL, select_dims, NULL);
  status = H5Dwrite(dataset, rowType, dataspace,
		    filespace, H5P_DEFAULT, item);
  status = H5Sclose(dataspace);
  status = H5Sclose(filespace);
}


void write_data (hid_t container_object){
  hid_t strType, rowType, dataspace, dataset;
  hid_t dataset_properties;
  hsize_t dims[1], maxdims[1], i;
  herr_t status;

    strType = H5Tcopy(H5T_C_S1);
    status = H5Tset_size(strType, H5T_VARIABLE);
    strType = H5Tcopy(strType);

    rowType = H5Tcreate(H5T_COMPOUND, sizeof(ROWDATA));
    H5Tinsert(rowType, "var1", HOFFSET(ROWDATA, var1), H5T_NATIVE_INT);
    H5Tinsert(rowType, "str", HOFFSET(ROWDATA, str), strType);
    H5Tinsert(rowType, "d1", HOFFSET(ROWDATA, d1), H5T_NATIVE_DOUBLE);
    H5Tinsert(rowType, "var2", HOFFSET(ROWDATA, var2), H5T_NATIVE_INT);
    H5Tinsert(rowType, "var4", HOFFSET(ROWDATA, var4), H5T_NATIVE_INT);
    H5Tinsert(rowType, "var3", HOFFSET(ROWDATA, var3), H5T_NATIVE_LLONG);
    H5Tinsert(rowType, "d2", HOFFSET(ROWDATA, d2), H5T_NATIVE_DOUBLE);

    dims[0] = 1;
    maxdims[0] = H5S_UNLIMITED;
    dataspace = H5Screate_simple(1, dims, maxdims);

    dataset_properties = H5Pcreate(H5P_DATASET_CREATE);
    dims[0] = CHUNK_DIMS;
    status = H5Pset_chunk(dataset_properties, 1, dims);
#if USE_COMPRESSION
    status = H5Pset_deflate(dataset_properties, 1);
#endif

    dataset = H5Dcreate(container_object, DATASET_NAME, rowType, dataspace,
                        H5P_DEFAULT, dataset_properties, H5P_DEFAULT);

    status = H5Pclose(dataset_properties);

    for (i = 0; i < NUMBER_OF_BLOCKS; ++i)
    {
        write_block(dataset, rowType, i);
    }
    status = H5Sclose (dataspace);
    status = H5Dclose (dataset);
}
**/

//row item[ROWS_PER_BLOCK];

int main(){
  row *item = (row *) malloc(sizeof(row)* ROWS_PER_BLOCK);

  herr_t status;
  const char* filename="testH5Utils.hd5";
  
  hid_t file = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

  if(file<0) std::cout<<"failed to create file"<<std::endl;
  
  status = H5LTset_attribute_string(file,"/","CLASS","GROUP");
  status = H5LTset_attribute_string(file,"/","PYTABLES_FORMAT_VERSION","2.1");
  status = H5LTset_attribute_string(file,"/","TITLE","");
  status = H5LTset_attribute_string(file,"/","VERSION","1.0");
  //std::cout<<sizeof(row)<<std::endl;
  
  hid_t rowType = H5Tcreate(H5T_COMPOUND,sizeof(row));
  
  hid_t strType = H5Tcopy(H5T_C_S1);
  //status = H5Tset_size(strType, H5T_VARIABLE);
  status = H5Tset_size(strType, 25);
  strType = H5Tcopy(strType);
  H5Tinsert(rowType,"var1",HOFFSET(row,var1), H5T_NATIVE_INT);
  H5Tinsert(rowType,"str",HOFFSET(row,str), strType);
  H5Tinsert(rowType, "d1", HOFFSET(row, d1), H5T_NATIVE_DOUBLE);
  H5Tinsert(rowType, "var2", HOFFSET(row, var2), H5T_NATIVE_INT);
  H5Tinsert(rowType, "var4", HOFFSET(row, var4), H5T_NATIVE_INT);
  H5Tinsert(rowType, "var3", HOFFSET(row, var3), H5T_NATIVE_LLONG);
  H5Tinsert(rowType, "d2", HOFFSET(row, d2), H5T_NATIVE_DOUBLE);
  hsize_t arraydims[1]; arraydims[0]=3564;
  hid_t lumisid = H5Tarray_create(H5T_NATIVE_FLOAT,1,arraydims);
  H5Tinsert(rowType, "lumis", HOFFSET(row, lumis), lumisid);
  H5Tclose(strType);
  H5Tclose(lumisid);

  hid_t tabid = H5TBOmake_table(file,"mytable",rowType,0,ROWS_PER_BLOCK,0,0);
  if(tabid <0 ) std::cout<<"failed to create mytable"<<std::endl;

  status = H5LTset_attribute_string(tabid,"/mytable","CLASS","TABLE");
  status = H5LTset_attribute_string(tabid,"/mytable","TITLE","mytable");
  status = H5LTset_attribute_string(tabid,"/mytable","VERSION","2.7");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_0_NAME","var1");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_1_NAME","str");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_2_NAME","d1");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_3_NAME","var2");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_4_NAME","var4");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_5_NAME","var3");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_6_NAME","d2");
  status = H5LTset_attribute_string(tabid,"/mytable","FIELD_7_NAME","lumis");

  //char strs[ROWS_PER_BLOCK][25];

  row* current=0;

  memset(item, 0xff, sizeof(item));
  static int n = 0;

  for(size_t b = 0; b<NUMBER_OF_BLOCKS; ++b){
    for (size_t i = 0; i < ROWS_PER_BLOCK; ++i){
      n = (607479349 * n + 307491661) & 0x7fffffff;
      current = &item[i];
      current->var1 = -(((int)b) * 100 + i);
      sprintf(current->str,"%10d",n);
      current->d1 = ((int)b) * 1000.0 + i;
      current->var2 = -(((int)b) * 10000 + i);
      current->var3 = -(((int)b) * 11111 + i);
      current->var4 = -(((int)b) * 22222 + i);
      current->d2 = ((int)b) * 2000.0 + i;
      for(size_t l = 0; l<3564; ++l){ 
      	current->lumis[l]=0.5*float(l);
      }
    }
    status = H5TBOappend_records(tabid,rowType,ROWS_PER_BLOCK,b*ROWS_PER_BLOCK,item);
  }
  
  H5Fclose(file);
  free (item);  
}
