#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/TCDSTopics.hh"
#include "bril/lumistore/StorageUnit.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/TimeVal.h"
#include <string>
#include <queue>
#include <iostream>
using namespace interface::bril;
using namespace bril::lumistore;
int main(){
 std::string memPoolName = std::string("test_memPool");
  toolbox::mem::MemoryPoolFactory* factory = toolbox::mem::MemoryPoolFactory::getInstance();
  toolbox::mem::HeapAllocator *allocator = new toolbox::mem::HeapAllocator();
  toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
  toolbox::mem::Pool* mempool = factory->createPool(urn,allocator);
  //create storageunit queue
  std::queue<StorageUnit> squeue;
  shared::CompoundDataStreamer streamer(tcdsT::payloaddict());
  // simulate sending
  for(unsigned int i=0; i<10; ++i){
    size_t totalsize = tcdsT::maxsize();
    toolbox::mem::Reference* ref = factory->getFrame(mempool,totalsize);
    void* data = ref->getDataLocation();
    ref->setDataSize(totalsize);
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday(); 
    ((shared::DatumHead*)data)->setTime(2312,123450+i,i,1,t.sec(),t.millisec());
    ((shared::DatumHead*)data)->setResource(shared::DataSource::TCDS,0,0,shared::StorageType::COMPOUND);
    ((shared::DatumHead*)data)->setFrequency(64);
    ((shared::DatumHead*)data)->setTotalsize(totalsize);
    unsigned int norb = 4096;
    unsigned int norbls = 4096*64;
    bool cmson = false;
    float deadfrac = 0.05;
    unsigned int ncollidingbx= 12;
    streamer.insert_field( ((shared::DatumHead*)data)->payloadanchor, "norb", &norb );
    streamer.insert_field( ((shared::DatumHead*)data)->payloadanchor, "norbls", &norbls );
    streamer.insert_field( ((shared::DatumHead*)data)->payloadanchor, "cmson", &cmson );
    streamer.insert_field( ((shared::DatumHead*)data)->payloadanchor, "deadfrac", &deadfrac );
    streamer.insert_field( ((shared::DatumHead*)data)->payloadanchor, "ncollidingbx", &ncollidingbx );
    StorageUnit s(tcdsT::topicname(),ref);
    squeue.push(s);
  }
  // stream out on receiving
  for(unsigned int i=0; i<10; ++i){
    StorageUnit s = squeue.front();
    std::cout<<"nitems "<<s.dataheader()->nitems()<<std::endl;
    std::cout<<"total size "<<s.dataheader()->totalsize()<<std::endl;
    std::cout<<"payload size "<<s.dataheader()->payloadsize()<<std::endl;
    std::cout<<"fillnum "<<s.dataheader()->fillnum<<std::endl;
    std::cout<<"runnum "<<s.dataheader()->runnum<<std::endl;
    unsigned char* payloadbuffer = s.payloadbuffer();
    unsigned int norb = 0;
    unsigned int norbls = 0;
    bool cmson = true;
    float deadfrac = 0.0;
    unsigned int ncollidingbx= 0;
    streamer.extract_field(&norb, "norb", payloadbuffer);
    streamer.extract_field(&norbls, "norbls", payloadbuffer);
    streamer.extract_field(&cmson, "cmson", payloadbuffer);
    streamer.extract_field(&deadfrac, "deadfrac", payloadbuffer);
    streamer.extract_field(&ncollidingbx, "ncollidingbx", payloadbuffer);
    s.release();
    squeue.pop();
    std::cout<<"norb "<<norb<<" , norbls "<<norbls<<" , cmson "<<cmson<<" , deadfrac "<<deadfrac<<" , ncollidingbx "<<ncollidingbx<<std::endl;
  }
  //toolbox::mem::MemoryPoolFactory::destroyInstance();
}
