#include "toolbox/TimeVal.h"
#include "toolbox/Runtime.h"
#include "bril/lumistore/DBUtils.h"
#include <string>
#include <iostream>
#include <vector>
#include <log4cplus/logger.h>
#include "log4cplus/consoleappender.h"
#include "log4cplus/loglevel.h"
#include <log4cplus/loggingmacros.h>

//#include <iomanip>
using namespace bril::lumistore;

int main(){

  std::string lumischema("CMS_LUMI_PROD");
  log4cplus::initialize();
  log4cplus::Logger root = log4cplus::Logger::getRoot();
  log4cplus::SharedAppenderPtr append_1(new log4cplus::ConsoleAppender());
  log4cplus::Logger::getRoot().addAppender(append_1);  
  root.setLogLevel(log4cplus::DEBUG_LOG_LEVEL);
  std::vector<int64_t> ids;
  DBUtils d;
  int t= toolbox::getRuntime()->getPid();
  std::cout<<"pid "<<t<<std::endl;
  for(int i=0;i<10;++i){
    usleep(50000);
    int64_t id = d.getID(t);
    std::cout<<"id "<<id<<std::endl;
    ids.push_back(id);
  }

  std::map<std::string,soci::connection_pool*> pools;
  std::map<std::string,std::string> r;
  d.getPWD("/home/xiezhen/secret","cms_orcoff_prep",r);
  d.getPWD("/home/xiezhen/secret","cms_omds_lb",r);
  const size_t poolSize = 10;
  
  for(std::map<std::string,std::string>::iterator it=r.begin(); it!=r.end(); ++it){
    std::cout<<it->first<<" "<<it->second<<std::endl;   
  }
  
  for(std::map<std::string,std::string>::iterator it=r.begin(); it!=r.end(); ++it){
    soci::connection_pool* pool=new soci::connection_pool(poolSize);
    pools.insert(std::make_pair(it->first,pool));
  }
  uint32_t runnum=246305;
  uint32_t lsnum=2;
  std::string runinfourl("oracle://service=cms_omds_lb user=cms_runinfo_r");
  soci::connection_pool* runinfopool = pools[runinfourl];
  std::string runinfopass=r[runinfourl];
  for(size_t i=0; i!=poolSize; ++i){
    runinfopool->at(i).open(runinfourl+" password="+runinfopass);//open in disconnected state
  }
  soci::session* runinfosession = new soci::session(*runinfopool);
  runinfosession->uppercase_column_names(true);
  soci::transaction* runinfotr = new soci::transaction(*runinfosession);
  std::string shardid("3");
  std::string hltkey;  
  uint32_t hltconfigid;
  bool collidable[3564];
  for(int i=0; i<3564; ++i) collidable[i]=false;
  
  DBUtils::HltData hltdata;

  std::cout<<"#### Test update_run_runinfo"<<std::endl;
  d.update_run_runinfo(root,runinfosession,runnum,hltkey,hltconfigid);
  std::cout<<"hltkey "<<hltkey<<" hltconfigid "<<hltconfigid<<std::endl;

  std::cout<<"#### Test update_ls_hlt"<<std::endl;
  d.update_ls_hlt(root,runinfosession,runnum,lsnum,hltdata);
  for(std::vector<uint32_t>::iterator it=hltdata.hltpaths.begin(); it!=hltdata.hltpaths.end(); ++it){
    size_t pos = it-hltdata.hltpaths.begin();
    std::cout<<"hltpathid "<<*it<<" l1pass "<<hltdata.l1pass[pos]<<" hltaccept "<<hltdata.hltaccept[pos]<<std::endl;
  }

  std::string trgurl("oracle://service=cms_omds_lb user=cms_trg_r");
  soci::connection_pool* trgpool = pools[trgurl];
  std::string trgpass=r[trgurl];
  for(size_t i=0; i!=poolSize; ++i){
    trgpool->at(i).open(trgurl+" password="+trgpass);//open in disconnected state
  }
  soci::session* trgsession = new soci::session(*trgpool);
  trgsession->uppercase_column_names(true);
  soci::transaction* trgtr = new soci::transaction(*trgsession);
  std::string gt_rs_key;
  std::vector<uint16_t> algoids;
  std::vector<uint16_t> techids;
  std::vector<std::string> algoalias;
  std::vector<std::string> techalias;
  std::vector<bool> algomasks;
  std::vector<bool> ttmasks;
  std::cout<<"#### Test update_run_trgconfig"<<std::endl;
  d.update_run_trgconfig(root,trgsession,runnum,gt_rs_key,algoids,algomasks,algoalias,techids,ttmasks,techalias);
  std::cout<<"gt_rs_key "<<gt_rs_key<<" algomasks size "<<algomasks.size()<<" ttmasks "<<ttmasks.size()<<std::endl;
  for(std::vector<uint16_t>::iterator it=algoids.begin(); it!=algoids.end(); ++it){
    size_t pos = it-algoids.begin();
    std::cout<<"algoid "<<*it<<" algoalias "<<algoalias[pos]<<" algomask "<<algomasks[pos]<<std::endl;
  }
  for(std::vector<uint16_t>::iterator it=techids.begin(); it!=techids.end(); ++it){
    size_t pos = it-techids.begin();
    std::cout<<"techid "<<*it<<" techalias "<<techalias[pos]<<" techmask "<<ttmasks[pos]<<std::endl;
  }
  
  uint32_t norb = 1234;
  uint16_t targetegev = 3500;
  uint32_t fillnum = 3833;
 
  std::string beamstatus("STABLE BEAMS");
  float egev = 1234.5;
  float intensity1 = 2.5e11;
  float intensity2 = 5.2e11;
  std::vector<float> bxintensity1;
  std::vector<float> bxintensity2;
  for(size_t i=0; i<3564; ++i){
    bxintensity1.push_back(i*0.25);
    bxintensity2.push_back(i*0.15);
  }
  float deadtimefrac = 0.022;
  std::vector<std::string> providers;
  providers.push_back("BCMF");
  providers.push_back("PLT");
  DBUtils::LumiData lumidata;
  lumidata.avgraw = 12.5;
  lumidata.avg = 17.5;
  lumidata.normtag = "test";
  lumidata.normtagid = -1;
  for(size_t i=0; i<3564; ++i){
    lumidata.bxraw[i] = 2.3;
    lumidata.bx[i] = 3.2;
  }
  bool cmson = false;
  std::string best_normtag = lumidata.normtag ;
  int64_t best_normtagid = lumidata.normtagid;
  std::string provider("BCMF");
  float best_delivered = 1.5;
  float best_recorded = 1.3;
  float best_avgpu = 0.5;

  std::vector<float> best_bxdelivered;
  for(size_t i = 0; i<3564; ++i){
    best_bxdelivered.push_back(0.75);
  }

  toolbox::TimeVal now=toolbox::TimeVal::gettimeofday();
  uint32_t timestampsec = now.sec();
  uint32_t timestampmsec = now.millisec();  
  std::cout<<"#### Test update_ls_trg"<<std::endl;
  DBUtils::TrgData trgdata;
  d.update_ls_trg(root,trgsession,runnum,lsnum,algoids,techids,trgdata);
  for(std::vector<uint16_t>::iterator it=algoids.begin(); it!=algoids.end(); ++it){
    size_t pos = it-algoids.begin();
    std::cout<<"algoid "<<*it<<" algoprescval "<<trgdata.algoprescvals[pos]<<" algocounts "<<trgdata.algocounts[pos]<<std::endl;
  }
  for(std::vector<uint16_t>::iterator it=techids.begin(); it!=techids.end(); ++it){
    size_t pos = it-techids.begin();
    std::cout<<"techid "<<*it<<" techprescval "<<trgdata.techprescvals[pos]<<" techcount "<<trgdata.techcounts[pos]<<std::endl;
  }
  

  delete runinfotr;
  delete runinfosession;
  delete trgtr;
  delete trgsession;

  std::string lumiurl("oracle://service=cms_orcoff_prep user=cms_lumi_prod");
  soci::connection_pool* lumipool = pools[lumiurl];
  std::string lumipass=r[lumiurl];
  for(size_t i=0; i!=poolSize; ++i){
    lumipool->at(i).open(lumiurl+" password="+lumipass);//open in disconnected state
  }

  soci::session* lumisession = new soci::session(*lumipool);
  lumisession->uppercase_column_names(true);

  soci::transaction* lumitr = new soci::transaction(*lumisession);
  try{
    std::cout<<"#### Test exists_rundata"<<std::endl;
    int64_t existsrundata = d.exists_rundata(root, lumisession,lumischema,runnum);
    std::cout<<"exists lumi run data ? "<<existsrundata<<std::endl;
    int64_t rundatatagid = ids[0];
    int64_t lsdatatagid = ids[0];
    
    if(existsrundata){
      std::cout<<"online data exists for run "<<runnum<<" do nothing"<<std::endl;
    }else{
      std::cout<<"#### Test write_ids_datatag "<<lsdatatagid<<std::endl;
      d.do_write_runinfo(root,lumisession,lumischema,runnum,rundatatagid,hltconfigid,hltkey,gt_rs_key,"2nominals_10pilots_RomanPot_Alignment", collidable,2,64);    
    }
    int64_t existslsdata = d.exists_lsdata(root, lumisession,lumischema,runnum,lsnum);
    std::cout<<"exists lumi ls data ? "<<existslsdata<<std::endl;
    if(existslsdata){
      std::cout<<"online data exists for run "<<runnum<<" ls "<<lsnum<<" do nothing"<<std::endl;
    }else{
      std::cout<<"#### Test update_normtagid "<<lsdatatagid<<std::endl;
      lumidata.normtagid = d.exists_normtag(root, lumisession, lumischema, lumidata.normtag );
      best_normtagid = lumidata.normtagid ;
      std::cout<<"DONE"<<std::endl;

      std::cout<<"#### Test write_ids_datatag "<<lsdatatagid<<std::endl;
      d.do_write_ids_datatag(root,lumisession,lumischema,runnum,lsnum, 0, lsdatatagid ,fillnum,timestampsec,false,norb,targetegev, beamstatus);
      std::cout<<"DONE"<<std::endl;

      std::cout<<"#### Test write_timeindex "<<std::endl;
      d.do_write_timeindex(root,lumisession,lumischema,runnum,lsnum,fillnum,timestampsec,timestampmsec);
      std::cout<<"DONE"<<std::endl;

      std::cout<<"#### Test do_write_beam "<<lsdatatagid<<std::endl;      
      d.do_write_beam(root,lumisession,lumischema,shardid,lsdatatagid,egev,intensity1,intensity2,bxintensity1,bxintensity2);
      std::cout<<"DONE"<<std::endl;
      
      std::cout<<"#### Test do_write_deadtime "<<lsdatatagid<<std::endl;
      d.do_write_deadtime(root,lumisession,lumischema,shardid,lsdatatagid,deadtimefrac);
      std::cout<<"DONE"<<std::endl;
      
      for(std::vector<std::string>::iterator it=providers.begin(); it!=providers.end(); ++it){
	std::cout<<"#### Test do_write_rawlumi "<<lsdatatagid<<" "<<*it<<std::endl;
	d.do_write_rawlumi(root,lumisession,lumischema,shardid,*it,lsdatatagid,lumidata);
	std::cout<<"#### Test do_write_resultlumi "<<lsdatatagid<<" "<<*it<<std::endl;	
	d.do_write_resultlumi(root,lumisession,lumischema,shardid,*it,lsdatatagid,lumidata);
      }
      std::cout<<"DONE"<<std::endl;
    }
    std::cout<<"#### Test do_write_bestlumi "<<lsdatatagid<<std::endl;
    d.do_write_bestlumi(root,lumisession,lumischema,shardid,runnum,lsnum,fillnum,cmson,beamstatus,provider,best_normtag,best_normtagid,best_delivered,best_recorded,best_avgpu,best_bxdelivered);
    std::cout<<"DONE"<<std::endl;

    std::cout<<"#### Test do_write_trg "<<lsdatatagid<<std::endl;
    d.do_write_trg(root,lumisession,lumischema,shardid,lsdatatagid,algoids,algomasks,algoalias,techids,ttmasks,techalias,trgdata);
    std::cout<<"DONE"<<std::endl;

    std::cout<<"#### Test do_write_hlt "<<lsdatatagid<<std::endl;
    d.do_write_hlt(root,lumisession,lumischema,shardid,lsdatatagid,hltdata);
    std::cout<<"DONE"<<std::endl;

    lumitr->commit();
    
    toolbox::TimeVal timeelasped = toolbox::TimeVal::gettimeofday() - now;
    std::cout<<"Database operation takes sec: "<<timeelasped.sec()<<"."<<timeelasped.millisec()<<std::endl;
  }catch(bril::lumistore::exception::UniquekeyViolation& e){
    std::cout<<"duplicate entry, do nothing"<<std::endl;
    lumitr->rollback();
    std::cout<<"rolled back"<<std::endl;
  }catch(bril::lumistore::exception::DBError& e){
    std::cout<<"db error "<<e.message()<<std::endl;
    lumitr->rollback();
    std::cout<<"rolled back"<<std::endl;
  }
  delete lumitr;
  delete lumisession;

  for(std::map<std::string,soci::connection_pool*>::iterator it=pools.begin(); it!=pools.end(); ++it){
    delete it->second;
  }

  log4cplus::Logger::shutdown();

}
