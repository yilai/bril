#include "hdf5.h"
#include <stdio.h>
#include <stdlib.h>

#define FILE            "h5ex_t_objref.h5"
#define DATASET         "DS1"
#define DIM0            2

int
main (void)
{
    hid_t       file, space, dset, obj;     /* Handles */
    herr_t      status;
    hsize_t     dims[1] = {DIM0};
    hobj_ref_t  wdata[DIM0],                /* Write buffer */
                *rdata;                     /* Read buffer */
    H5O_type_t  objtype;
    ssize_t     size;
    char        *name;
    int         ndims,
                i;

    /*
     * Create a new file using the default properties.
     * hid_t H5Fcreate( const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id ) 
     * const char *name     	IN: Name of the file to access.
     * uintn flags 	IN: File access flags. Allowable values are:
         H5F_ACC_TRUNC Truncate file, if it already exists, erasing all data previously stored in the file. 
         H5F_ACC_EXCL Fail if file already exists. 
         H5F_ACC_TRUNC and H5F_ACC_EXCL are mutually exclusive; use exactly one.
         additional flag H5F_ACC_DEBUG prints debug information.
     * hid_t fcpl_id 	IN: File creation property list identifier, used when modifying default file meta-data. Use H5P_DEFAULT to specify default file creation properties.
     * hid_t fapl_id 	IN: File access property list identifier. If parallel file access is desired, this is a collective call according to the communicator stored in the fapl_id. Use H5P_DEFAULT for default file access properties.
     */
    file = H5Fcreate (FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    /*
     * Create a dataset with a null dataspace.
     * hid_t H5Screate( H5S_class_t type ) 
     * H5Screate creates a new dataspace of a particular type. Currently supported types are as follows:
         H5S_SCALAR
         H5S_SIMPLE
         H5S_NULL 
         A null dataspace, H5S_NULL, has no data elements. 
     */
    space = H5Screate (H5S_NULL);
    /* Creates a new dataset and links it to a location in the file. 
      hid_t H5Dcreate( hid_t loc_id, const char *name, hid_t type_id, hid_t space_id, hid_t dcpl_id ) 
     */
    obj = H5Dcreate (file, "DS2", H5T_STD_I32LE, space, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dclose (obj);
    status = H5Sclose (space);

    /*
     * Create a new empty group and links it to a location in the file. 
     * hid_t H5Gcreate( hid_t loc_id, const char *name, size_t size_hint ) 
     */
    obj = H5Gcreate (file, "G1", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Gclose (obj);

    /*
     * Create references to the previously created objects.  Passing -1
     * as space_id causes this parameter to be ignored.  Other values
     * besides valid dataspaces result in an error.
     * Creates a reference. 
     * herr_t H5Rcreate( void *ref, hid_t loc_id, const char *name, H5R_type_t ref_type, hid_t space_id ) 
     H5R_type_t ref_type :
         hdset_reg_ref_t  H5R_DATASET_REGION   	Dataset region reference
         hobj_ref_t 	  H5R_OBJECT 	Object reference

     void *ref 	OUT: Reference created by the function call.
     hid_t loc_id 	 IN: Location identifier used to locate the object being pointed to.
     const char *name 	 IN: Name of object at location loc_id.
     H5R_type_t ref_type IN: Type of reference.
     hid_t space_id 	 IN: Dataspace identifier with selection. Used only for dataset region references; pass as -1 if reference is an object reference, i.e., of type H5R_OBJECT. 
     */
    status = H5Rcreate (&wdata[0], file, "G1", H5R_OBJECT, -1);
    status = H5Rcreate (&wdata[1], file, "DS2", H5R_OBJECT, -1);

    /*
     * Create dataspace.  Setting maximum size to NULL sets the maximum
     * size to be the current size.
     */
    space = H5Screate_simple (1, dims, NULL);

    /*
     * Create the dataset and write the object references to it.
     */
    dset = H5Dcreate (file, DATASET, H5T_STD_REF_OBJ, space, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite (dset, H5T_STD_REF_OBJ, H5S_ALL, H5S_ALL, H5P_DEFAULT,wdata);

    /*
     * Close and release resources.
     */
    status = H5Dclose (dset);
    status = H5Sclose (space);
    status = H5Fclose (file);


    /*
     * Now we begin the read section of this example.  Here we assume
     * the dataset has the same name and rank, but can have any size.
     * Therefore we must allocate a new array to read in data using
     * malloc().
     */

    /*
     * Open file and dataset.
     */
    file = H5Fopen (FILE, H5F_ACC_RDONLY, H5P_DEFAULT);
    dset = H5Dopen (file, DATASET, H5P_DEFAULT);

    /*
     * Get dataspace and allocate memory for read buffer.
     */
    space = H5Dget_space (dset);
    ndims = H5Sget_simple_extent_dims (space, dims, NULL);
    rdata = (hobj_ref_t *) malloc (dims[0] * sizeof (hobj_ref_t));

    /*
     * Read the data.
     * herr_t H5Dread( hid_t dataset_id, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t xfer_plist_id, void * buf ) 
     * Reads raw data from a dataset into a buffer. 
     * hid_t dataset_id 	IN: Identifier of the dataset read from.
     * hid_t mem_type_id 	IN: Identifier of the memory datatype.
     * hid_t mem_space_id 	IN: Identifier of the memory dataspace.
     * hid_t file_space_id 	IN: Identifier of the dataset's dataspace in the file.
     * hid_t xfer_plist_id     	IN: Identifier of a transfer property list for this I/O operation.
     * void * buf 	OUT: Buffer to receive data read from file.
     */
    status = H5Dread (dset, H5T_STD_REF_OBJ, H5S_ALL, H5S_ALL, H5P_DEFAULT,rdata);

    /*
     * Output the data to the screen.
     */
    for (i=0; i<dims[0]; i++) {
        printf ("%s[%d]:\n  ->", DATASET, i);

        /*
         * Open the referenced object, get its name and type.
	 * hid_t H5Rdereference( hid_t obj_id, H5R_type_t ref_type, void *ref ) 
	 * hid_t obj_id    IN: Valid identifier for the file containing the referenced object or any object in that file.
	 * H5R_type_t ref_type   IN: The reference type of ref.
	 * void *ref 	  IN: Reference to open.
         */
        obj = H5Rdereference (dset, H5R_OBJECT, &rdata[i]);
	/* Retrieves the type of object that an object reference points to. 
	 * herr_t H5Rget_obj_type( hid_t loc_id, H5R_type_t ref_type, void *ref, H5O_type_t *obj_type ) 
	 */
        status = H5Rget_obj_type (dset, H5R_OBJECT, &rdata[i], &objtype);

        /*
         * Get the length of the name, allocate space, then retrieve the name.
	 * ssize_t H5Iget_name( hid_t obj_id, char *name, size_t size ) 
	 * Retrieves a name of an object based on the object identifier. 
	 * H5Iget_name retrieves a name for the object identified by obj_id. 
	 * Up to size characters of the name are returned in name; additional characters, if any, are not returned to the user application. If the length of the name, which determines the required value of size, is unknown, a preliminary H5Iget_name call can be made. The return value of this call will be the size in bytes of the object name. That value, plus 1 for a NULL terminator, is then assigned to size for a second H5Iget_name call, which will retrieve the actual name. 
         * size_t size 	IN: The size of the name buffer; must be the size of the name in bytes plus 1 for a NULL terminator.
         */
        size = 1 + H5Iget_name (obj, NULL, 0);
        name = (char *) malloc (size);
        size = H5Iget_name (obj, name, size);

        /*
         * Print the object type and close the object.
         */
        switch (objtype) {
            case H5O_TYPE_GROUP:
                printf ("Group");
                break;
            case H5O_TYPE_DATASET:
                printf ("Dataset");
                break;
            case H5O_TYPE_NAMED_DATATYPE:
                printf ("Named Datatype");
        }
        status = H5Oclose (obj);

        /*
         * Print the name and deallocate space for the name.
         */
        printf (": %s\n", name);
        free (name);
    }

    /*
     * Close and release resources.
     */
    free (rdata);
    status = H5Dclose (dset);
    status = H5Sclose (space);
    status = H5Fclose (file);

    return 0;
}
