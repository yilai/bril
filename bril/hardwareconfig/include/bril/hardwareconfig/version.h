// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_hardwareconfig_version_h_
#define _bril_hardwareconfig_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILHARDWARECONFIG_VERSION_MAJOR 3
#define BRIL_BRILHARDWARECONFIG_VERSION_MINOR 1
#define BRIL_BRILHARDWARECONFIG_VERSION_PATCH 10
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILHARDWARECONFIG_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILHARDWARECONFIG_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILHARDWARECONFIG_VERSION_MAJOR,BRIL_BRILHARDWARECONFIG_VERSION_MINOR,BRIL_BRILHARDWARECONFIG_VERSION_PATCH)
#ifndef BRIL_BRILHARDWARECONFIG_PREVIOUS_VERSIONS
#define BRIL_BRILHARDWARECONFIG_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILHARDWARECONFIG_VERSION_MAJOR,BRIL_BRILHARDWARECONFIG_VERSION_MINOR,BRIL_BRILHARDWARECONFIG_VERSION_PATCH)
#else
#define BRIL_BRILHARDWARECONFIG_FULL_VERSION_LIST BRIL_BRILHARDWARECONFIG_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILHARDWARECONFIG_VERSION_MAJOR,BRIL_BRILHARDWARECONFIG_VERSION_MINOR,BRIL_BRILHARDWARECONFIG_VERSION_PATCH)
#endif

namespace brildipprocessor
{
        const std::string project = "bril";
	const std::string package = "brilhardwareconfig";
	const std::string versions = BRIL_BRILHARDWARECONFIG_FULL_VERSION_LIST;
	const std::string summary = "brilDAQ hardware config files";
	const std::string description = "collection of brilDAQ hardware config files. Only xml directory is used. The deployment location /opt/xdaq/htdocs/bril/hardwareconfig/xml .";
	const std::string authors = " ";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
