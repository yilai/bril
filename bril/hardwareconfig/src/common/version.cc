// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#include "bril/hardwareconfig/version.h"
#include "config/version.h"

GETPACKAGEINFO(brilhardwareconfig)

void brilhardwareconfig::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string> > brilhardwareconfig::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config);
	return dependencies;
}
