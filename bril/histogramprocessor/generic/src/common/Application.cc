#include "bril/histogramprocessor/generic/Application.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "toolbox/Runtime.h"
#include "xdata/UnsignedInteger.h"
#include "b2in/nub/Method.h"
#include "toolbox/mem/AutoReference.h"
#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL(bril::histogramprocessorgeneric::Application)

namespace bril{
    namespace histogramprocessorgeneric{

        Application::Application(xdaq::ApplicationStub * s): xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this), m_appLock(toolbox::BSem::FULL) {
            // set icon
            s->getDescriptor()->setAttribute("icon", "/bril/histogramprocessor/generic/html/histogram.png");
	        s->getDescriptor()->setAttribute("icon16x16", "/bril/histogramprocessor/generic/html/histogram.png");

            // web part
            xgi::framework::deferredbind(this, this, &bril::histogramprocessorgeneric::Application::Default, "Default");
            xgi::deferredbind ( this, this, &Application::requestRawData, "requestRawData" );
            LOG4CPLUS_INFO(this->getApplicationLogger(), toolbox::toString("Load URL %s in a browser", getApplicationContext()->getContextDescriptor()->getURL().c_str()));

            // set shutdown listener
            m_shutdown_request = false;
            toolbox::getRuntime()->addShutdownListener( this );

            // set configuration
            try {
                this->getApplicationInfoSpace()->fireItemAvailable("poll_interval", &m_config_poll_interval );
                this->getApplicationInfoSpace()->fireItemAvailable("eventinginput", &m_config_eventing_input );
                this->getApplicationInfoSpace()->fireItemAvailable("channels", &m_config_channels);
                this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
            } catch(xdata::exception::Exception& e) {
                LOG4CPLUS_FATAL(getApplicationLogger(), __func__ << "Failed to set infospace" );
                notifyQualified ( "error", e );
            }

            // now event binding
            b2in::nub::bind(this, &bril::histogramprocessorgeneric::Application::onMessage);

            // memory pool
            /*try {
                toolbox::net::URN m_mem_urn( "mem", getApplicationDescriptor()->getURN() );
                toolbox::mem::HeapAllocator * allocator = new toolbox::mem::HeapAllocator();
                m_memory_pool = toolbox::mem::getMemoryPoolFactory()->createPool(m_mem_urn, allocator);
            } catch(toolbox::task::exception::Exception & e) {
                LOG4CPLUS_FATAL(getApplicationLogger(), __func__ << "Failed to allocate a memory pool" );
            }*/

            // done
            LOG4CPLUS_INFO(this->getApplicationLogger(),  "Done init");
        }

        Application::~Application() {
        }

        void Application::actionPerformed( xdata::Event& e ) {
            LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received xdata event " << e.type() );
            try
            {
                if ( e.type() == "urn:xdaq-event:setDefaultValues" ) //this is used as an initialization signal
                {
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": setDefaultValues " );

                    // set eventing
                    for (size_t element = 0; element < m_config_eventing_input.elements(); element++) {
                        std::string t_bus = m_config_eventing_input[element].getProperty ("bus");
                        std::string t_topics = m_config_eventing_input[element].getProperty ("topics");
                        auto t_topicNames = toolbox::parseTokenSet (t_topics, ",");

                        for(auto t_topic : t_topicNames) {
                            try {
                                LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": subscribing to the topic " << t_topic );
                                this->getEventingBus(t_bus).subscribe(t_topic);
                            } catch (eventing::api::exception::Exception& e) {
                                LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": failed to subscribe to the topic " << t_topic );
                                notifyQualified("error", e);
                            }
                        }
                    }

                    // set workloops
                    toolbox::net::URN inputprocess_urn( "inputprocess", getApplicationDescriptor()->getURN() );
                    m_inputprocess_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( inputprocess_urn.toString(), "waiting" );
                    toolbox::task::ActionSignature* inputprocess_as = toolbox::task::bind( this, &bril::histogramprocessorgeneric::Application::inputProcessJob, "inputprocess" );
                    m_inputprocess_wl->activate();
                    m_inputprocess_wl->submit( inputprocess_as );
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Starting input processing loop " );

                    // set charts
                    setupCharts();

                    // done
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": setDefaultValues finished " );
                }
                else if ( e.type() == "ItemChangedEvent" )
                {
                    std::string item = static_cast<xdata::ItemEvent&>( e ).itemName();
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Parameter " << item << " changed." );

                }
            }
            catch ( xcept::Exception& e )
            {
                notifyQualified( "error", e );
            }

        }

        void Application::actionPerformed( toolbox::Event& e ) {
            LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received toolbox event " << e.type() );
            try
            {
                /*if ( e.type() == "eventing::api::BusReadyToPublish" )
                {
                    std::string busname = static_cast<eventing::api::Bus*>( e.originator() )->getBusName();
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Bus \"" << busname << "\" is ready." );
                }*/
                if ( e.type() == "Shutdown" )
                {
                    // detected shutdown event
                    LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Shutting down" );
                    m_shutdown_request = true;
                    usleep( 500000 );
                    // something to remove
                    delete m_rawoccupancyChart;
                    delete m_avgoccupancyChart;
                    delete m_rateChart;
                }
            }
            catch ( xcept::Exception& e )
            {
                notifyQualified( "error", e );
            }

        }

        void Application::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist ) {

            std::string action = plist.getProperty("urn:b2in-eventing:action");
            if ( action == "notify" && ref!=0 ){
                std::string topic = plist.getProperty("urn:b2in-eventing:topic");
                LOG4CPLUS_DEBUG(getApplicationLogger(), "Received topic " + topic);
                std::pair<std::string, toolbox::mem::Reference*> pair = std::make_pair (topic, ref);
                m_inputQueue.push (pair );
            }

        }

        ////////////////////////////////////////////////////////////////////////////////
        //WORKLOOPS
        ////////////////////////////////////////////////////////////////////////////////
        bool Application::inputProcessJob (toolbox::task::WorkLoop* wl)
        {
            try
            {

                while (m_inputQueue.elements() )
                {
                    //LOG4CPLUS_INFO (m_logger, RED << "Input QUEUE size: " << m_inputQueue.elements() << RESET);
                    std::pair<std::string, toolbox::mem::Reference*> pair = m_inputQueue.pop();
                    toolbox::mem::AutoReference t_refGuard (pair.second); // guarantees ref is release when refguard goes out of scope (like mutex lock_guard)

                    //raw hist
                    if (pair.first == this->getHistogramTopicname() )
                    {
                        rawhistProcessJob(pair.second);
                    }

                }

            }
            catch ( xcept::Exception& e )
            {
                LOG4CPLUS_ERROR ( getApplicationLogger(), __func__ << ": Caught exception " << e.what() );
                notifyQualified ( "error", e );
            }
            usleep(m_config_poll_interval);
            return true;
        }

        void Application::rawhistProcessJob (toolbox::mem::Reference* rawhist) {

            // header
            interface::bril::shared::DatumHead *t_inHeader = (interface::bril::shared::DatumHead*)(rawhist->getDataLocation());

            // variable to store data
            uint32_t nbins = 0;
            uint32_t counterwidth = 0;
            uint32_t incrementwidth = 0;
            uint32_t nunits = 0;
            uint32_t orbitcounter = 0;
            uint8_t counterovf = 0;
            uint8_t incrementovf = 0;

            // extract data
            interface::bril::shared::CompoundDataStreamer t_streamer(this->getHistogramPayloadDict());
            t_streamer.extract_field(&nbins, "nbins", t_inHeader->payloadanchor);
            t_streamer.extract_field(&counterwidth, "counterwidth", t_inHeader->payloadanchor);
            t_streamer.extract_field(&incrementwidth, "incrementwidth", t_inHeader->payloadanchor);
            t_streamer.extract_field(&nunits, "nunits", t_inHeader->payloadanchor);
            t_streamer.extract_field(&orbitcounter, "orbitcounter", t_inHeader->payloadanchor);
            t_streamer.extract_field(&counterovf, "counterovf", t_inHeader->payloadanchor);
            t_streamer.extract_field(&incrementovf, "incrementovf", t_inHeader->payloadanchor);

            //
            LOG4CPLUS_DEBUG(getApplicationLogger(), __func__ << "Check parsing: nbins=" << nbins << ", norbits=" << orbitcounter);

            // check histogram
            if (this->getHistogramParameters().fNbins != nbins ) LOG4CPLUS_ERROR ( getApplicationLogger(), __func__ << ": Wrong histogram number of bins " << (uint32_t)nbins );
            else if (this->getHistogramParameters().fCounterWidth != counterwidth ) LOG4CPLUS_ERROR ( getApplicationLogger(), __func__ << ": Wrong histogram counter width " << (uint32_t)counterwidth );
            else if (this->getHistogramParameters().fIncrementWidth != incrementwidth ) LOG4CPLUS_ERROR ( getApplicationLogger(), __func__ << ": Wrong histogram increment width " << (uint32_t)incrementwidth );
            else if (this->getHistogramParameters().fNunits != nunits ) LOG4CPLUS_ERROR ( getApplicationLogger(), __func__ << ": Wrong histogram n units " << (uint32_t)nunits );
            else {
                // get algo id
                uint32_t algoID = t_inHeader->getAlgoID();
                uint32_t channelID = t_inHeader->getChannelID();

                // fill webchart
                uint32_t bxraw[this->getHistogramParameters().fNbins];
                memset (bxraw, 0, sizeof (bxraw) );
                t_streamer.extract_field(bxraw, "bxraw", t_inHeader->payloadanchor);
                std::vector<uint32_t> bxraw_vec(this->getHistogramParameters().fNbins);
                memcpy(&bxraw_vec[0], &bxraw[0], this->getHistogramParameters().fNbins*sizeof(uint32_t));
                fillChartData<uint32_t> ("raw", bxraw_vec, channelID, algoID);

                // update rate
                std::vector<uint32_t> lRateVec;
                lRateVec.push_back(t_inHeader->lsnum);
                lRateVec.push_back(nbins); // tricky because we take only the nbins here, not the actual size iincluding header and errors
                fillChartData<uint32_t> ("rate", lRateVec, channelID, algoID);
            }
        }

        void Application::Default(xgi::Input * in, xgi::Output * out) {
              *out << "<script src=\"/bril/histogramprocessor/generic/html/highcharts.src.js\"></script>";
              *out << "<h2 align=\"center\">" << this->getApplicationSummary() << "</h2>";
              *out << cgicc::br();
              *out << "<div style = \"float:left; width:50%;\">";
              m_rawoccupancyChart->writeChart (out);
              *out << cgicc::br();
              *out << "</div>";
              *out << "<div style = \"margin-left:50%;\">";
              m_avgoccupancyChart->writeChart (out);
              *out << cgicc::br();
              *out << "</div>";
              *out << "<div style = \"float:left; width:50%;\">";
              m_rateChart->writeChart (out);
              *out << cgicc::br();
              *out << "</div>";
        }

        void Application::requestRawData(xgi::Input * in, xgi::Output * out ) {
            // init
            m_appLock.take();
            cgicc::Cgicc cgi ( in ); // just do this
            bril::webutils::WebChart::dataHeader ( out ); // and this
            // input data
            m_rawoccupancyChart->writeDataForQuery ( cgi, out, m_rawoccupancyData );
            m_avgoccupancyChart->writeDataForQuery ( cgi, out, m_avgoccupancyData );
            m_rateChart->writeDataForQuery ( cgi, out, m_rateData );
            // release
            m_appLock.give();
        }

        void Application::setupCharts() {
            // Vector with Colors
            std::vector<std::string> t_colors = {"#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                                                 "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                                                 "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                                                 "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                                                 "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                                                 "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                                                 "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                                                 "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

                                                 "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                                                 "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                                                 "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                                                 "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                                                 "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
                                                 "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
                                                 "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
                                                 "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379"
                                                };

            std::vector<std::string> t_colors_overview = {"#e6194b", "#3cb44b", "#ffe119", "#0082c8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#d2f53c", "#fabebe", "#800000", "#008080", "#000000"};

            // Callback strings
            std::string callbackRaw = this->getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN() + "/requestRawData";

            // Chart
            bril::webutils::WebChart::property_map propmap;
            for (uint16_t element = 0; element < m_config_channels.elements(); element++ ) {
                std::stringstream ss;
                ss << "Channel_" << m_config_channels[element].getProperty("channel");
                propmap[ss.str()] = bril::webutils::WebChart::series_properties();
                m_rawoccupancyData[ss.str()] = std::vector<uint32_t>(5, 0); // dummy to avoid having empty histogram
                m_avgoccupancyData[ss.str()] = std::vector<float>(5, 0); // dummy to avoid having empty histogram
                m_avgoccupancyCount[ss.str()] = 0;
                m_rateData[ss.str()] = std::vector<float>();
            }
            m_rawoccupancyChart = new bril::webutils::WebChart ("chart_rawoccupancy", callbackRaw,
                                                                "type: 'column', zoomType: 'x', animation: 'false'",
                                                                "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Raw Occupancy Data [NB]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
                                                                propmap);
            m_avgoccupancyChart = new bril::webutils::WebChart ("chart_avgoccupancy", callbackRaw,
                                                                "type: 'column', zoomType: 'x', animation: 'false'",
                                                                "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Average Occupancy Data [NB]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Average Counts', useHTML: true }, allowDecimals: true }", //other options
                                                                propmap);

            m_rateChart = new bril::webutils::WebChart ("chart_rate", callbackRaw,
                                                                "type: 'column', zoomType: 'x', animation: 'false'",
                                                                "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Accumulated Data Size' }, tooltip: { enable : false }, xAxis: {title: {text: 'Lumi Section', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Accumulated Data Size [KBytes]', useHTML: true }, allowDecimals: true }", //other options
                                                                propmap);

        }

        template<typename DataT> void Application::fillChartData (std::string type, std::vector<DataT> bufVec, uint32_t channelID, uint32_t algoID)
        {
            // channel
            std::stringstream ss;
            ss << "Channel_" << channelID;

            m_appLock.take();
            if (type == "raw") {
                // first raw data
                m_rawoccupancyData[ss.str()] = bufVec;
                m_rawoccupancyChart->newDataReady();

                // now avg
                auto temp_avgoccupancyCount = m_avgoccupancyCount[ss.str()];
                if (temp_avgoccupancyCount > 0) {
                    auto temp_avgoccupancyData = m_avgoccupancyData[ss.str()];
                    for(uint16_t i = 0; i < temp_avgoccupancyData.size(); i++) {
                        temp_avgoccupancyData[i] = temp_avgoccupancyData[i]*temp_avgoccupancyCount/(temp_avgoccupancyCount+1.0) + bufVec[i]/(temp_avgoccupancyCount+1.0);
                        //if (bufVec[i] > 0) LOG4CPLUS_INFO ( getApplicationLogger(), __func__ << ": Non-zero bin " << i << " with " << bufVec[i] << " counts" );
                    }
                    m_avgoccupancyData[ss.str()] = temp_avgoccupancyData;
                    m_avgoccupancyChart->newDataReady();
                } else m_avgoccupancyData[ss.str()] = std::vector<float>(bufVec.begin(), bufVec.end());
                m_avgoccupancyCount[ss.str()] = temp_avgoccupancyCount + 1;

            } else if (type == "rate") {
                // assign
                auto lumisection = bufVec[0];
                auto totalwords = bufVec[1];
                float totalsize = totalwords*32.0/1024.0;
                // now make sure we have enough data
                auto temp_rateData = m_rateData[ss.str()];
                // now do counting
                if (lumisection == temp_rateData.size()-1) temp_rateData[lumisection] += totalsize;
                else if (lumisection == temp_rateData.size()) temp_rateData.push_back(totalsize);
                else if (lumisection > temp_rateData.size()) {
                    temp_rateData.resize(lumisection);
                    temp_rateData.push_back(totalsize);
                } else if (lumisection < temp_rateData.size()-1) {
                    temp_rateData.clear();
                    temp_rateData.resize(lumisection);
                    temp_rateData.push_back(totalsize);
                }
                m_rateData[ss.str()] = temp_rateData;
                m_rateChart->newDataReady();
            }
            m_appLock.give();

        }



    }
}