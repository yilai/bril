#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xdata/TimeVal.h"
#include "b2in/nub/Method.h"
#include "xdata/InfoSpaceFactory.h"

#include "interface/bril/RADMONTopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "bril/radmonprocessor/Application.h" 
#include "bril/radmonprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>
#include <bitset>

XDAQ_INSTANTIATOR_IMPL (bril::radmonprocessor::Application)

bril::radmonprocessor::Application::Application (xdaq::ApplicationStub* s): xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_mutex(toolbox::BSem::FULL)
{
  m_fillnum = 0;
  m_runnum  = 0;
  m_lsnum   = 0;
  m_nbnum   = 0;  
  m_tssec   = 0;
  m_tsmsec  = 0;
  m_bus.fromString("brildata");
  m_fluxcalibtag.fromString("None");
  m_lumicalibtag.fromString("None");
  m_flashIS = 0;
  m_publishcount = 0;
  /**
     bind "Default" method as my web page method
  */
  xgi::framework::deferredbind(this,this,&bril::radmonprocessor::Application::Default,"Default");
  
  /*
    bind "onMessage" with b2in actions from the eventing
  */
  b2in::nub::bind(this, &bril::radmonprocessor::Application::onMessage);
  
  /**
     some information about this application
  */ 
  
  m_poolFactory = toolbox::mem::getMemoryPoolFactory();

  toolbox::net::URN memurn("toolbox-mem-pool",getApplicationDescriptor()->getClassName()+"_mem"); 
  toolbox::mem::HeapAllocator * allocator = new toolbox::mem::HeapAllocator();
  try{   
    m_memPool = m_poolFactory->createPool(memurn,allocator);
  }catch(xcept::Exception& e){
    std::string msg("Failed to setup memory pool ");
    LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
    XCEPT_RETHROW(bril::radmonprocessor::exception::Exception,msg,e);	
  }

  // Initialization of default values
  // 
  try
  {
    getApplicationInfoSpace()->fireItemAvailable("bus",    &m_bus    );

    getApplicationInfoSpace()->fireItemAvailable("inTopics",    &m_inputtopicsStr    );

    getApplicationInfoSpace()->fireItemAvailable("outTopics",   &m_outputtopicsStr   );

    getApplicationInfoSpace()->fireItemAvailable("fluxcalibtag",     &m_fluxcalibtag   );

    getApplicationInfoSpace()->fireItemAvailable("lumicalibtag",     &m_lumicalibtag   );

    getApplicationInfoSpace()->fireItemAvailable("fluxcalibcoeff",   &m_fluxcalibcoeffs);

    getApplicationInfoSpace()->fireItemAvailable("lumicalibcoeff",   &m_lumicalibcoeffs);

    getApplicationInfoSpace()->fireItemAvailable("lumipluschannels", &m_lumipluschannels );

    getApplicationInfoSpace()->fireItemAvailable("lumiminuschannels",&m_lumiminuschannels);

    getApplicationInfoSpace()->fireItemAvailable("dimdnsnode",&m_dimdnsnode);

    getApplicationInfoSpace()->fireItemAvailable("dimdnsport",&m_dimdnsport);

    getApplicationInfoSpace()->fireItemAvailable("dimserver", &m_dimserver );
    
    getApplicationInfoSpace()->fireItemAvailable("dimservice", &m_dimservice );
    
    getApplicationInfoSpace()->fireItemAvailable("dimcommand", &m_dimcommand );
    
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues"     );
    m_wl_acquire_publish = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_acquire_publish","waiting"); 
    m_as_acquire_publish = toolbox::task::bind(this,&bril::radmonprocessor::Application::acquire_publish,"acquire_publish");
  }catch(xdata::exception::Exception& e){
    std::string msg = "Failed to setup application infospace ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::radmonprocessor::exception::Exception,msg,e); 
  }
  m_wl_acquire_publish = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_acquire_publish","waiting");
  m_as_acquire_publish = toolbox::task::bind(this,&bril::radmonprocessor::Application::acquire_publish,"acquire_publish");
}

bril::radmonprocessor::Application::~Application ()
{
  if(m_radmon_service) delete m_radmon_service;
  cleanFlash();
}


// This makes the HyperDAQ page

void bril::radmonprocessor::Application::Default (xgi::Input * in, xgi::Output * out)
{
  //*out << busesToHTML();
  *out<<"Hello";
}


// Sets default values of application according to parameters in the 
// xml configuration file.  Listens for xdata::Event and reacts when 
// it's of type "urn:xdaq-event:setDefaultValues"

void bril::radmonprocessor::Application::actionPerformed(xdata::Event& e)
{
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());

  std::stringstream msg;

  if( e.type() == "urn:xdaq-event:setDefaultValues" ){      
    //Lumi calibration factors
    for(size_t i=0;i<m_lumicalibcoeffs.elements();++i){
      xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_lumicalibcoeffs.elementAt(i));
      if (p){
	float slope = std::atof(p->getProperty("slope").c_str());
	float intercept = std::atof(p->getProperty("intercept").c_str()); 
	lumicalibslopes[i] = slope;
	lumicalibintercepts[i] = intercept;
      }
    }     
    m_inputtopics = toolbox::parseTokenList(m_inputtopicsStr.value_,",");
    m_outputtopics = toolbox::parseTokenList(m_outputtopicsStr.value_,",");
    m_wl_acquire_publish->activate();
    connectToDIM();
    this->getEventingBus(m_bus.value_).addActionListener(this);

    // declare flashlist
    std::string urn = createQualifiedInfoSpace("lumimon_radmon").toString();
    m_flashIS = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(urn));  
    initFlash();  
  }
}

void  bril::radmonprocessor::Application::actionPerformed(toolbox::Event& e)
{  
  if ( e.type() == "eventing::api::BusReadyToPublish" ){
    std::stringstream msg;
    msg<< "Eventing bus '" << m_bus.toString() << "' is ready to publish";
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());    
    for( std::list<std::string>::iterator it=m_inputtopics.begin(); it!=m_inputtopics.end(); ++it ){
      try{      
	this->getEventingBus(m_bus.value_).subscribe(*it);
      }catch(eventing::api::exception::Exception& e){
	std::string msg("Failed to subscribe to eventing bus "+m_bus.value_+" topic "+*it);
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	XCEPT_RETHROW(bril::radmonprocessor::exception::Exception,msg,e);     
      }        
    }
  }
}

void bril::radmonprocessor::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist)
{
  std::string action = plist.getProperty("urn:b2in-eventing:action"); 
  if (action == "notify"){
    if( ref !=0 ){
      std::string topic = plist.getProperty("urn:b2in-eventing:topic");
      LOG4CPLUS_DEBUG(this->getApplicationLogger(), std::string("Received topic ")+topic);
      std::string data_version = plist.getProperty("DATA_VERSION");
      std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
      if(data_version.empty() || data_version!=interface::bril::shared::DATA_VERSION){
	std::string msg("Received bril message "+topic+" with no or wrong version, do not process.");
	LOG4CPLUS_ERROR(getApplicationLogger(),msg);
	ref->release(); ref=0;
	return;
      }      
      
      interface::bril::shared::DatumHead * inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());      
      if ( topic == "NB4" ){
	m_mutex.take();
	m_fillnum = inheader->fillnum;
	m_runnum  = inheader->runnum;
	m_lsnum   = inheader->lsnum;
	m_nbnum   = inheader->nbnum;  
	m_tssec   = inheader->timestampsec;
	m_tsmsec  = inheader->timestampmsec;
	m_mutex.give();
	if( m_fillnum>0&&m_runnum>0&&m_lsnum>0 ){//discard crazy time
	  m_wl_acquire_publish->submit(m_as_acquire_publish);
	}      
      }else if ( topic == "beam" ){
	if(payloaddict.empty()){
	  std::string msg("Received beam with no dictionary, do not process");
	  LOG4CPLUS_INFO(getApplicationLogger(),msg);
	  ref->release(); ref = 0;
	  return;
	}
	interface::bril::shared::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());      
	char beam_mode[50];
	tc.extract_field(beam_mode, "status", inheader->payloadanchor);
	m_beam_mode = std::string(beam_mode);
      }
    }
  }
  if(ref!=0) ref->release();
}

bool bril::radmonprocessor::Application::acquire_publish(toolbox::task::WorkLoop* wl){  
  m_mutex.take();
  int fillnum = m_fillnum;
  int runnum = m_runnum;
  int lsnum = m_lsnum;
  int nbnum = m_nbnum;
  int tssec =  m_tssec;
  int tsmsec = m_tsmsec; 
  m_mutex.give();

  std::stringstream ss;  
  ss<<"acquire_publish: Request DIM server to send data for the fill " << fillnum << ", run " << runnum << ", ls "<<lsnum<<", nb " << nbnum;  
  LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); 
  ss.str(""); ss.clear();

  usleep(50000); //synchronize with beam

  // Make command 
  m_cmd_data.command = 0;  	      // "0" means  "publish data" 
  m_cmd_data.cmd_arg = 0;  	     // Not used
  m_cmd_data.tstamp = tssec;
  m_cmd_data.msec = tsmsec; 
  m_cmd_data.fill = fillnum;	   
  m_cmd_data.runnr = runnum;       
  m_cmd_data.lumisection = lsnum;
  m_cmd_data.nibble = nbnum;
  for (int i=0; i<16; i++) {m_cmd_data.data[i] = 0.; } // Not used  here

  // Send the request to DIM server
  int status = DimClient::sendCommand(m_dimcommand.value_.c_str(), (void*) &m_cmd_data, sizeof(m_cmd_data));

  if (status == 0) {    
    LOG4CPLUS_ERROR(getApplicationLogger(), "DIM server is not responding");  
    return false;
  }

  if( strcmp(m_radmon_service->getName(), m_dimservice.value_.c_str()) != 0) {
    LOG4CPLUS_ERROR(getApplicationLogger(), "dimservice name mismatch");  
    return false;
  }
  // Read data and  check  if nibble/ls is the same (500 ms)
  bool data_ready = false;
  for(int npasses=0; npasses<100; ++npasses){
    m_radmon_data =  (radmon_data_t*)m_radmon_service->getData();
    if (m_radmon_data->runnr==runnum && m_radmon_data->nibble == nbnum && m_radmon_data->lumisection == lsnum){
      data_ready = true;
      ss<<"Acquired data after npasses "<<npasses;
      LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); 
      break;
    }    
    usleep(5000);
  }
  
  if ( !data_ready ) {
    std::string msg("Data not ready!");
    LOG4CPLUS_INFO(getApplicationLogger(), msg);  // Not sure if it's just info
    return false; // skip this time interval
  }  
 
  // 5. Publish raw data
  std::list<std::string>::iterator fIt;
  fIt = std::find( m_outputtopics.begin(), m_outputtopics.end(), interface::bril::radmonrawT::topicname() ); 
  if( fIt!=m_outputtopics.end() ){
    publishRawData(fillnum,runnum,lsnum,nbnum,tssec,tsmsec);
  }

  // 6. Process and publish Lumi
  fIt = std::find( m_outputtopics.begin(), m_outputtopics.end(), interface::bril::radmonlumiT::topicname() );
  if( fIt!=m_outputtopics.end() ){
    publishLumi(fillnum,runnum,lsnum,nbnum,tssec,tsmsec);
  }
  // 7. Process and publish Flux
  fIt = std::find( m_outputtopics.begin(), m_outputtopics.end(), interface::bril::radmonfluxT::topicname() );
  if( fIt!=m_outputtopics.end() ){
    publishFlux(fillnum,runnum,lsnum,nbnum,tssec,tsmsec); 
  }

  return false;
}


void bril::radmonprocessor::Application::calculateLumi(const std::string& channels, float& lumi, float& rms)
{
  float sum = 0.;
  int nch = 0;
  
//         Channels to calculate lumi
  std::vector<std::string> chnls;
  std::list<std::string> chnls_list = toolbox::parseTokenList(channels,",");
  std::copy( chnls_list.begin(), chnls_list.end(), std::back_inserter( chnls ) );
  
  // Calculate lumi
  for (size_t i=0; i<chnls.size(); i++)
  {
    int n = atoi(chnls.at(i).c_str());
    std::bitset<16> mask(m_radmon_data->status[n]);
    if (mask[0] == 1)
    {
      float v = m_radmon_data->rate[n] *  lumicalibslopes[n] + lumicalibintercepts[n];
      sum += v;
      nch++;
    }
  }

  // Calculate RMS
  if (nch <= 0) {
    lumi = 0.;
    rms = 0.;
    return;
  }
  else 
  {
    lumi = sum/nch;
    sum = 0.;
    for (size_t i=0; i<chnls.size(); i++)
    {
      int n = atoi(chnls.at(i).c_str());
      std::bitset<16> mask(m_radmon_data->status[n]);

      if (mask[0] == 1)
      {
        float v = m_radmon_data->rate[n] *  lumicalibslopes[n] + lumicalibintercepts[n];
        sum += (v - lumi) * (v - lumi);
      }
    }
    rms = sqrt(sum) / nch;
//     std::stringstream ss;
//     ss.str("");
//     ss << "Channels: " << channels << " nchan: " <<  nch << "  Lumi: " << lumi << "  rms: " << rms ;
//     LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
    
    return;
  }
}

void bril::radmonprocessor::Application::calculateAvgLumi(const std::string& channels, float& lumi)
{
  float sum = 0.;
  int nch = 0;
  
//         Channels to calculate lumi
  std::vector<std::string> chnls;
  std::list<std::string> chnls_list = toolbox::parseTokenList(channels,",");
  std::copy( chnls_list.begin(), chnls_list.end(), std::back_inserter( chnls ) );

  // Calculate lumi
  for (size_t i=0; i<chnls.size(); i++){
    int n = atoi(chnls.at(i).c_str());
    std::bitset<16> mask(m_radmon_data->status[n]);
    if (mask[0] == 1){
      float v = m_radmon_data->rate[n] *  lumicalibslopes[n] + lumicalibintercepts[n];
      sum += v;
      nch++;
    }
  }
  
  if (nch <= 0) {
    lumi = 0.;
    LOG4CPLUS_INFO(getApplicationLogger(), "calculateAvgLumi: get no lumi channels.");
    return;
  } else {
    lumi = sum/nch;
    std::stringstream ss;
    ss << "calculateAvgLumi: channels: " << channels << " nchan: " <<  nch << "  lumi: " << lumi;
    //     LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
    return;
  }
}

void bril::radmonprocessor::Application::connectToDIM()
{ 
    DimClient::setDnsNode(m_dimdnsnode.c_str(), m_dimdnsport);
    m_radmon_service = new DimInfo(m_dimservice.c_str(), -1);
}

void bril::radmonprocessor::Application::publishRawData(int fillnum,int runnum,int lsnum, int nbnum, int tssec,int tsmsec)
{   
  std::stringstream ss;
  ss<<"publishRawData "<<fillnum<<" "<<runnum<<" "<<lsnum<<" "<<nbnum;  
  LOG4CPLUS_INFO(getApplicationLogger(), ss.str() ); 

  float rate[interface::bril::NRADMONCHANNELS];
  float voltage[interface::bril::NRADMONCHANNELS];
  float current[interface::bril::NRADMONCHANNELS];
  uint16_t status[interface::bril::NRADMONCHANNELS];
  for(unsigned int i=0; i<interface::bril::NRADMONCHANNELS; ++i){
    rate[i] = 0;
    voltage[i] = 0;
    current[i] = 0;
    status[i] = 0;
  }

  uint16_t readouttime = m_radmon_data->dt_readout;  
  xdata::Properties plist;
  plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
  std::string payloaddict = interface::bril::radmonrawT::payloaddict();
  plist.setProperty("PAYLOAD_DICT",payloaddict);
  toolbox::mem::Reference * ref = NULL;
  
  try{
    ref = m_poolFactory->getFrame(m_memPool,interface::bril::radmonrawT::maxsize());
  }catch(toolbox::mem::exception::Exception &ex){
    std::string msg("Failed to allocate memory for radmonraw");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(ex));
    XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
    this->notifyQualified("error",myerrorobj);
    return;
  }

  ref->setDataSize(interface::bril::radmonrawT::maxsize());
  interface::bril::radmonrawT * data = reinterpret_cast<interface::bril::radmonrawT *>(ref->getDataLocation());
  interface::bril::shared::CompoundDataStreamer tc(payloaddict);
  
  memcpy( rate, m_radmon_data->rate, sizeof(rate) );
  memcpy( voltage, m_radmon_data->voltage, sizeof(voltage) );
  memcpy( current, m_radmon_data->current, sizeof(current) );
  memcpy( status, m_radmon_data->status, sizeof(status) );
  
  tc.insert_field(data->payloadanchor, "rate",    rate);
  tc.insert_field(data->payloadanchor, "voltage", voltage);
  tc.insert_field(data->payloadanchor, "current", current);
  tc.insert_field(data->payloadanchor, "status",  status);
  tc.insert_field(data->payloadanchor, "readouttime", &readouttime);   
  
  data->setTime(fillnum,runnum,lsnum,nbnum,tssec,tsmsec);
  data->setResource(interface::bril::shared::DataSource::RADMON,interface::bril::RADMONAlgos::SIMPLE_CALIBRATION,0,interface::bril::shared::StorageType::COMPOUND);
  data->setFrequency(interface::bril::shared::FrequencyType::NB4);
  data->setTotalsize(interface::bril::radmonrawT::maxsize());      
  
  try{
    getEventingBus(m_bus.value_).publish(interface::bril::radmonrawT::topicname(),ref,plist);
  }catch(std::exception & ex){
    std::string msg("Failed to publish rawdata");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg); 
    ref->release(); ref = 0;
    XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
    this->notifyQualified("error",myerrorobj);
  }
}

void bril::radmonprocessor::Application::publishLumi(int fillnum,int runnum,int lsnum,int nbnum,int tssec,int tsmsec)
{ 
  std::stringstream ss;
  ss<<"publishLumi "<<fillnum<<" "<<runnum<<" "<<lsnum<<" "<<nbnum;  
  LOG4CPLUS_INFO(getApplicationLogger(), ss.str() ); 

  xdata::Properties plist;
  plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
  if( m_beam_mode!="STABLE BEAMS" && m_beam_mode!="SQUEEZE" && m_beam_mode!="FLAT TOP" && m_beam_mode!="ADJUST"){
    plist.setProperty("NOSTORE", "1");
  }
  std::string payloaddict = interface::bril::radmonlumiT::payloaddict();
  plist.setProperty("PAYLOAD_DICT",payloaddict);
  if( m_beam_mode!="STABLE BEAMS" && m_beam_mode!="SQUEEZE" && m_beam_mode!="FLAT TOP" && m_beam_mode!="ADJUST"){
    plist.setProperty("NOSTORE", "1");
  }

  toolbox::mem::Reference * ref = NULL;
  try{
    ref = m_poolFactory->getFrame(m_memPool,interface::bril::radmonlumiT::maxsize());
  }catch(toolbox::mem::exception::Exception &ex){
    std::string msg("Failed to allocate memory for radmonlumi topic");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(ex));
    XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
    this->notifyQualified("error",myerrorobj);
    return;
  }

  ref->setDataSize(interface::bril::radmonlumiT::maxsize());
  
  std::string all_lumichannels = m_lumiminuschannels.value_ + "," + m_lumipluschannels.value_;
  float avglumi = 0;
  calculateAvgLumi(all_lumichannels, avglumi);  

  interface::bril::radmonlumiT * data = reinterpret_cast<interface::bril::radmonlumiT *>(ref->getDataLocation());
  interface::bril::shared::CompoundDataStreamer tc(payloaddict);
  tc.insert_field(data->payloadanchor, "calibtag" ,  m_lumicalibtag.value_.c_str() );
  tc.insert_field(data->payloadanchor, "avgraw",    &avglumi);
  tc.insert_field(data->payloadanchor, "avg",    &avglumi);

  data->setTime(fillnum,runnum,lsnum,nbnum,tssec,tsmsec);
  data->setResource(interface::bril::shared::DataSource::RADMON,interface::bril::RADMONAlgos::SIMPLE_CALIBRATION,0,interface::bril::shared::StorageType::COMPOUND);
  data->setFrequency(interface::bril::shared::FrequencyType::NB4);
  data->setTotalsize(interface::bril::radmonlumiT::maxsize());      
  ss.str(""); ss.clear();
  ss<<"lumi "<<avglumi<<",NOSTORE "<<plist.getProperty("NOSTORE");
  LOG4CPLUS_INFO(getApplicationLogger(), ss.str() ); 

  bool fireflash = false;
  if( m_flashIS && m_publishcount>15 ) {
    fireflash=true;
  }

  try{
    getEventingBus(m_bus.value_).publish(interface::bril::radmonlumiT::topicname(),ref,plist);
  }catch(std::exception & ex){
    std::string msg("Failed to publish radmonlumi");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg); 
    ref->release(); ref = 0;
    XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
    this->notifyQualified("error",myerrorobj);
  }
  m_publishcount++;

  if( fireflash ){// fire to flashlist only every 16 times of firing to eventing	  	
    try{
      xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_flashIS->find("fillnum") );
      p_fillnum->value_ = fillnum; 
      
      xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_flashIS->find("runnum") );
      p_runnum->value_ = runnum; 
      
      xdata::UnsignedInteger32* p_lsnum = dynamic_cast< xdata::UnsignedInteger32* >( m_flashIS->find("lsnum") );
      p_lsnum->value_ = lsnum;
      
      
      xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( m_flashIS->find("timestamp") );
      p_timestamp->value_.sec( tssec );
      p_timestamp->value_.usec( tsmsec*1000 );
      
      xdata::Float* p_avglumi =  dynamic_cast< xdata::Float* >( m_flashIS->find("avglumi") );
      p_avglumi->value_ = avglumi;
      ss.str(""); ss.clear();
      ss<<"Firing flash radmonlumi";
      LOG4CPLUS_INFO(getApplicationLogger(), ss.str() ); 
      m_flashIS->fireItemGroupChanged(m_flashfields,this);
      
    }catch(xdata::exception::Exception& e){
      std::string msg("Failed to fire flashlist");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
      XCEPT_DECLARE_NESTED(bril::radmonprocessor::exception::Exception,myerrorobj,msg,e);
      notifyQualified("error",myerrorobj);
    }
    m_publishcount = 0;
  }
}

void bril::radmonprocessor::Application::publishFlux(int fillnum,int runnum,int lsnum,int nbnum,int tssec,int tsmsec)
{  
  std::stringstream ss;
  ss<<"publishFlux "<<fillnum<<" "<<runnum<<" "<<lsnum<<" "<<nbnum;  
  LOG4CPLUS_INFO(getApplicationLogger(), ss.str() ); 

  float flux[interface::bril::NRADMONCHANNELS];
  xdata::Properties plist;
  plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION); 
  std::string payloaddict = interface::bril::radmonfluxT::payloaddict();
  plist.setProperty("PAYLOAD_DICT",payloaddict);
  if( m_beam_mode!="STABLE BEAMS" && m_beam_mode!="SQUEEZE" && m_beam_mode!="FLAT TOP" && m_beam_mode!="ADJUST"){
    plist.setProperty("NOSTORE", "1");
  }

  toolbox::mem::Reference * ref = NULL; 
  try{
    ref = m_poolFactory->getFrame(m_memPool,interface::bril::radmonfluxT::maxsize());
  }catch(toolbox::mem::exception::Exception &ex){
    std::string msg("Failed to allocate memory for radmonflux");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(ex));
    XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
    this->notifyQualified("error",myerrorobj);
    return;
  }   
  ss.str(""); ss.clear();
  ss<<"flux: ";
  ref->setDataSize(interface::bril::radmonfluxT::maxsize());  
  for (unsigned int i=0; i<interface::bril::NRADMONCHANNELS; i++){
    std::bitset<16> mask(m_radmon_data->status[i]);
    if (mask[0] == 1) {
      float fluff = dynamic_cast<xdata::Float*>(m_fluxcalibcoeffs.elementAt(i))->value_;
      flux[i] =  m_radmon_data->rate[i] * fluff;
      ss<<i<<","<<flux[i]<<" ";
    }else{
      flux[i] = 0.;
    }
  }
  ss<<",NOSTORE "<<plist.getProperty("NOSTORE");
  LOG4CPLUS_INFO(getApplicationLogger(), ss.str() ); 

  interface::bril::radmonfluxT * header = (interface::bril::radmonfluxT *)(ref->getDataLocation());
  memcpy( header->payloadanchor, flux, sizeof(float)*interface::bril::NRADMONCHANNELS );
  header->setTime(fillnum,runnum,lsnum,nbnum,tssec,tsmsec);
  header->setResource(interface::bril::shared::DataSource::RADMON,interface::bril::RADMONAlgos::SIMPLE_CALIBRATION,0, interface::bril::shared::StorageType::FLOAT);
  header->setFrequency(interface::bril::shared::FrequencyType::NB4);
  header->setTotalsize(interface::bril::radmonfluxT::maxsize());      

  try{
    getEventingBus(m_bus.value_).publish(interface::bril::radmonfluxT::topicname(),ref,plist);
  }catch(std::exception & ex){
    std::string msg("Failed to publish radmonflux");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg); 
    ref->release(); ref = 0;
    XCEPT_DECLARE(exception::Exception,myerrorobj,msg);
    this->notifyQualified("error",myerrorobj);
  }
}

    
void bril::radmonprocessor::Application::initFlash(){
  std::vector< xdata::Serializable*> fieldvalues;
  std::list< std::string > fieldnames;
      std::string fieldname;
      fieldname = "fillnum";
      m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "runnum";
      m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "lsnum";
      m_flashvalues.push_back( new xdata::UnsignedInteger32(0) );
      m_flashfields.push_back( fieldname );

      fieldname = "timestamp";
      m_flashvalues.push_back( new xdata::TimeVal(toolbox::TimeVal()) );
      m_flashfields.push_back( fieldname );
      
      fieldname = "avglumi";
      m_flashvalues.push_back( new xdata::Float(0.) );
      m_flashfields.push_back( fieldname );                 
      
      try{
	std::vector<xdata::Serializable*>::const_iterator vIt = m_flashvalues.begin();
	std::list<std::string>::const_iterator nameIt=m_flashfields.begin(); 
	for(; nameIt!=m_flashfields.end(); ++nameIt, ++vIt ){
	  m_flashIS->fireItemAvailable(*nameIt,*vIt);
	}
      }catch(xdata::exception::Exception& e){
	std::string msg( "Failed to declare flashlist radmonlumi" );
	LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
	XCEPT_RETHROW(xdaq::exception::Exception, msg, e);
      }
}   

void bril::radmonprocessor::Application::cleanFlash(){
  for(std::vector< xdata::Serializable* >::iterator it=m_flashvalues.begin(); it!=m_flashvalues.end(); ++it){
     delete *it;
  }
  m_flashvalues.clear();
} 

