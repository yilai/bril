// $Id$
#ifndef _bril_radmonprocessor_version_h_
#define _bril_radmonrocessor_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILRADMONPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILRADMONPROCESSOR_VERSION_MINOR 0
#define BRIL_BRILRADMONPROCESSOR_VERSION_PATCH 0
#define BRILRADMONPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILRADMONPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILRADMONPROCESSOR_VERSION_MAJOR,BRIL_BRILRADMONPROCESSOR_VERSION_MINOR,BRIL_BRILRADMONPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILRADMONPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILRADMONPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILRADMONPROCESSOR_VERSION_MAJOR,BRIL_BRILRADMONPROCESSOR_VERSION_MINOR,BRIL_BRILRADMONPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILRADMONPROCESSOR_FULL_VERSION_LIST BRIL_BRILRADMONPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILRADMONPROCESSOR_VERSION_MAJOR,BRIL_BRILRADMONPROCESSOR_VERSION_MINOR,BRIL_BRILRADMONPROCESSOR_VERSION_PATCH)
#endif

namespace brilradmonprocessor
{
  const std::string project = "bril";
  const std::string package     = "brilradmonprocessor";
  const std::string versions = BRIL_BRILRADMONPROCESSOR_FULL_VERSION_LIST;
  const std::string summary     = "BRIL DAQ radmonprocessor";
  const std::string description = "collect and process RADMON data from a DIM server";
  const std::string authors     = "Arkday Lokhovitskiy";
  const std::string link        = "";

  config::PackageInfo getPackageInfo ();
  
  void checkPackageDependencies ();

  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
