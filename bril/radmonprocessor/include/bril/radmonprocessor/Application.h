#ifndef _bril_radmonprocessor_Application_h_
#define _bril_radmonprocessor_Application_h_
#include <string>
#include <list>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Integer.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/BSem.h"

// =============
#include "dimServices.h"
#include <dic.hxx>

namespace bril
{
  namespace radmonprocessor
  {
    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener
    {
    public:

      XDAQ_INSTANTIATOR();
      
      Application (xdaq::ApplicationStub* s);

      virtual ~Application ();

      void Default (xgi::Input * in, xgi::Output * out);

      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);  

      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);

      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);     

    private:

      // some information about this application
      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool              *m_memPool;

      int m_fillnum;
      int m_runnum;
      int m_lsnum;
      int m_nbnum;
      int m_tssec;
      int m_tsmsec;
      std::string m_beam_mode;

      // eventinginput configuration  
      xdata::String m_bus;
      xdata::String m_inputtopicsStr;      
      xdata::String m_outputtopicsStr;
      std::list<std::string> m_inputtopics;
      std::list<std::string> m_outputtopics;

      // Calibration parameters
      //
      xdata::String m_fluxcalibtag;
      xdata::String m_lumicalibtag;

      xdata::Vector<xdata::Float> m_fluxcalibcoeffs;

      // Lumi +Z side channels

      xdata::String  m_lumipluschannels;

      // Lumi +Z side channels

      xdata::String  m_lumiminuschannels;
      
      xdata::Vector<xdata::Properties> m_lumicalibcoeffs;
      std::map<int, float> lumicalibslopes;
      std::map<int, float> lumicalibintercepts;

      // DIM server connection parameters

      xdata::String   m_dimdnsnode;
      xdata::Integer  m_dimdnsport;
      xdata::String   m_dimserver;
      xdata::String   m_dimservice;
      xdata::String   m_dimcommand;     

      xdata::InfoSpace* m_flashIS;
      int m_publishcount;
      std::vector< xdata::Serializable* >  m_flashvalues;
      std::list< std::string >	m_flashfields;

      private:

      // Main acquisition function: called from onMessage on receiving NB4
      //
      bool acquire_publish(toolbox::task::WorkLoop* wl);

      // (Re)connect to a DIM server instance 
      // 
      void connectToDIM();
      
      // Calculate and publish luminosity

      void publishRawData(int fillnum,int runnum,int lsnum,int nbnum,int tssec,int tsmsec);      

      // Calculate and publish luminosity

      void publishLumi(int fillnum,int runnum,int lsnum,int nbnum,int tssec,int tsmsec);

      // Calculate and publish flux

      void publishFlux(int fillnum,int runnum,int lsnum,int nbnum,int tssec,int tsmsec);      
      
      void initFlash();
      void cleanFlash();

      void calculateLumi(const std::string& channels, float& lumi, float& rms);
      void calculateAvgLumi(const std::string& channels, float& lumi);
      
      // Radmon service
      DimInfo *m_radmon_service;
      // Data ready flag  (DIM callback method received new data)
    
      // Structure to hold data
      radmon_data_t *m_radmon_data;  
      //Structure to hold command
      radmon_cmd_t  m_cmd_data;      

      toolbox::task::WorkLoop* m_wl_acquire_publish;
      toolbox::task::ActionSignature* m_as_acquire_publish;

      // work loop termination flag
      // 
      //bool m_terminate;      
      
      // It's used to perform mutual exclusive access 
      // to shared resources  in Timers and WorkLoops
      // 
      toolbox::BSem m_mutex;

      // nocopy protection

      Application(const Application&); Application& operator=(const Application&);
    };
  }
}
#endif
