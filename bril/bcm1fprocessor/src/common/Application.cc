#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"

#include "bril/bcm1fprocessor/Application.h"
#include "bril/bcm1fprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>

#include "bril/webutils/WebUtils.h"

XDAQ_INSTANTIATOR_IMPL (bril::bcm1fprocessor::Application)

const int bril::bcm1fprocessor::Application::s_nBcm1fChannels;
const int bril::bcm1fprocessor::Application::s_nAllChannels;
const int bril::bcm1fprocessor::Application::s_maxnbx;
const int bril::bcm1fprocessor::Application::s_num_nb_to_collect;
const int bril::bcm1fprocessor::Application::s_minimum_present_channels;
const int bril::bcm1fprocessor::Application::s_cache_lifetime_sec;
constexpr float bril::bcm1fprocessor::Application::s_areaSilicon;
constexpr float bril::bcm1fprocessor::Application::s_sec_per_nibble;

bril::bcm1fprocessor::Application::Application (xdaq::ApplicationStub* s) : xdaq::Application (s), xgi::framework::UIManager (this), eventing::api::Member (this), m_applock (toolbox::BSem::FULL)
{
    xgi::framework::deferredbind (this, this, &bril::bcm1fprocessor::Application::Default, "Default");
    xgi::framework::deferredbind (this, this, &bril::bcm1fprocessor::Application::plotting, "plotting");
    xgi::framework::deferredbind (this, this, &bril::bcm1fprocessor::Application::RawRHUhists, "RawRHUhists");
    xgi::framework::deferredbind (this, this, &bril::bcm1fprocessor::Application::RawBPTXhists, "RawBPTXhists");
    xgi::deferredbind ( this, this, &Application::requestData, "requestData" ); //naked data
    b2in::nub::bind (this, &bril::bcm1fprocessor::Application::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor = getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    m_last_process_time = toolbox::TimeVal::gettimeofday();
    m_nHistsDiscarded = 0;
    m_BG1_plus = 0;
    m_BG2_minus = 0;
    m_BG1_nc = 0;
    m_BG2_nc = 0;
    m_BG1_lead = 0;
    m_BG2_lead = 0;
    m_last4_bkg1[0] = m_last4_bkg1[1] = m_last4_bkg1[2] = m_last4_bkg1[3] = 0;
    m_last4_bkg1_lead[0] = m_last4_bkg1_lead[1] = m_last4_bkg1_lead[2] = m_last4_bkg1_lead[3] = 0;
    m_last4_bkg1_nc[0] = m_last4_bkg1_nc[1] = m_last4_bkg1_nc[2] = m_last4_bkg1_nc[3] = 0;

    m_last4_bkg2[0] = m_last4_bkg2[1] = m_last4_bkg2[2] = m_last4_bkg2[3] = 0;
    m_last4_bkg2_lead[0] = m_last4_bkg2_lead[1] = m_last4_bkg2_lead[2] = m_last4_bkg2_lead[3] = 0;
    m_last4_bkg2_nc[0] = m_last4_bkg2_nc[1] = m_last4_bkg2_nc[2] = m_last4_bkg2_nc[3] = 0;

    m_lumi_avg = 0;
    m_lumi_avg_raw = 0;
    memset (m_lumi_bx, 0, sizeof (m_lumi_bx) );
    memset (m_lumi_bx_raw, 0, sizeof (m_lumi_bx_raw) );
    memset (m_lumi_perchannel_bx, 0, sizeof (m_lumi_perchannel_bx) );
    memset (m_lumi_perchannel_bxRaw, 0, sizeof (m_lumi_perchannel_bxRaw) );

    //  memset (m_useChannelForLumi,true,sizeof(m_useChannelForLumi));
    memset (m_useChannelForLumi, false, sizeof (m_useChannelForLumi) );
    memset (m_useChannelForBkgd, false, sizeof (m_useChannelForBkgd) );
    //memset(m_sensorConnected,false,sizeof(m_sensorConnected));

    memset (m_bkgd1mask, false, sizeof (m_bkgd1mask) );
    memset (m_bkgd2mask, false, sizeof (m_bkgd2mask) );
    memset (m_bkgd1leadmask, false, sizeof (m_bkgd1leadmask) );
    memset (m_bkgd2leadmask, false, sizeof (m_bkgd2leadmask) );
    memset (m_bkgd1NCmask, false, sizeof (m_bkgd1NCmask) );
    memset (m_bkgd2NCmask, false, sizeof (m_bkgd2NCmask) );
    memset (m_collmask, false, sizeof (m_collmask) );
    memset (m_channel_spike_mask, true, sizeof(m_channel_spike_mask));
    for(size_t i=0; i < sizeof(m_channel_count_nospike)/sizeof(m_channel_count_nospike[0]) ; i++ ){
        m_channel_count_nospike[i] = m_time_spike;
    }

    memset (m_rates_perchannel_percollidingbunches, 0., sizeof( m_rates_perchannel_percollidingbunches));
    memset (m_lumi_percshape, 0., sizeof( m_lumi_percshape));
    memset (m_lumi_percshapeRaw, 0., sizeof( m_lumi_percshapeRaw));

    m_vdmflag = false;
    // for (unsigned int bunch = 0; bunch < 3564; bunch++)
    //   {
    //     if (m_bkgd1mask[bunch] == true)
    //       {
    //      std::stringstream ss;
    //         ss << "(memset) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
    //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    //       }
    //     if (m_bkgd2mask[bunch] == true)
    //       {
    //      std::stringstream ss;
    //         ss << "(memset) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
    //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    //       }
    //   }


    m_iTot1 = 0.;
    m_iTot2 = 0.;

    memset (m_iBunch1, 0., sizeof (m_iBunch1) );
    memset (m_iBunch2, 0., sizeof (m_iBunch2) );

    //  memset (m_totalChannelRate,0.,sizeof(m_totalChannelRate));
    //  std::fill(begin(m_totalChannelRate), end(m_totalChannelRate), 0.); // not needed?
    //  m_totalChannelRate(64,0.); // initialize xdata::Vector of xdata::Floats? NOPE
    //  m_totalChannelRate = new xdata::Vector();
    //  m_totalChannelRate.resize(s_nAllChannels);
    m_totalChannelRate.resize (s_nAllChannels, 0.);
    m_lumiChannel.resize(52, 0.);
    m_lumiChannelRaw.resize(52, 0.);
    // for (int i = 0; i < 64; i++)
    //   {
    //     m_totalChannelRate.push_back(0.);
    //   }
    //  std::fill(m_totalChannelRate.begin(), m_totalChannelRate.end(), 0.);

    m_countAlbedoHigherThanBackground = 0;

    m_beammode = "";

    m_tp_mask_start.clear();
    m_tp_mask_end.clear();

    toolbox::net::URN memurn ("toolbox-mem-pool", m_classname + "_mem");

    try
    {
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool (memurn, allocator);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_datasources");
        m_appInfoSpace->fireItemAvailable ("eventinginput", &m_datasources);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_outputtopics");
        m_appInfoSpace->fireItemAvailable ("eventingoutput", &m_outputtopics);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_nRHUs");
        m_appInfoSpace->fireItemAvailable ("nRHUs", &m_nRHUs);
        m_appInfoSpace->fireItemAvailable ("TestPulseMask", &m_tp_mask);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_calibtag");
        m_appInfoSpace->fireItemAvailable ("calibtag", &m_calibtag);
        // //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_sigma_vis");
        // m_appInfoSpace->fireItemAvailable("sigmavis",&m_sigma_vis);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_sigma_vis_si");
        m_appInfoSpace->fireItemAvailable ("sigmavissi", &m_sigma_vis_si);
        m_appInfoSpace->fireItemAvailable ("channel_trip_threshold", &m_trip_threshold);
        m_appInfoSpace->fireItemAvailable ("lower_spike_ratio", &m_lower_spike_ratio);
        m_appInfoSpace->fireItemAvailable ("upper_spike_ratio", &m_upper_spike_ratio);
        m_appInfoSpace->fireItemAvailable ("time_spike", &m_time_spike);
        m_appInfoSpace->fireItemAvailable ("SBILcalibsi", &m_calib_at_SBIL_si);
        m_appInfoSpace->fireItemAvailable ("nonlinearitysi", &m_nonlinearity_si);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_verbose");
        m_appInfoSpace->fireItemAvailable ("verbose", &m_verbose);

        m_appInfoSpace->fireItemAvailable ("albedo_calculation_time", &m_albedo_calculation_time);
        m_appInfoSpace->fireItemAvailable ("albedo_queue_length", &m_albedo_queue_length);
        m_appInfoSpace->fireItemAvailable ("noise_calc_start", &m_noise_calc_start);
        m_appInfoSpace->fireItemAvailable ("noise_calc_end", &m_noise_calc_end);

        m_appInfoSpace->fireItemAvailable ("albedo_model_si", &m_albedo_model_si_str);
        m_appInfoSpace->fireItemAvailable ("excluded_channels_lumi", &m_excluded_channels_lumi);
        m_appInfoSpace->fireItemAvailable ("excluded_channels_bkgd", &m_excluded_channels_bkgd);
        //m_appInfoSpace->fireItemAvailable("sensorConnected",&m_sensorConnected);
        // //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_useChannelCalibrations");
        // m_appInfoSpace->fireItemAvailable("useChannelCalibrations",&m_useChannelCalibrations);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelCalibrations");
        // m_appInfoSpace->fireItemAvailable("channelCalibrations",&m_channelCalibrations);
        // //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelAcceptances");
        m_appInfoSpace->fireItemAvailable ("channelAcceptances", &m_channelAcceptances);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelEfficiencies");
        m_appInfoSpace->fireItemAvailable ("channelEfficiencies", &m_channelEfficiencies);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelCorrections");
        m_appInfoSpace->fireItemAvailable ("channelCorrections", &m_channelCorrections);
        m_appInfoSpace->addListener (this, "urn:xdaq-event:setDefaultValues");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop (m_appDescriptor->getURN() + "_publishing", "waiting");
        m_albedo_calculation = toolbox::task::getWorkLoopFactory()->getWorkLoop (m_appDescriptor->getURN() + "_albedo_calculation", "waiting");
        setupMonitoring();
        m_tab = Tab::DEFAULT;
    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to setup memory pool ");
        LOG4CPLUS_FATAL (getApplicationLogger(), msg + stdformat_exception_history (e) );
        XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg, e);
    }

}

bril::bcm1fprocessor::Application::~Application ()
{
    QueueStoreIt it;

    for (it = m_topicoutqueues.begin(); it != m_topicoutqueues.end(); ++it)
        delete it->second;

    m_topicoutqueues.clear();
}


// This makes the HyperDAQ page
void bril::bcm1fprocessor::Application::Default (xgi::Input* in, xgi::Output* out)
{
    //  m_applock.take();
    m_tab = Tab::DEFAULT;
    this->createTabBar(out, m_tab);

    *out << busesToHTML();
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN();
    *out << "URL: " << appurl;
    *out << cgicc::br();

    *out << cgicc::table().set ("class", "xdaq-table-vertical");
    *out << cgicc::caption ("input/output");
    *out << cgicc::tbody();

    *out << cgicc::tr();
    *out << cgicc::th ("calibtag");
    *out << cgicc::td ( m_calibtag );
    *out << cgicc::tr();

    // *out << cgicc::tr();
    // *out << cgicc::th("sigmaVis");
    // *out << cgicc::td( m_sigma_vis.toString() );
    // *out << cgicc::tr();


    *out << cgicc::tr();
    *out << cgicc::th ("sigmaVis (Si)");
    *out << cgicc::td ( m_sigma_vis_si.toString() );
    *out << cgicc::tr();

    // *out << cgicc::tr();
    // *out << cgicc::th("useChannelCalibrations");
    // *out << cgicc::td( m_useChannelCalibrations.toString() );
    // *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("in_topic");
    *out << cgicc::td ( interface::bril::bcm1fhistT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("in_topic");
    *out << cgicc::td ( interface::bril::beamT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("in_topic");
    *out << cgicc::td ( interface::bril::vdmflagT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("in_topic");
    *out << cgicc::td ( interface::bril::NB4T::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("out_topic");
    *out << cgicc::td ( interface::bril::bcm1fagghistT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("out_topic");
    *out << cgicc::td ( interface::bril::bcm1fbkgT::topicname() );
    *out << cgicc::tr();

    // *out << cgicc::tr();
    // *out << cgicc::th("out_topic");
    // *out << cgicc::td( interface::bril::bcm1fbkghistosT::topicname() );
    // *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("out_topic");
    *out << cgicc::td ( interface::bril::bcm1flumiT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("out_topic");
    *out << cgicc::td ( interface::bril::bcm1flumiperchannelT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("out_topic");
    *out << cgicc::td ( interface::bril::bxconfigT::topicname() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("Fill number");
    *out << cgicc::td ( m_mon_fill.toString() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("Run number");
    *out << cgicc::td ( m_mon_run.toString() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("LS number");
    *out << cgicc::td ( m_mon_ls.toString() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("Nibble number");
    *out << cgicc::td ( m_mon_nb.toString() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("# hists discarded on LS/run boundary");
    *out << cgicc::td ( m_nHistsDiscarded.toString() );
    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();



    *out << "List of all valid (connected) channels: ";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-horizontal");
    *out << cgicc::tbody();

    *out << cgicc::tr();

    //  *out << cgicc::th("Channels");
    for (xdata::Integer channel = 0; channel < s_nBcm1fChannels; channel++)
        *out << cgicc::td ( channel.toString() );

    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();

    *out << "Channels included in luminosity: ";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-horizontal");
    *out << cgicc::tbody();

    *out << cgicc::tr();

    //  *out << cgicc::th("Channels");
    for (xdata::Integer channel = 0; channel < s_nBcm1fChannels; channel++)
    {
        if (m_useChannelForLumi[channel.value_])
            *out << cgicc::td ( channel.toString() );
    }

    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();

    *out << "Channels included in background: ";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-horizontal");
    *out << cgicc::tbody();

    *out << cgicc::tr();

    //  *out << cgicc::th("Channels");
    for (xdata::Integer channel = 0; channel < s_nBcm1fChannels; channel++)
    {
        if (m_useChannelForBkgd[channel.value_])
            *out << cgicc::td ( channel.toString() );
    }

    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();


    *out << "The current background numbers ARE: ";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-vertical");
    *out << cgicc::tbody();

    *out << cgicc::tr();
    *out << cgicc::th ("BG 1");
    *out << cgicc::td ( m_BG1_plus.toString() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("BG 2");
    *out << cgicc::td ( m_BG2_minus.toString() );
    *out << cgicc::tr();

    *out << cgicc::tr();
    *out << cgicc::th ("How many times is albedo higher than background?");
    *out << cgicc::td ( xdata::Integer (m_countAlbedoHigherThanBackground).toString() );
    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();

    *out << "Bunches used for luminosity:";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-horizontal").set("style", "width: 100vh;display: block;overflow-y: auto;");
    *out << cgicc::tbody();

    *out << cgicc::tr();
    for (int i = 0; i < s_maxnbx; i++) {
        if (m_collmask[i])
            *out << cgicc::td(std::to_string(i));
    }
    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();

    *out << "Bunches used for BG 1:";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-horizontal").set("style", "width: 100vh;display: block;overflow-y: auto;");
    *out << cgicc::tbody();

    *out << cgicc::tr();
    for (int i = 0; i < s_maxnbx; i++) {
        if (m_bkgd1mask[i])
            *out << cgicc::td(std::to_string(i));
    }
    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();

    *out << "Bunches used for BG 2:";
    *out << cgicc::br();
    *out << cgicc::table().set ("class", "xdaq-table-horizontal").set("style", "width: 100vh;display: block;overflow-y: auto;");
    *out << cgicc::tbody();

    *out << cgicc::tr();
    for (int i = 0; i < s_maxnbx; i++) {
        if (m_bkgd2mask[i])
            *out << cgicc::td(std::to_string(i));
    }
    *out << cgicc::tr();

    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::br();
}

void bril::bcm1fprocessor::Application::plotting (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::PLOTTING;
    this->createTabBar(out, m_tab);

    using namespace cgicc;
    using std::endl;

    //bril::webutils::WebChart::pageHeader ( out );
    //Load the local highcharts.src.js
    //*out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.src.js\"></script>";
    //*out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.js\"></script>";
    //*out << "<script src=\"/bril/bcm1fprocessor/html/boost.js\"></script>";

    *out << cgicc::div().set ( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
    *out << "<div style = \"float:left; width:50%;\">";
    m_chart_lumi_raw_si->writeChart ( out );
    *out << "</div>";
    *out << cgicc::div().set ( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
    *out << "<div style = \"float:left; width:50%;\">";
    m_chart_albedoqueue_si->writeChart ( out );
    *out << "</div>";

    m_chart_devel->writeChart ( out );
    *out << "</div>";
    m_chart_fourbinsperbx1->writeChart (out);
}
void bril::bcm1fprocessor::Application::RawRHUhists (xgi::Input* in, xgi::Output* out)
{

    m_tab = Tab::RHU;
    this->createTabBar(out, m_tab);

    using namespace cgicc;
    using std::endl;

    //bril::webutils::WebChart::pageHeader ( out );
    //Load the local highcharts.src.js
    //*out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.src.js\"></script>";
    //*out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.js\"></script>";
    //*out << "<script src=\"/bril/bcm1fprocessor/html/boost.js\"></script>";

    *out << cgicc::div().set ( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
    *out << "<div style = \"float:left; width:50%;\">";
    m_chart_fourbinsperbx1->writeChart ( out );
    *out << "</div> <div style = \"margin-left:50%;\">";
    m_chart_fourbinsperbx2->writeChart ( out );
    *out << "</div>";
    *out << cgicc::div().set ( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
    *out << "<div style = \"float:left; width:50%;\">";
    m_chart_fourbinsperbx3->writeChart ( out );
    *out << "</div> <div style = \"margin-left:50%;\">";
    m_chart_fourbinsperbx4->writeChart ( out );
    *out << "</div>";
    *out << "<div style = \"float:left; width:50%;\">";
    m_chart_fourbinsperbx5->writeChart ( out );
    *out << "</div> <div style = \"margin-left:50%;\">";
    m_chart_fourbinsperbx6->writeChart ( out );
    *out << "</div>";
}

void bril::bcm1fprocessor::Application::RawBPTXhists (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::BPTX;
    this->createTabBar(out, m_tab);

    using namespace cgicc;
    using std::endl;

    //bril::webutils::WebChart::pageHeader ( out );
    //Load the local highcharts.src.js
    //*out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.src.js\"></script>";
    //*out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.js\"></script>";
    //*out << "<script src=\"/bril/bcm1fprocessor/html/boost.js\"></script>";

    *out << cgicc::div().set ( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
    *out << "<div style = \"float:left; width:100%;\">";
    m_chart_BPTX->writeChart ( out );
    *out << "</div>";
    *out << "<div style = \"float:left; width:100%;\">";
    m_chart_BPTX_perBX->writeChart ( out );
    *out << "</div>";
    //*out << cgicc::div().set ( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
    //*out << "<div style = \"float:left; width:50%;\">";
    //m_chart_fourbinsperbx3->writeChart ( out );
    //*out << "</div> <div style = \"margin-left:50%;\">";
    //m_chart_fourbinsperbx4->writeChart ( out );
    //*out << "</div>";
    //*out << "<div style = \"float:left; width:50%;\">";
    //m_chart_fourbinsperbx5->writeChart ( out );
    //*out << "</div> <div style = \"margin-left:50%;\">";
    //m_chart_fourbinsperbx6->writeChart ( out );
    //*out << "</div>";
}

void bril::bcm1fprocessor::Application::setupCharts()
{
    using namespace webutils;
    std::string cb = getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestData";

    WebChart::property_map lumiplotpmap;
    lumiplotpmap["corrected"] = WebChart::series_properties( );//"#33cccc" );
    lumiplotpmap["uncorrected"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_lumi_raw_si = new WebChart ( "Lumirawsi", cb, "type: 'line', zoomType: 'xy', animation: false",
                                         "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                         //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
                                         "title: { text: 'Lumi raw Silicon' }, tooltip: { enable : false }", lumiplotpmap );

    m_chart_albedoqueue_si = new WebChart ( "Albedoqueuesi", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
                                            "title: { text: 'Albedo Queue Silicon' }, tooltip: { enable : false }", lumiplotpmap );


    WebChart::property_map develplotpmap;
    develplotpmap["hist1"] = WebChart::series_properties( );//"#33cccc" );
    develplotpmap["hist2"] = WebChart::series_properties( );//"#33cccc" );
    //  develplotpmap["hist3"] = WebChart::series_properties( );//"#33cccc" );
    //  develplotpmap["hist4"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_devel = new WebChart ( "devel", cb, "type: 'line', zoomType: 'xy', animation: false",
                                   "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                   "title: { text: 'Development Histogram' }, tooltip: { enable : false }", develplotpmap );

    WebChart::property_map fourbinsperbxplotpmap1;
    fourbinsperbxplotpmap1["Channel 1"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 2"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 3"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 4"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 5"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 6"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 7"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap1["Channel 8"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_fourbinsperbx1 = new WebChart ( "fourbinsperbx1", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap1 );
    WebChart::property_map fourbinsperbxplotpmap2;
    fourbinsperbxplotpmap2["Channel 09"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 10"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 11"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 12"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 13"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 14"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 15"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap2["Channel 16"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_fourbinsperbx2 = new WebChart ( "fourbinsperbx2", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap2 );
    WebChart::property_map fourbinsperbxplotpmap3;
    fourbinsperbxplotpmap3["Channel 17"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 18"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 19"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 20"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 21"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 22"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 23"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap3["Channel 24"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_fourbinsperbx3 = new WebChart ( "fourbinsperbx3", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap3 );
    WebChart::property_map fourbinsperbxplotpmap4;
    fourbinsperbxplotpmap4["Channel 25"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 26"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 27"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 28"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 29"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 30"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 31"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap4["Channel 32"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_fourbinsperbx4 = new WebChart ( "fourbinsperbx4", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap4 );
    WebChart::property_map fourbinsperbxplotpmap5;
    fourbinsperbxplotpmap5["Channel 33"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 34"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 35"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 36"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 37"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 38"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 39"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap5["Channel 40"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_fourbinsperbx5 = new WebChart ( "fourbinsperbx5", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap5 );
    WebChart::property_map fourbinsperbxplotpmap6;
    fourbinsperbxplotpmap6["Channel 41"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 42"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 43"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 44"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 45"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 46"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 47"] = WebChart::series_properties( );//"#33cccc" );
    fourbinsperbxplotpmap6["Channel 48"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_fourbinsperbx6 = new WebChart ( "fourbinsperbx6", cb, "type: 'line', zoomType: 'xy', animation: false",
                                            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                            "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap6 );

    //RAW BPTX plots
    WebChart::property_map BPTXplotpmap;
    BPTXplotpmap["B1"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["B1 or B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["B1 and B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["B1 and !B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["!B1 and B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["Beam Gas 0"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap["Beam Gas 1"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_BPTX = new WebChart ( "BPTX_raw", cb, "type: 'line', zoomType: 'xy', animation: false",
                                  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                  "title: { text: 'RAW BPTX Histogram' }, tooltip: { enable : false }", BPTXplotpmap );

    //aggregated BPTX plots
    WebChart::property_map BPTXplotpmap_perBX;
    BPTXplotpmap_perBX["B1"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["B1 or B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["B1 and B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["B1 and !B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["!B1 and B2"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["Beam Gas 0"] = WebChart::series_properties( );//"#33cccc" );
    BPTXplotpmap_perBX["Beam Gas 1"] = WebChart::series_properties( );//"#33cccc" );
    m_chart_BPTX_perBX = new WebChart ( "BPTX", cb, "type: 'line', zoomType: 'xy', animation: false",
                                        "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
                                        "title: { text: 'BPTX Histogram' }, tooltip: { enable : false }", BPTXplotpmap_perBX );
}

void bril::bcm1fprocessor::Application::requestData ( xgi::Input* in, xgi::Output* out )
{
    //LOG4CPLUS_INFO(getApplicationLogger(), "*************data requested *************");
    using namespace cgicc;
    bril::webutils::WebChart::dataHeader ( out );
    Cgicc cgi ( in );

    std::map<std::string, std::vector<float>> map_lumiraw_si;
    map_lumiraw_si["corrected"] = filterHistForPlot (m_lumi_bx_raw, 3564);
    map_lumiraw_si["uncorrected"] = filterHistForPlot (m_raw_uncor_si, 3564);
    m_chart_lumi_raw_si->writeDataForQuery ( cgi, out,  map_lumiraw_si);

    std::map<std::string, std::vector<float>> map_albedo_si;
    map_albedo_si["corrected"] = filterHistForPlot (m_albedoqueue_hist_si_cor, 3564);
    map_albedo_si["uncorrected"] = filterHistForPlot (m_albedoqueue_hist_si_uncor, 3564);
    m_chart_albedoqueue_si->writeDataForQuery ( cgi, out,  map_albedo_si);

    std::map<std::string, std::vector<float>> map_devel;
    map_devel["hist1"] = filterHistForPlot (m_devel_hist1, 3564);
    map_devel["hist2"] = filterHistForPlot (m_devel_hist2, 3564);
    m_chart_devel->newDataReady();
    m_chart_devel->writeDataForQuery ( cgi, out,  map_devel);

    std::map<std::string, std::vector<float>> map_fourbinsperbx1;
    map_fourbinsperbx1["Channel 1"] = filterHistForPlot (m_fourbinsperbx[0], 14256);
    map_fourbinsperbx1["Channel 2"] = filterHistForPlot (m_fourbinsperbx[1], 14256);
    map_fourbinsperbx1["Channel 3"] = filterHistForPlot (m_fourbinsperbx[2], 14256);
    map_fourbinsperbx1["Channel 4"] = filterHistForPlot (m_fourbinsperbx[3], 14256);
    map_fourbinsperbx1["Channel 5"] = filterHistForPlot (m_fourbinsperbx[4], 14256);
    map_fourbinsperbx1["Channel 6"] = filterHistForPlot (m_fourbinsperbx[5], 14256);
    map_fourbinsperbx1["Channel 7"] = filterHistForPlot (m_fourbinsperbx[6], 14256);
    map_fourbinsperbx1["Channel 8"] = filterHistForPlot (m_fourbinsperbx[7], 14256);
    m_chart_fourbinsperbx1->newDataReady();
    m_chart_fourbinsperbx1->writeDataForQuery ( cgi, out,  map_fourbinsperbx1);

    std::map<std::string, std::vector<float>> map_fourbinsperbx2;
    map_fourbinsperbx2["Channel 09"] = filterHistForPlot (m_fourbinsperbx[8], 14256);
    map_fourbinsperbx2["Channel 10"] = filterHistForPlot (m_fourbinsperbx[9], 14256);
    map_fourbinsperbx2["Channel 11"] = filterHistForPlot (m_fourbinsperbx[10], 14256);
    map_fourbinsperbx2["Channel 12"] = filterHistForPlot (m_fourbinsperbx[11], 14256);
    map_fourbinsperbx2["Channel 13"] = filterHistForPlot (m_fourbinsperbx[12], 14256);
    map_fourbinsperbx2["Channel 14"] = filterHistForPlot (m_fourbinsperbx[13], 14256);
    map_fourbinsperbx2["Channel 15"] = filterHistForPlot (m_fourbinsperbx[14], 14256);
    map_fourbinsperbx2["Channel 16"] = filterHistForPlot (m_fourbinsperbx[15], 14256);
    m_chart_fourbinsperbx2->newDataReady();
    m_chart_fourbinsperbx2->writeDataForQuery ( cgi, out,  map_fourbinsperbx2);

    std::map<std::string, std::vector<float>> map_fourbinsperbx3;
    map_fourbinsperbx3["Channel 17"] = filterHistForPlot (m_fourbinsperbx[16], 14256);
    map_fourbinsperbx3["Channel 18"] = filterHistForPlot (m_fourbinsperbx[17], 14256);
    map_fourbinsperbx3["Channel 19"] = filterHistForPlot (m_fourbinsperbx[18], 14256);
    map_fourbinsperbx3["Channel 20"] = filterHistForPlot (m_fourbinsperbx[19], 14256);
    map_fourbinsperbx3["Channel 21"] = filterHistForPlot (m_fourbinsperbx[20], 14256);
    map_fourbinsperbx3["Channel 22"] = filterHistForPlot (m_fourbinsperbx[21], 14256);
    map_fourbinsperbx3["Channel 23"] = filterHistForPlot (m_fourbinsperbx[22], 14256);
    map_fourbinsperbx3["Channel 24"] = filterHistForPlot (m_fourbinsperbx[23], 14256);
    m_chart_fourbinsperbx3->newDataReady();
    m_chart_fourbinsperbx3->writeDataForQuery ( cgi, out,  map_fourbinsperbx3);

    std::map<std::string, std::vector<float>> map_fourbinsperbx4;
    map_fourbinsperbx4["Channel 25"] = filterHistForPlot (m_fourbinsperbx[24], 14256);
    map_fourbinsperbx4["Channel 26"] = filterHistForPlot (m_fourbinsperbx[25], 14256);
    map_fourbinsperbx4["Channel 27"] = filterHistForPlot (m_fourbinsperbx[26], 14256);
    map_fourbinsperbx4["Channel 28"] = filterHistForPlot (m_fourbinsperbx[27], 14256);
    map_fourbinsperbx4["Channel 29"] = filterHistForPlot (m_fourbinsperbx[28], 14256);
    map_fourbinsperbx4["Channel 30"] = filterHistForPlot (m_fourbinsperbx[29], 14256);
    map_fourbinsperbx4["Channel 31"] = filterHistForPlot (m_fourbinsperbx[30], 14256);
    map_fourbinsperbx4["Channel 32"] = filterHistForPlot (m_fourbinsperbx[31], 14256);
    m_chart_fourbinsperbx4->newDataReady();
    m_chart_fourbinsperbx4->writeDataForQuery ( cgi, out,  map_fourbinsperbx4);

    std::map<std::string, std::vector<float>> map_fourbinsperbx5;
    map_fourbinsperbx5["Channel 33"] = filterHistForPlot (m_fourbinsperbx[32], 14256);
    map_fourbinsperbx5["Channel 34"] = filterHistForPlot (m_fourbinsperbx[33], 14256);
    map_fourbinsperbx5["Channel 35"] = filterHistForPlot (m_fourbinsperbx[34], 14256);
    map_fourbinsperbx5["Channel 36"] = filterHistForPlot (m_fourbinsperbx[35], 14256);
    map_fourbinsperbx5["Channel 37"] = filterHistForPlot (m_fourbinsperbx[36], 14256);
    map_fourbinsperbx5["Channel 38"] = filterHistForPlot (m_fourbinsperbx[37], 14256);
    map_fourbinsperbx5["Channel 39"] = filterHistForPlot (m_fourbinsperbx[38], 14256);
    map_fourbinsperbx5["Channel 40"] = filterHistForPlot (m_fourbinsperbx[39], 14256);

    m_chart_fourbinsperbx5->writeDataForQuery ( cgi, out,  map_fourbinsperbx5);
    m_chart_fourbinsperbx5->newDataReady();

    std::map<std::string, std::vector<float>> map_fourbinsperbx6;
    map_fourbinsperbx6["Channel 41"] = filterHistForPlot (m_fourbinsperbx[40], 14256);
    map_fourbinsperbx6["Channel 42"] = filterHistForPlot (m_fourbinsperbx[41], 14256);
    map_fourbinsperbx6["Channel 43"] = filterHistForPlot (m_fourbinsperbx[42], 14256);
    map_fourbinsperbx6["Channel 44"] = filterHistForPlot (m_fourbinsperbx[43], 14256);
    map_fourbinsperbx6["Channel 45"] = filterHistForPlot (m_fourbinsperbx[44], 14256);
    map_fourbinsperbx6["Channel 46"] = filterHistForPlot (m_fourbinsperbx[45], 14256);
    map_fourbinsperbx6["Channel 47"] = filterHistForPlot (m_fourbinsperbx[46], 14256);
    map_fourbinsperbx6["Channel 48"] = filterHistForPlot (m_fourbinsperbx[47], 14256);
    m_chart_fourbinsperbx6->newDataReady();
    m_chart_fourbinsperbx6->writeDataForQuery ( cgi, out,  map_fourbinsperbx6);

    //BPTX plots
    //raw / full resolution
    std::map<std::string, std::vector<float>> map_BPTX;
    map_BPTX["B1"] = filterHistForPlot (m_fourbinsperbx[48], 14256);
    map_BPTX["B2"] = filterHistForPlot (m_fourbinsperbx[49], 14256);
    map_BPTX["B1 or B2"] = filterHistForPlot (m_fourbinsperbx[50], 14256);
    map_BPTX["B1 and B2"] = filterHistForPlot (m_fourbinsperbx[51], 14256);
    map_BPTX["B1 and !B2"] = filterHistForPlot (m_fourbinsperbx[52], 14256);
    map_BPTX["!B1 and B2"] = filterHistForPlot (m_fourbinsperbx[53], 14256);
    map_BPTX["Beam Gas 0"] = filterHistForPlot (m_fourbinsperbx[54], 14256);
    map_BPTX["Beam Gas 1"] = filterHistForPlot (m_fourbinsperbx[55], 14256);
    m_chart_BPTX->newDataReady();
    m_chart_BPTX->writeDataForQuery ( cgi, out,  map_BPTX);
    //raw /perBX
    std::map<std::string, std::vector<float>> map_BPTX_perBX;
    map_BPTX_perBX["B1"] = filterHistForPlot (m_BPTXhist[0], 3564);
    map_BPTX_perBX["B2"] = filterHistForPlot (m_BPTXhist[1], 3564);
    map_BPTX_perBX["B1 or B2"] = filterHistForPlot (m_BPTXhist[2], 3564);
    map_BPTX_perBX["B1 and B2"] = filterHistForPlot (m_BPTXhist[3], 3564);
    map_BPTX_perBX["B1 and !B2"] = filterHistForPlot (m_BPTXhist[4], 3564);
    map_BPTX_perBX["!B1 and B2"] = filterHistForPlot (m_BPTXhist[5], 3564);
    map_BPTX_perBX["Beam Gas 0"] = filterHistForPlot (m_BPTXhist[6], 3564);
    map_BPTX_perBX["Beam Gas 1"] = filterHistForPlot (m_BPTXhist[7], 3564);
    m_chart_BPTX_perBX->newDataReady();
    m_chart_BPTX_perBX->writeDataForQuery ( cgi, out,  map_BPTX_perBX);
}

template <typename T>
std::vector<float> bril::bcm1fprocessor::Application::filterHistForPlot (T in, int len)
{
    std::vector<float> ret;

    for (int i = 0; i < len; i++)
    {
        if (!isinf (in[i]) || !isnan (in[i]) )
            ret.push_back (in[i]);
        else
            ret.push_back (-1.0);
    }

    return ret;
}





// Sets default values of application according to parameters in the
// xml configuration file.  Listens for xdata::Event and reacts when it's of type
// "urn:xdaq-event:setDefaultValues"
void bril::bcm1fprocessor::Application::actionPerformed (xdata::Event& e)
{
    LOG4CPLUS_DEBUG (getApplicationLogger(), "Received xdata event " << e.type() );
    std::stringstream msg;

    if ( e.type() == "urn:xdaq-event:setDefaultValues" )
    {
        size_t nsources = m_datasources.elements();

        try
        {
            for (size_t i = 0; i < nsources; ++i)
            {
                xdata::Properties* p = dynamic_cast<xdata::Properties*> (m_datasources.elementAt (i) );
                xdata::String databus;
                xdata::String topicsStr;

                if (p)
                {
                    databus = p->getProperty ("bus");
                    topicsStr = p->getProperty ("topics");
                    std::set<std::string> topics = toolbox::parseTokenSet (topicsStr.value_, ",");
                    m_in_busTotopics.insert (std::make_pair (databus.value_, topics) );
                }
            }

            subscribeall();
            size_t ntopics = m_outputtopics.elements();

            for (size_t i = 0; i < ntopics; ++i)
            {
                xdata::Properties* p = dynamic_cast<xdata::Properties*> (m_outputtopics.elementAt (i) );

                if (p)
                {
                    xdata::String topicname = p->getProperty ("topic");
                    xdata::String outputbusStr = p->getProperty ("buses");
                    std::set<std::string> outputbuses = toolbox::parseTokenSet (outputbusStr.value_, ",");

                    for (std::set<std::string>::iterator it = outputbuses.begin(); it != outputbuses.end(); ++it)
                        m_out_topicTobuses.insert (std::make_pair (topicname.value_, *it) );

                    m_topicoutqueues.insert (std::make_pair (topicname.value_, new toolbox::squeue<toolbox::mem::Reference*>) );
                    m_unreadybuses.insert (outputbuses.begin(), outputbuses.end() );
                }
            }

            TopicStoreIt it = m_out_topicTobuses.find(interface::bril::bcm1flumiperchannelT::topicname());
            if (it != m_out_topicTobuses.end()) {
                // insert them with the same bus as the "parent" topic
                std::string& parentBus = it->second;
                for (int i=0; i<52; ++i)  
                {
                std::string topicname = interface::bril::bcm1flumiperchannelT::topicname() + "_" + std::to_string(i);
                m_topicoutqueues.insert(std::make_pair(topicname, new toolbox::squeue<toolbox::mem::Reference*>));
                m_out_topicTobuses.insert(std::make_pair(topicname, parentBus));
                }
            }


            for (std::set<std::string>::iterator it = m_unreadybuses.begin(); it != m_unreadybuses.end(); ++it)
            {
                this->getEventingBus (*it).addActionListener (this);
                //if(this->getEventingBus(*it).canPublish()) continue;
            }

            // unsigned int myNRhus = (unsigned int) m_nRHUs;
            m_collectchannel = interface::bril::BCM1F_NRU * m_nRHUs;
            std::stringstream ss;
            ss.str ("");
            ss << "Number of RHUs is " << m_nRHUs << ", number of channels to collect is " << m_collectchannel
               //<< ", sigmaVis is " << m_sigma_vis;
               << ", sigmaVis (Si) is " << m_sigma_vis_si;
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

            std::set<std::string> excluded_channels_lumi_str = toolbox::parseTokenSet(m_excluded_channels_lumi, ",");
            std::set<std::string> excluded_channels_bkgd_str = toolbox::parseTokenSet(m_excluded_channels_bkgd, ",");
            std::set<int> excluded_channels_lumi;
            for (std::string chn: excluded_channels_lumi_str) {
                int chn_int = std::stoi(chn);
                if (chn_int < 0 || chn_int > s_nBcm1fChannels)
                    LOG4CPLUS_WARN (getApplicationLogger(), "Invalid exclusion of channel in excluded_channels_lumi");
                else
                    excluded_channels_lumi.insert(chn_int);
            }
            std::set<int> excluded_channels_bkgd;
            for (std::string chn: excluded_channels_bkgd_str) {
                int chn_int = std::stoi(chn);
                if (chn_int < 0 || chn_int > s_nBcm1fChannels)
                    LOG4CPLUS_WARN (getApplicationLogger(), "Invalid exclusion of channel in excluded_channels_bkgd");
                else
                    excluded_channels_bkgd.insert(chn_int);
            }
            LOG4CPLUS_DEBUG (getApplicationLogger(), "Now looping to set active channels for lumi and bkgd");

            for (int i = 0; i < s_nBcm1fChannels; i++)
            {

                std::stringstream ss;
                ss << "Channel " << i ;

                m_useChannelForLumi[i] = excluded_channels_lumi.count(i) == 0;
                m_useChannelForBkgd[i] = excluded_channels_bkgd.count(i) == 0;

                m_useChannelForLumi[i] ? (ss << "  Lumi: True") : (ss << "  Lumi: False");
                m_useChannelForBkgd[i] ? (ss << "  Bkgd: True") : (ss << "  Bkgd: False");


                xdata::Float* channelCorrection1 = dynamic_cast<xdata::Float*> (m_channelAcceptances.elementAt (i) );
                xdata::Float* channelCorrection2 = dynamic_cast<xdata::Float*> (m_channelEfficiencies.elementAt (i) );
                xdata::Float* channelCorrection3 = dynamic_cast<xdata::Float*> (m_channelCorrections.elementAt (i) );

                m_channelScaleFactor[i] = channelCorrection1->value_ * channelCorrection2->value_ * channelCorrection3->value_;

                ss << "  Acceptance: " << channelCorrection1->value_;
                ss << "  Efficiency: " << channelCorrection2->value_;
                ss << "  Other correction factor: " << channelCorrection3->value_;

                ss << "  Overall correction: " << m_channelScaleFactor[i];

                LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );

            }

            size_t tp_mask_size = m_tp_mask.elements();
            for (size_t i = 0; i < tp_mask_size; ++i)
            {
                xdata::Properties* p = dynamic_cast<xdata::Properties*> (m_tp_mask.elementAt (i) );

                if (p)
                {
                    std::stringstream ss;

                    unsigned int t_RHU = std::stoul (p->getProperty ("RHU") );
                    unsigned int t_start = std::stoul (p->getProperty ("start") );
                    unsigned int t_end = std::stoul (p->getProperty ("end") );
                    m_tp_mask_start[t_RHU]=t_start;
                    m_tp_mask_end[t_RHU]=t_end;
                    ss << "TP mask for RHU " << t_RHU << " starts at bin " << t_start <<" and ends at bin " << t_end << " -using default for non-listed values" << std::endl;
                    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
                }
            }


            LOG4CPLUS_DEBUG (getApplicationLogger(), "***DONE*** Now looping to set active channels for lumi and bkgd: ***DONE***");

            // parse albedo model
            std::stringstream albedo_model_si_ss (m_albedo_model_si_str.value_);

            while (albedo_model_si_ss.good() )
            {
                std::string substr;
                getline ( albedo_model_si_ss, substr, ',' );
                m_albedo_model_si.push_back ( stof (substr) );
            }

            if (m_albedo_model_si.size() > 3564)
                LOG4CPLUS_WARN (getApplicationLogger(), "Si albedo model longer than one orbit, entries after 3564 will be ignored");

            std::stringstream ss3;
            ss3 << "Si albedo model: ";

            for (size_t i = 0; i < m_albedo_model_si.size(); i++)
                ss3 << m_albedo_model_si[i] << ",";

            LOG4CPLUS_DEBUG (getApplicationLogger(), ss3.str() );



            setupCharts();


        }
        catch (xdata::exception::Exception& e)
        {
            msg << "Failed to parse application property";
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str() );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg.str(), e);
        }
    }
}



// When buses are ready to publish, start the publishing workloop and
// timer to check for stale cache. Listens for toolbox::Event
void  bril::bcm1fprocessor::Application::actionPerformed (toolbox::Event& e)
{
    if (e.type() == "eventing::api::BusReadyToPublish")
    {
        std::string busname = (static_cast<eventing::api::Bus*> (e.originator() ) )->getBusName();
        std::stringstream msg;
        msg << "event Bus '" << busname << "' is ready to publish";
        m_unreadybuses.erase (busname);

        if (m_unreadybuses.size() != 0) return; //wait until all buses are ready

        try
        {
            toolbox::task::ActionSignature* as_publishing = toolbox::task::bind (this, &bril::bcm1fprocessor::Application::publishing, "publishing");
            m_publishing->activate();
            m_publishing->submit (as_publishing);
        }
        catch (toolbox::task::exception::Exception& e)
        {
            msg << "Failed to start publishing workloop " << stdformat_exception_history (e);
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str() );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg.str(), e);
        }

        try
        {
            std::string appuuid = m_appDescriptor->getUUID().toString();
            toolbox::TimeInterval checkinterval (10, 0); // check every 10 seconds
            std::string timername ("stalecachecheck_timer");
            toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer (timername + "_" + appuuid);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate (start, this, checkinterval, 0, timername);
        }
        catch (toolbox::exception::Exception& e)
        {
            std::string msg ("failed to start timer ");
            LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg, e);
        }

        try
        {
            toolbox::task::ActionSignature* as_albedoing = toolbox::task::bind (this, &bril::bcm1fprocessor::Application::albedo_calculation, "publishing");
            m_albedo_calculation->activate();
            m_albedo_calculation->submit (as_albedoing);
        }
        catch (toolbox::task::exception::Exception& e)
        {
            msg << "Failed to start albedo calculation workloop " << stdformat_exception_history (e);
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str() );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg.str(), e);
        }
    }
}

// subscribe to all input buses
void bril::bcm1fprocessor::Application::subscribeall()
{
    for (std::map<std::string, std::set<std::string> >::iterator bit = m_in_busTotopics.begin(); bit != m_in_busTotopics.end(); ++bit )
    {
        std::string busname = bit->first;

        for (std::set<std::string>::iterator topicit = bit->second.begin(); topicit != bit->second.end(); ++topicit)
        {
            if (m_verbose)
                LOG4CPLUS_INFO (getApplicationLogger(), "subscribing " + busname + ":" + *topicit);

            try
            {
                this->getEventingBus (busname).subscribe (*topicit);
            }
            catch (eventing::api::exception::Exception& e)
            {
                LOG4CPLUS_ERROR (getApplicationLogger(), "failed to subscribe, remove topic " + stdformat_exception_history (e) );
                m_in_busTotopics[busname].erase (*topicit);
            }
        }
    }
}

// Listens for reception of data on the eventing.
// If full statistics collected, call do_process on the already-accumulated nibbles.
void bril::bcm1fprocessor::Application::onMessage (toolbox::mem::Reference* ref, xdata::Properties& plist)
{
    std::string action = plist.getProperty ("urn:b2in-eventing:action");

    if (action == "notify")
    {
        std::string topic = plist.getProperty ("urn:b2in-eventing:topic");
        //LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), "Received data from "+topic); }
        std::string v = plist.getProperty ("DATA_VERSION");

        if (v.empty() )
        {
            std::string msg ("Received data message without version header, do nothing");
            LOG4CPLUS_ERROR (getApplicationLogger(), msg);

            if (ref != 0)
            {
                ref->release();
                ref = 0;
            }

            return;
        }

        std::stringstream ss;
        ss << "Received data from " + topic;
        LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );


        interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*) (ref->getDataLocation() );


        if ( topic == interface::bril::bcm1fhistT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", retrieving source histos channelid "
               << inheader->getChannelID() << " in (lastheader) nibble "
               << m_lastheader.nbnum << " (this histo has nibble)" << inheader->nbnum;
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

            retrieveSourceHistos (ref, inheader);
        }
        else if (topic == interface::bril::beamT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", making bunch masks";
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

            makeBunchMasks (inheader);
        }
        else if (topic == interface::bril::vdmflagT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", updating vdm flag";
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

            interface::bril::vdmflagT* me = (interface::bril::vdmflagT*)( ref->getDataLocation() );
            m_vdmflag = me->payload()[0];	  
            
        }
        else if (topic == interface::bril::NB4T::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", this is 4NB clock tick";
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );

        }
    }

    if (ref != 0)
    {
        ref->release();
        ref = 0;
    }
}

// When BCM1F data received on eventing, check and store it
void bril::bcm1fprocessor::Application::retrieveSourceHistos (toolbox::mem::Reference* ref, interface::bril::shared::DatumHead* inheader)
{
    int n_present_channels = 0;
    for (InDataCache::iterator histchannelit = m_bcm1fhistcache.begin(); histchannelit != m_bcm1fhistcache.end(); ++histchannelit)
    {
        if (histchannelit->first <= s_nBcm1fChannels && histchannelit->second->nbcount >= s_num_nb_to_collect)
            n_present_channels++;
    }

    if (inheader->nbnum != m_lastheader.nbnum && inheader->nbnum % s_num_nb_to_collect == 1 && n_present_channels >= s_minimum_present_channels)
    {
        do_process (m_lastheader);
        m_last_process_time = toolbox::TimeVal::gettimeofday();
        std::stringstream ss;
        ss << "processed last header: run " << m_lastheader.runnum
            << " ls " << m_lastheader.lsnum
            << " nb " << m_lastheader.nbnum
            << " (right now in run " << inheader->runnum
            << " ls " << inheader->lsnum
            << " nb " << inheader->nbnum
            << "). BG1: " << m_BG1_plus.value_
            << " BG2: " << m_BG2_minus.value_
            << " Si Lumi: " << m_lumi_avg
            << " Si Lumi raw: " << m_lumi_avg_raw;
        LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

        toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
        m_mon_timestamp = t;
        m_mon_fill = inheader->fillnum;
        m_mon_run = inheader->runnum;
        m_mon_ls = inheader->lsnum;
        m_mon_nb = inheader->nbnum;
        m_mon_infospace->fireItemGroupChanged ( m_mon_varlist, this );

        clear_cache();
    }
    else if ((toolbox::TimeVal::gettimeofday() - m_last_process_time).sec() >= s_cache_lifetime_sec)
    {
        m_last_process_time = toolbox::TimeVal::gettimeofday();
        clear_cache();
    }

    unsigned int channelId = inheader->getChannelID();

    std::map<unsigned int, InData*>::iterator channelit = m_bcm1fhistcache.find (channelId);

    // Now, irrespective of whether or not the last block was processed,
    // check if the current histogram's channelid is already in the list.
    // If not, insert this channelid and the histogram's contents
    if (channelit == m_bcm1fhistcache.end() )
    {
        toolbox::mem::Reference*  myref =  m_poolFactory->getFrame (m_memPool, ref->getDataSize() );
        memcpy (myref->getDataLocation(), ref->getDataLocation(), ref->getDataSize() );
        InData* d = new InData (1, myref);
        m_bcm1fhistcache.insert (std::make_pair (channelId, d) );
    }
    // If it's already there, accumulate this histogram's contents, too
    else
    {
        interface::bril::bcm1fhistT* indata = (interface::bril::bcm1fhistT*) (ref->getDataLocation() );
        hist_accumulateInChannel (channelit, indata);
    }

    m_lastheader = *inheader;
    // MOVE THIS INSIDE THE BCM1FHIST TOPIC CHECK, not relevant for other topics
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    unsigned int nowsec = t.sec();
    m_lastreceivedsec = nowsec;
}

// When BEAM data received on eventing, use it to make bunch
// masks for collision products and both beam backgrounds
void bril::bcm1fprocessor::Application::makeBunchMasks (interface::bril::shared::DatumHead* inheader)
{
    // DEFINE_COMPOUND_TOPIC(beam,"status:str28:1 egev:float:1 targetegev:uint16:1
    // bxconfig1:bool:3564 bxconfig2:bool:3564 intensity1:float:1 intensity2:float:1
    // bxintensity1:float:3564 bxintensity2:float:3564 machinemode:str20:1 fillscheme:str64:1"
    float _beamItot1;
    float _beamItot2;
    float _bx1[s_maxnbx];
    float _bx2[s_maxnbx];
    bool _con1[s_maxnbx];
    bool _con2[s_maxnbx];
    int lastCollBunch = -300;
    char _machinemode[50];
    char _beammode[50];
    interface::bril::shared::CompoundDataStreamer tc (interface::bril::beamT::payloaddict() );
    tc.extract_field (&_beamItot1, "intensity1",  inheader->payloadanchor);
    tc.extract_field (&_beamItot2, "intensity2",  inheader->payloadanchor);
    tc.extract_field (_bx1, "bxintensity1",  inheader->payloadanchor);
    tc.extract_field (_bx2, "bxintensity2",  inheader->payloadanchor);
    tc.extract_field (_con1, "bxconfig1",  inheader->payloadanchor);
    tc.extract_field (_con2, "bxconfig2",  inheader->payloadanchor);
    tc.extract_field (_machinemode, "machinemode",  inheader->payloadanchor);
    tc.extract_field (_beammode, "status",  inheader->payloadanchor);
    m_iTot1 = _beamItot1;
    m_iTot2 = _beamItot2;
    std::string machinemode = std::string (_machinemode);
    std::string beammode = std::string (_beammode);
    m_beammode = beammode;
    bool collidable = false;

    //bool nobeam = false;
    // use collision mask for lumi
    if ( beammode == "FLAT TOP" ||
            beammode == "SQUEEZE" ||
            beammode == "ADJUST" ||
            beammode == "STABLE BEAMS" ||
            beammode == "UNSTABLE BEAMS" ||
            beammode == "BEAM DUMP WARNING"
       )
        collidable = true;

    // don't use bunch masks for background, no beam with these modes
    /*
    if ( beammode == "SETUP" ||
         beammode == "BEAM DUMP" ||
         beammode == "RAMP DOWN" ||
         beammode == "NO BEAM" ||
         beammode == "ABORT" ||
         beammode == "RECOVERY" ||
         beammode == "CYCLING"
         )
      {
        nobeam = true;
      }
    **/
    std::stringstream ss;
    ss << "Mode: " << machinemode << " : " << beammode << std::endl;

    if (m_verbose)
        LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

    std::stringstream col;
    std::stringstream bk1;
    std::stringstream bk2;
    bk1 << "BKGD1 BX: ";
    bk2 << "BKGD2 BX: ";
    col << "COLL BX: ";

    int nbk1 = 0;
    int nbk2 = 0;
    int ncoll = 0;

    for (int bx = 0; bx < s_maxnbx; bx++)
    {
        m_iBunch1[bx] = _bx1[bx];
        m_iBunch2[bx] = _bx2[bx];
        // if ((bx % 100) == 0 && (bx < 1000))
        //       {
        //          ss.str("");
        //         ss << "Bunch " << bx << " beam 1: " << m_iBunch1[bx] << "  beam 2: " << m_iBunch2[bx] << std::endl;
        //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        m_collmask[bx] = m_bkgd1mask[bx] = m_bkgd2mask[bx] = m_bkgd1leadmask[bx] = m_bkgd2leadmask[bx] = m_bkgd1NCmask[bx] = m_bkgd2NCmask[bx] = false;
        int lastColl = bx - lastCollBunch;

        //      if((_con1[bx] &&(!collidable ||lastColl>30)) || nobeam)
        if (_con1[bx] && (!collidable || lastColl > 30) )
        {
            //ss.str("");
            //ss << "(1) Setting beam 1 mask to TRUE for bunch " << bx << std::endl;
            //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            m_bkgd1mask[bx] = true;
            m_bkgd1NCmask[bx] = true;
            nbk1++;
            bk1 << "," << bx;
        }

        //      if((_con2[bx] &&(!collidable || lastColl>30)) || nobeam)
        if (_con2[bx] && (!collidable || lastColl > 30) )
        {
            //ss.str("");
            //ss << "(1) Setting beam 2 mask to TRUE for bunch " << bx << std::endl;
            //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            m_bkgd2mask[bx] = true;
            m_bkgd2NCmask[bx] = true;
            nbk2++;
            bk2 << "," << bx;
        }

        if (_con1[bx] && _con2[bx])
        {
            m_collmask[bx] = true;
            m_bkgd1NCmask[bx] = m_bkgd2NCmask[bx] = false;
            if (lastColl > 30) m_bkgd1leadmask[bx] = m_bkgd2leadmask[bx] = true;
            lastCollBunch = bx;
            ncoll++;
            col << "," << bx;
        }
    }

    if (nbk1 < 200)
        ss << bk1.str() << std::endl;

    if (nbk2 < 200)
        ss << bk2.str() << std::endl;

    if (ncoll < 200)
        ss << col.str() << std::endl;

}


// Process the accumulated nibbles:
// Calls functions that create and queue for publishing the following products
//  * agghists
//  * background numbers
//  * luminosity numbers
void bril::bcm1fprocessor::Application::do_process (interface::bril::shared::DatumHead& inheader)
{
    LOG4CPLUS_DEBUG (getApplicationLogger(), "Now entered do_process: VERY BEGINNING");

    // if(inheader.runnum==0)
    //   {
    //     return;
    //   }

    std::stringstream ss;
    ss.str ("");
    ss << "do_process run " << inheader.runnum
       << " ls " << inheader.lsnum
       << " nb " << inheader.nbnum
       << " channel " << inheader.getChannelID();
    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );


    // 1. first make agghists, using same loop calculate total rates
    LOG4CPLUS_DEBUG (getApplicationLogger(), "About to makeAggHistsAndTotalRates");
    makeAggHistsAndTotalRates (inheader);


    // 3. channel mask needed for background and luminosity numbers
    LOG4CPLUS_DEBUG (getApplicationLogger(), "About to makeBkgNumbers");
    makeBkgNumbers (inheader);

    LOG4CPLUS_DEBUG (getApplicationLogger(), "About to makeLumiNumbers");
    makeLumiNumbers (inheader);
    //makebxhistnumbers(inheader);
    makeCollidingRates (inheader);

}

// Loops through RHU histograms, makes the agghists for each channel
// and at the same time creates total rates for each channel for
// (monitoring purposes and) making the channel mask.
// Queues the agghists for publishing
void bril::bcm1fprocessor::Application::makeAggHistsAndTotalRates (interface::bril::shared::DatumHead& inheader)
{

    // first for aggregated histogram

    toolbox::mem::Reference* bcm1fagghist = 0;
    //  toolbox::mem::Reference* bcm1fagghist2 = 0; // for lumi bin only
    toolbox::mem::Reference* bxconfig = 0;

    //  memset (m_totalChannelRate,0.,sizeof(m_totalChannelRate));
    //std::fill(begin(m_totalChannelRate), end(m_totalChannelRate), 0.);
    //  std::fill(m_totalChannelRate.begin(), m_totalChannelRate.end(), 0.);
    m_totalChannelRate.resize (s_nAllChannels, 0.);


    try
    {
        LOG4CPLUS_DEBUG (getApplicationLogger(), "entered try loop agghist");

        // PICKING OUT THE AGGHISTS FROM BPTX
        // AND PUBLISHING THEM IN THEIR OWN TOPIC
        // ***CHANNELS HARDCODED***
        // BPTX RHU Channel listing from Hans' e-mail of Apr. 20, 2016
        // 0    B1
        // 1    B2
        // 2    OR
        // 3    AND
        // 4    B1 AND not(B2)
        // 5    not(B1) AND B2
        // 6    BeamGas B1
        // 7    BeamGas B2

        bool beam1bptx[3564];
        bool beam2bptx[3564];

        memset (beam1bptx, false, sizeof (beam1bptx) );
        memset (beam2bptx, false, sizeof (beam2bptx) );

        // MAKE BPTX OUTPUT PRODUCT
        LOG4CPLUS_DEBUG (getApplicationLogger(), "Making BPTX output product");
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::bxconfigT::payloaddict();
        xdata::Properties plist;
        plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
        plist.setProperty ("PAYLOAD_DICT", pdict);
        //size_t totalsize = interface::bril::bxconfigT::maxsize();
        // END COMPOUND-SPECIFIC STUFF

        bxconfig = m_poolFactory->getFrame (m_memPool, interface::bril::bxconfigT::maxsize() );
        bxconfig->setDataSize (interface::bril::bxconfigT::maxsize() );
        interface::bril::bxconfigT* bxconfighist = (interface::bril::bxconfigT*) (bxconfig->getDataLocation() );

        // fill, run, ls, ln, sec, msec
        bxconfighist->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
        bxconfighist->setFrequency (4);

        // sourceid, algo, channel, payloadtype
        bxconfighist->setResource (interface::bril::shared::DataSource::BCM1F, 0, 0, interface::bril::shared::StorageType::COMPOUND);
        bxconfighist->setTotalsize (interface::bril::bxconfigT::maxsize() );
        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::shared::CompoundDataStreamer tc (pdict);
        // END MORE COMPOUND-SPECIFIC STUFF


        // MAKE AGGHIST OUTPUT PRODUCTS AND FILL THEM AND BPTX OUTPUT PRODUCT (loop over all channels)
        for (InDataCache::iterator channelit = m_bcm1fhistcache.begin(); channelit != m_bcm1fhistcache.end(); ++channelit)
        {
            LOG4CPLUS_DEBUG (getApplicationLogger(), "Accessing this channel of histcache");
            unsigned int channelid = channelit->first;
            std::stringstream ss;
            ss.str ("");
            ss << "Channelid is " << channelid;
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );



            unsigned int channelIndex = channelid - 1;

            //get the TP mask for this particular channel
            unsigned int tp_mask_start = 0;
            unsigned int tp_mask_end = 0;
            this->get_RHU_tp_mask(channelIndex, tp_mask_start, tp_mask_end);

            bcm1fagghist = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1fagghistT::maxsize() );
            bcm1fagghist->setDataSize (interface::bril::bcm1fagghistT::maxsize() );
            interface::bril::bcm1fagghistT* agghist = (interface::bril::bcm1fagghistT*) (bcm1fagghist->getDataLocation() );

            // fill, run, ls, ln, sec, msec
            agghist->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
            agghist->setFrequency (4);
            // sourceid, algo, channel, payloadtype
            agghist->setResource (interface::bril::shared::DataSource::BCM1F, interface::bril::BCM1FAggAlgos::SUM, channelid, interface::bril::shared::StorageType::UINT16);
            agghist->setTotalsize (interface::bril::bcm1fagghistT::maxsize() );

            //initialize array
            for (size_t bin = 0; bin < interface::bril::bcm1fagghistT::n(); ++bin)
                agghist->payload() [bin] = 0.;

            toolbox::mem::Reference* histref = channelit->second->dataref;
            interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*) (histref->getDataLocation() );

            // TODO: DO REAL PROCESSING HERE
            // each output bin should sum the corresponding four input bins
            float bx4sum = 0.;
            size_t nbins = histdataptr->n();

            float nibbleNorm = (4. / channelit->second->nbcount);

            //         if (channelid > 48)
            //           {
            // for (unsigned int bunch = 0; bunch < 3564; bunch++)
            //   {
            //     if (m_bkgd1mask[bunch] == true)
            //       {
            //      std::stringstream ss;
            //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " before fill) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
            //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            //       }
            //     if (m_bkgd2mask[bunch] == true)
            //       {
            //      std::stringstream ss;
            //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " before fill) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
            //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            //       }
            //   }
            //           }

            float rateTemp = 0.;
            float bx0bin0 = 0.;

            for (size_t rhubin = 0; rhubin < nbins; ++rhubin )
            {
                // Mask out test pulse bins, but only for channels 1-48 -- although this is in the abort gap anyway
                // Now also need to mask out test pulses for LUT RHU
                //if (!(rhubin >= 14136 && rhubin < 14196) || (channelid >= 49 && channelid <= 56 ))
                //if (!(rhubin >= 14136 && rhubin < 14220) || (channelid >= 49 && channelid <= 56 )) // Hans changed some timing of test pulse, don't know exactly where it is now, other than it comes later
                //if (!(rhubin >= 14136 && rhubin < 14230) || (channelid >= 49 && channelid <= 56 )) // Again 26May16, for some reason??!
                if (! (rhubin >= tp_mask_start && rhubin < tp_mask_end) || (channelid >= 49 && channelid <= 56 ) ) // 03May17, for making bins uniform size
                {
                    bx4sum += histdataptr->payload() [rhubin];
                    //m_totalChannelRate[channelIndex] += nibbleNorm*histdataptr->payload()[rhubin];
                    //m_totalChannelRate.elementAt(channelIndex) += nibbleNorm*histdataptr->payload()[rhubin];
                    rateTemp += nibbleNorm * histdataptr->payload() [rhubin];

                    if ( (rhubin + 1) % 4 == 0) // e.g. we want bins 0,1,2,3 together, so we sum after bin 3, when nbins+1 = 4 and 4%4=0.
                    {
                        unsigned int bxidx = rhubin / 4;

                        if (channelid == 48 + 0 && bx4sum > 4) // B1 is channel 0
                        {
                            beam1bptx[bxidx] = true;
                            // if (bxidx < 100)
                            //   std::cout << "BPTX 1 filled bunch at bx " << bxidx << std::endl;
                        }

                        if (channelid == 48 + 1 && bx4sum > 4) // B2 is channel 1
                        {
                            beam2bptx[bxidx] = true;
                            // if (bxidx < 100)
                            //   std::cout << "BPTX 2 filled bunch at bx " << bxidx << std::endl;
                        }
                    }


                    if (rhubin == 0) // first RHU bin
                    {
                        bx0bin0 = bx4sum;
                        bx4sum = 0.;
                    }
                    else if  (rhubin == (nbins - 1) ) // last RHU bin
                    {
                        bx4sum += bx0bin0;  //add first RHU bin to last BX (cosmetics)
                        unsigned int bxidx = rhubin / 4;
                        agghist->payload() [bxidx] += nibbleNorm * bx4sum; //accumulate 4 nibbles
                        bx4sum = 0.;
                        bx0bin0 = 0.; //probably not necessary

                    }
                    else if ( (rhubin) % 4 == 0) // e.g. we want bins 0,1,2,3 together, so we sum after bin 3, when nbins+1 = 4 and 4%4=0.
                    {
                        unsigned int bxidx = (rhubin / 4) - 1; // summing ends already inthe next BX, so we subtract 1, the case of 0/4-1 is protect from in if above
                        agghist->payload() [bxidx] += nibbleNorm * bx4sum; //accumulate 4 nibbles
                        bx4sum = 0.;
                    }

                    // if (rhubin % 4 == 2) // TAKE BIN 2 FOR COLLISIONS! (out of 0,1,2,3)
                    //   {
                    //     unsigned int bxidx = rhubin/4;
                    //     //agghist->payload()[bxidx] = histdataptr->payload()[rhubin]; //accumulate 4 nibbles
                    //     //agghist->payload()[bxidx] += bx4sum; //accumulate 4 nibbles
                    //     if (channelid == 48+2 && bx4sum > 4)
                    //       {
                    //         beam1bptx[bxidx] = true;
                    //       }
                    //     if (channelid == 48+3 && bx4sum > 4)
                    //       {
                    //         beam2bptx[bxidx] = true;
                    //       }
                    //     bx4sum = 0.;
                    //   }
                }
            }

            //((xdata::Float*) m_totalChannelRate.elementAt(channelIndex)).setValue(rateTemp);
            // xdata::Float* channelRateTemp = (xdata::Float*) m_totalChannelRate.elementAt(channelIndex);
            // channelRateTemp->setValue(rateTemp);
            //m_totalChannelRate.elementAt(channelIndex) = rateTemp;
            m_totalChannelRate[channelIndex] = rateTemp;

            LOG4CPLUS_DEBUG (getApplicationLogger(), "Pushing this channel's agghists to output queue");

            //m_topicoutqueues[interface::bril::bcm1fagghistT::topicname()]->push(bcm1fagghist);
            //if (m_sensorConnected[channelIndex] & channelIndex < 48) // normal running
            if (channelIndex < 48)
                //if(channelIndex < 48) // VdM only
                m_topicoutqueues[interface::bril::bcm1fagghistT::topicname()]->push (bcm1fagghist);
            else if (bcm1fagghist)
            {
                bcm1fagghist->release();
                bcm1fagghist = 0;
            }


            //         if (channelid > 48)
            //           {
            // for (unsigned int bunch = 0; bunch < 3564; bunch++)
            //   {
            //     if (m_bkgd1mask[bunch] == true)
            //       {
            //      std::stringstream ss;
            //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " after fill) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
            //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            //       }
            //     if (m_bkgd2mask[bunch] == true)
            //       {
            //      std::stringstream ss;
            //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " after fill) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
            //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            //       }
            //   }
            //           }


        }

        // for (unsigned int bunch = 0; bunch < 3564; bunch++)
        //   {
        //     if (m_bkgd1mask[bunch] == true)
        //       {
        //          std::stringstream ss;
        //         ss << "(makeAgghistsAndTotalRates 3) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
        //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        //     if (m_bkgd2mask[bunch] == true)
        //       {
        //          std::stringstream ss;
        //         ss << "(makeAgghistsAndTotalRates 3) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
        //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        //   }

        LOG4CPLUS_DEBUG (getApplicationLogger(), "Done pushing agghists");

        // PUSH BPTX PRODUCT, TOO

        // DEBUUUUUUG
        // std::cout << "BPTX beam 1 bunch pattern before bunch 100" << std::endl;
        // for (unsigned int bunchindex = 0; bunchindex < 100; bunchindex++)
        //       {
        //         std::cout << beam1bptx[bunchindex] << ", ";
        //         if ((bunchindex+1) % 20 == 0)
        //           {
        //             std::cout << std::endl;
        //           }
        //       }
        // std::cout << "BPTX beam 2 bunch pattern before bunch 100" << std::endl;
        // for (unsigned int bunchindex = 0; bunchindex < 100; bunchindex++)
        //       {
        //         std::cout << beam2bptx[bunchindex] << ", ";
        //         if ((bunchindex+1) % 20 == 0)
        //           {
        //             std::cout << std::endl;
        //           }
        //       }

        // MORE COMPOUND-SPECIFIC STUFF
        LOG4CPLUS_DEBUG (getApplicationLogger(), "Put beam1 into topic");
        tc.insert_field ( bxconfighist->payloadanchor, "beam1", beam1bptx);
        LOG4CPLUS_DEBUG (getApplicationLogger(), "Put beam2 into topic");
        tc.insert_field ( bxconfighist->payloadanchor, "beam2", beam2bptx);
        LOG4CPLUS_DEBUG (getApplicationLogger(), "Put datasource into topic");
        const unsigned short mydatasource = interface::bril::bxconfig_bptx_rhu;
        tc.insert_field ( bxconfighist->payloadanchor, "datasource", &mydatasource);
        // END MORE COMPOUND-SPECIFIC STUFF
        LOG4CPLUS_DEBUG (getApplicationLogger(), "Push topic to output queue");
        m_topicoutqueues[interface::bril::bxconfigT::topicname()]->push (bxconfig);
        LOG4CPLUS_DEBUG (getApplicationLogger(), "Done pushing bptx");

    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to process data for bcm1fagghist / bxconfig");
        LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );

        if (bcm1fagghist)
        {
            bcm1fagghist->release();
            bcm1fagghist = 0;
        }

        if (bxconfig)
        {
            bxconfig->release();
            bxconfig = 0;
        }
    }

    // THIS IS THE STANDARD ONE
    std::stringstream ss;
    ss << "Total channel rates (excl. TP):" << std::flush;

    for (int channel = 0; channel < 48; channel++)
    {
        //ss << " " << m_totalChannelRate[channel];
        xdata::Float* xdatafloattemp = (xdata::Float*) m_totalChannelRate.elementAt (channel);
        //ss << " " << ((xdata::Float*) m_totalChannelRate.elementAt(channel)).toString();
        float floattemp = xdatafloattemp->value_;
        //ss << " " << xdatafloattemp->toString();
        ss << " " << floattemp;
    }

    //ss << std::endl;
    if (m_verbose)
        LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

    // ss.str("");
    // ss << "Total channel rates (excl. TP) normalized to typical rate:" << std::flush;
    // for (int channel = 0; channel < 48; channel++)
    //   {
    //  //     ss << " " << (m_totalChannelRate[channel]/m_typicalChannelCounts[channel]);
    //     ss << " " << (m_totalChannelRate.elementAt(channel)/m_typicalChannelCounts[channel]);
    //   }
    // if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

    // for (unsigned int bunch = 0; bunch < 3564; bunch++)
    //   {
    //     if (m_bkgd1mask[bunch] == true)
    //       {
    //      ss.str("");
    //         ss << "(makeAgghistsAndTotalRates 4) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
    //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    //       }
    //     if (m_bkgd2mask[bunch] == true)
    //       {
    //      ss.str("");
    //         ss << "(makeAgghistsAndTotalRates 4) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
    //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    //       }
    //   }

}

// Calculates beam 1 and beam 2 background numbers,
// queues them for publishing
void bril::bcm1fprocessor::Application::makeBkgNumbers (interface::bril::shared::DatumHead& inheader)
{

    // now for BG numbers

    toolbox::mem::Reference* bcm1fbkg = 0;
    //  toolbox::mem::Reference* bcm1fbkghistos = 0;

    // Each BG number should (initial hack) sum the total of the + and - end histograms, respectively
    LOG4CPLUS_DEBUG (getApplicationLogger(), "Still in do_process: bkgsums");

    try
    {
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::bcm1fbkgT::payloaddict();
        xdata::Properties plist;
        plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
        plist.setProperty ("PAYLOAD_DICT", pdict);
        //size_t totalsize = interface::bril::bcm1fbkgT::maxsize();
        // END COMPOUND-SPECIFIC STUFF

        bcm1fbkg = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1fbkgT::maxsize() );
        bcm1fbkg->setDataSize (interface::bril::bcm1fbkgT::maxsize() );
        interface::bril::bcm1fbkgT* bkghist = (interface::bril::bcm1fbkgT*) (bcm1fbkg->getDataLocation() );

        // fill, run, ls, ln, sec, msec
        bkghist->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
        bkghist->setFrequency (4);

        // sourceid, algo, channel, payloadtype
        bkghist->setResource (interface::bril::shared::DataSource::BCM1F, 0, 0, interface::bril::shared::StorageType::COMPOUND);
        bkghist->setTotalsize (interface::bril::bcm1fbkgT::maxsize() );

        // // FOR COMPOUND DATA TYPE
        // std::string pdicthist = interface::bril::bcm1fbkghistosT::payloaddict();
        // xdata::Properties plisthist;
        // plisthist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
        // plisthist.setProperty("PAYLOAD_DICT",pdicthist);
        // //size_t totalsize = interface::bril::bcm1fbkghistosT::maxsize();
        // // END COMPOUND-SPECIFIC STUFF

        // bcm1fbkghistos = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fbkghistosT::maxsize());
        // bcm1fbkghistos->setDataSize(interface::bril::bcm1fbkghistosT::maxsize());
        // interface::bril::bcm1fbkghistosT* bkgbxhist = (interface::bril::bcm1fbkghistosT*)(bcm1fbkghistos->getDataLocation());

        // // fill, run, ls, ln, sec, msec
        // bkgbxhist->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        // bkgbxhist->setFrequency(64);

        // // sourceid, algo, channel, payloadtype
        // bkgbxhist->setResource(interface::bril::DataSource::BCM1F,NULL,NULL,interface::bril::StorageType::COMPOUND);
        // bkgbxhist->setTotalsize(interface::bril::bcm1fbkghistosT::maxsize());

        //for(size_t i = 0; i < interface::bril::bcm1fbkgT::n(); ++i){bkghist->payload()[i] = 0.;}//initialize array

        float bkg1sum[3] = {0.}; // 0 for standard, 1 for lead, 2 for NC
        float bkg2sum[3] = {0.};

        float bunchCurrent1Sum[3] = {0.};
        float bunchCurrent2Sum[3] = {0.};


        float nActiveChannels1 = 0.;
        float nActiveChannels2 = 0.;

        float totActiveArea1 = 0.;
        float totActiveArea2 = 0.;

        // for (unsigned int bunch = 0; bunch < 3564; bunch++)
        //       {
        //         if (m_bkgd1mask[bunch] == true)
        //           {
        //              std::stringstream ss;
        //             ss << "(makeBkgNumbers) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
        //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //           }
        //         if (m_bkgd2mask[bunch] == true)
        //           {
        //              std::stringstream ss;
        //             ss << "(makeBkgNumbers) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
        //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //           }
        //       }

        for (InDataCache::iterator channelit = m_bcm1fhistcache.begin(); channelit != m_bcm1fhistcache.end(); ++channelit)
        {
            toolbox::mem::Reference* histref = channelit->second->dataref;
            interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*) (histref->getDataLocation() );
            float nNibbles = (float) channelit->second->nbcount;
            // TODO: DO REAL PROCESSING HERE
            //        // each output bin should sum the corresponding four input bins
            // each of m_BG1_plus and m_BG2_minus should sum together the full histograms
            // of the 24 channels on each end, over this time period, normalizing by
            // number of actually-picked-up histograms to account for missed data nibbles
            // Determine which end it's on by the channelId: 0-23 for plus, 24-47 for minus
            size_t nbins = histdataptr->n();
            unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1

            //get the TP mask for this particular channel
            unsigned int channelIndex = channelId-1; //counts from 0
            unsigned int tp_mask_start = 0;
            unsigned int tp_mask_end = 0;
            this->get_RHU_tp_mask(channelIndex, tp_mask_start, tp_mask_end);

            // BKGD 1
            if (channelId >= 1 && channelId <= 24)
            {
                bool useChannel = false;

                if (m_useChannelForBkgd[channelId - 1] && m_channel_spike_mask[channelId-1])
                {
                    useChannel = true;
                    totActiveArea1 = totActiveArea1 + s_areaSilicon * m_channelScaleFactor[channelId - 1];
                    nActiveChannels1++;

                    // loop over RHU bins
                    if (useChannel)
                    {
                        for (size_t rhubin = 0; rhubin < nbins; ++rhubin )
                        {
                            // Mask out test pulse bins and require filled bunch
                            //if (!(rhubin >= 14136 && rhubin < 14196) && m_bkgd1mask[rhubin/4])
                            if (! (rhubin >= tp_mask_start && rhubin < tp_mask_end) && m_bkgd1mask[rhubin / 4])
                            {
                                if (rhubin % 4 == 0) // TAKE BIN 0 FOR BACKGROUND! (out of 0,1,2,3)
                                {
                                    // Find the albedo level from the previous 20 bins, average
                                    // then subtract from the background bin
                                    float albedo = 0;
                                    float nAlbedoBins = 0.;

                                    for (int albedoBinIndex = 0; albedoBinIndex < 20; albedoBinIndex++)
                                    {
                                        int albedoBin = rhubin - albedoBinIndex - 1;

                                        if (albedoBin < 0)
                                            albedoBin = albedoBin + 14256;

                                        if (albedoBin % 2 == 1)
                                        {
                                            albedo = albedo + (float (histdataptr->payload() [albedoBin]) ) / nNibbles;
                                            nAlbedoBins++;
                                        }
                                    }

                                    //albedo = albedo / 20.;
                                    albedo = albedo / nAlbedoBins;
                                    float toAdd = (float (histdataptr->payload() [rhubin]) / m_channelScaleFactor[channelId - 1]) / nNibbles;
                                    // if (toAdd > 0.)
                                    //       {
                                    //          ss.str("");
                                    //         ss << "BG1 channelid " << channelId << " rhubin "
                                    //                 << rhubin << ": toAdd > 0.: " << toAdd
                                    //                 << " albedo " << albedo;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                    // if (m_iBunch1[rhubin/4] > 0.)
                                    //       {
                                    //         toAdd = toAdd/m_iBunch1[rhubin/4]*1e11;
                                    //       }
                                    toAdd = toAdd - albedo;

                                    if (toAdd < 0.)
                                    {
                                        toAdd = 0;
                                        m_countAlbedoHigherThanBackground++;
                                    }

                                    // if (toAdd > 0.)
                                    //       {
                                    //          ss.str("")
                                    //         ss << "BG1 channelid " << channelId << " rhubin "
                                    //                 << rhubin << ": toAdd > 0.: " << toAdd << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                    bkg1sum[0] += toAdd;
                                    if ( m_bkgd1leadmask[rhubin / 4]) bkg1sum[1] += toAdd;
                                    if ( m_bkgd1NCmask[rhubin / 4]) bkg1sum[2] += toAdd;
                                    //bunchCurrent1Sum += m_iBunch1[rhubin/4]/1e11;
                                    // if (m_iBunch1[rhubin/4] > 0.)
                                    //       {
                                    //          ss.str("");
                                    //         ss << "Beam 1 current in bunch " << rhubin/4 << " is " << m_iBunch1[rhubin/4] << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                    // if (toAdd > 0.)
                                    //       {
                                    //          ss.str("");
                                    //         ss << "BG1 channelid " << channelId << " rhubin "
                                    //                 << rhubin << ": bkg1sum " << bkg1sum
                                    //                 << " bunchCurrent1Sum " << bunchCurrent1Sum << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                    //bkg1sum += (float(histdataptr->payload()[rhubin]))/nNibbles;
                                }
                            }
                        }
                    }
                }
            }
            // BKGD 2
            else if (channelId >= 25 && channelId <= 48)
            {
                bool useChannel = false;

                //if (channelId != 25 && channelId != 26)
                if (m_useChannelForBkgd[channelId - 1] && m_channel_spike_mask[channelId-1])
                {
                    useChannel = true;
                    totActiveArea2 = totActiveArea2 + s_areaSilicon * m_channelScaleFactor[channelId - 1];
                    nActiveChannels2++;

                    // loop over RHU bins
                    if (useChannel)
                    {
                        for (size_t rhubin = 0; rhubin < nbins; ++rhubin )
                        {
                            // Mask out test pulse bins and require filled bunch
                            //if (!(rhubin >= 14136 && rhubin < 14196) && m_bkgd2mask[rhubin/4])
                            if (! (rhubin >= tp_mask_start && rhubin < tp_mask_end) && m_bkgd2mask[rhubin / 4])
                            {
                                if (rhubin % 4 == 0) // TAKE BIN 0 FOR BACKGROUND! (out of 0,1,2,3)
                                {
                                    // Find the albedo level from the previous 20 bins, average
                                    // then subtract from the background bin
                                    float albedo = 0;
                                    float nAlbedoBins = 0.;

                                    for (int albedoBinIndex = 0; albedoBinIndex < 20; albedoBinIndex++)
                                    {
                                        int albedoBin = rhubin - albedoBinIndex - 1;

                                        if (albedoBin < 0)
                                            albedoBin = albedoBin + 14256;

                                        if (albedoBin % 2 == 1)
                                        {
                                            albedo = albedo + (float (histdataptr->payload() [albedoBin]) ) / nNibbles;
                                            nAlbedoBins++;
                                        }
                                    }

                                    //albedo = albedo / 20.;
                                    albedo = albedo / nAlbedoBins;
                                    float toAdd = (float (histdataptr->payload() [rhubin]) / m_channelScaleFactor[channelId - 1]) / nNibbles;
                                    // if (toAdd > 0.)
                                    //       {
                                    //          ss.str("");
                                    //         ss << "BG2 channelid " << channelId << " rhubin "
                                    //                 << rhubin << ": toAdd > 0.: " << toAdd
                                    //                 << " albedo " << albedo << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                    // if (m_iBunch2[rhubin/4] > 0.)
                                    //       {
                                    //         toAdd = toAdd/m_iBunch2[rhubin/4]*1e11;
                                    //       }
                                    toAdd = toAdd - albedo;

                                    if (toAdd < 0.)
                                    {
                                        toAdd = 0;
                                        m_countAlbedoHigherThanBackground++;
                                    }

                                    // if (toAdd > 0.)
                                    //       {
                                    //          ss.str("");
                                    //         ss << "BG2 channelid " << channelId << " rhubin "
                                    //                 << rhubin << ": toAdd > 0.: " << toAdd << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                    bkg2sum[0] += toAdd;
                                    if ( m_bkgd2leadmask[rhubin / 4]) bkg2sum[1] += toAdd;
                                    if ( m_bkgd2NCmask[rhubin / 4]) bkg2sum[2] += toAdd;
                                    //bunchCurrent2Sum += m_iBunch2[rhubin/4]/1e11;
                                    // if (m_iBunch2[rhubin/4] > 0.)
                                    //   {
                                    //          ss.str("");
                                    //         ss << "Beam 2 current in bunch " << rhubin/4 << " is " << m_iBunch2[rhubin/4] << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //   }
                                    //bkg2sum += (float(histdataptr->payload()[rhubin]))/nNibbles;
                                    // if (toAdd > 0.)
                                    //       {
                                    //          ss.str(""):
                                    //         ss << "BG2 channelid " << channelId << " rhubin "
                                    //                 << rhubin << ": bkg2sum " << bkg2sum
                                    //                 << " bunchCurrent2Sum " << bunchCurrent2Sum << std::endl;
                                    //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                                    //       }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Each BG number should (initial hack) sum the total of the + and - end histograms, respectively
        // make m_BG1_plus and m_BG2_minus here
        // RIGHT NOW THEY ARE NORMALIZED TO ONE NIBBLE! (to account for different numbers of nibbles from different boards)
        // Okay, *NOW* I am fully accounting for them to be normalized to the number of nibbles that have been accumulated

        // Now normalization to Hz/cm^2
        // 1NB -> 1s: 1 LN = ~0.3 s = 4096 orbits * 3564 bxs/orbit * 25 ns/bx = 364953600 ns = 0.365 s
        // hits/cm^2 = Total hits / nActiveSensors / sensor surface area = total hits / nActiveSensors / (0.5 cm)^2

        // for (unsigned int bunch = 0; bunch < 3564; bunch++)
        //       {
        //         if (m_bkgd1mask[bunch] == true)
        //           {
        //              ss.str("");
        //             ss << "(3) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
        //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //           }
        //         if (m_bkgd2mask[bunch] == true)
        //           {
        //              ss.str("");
        //             ss << "(3) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
        //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //           }
        //       }

        for (unsigned int bunch = 0; bunch < 3564; bunch++)
        {
            if (m_bkgd1mask[bunch])
            {
                // ss.str("");
                // ss << "Beam 1 bunch " << bunch << " adding current " << m_iBunch1[bunch] << std::endl;
                // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                bunchCurrent1Sum[0] = bunchCurrent1Sum[0] + m_iBunch1[bunch] / 1e11;
            }

            if (m_bkgd2mask[bunch])
            {
                // ss.str("");
                // ss << "Beam 2 bunch " << bunch << " adding current " << m_iBunch2[bunch] << std::endl;
                // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                bunchCurrent2Sum[0] = bunchCurrent2Sum[0] + m_iBunch2[bunch] / 1e11;
            }
            if (m_bkgd1leadmask[bunch])
            {
                // ss.str("");
                // ss << "Beam 1 bunch " << bunch << " adding current " << m_iBunch1[bunch] << std::endl;
                // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                bunchCurrent1Sum[1] = bunchCurrent1Sum[1] + m_iBunch1[bunch] / 1e11;
            }

            if (m_bkgd2leadmask[bunch])
            {
                // ss.str("");
                // ss << "Beam 2 bunch " << bunch << " adding current " << m_iBunch2[bunch] << std::endl;
                // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                bunchCurrent2Sum[1] = bunchCurrent2Sum[1] + m_iBunch2[bunch] / 1e11;
            }
            
            if (m_bkgd1NCmask[bunch])
            {
                // ss.str("");
                // ss << "Beam 1 bunch " << bunch << " adding current " << m_iBunch1[bunch] << std::endl;
                // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                bunchCurrent1Sum[2] = bunchCurrent1Sum[2] + m_iBunch1[bunch] / 1e11;
            }

            if (m_bkgd2NCmask[bunch])
            {
                // ss.str("");
                // ss << "Beam 2 bunch " << bunch << " adding current " << m_iBunch2[bunch] << std::endl;
                // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                bunchCurrent2Sum[2] = bunchCurrent2Sum[2] + m_iBunch2[bunch] / 1e11;
            }
        }

        //ss.str("");
        std::stringstream ss;
        ss << "BKGD numbers pre-normalization: 1) " << bkg1sum << "  2) " << bkg2sum << std::endl;
        LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
        
        for (unsigned int i=0; i<3;i++)
        {
            bkg1sum[i] = bkg1sum[i] / s_sec_per_nibble / totActiveArea1;
            bkg2sum[i] = bkg2sum[i] / s_sec_per_nibble / totActiveArea2;

            if (bunchCurrent1Sum[i] > 0.01) // units of 10^11 protons
            {
                bkg1sum[i] = bkg1sum[i] / bunchCurrent1Sum[i];
                if (i==0)
                {
                    ss.str ("");
                    ss << "bunchCurrent1Sum is " << bunchCurrent1Sum[i] << " (in units of 1e11 protons)" << std::endl;
                    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
                }
            }
            else
            {
                bkg1sum[i] = 0.;
                if (i==0)
                {
                    ss.str ("");
                    ss << "bunchCurrent1Sum is " << bunchCurrent1Sum[i] << ", IGNORED (<0.01e11)" << std::endl;
                    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
                }
            }

            if (bunchCurrent2Sum[i] > 0.01) // units of 10^11 protons
            {
                bkg2sum[i] = bkg2sum[i] / bunchCurrent2Sum[i];
                if (i==0)
                {
                    ss.str ("");
                    ss << "bunchCurrent2Sum is " << bunchCurrent2Sum[i] <<  " (in units of 1e11 protons)" << std::endl;
                    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
                }
            }
            else
            {
                bkg2sum[i] = 0.;
                if (i==0)
                {
                    ss.str ("");
                    ss << "bunchCurrent2Sum is " << bunchCurrent2Sum[i] << ", IGNORED (<0.01e11)" << std::endl;
                    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
                }
            }
        }

        // // for doing the current normazliation for all bunches at once
        // if(m_iTot1>1e11)
        //       {
        //         bkg1sum=bkg1sum/m_iTot1*1e11;
        //       }
        // if(m_iTot2>1e11)
        //       {
        //         bkg2sum=bkg2sum/m_iTot2*1e11;
        //       }

        //filtering of the incidental spikes in the bkg
        m_last4_bkg1[3] = m_last4_bkg1[2];
        m_last4_bkg1[2] = m_last4_bkg1[1];
        m_last4_bkg1[1] = m_last4_bkg1[0];
        m_last4_bkg1[0] = bkg1sum[0];

        m_last4_bkg1_lead[3] = m_last4_bkg1_lead[2];
        m_last4_bkg1_lead[2] = m_last4_bkg1_lead[1];
        m_last4_bkg1_lead[1] = m_last4_bkg1_lead[0];
        m_last4_bkg1_lead[0] = bkg1sum[1];

        m_last4_bkg1_nc[3] = m_last4_bkg1_nc[2];
        m_last4_bkg1_nc[2] = m_last4_bkg1_nc[1];
        m_last4_bkg1_nc[1] = m_last4_bkg1_nc[0];
        m_last4_bkg1_nc[0] = bkg1sum[2];

        if (bunchCurrent1Sum[0] > 0.5)
            m_BG1_plus = m_last4_bkg1[0];
        else
            m_BG1_plus = (m_last4_bkg1[0] + m_last4_bkg1[1] + m_last4_bkg1[2] + m_last4_bkg1[3]) / 4;

        if (bunchCurrent1Sum[1] > 0.5)
            m_BG1_lead = m_last4_bkg1_lead[0];
        else
            m_BG1_lead = (m_last4_bkg1_lead[0] + m_last4_bkg1_lead[1] + m_last4_bkg1_lead[2] + m_last4_bkg1_lead[3]) / 4;

        if (bunchCurrent1Sum[2] > 0.5)
            m_BG1_nc = m_last4_bkg1_nc[0];
        else
            m_BG1_nc = (m_last4_bkg1_nc[0] + m_last4_bkg1_nc[1] + m_last4_bkg1_nc[2] + m_last4_bkg1_nc[3]) / 4;

        m_last4_bkg2[3] = m_last4_bkg2[2];
        m_last4_bkg2[2] = m_last4_bkg2[1];
        m_last4_bkg2[1] = m_last4_bkg2[0];
        m_last4_bkg2[0] = bkg2sum[0];

        m_last4_bkg2_lead[3] = m_last4_bkg2_lead[2];
        m_last4_bkg2_lead[2] = m_last4_bkg2_lead[1];
        m_last4_bkg2_lead[1] = m_last4_bkg2_lead[0];
        m_last4_bkg2_lead[0] = bkg2sum[1];

        m_last4_bkg2_nc[3] = m_last4_bkg2_nc[2];
        m_last4_bkg2_nc[2] = m_last4_bkg2_nc[1];
        m_last4_bkg2_nc[1] = m_last4_bkg2_nc[0];
        m_last4_bkg2_nc[0] = bkg2sum[2];

        if (bunchCurrent2Sum[0] > 0.5)
            m_BG2_minus = m_last4_bkg2[0];
        else
            m_BG2_minus = (m_last4_bkg2[0] + m_last4_bkg2[1] + m_last4_bkg2[2] + m_last4_bkg2[3]) / 4;

        if (bunchCurrent2Sum[1] > 0.5)
            m_BG2_lead = m_last4_bkg2_lead[0];
        else
            m_BG2_lead = (m_last4_bkg2_lead[0] + m_last4_bkg2_lead[1] + m_last4_bkg2_lead[2] + m_last4_bkg2_lead[3]) / 4;

        if (bunchCurrent2Sum[2] > 0.5)
            m_BG2_nc = m_last4_bkg2_nc[0];
        else
            m_BG2_nc = (m_last4_bkg2_nc[0] + m_last4_bkg2_nc[1] + m_last4_bkg2_nc[2] + m_last4_bkg2_nc[3]) / 4;

        //std::stringstream ss;
        ss.str ("");
        ss << "BG1 (plus): " << bkg1sum[0] << "  BG2 (minus): " << bkg2sum[0]
           << " BG1 LEAD (plus): " << bkg1sum[1] << "  BG2 LEAD (minus): " << bkg2sum[1]
           << " BG1 NC (plus): " << bkg1sum[2] << "  BG2 NC (minus): " << bkg2sum[2]
           << " with " << nActiveChannels1 << " active channels for beam 1 "
           << " and " << nActiveChannels2 << " active channels for beam 2 ";

        //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        if (m_verbose)
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

        //std::cout << ss.str() << std::endl;

        if (bkg1sum[0] > 20)
        {
            ss.str ("");
            ss << "BG>20! BKGD 1 is " << bkg1sum;
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );
        }

        if (bkg2sum[0] > 20)
        {
            ss.str ("");
            ss << "BG>20! BKGD 2 is " << bkg2sum;
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );
        }

        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::shared::CompoundDataStreamer tc (pdict);
        tc.insert_field ( bkghist->payloadanchor, "plusz", &m_BG1_plus.value_);
        tc.insert_field ( bkghist->payloadanchor, "minusz", &m_BG2_minus.value_);
        tc.insert_field ( bkghist->payloadanchor, "nc1", &m_BG1_nc.value_);
        tc.insert_field ( bkghist->payloadanchor, "nc2", &m_BG2_nc.value_);
        tc.insert_field ( bkghist->payloadanchor, "lead1", &m_BG1_lead.value_);
        tc.insert_field ( bkghist->payloadanchor, "lead2", &m_BG2_lead.value_);
        // END MORE COMPOUND-SPECIFIC STUFF
        m_topicoutqueues[interface::bril::bcm1fbkgT::topicname()]->push (bcm1fbkg);
    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to process data for bcm1fbkg");
        LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );

        if (bcm1fbkg)
        {
            bcm1fbkg->release();
            bcm1fbkg = 0;
        }
    }

}

void bril::bcm1fprocessor::Application::makeCollidingRates (interface::bril::shared::DatumHead& inheader)
{
    try
    {
        std::string pdict = interface::bril::bcm1flumiperchannelT::payloaddict();
        xdata::Properties plist;
        plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
        plist.setProperty ("PAYLOAD_DICT", pdict);

        // bcm1flumi_per_channel = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1flumiperchannelT::maxsize() );
        // bcm1flumi_per_channel->setDataSize (interface::bril::bcm1flumiperchannelT::maxsize() );
        // interface::bril::bcm1flumiperchannelT* ratehist = (interface::bril::bcm1flumiperchannelT*) (bcm1flumi_per_channel->getDataLocation() );

        // ratehist->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
        // ratehist->setFrequency (4);
        // ratehist->setResource (interface::bril::shared::DataSource::BCM1FSI, 0, 0, interface::bril::shared::StorageType::COMPOUND);
        // ratehist->setTotalsize (interface::bril::bcm1flumiperchannelT::maxsize() );

        double lumi_sensor[48] = {0.};
        double lumi_sensorRaw[48] = {0.};
        memset (m_rates_perchannel_percollidingbunches, 0, sizeof(m_rates_perchannel_percollidingbunches));
        double lumi_cshape[4] = {0.};
        double lumi_cshapeRaw[4] = {0.};
        memset (m_lumi_percshape, 0, sizeof(m_lumi_percshape));
        memset (m_lumi_percshapeRaw, 0, sizeof(m_lumi_percshapeRaw));
        m_lumiChannel.resize(52, 0.);
        m_lumiChannelRaw.resize(52, 0.);
        unsigned int maskhigh = 0;
        unsigned int masklow = 0;

        double sigmaVis = m_sigma_vis_si;
        double calibSBIL = m_calib_at_SBIL_si;
        double nonlinearity = m_nonlinearity_si;

        int nSensor[4] = {0};

        for (InDataCache::iterator channelit = m_bcm1fhistcache.begin(); channelit != m_bcm1fhistcache.end(); ++channelit)
        {
        
            toolbox::mem::Reference* histref = channelit->second->dataref;
            interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*) (histref->getDataLocation() );
            unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
            unsigned int channel = channelId - 1;
            double nb4Orbits = 4096. * channelit->second->nbcount;
            double histBxBins[3564];
            memset (histBxBins, 0, sizeof (histBxBins) );
            double histfourBxBins[14256];
            memset (histfourBxBins, 0, sizeof (histfourBxBins) );
        
            for (size_t bxbin = 0; bxbin < 3564; ++bxbin )
            {
                //if (!(bxbin >= 3534 && bxbin < 3549))
                if (! (bxbin >= 3534 && bxbin < 3564) ) // Mask rest of abort gap to easily avoid problems in new algo below
                {
                    //histBxBins[bxbin] = (float(histdataptr->payload()[bxbin*4+2])); // TAKE BIN 2 FOR COLLISIONS! (out of 0,1,2,3)
                    // NEW ALGORITHM: Take four bins, 1 before peak, peak itself, and two afterwards.
                    histBxBins[bxbin] = 0.;
                    if (channel >= 0 && channel < 48)
                    {
                        for (int mybin = 1; mybin < 5; mybin++)
                        {
                            histBxBins[bxbin] = histBxBins[bxbin] + (float (histdataptr->payload() [bxbin * 4 + mybin]) ); // TAKE 4 BINS FOR COLLISIONS! (1,2,3, and 4 out of 0,1,2,3 for a single bunch -- means also take first bin of next bx)
                        }
                    }

                }
            }
            if (channel >= 0 && channel < 48)
            {
                // Create channel mask to publish, two uint32s
                if (channel < 32)
                    masklow = masklow | (1 << channel);
                else if (channel >= 32 && channel < 64)
                    maskhigh = maskhigh | (1 << (channel - 32) );
                else
                {
                    // wrong channelid!
                }

                double r0 = 0;
                // make zero counting
                for (int bxBin = 0; bxBin < 3564; bxBin++)
                {
                    if (histBxBins[bxBin] > 0)
                    {
                        r0 = 1 - ( (double) histBxBins[bxBin] / nb4Orbits); // 1- probability of a non-zero (#non-zero/#orbits)   

                        if (r0 < 0.) r0 = 0.;

                        r0 = -log (r0);
                        r0 = (r0 * m_albedo_fraction_si[bxBin]) - m_raw_noise_si;

                        m_lumi_perchannel_bx[channel][bxBin] =  (r0 * 11246 / sigmaVis + pow (r0, 2) * pow (11246 / sigmaVis, 2) * nonlinearity * (1 + nonlinearity * calibSBIL));
                        // correct by efficiency scale factor
                        if (m_channelScaleFactor[channel]!=0.0) {
                            m_lumi_perchannel_bx[channel][bxBin] /= m_channelScaleFactor[channel];
                        }
                        else{
                            std::string msg ("ZERO EFFICIENCY in channel ");
                            LOG4CPLUS_ERROR (getApplicationLogger(), msg + std::to_string(channel) );
                        }
                        m_lumi_perchannel_bxRaw[channel][bxBin] = r0;
                        if (m_collmask[bxBin])
                        {
                            lumi_sensor[channel] += m_lumi_perchannel_bx[channel][bxBin];
                            lumi_sensorRaw[channel] += r0;
                        }
                    }
                }
                if (m_useChannelForLumi[channel])
                {
                    nSensor[channel/12]++;
                    lumi_cshape[channel/12] += lumi_sensor[channel];
                    lumi_cshapeRaw[channel/12] += lumi_sensorRaw[channel];
                }
                m_rates_perchannel_percollidingbunches[channel] = lumi_sensor[channel];
                m_lumiChannel[channel] = lumi_sensor[channel];
                m_lumiChannelRaw[channel] = lumi_sensorRaw[channel];
            }
        }
        
        for(int i = 0; i<4; i++)
        {                             

            if (nSensor[i] > 0) 
            {
                m_lumi_percshape[i] = lumi_cshape[i]/nSensor[i];
                m_lumi_percshapeRaw[i] = lumi_cshapeRaw[i]/nSensor[i];
            }

            m_lumiChannel[48 +i] = m_lumi_percshape[i];
            m_lumiChannelRaw[48 +i] = m_lumi_percshapeRaw[i];

        }

        // interface::bril::shared::CompoundDataStreamer tc (pdict);
        // tc.insert_field(ratehist->payloadanchor, "lumiperchannel", m_rates_perchannel_percollidingbunches);
        // tc.insert_field(ratehist->payloadanchor, "lumipercshape", m_lumi_percshape);
        // m_topicoutqueues[interface::bril::bcm1flumiperchannelT::topicname()]->push(bcm1flumi_per_channel);
        if (m_vdmflag)
        {
            for (int ch = 0;ch < 52; ch++)
            {
                toolbox::mem::Reference* bcm1flumi_per_channel_ref = 0;
                std::string topicname = interface::bril::bcm1flumiperchannelT::topicname() + '_' + std::to_string(ch);
                try
                {
                    bcm1flumi_per_channel_ref = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1flumiperchannelT::maxsize() );
                    bcm1flumi_per_channel_ref->setDataSize (interface::bril::bcm1flumiperchannelT::maxsize() );
                    interface::bril::bcm1flumiperchannelT* ratehist = (interface::bril::bcm1flumiperchannelT*) (bcm1flumi_per_channel_ref->getDataLocation() );

                    ratehist->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
                    ratehist->setFrequency (4);
                    ratehist->setResource (interface::bril::shared::DataSource::BCM1FSI, 0, 0, interface::bril::shared::StorageType::COMPOUND);
                    ratehist->setTotalsize (interface::bril::bcm1flumiperchannelT::maxsize() );
                    
                    interface::bril::shared::CompoundDataStreamer tc (pdict);
                    tc.insert_field (ratehist->payloadanchor, "calibtag", m_calibtag.value_.c_str() );
                    tc.insert_field (ratehist->payloadanchor, "avgraw", &m_lumiChannelRaw[ch].value_);
                    tc.insert_field (ratehist->payloadanchor, "avg", &m_lumiChannel[ch].value_);
                    tc.insert_field (ratehist->payloadanchor, "bxraw", m_lumi_perchannel_bxRaw[ch]);
                    tc.insert_field (ratehist->payloadanchor, "bx", m_lumi_perchannel_bx[ch]);
                    tc.insert_field (ratehist->payloadanchor, "masklow", &masklow);
                    tc.insert_field (ratehist->payloadanchor, "maskhigh", &maskhigh);
                    m_topicoutqueues[topicname]->push(bcm1flumi_per_channel_ref);
                }
                catch (xcept::Exception& e)
                {
                    std::string msg ("Failed to process data for bcm1fXXlumiperchannel");
                    LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );
                    if (bcm1flumi_per_channel_ref)
                    {
                        bcm1flumi_per_channel_ref->release();
                        bcm1flumi_per_channel_ref = 0;
                    }
                }
            }
        }


        std::stringstream ss;
        ss << "Lumi per channel: ";

        for( int channel=0;channel<52;channel++)
        {
            xdata::Float* xdatafloattemp = (xdata::Float*) m_lumiChannel.elementAt (channel);
            //ss << " " << ((xdata::Float*) m_totalChannelRate.elementAt(channel)).toString();
            float floattemp = xdatafloattemp->value_;
            ss << floattemp << " ";
        }

        ss << std::endl;

        if (m_verbose)
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );


    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to process data for bcm1fXXratepercollidingbunches");
        LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );
    }

}

// Calculates the luminosity numbers (full-orbit values
// and bunch-by-bunch histograms),
// and queues them for publishing
void bril::bcm1fprocessor::Application::makeLumiNumbers (interface::bril::shared::DatumHead& inheader)
{

    toolbox::mem::Reference* bcm1flumi = 0;

    LOG4CPLUS_DEBUG (getApplicationLogger(), "Still in do_process: bcm1fXXlumi");

    try
    {
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::bcm1flumiT::payloaddict();
        xdata::Properties plist;
        plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
        plist.setProperty ("PAYLOAD_DICT", pdict);
        // END COMPOUND-SPECIFIC STUFF

        bcm1flumi = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1flumiT::maxsize() );
        bcm1flumi->setDataSize (interface::bril::bcm1flumiT::maxsize() );
        interface::bril::bcm1flumiT* lumihist = (interface::bril::bcm1flumiT*) (bcm1flumi->getDataLocation() );

        // fill, run, ls, ln, sec, msec
        lumihist->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
        lumihist->setFrequency (4);

        // sourceid, algo, channel, payloadtype
        lumihist->setResource (interface::bril::shared::DataSource::BCM1FSI, 0, 0, interface::bril::shared::StorageType::COMPOUND);
        lumihist->setTotalsize (interface::bril::bcm1flumiT::maxsize() );

        // resetting values of global variables to zero
        m_lumi_avg = 0;
        m_lumi_avg_raw = 0;
        memset (m_lumi_bx, 0, sizeof (m_lumi_bx) );
        memset (m_lumi_bx_raw, 0, sizeof (m_lumi_bx_raw) );
        memset (m_lumi_perchannel_bx, 0, sizeof (m_lumi_bx) );
        memset (m_lumi_perchannel_bxRaw, 0, sizeof (m_lumi_bx_raw) );
        memset (m_raw_uncor_si, 0, sizeof (m_raw_uncor_si) );
        memset (m_fourbinsperbx, 0, sizeof (m_fourbinsperbx) );

        memset (m_BPTXhist, 0, sizeof (m_BPTXhist) );
        //      double histos4NB[48][3564];

        unsigned int maskhigh = 0;
        unsigned int masklow = 0;

        double muBX[48][3564];
        memset (muBX, 0, sizeof (muBX) );
        double lumiBX[48][3564];
        memset (lumiBX, 0, sizeof (lumiBX) );
        double lumiSensor[48];
        memset (lumiSensor, 0, sizeof (lumiSensor) );
        double lumiSensorRaw[48];
        memset (lumiSensorRaw, 0, sizeof (lumiSensorRaw) );
        double lumiperBX[3564];
        memset (lumiperBX, 0, sizeof (lumiperBX) );

        //double sigmaVis=2.825E-28; //this is in cm2 for 14 TeV
        //      double sigmaVis=2.63E-28; //this is in cm2 for 13 TeV
        //double sigmaVis=0.67E-28 ; //this is in cm2 for 900GeV
        double sigmaVis = m_sigma_vis_si;
        double calibSBIL = m_calib_at_SBIL_si;
        double nonlinearity = m_nonlinearity_si;

        int nSensors = 0;
        int nSensorsSpike = 0;
        double instLumi = 0.;
        double instLumiRaw = 0.;

        int highest_lowest_channels[4] = {}; //0 highest channel, 1 second highest, 2 lowest, 3 second lowest
        double highest_lowest_lumi_values[4] = {0.,0.,1e12,1e12}; //0 highest channel, 1 second highest, 2 lowest, 3 second lowest
        double trimmed_mean = 0.;

        double uncorrected_mu_perchannel[48][3564];
        memset (uncorrected_mu_perchannel, 0, sizeof (uncorrected_mu_perchannel) );

        // loop over channels -- ALL channels includes BPTX etc, but only channelid 1-48 are taken for lumi
        for (InDataCache::iterator channelit = m_bcm1fhistcache.begin(); channelit != m_bcm1fhistcache.end(); ++channelit)
        {
            toolbox::mem::Reference* histref = channelit->second->dataref;
            interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*) (histref->getDataLocation() );
            //float nNibbles = (float) channelit->second->nbcount;
            // TODO: DO REAL PROCESSING HERE
            //        // each output bin should sum the corresponding four input bins
            //size_t nbins = histdataptr->n();
            unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
            unsigned int channel = channelId - 1;
            double nb4Orbits = 4096. * channelit->second->nbcount;

            // take this from 14256 bins down to 3564 bins
            double histBxBins[3564];
            memset (histBxBins, 0, sizeof (histBxBins) );
            double histfourBxBins[14256];
            memset (histfourBxBins, 0, sizeof (histfourBxBins) );

            for (size_t bxbin = 0; bxbin < 3564; ++bxbin )
            {
                //if (!(bxbin >= 3534 && bxbin < 3549))
                if (! (bxbin >= 3534 && bxbin < 3564) ) // Mask rest of abort gap to easily avoid problems in new algo below
                {
                    //histBxBins[bxbin] = (float(histdataptr->payload()[bxbin*4+2])); // TAKE BIN 2 FOR COLLISIONS! (out of 0,1,2,3)
                    // NEW ALGORITHM: Take four bins, 1 before peak, peak itself, and two afterwards.
                    histBxBins[bxbin] = 0.;

                    if (channel >= 0 && channel < 48)
                    {
                        for (int mybin = 1; mybin < 5; mybin++)
                        {
                            histBxBins[bxbin] = histBxBins[bxbin] + (float (histdataptr->payload() [bxbin * 4 + mybin]) ); // TAKE 4 BINS FOR COLLISIONS! (1,2,3, and 4 out of 0,1,2,3 for a single bunch -- means also take first bin of next bx)
                        }
                    }

                    //for BPTX channels, aggregate all 4 bins in a BX toghether
                    if (channel > 47 && channel < 56)
                    {
                        for (int mybin = 0; mybin < 4; mybin++)
                            histBxBins[bxbin]  = histBxBins[bxbin] + (float (histdataptr->payload() [bxbin * 4 + mybin]) );
                    }
                }
            }

            for (size_t bxbin = 0; bxbin < 14256; ++bxbin )
                m_fourbinsperbx[channel][bxbin] = histdataptr->payload() [bxbin];

            int nonZeroBunches = 0;


            // this is going through histogram of 3564 bins
            if (channel >= 0 && channel < 48)
            {
                if (m_useChannelForLumi[channel])
                {
                    nSensors++;
                    if (m_channel_spike_mask[channel]) nSensorsSpike++;

                    // Create channel mask to publish, two uint32s
                    if (channel < 32)
                        masklow = masklow | (1 << channel);
                    else if (channel >= 32 && channel < 64)
                        maskhigh = maskhigh | (1 << (channel - 32) );
                    else
                    {
                        // wrong channelid!
                    }


                    // make zero counting
                    for (int bxBin = 0; bxBin < 3564; bxBin++)
                    {
                        if (histBxBins[bxBin] > 0) // FOR DEBUG ONLY
                        {
                            // it is the number of hits getting scaled, because we're correcting on the rate
                            double r0 = 1 - ( (double) histBxBins[bxBin] / nb4Orbits); // 1- probability of a non-zero (#non-zero/#orbits)

                            if (r0 < 0.) r0 = 0.;

                            muBX[channel][bxBin] = -log (r0); //the so called mu value or occupancy, this is proportional to luminosity

                            //add uncorrected mu to put it into albedo correction
                            m_raw_uncor_si[bxBin] = m_raw_uncor_si[bxBin] + muBX[channel][bxBin];
                            uncorrected_mu_perchannel[channel][bxBin] = muBX[channel][bxBin];
                            //Albedo correction. Albedo fraction should get updated regularily by the workloop
                            muBX[channel][bxBin] = (muBX[channel][bxBin] * m_albedo_fraction_si[bxBin]) - m_raw_noise_si;
                            lumiBX[channel][bxBin] = muBX[channel][bxBin] * 11246 / sigmaVis + pow (muBX[channel][bxBin], 2) * pow (11246 / sigmaVis, 2) * nonlinearity * (1 + nonlinearity * calibSBIL); //the 11246 to  get s-1
                            lumiperBX[bxBin] += lumiBX[channel][bxBin];
                            m_lumi_bx_raw[bxBin] = m_lumi_bx_raw[bxBin] + muBX[channel][bxBin];
                            m_lumi_bx[bxBin] = lumiperBX[bxBin];

                            // make sums from histogram with bx masking
                            if (m_collmask[bxBin])
                            {
                                lumiSensor[channel] += lumiBX[channel][bxBin];
                                lumiSensorRaw[channel] += muBX[channel][bxBin];
                                nonZeroBunches++;
                            }
                        }
                    } // end loop through bins j


                    // now sum up all channels from per channel sums

                    if (m_channelScaleFactor[channel]==0.0) {
                        std::string msg ("ZERO EFFICIENCY in channel ");
                        LOG4CPLUS_ERROR (getApplicationLogger(), msg + std::to_string(channel) );
                    }
                    else{
                        //apply efficiency scale factor
                        lumiSensor[channel] /= m_channelScaleFactor[channel];
                    }

                    instLumi += lumiSensor[channel];
                    instLumiRaw += lumiSensorRaw[channel];

                    //check if outlier, for trimmed mean
                    if (m_channel_spike_mask[channel]){
                        if (lumiSensor[channel] > highest_lowest_lumi_values[0])
                        {
                            highest_lowest_channels[1] = highest_lowest_channels[0];
                            highest_lowest_lumi_values[1] = highest_lowest_lumi_values[0];
                            highest_lowest_channels[0] = channel;
                            highest_lowest_lumi_values[0] = lumiSensor[channel];

                        }
                        else if (lumiSensor[channel] > highest_lowest_lumi_values[1])
                        {
                            highest_lowest_channels[1] = channel;
                            highest_lowest_lumi_values[1] = lumiSensor[channel];
                        }

                        if (lumiSensor[channel] < highest_lowest_lumi_values[2])
                        {
                            highest_lowest_channels[3] = highest_lowest_channels[2];
                            highest_lowest_lumi_values[3] = highest_lowest_lumi_values[2];
                            highest_lowest_channels[2] = channel;
                            highest_lowest_lumi_values[2] = lumiSensor[channel];
                        }
                        else if (lumiSensor[channel] < highest_lowest_lumi_values[3])
                        {
                            highest_lowest_channels[3] = channel;
                            highest_lowest_lumi_values[3] = lumiSensor[channel];
                        }
                    }
                } // end if use channel for lumi
            } // end only use channels 1-48

            if (channel > 47 && channel < 56)
                for (int ibin = 0; ibin < 3564; ibin++)
                    m_BPTXhist[channel - 48][ibin] = static_cast<float> (histBxBins[ibin]);
        } // end loop through channels i

        //Normalize everything with number of sensors
        if (nSensorsSpike > 4 && instLumi > m_trip_threshold)
        {

            for (int i = 0; i < 48; i++)
            {
                bool channel_in = true;
                for (int j = 0; j < 4; j++)
                {
                    if(i==highest_lowest_channels[j]) channel_in = false;
                }
                if(channel_in && m_useChannelForLumi[i] && m_channel_spike_mask[i]) trimmed_mean+=lumiSensor[i];
            }

            trimmed_mean /= (float) (nSensors - 4);
            if(trimmed_mean <= 0) {
                LOG4CPLUS_INFO (getApplicationLogger(), "TRIMMED MEAN IN SPIKE FINDER IS <=ZERO!! setting to small val");
                trimmed_mean = 0.000000001;
            }
            std::stringstream ss;

            int channels_on_b1 = 0;
            int channels_on_b2 = 0;

            //Check if each channel is spiked or off due to trip
            for(int i = 0; i < 48; i++)
            {
                bool out_of_window = ((lumiSensor[i]/trimmed_mean < m_lower_spike_ratio) && trimmed_mean > 0.00000001) || (lumiSensor[i]/trimmed_mean > m_upper_spike_ratio);
                bool spiked = m_channel_count_nospike[i] < m_time_spike;
                if((out_of_window || spiked) && m_useChannelForLumi[i])
                {
                    instLumi -= lumiSensor[i];
                    instLumiRaw -= lumiSensorRaw[i];
                    for (int bxBin = 0; bxBin < 3564; bxBin++)
                    {
                        m_lumi_bx[bxBin] -= lumiBX[i][bxBin];
                        m_lumi_bx_raw[bxBin] -= muBX[i][bxBin];
                        m_raw_uncor_si[bxBin] -= uncorrected_mu_perchannel[i][bxBin];
                    }
                    --nSensors;
                    m_channel_spike_mask[i] = false;
                    if (out_of_window)
                        m_channel_count_nospike[i] = 0;
                    if (i < 24) channels_on_b1+=1;
                    if (i > 23) channels_on_b2+=1;             
                }
                if (spiked && !out_of_window && m_useChannelForLumi[i])
                {
                    m_channel_count_nospike[i] +=1;
                    if(m_channel_count_nospike[i] >= m_time_spike) m_channel_spike_mask[i] = true;
                }
            } 

            // WORKAROUND FOR WHEN THERE ARE STILL 0 ACTIVE CHANNELS PER ONE SIDE, DESPITE THE THRESHOLD REQUIREMENT
            if (channels_on_b1 == 0)
            {
                float hi_lumi = 0;
                int id_to_unmask = 0;
                for(int i = 0; i < 24; i++)
                {
                    if (lumiSensor[i] > hi_lumi && m_useChannelForLumi[i])
                    {
                        id_to_unmask = i;
                        hi_lumi = lumiSensor[i];
                    }
                }
                m_channel_spike_mask[id_to_unmask] = true;
            }

            if (channels_on_b2 == 0)
            {
                float hi_lumi = 0;
                int id_to_unmask = 0;
                for(int i = 24; i < 48; i++)
                {
                    if (lumiSensor[i] > hi_lumi && m_useChannelForLumi[i])
                    {
                        id_to_unmask = i;
                        hi_lumi = lumiSensor[i];
                    }
                }
                m_channel_spike_mask[id_to_unmask] = true;
            }

        }
        else if (instLumi <= m_trip_threshold) {
            LOG4CPLUS_INFO (getApplicationLogger(), "RESETTING spiked channels due to low lumi " );
            // reset the counter if we go lower than threshold
            for(size_t i=0; i < sizeof(m_channel_count_nospike)/sizeof(m_channel_count_nospike[0]) ; i++ ){
                m_channel_count_nospike[i] = m_time_spike;
            }
            memset (m_channel_spike_mask, true, sizeof(m_channel_spike_mask));
        }
        else {
            LOG4CPLUS_ERROR (getApplicationLogger(), "TOO MANY CHANNELS SPIKED! trying reset " );
            // reset the counter if we go lower than threshold
            for(size_t i=0; i < sizeof(m_channel_count_nospike)/sizeof(m_channel_count_nospike[0]) ; i++ ){
                m_channel_count_nospike[i] = m_time_spike;
            }
            memset (m_channel_spike_mask, true, sizeof(m_channel_spike_mask));
        }


        if (nSensors > 0)
        {
            instLumi /= (float) nSensors;
            instLumiRaw /= (float) nSensors;
        }


        for (int bxBin = 0; bxBin < 3564; bxBin++)
        {
            if(nSensors<=0) continue;
            m_lumi_bx[bxBin] /= nSensors;
            m_lumi_bx_raw[bxBin] /= nSensors;
            m_raw_uncor_si[bxBin] /= nSensors;
        }

        //push raw histogram to albedo correction

        std::array<double, 3564> raw_uncor_si;

        for (int i = 0; i < 3564; i++)
            raw_uncor_si[i] = m_raw_uncor_si[i];

        m_albedoqueuesi.push_back (raw_uncor_si);

        // convert variable type
        m_lumi_avg.value_ = instLumi;
        m_lumi_avg_raw.value_ = instLumiRaw;

        std::stringstream ss;
        ss << "Avg lumi: " << m_lumi_avg << "  Avg lumi raw: " << m_lumi_avg_raw;

        if (m_verbose)
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

        ss.str ("");
        ss << "Channels masked (default): ";

        for (int bit = 0; bit < 32; bit++)
        if (!((masklow >> bit) & 1)){
            ss << std::to_string(bit)<< " ";
        }
            // ss << std::to_string(bit)<< ":" << ( (masklow >> bit) & 1) << " ";

        for (int bit = 0; bit < 32; bit++)
            if (!((maskhigh >> bit) & 1)){
                ss << std::to_string(bit+32)<< " ";
            }
            // ss << std::to_string(bit+32)<< ":" << ( (maskhigh >> bit) & 1) << " ";
        
        ss << std::endl;
        ss << "Channels masked for spikes: ";
        for (int i =0;i<48;i++)
        {
            if (!m_channel_spike_mask[i])
                ss << i << " ";
        }
        ss << std::endl;
        if (m_verbose)
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

        interface::bril::shared::CompoundDataStreamer tc (pdict);
        tc.insert_field ( lumihist->payloadanchor, "calibtag", m_calibtag.value_.c_str() );
        tc.insert_field ( lumihist->payloadanchor, "avgraw", &m_lumi_avg_raw.value_);
        tc.insert_field ( lumihist->payloadanchor, "avg", &m_lumi_avg.value_);
        tc.insert_field ( lumihist->payloadanchor, "bxraw", m_lumi_bx_raw);
        tc.insert_field ( lumihist->payloadanchor, "bx", m_lumi_bx);
        tc.insert_field ( lumihist->payloadanchor, "4bxraw", m_fourbinsperbx);
        tc.insert_field ( lumihist->payloadanchor, "masklow", &masklow);
        tc.insert_field ( lumihist->payloadanchor, "maskhigh", &maskhigh);
        m_topicoutqueues[interface::bril::bcm1flumiT::topicname()]->push (bcm1flumi);

        //new data for plotting
        m_chart_lumi_raw_si->newDataReady();
        //-------------------------------------------------------------------------------------------------
        // loop over channels -- ALL channels includes BPTX etc, but only channelid 1-48 are taken for lumi
        // for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
        // {
        // toolbox::mem::Reference* histref = channelit->second->dataref;
        // interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());

        // unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
        // unsigned int channel = channelId - 1;
        // for(size_t bxbin = 0; bxbin < 14265; ++bxbin )
        // {m_fourbinsperbx[channel][bxbin] = histdataptr->payload()[bxbin];}}
        // m_chart_fourbinsperbx->newDataReady();
        // m_chart_lumi_raw_si->newDataReady();
        // -------------------------------------------------------------
    }



    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to process data for bcm1fXXlumi");
        LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );

        if (bcm1flumi)
        {
            bcm1flumi->release();
            bcm1flumi = 0;
        }
    }
}
/* void bril::bcm1fprocessor::Application::makebxhistnumbers(interface::bril::DatumHead& inheader)
{
    // loop over channels -- ALL channels includes BPTX etc, but only channelid 1-48 are taken for lumi
    for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
    {
      toolbox::mem::Reference* histref = channelit->second->dataref;
      interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());

      unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
      unsigned int channel = channelId - 1;
      for(size_t bxbin = 0; bxbin < 14265; ++bxbin )
    {m_fourbinsperbx[channel][bxbin] = histdataptr->payload()[bxbin];}}
    m_chart_fourbinsperbx->newDataReady();
    //m_chart_lumi_raw_si->newDataReady();
}
 */
//Albedo workloop, calculates the albedo fraction regularily and puts the queue to its defined buffer length
bool bril::bcm1fprocessor::Application::albedo_calculation (toolbox::task::WorkLoop* wl)
{
    // temporary parameters, move to XML
    // int albedo_calculation_time = 10; //seconds
    // int albedo_queue_length = 100; //entires, time = entries * 1.4s
    // int noise_calc_start = 3480;
    // int noise_calc_end = 3530;

    // average all data in queue

    unsigned int no_entries_si = 0; //could check queue length, but want to avoid issues when queue is being filled while looping
    double uncorrected_si[3564];
    memset (uncorrected_si, 0.0, sizeof (uncorrected_si) );
    double corrected_si[3564];
    memset (corrected_si, 0.0, sizeof (corrected_si) );

    for (AlbedoIt it = m_albedoqueuesi.begin(); it != m_albedoqueuesi.end(); ++it)
    {
        for (int bx = 0; bx < 3564; bx++)
            uncorrected_si[bx] += it->at (bx);

        no_entries_si++;
    }


    // normalize with counted number of elements.
    for (int bx = 0; bx < 3564; bx++)
        uncorrected_si[bx] /= no_entries_si;



    if (no_entries_si != 0)
    {
        //calculate correction and update fraction array
        // initialize
        for (int i = 0; i < 3564; i++)
            corrected_si[i] = uncorrected_si[i];

        // correct the data
        for (int i = 0; i < 3564; i++)
        {
            if (m_collmask[i])
            {
                for (size_t j = i + 1; j < i + m_albedo_model_si.size(); j++) //separate loop becacuse model could be different length, currently use same model
                {
                    if ( j >= 3564 )
                        corrected_si[j - 3564] -= corrected_si[i] * m_albedo_model_si[j - i];
                    else
                    {
                        // correct the data
                        corrected_si[j] -= corrected_si[i] * m_albedo_model_si[j - i];
                    }
                }

            }
        }

        //make ratios
        for (int i = 0; i < 3564; i++)
        {
            m_albedoqueue_hist_si_uncor[i] = uncorrected_si[i];
            m_albedoqueue_hist_si_cor[i] = corrected_si[i];

            if (uncorrected_si[i] != 0.0)
                m_albedo_fraction_si[i] = corrected_si[i] / uncorrected_si[i];
            else
                m_albedo_fraction_si[i] = 1.0;

            m_devel_hist2[i] = m_albedo_fraction_si[i];
        }




        // calculate noise level in abort gap

        float noise_si = 0;
        int count = 0;

        for (unsigned int i = m_noise_calc_start; i <= m_noise_calc_end; i++)
        {
            noise_si += corrected_si[i];
            count++;
        }

        m_raw_noise_si = noise_si / count;


        //reduce queue to pre-defined length.
        if (no_entries_si > m_albedo_queue_length)
        {
            for (unsigned int i = 0; i < (no_entries_si - m_albedo_queue_length); i++)
                m_albedoqueuesi.pop_front();
        }

        //Fire plotting
        m_chart_albedoqueue_si->newDataReady();



    }

    sleep (m_albedo_calculation_time); // Run albedo correctino every X seconds (defined in XML)
    return true;
}


// Clear the fully-accumulated histograms in preparation
// to accumulate more
void bril::bcm1fprocessor::Application::clear_cache()
{
    LOG4CPLUS_DEBUG (getApplicationLogger(), "clear_cache");

    for (InDataCache::iterator it = m_bcm1fhistcache.begin(); it != m_bcm1fhistcache.end(); ++it)
    {
        if (it->second->dataref != 0)
        {
            it->second->dataref->release();
            it->second->dataref = 0;
        }

        delete it->second;
    }

    m_bcm1fhistcache.clear();
}

// Accumulate this nibble histogram's data in the bins of the given channel
void bril::bcm1fprocessor::Application::hist_accumulateInChannel (InDataCache::iterator channelit, interface::bril::bcm1fhistT* indata)
{
    toolbox::mem::Reference* basedata = channelit->second->dataref;
    interface::bril::bcm1fhistT* basehistdata = (interface::bril::bcm1fhistT*) ( basedata->getDataLocation() );

    for (size_t i = 0; i < basehistdata->n(); ++i)
        basehistdata->payload() [i] += indata->payload() [i];

    channelit->second->nbcount++;
}

// In this function we want to see if we have accumulated enough nibbles to process
// the data.  This accounts for some channels being inactive due to missing RHU (I think).
// At least one channel must have nibbles accumulated, and all channels with nibbles
// must have accumulated enough nibbles to process.
// This also accounts for the possibility that some boards are "missing" nibbles and
// sending too few; the processor will wait until each board sends at least the
// necessary number (and then normalize by the total number of nibbles sent).
// This situation should never happen during real running (only on internal LN signal
// if counter threshold misconfigured).
bool bril::bcm1fprocessor::Application::check_nbstatistics (unsigned short nnbs)
{
    bool someChannelHasData = false;
    bool allOccupiedChannelsAboveThreshold = true;
    int maxNibblesCounted = 0;

    for (InDataCache::iterator histchannelit = m_bcm1fhistcache.begin(); histchannelit != m_bcm1fhistcache.end(); ++histchannelit)
    {
        //if(histchannelit->second->nbcount!=nnbs) {
        // if not enough nibbles received in one channel, not ready to process yet
        // but ignore any channels who have 0 statistics as not active
        if (histchannelit->second->nbcount != 0)
        {
            someChannelHasData = true;

            if (histchannelit->second->nbcount < nnbs)
            {
                //std::stringstream ss;
                //ss<<"channel "<<histchannelit->first<<" nbcount "<<histchannelit->second->nbcount<<std::endl;
                //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                //return false;
                allOccupiedChannelsAboveThreshold = false;
            }

            if (histchannelit->second->nbcount > maxNibblesCounted)
                maxNibblesCounted = histchannelit->second->nbcount;
        }
    }

    // only return true if all channels are either zero or above threshold AND there is at least one channel with data
    if ( (someChannelHasData == true) && (allOccupiedChannelsAboveThreshold == true) )
    {
        //std::stringstream ss;
        //ss<<"nibble stat full"<<std::endl;
        //ss << "maxNibblesCounted full: " << maxNibblesCounted << std::endl;
        //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        return true;
    }

    //std::stringstream ss;
    //ss << "maxNibblesCounted not full: " << maxNibblesCounted << std::endl;
    //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    return false;
}

bool bril::bcm1fprocessor::Application::publishing (toolbox::task::WorkLoop* wl)
{
    QueueStoreIt it;

    for (it = m_topicoutqueues.begin(); it != m_topicoutqueues.end(); ++it)
    {
        if (it->second->empty() ) continue;

        std::string topicname = it->first;
        toolbox::mem::Reference* data = it->second->pop();


        std::pair<TopicStoreIt, TopicStoreIt > ret = m_out_topicTobuses.equal_range (topicname);

        for (TopicStoreIt topicit = ret.first; topicit != ret.second; ++topicit)
        {
            if (data)
            {
                std::string payloaddict;

                if (topicname == "bcm1fbkg")
                    payloaddict = interface::bril::bcm1fbkgT::payloaddict();

                else if (topicname == "bcm1flumi")
                    payloaddict = interface::bril::bcm1flumiT::payloaddict();

                else if (topicname.find("bcm1flumiperchannel")!=std::string::npos)
                    payloaddict = interface::bril::bcm1flumiperchannelT::payloaddict();

                else if (topicname == "bxconfig")
                    payloaddict = interface::bril::bxconfigT::payloaddict();

                else if (topicname == "bcm1fagghist")
                    payloaddict = interface::bril::bcm1fagghistT::payloaddict();
                else
                    LOG4CPLUS_ERROR (getApplicationLogger(), "Wrong topic name to publish!");

                do_publish (topicit->second, topicname, payloaddict, data->duplicate() );
            }
        }

        if (data) data->release();
    }

    usleep (1000); // Cool down the workloop
    return true;
}

void bril::bcm1fprocessor::Application::do_publish (const std::string& busname, const std::string& topicname, const std::string& pdict, toolbox::mem::Reference* bufRef)
{
    std::stringstream msg;

    try
    {
        xdata::Properties plist;
        plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
        plist.setProperty ("PAYLOAD_DICT", pdict);

        if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" )
        {
            plist.setProperty ("NOSTORE", "1"); // COMMENT FOR DEVELOPMENT
        }

        msg << "publish to " << busname << " , " << topicname << ", channelId " << m_lastheader.channelid << ", run " << m_lastheader.runnum << " ls " << m_lastheader.lsnum << " nb " << m_lastheader.nbnum;
        LOG4CPLUS_DEBUG (getApplicationLogger(), msg.str() );
        //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), msg.str()); }
        this->getEventingBus (busname).publish (topicname, bufRef, plist);
    }
    catch (xcept::Exception& e)
    {
        msg << "Failed to publish " << topicname << " to " << busname;
        LOG4CPLUS_ERROR (getApplicationLogger(), stdformat_exception_history (e) );

        if (bufRef)
        {
            bufRef->release();
            bufRef = 0;
        }

        XCEPT_DECLARE_NESTED (bril::bcm1fprocessor::exception::Exception, myerrorobj, msg.str(), e);
        this->notifyQualified ("fatal", myerrorobj);
    }

}

// When nibble-histogram receiving times out, process the received data
// and/or clear the stale data
void bril::bcm1fprocessor::Application::timeExpired (toolbox::task::TimerEvent& e)
{
    if (m_lastheader.runnum == 0) return; //cache is clean, do nothing

    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    unsigned int nowsec = t.sec();

    if ( (nowsec - m_lastreceivedsec) > 5 )
    {
        std::stringstream ss;
        ss << "Found stale cache. run " << m_lastheader.runnum << " ls " << m_lastheader.lsnum;

        if (check_nbstatistics (s_num_nb_to_collect))
        {
            LOG4CPLUS_DEBUG (getApplicationLogger(), "Full statistics, process the cache ");
            do_process (m_lastheader);
            clear_cache();
        }
        else
        {
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );
            LOG4CPLUS_INFO (getApplicationLogger(), "Insufficient statistics collected, discard the cache ");
            m_lastheader.runnum = 0;
            m_lastheader.lsnum = 0;
            //m_lastheader.cmslsnum = 0;
            m_lastheader.nbnum = 0;
            clear_cache();
        }
    }
}

void bril::bcm1fprocessor::Application::setupMonitoring()
{
    LOG4CPLUS_DEBUG ( getApplicationLogger(), __func__ );
    std::string monurn = createQualifiedInfoSpace ( "bcm1fprocessorMon" ).toString();
    m_mon_infospace = xdata::getInfoSpaceFactory()->get ( monurn );
    const int nvars = 15;
    const char* names[nvars] = { "beammode", "fill", "run", "ls", "nb", "timestamp", "background1", "background2", "luminositysi", "channelrates" ,"channellumis","BCM1FVME_NC_1","BCM1FVME_NC_2","BCM1FVME_Lead_1","BCM1FVME_Lead_2"};
    xdata::Serializable* vars[nvars] = { &m_beammode, &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_BG1_plus, &m_BG2_minus, &m_lumi_avg, &m_totalChannelRate, &m_lumiChannel,&m_BG1_nc,&m_BG2_nc,&m_BG1_lead,&m_BG2_lead};

    for ( int i = 0; i != nvars; ++i )
    {
        m_mon_varlist.push_back ( names[i] );
        m_mon_vars.push_back ( vars[i] );
        m_mon_infospace->fireItemAvailable ( names[i], vars[i] );
    }
}

void bril::bcm1fprocessor::Application::createTabBar (/*xgi::Input* in,*/ xgi::Output* out, bril::bcm1fprocessor::Tab tab)
{
    using namespace cgicc;
    std::string t_urn = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/";
    //draw the xdaq header
    //this->getHTMLHeader (in, out);
    // Create the Title, Tab bar

    //Style this thing
    *out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/bril/bcm1fprocessor/html/stylesheet.css\" media=\"screen\" />";
    *out << "<script src=\"/bril/bcm1fprocessor/html/highcharts.js\"></script>";
    *out << "<script src=\"/bril/bcm1fprocessor/html/boost.js\"></script>";

    std::ostringstream cTabBarString;
    std::string beammode = m_beammode;

    // switch to show the current tab
    switch (tab)
    {
        case Tab::DEFAULT:
            cTabBarString << a ("Default").set ("href", t_urn + "Default").set ("class", "button active") << a ("plotting").set ("href", t_urn + "plotting").set ("class", "button") << a ("RawRHUhists").set ("href", t_urn + "RawRHUhists").set ("class", "button") << a ("RawBPTXhists").set ("href", t_urn + "RawBPTXhists").set ("class", "button") << a ("BeamMode: " + beammode).set ("class", "text");
            break;

        case Tab::PLOTTING:
            cTabBarString << a ("Default").set ("href", t_urn + "Default").set ("class", "button") << a ("plotting").set ("href", t_urn + "plotting").set ("class", "button active") << a ("RawRHUhists").set ("href", t_urn + "RawRHUhists").set ("class", "button") << a ("RawBPTXhists").set ("href", t_urn + "RawBPTXhists").set ("class", "button") << a ("BeamMode: " + beammode).set ("class", "text");
            break;

        case Tab::RHU:
            cTabBarString << a ("Default").set ("href", t_urn + "Default").set ("class", "button") << a ("plotting").set ("href", t_urn + "plotting").set ("class", "button") << a ("RawRHUhists").set ("href", t_urn + "RawRHUhists").set ("class", "button active") << a ("RawBPTXhists").set ("href", t_urn + "RawBPTXhists").set ("class", "button") << a ("BeamMode: " + beammode).set ("class", "text");
            break;

        case Tab::BPTX:
            cTabBarString << a ("Default").set ("href", t_urn + "Default").set ("class", "button") << a ("plotting").set ("href", t_urn + "plotting").set ("class", "button") << a ("RawRHUhists").set ("href", t_urn + "RawRHUhists").set ("class", "button") << a ("RawBPTXhists").set ("href", t_urn + "RawBPTXhists").set ("class", "button active") << a ("BeamMode: " + beammode).set ("class", "text");
            break;

    }

    *out << cgicc::div ().set ("class", "title").add (h2 (a ("BCM1F Processor" ).set ("href", t_urn + "Default").set ("style", "float: left") ) ) << std::endl;
    *out << cgicc::div (cTabBarString.str() ).set ("class", "tab") << std::endl;
}
