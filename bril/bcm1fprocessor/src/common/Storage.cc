#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"

#include "bril/bcm1fprocessor/Storage.h"
#include "bril/bcm1fprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>

XDAQ_INSTANTIATOR_IMPL (bril::bcm1fprocessor::Storage)

bril::bcm1fprocessor::Storage::Storage (xdaq::ApplicationStub* s) : xdaq::Application (s), xgi::framework::UIManager (this), eventing::api::Member (this), m_applock (toolbox::BSem::FULL)
{
    xgi::framework::deferredbind (this, this, &bril::bcm1fprocessor::Storage::Default, "Default");
    b2in::nub::bind (this, &bril::bcm1fprocessor::Storage::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor = getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    m_collectnb = 4;
    //m_collectnb = 1;
    //  m_collectchannel = interface::bril::BCM1F_NRU*(interface::bril::BCM1F_NCRATE-1);
    //m_collectchannel = 1;
    m_comissioningMode = false;
    m_numberOfNibbles = 0; // 4;
    m_nChannelsThisNibble = 0;
    m_nHistsDiscarded = 0;
    m_alignedNB4 = false;

    for (int i = 0; i < 48; i++)
    {
        m_nibbles_in_ls[i] = 0;

        for (int j = 0; j < 14256; j++)
            m_summing_Hist[i][j] = 0;
    }

    m_last_ls_num = 0;
    m_VdMisIP5 = false;
    m_VdMactive = false;

    //  memset (m_useChannelForLumi,true,sizeof(m_useChannelForLumi));

    m_iTot1 = 0.;
    m_iTot2 = 0.;

    memset (m_iBunch1, 0., sizeof (m_iBunch1) );
    memset (m_iBunch2, 0., sizeof (m_iBunch2) );

    memset (m_agg_hist_nb4, false, sizeof (m_agg_hist_nb4) );
    memset (m_agg_hist_ls, false, sizeof (m_agg_hist_ls) );

    //  memset (m_totalChannelRate,0.,sizeof(m_totalChannelRate));
    //  std::fill(begin(m_totalChannelRate), end(m_totalChannelRate), 0.); // not needed?
    //  m_totalChannelRate(64,0.); // initialize xdata::Vector of xdata::Floats? NOPE
    //  m_totalChannelRate = new xdata::Vector();
    //  m_totalChannelRate.resize(s_nAllChannels);
    m_totalChannelRate.resize (s_nAllChannels, 0.);
    // for (int i = 0; i < 64; i++)
    //   {
    //     m_totalChannelRate.push_back(0.);
    //   }
    //  std::fill(m_totalChannelRate.begin(), m_totalChannelRate.end(), 0.);

    m_countAlbedoHigherThanBackground = 0;

    m_beammode = "";

    toolbox::net::URN memurn ("toolbox-mem-pool", m_classname + "_mem");

    try
    {
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool (memurn, allocator);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_datasources");
        m_appInfoSpace->fireItemAvailable ("eventinginput", &m_datasources);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_outputtopics");
        m_appInfoSpace->fireItemAvailable ("eventingoutput", &m_outputtopics);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_nRHUs");
        m_appInfoSpace->fireItemAvailable ("ComissioningMode", &m_comissioningMode);
        LOG4CPLUS_INFO (getApplicationLogger(), "fireItemAvailable: m_comissioningMode");
        m_appInfoSpace->fireItemAvailable ("nRHUs", &m_nRHUs);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_calibtag");
        m_appInfoSpace->fireItemAvailable ("verbose", &m_verbose);
        //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_activeChannelsLumi");
        m_appInfoSpace->addListener (this, "urn:xdaq-event:setDefaultValues");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop (m_appDescriptor->getURN() + "_publishing", "waiting");
    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to setup memory pool ");
        LOG4CPLUS_FATAL (getApplicationLogger(), msg + stdformat_exception_history (e) );
        XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg, e);
    }

}

bril::bcm1fprocessor::Storage::~Storage ()
{
    QueueStoreIt it;

    for (it = m_topicoutqueues.begin(); it != m_topicoutqueues.end(); ++it)
        delete it->second;

    m_topicoutqueues.clear();
}


// This makes the HyperDAQ page
void bril::bcm1fprocessor::Storage::Default (xgi::Input* in, xgi::Output* out)
{
    //  m_applock.take();

    *out << busesToHTML();
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN();
    *out << "URL: " << appurl;
    *out << cgicc::br();
}


// Sets default values of application according to parameters in the
// xml configuration file.  Listens for xdata::Event and reacts when it's of type
// "urn:xdaq-event:setDefaultValues"
void bril::bcm1fprocessor::Storage::actionPerformed (xdata::Event& e)
{
    LOG4CPLUS_DEBUG (getApplicationLogger(), "Received xdata event " << e.type() );
    std::stringstream msg;

    if ( e.type() == "urn:xdaq-event:setDefaultValues" )
    {
        size_t nsources = m_datasources.elements();

        try
        {
            for (size_t i = 0; i < nsources; ++i)
            {
                xdata::Properties* p = dynamic_cast<xdata::Properties*> (m_datasources.elementAt (i) );
                xdata::String databus;
                xdata::String topicsStr;

                if (p)
                {
                    databus = p->getProperty ("bus");
                    topicsStr = p->getProperty ("topics");
                    std::set<std::string> topics = toolbox::parseTokenSet (topicsStr.value_, ",");
                    m_in_busTotopics.insert (std::make_pair (databus.value_, topics) );
                }
            }

            subscribeall();
            size_t ntopics = m_outputtopics.elements();

            for (size_t i = 0; i < ntopics; ++i)
            {
                xdata::Properties* p = dynamic_cast<xdata::Properties*> (m_outputtopics.elementAt (i) );

                if (p)
                {
                    xdata::String topicname = p->getProperty ("topic");
                    xdata::String outputbusStr = p->getProperty ("buses");
                    std::set<std::string> outputbuses = toolbox::parseTokenSet (outputbusStr.value_, ",");

                    for (std::set<std::string>::iterator it = outputbuses.begin(); it != outputbuses.end(); ++it)
                        m_out_topicTobuses.insert (std::make_pair (topicname.value_, *it) );

                    m_topicoutqueues.insert (std::make_pair (topicname.value_, new toolbox::squeue<toolbox::mem::Reference*>) );
                    m_unreadybuses.insert (outputbuses.begin(), outputbuses.end() );
                }
            }

            for (std::set<std::string>::iterator it = m_unreadybuses.begin(); it != m_unreadybuses.end(); ++it)
            {
                this->getEventingBus (*it).addActionListener (this);
                //if(this->getEventingBus(*it).canPublish()) continue;
            }

            // unsigned int myNRhus = (unsigned int) m_nRHUs;
            m_collectchannel = interface::bril::BCM1F_NRU * m_nRHUs;
            std::stringstream ss;
            ss.str ("");
            ss << "Number of RHUs is " << m_nRHUs << ", number of channels to collect is " << m_collectchannel;
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

        }
        catch (xdata::exception::Exception& e)
        {
            msg << "Failed to parse application property";
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str() );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg.str(), e);
        }
    }
}

// When buses are ready to publish, start the publishing workloop and
// timer to check for stale cache. Listens for toolbox::Event
void  bril::bcm1fprocessor::Storage::actionPerformed (toolbox::Event& e)
{
    if (e.type() == "eventing::api::BusReadyToPublish")
    {
        std::string busname = (static_cast<eventing::api::Bus*> (e.originator() ) )->getBusName();
        std::stringstream msg;
        msg << "event Bus '" << busname << "' is ready to publish" ;
        LOG4CPLUS_INFO (getApplicationLogger(), msg.str() );
        m_unreadybuses.erase (busname);

        if (m_unreadybuses.size() != 0) return; //wait until all buses are ready

        try
        {
            toolbox::task::ActionSignature* as_publishing = toolbox::task::bind (this, &bril::bcm1fprocessor::Storage::publishing, "publishing");
            m_publishing->activate();
            m_publishing->submit (as_publishing);
        }
        catch (toolbox::task::exception::Exception& e)
        {
            msg << "Failed to start publishing workloop " << stdformat_exception_history (e);
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str() );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg.str(), e);
        }

        try
        {
            std::string appuuid = m_appDescriptor->getUUID().toString();
            toolbox::TimeInterval checkinterval (10, 0); // check every 10 seconds
            std::string timername ("stalecachecheck_timer");
            toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer (timername + "_" + appuuid);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate (start, this, checkinterval, 0, timername);
        }
        catch (toolbox::exception::Exception& e)
        {
            std::string msg ("failed to start timer ");
            LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );
            XCEPT_RETHROW (bril::bcm1fprocessor::exception::Exception, msg, e);
        }
    }
}

// subscribe to all input buses
void bril::bcm1fprocessor::Storage::subscribeall()
{
    for (std::map<std::string, std::set<std::string> >::iterator bit = m_in_busTotopics.begin(); bit != m_in_busTotopics.end(); ++bit )
    {
        std::string busname = bit->first;

        for (std::set<std::string>::iterator topicit = bit->second.begin(); topicit != bit->second.end(); ++topicit)
        {
            if (m_verbose)
                LOG4CPLUS_INFO (getApplicationLogger(), "subscribing " + busname + ":" + *topicit);

            try
            {
                this->getEventingBus (busname).subscribe (*topicit);
            }
            catch (eventing::api::exception::Exception& e)
            {
                LOG4CPLUS_ERROR (getApplicationLogger(), "failed to subscribe, remove topic " + stdformat_exception_history (e) );
                m_in_busTotopics[busname].erase (*topicit);
            }
        }
    }
}

// Listens for reception of data on the eventing.
void bril::bcm1fprocessor::Storage::onMessage (toolbox::mem::Reference* ref, xdata::Properties& plist)
{
    std::string action = plist.getProperty ("urn:b2in-eventing:action");

    if (action == "notify")
    {
        std::string topic = plist.getProperty ("urn:b2in-eventing:topic");
        //LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), "Received data from "+topic); }
        std::string payloaddict = plist.getProperty ("PAYLOAD_DICT");

        std::string v = plist.getProperty ("DATA_VERSION");

        if (v.empty() )
        {
            std::string msg ("Received data message without version header, do nothing");
            LOG4CPLUS_ERROR (getApplicationLogger(), msg);

            if (ref != 0)
            {
                ref->release();
                ref = 0;
            }

            return;
        }

        // if(v!=interface::bril::DATA_VERSION)
        //   {
        //  std::string msg("Mismatched bril data version in received message, do nothing");
        //  LOG4CPLUS_ERROR(getApplicationLogger(),msg);
        //  if(ref!=0){
        //    ref->release();
        //    ref=0;
        //  }
        //  return;
        //   }

        std::stringstream ss;
        ss << "Received data from " + topic;
        LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
        //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }


        // for (unsigned int bunch = 0; bunch < 3564; bunch++)
        //   {
        //  if (m_bkgd1mask[bunch] == true)
        //    {
        //      std::stringstream ss;
        //      ss << "(onMessage) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
        //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //    }
        //  if (m_bkgd2mask[bunch] == true)
        //    {
        //      std::stringstream ss;
        //      ss << "(onMessage) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
        //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //    }
        //   }

        interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*) (ref->getDataLocation() );

        // if(topic == m_receivingtopic.toString()){
        //   do_process(ref->getDataLocation(),ref->getDataSize());
        // }

        //std::stringstream ss;
        //ss<<"runnum "<<runnum<<" m_lastheader.runnum "<<m_lastheader.runnum<<" lsnum "<<lsnum<<" m_lastheader.lsnum "<<m_lastheader.lsnum<<std::endl;
        //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());


        if ( topic == interface::bril::bcm1fhistT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", retrieving source histos channelid "
               << inheader->getChannelID() << " in (lastheader) nibble "
               << m_lastheader.nbnum << " (this histo has nibble)" << inheader->nbnum;
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

            retrieveSourceHistos (ref, inheader);
        }
        else if (topic == interface::bril::beamT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", making bunch masks";
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

            makeBunchMasks (inheader);
        }
        else if (topic == interface::bril::vdmflagT::topicname() )
        {
            bool flag = false;
            interface::bril::vdmflagT* me = (interface::bril::vdmflagT*) ( ref->getDataLocation() );
            flag = me->payload() [0];

            if ( m_VdMactive == true && flag == false )
                m_VdMactive = false;
            else if (  m_VdMactive == false && flag == true )
                m_VdMactive = true;
        }
        else if (topic == interface::bril::vdmscanT::topicname() )
        {
            unsigned char ip;
            interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*) (ref->getDataLocation() );
            interface::bril::shared::CompoundDataStreamer tc (payloaddict);
            tc.extract_field ( &ip, "ip", thead->payloadanchor );

            if ( ip == 32 )
                m_VdMisIP5 = true;
            else
                m_VdMisIP5 = false;
        }

        if (ref != 0)
        {
            ref->release();
            ref = 0;
        }
    }
}// When BEAM data received on eventing, use it to make bunch
// masks for collision products and both beam backgrounds
void bril::bcm1fprocessor::Storage::makeBunchMasks (interface::bril::shared::DatumHead* inheader)
{
    char _machinemode[50];
    char _beammode[50];
    interface::bril::shared::CompoundDataStreamer tc (interface::bril::beamT::payloaddict() );
    tc.extract_field (_machinemode, "machinemode",  inheader->payloadanchor);
    tc.extract_field (_beammode, "status",  inheader->payloadanchor);
    std::string machinemode = std::string (_machinemode);
    std::string beammode = std::string (_beammode);
    m_beammode = beammode;

    std::stringstream ss;
    ss << "Mode: " << machinemode << " : " << beammode << std::endl;
    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );

}



// When BCM1F data received on eventing, check and store it
void bril::bcm1fprocessor::Storage::retrieveSourceHistos (toolbox::mem::Reference* ref, interface::bril::shared::DatumHead* inheader)
{

    unsigned int channelId = inheader->getChannelID();
    unsigned int fillnum = inheader->fillnum;
    unsigned int runnum = inheader->runnum;
    unsigned int lsnum = inheader->lsnum;
    unsigned int nbnum = inheader->nbnum;

    // if new nibble, reset nChannelsThisNibble
    if (nbnum != m_lastheader.nbnum)
        m_nChannelsThisNibble = 0;

    std::stringstream ss;
    ss << "Received BCM1F hist data #" << m_nChannelsThisNibble
       << ", run: " << runnum << " ls: " << lsnum << " nb: " << nbnum << " channelId: " << channelId;
    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );



    std::map<unsigned int, InData*>::iterator channelit = m_bcm1fhistcache.find (channelId);

    // Now, irrespective of whether or not the last block was processed,
    // check if the current histogram's channelid is already in the list.
    // If not, insert this channelid and the histogram's contents
    if (channelit == m_bcm1fhistcache.end() )
    {
        toolbox::mem::Reference*  myref =  m_poolFactory->getFrame (m_memPool, ref->getDataSize() );
        memcpy (myref->getDataLocation(), ref->getDataLocation(), ref->getDataSize() );
        InData* d = new InData (1, myref);
        m_bcm1fhistcache.insert (std::make_pair (channelId, d) );
        m_lastheader = *inheader;
        m_numberOfNibbles = 1;
        m_nChannelsThisNibble++;
    }
    // If it's already there, accumulate this histogram's contents, too
    else
    {
        interface::bril::bcm1fhistT* indata = (interface::bril::bcm1fhistT*) (ref->getDataLocation() );
        hist_accumulateInChannel (channelit, indata);
        m_lastheader = *inheader;
        m_nChannelsThisNibble++;
    }

    // MOVE THIS INSIDE THE BCM1FHIST TOPIC CHECK, not relevant for other topics
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    unsigned int nowsec = t.sec();
    m_lastreceivedsec = nowsec;



    // Check if we have collected all channels for all nibbles
    // If so, then process the lot -- starting at last_header

    // I DON'T THINK CHECK_CHANNELSTATISTICS IS DOING WHAT WE WANT -- returns true if one histogram each from all channels have been collected, no matter how many nibbles' worth!  So starts constantly returning true after the first nibble, I think
    if ( check_channelstatistics ( m_collectchannel ) )
    {
        LOG4CPLUS_DEBUG (getApplicationLogger(), "All channels collected for this nibble");

        if ( nbnum % 4 == 0) // 1
        {

            //if( check_nbstatistics( m_collectnb ) )
            if ( check_nbstatistics_simple ( m_collectnb ) )
            {
                LOG4CPLUS_DEBUG (getApplicationLogger(), "Full statistics collected, conclude the previous processing ");
                //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), "Full statistics collected, conclude the previous processing "); }
                do_process (m_lastheader);
                std::stringstream ss;
                ss.str ("");
                ss << "processed last header: run " << m_lastheader.runnum
                   << " ls " << m_lastheader.lsnum
                   << " nb " << m_lastheader.nbnum
                   << " (right now in run " << runnum
                   << " ls " << lsnum
                   << " nb " << nbnum
                   << ") ";
                LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );
                m_numberOfNibbles = 0;
                m_nChannelsThisNibble = 0;
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                // m_mon_timestamp.value_ = t;
                m_mon_timestamp = t;
                m_mon_fill = fillnum;
                m_mon_run = runnum;
                m_mon_ls = lsnum;
                m_mon_nb = nbnum;
                clear_cache();
            }
            // else if not enough nibbles, check if it's a run/ls boundary
            // and process anyway if so
            // (also checks if run and ls numbers are valid)
            else if ( ( m_lastheader.runnum != 0 && m_lastheader.lsnum != 0 )
                      && ( lsnum != m_lastheader.lsnum ) )
            {
                //ss.str("");
                //ss<<"current statistics channel "<<m_bcm1fhistcache.size()<<std::endl;
                //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());

                ss.str ("");
                ss << "Detected fewer nibbles than expected on run/ls boundary (" << m_numberOfNibbles
                   << " nibbles), process and scale by nibbles received: run " << m_lastheader.runnum
                   << " ls " << m_lastheader.lsnum << " nibble " << m_lastheader.nbnum << std::endl;

                if (m_numberOfNibbles > 0)
                {
                    do_process (m_lastheader);
                    ss << "processed last header: run " << m_lastheader.runnum
                       << " ls " << m_lastheader.lsnum
                       << " nb " << m_lastheader.nbnum
                       << " (right now in run " << runnum
                       << " ls " << lsnum
                       << " nb " << nbnum
                       << "). " ;
                }
                else
                    ss << "(Not) dividing by 0 collected nibbles, instead ignoring and clearing cache!";

                // if (m_alignedNB4)
                //    ss << " m_alignedNB4 is TRUE ";
                // else
                //    ss << " m_alignedNB4 is FALSE ";
                // m_nHistsDiscarded++;
                // ss << " m_nHistsDiscarded is " << m_nHistsDiscarded;
                if (m_verbose)
                    LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );

                m_numberOfNibbles = 0;
                m_nChannelsThisNibble = 0;
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                // m_mon_timestamp.value_ = t;
                m_mon_timestamp = t;
                m_mon_fill = fillnum;
                m_mon_run = runnum;
                m_mon_ls = lsnum;
                m_mon_nb = nbnum;
                clear_cache();
            }
        }
    }

    if ( !m_alignedNB4)
    {
        if ( m_lastheader.nbnum % 4 == 0 )
            m_alignedNB4 = true;
    }
}



// Process the accumulated nibbles:
// Calls functions that create and queue for publishing the following products
//  * agghists
void bril::bcm1fprocessor::Storage::do_process (interface::bril::shared::DatumHead& inheader)
{
    LOG4CPLUS_DEBUG (getApplicationLogger(), "Now entered do_process: VERY BEGINNING");

    bool publish_status = 0;

    if (m_comissioningMode) publish_status = 1;
    else
    {
        if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" ) publish_status = 0;
        else publish_status = 1;
    }

    if (publish_status)
    {
        if (m_VdMisIP5)
            publish_nb4 (inheader);

        aggregate_ls (inheader);
    }

    std::stringstream ss;
    ss.str ("");
    ss << "do_process run " << inheader.runnum
       << " ls " << inheader.lsnum
       << " nb " << inheader.nbnum
       << " channel " << inheader.getChannelID();
    LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );

}


//This is to publish the histograms aggregated per nb4
void bril::bcm1fprocessor::Storage::publish_nb4 (interface::bril::shared::DatumHead& inheader)
{
    toolbox::mem::Reference* bcm1fhistnb4 = 0;

    try
    {
        for (InDataCache::iterator channelit = m_bcm1fhistcache.begin(); channelit != m_bcm1fhistcache.end(); ++channelit)
        {
            unsigned int channelid = channelit->first;

            if (channelid < 49) // everything but bptx
            {
                toolbox::mem::Reference* histref = channelit->second->dataref;
                interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*) (histref->getDataLocation() );


                //creating a memory pool
                bcm1fhistnb4 = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1fhistnb4T::maxsize() );
                bcm1fhistnb4->setDataSize (interface::bril::bcm1fhistnb4T::maxsize() );
                interface::bril::bcm1fhistnb4T* histnb4 = (interface::bril::bcm1fhistnb4T*) (bcm1fhistnb4->getDataLocation() );
                //setting timing info
                histnb4->setTime (inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
                histnb4->setFrequency (interface::bril::shared::FrequencyType::NB4);
                //setting storage size
                histnb4->setResource (interface::bril::shared::DataSource::BCM1F, interface::bril::BCM1FAggAlgos::SUM, channelid, interface::bril::shared::StorageType::UINT64);
                histnb4->setTotalsize (interface::bril::bcm1fhistnb4T::maxsize() );

                std::string pdict = interface::bril::bcm1fhistnb4T::payloaddict();

                for (size_t bin = 0; bin < 14256; ++bin)
                {
                    m_agg_hist_nb4[bin] = histdataptr->payload() [bin];

                    std::stringstream ss;
                };

                interface::bril::shared::CompoundDataStreamer tc (pdict);

                tc.insert_field ( histnb4->payloadanchor, "nb4hist", m_agg_hist_nb4); // not sure

                tc.insert_field ( histnb4->payloadanchor, "nbcount", &channelit->second->nbcount);


                //for(size_t bin = 0; bin < interface::bril::bcm1fhistnb4T::n(); ++bin){
                //  histnb4->payload()[bin] = histdataptr->payload()[bin];
                //}

                LOG4CPLUS_DEBUG (getApplicationLogger(), "Pushing this channel's histnb4s to output queue");

                m_topicoutqueues[interface::bril::bcm1fhistnb4T::topicname()]->push (bcm1fhistnb4);
            }
        }
    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to process data for bcm1fhistnb4 ");
        LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );

        if (bcm1fhistnb4)
        {
            bcm1fhistnb4->release();
            bcm1fhistnb4 = 0;
        }
    }
}

//This is to publish the histograms per ls
void bril::bcm1fprocessor::Storage::aggregate_ls (interface::bril::shared::DatumHead& inheader)
{
    toolbox::mem::Reference* bcm1fhistls = 0;

    try
    {
        //publish and collect the new data
        if (inheader.lsnum != m_last_ls_num)
        {


            //first publish
            for (int channelid = 0; channelid < 48; channelid++)
            {

                //create a memory pool of a needed size
                bcm1fhistls = m_poolFactory->getFrame (m_memPool, interface::bril::bcm1fhistlsT::maxsize() );
                bcm1fhistls->setDataSize (interface::bril::bcm1fhistlsT::maxsize() );
                interface::bril::bcm1fhistlsT* histls = (interface::bril::bcm1fhistlsT*) (bcm1fhistls->getDataLocation() );
                //setting timing info
                histls->setTime (inheader.fillnum, inheader.runnum, m_last_ls_num, 0, inheader.timestampsec, inheader.timestampmsec);
                histls->setFrequency (interface::bril::shared::FrequencyType::NB64);
                //setting storage size
                histls->setResource (interface::bril::shared::DataSource::BCM1F, interface::bril::BCM1FAggAlgos::SUM, channelid + 1, interface::bril::shared::StorageType::UINT64);
                histls->setTotalsize (interface::bril::bcm1fhistlsT::maxsize() );

                std::string pdict = interface::bril::bcm1fhistlsT::payloaddict();

                interface::bril::shared::CompoundDataStreamer tc (pdict);
                tc.insert_field ( histls->payloadanchor, "lshist", m_summing_Hist[channelid]);
                tc.insert_field ( histls->payloadanchor, "nbcount", &m_nibbles_in_ls[channelid].value_);

                //for(size_t bin = 0; bin < interface::bril::bcm1fhistlsT::n(); ++bin){
                //  histls->payload()[bin] = m_summing_Hist[channelid - 1][bin];
                //}

                LOG4CPLUS_DEBUG (getApplicationLogger(), "Pushing this channel's histlss to output queue");
                m_topicoutqueues[interface::bril::bcm1fhistlsT::topicname()]->push (bcm1fhistls);

            }

            //and collect the new data from the current LS
            //set summing array to 0

            for (int i = 0; i < 48; i++)
            {
                m_nibbles_in_ls[i] = 0;

                for (int j = 0; j < 14256; j++)
                    m_summing_Hist[i][j] = 0;
            }

            m_last_ls_num = inheader.lsnum;
        }

        //summ the array
        for (InDataCache::iterator channelit = m_bcm1fhistcache.begin(); channelit != m_bcm1fhistcache.end(); ++channelit)
        {
            unsigned int channelId = channelit->first;

            if (channelId > 48)
            {
                continue; //Channel above 48 are BPTX and are not concerned by this application.
            }

            toolbox::mem::Reference* histref = channelit->second->dataref;
            interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*) (histref->getDataLocation() );

            m_nibbles_in_ls[channelId - 1] = m_nibbles_in_ls[channelId - 1] + channelit->second->nbcount;

            for (int i = 0; i < 14256; i++)
                m_summing_Hist[channelId - 1][i] += histdataptr->payload() [i];

        }
    }
    catch (xcept::Exception& e)
    {
        std::string msg ("Failed to accummulate the histograms per LS ");
        LOG4CPLUS_ERROR (getApplicationLogger(), msg + stdformat_exception_history (e) );

        if (bcm1fhistls)
        {
            bcm1fhistls->release();
            bcm1fhistls = 0;
        }
    }
}


// Clear the fully-accumulated histograms in preparation
// to accumulate more
void bril::bcm1fprocessor::Storage::clear_cache()
{
    LOG4CPLUS_DEBUG (getApplicationLogger(), "clear_cache");

    for (InDataCache::iterator it = m_bcm1fhistcache.begin(); it != m_bcm1fhistcache.end(); ++it)
    {
        if (it->second->dataref != 0)
        {
            it->second->dataref->release();
            it->second->dataref = 0;
        }

        delete it->second;
    }

    m_bcm1fhistcache.clear();
}

// Accumulate this nibble histogram's data in the bins of the given channel
void bril::bcm1fprocessor::Storage::hist_accumulateInChannel (InDataCache::iterator channelit, interface::bril::bcm1fhistT* indata)
{
    toolbox::mem::Reference* basedata = channelit->second->dataref;
    interface::bril::bcm1fhistT* basehistdata = (interface::bril::bcm1fhistT*) ( basedata->getDataLocation() );

    for (size_t i = 0; i < basehistdata->n(); ++i)
        basehistdata->payload() [i] += indata->payload() [i];

    channelit->second->nbcount++;

    if (channelit->second->nbcount > m_numberOfNibbles)
        m_numberOfNibbles = channelit->second->nbcount;
}

bool bril::bcm1fprocessor::Storage::check_channelstatistics (unsigned int nchannels)
{
    if (m_nChannelsThisNibble < nchannels)
    {
        //std::stringstream ss;
        //ss << "Size of cache is now " << m_bcm1fhistcache.size() << std::endl;
        // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        return false;
    }

    //std::stringstream ss;
    //ss << "Size of cache is now " << m_bcm1fhistcache.size() << std::endl;
    // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    return true;
}

// In this function we want to see if we have accumulated enough nibbles to process
// the data.  This accounts for some channels being inactive due to missing RHU (I think).
// At least one channel must have nibbles accumulated, and all channels with nibbles
// must have accumulated enough nibbles to process.
// This also accounts for the possibility that some boards are "missing" nibbles and
// sending too few; the processor will wait until each board sends at least the
// necessary number (and then normalize by the total number of nibbles sent).
// This situation should never happen during real running (only on internal LN signal
// if counter threshold misconfigured).
bool bril::bcm1fprocessor::Storage::check_nbstatistics (unsigned short nnbs)
{
    bool someChannelHasData = false;
    bool allOccupiedChannelsAboveThreshold = true;
    int maxNibblesCounted = 0;

    for (InDataCache::iterator histchannelit = m_bcm1fhistcache.begin(); histchannelit != m_bcm1fhistcache.end(); ++histchannelit)
    {
        //if(histchannelit->second->nbcount!=nnbs) {
        // if not enough nibbles received in one channel, not ready to process yet
        // but ignore any channels who have 0 statistics as not active
        if (histchannelit->second->nbcount != 0)
        {
            someChannelHasData = true;

            if (histchannelit->second->nbcount < nnbs)
            {
                std::stringstream ss;
                ss << "channel " << histchannelit->first << " nbcount " << histchannelit->second->nbcount << std::endl;
                LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );
                allOccupiedChannelsAboveThreshold = false;
            }

            if (histchannelit->second->nbcount > maxNibblesCounted)
                maxNibblesCounted = histchannelit->second->nbcount;
        }
    }

    // only return true if all channels are either zero or above threshold AND there is at least one channel with data
    if ( (someChannelHasData == true) && (allOccupiedChannelsAboveThreshold == true) )
    {
        //std::stringstream ss;
        //ss<<"nibble stat full"<<std::endl;
        //ss << "maxNibblesCounted full: " << maxNibblesCounted << std::endl;
        //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        return true;
    }

    //std::stringstream ss;
    //ss << "maxNibblesCounted not full: " << maxNibblesCounted << std::endl;
    //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    return false;
}

// This function merely checks if the collected number of nibbles is the full amount
// expected.  There is no workaround for RHUs that have dropped out, which so far
// (early 2017) has not been necessary.
// This one was written because the regular check_nbstatistics() function does not
// appear to be working exactly as expected.
bool bril::bcm1fprocessor::Storage::check_nbstatistics_simple (unsigned short nnbs)
{
    // If number of collected nibbles = number of nibbles to collect * number of RHUs * number of channels per RHU
    // then full statisics collected
    // Implemented as: if all channels collected have full set of nibbles collected, then full statistics collected

    bool allChannelsFull = true;

    for (InDataCache::iterator histchannelit = m_bcm1fhistcache.begin(); histchannelit != m_bcm1fhistcache.end(); ++histchannelit)
    {
        if (histchannelit->second->nbcount < nnbs)
            allChannelsFull = false;
    }

    return allChannelsFull;
}

bool bril::bcm1fprocessor::Storage::publishing (toolbox::task::WorkLoop* wl)
{
    QueueStoreIt it;

    for (it = m_topicoutqueues.begin(); it != m_topicoutqueues.end(); ++it)
    {
        //LOG4CPLUS_INFO(getApplicationLogger(),"Test in loop");
        if (it->second->empty() ) continue;

        std::string topicname = it->first;
        toolbox::mem::Reference* data = it->second->pop();

        std::pair<TopicStoreIt, TopicStoreIt > ret = m_out_topicTobuses.equal_range (topicname);

        for (TopicStoreIt topicit = ret.first; topicit != ret.second; ++topicit)
        {
            if (data)
            {
                std::string payloaddict;

                if (topicname == "bcm1fhistnb4")
                    payloaddict = interface::bril::bcm1fhistnb4T::payloaddict();
                else if (topicname == "bcm1fhistls")
                    payloaddict = interface::bril::bcm1fhistlsT::payloaddict();
                else
                    LOG4CPLUS_INFO (getApplicationLogger(), "Wrong topic name to publish!");

                do_publish (topicit->second, topicname, payloaddict, data->duplicate() );
            }
        }

        if (data) data->release();
    }

    //LOG4CPLUS_INFO(getApplicationLogger(),"Test");
    usleep (100);
    return true;
}

void bril::bcm1fprocessor::Storage::do_publish (const std::string& busname, const std::string& topicname, const std::string& pdict, toolbox::mem::Reference* bufRef)
{
    std::stringstream msg;

    try
    {
        xdata::Properties plist;
        plist.setProperty ("DATA_VERSION", interface::bril::shared::DATA_VERSION);
        plist.setProperty ("PAYLOAD_DICT", pdict);

        //if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" )
        //{
        //      plist.setProperty("NOSTORE","1"); // COMMENT FOR DEVELOPMENT
        //}

        msg << "publish to " << busname << " , " << topicname << ", channelId " << m_lastheader.channelid << ", run " << m_lastheader.runnum << " ls " << m_lastheader.lsnum << " nb " << m_lastheader.nbnum;
        LOG4CPLUS_DEBUG (getApplicationLogger(), msg.str() );
        //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), msg.str()); }
        this->getEventingBus (busname).publish (topicname, bufRef, plist);
    }
    catch (xcept::Exception& e)
    {
        msg << "Failed to publish " << topicname << " to " << busname;
        LOG4CPLUS_ERROR (getApplicationLogger(), stdformat_exception_history (e) );

        if (bufRef)
        {
            bufRef->release();
            bufRef = 0;
        }

        XCEPT_DECLARE_NESTED (bril::bcm1fprocessor::exception::Exception, myerrorobj, msg.str(), e);
        this->notifyQualified ("fatal", myerrorobj);
    }

    // if(bufRef){
    //   bufRef->release();
    //   bufRef = 0;
    // }
}

// When nibble-histogram receiving times out, process the received data
// and/or clear the stale data
void bril::bcm1fprocessor::Storage::timeExpired (toolbox::task::TimerEvent& e)
{
    if (m_lastheader.runnum == 0) return; //cache is clean, do nothing

    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    unsigned int nowsec = t.sec();

    if ( (nowsec - m_lastreceivedsec) > 5 )
    {
        std::stringstream ss;
        ss << "Found stale cache. run " << m_lastheader.runnum << " ls " << m_lastheader.lsnum;

        if ( check_channelstatistics (m_collectchannel) && check_nbstatistics (m_collectnb) )
        {
            LOG4CPLUS_DEBUG (getApplicationLogger(), "Full statistics, process the cache ");
            do_process (m_lastheader);
            clear_cache();
        }
        else
        {
            LOG4CPLUS_INFO (getApplicationLogger(), ss.str() );
            LOG4CPLUS_INFO (getApplicationLogger(), "Insufficient statistics collected, discard the cache ");
            m_lastheader.runnum = 0;
            m_lastheader.lsnum = 0;
            //m_lastheader.cmslsnum = 0;
            m_lastheader.nbnum = 0;
            clear_cache();
        }
    }
}
