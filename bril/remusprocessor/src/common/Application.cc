#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Table.h"
#include "xdata/Float.h"
#include "toolbox/mem/AutoReference.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Guard.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "bril/remusprocessor/Application.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "b2in/nub/Method.h"
#include "interface/bril/REMUSTopics.hh"
#include <algorithm>
XDAQ_INSTANTIATOR_IMPL (bril::remusprocessor::Application)

bril::remusprocessor::Application::Application(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&Application::Default, "Default");
  b2in::nub::bind(this, &bril::remusprocessor::Application::onMessage);
  toolbox::net::UUID uuid;
  m_processuuid = uuid.toString();
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  std::string memPoolName = std::string("remusprocessor_")+m_processuuid+std::string("_mem");
  toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
  toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
  m_memPool = m_poolFactory->createPool(urn,allocator);
  
  m_wl_evtpublish = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_processuuid+"_evtpublish","waiting");
  m_as_evtpublish = toolbox::task::bind(this,&bril::remusprocessor::Application::do_publishToEventing,"evtpublish");
  m_fillnum = 0;
  m_runnum = 0;
  m_lsnum = 0;
  m_tssec = 0;
  m_tsmsec = 0;
  m_evtcanpublish = false;
  m_busName = "brildata";
  m_dipbusName = "slimdata";
  m_signalTopic = "NB4";
  m_inTopics = "dip/REMUS/CMS/PMIL5514,dip/REMUS/CMS/PMIL5515,dip/REMUS/CMS/PMIL5516,dip/REMUS/CMS/PMIL5517";
  m_inCalibrations = "1,1,1,1";
  m_outTopicBase = "remuslumi";//outtopics outTopicBase+'_'+number plus one which is the best

  getApplicationInfoSpace()->fireItemAvailable("signalTopic",&m_signalTopic);
  getApplicationInfoSpace()->fireItemAvailable("busName",&m_busName);
  getApplicationInfoSpace()->fireItemAvailable("dipbusName",&m_dipbusName); 
  getApplicationInfoSpace()->fireItemAvailable("inTopics",&m_inTopics); 
  getApplicationInfoSpace()->fireItemAvailable("outTopicBase",&m_outTopicBase); 
  getApplicationInfoSpace()->fireItemAvailable("inCalibrations",&m_inCalibrations);
  getApplicationInfoSpace()->fireItemAvailable("bestTopic",&m_bestTopic);
  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  
}

void bril::remusprocessor::Application::Default(xgi::Input * in, xgi::Output * out) {}

void bril::remusprocessor::Application::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    std::stringstream ss;
    m_bestremusid = getID(m_bestTopic.value_);
    this->getEventingBus(m_busName.value_).addActionListener(this);  
    std::list< std::string > presult = toolbox::parseTokenList(m_inTopics.value_,","); 
    std::list< std::string > iresult = toolbox::parseTokenList(m_inCalibrations.value_,",");
    if( presult.size()!=iresult.size() ){
      ss<<"Non-matching inTopics and inCalibration size "<<presult.size()<<" : "<<iresult.size();
      XCEPT_RAISE(bril::remusprocessor::exception::Exception,ss.str());
    }
    m_dipsubs.insert( m_dipsubs.end(),presult.begin(), presult.end() );   
    std::vector< std::string > incalibstr; 
    incalibstr.insert( incalibstr.end(),iresult.begin(), iresult.end() );
    for( unsigned i=0; i<m_dipsubs.size(); ++i ){
      int remusid = getID(m_dipsubs[i]);
      float initval = 0.;
      float calibval = ::atof( incalibstr[i].c_str() );
      m_remuscache.insert( std::make_pair(remusid,initval) );
      m_calibcache.insert( std::make_pair(remusid,calibval) );
    }    
    toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
    toolbox::TimeInterval vsec(3,0);//3 sec later to subscribe to all topics
    toolbox::TimeVal startT = now + vsec;
    toolbox::net::UUID uuid;
    std::string startsubtimer_name = "RemusProcessor_timer_"+uuid.toString();  
    toolbox::task::Timer* t = toolbox::task::TimerFactory::getInstance()->createTimer( startsubtimer_name );
    t->schedule( this, startT, (void*)0, "startsubscription" );
    m_wl_evtpublish->activate();
  }
}

void bril::remusprocessor::Application::actionPerformed(toolbox::Event& e){  
  //brildata ready to publish
  if ( e.type() == "eventing::api::BusReadyToPublish" ){
    m_evtcanpublish = true;
  }
}

int bril::remusprocessor::Application::getID( const std::string& topicname ){
  std::string idstr = topicname.substr(topicname.size()-4,4);
  int id=std::stoi(idstr);
  return id;
}

void bril::remusprocessor::Application::handleDipMessage( std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist ){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"remusprocessor::Application::handleMessage from "+topic);
  std::stringstream ss;
  xdata::Table table;
  xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
  xdata::exdr::Serializer serializer;
  try{
    serializer.import(&table, &inBuffer);
  }catch (xdata::exception::Exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
    ref->release();
  }	
  int remusid = getID(topic);
  if( m_remuscache.find(remusid)==m_remuscache.end() ){
    ss<<"remusid "<<remusid<<" is not registered, do nothing";
    LOG4CPLUS_ERROR(getApplicationLogger(),"DipError "+ss.str());
    return;
  }
  xdata::Serializable* p = 0;
  float val = 0;
  try{
    p = table.getValueAt(0,"DR");
    val = dynamic_cast<xdata::Float*>(p)->value_;
  }catch(xdata::exception::Exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),e.what()); 
    ref->release();
  }	
  { //start lock scope
    toolbox::task::Guard<toolbox::BSem> g(m_applock);
    m_remuscache[remusid] = val;
  }
}

bool bril::remusprocessor::Application::do_publishToEventing(toolbox::task::WorkLoop* wl){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"Entering publish_to_eventing");
  if(!m_evtcanpublish) return false;
  usleep(400000);
  std::stringstream ss;  
  std::string besttopicname=interface::bril::remuslumiT::topicname(); 
  {//start lock scope
  toolbox::task::Guard<toolbox::BSem> g(m_applock);
  for( auto const& item:m_remuscache ){
    int remusid=item.first;
    ss<<remusid;
    std::string remusidstr=ss.str();
    ss.str("");
    std::string topicname=interface::bril::remuslumiT::topicname()+"_"+remusidstr;
    float rawval=item.second;
    float calibfactor=m_calibcache[remusid];
    float lumival=rawval*calibfactor;
    do_publishlumi(topicname,rawval,lumival);
    if( remusid==m_bestremusid ){
      //for bestremusid, also publish to remuslumi topic
      do_publishlumi(besttopicname,rawval,lumival);
    }
  }
  }
  return false;
}

void bril::remusprocessor::Application::do_publishlumi(const std::string& topicname,float rawlumi,float caliblumi){
  std::stringstream ss;
  toolbox::mem::Reference* bufRef = 0;
  try{
    std::string pdict = interface::bril::remuslumiT::payloaddict();
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",pdict);
    size_t totalsize = interface::bril::remuslumiT::maxsize();
    bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(m_memPool,totalsize);
    bufRef->setDataSize(totalsize);
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_fillnum,m_runnum,m_lsnum,m_nbnum,m_tssec,m_tsmsec);
    header->setResource(interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(totalsize);
    interface::bril::shared::CompoundDataStreamer tc(pdict);
    std::string calibtag("default");
    tc.insert_field( header->payloadanchor, "calibtag", calibtag.c_str());
    tc.insert_field( header->payloadanchor, "avgraw", &rawlumi);
    tc.insert_field( header->payloadanchor, "avg", &caliblumi);
    ss<<"Publishing "<<m_fillnum<<","<<m_runnum<<","<<m_lsnum<<","<<m_nbnum<<","<<m_tssec<<"."<<m_tsmsec<<" , "<<topicname<<":"<<rawlumi<<" "<<caliblumi<<" calibtag "<<calibtag;
    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    this->getEventingBus(m_busName.value_).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    ss<<"Failed to publish "<<topicname<<" to "<<m_busName.value_<<" "<<e.what();
    LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
    if(bufRef){
      bufRef->release() ;
    }
    XCEPT_DECLARE_NESTED(bril::remusprocessor::exception::Exception,myerrorobj,ss.str(),e);
    notifyQualified("fatal",myerrorobj);
  }
}

void bril::remusprocessor::Application::timeExpired( toolbox::task::TimerEvent& e ){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "timeExpired");
  std::string name = e.getTimerTask()->name;
  std::stringstream ss;
  if( name.find("startsubscription")!=std::string::npos ){	
    try{
      ss<<"subscribing to "<<m_signalTopic.toString()<<" on "<<m_busName.value_;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      ss.str("");
      this->getEventingBus(m_busName.value_).subscribe(m_signalTopic.value_);	  
    }catch(eventing::api::exception::Exception& e){
      ss<<"Failed to subscribe to eventing "<<m_busName.value_<<" "<<e.what();
      LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
      XCEPT_RETHROW(bril::remusprocessor::exception::Exception,ss.str(),e);     
    }
    
    try{
      for( auto const& name:m_dipsubs ){
	ss<<"subscribing to "<<name<<" on "<<m_dipbusName.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	ss.str("");
	this->getEventingBus(m_dipbusName.value_).subscribe( name );
      }
    }catch(eventing::api::exception::Exception& e){
      ss<<"Failed to subscribe to eventing "<<m_dipbusName.value_<<" "<<e.what();
      LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
      XCEPT_RETHROW(bril::remusprocessor::exception::Exception,ss.str(),e);     
    }
  }
}

void bril::remusprocessor::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"&&ref!=0){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
    std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
    if(topic == m_signalTopic.toString()){
      //parse timing signal message header to get timing info
      interface::bril::shared::DatumHead * thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());      
      m_fillnum = thead->fillnum;
      m_runnum = thead->runnum;
      m_lsnum = thead->lsnum;
      m_nbnum = thead->nbnum;
      m_tssec =  thead->timestampsec;
      m_tsmsec =  thead->timestampmsec;
      m_wl_evtpublish->submit(m_as_evtpublish);
    }else if( topic.find("dip/")!=std::string::npos ){
      handleDipMessage(topic,ref,plist);
    }
  }
}

