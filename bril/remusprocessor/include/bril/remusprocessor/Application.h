#ifndef _bril_remusprocessor_Application_h_
#define _bril_remusprocessor_Application_h_
#include <string>
#include <map>
#include <vector>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Properties.h"
#include "eventing/api/Member.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "bril/remusprocessor/exception/Exception.h"

namespace toolbox
 {
  namespace task
   {
     class WorkLoop;
     class ActionSignature;
   }
 }

namespace bril{

    namespace remusprocessor{

    class Application : public xdaq::Application,
      public xgi::framework::UIManager,
      public eventing::api::Member,
      public xdata::ActionListener,
      public toolbox::ActionListener,
      public toolbox::task::TimerListener
{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s);
      // destructor
      virtual ~Application(){}
      // xgi(web) callback
      virtual void Default(xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      virtual void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      //timer callback
      virtual void timeExpired( toolbox::task::TimerEvent& );

    private:
      int getID( const std::string& topicname );
      void handleDipMessage( std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist );
      bool do_publishToEventing(toolbox::task::WorkLoop* wl);
      void do_publishlumi(const std::string& topicname, float rawlumi, float caliblumi);

    private:
      toolbox::BSem m_applock;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;

      //configuration params
      xdata::String m_busName;
      xdata::String m_dipbusName;
      xdata::String m_signalTopic;
      xdata::String m_inTopics;
      xdata::String m_outTopicBase;
      xdata::String m_inCalibrations;
      xdata::String m_bestTopic;

      //registry and cache
      std::vector< std::string > m_dipsubs;
      
      unsigned int m_fillnum;
      unsigned int m_runnum;
      unsigned int m_lsnum;
      unsigned int m_nbnum;
      unsigned int m_tssec;
      unsigned int m_tsmsec;      
      bool m_evtcanpublish;
      std::string m_processuuid;
      int m_bestremusid;
      std::map<int,float> m_remuscache;
      std::map<int,float> m_calibcache;
      
      //workloops
      toolbox::task::WorkLoop* m_wl_evtpublish;
      toolbox::task::ActionSignature* m_as_evtpublish;

      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }//ns remusprocessor
}//ns bril
#endif
