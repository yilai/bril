// $Id$

/*************************************************************************
 * XDAQ Application Template                           *
 * Copyright (C) 2000-2009, CERN.                      *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_hfsource_version_h_
#define _bril_hfsource_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRIL_BRILHFSOURCE_VERSION_MAJOR 5
#define BRIL_BRILHFSOURCE_VERSION_MINOR 1
#define BRIL_BRILHFSOURCE_VERSION_PATCH 3
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILHFSOURCE_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_BRILHFSOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILHFSOURCE_VERSION_MAJOR,BRIL_BRILHFSOURCE_VERSION_MINOR,BRIL_BRILHFSOURCE_VERSION_PATCH)
#ifndef BRIL_BRILHFSOURCE_PREVIOUS_VERSIONS
#define BRIL_BRILHFSOURCE_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILHFSOURCE_VERSION_MAJOR,BRIL_BRILHFSOURCE_VERSION_MINOR,BRIL_BRILHFSOURCE_VERSION_PATCH)
#else
#define BRIL_BRILHFSOURCE_FULL_VERSION_LIST BRIL_BRILHFSOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILHFSOURCE_VERSION_MAJOR,BRIL_BRILHFSOURCE_VERSION_MINOR,BRIL_BRILHFSOURCE_VERSION_PATCH)
#endif

namespace brilhfsource
{
  const std::string project = "bril";
  const std::string package = "brilhfsource";
  const std::string versions = BRIL_BRILHFSOURCE_FULL_VERSION_LIST;
  const std::string summary = "XDAQ brilDAQ hfsource";
  const std::string description = "bril hf readout";
  const std::string authors = "C.Palmer, A.Shevelev";
  const std::string link = "http://xdaqwiki.cern.ch";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
