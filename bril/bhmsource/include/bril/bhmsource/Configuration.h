/**
 * @brief BHM Source xdaq Application.
 *
 * @author N. Tosi \<nicko@cern.ch\>
 *
 * This struct holds all the configuration variables
 *
 */

#ifndef _bril_bhmsource_Configuration_h_
#define _bril_bhmsource_Configuration_h_

#include <initializer_list>

#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Properties.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Vector.h"

namespace bril
 {

  namespace bhmsource
   {

    struct Configuration
     {

      //Application configuration
      xdata::Vector<xdata::Properties> data_sources;
      xdata::Vector<xdata::Properties> output_topics;
      xdata::Integer beam; // 1 or 2
      xdata::Boolean collect_amplitude;
      xdata::Boolean calibration_enabled;
      xdata::UnsignedInteger mon_timer_period_sec;
      xdata::String uhtr_uri;
      xdata::String ngccmsrv_uri;
      xdata::Integer rm_slot;

      //Hardware Configuration
      xdata::Vector<xdata::Properties> channels; //24x name, uhtr_threshold, qie_reg, clk_phase
      xdata::Integer integrate_nibbles; //number of nibbles to integrate in HW
      xdata::Integer uhtr_orbit_phase; //in BX
      xdata::String uhtr_tdcmap; //map of TDC string of 64 comma separated values, [0-4]
      xdata::Integer pulser_delay;
      xdata::Float pulser_bias_top;
      xdata::Float pulser_bias_bot;
      xdata::Float sipm_bias_top;
      xdata::Float sipm_bias_bot;

      std::initializer_list<const char*> info_varnames() const //only interesting variables are reported
       {
        return { "Beam", "RM slot", "Integration Nibbles", "Orbit Phase", "TDC Map", "Amplitude enabled", "Calibration Enabled", "Pulser Bias Near", "Pulser Bias Far", "SiPM Bias Near", "SiPM Bias Far" };
       }

      std::initializer_list<const xdata::Serializable*> info_variables() const //same as above
       {
        std::initializer_list<const xdata::Serializable*> lst{ &beam, &rm_slot, &integrate_nibbles, &uhtr_orbit_phase, &uhtr_tdcmap, &collect_amplitude, &calibration_enabled, &pulser_bias_top, &pulser_bias_bot, &sipm_bias_top, &sipm_bias_bot };
        return lst;
       }

      std::vector<std::string> channel_names() /*const*/
       {
        std::vector<std::string> ret;
        for ( auto ch = channels.begin(); ch != channels.end(); ++ch )
          ret.push_back( ch->getProperty( "name" ) );
        return ret;
       }

      std::vector<std::string> uhtr_thresholds() /*const*/
       {
        std::vector<std::string> ret;
        for ( auto ch = channels.begin(); ch != channels.end(); ++ch )
          ret.push_back( ch->getProperty( "uhtr_threshold" ) );
        return ret;
       }

      std::vector<std::string> qie_thresholds() /*const*/
       {
        std::vector<std::string> ret;
        for ( auto ch = channels.begin(); ch != channels.end(); ++ch )
          ret.push_back( ch->getProperty( "qie_threshold" ) );
        return ret;
       }

      std::vector<std::string> igloo_clock_phases() /*const*/
       {
        std::vector<std::string> ret;
        for ( auto ch = channels.begin(); ch != channels.end(); ++ch )
          ret.push_back( ch->getProperty( "clk_phase" ) );
        return ret;
       }

     };

   }

 }

#endif

