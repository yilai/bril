/**
 * @brief BHM Source xdaq Application.
 *
 * @author N. Tosi \<nicko@cern.ch\>
 *
 * This class provides access to the BHM Back End configuration and readout
 *
 */

#ifndef _bril_bhmsource_BackEnd_h_
#define _bril_bhmsource_BackEnd_h_

#include <string>

#include "toolbox/BSem.h"


//Check that nobody changed the API while I wasn't looking ;)
//#include "hcal/uhtr/version.h"
//#if ( ( HCALUHTR_VERSION_MAJOR != 2 ) || ( HCALUHTR_VERSION_MINOR != 4 ) || ( HCALUHTR_VERSION_PATCH > 2 ) )
//#error "HCAL API Mismatch"
//#endif
#include "hcal/uhtr/uHTR.hh"

#include "bril/bhmsource/Configuration.h"

namespace xdaq
 {

  class Application;

 }


namespace bril
 {

  namespace bhmsource
   {

    class Uhtr
     {

      public:
      typedef hcal::uhtr::uHTR::LumiHistogram OccupancyHistogram;
      typedef std::vector<OccupancyHistogram> OccupancyHistogramList;
      typedef std::vector<uint32_t> AmplitudeHistogram;

      struct AmplitudeHistogramList
       {
        AmplitudeHistogram histo[24][4];
        uint32_t fill;
        uint32_t run;
        uint32_t ls;
        uint32_t nb;
       };

      public:
      Uhtr( Configuration&, xdaq::Application* );

      ~Uhtr();

      void reset();

      bool sanityCheck();

      void loadConfig();

      void readConfig();

      OccupancyHistogramList getOccupancy();

      AmplitudeHistogramList getAmplitude();

      std::vector<std::string> getLinkFragments( size_t len );

      std::vector<AmplitudeHistogram> getLinkHistograms( size_t integration );

      private:
      toolbox::BSem m_lock;
      hcal::uhtr::uHTR* m_uhtr;
      Configuration& r_cfg;
      xdaq::Application* m_app;

     };

   }

 }

#endif

