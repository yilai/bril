// $Id$
#ifndef _bril_bptxprocessor_version_h_
#define _bril_bptxprocessor_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILBPTXPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILBPTXPROCESSOR_VERSION_MINOR 0
#define BRIL_BRILBPTXPROCESSOR_VERSION_PATCH 0
#define BRILBPTXPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILBPTXPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILBPTXPROCESSOR_VERSION_MAJOR,BRIL_BRILBPTXPROCESSOR_VERSION_MINOR,BRIL_BRILBPTXPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILBPTXPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILBPTXPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILBPTXPROCESSOR_VERSION_MAJOR,BRIL_BRILBPTXPROCESSOR_VERSION_MINOR,BRIL_BRILBPTXPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILBPTXPROCESSOR_FULL_VERSION_LIST BRIL_BRILBPTXPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILBPTXPROCESSOR_VERSION_MAJOR,BRIL_BRILBPTXPROCESSOR_VERSION_MINOR,BRIL_BRILBPTXPROCESSOR_VERSION_PATCH)
#endif
namespace brilbptxprocessor{
  const std::string project = "bril";
  const std::string package = "brilbptxprocessor";
  const std::string versions = BRIL_BRILBPTXPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bptxprocessor";
  const std::string description = "Read the data from scope monitoring scripts and republish to eventing and hyperDaq";
  const std::string authors = "Andrey Pozdnyakov";
  const std::string link = "http://vmebptx.cms:58008/urn:xdaq-application:lid=121";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
