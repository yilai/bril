
#ifndef _bril_hfprocessor_HFProcessor_h_
#define _bril_hfprocessor_HFProcessor_h_
#include <string>
#include <list>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xgi/framework/UIManager.h"

#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/EventDispatcher.h"
#include "interface/bril/HFTopics.hh"
#include "interface/bril/BEAMTopics.hh"

#include "toolbox/BSem.h"
#include "log4cplus/logger.h"
namespace toolbox{
  namespace task{
    class WorkLoop;
  }
}
namespace bril{
  namespace hfprocessor{
    class HFProcessor : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener{
    public:
      struct InData{
        InData(unsigned short n,toolbox::mem::Reference* d):nbcount(n),dataref(d){}
        InData():nbcount(0),dataref(0){}
        unsigned short nbcount;
        toolbox::mem::Reference* dataref;
      };

    public:
      XDAQ_INSTANTIATOR();
      // constructor
      HFProcessor (xdaq::ApplicationStub* s);
      // destructor
      ~HFProcessor ();
      // xgi(web) callback
      void Default (xgi::Input * in, xgi::Output * out);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
      // timer callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      
    private:
      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      #ifdef x86_64_centos7
      const
      #endif
      xdaq::ApplicationDescriptor *m_appDescriptor;
      std::string m_classname;
      std::stringstream debugStatement;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;     
      unsigned int m_collectnb;
      unsigned int m_collectchannel;
      bool m_alignedNB4;

      // application lock, it's useful to always include one
      toolbox::BSem m_applock;

      //eventinginput, eventingoutput configuration  
      xdata::Vector<xdata::Properties> m_datasources;
      xdata::Vector<xdata::Properties> m_outputtopics;
      xdata::Integer orbits_per_nibble;         // lumi nibble size (integer number of orbits)
      xdata::Integer integration_period_nb;     // integration period in lumi nibbles
      xdata::Integer nibbles_per_section;       // lumi section size (integer number of lumi nibbles)
      xdata::Boolean applyAfterglowCorr;
      xdata::Boolean applyPedestalCorr;
      xdata::Boolean useCustomBXMask;
      
      std::map<std::string, std::set<std::string> > m_in_busTotopics;
      typedef std::multimap< std::string, std::string > TopicStore;
      typedef std::multimap< std::string, std::string >::iterator TopicStoreIt;
      TopicStore m_out_topicTobuses;
      //count how many outgoing buses not ready
      std::set<std::string> m_unreadybuses;

      // incoming data cache // map from boardID (=crate*100+board) to hist
      std::map<unsigned int,toolbox::mem::Reference*>  hfRawHist;
      std::map<unsigned int,toolbox::mem::Reference*>  hfValidHist;
      toolbox::mem::Reference* afterglowBufRef;
      toolbox::mem::Reference* pedBufRef;
      toolbox::mem::Reference* histBufRef;
      toolbox::mem::Reference* bxBufRef;
     
      uint32_t aggHistPerBoardPerBX[36][interface::bril::shared::MAX_NBX];
      uint32_t aggValidHistPerBoardPerBX[36][interface::bril::shared::MAX_NBX];
      float    muHistPerBoardPerBX[36][interface::bril::shared::MAX_NBX];
      float    lumiHistPerBoardPerBX[36][interface::bril::shared::MAX_NBX];
      
      float    lumiHistPerBoard[36];
      
      uint32_t aggHistPerBX[interface::bril::shared::MAX_NBX];
      uint32_t aggValidHistPerBX[interface::bril::shared::MAX_NBX];
      float    aggNormHistPerBX[interface::bril::shared::MAX_NBX];
      float    muHistPerBX[interface::bril::shared::MAX_NBX];
      float    lumiHistPerBX[interface::bril::shared::MAX_NBX];

      float    muTotal;
      float    lumiTotal;
      
      int integration_period_orbits; // integration period in orbits
      float integration_period_secs; // integration period in seconds
      std::string curTopic;
      
      unsigned int lastMessageTime; //for std cout message
      unsigned int lastDataAcquired;
      double firstDataReceived;
      bool computeLumiNow;
      bool newLumiBlock;
      bool rejectHistUntilNewLN;
      bool saneData;
      xdata::String m_vdmflagTopic;
      bool m_vdmflag;

      unsigned int lastPubRun;
      unsigned int lastPubLS;
      unsigned int lastPubLN;
      double lastPub;

      toolbox::TimeVal timeNow;

      // outgoing data cache
      interface::bril::shared::DatumHead prevDataHeader;
      
      typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* > QueueStore;
      typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* >::iterator QueueStoreIt;
      QueueStore m_topicoutqueues;
      toolbox::task::WorkLoop* m_publishing;

      // counters for number of nibbles received
      //xdata::UnsignedInteger
      xdata::UnsignedInteger m_nHistsDiscarded;

      // lumi calibration and numbers to publish
      xdata::String m_calibtag;
      xdata::Float sigmaVis;
      xdata::String lumiAlgo;
      xdata::Float quadraticCorrection;
      xdata::Float BMthresOC;
      xdata::Float BMthresET;
      xdata::Float BMmin;
      xdata::UnsignedInteger HCALMaskLow;
      xdata::UnsignedInteger HCALMaskHigh;
      xdata::UnsignedInteger MAX_NBX_noAG;
      xdata::Float NearNoiseValue;
      xdata::Float AboveNoiseValue;
    
      std::string thisLumiTopic;

      // channel configuration
      unsigned short boardAvailableThisBlock[36];  //1 for CMS1/CMS_ET + 2 for CMS_Valid
      unsigned short NBoardsCollected;
      xdata::String m_beammode;
      
      double timeValidHist[36]; 
      double timeHist[36]; 

      // bunch masks for background and collisions
      bool activeBXMask[interface::bril::shared::MAX_NBX];
      bool m_iscolliding1[interface::bril::shared::MAX_NBX];
      bool m_iscolliding2[interface::bril::shared::MAX_NBX];
      bool m_machine_mask[interface::bril::shared::MAX_NBX];
      
      int NActiveBX;
      int NActiveBXN[150];
      unsigned short maxBX;
      short firstBX;
      
      //Afterglow Correction
      float hfafterglowPerBX[interface::bril::shared::MAX_NBX];
      float muUncorrectedPerBX[interface::bril::shared::MAX_NBX];
      float HFSBR[interface::bril::shared::MAX_NBX]; 
      float hfAfterGlowTotalScale;

      float pedestal[4];

    private:
      // methods
      //      void do_process(void* dataptr,size_t buffersize);
      //void do_process(interface::bril::shared::DatumHead& inheader);
      void do_process(interface::bril::shared::DatumHead& inheader);
      void ClearCache();
      bool publishing(toolbox::task::WorkLoop* wl);
      void do_publish(const std::string& busname,const std::string& topicname,const std::string& pdict,toolbox::mem::Reference* bufRef);
      void subscribeall();
      
      void RetrieveSourceHistos(toolbox::mem::Reference * ref, interface::bril::shared::DatumHead * inheader);
      bool SumHists(interface::bril::shared::DatumHead& inheader);
      void MakeDynamicBXMask(interface::bril::shared::DatumHead& inheader, float fracThreshold=0.1);
      void ComputeLumi(interface::bril::shared::DatumHead& inheader);
      void ComputeAfterglow(interface::bril::shared::DatumHead& inheader);
      void SubtractPedestal(interface::bril::shared::DatumHead& inheader);
      void InitializeHFSBR();
      void setMask();
      
      // nocopy protection
      HFProcessor(const HFProcessor&);
      HFProcessor& operator=(const HFProcessor&);

      //flashlist stuff
      void SetupMonitoring();
      xdata::InfoSpace* m_mon_infospace;
      std::list<std::string> m_mon_varlist;
      std::vector<xdata::Serializable*> m_mon_vars;
      xdata::UnsignedInteger m_mon_fill;
      xdata::UnsignedInteger m_mon_run;
      xdata::UnsignedInteger m_mon_ls;
      xdata::UnsignedInteger m_mon_nb;
      xdata::TimeVal m_mon_timestamp;
      xdata::Float           m_mon_nActiveBX;
      xdata::Float           m_mon_nBoards;
      xdata::UnsignedInteger m_mon_nCollected;
      xdata::Vector<xdata::Float>   m_mon_nbMod4PerBoard;
      xdata::Vector<xdata::Float>   m_mon_lumiPerBoard;
      xdata::Vector<xdata::Integer> m_mon_bunchMask;
    };
  }
}
#endif
