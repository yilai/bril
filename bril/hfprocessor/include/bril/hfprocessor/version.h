// $Id$
#ifndef _bril_hfprocessor_version_h_
#define _bril_hfprocessor_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILHFPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILHFPROCESSOR_VERSION_MINOR 0
#define BRIL_BRILHFPROCESSOR_VERSION_PATCH 5
#define BRILHFPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILHFPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILHFPROCESSOR_VERSION_MAJOR,BRIL_BRILHFPROCESSOR_VERSION_MINOR,BRIL_BRILHFPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILHFPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILHFPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILHFPROCESSOR_VERSION_MAJOR,BRIL_BRILHFPROCESSOR_VERSION_MINOR,BRIL_BRILHFPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILHFPROCESSOR_FULL_VERSION_LIST BRIL_BRILHFPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILHFPROCESSOR_VERSION_MAJOR,BRIL_BRILHFPROCESSOR_VERSION_MINOR,BRIL_BRILHFPROCESSOR_VERSION_PATCH)
#endif
namespace brilhfprocessor{
  const std::string project = "bril";
  const std::string package = "brilhfprocessor";
  const std::string versions = BRIL_BRILHFPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ hfprocessor";
  const std::string description = "collect and process hf histograms from hfsource";
  const std::string authors = "C.Palmer";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
