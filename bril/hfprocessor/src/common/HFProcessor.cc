#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "bril/hfprocessor/HFProcessor.h" 
#include "bril/hfprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>
#include <stdio.h>
#include <string.h>

#include "interface/bril/BEAMTopics.hh"

XDAQ_INSTANTIATOR_IMPL (bril::hfprocessor::HFProcessor)

using namespace interface::bril;
 
bril::hfprocessor::HFProcessor::HFProcessor (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::hfprocessor::HFProcessor::Default,"Default");
    b2in::nub::bind(this, &bril::hfprocessor::HFProcessor::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor= getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    m_collectnb = 4;
    m_nHistsDiscarded = 0;
    m_alignedNB4 = false;
    lumiTotal = 0;
    muTotal = 0;
    computeLumiNow = false;
    saneData = false;
    rejectHistUntilNewLN = false;
    m_vdmflagTopic.fromString("vdmflag");
    m_vdmflag = false;

    lastPubRun=1;
    lastPubLS=1;
    lastPubLN=1;
    lastPub=1;
    hfAfterGlowTotalScale=1;

    thisLumiTopic="";

    m_mon_nbMod4PerBoard.resize(36, -1);
    m_mon_lumiPerBoard.resize(36, 0);
    m_mon_bunchMask.resize(interface::bril::shared::MAX_NBX,0);
    m_mon_nActiveBX = 0;
    m_mon_nBoards   = 0;
    m_mon_nCollected=0;
    BMthresET= 0.001;
    BMthresOC= 8e-6;
    NearNoiseValue = 1e-4;
    AboveNoiseValue = 1e-5;
    BMmin = 1e-2;
    HCALMaskLow = 3481;
    HCALMaskHigh = 3499;
    MAX_NBX_noAG = 3480;
    
    m_beammode = "";
        
    afterglowBufRef=0;
    pedBufRef = 0;
    histBufRef = 0;
    bxBufRef = 0;
    
    // Bunch mask
    memset (m_iscolliding1, false, sizeof (m_iscolliding1) );
    memset (m_iscolliding2, false, sizeof (m_iscolliding2) );
    memset (m_machine_mask, false, sizeof (m_machine_mask) );

  
    debugStatement.str("");
    debugStatement.precision(3);
    
    toolbox::net::URN memurn("toolbox-mem-pool",m_classname+"_mem"); 
    try{
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool(memurn,allocator);
        m_appInfoSpace->fireItemAvailable("eventinginput",&m_datasources);
        LOG4CPLUS_DEBUG(getApplicationLogger(), "got input eventing topics");
        m_appInfoSpace->fireItemAvailable("eventingoutput",&m_outputtopics);
        LOG4CPLUS_DEBUG(getApplicationLogger(), "got output eventing topics");
        m_appInfoSpace->fireItemAvailable("calibtag",&m_calibtag);
        m_appInfoSpace->addListener(this,"urn:xdaq-event:setDefaultValues");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_publishing","waiting");
   
        //CP my stuff
        m_appInfoSpace->fireItemAvailable("sigmaVis",                 &sigmaVis);
        m_appInfoSpace->fireItemAvailable("lumiAlgo",                 &lumiAlgo);
        m_appInfoSpace->fireItemAvailable("quadraticCorrection",      &quadraticCorrection);
        m_appInfoSpace->fireItemAvailable("orbits_per_nibble",        &orbits_per_nibble);
        m_appInfoSpace->fireItemAvailable("integration_period_nb",    &integration_period_nb);
        m_appInfoSpace->fireItemAvailable("nibbles_per_section",      &nibbles_per_section);
        m_appInfoSpace->fireItemAvailable("applyAfterglowCorr",       &applyAfterglowCorr);
        m_appInfoSpace->fireItemAvailable("applyPedestalCorr",        &applyPedestalCorr);
        m_appInfoSpace->fireItemAvailable("useCustomBXMask",          &useCustomBXMask);
        m_appInfoSpace->fireItemAvailable("vdmflagTopic",             &m_vdmflagTopic);
        m_appInfoSpace->fireItemAvailable("BMthresET",                &BMthresET);
        m_appInfoSpace->fireItemAvailable("BMthresOC",                &BMthresOC);
        m_appInfoSpace->fireItemAvailable("NearNoiseValue",           &NearNoiseValue);
        m_appInfoSpace->fireItemAvailable("AboveNoiseValue",          &AboveNoiseValue);
        m_appInfoSpace->fireItemAvailable("BMmin",                    &BMmin);

    } catch(xcept::Exception& e){
        std::string msg("Failed to setup memory pool ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        //XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg,e);  
    }

    SetupMonitoring();
}

bril::hfprocessor::HFProcessor::~HFProcessor (){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    delete it->second;
  }
  m_topicoutqueues.clear();
}


// This makes the HyperDAQ page
void bril::hfprocessor::HFProcessor::Default (xgi::Input * in, xgi::Output * out){
  //m_applock.take();

  ///////*out << busesToHTML();
  ///////std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
  ///////*out << "URL: "<< appurl;
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::caption("input/output");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("calibtag");
  ///////*out << cgicc::td( m_calibtag );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("sigmaVis");
  ///////*out << cgicc::td( sigmaVis.toString() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("in_topic");
  ///////*out << cgicc::td( interface::bril::hfCMS1T::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("out_topic");
  ///////*out << cgicc::td( interface::bril::hfOcc1AggT::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("out_topic");
  ///////*out << cgicc::td( interface::bril::hfoclumiT::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("# hists discarded on LS/run boundary");
  ///////*out << cgicc::td( m_nHistsDiscarded.toString() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();  
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();

  ///////*out << "Automatic channel masking is ";
  ///////if (m_useAutomaticChannelMasking)
  ///////  {
  ///////    *out << "ON";
  ///////  }
  ///////else
  ///////  {
  ///////    *out << "OFF";
  ///////  }
  ///////*out << cgicc::br();
  ///////*out << cgicc::br();

  ///////*out << "Permanently-masked channels: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-horizontal");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  /////////  *out << cgicc::th("Channels");
  ///////for (xdata::Integer channel = 0; channel < 48; channel++)
  ///////  {
  ///////    if (!m_useChannelForLumi[channel.value_])
  ///////{
  ///////  *out << cgicc::td( channel.toString() );
  ///////}
  ///////  }
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "Temporarily-masked channels: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-horizontal");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  /////////  *out << cgicc::th("Channels");
  ///////for (xdata::Integer channel = 0; channel < 48; channel++)
  ///////  {
  ///////    if (!boardAvailableThisBlock[channel.value_])
  ///////{
  ///////  *out << cgicc::td( channel.toString() );
  ///////}
  ///////  }
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "The current background numbers ARE: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("BG 1");
  ///////*out << cgicc::td( m_BG1_plus.toString() );
  ///////// *out << cgicc::td( &m_BG1_plus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("BG 2");
  ///////*out << cgicc::td( m_BG2_minus.toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("How many times is albedo higher than background?");
  ///////*out << cgicc::td( xdata::Integer(m_countAlbedoHigherThanBackground).toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "And the current lumi average numbers ARE: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("Lumi (calibrated)");
  ///////*out << cgicc::td( lumiTotal.toString() );
  ///////// *out << cgicc::td( &m_BG1_plus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("Lumi (raw)");
  ///////*out << cgicc::td( lumiTotal_raw.toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "First few elements from lumi histogram: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////for (unsigned int bin = 0; bin < 20; bin++)
  ///////  {
  ///////    *out << cgicc::tr();
  ///////    *out << cgicc::th( xdata::Integer(bin).toString() );
  ///////    *out << cgicc::td( xdata::Float(lumiHistPerBX[bin]).toString() );
  ///////    *out << cgicc::tr();
  ///////  }

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();

  //m_applock.give();
}

void bril::hfprocessor::HFProcessor::SetupMonitoring(){
    LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
    std::string monurn = createQualifiedInfoSpace( "hfProcessorMon" ).toString();
    m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
    const int nvars = 11;
    const char* names[nvars] = { "fill", "run", "ls", "nb", "timestamp", "nActiveBX", "nBoards", "statsCollected", "nbMod4PerBoard", "lumiPerBoard", "bunchMask"};
    xdata::Serializable* vars[nvars] = { &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_mon_nActiveBX, &m_mon_nBoards, &m_mon_nCollected, &m_mon_nbMod4PerBoard, &m_mon_lumiPerBoard, &m_mon_bunchMask };
    for ( int i = 0; i != nvars; ++i ){
        m_mon_varlist.push_back( names[i] );
        m_mon_vars.push_back( vars[i] );
        m_mon_infospace->fireItemAvailable( names[i], vars[i] );
    }
}


// Sets default values of application according to parameters in the 
// xml configuration file.  Listens for xdata::Event and reacts when it's of type
// "urn:xdaq-event:setDefaultValues"
void bril::hfprocessor::HFProcessor::actionPerformed(xdata::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    std::stringstream msg;
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
        size_t nsources = m_datasources.elements();
        try{
            for(size_t i=0;i<nsources;++i){
                xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
                xdata::String databus;
                xdata::String topicsStr;
                if(p){
                  databus = p->getProperty("bus");     
                  topicsStr = p->getProperty("topics");
                  std::set<std::string> topics = toolbox::parseTokenSet(topicsStr.value_,",");     
                  m_in_busTotopics.insert(std::make_pair(databus.value_,topics));
                }
            }
            
            this->getEventingBus("brildata").subscribe(interface::bril::beamT::topicname());
            
            subscribeall();
            size_t ntopics = m_outputtopics.elements();
            for(size_t i=0;i<ntopics;++i){
                xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_outputtopics.elementAt(i));
                if(p){
                  xdata::String topicname = p->getProperty("topic");
                  xdata::String outputbusStr = p->getProperty("buses");
                  std::set<std::string> outputbuses = toolbox::parseTokenSet(outputbusStr.value_,",");
                  for(std::set<std::string>::iterator it=outputbuses.begin(); it!=outputbuses.end(); ++it){
                    m_out_topicTobuses.insert(std::make_pair(topicname.value_,*it));
                  }
                  m_topicoutqueues.insert(std::make_pair(topicname.value_,new toolbox::squeue<toolbox::mem::Reference*>));
                  m_unreadybuses.insert(outputbuses.begin(),outputbuses.end());
                }
            }
            for(std::set<std::string>::iterator it=m_unreadybuses.begin(); it!=m_unreadybuses.end(); ++it){
                debugStatement<<"listening?"<<*it;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                this->getEventingBus(*it).addActionListener(this);
            }
    
            integration_period_orbits=integration_period_nb.value_ * orbits_per_nibble.value_;
            integration_period_secs=integration_period_orbits / 11245.6;
            
            if (lumiAlgo.value_ == "occupancy") {
                thisLumiTopic.append(interface::bril::hfoclumiT::topicname());
            } else if (lumiAlgo.value_ == "etsum") {
                thisLumiTopic.append(interface::bril::hfetlumiT::topicname());
            }
            InitializeHFSBR(); //need lumiAlgo
    
        }catch(xdata::exception::Exception& e){
            msg<<"Failed to parse application property";
            LOG4CPLUS_ERROR(getApplicationLogger(), msg.str());
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception, msg.str(), e);
        }      
    }
}

// When buses are ready to publish, start the publishing workloop and 
// timer to check for stale cache. Listens for toolbox::Event
void  bril::hfprocessor::HFProcessor::actionPerformed(toolbox::Event& e){
    if(e.type() == "eventing::api::BusReadyToPublish"){
        std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
        std::stringstream msg;
        msg<< "event Bus '" << busname << "' is ready to publish";
        m_unreadybuses.erase(busname);
        if(m_unreadybuses.size()!=0) return; //wait until all buses are ready
        try{    
            toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::hfprocessor::HFProcessor::publishing,"publishing");
            m_publishing->activate();
            m_publishing->submit(as_publishing);
        }catch(toolbox::task::exception::Exception& e){
            msg<<"Failed to start publishing workloop "<<stdformat_exception_history(e);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg.str(),e);  
        }    
        try{
            std::string appuuid = m_appDescriptor->getUUID().toString();
            // check timeExpired every 120 seconds
            // huge time right now because timeExpired is currently doing nothing
            toolbox::TimeInterval checkinterval(120,0);
            std::string timername("stalecachecheck_timer");
            toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername+"_"+appuuid);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate(start, this, checkinterval, 0, timername);
        }catch(toolbox::exception::Exception& e){
            std::string msg("failed to start timer ");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e) );
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg,e);
        }
    }
}

// subscribe to all input buses
void bril::hfprocessor::HFProcessor::subscribeall(){
    for(std::map<std::string, std::set<std::string> >::iterator bit=m_in_busTotopics.begin(); bit!=m_in_busTotopics.end(); ++bit ){
        std::string busname = bit->first; 
        for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){       
            LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*topicit); 
            try{
                this->getEventingBus(busname).subscribe(*topicit);
            }catch(eventing::api::exception::Exception& e){
                LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
                m_in_busTotopics[busname].erase(*topicit);
            }
        }
    }
}

// Listens for reception of data on the eventing.  
// If full statistics collected, call do_process on the already-accumulated nibbles.
void bril::hfprocessor::HFProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
    debugStatement<<"In onMessage";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        curTopic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        std::string v = plist.getProperty("DATA_VERSION");
        if(v.empty()){
            std::string msg("Received data message without version header, do nothing");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            if(ref!=0){
                ref->release();
                ref=0;
            }
            return;
        }
        if(v!=interface::bril::shared::DATA_VERSION){
            std::string msg("Mismatched bril data version in received message, do nothing");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            if(ref!=0){
                ref->release();
                ref=0;
            }
            return;
        }    
    
        debugStatement<<"Received data from "+topic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

        interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());

        if( topic == m_vdmflagTopic.value_ ){
            bool flag = false;
            interface::bril::vdmflagT* me = (interface::bril::vdmflagT*)( ref->getDataLocation() );
            flag = me->payload()[0];

            debugStatement<<"cur flag in prog "<<m_vdmflag<<" flag from topic "<<flag ;
            LOG4CPLUS_DEBUG(getApplicationLogger(),debugStatement.str());
            debugStatement.str("");
            
            if( m_vdmflag==true && flag==false ){
                m_vdmflag = false;
                LOG4CPLUS_INFO(getApplicationLogger(), "TURNING OFF VDMFLAG--BUNCHMASK IS DYNAMIC");
            }else if(  m_vdmflag==false && flag==true ){
                m_vdmflag = true;
                LOG4CPLUS_INFO(getApplicationLogger(), "TURNING ON VDMFLAG--BUNCHMASK IS FIXED");
            }
        }
        
        if (topic == interface::bril::beamT::topicname() )
        {
            std::stringstream ss;
            ss << "Received data from " + topic + ", making bunch masks";
            LOG4CPLUS_DEBUG (getApplicationLogger(), ss.str() );
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
            
            std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
            interface::bril::shared::CompoundDataStreamer tc(payload_dict);
                
            tc.extract_field(m_iscolliding1, "bxconfig1", inheader->payloadanchor);
            tc.extract_field(m_iscolliding2, "bxconfig2", inheader->payloadanchor);
        }


        if( topic == interface::bril::hfCMS1T::topicname() ){
            if (lumiAlgo == "occupancy") {
                RetrieveSourceHistos(ref, inheader);
            } else {
                LOG4CPLUS_ERROR(getApplicationLogger(),"CMS1 topic found but you aren't running occupancy lumi.");
            }
        } else if( topic == interface::bril::hfCMS_ETT::topicname()) {
            if (lumiAlgo == "etsum") {
                RetrieveSourceHistos(ref, inheader);
            } else {
                LOG4CPLUS_ERROR(getApplicationLogger(),"CMS_ET topic found but you aren't running etsum lumi.");
            }
        } else if( topic == interface::bril::hfCMS_VALIDT::topicname() ) {
            RetrieveSourceHistos(ref, inheader);
        }
    }
    if(ref!=0){
        ref->release();
        ref=0;
    }
}

// When HF data received on eventing, check and store it
void bril::hfprocessor::HFProcessor::RetrieveSourceHistos(toolbox::mem::Reference * ref, interface::bril::shared::DatumHead * inheader) {
    // assume data is guilty until proven innocent
    saneData = false;
    newLumiBlock = false;

    debugStatement<<"START:  NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    unsigned int crate     = inheader->getDataSourceID();
    unsigned int board     = inheader->getChannelID();
    unsigned int boardID   = crate*100+board;
    unsigned int boardIndex = (board - 1) + (crate==32)*24 + (crate==29)*12;
    debugStatement<<"crate, board, boardID "<<crate<<","<<board<<","<<boardID;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    unsigned int runnum = inheader->runnum;
    unsigned int lsnum = inheader->lsnum;
    unsigned int nbnum = inheader->nbnum;
    debugStatement<<"Received data from run "<<runnum<<" ls "<<lsnum<<" nb "<<inheader->nbnum<<" boardID "<<boardID;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    std::string topic;


    // Check if we have collected all channels for all nibbles
    // If so, then process the lot -- starting at last_header
    // (NOT current header!)
   
    debugStatement<< "process last header: run " << prevDataHeader.runnum 
               << " ls " << prevDataHeader.lsnum 
               << " nb " << prevDataHeader.nbnum
               << " (right now in run) " << runnum 
               << " ls " << lsnum 
               << " nb " << nbnum 
               << " Lumi: " << lumiTotal 
               << " Lumi raw: " << muTotal;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");



    // FIXME add consistency checks here

    // CP HACK to make lumi nibble line up.
    // MUST DO BEFORE ANALYZING
    debugStatement<<"making the 4nib line up "<<integration_period_nb.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    if(integration_period_nb.value_>1){
        int modnib=inheader->nbnum % integration_period_nb.value_;
        m_mon_nbMod4PerBoard[boardIndex]=m_mon_nbMod4PerBoard[boardIndex]+modnib; 
        debugStatement<<"nib "<<inheader->nbnum<<" int period "<<integration_period_nb.value_<<" mod "<<modnib;
        if(modnib!=0){
            // always advance
            inheader->nbnum=inheader->nbnum+(integration_period_nb.value_-modnib);
            //democratic
            ////float offsetFrac = (float)modnib / integration_period_nb.value_;
            ////if(offsetFrac>0.5){
            ////    inheader->nbnum=inheader->nbnum+(integration_period_nb.value_-modnib);
            ////} else {
            ////    inheader->nbnum=inheader->nbnum-(modnib);
            ////}
            if(inheader->nbnum==0) {
                inheader->nbnum=64;
                inheader->lsnum-=1;
                debugStatement<<" new LS "<<inheader->lsnum;
            }
            debugStatement<<" new nib "<<inheader->nbnum;
        }
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        runnum = inheader->runnum;
        lsnum = inheader->lsnum;
        nbnum = inheader->nbnum;
    }


    //SanityChecks
    if(runnum==prevDataHeader.runnum&&lsnum==prevDataHeader.lsnum&&nbnum==prevDataHeader.nbnum){
        saneData=true;
    } else if((runnum-prevDataHeader.runnum)>0){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New run");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==1)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New LS");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum>1)) {
        debugStatement<<"New LS - "<<lsnum-prevDataHeader.lsnum<<" LSs later";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        newLumiBlock = true;
        saneData=true;

    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==0)&&(nbnum-prevDataHeader.nbnum==4)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New block, same run,ls - 4ln later");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==0)&&(nbnum-prevDataHeader.nbnum>4)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(),"New block, same run,ls - more than 4ln later");
        newLumiBlock = true;
        saneData=true;
    } else {
        std::stringstream message;
        message<<"INSANE DATA!!\n";
        message<<"\t PREV run,ls,ln "<<prevDataHeader.runnum<<","<<prevDataHeader.lsnum<<","<<prevDataHeader.nbnum<<"\n";
        message<<"\t THIS run,ls,ln "<<runnum<<","<<lsnum<<","<<nbnum<<"\n";
        message<<"\t THIS boardID "<<boardID;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());
    }

    //if(runnum==lastPubRun&&lsnum==lastPubLS&&nbnum==lastPubLN){
    double thisLN=runnum*1e7+lsnum*1e2+nbnum;
    if(thisLN<=lastPub){
        debugStatement.precision(13);
        debugStatement<<"Last published "<<lastPub<<"  This nibble "<<thisLN
                <<"  Diff "<<lastPub-thisLN<<" dropping this histogram";
        debugStatement<<"  crate, board  "<<crate<<","<<board;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.precision(3);
        debugStatement.str("");
        saneData=false;
    }
    
    timeNow = toolbox::TimeVal::gettimeofday();
    double timeInProcessor=(double)timeNow.sec()+(double)timeNow.millisec()/1000.;
    double timeInHeader=(double)inheader->timestampsec+(double)inheader->timestampmsec/1000.;
    if(timeInProcessor-timeInHeader>0.3){
        debugStatement.precision(4);
        debugStatement<<"Larger than 0.2 s between InProcessor InSource Diff/InBus "<<timeInProcessor<<" "<<timeInHeader<<" "<<timeInHeader-timeInProcessor<<" "<<curTopic;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.precision(3);
        debugStatement.str("");
    }
    if(!saneData) return;

    if(newLumiBlock&&NBoardsCollected>0) {
        computeLumiNow=true;
    }

    if(newLumiBlock){
        firstDataReceived = timeInProcessor;
        debugStatement<<"newLumiBlock:  NBoardsCollected "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        //rejectHistUntilNewLN = false;
    } 


    if(computeLumiNow){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "found new lumi block-going to send lumi");
        do_process(prevDataHeader);
        computeLumiNow=false;
    }


    // Only proceed if the received topic if it is the raw (occ/etsum) hist or the validity hist
    if( (curTopic == interface::bril::hfCMS1T::topicname() && lumiAlgo == "occupancy") 
     || (curTopic == interface::bril::hfCMS_ETT::topicname() &&lumiAlgo == "etsum") ){
        debugStatement<<"FOUND raw hist topic "<<curTopic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        std::map<unsigned int,toolbox::mem::Reference*>::iterator  boardIter = hfRawHist.find(boardID);
        if(boardIter==hfRawHist.end()) {
            debugStatement<<"don't have the hist. add to list";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
            memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
            hfRawHist.insert(std::make_pair(boardID,myref));
            boardAvailableThisBlock[boardIndex]=boardAvailableThisBlock[boardIndex]+1;
            timeHist[boardIndex]=timeInProcessor;
        } else {
            LOG4CPLUS_INFO(getApplicationLogger(), "already got it -- this should not happen -- I should have sent lumi and reset.");
            return;
        }
    }
    
    if( curTopic == interface::bril::hfCMS_VALIDT::topicname() ) {
        debugStatement<<"FOUND hfCMS_VALIDT topic "<<curTopic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        std::map<unsigned int,toolbox::mem::Reference*>::iterator  boardIter = hfValidHist.find(boardID);
        if(boardIter==hfValidHist.end()) {
            debugStatement<<"don't have the hist. add to list";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
            memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
            hfValidHist.insert(std::make_pair(boardID,myref));
            boardAvailableThisBlock[boardIndex]=boardAvailableThisBlock[boardIndex]+2;
            timeValidHist[boardIndex]=timeInProcessor;
        } else {
            LOG4CPLUS_INFO(getApplicationLogger(), "already got it -- this should not happen -- I should have sent lumi and reset.");
            return;
        }
    }
   
    if(boardAvailableThisBlock[boardIndex]==3){
        NBoardsCollected++;
        debugStatement<<"NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if(NBoardsCollected==36){
            debugStatement<<"all boards collected--going to send lumi";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            do_process(prevDataHeader);
        }    
    }
    
    timeNow = toolbox::TimeVal::gettimeofday();
    lastDataAcquired = timeNow.sec();
    prevDataHeader = *inheader;

    debugStatement<<"END:  NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
}

// Process the accumulated nibbles:
// Calls functions that create and queue for publishing the following products
//  * agghists
//  * luminosity numbers
//  * BX mask
void bril::hfprocessor::HFProcessor::do_process(interface::bril::shared::DatumHead& inheader){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Now entered do_process: VERY BEGINNING");
    
    std::stringstream ss;
    ss.str("");
    ss << "do_process run " << inheader.runnum 
       << " ls " << inheader.lsnum
       << " nb " <<inheader.nbnum
       << " channel " << inheader.getChannelID();
    LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    
    
    debugStatement<<"Total:  "<<NBoardsCollected<<" \n";
    debugStatement<<" run " << prevDataHeader.runnum 
       << " ls " << prevDataHeader.lsnum 
       << " nb " << prevDataHeader.nbnum<<"\n";
    debugStatement<<"Missing ";
    for(int thisInd=0; thisInd<36; thisInd++){
        if(boardAvailableThisBlock[thisInd]!=3){
            debugStatement<<thisInd<<" "<<boardAvailableThisBlock[thisInd]<<" ";
        }
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // 1. first make agghists, using same loop calculate total rates
    LOG4CPLUS_DEBUG(getApplicationLogger(), "About to SumHists");
    debugStatement<<"1) read histograms into local memory\n";
    debugStatement<<"  a) all boards merged \n";
    debugStatement<<"  b) each board \n";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    bool success=SumHists(inheader);
    debugStatement<<"  c) clear cache of hists \n";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    if(!success) {
        ClearCache();
        return;
    }
    
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "2) compute/publish data");
    ComputeLumi(inheader);
    lastPubRun=inheader.runnum;
    lastPubLS=inheader.lsnum;
    lastPubLN=inheader.nbnum;
    lastPub=lastPubRun*1e7+lastPubLS*1e2+lastPubLN;
    
    ClearCache();
}

// Loops  through  uHTR histograms,   makes the agghists for  each c hannel
// and at the same time creates total rates for each channel for monitoring 
// purposes and) making the channel mask. 
//
// Queues the agghists for publishing -->

bool bril::hfprocessor::HFProcessor::SumHists(interface::bril::shared::DatumHead& inheader) {
    // assume failure until success
    bool success=false;
    
    memset(aggHistPerBoardPerBX  ,0,sizeof(aggHistPerBoardPerBX));
    memset(aggValidHistPerBoardPerBX,0,sizeof(aggValidHistPerBoardPerBX));
    memset(muHistPerBoardPerBX   ,0,sizeof(muHistPerBoardPerBX));
    memset(lumiHistPerBoardPerBX ,0,sizeof(lumiHistPerBoardPerBX));
    memset(lumiHistPerBoard      ,0,sizeof(lumiHistPerBoard));
    memset(aggHistPerBX          ,0,sizeof(aggHistPerBX));
    memset(aggValidHistPerBX        ,0,sizeof(aggValidHistPerBX));
    memset(muHistPerBX           ,0,sizeof(muHistPerBX));
    memset(lumiHistPerBX         ,0,sizeof(lumiHistPerBX));
      
    if(!m_vdmflag){
        memset(activeBXMask         ,false,sizeof(activeBXMask));
        NActiveBX=0;
        maxBX=0;
        firstBX=-1;
    }
    
    size_t bxbins = interface::bril::shared::MAX_NBX;

    try{
        debugStatement<<"size of hfRawHist "<<hfRawHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if(hfRawHist.size()==0){
            LOG4CPLUS_INFO(getApplicationLogger(), "no data?!  ugh i'm done (SumHists)");
            return success;
        }
        
        // loop over occupancy histograms; 
        // ensure there is a corresponding valid histogram;
        // extract primitive data; 
        // sum up per BX over all boards
        // save which boards were fully functional
        for(std::map<unsigned int, toolbox::mem::Reference*>::iterator boardIter=hfRawHist.begin(); boardIter!=hfRawHist.end(); ++boardIter){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Accessing this channel of histcache");
            
            unsigned int channelid = boardIter->first;
            unsigned int crate = channelid/100;
            unsigned int board = channelid-crate*100;
            unsigned int boardIndex = (board - 1) + (crate==32)*24 + (crate==29)*12;
    
            if(crate!=22&&crate!=29&&crate!=32){
                debugStatement<<"crate is not 22,29,32 "<<crate;
                LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
            }
    
            std::map<unsigned int, toolbox::mem::Reference*>::iterator validIter=hfValidHist.find(channelid);
            if(validIter==hfValidHist.end()){
                debugStatement<<"crate, board, boardIndex "<<crate<<" "<<board<<" "<<boardIndex;
                debugStatement<<"\nThere is no valid histogram for this occupancy hist... skipping it.";
                LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                continue;
            }
            
            toolbox::mem::Reference* histref = boardIter->second;
            interface::bril::hfCMS1T* histdataptr = (interface::bril::hfCMS1T*)(histref->getDataLocation());
            
            toolbox::mem::Reference* validref = validIter->second;
            interface::bril::hfCMS_VALIDT* validdataptr = (interface::bril::hfCMS_VALIDT*)(validref->getDataLocation());
            
            // extract primitive data; 
            // sum up per BX over all boards
            for(size_t bxid=0; bxid<bxbins; bxid++){
                aggHistPerBoardPerBX[boardIndex][bxid]+=histdataptr->payload()[bxid];
                aggValidHistPerBoardPerBX[boardIndex][bxid]+=validdataptr->payload()[bxid];
                aggHistPerBX[bxid]+=aggHistPerBoardPerBX[boardIndex][bxid];
                aggValidHistPerBX[bxid]+=aggValidHistPerBoardPerBX[boardIndex][bxid];
            }
            
            for(size_t bxid=0; bxid<bxbins; bxid++){
                if(aggValidHistPerBX[bxid]>0){
                    aggNormHistPerBX[bxid]=aggHistPerBX[bxid]/aggValidHistPerBX[bxid];
                } else {
                    aggNormHistPerBX[bxid]=0;
                }
            }
            if(boardAvailableThisBlock[boardIndex]!=3){
                std::stringstream message;
                message<<"I have saved the occ/valid hist for "<<boardIndex<<" but boardAvailableThisBlock is "<<boardAvailableThisBlock[boardIndex]; 
                LOG4CPLUS_INFO(getApplicationLogger(),message.str());
            }
            success=true;
        }

        //format aggregate hist
        
        if (lumiAlgo.value_ == "occupancy") {
            histBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfOcc1AggT::maxsize());
            histBufRef->setDataSize(interface::bril::hfOcc1AggT::maxsize());        
        } else if (lumiAlgo.value_ == "etsum") {
            histBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfEtSumAggT::maxsize());
            histBufRef->setDataSize(interface::bril::hfEtSumAggT::maxsize());
        }        

        interface::bril::shared::DatumHead* aggHistHeader= (interface::bril::shared::DatumHead*)(histBufRef->getDataLocation());
        aggHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        aggHistHeader->setResource(interface::bril::shared::DataSource::HF,0,0,interface::bril::shared::StorageType::FLOAT);

        if (lumiAlgo.value_ == "occupancy") {
            aggHistHeader->setTotalsize(interface::bril::hfOcc1AggT::maxsize());
        } else if (lumiAlgo.value_ == "etsum") {
            aggHistHeader->setTotalsize(interface::bril::hfEtSumAggT::maxsize());
        }

        aggHistHeader->setFrequency(integration_period_nb.value_);

        memcpy(aggHistHeader->payloadanchor,aggNormHistPerBX,(interface::bril::shared::MAX_NBX)*sizeof(float));

        QueueStoreIt it = m_topicoutqueues.end();

        if (lumiAlgo.value_ == "occupancy") {
            it=m_topicoutqueues.find(interface::bril::hfOcc1AggT::topicname());
        } else if (lumiAlgo.value_ == "etsum") {
            it=m_topicoutqueues.find(interface::bril::hfEtSumAggT::topicname());
        }                

        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this agghist to output queue");
            it->second->push(histBufRef); //push agghist to publishing queue
        } else if(histBufRef) { 
            LOG4CPLUS_INFO(getApplicationLogger(),"histBufRef topic is not in topic queue... releasing memory");
            histBufRef->release(); 
            histBufRef= 0;
        }        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghist");
    } catch(xcept::Exception& e) {
        if(histBufRef) { histBufRef->release(); histBufRef= 0;  }
        std::string msg("Failed to process data for agghist");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
    }

    return success;
}



// Calculates the luminosity numbers (full-orbit values 
// and bunch-by-bunch histograms), 
// and queues them for publishing
void bril::hfprocessor::HFProcessor::ComputeLumi(interface::bril::shared::DatumHead& inheader){
    // NOOOOOWWWW for luminosity numbers
    
    toolbox::mem::Reference* lumiHistBufRef = 0;
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Still in do_process: lumiHistBufRef");  
    try{
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::hfoclumiT::payloaddict();
        if (lumiAlgo.value_ == "etsum") {
          pdict = interface::bril::hfetlumiT::payloaddict();
        }
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        // END COMPOUND-SPECIFIC STUFF
    

        //format aggregate hist
        toolbox::mem::Reference * lumiHistBufRef = 0;
        
        if (lumiAlgo.value_ == "occupancy") {
            lumiHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfoclumiT::maxsize());
            lumiHistBufRef->setDataSize(interface::bril::hfoclumiT::maxsize());        
        } else if (lumiAlgo.value_ == "etsum") {
            lumiHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfetlumiT::maxsize());
            lumiHistBufRef->setDataSize(interface::bril::hfetlumiT::maxsize());
        }
    
        // fill, run, ls, ln, sec, msec
        //interface::bril::hfoclumiT* lumihist = (interface::bril::hfoclumiT*)(lumiHistBufRef->getDataLocation());
        interface::bril::shared::DatumHead* lumiHistHeader= (interface::bril::shared::DatumHead*)(lumiHistBufRef->getDataLocation());
        lumiHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        lumiHistHeader->setFrequency(integration_period_nb.value_);
    
        // sourceid, algo, channel, payloadtype
        lumiHistHeader->setResource(interface::bril::shared::DataSource::HF,0,0,interface::bril::shared::StorageType::COMPOUND);

        if (lumiAlgo.value_ == "occupancy") {
            lumiHistHeader->setTotalsize(interface::bril::hfoclumiT::maxsize());
        } else if (lumiAlgo.value_ == "etsum") {
            lumiHistHeader->setTotalsize(interface::bril::hfetlumiT::maxsize());    
        }
    
        // resetting values of global variables to zero
        muTotal = 0;
        lumiTotal = 0;
    
        unsigned int maskhigh = 0; // board 32
        unsigned int masklow = 0; // boards 22 and 29
    
        for(int boardInd=0; boardInd<36; boardInd++){
            for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
                float ratio=aggHistPerBoardPerBX[boardInd][bxid];
                if( aggValidHistPerBoardPerBX[boardInd][bxid] > 0 ){
                    ratio=ratio/aggValidHistPerBoardPerBX[boardInd][bxid];
                    if (lumiAlgo.value_ == "occupancy") {
                        muHistPerBoardPerBX[boardInd][bxid]=-log(1-ratio);
                    } else if (lumiAlgo.value_ == "etsum") {
                        muHistPerBoardPerBX[boardInd][bxid]=ratio;
                    }
                } else {
                   ratio=0;
                }
            }
        }

        // compute mu's first
        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            float ratio=aggHistPerBX[bxid];
            if( aggValidHistPerBX[bxid] > 0 ){
                ratio=ratio/aggValidHistPerBX[bxid];
                if (lumiAlgo.value_ == "occupancy") {
                    muHistPerBX[bxid]=-log(1-ratio);
                } else if (lumiAlgo.value_ == "etsum") {
                    muHistPerBX[bxid]=ratio;
                }
            } else {
                ratio=0;
            }
        }
   
        // if scan is happening, lock the mask--don't change it
        if(useCustomBXMask) {
            setMask();
        } else {
            if(!m_vdmflag){
                MakeDynamicBXMask(inheader);
            }
        }
        // correct mu's for afterglow
        if(applyAfterglowCorr.value_){
            ComputeAfterglow(inheader);
        }
        
        if (applyPedestalCorr.value_) {
            if(lumiAlgo.value_ == "etsum"){
                SubtractPedestal(inheader);
            } else if (lumiAlgo.value_ == "occupancy") {
                SubtractPedestal(inheader);
            }
        }

        // compute sum of mu's (post correction)
        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){

		    debugStatement<<" bxid "<<bxid<< " isActive  "<<activeBXMask[bxid]<<" PostCorrection "<<muHistPerBX[bxid];
	            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        	    debugStatement.str("");
            if(activeBXMask[bxid]){
                muTotal+=muHistPerBX[bxid];
            }
        }
        
        
        // compute lumi with corrected mu's (if afterglow corr is on)
        for(int boardInd=0; boardInd<36; boardInd++){
            for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
                lumiHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid]*11245.6/sigmaVis;
                

                // non-linearity correction
                if(activeBXMask[bxid]){
                    lumiHistPerBoardPerBX[boardInd][bxid]=lumiHistPerBoardPerBX[boardInd][bxid]+quadraticCorrection*lumiHistPerBoardPerBX[boardInd][bxid]*lumiHistPerBoardPerBX[boardInd][bxid];
                }
            }
        }

        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            lumiHistPerBX[bxid]=(muHistPerBX[bxid])*11245.6/sigmaVis;
            // non-linearity correction
            if(activeBXMask[bxid]){
                lumiHistPerBX[bxid]= lumiHistPerBX[bxid]+quadraticCorrection*lumiHistPerBX[bxid]*lumiHistPerBX[bxid];
            }
        }


        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            if(activeBXMask[bxid]){
                lumiTotal+=lumiHistPerBX[bxid];
                for(int boardInd=0; boardInd<36; boardInd++){
                    if(boardAvailableThisBlock[boardInd]==3){
                        lumiHistPerBoard[boardInd]+=lumiHistPerBoardPerBX[boardInd][bxid];
                    }
                }
            }
        }
        
        // masklow is for presence of first two crates
        // maskhigh is for the third crate
        for(int boardInd=0; boardInd<36; boardInd++){
            if(boardAvailableThisBlock[boardInd]==3){
                m_mon_lumiPerBoard[boardInd]= m_mon_lumiPerBoard[boardInd]+lumiHistPerBoard[boardInd];
                if(boardInd<24){
                    masklow  += 2^(boardInd);
                } else {
                    maskhigh += 2^(boardInd-24);
                }
            }
        }

        // Make sure lumi is above zero before publishing (useful in case of custom mask) to avoid confusion to OMS people
        if (lumiTotal < 0) {
            lumiTotal = 0;
        }
      
        timeNow = toolbox::TimeVal::gettimeofday();
        unsigned int now = timeNow.sec();
        if(now-lastMessageTime>90){
            debugStatement<<"fill,run,LS,LN "<<inheader.fillnum<<","<<inheader.runnum<<","<<inheader.lsnum<<","<<inheader.nbnum
              <<" NActiveBX "<<NActiveBX
              <<" NBoardsCollected "<<NBoardsCollected
              <<" hfAfterGlowTotalScale "<<hfAfterGlowTotalScale
              <<" lumi combined "<<lumiTotal<<" per board ";
            for(int boardInd=0; boardInd<36; boardInd++){
                if(boardAvailableThisBlock[boardInd]==3){
                    debugStatement<<boardInd<<" "<<lumiHistPerBoard[boardInd]<<" ";
                }else{
                    debugStatement<<boardInd<<" N/A ";
                }
            }
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            lastMessageTime=now;
        }
        m_mon_fill = inheader.fillnum;
        m_mon_run  = inheader.runnum;
        m_mon_ls   = inheader.lsnum;
        m_mon_nb   = inheader.nbnum;
        m_mon_timestamp = timeNow;
        m_mon_nActiveBX = m_mon_nActiveBX+NActiveBX;
        m_mon_nBoards   = m_mon_nBoards  +NBoardsCollected;
        m_mon_nCollected++;
       
        if(inheader.lsnum!=lastPubLS){ // monitor every new LS
            //average over the m_mon_nCollected
            m_mon_nActiveBX=m_mon_nActiveBX/m_mon_nCollected;
            m_mon_nBoards=m_mon_nBoards/m_mon_nCollected;
            for(int index=0;index<36; index++){
                m_mon_nbMod4PerBoard[index]=m_mon_nbMod4PerBoard[index]/m_mon_nCollected;
                m_mon_lumiPerBoard[index]=m_mon_lumiPerBoard[index]/m_mon_nCollected;
            }
            //send data
            m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
            //reset everything
            m_mon_nActiveBX = 0;
            m_mon_nBoards   = 0;
            m_mon_nbMod4PerBoard.resize(36, -1);
            m_mon_lumiPerBoard.resize(36, 0);
            m_mon_bunchMask.resize(interface::bril::shared::MAX_NBX,0);
            m_mon_nCollected=0;
        }

        std::stringstream ss;
        ss.str("");
        ss << "Avg lumi: " << lumiTotal << "  Avg lumi raw: " << muTotal;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    
        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::shared::CompoundDataStreamer tc(pdict);       
        tc.insert_field( lumiHistHeader->payloadanchor, "calibtag", m_calibtag.value_.c_str());
        tc.insert_field( lumiHistHeader->payloadanchor, "avgraw",   &muTotal);
        tc.insert_field( lumiHistHeader->payloadanchor, "avg",      &lumiTotal);
        tc.insert_field( lumiHistHeader->payloadanchor, "bxraw",    muHistPerBX);
        tc.insert_field( lumiHistHeader->payloadanchor, "bx",       lumiHistPerBX);
        tc.insert_field( lumiHistHeader->payloadanchor, "masklow",  &masklow);
        tc.insert_field( lumiHistHeader->payloadanchor, "maskhigh", &maskhigh);

        // END MORE COMPOUND-SPECIFIC STUFF
        QueueStoreIt it = m_topicoutqueues.end();

        if (lumiAlgo.value_ == "occupancy") {
            it=m_topicoutqueues.find(interface::bril::hfoclumiT::topicname());
        } else if (lumiAlgo.value_ == "etsum") {
            it=m_topicoutqueues.find(interface::bril::hfetlumiT::topicname());
        }

        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "pushing lumi to queueTOPICS");
            it->second->push(lumiHistBufRef);
            LOG4CPLUS_DEBUG(getApplicationLogger(), "pushing lumi to queueTOPICS - done");
        }      
        else
        {
            if(lumiHistBufRef) { lumiHistBufRef->release(); lumiHistBufRef= 0;  }
        }
        //m_topicoutqueues[interface::bril::hfoclumiT::topicname()]->push(lumiHistBufRef);
    } catch(xcept::Exception& e){
        std::string msg("Failed to process data for lumiHistBufRef");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
        if(lumiHistBufRef) {
            lumiHistBufRef->release();
            lumiHistBufRef= 0;
        }
    }
}


void bril::hfprocessor::HFProcessor::MakeDynamicBXMask(interface::bril::shared::DatumHead& inheader, float fracThreshold){
    
    float minv=1;
    float maxv=0;
    NActiveBX=0;
    maxBX=0;
    firstBX=-1;

    for(int bxind=0; bxind<int(MAX_NBX_noAG); bxind++){


        float thisRatio=0;
        if (lumiAlgo.value_ == "occupancy") {
            thisRatio=aggHistPerBX[bxind];
            if(aggValidHistPerBX[bxind]>0){
                thisRatio=thisRatio/aggValidHistPerBX[bxind];
            } else {
                thisRatio=0;
            }
        } else if (lumiAlgo.value_ == "etsum") {
            thisRatio=muHistPerBX[bxind];
        }
        //only look at non-zero BXs
        if(minv>thisRatio && thisRatio!=0) minv=thisRatio;
        if(maxv<thisRatio) {
            maxv=thisRatio;
            maxBX=bxind;
        }
    }

    // if still 1, all bx are 0
    if(minv==1) minv=0;

    // minimum value of min is 1e-6
    float abMin=BMmin;
    int MaskLow = int(HCALMaskLow);
    int MaskHigh = int(HCALMaskHigh);

        //if (lumiAlgo.value_ == "etsum") abMin=1e-2;
    if (lumiAlgo.value_ == "etsum") abMin=BMthresET;
    if (lumiAlgo.value_ == "occupancy") abMin=BMthresOC;

    minv=std::max(minv,abMin);
    
    //make fraction dynamic
    //at very low lumi, need to distinguish more from noise... higher frac of diff
    //need smooth transition to normal fraction
    float nearNoise=NearNoiseValue;
    float aboveNoise=AboveNoiseValue;
    if (lumiAlgo.value_ == "etsum") {
        nearNoise=0.02;
        aboveNoise=0.05;
    }
    if( maxv-minv<nearNoise) fracThreshold=1.;
    else if(maxv<aboveNoise) fracThreshold= (aboveNoise-maxv+minv)/(aboveNoise-nearNoise) + (maxv-minv-nearNoise)*fracThreshold / (aboveNoise-nearNoise);

    float dynamicThreshold=std::max((maxv-minv)*fracThreshold,abMin);
    //float dynamicThreshold=(max-minv)*fracThreshold;
    debugStatement<<"minv, maxv, fracT, dynT "<<minv<<" "<<maxv<<" "<<fracThreshold<<" "<<dynamicThreshold;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    //if the difference between max and min is small compared to noise, then it is noise
    if(maxv-minv > 0.5*minv) {
        for(int bxind=0; bxind<interface::bril::shared::MAX_NBX; bxind++){
            float thisRatio=0;
	    bool MaskLED = bxind < MaskLow || bxind > MaskHigh;

            debugStatement<<"bxind  "<<bxind<<" MaskedLED  "<<MaskLED<<" Low "<<MaskLow<<" High "<<MaskHigh;
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");

            if (lumiAlgo.value_ == "occupancy") {
                thisRatio=aggHistPerBX[bxind];
                if(aggValidHistPerBX[bxind]>0){
                    thisRatio=thisRatio/aggValidHistPerBX[bxind];
                } else {
                    thisRatio=0;
                }
            } else if (lumiAlgo.value_ == "etsum") {
                thisRatio=muHistPerBX[bxind];
            }
    
            if( thisRatio-minv > dynamicThreshold && MaskLED) {
                debugStatement<<"passing threshold "<<bxind<<" "<<thisRatio<<" "<<minv;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                activeBXMask[bxind]=true;
                m_mon_bunchMask[bxind]=m_mon_bunchMask[bxind]+1;
                NActiveBX++;
                if(firstBX==-1) firstBX=bxind;
            }
        }
    }
    debugStatement<<"NActiveBX "<<NActiveBX;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

        std::stringstream message;
       message<<"================ CHECK THIS !!!!! NActiveBX "<<NActiveBX<<"  BunchMask Thershold  hfet "<<BMthresET<<" hfoc  "<<BMthresOC;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());

    try{
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::hfoclumiT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        // END COMPOUND-SPECIFIC STUFF
        
        //format bx mask hist
        bxBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::bunchmaskT::maxsize());
        bxBufRef->setDataSize(interface::bril::bunchmaskT::maxsize());

        interface::bril::shared::DatumHead* maskHistHeader= (interface::bril::shared::DatumHead*)(bxBufRef->getDataLocation());
        maskHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        maskHistHeader->setResource(interface::bril::shared::DataSource::HF,0,0,interface::bril::shared::StorageType::COMPOUND);
        maskHistHeader->setTotalsize(interface::bril::bunchmaskT::maxsize());
        maskHistHeader->setFrequency(integration_period_nb.value_);

        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::shared::CompoundDataStreamer tc(pdict);       
        tc.insert_field( maskHistHeader->payloadanchor, "ncollidingbx", &NActiveBX);
        tc.insert_field( maskHistHeader->payloadanchor, "firstlumibx",  &firstBX);
        tc.insert_field( maskHistHeader->payloadanchor, "maxlumibx",    &maxBX);
        tc.insert_field( maskHistHeader->payloadanchor, "bxmask",       activeBXMask);
        // END MORE COMPOUND-SPECIFIC STUFF
        QueueStoreIt it = m_topicoutqueues.find( interface::bril::bunchmaskT::topicname() );
        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this bx mask to output queue");
            it->second->push(bxBufRef); //push agghist to publishing queue
        } else if(bxBufRef) {
            LOG4CPLUS_INFO(getApplicationLogger(),"release bxref after assigning because topic not in queue's list");
            bxBufRef->release();
            bxBufRef= 0;
        }
        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghist");
    } catch(xcept::Exception& e){
        std::string msg("Failed to process data for lumiHistBufRef");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
        if(bxBufRef) {
            LOG4CPLUS_INFO(getApplicationLogger(),"release bxref in exception");
            bxBufRef->release();
            bxBufRef= 0;
        }
    }
}

void bril::hfprocessor::HFProcessor::ComputeAfterglow(interface::bril::shared::DatumHead& inheader){

    debugStatement<<"Begin afterglow calculation";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str() );
    debugStatement.str("");
    
    //First apply SBR on mu per BX
    //Compute the hf afterglow with corrected mu / uncorrected mu

    memset(muUncorrectedPerBX,   0, sizeof(muUncorrectedPerBX));
    memset(hfafterglowPerBX,     0, sizeof(hfafterglowPerBX));

    afterglowBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfafterglowfracT::maxsize());
    afterglowBufRef->setDataSize(interface::bril::hfafterglowfracT::maxsize());
    
    interface::bril::shared::DatumHead* afterglowHeader = (interface::bril::shared::DatumHead*)(afterglowBufRef->getDataLocation());
    afterglowHeader->setTime(inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
    afterglowHeader->setResource(interface::bril::shared::DataSource::HF, 0, 0, interface::bril::shared::StorageType::FLOAT);
    afterglowHeader->setTotalsize(interface::bril::hfafterglowfracT::maxsize());


    float noise = 0;
    int num_cut = 20;
    float idl = 0;
    
    debugStatement<<"The luminosity before correction is: ";
    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        muUncorrectedPerBX[bxid] = muHistPerBX[bxid];
        debugStatement<<", "<<muHistPerBX[bxid];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // modified afterglow model
    int type(0);
    float SBR(0);
    // For HFET only. TODO: add for HFOC
    float type1[3] = {0.00531323, 3.36155061e-04, 3.07006010e-05};
    float quadratic_type1[3] = {-0.00033145, -4.46711329e-06, 2.80396483e-06};

    for(int ibx=0; ibx<interface::bril::shared::MAX_NBX; ibx++){
        if(activeBXMask[ibx]){
            type = 0;        
            for(int jbx=ibx+1; jbx<ibx+interface::bril::shared::MAX_NBX; jbx++){
                
                // Define correction
                if (type < 3) {
                    if (lumiAlgo.value_ == "etsum") {
                        SBR = (muHistPerBX[ibx]*muHistPerBX[ibx] * quadratic_type1[type]) + (muHistPerBX[ibx] * type1[type]) + HFSBR[jbx-ibx];
                    } else {
                       // Modify later for HFOC
                       SBR = HFSBR[jbx-ibx]; 
                    }
                } else {
                    SBR = HFSBR[jbx-ibx];
                }

                if(jbx<interface::bril::shared::MAX_NBX) {
                    muHistPerBX[jbx] -= muHistPerBX[ibx] * SBR;
                } else {
                    muHistPerBX[jbx-interface::bril::shared::MAX_NBX] -= muHistPerBX[ibx] * SBR;
                }

                type += 1;
            }
        }
    }

    for(int bxid=0; bxid<num_cut; bxid++){
        noise+=muHistPerBX[bxid];
        idl+=1;
    }

    noise = noise/num_cut;
    
    float totalMuUn=0;
    float totalMuCorr=0;

    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        if(muUncorrectedPerBX[bxid]!=0){
            hfafterglowPerBX[bxid]=muHistPerBX[bxid]/muUncorrectedPerBX[bxid];
            totalMuUn+=muUncorrectedPerBX[bxid];
            totalMuCorr+=muHistPerBX[bxid];
        }else{
            hfafterglowPerBX[bxid]=0;
        }
    }

    if(totalMuUn>0){
        hfAfterGlowTotalScale=totalMuCorr/totalMuUn;
    }

    debugStatement<<"The total correction factor is:  "<<hfAfterGlowTotalScale;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    debugStatement<<"The mus after correction are: ";
    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        debugStatement<<", "<<muHistPerBX[bxid];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // send off hf afterglow frac
    memcpy(afterglowHeader->payloadanchor, hfafterglowPerBX, (interface::bril::shared::MAX_NBX)*sizeof(float));
    
    QueueStoreIt it = m_topicoutqueues.find( interface::bril::hfafterglowfracT::topicname() );
    if(it!=m_topicoutqueues.end()){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing the afterglow correction histogram to output queue");
        it->second->push(afterglowBufRef);
    } else if(afterglowBufRef) {
        LOG4CPLUS_INFO(getApplicationLogger(),"release afterglow ref after assigning because topic not in queue's list");
        afterglowBufRef->release();
        afterglowBufRef= 0;
    }

    LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing afterglow hist");


    // apply hf afterglow frac to the rest of the mu's
    for(int boardInd=0; boardInd<36; boardInd++){
        for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
            muHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid]*hfafterglowPerBX[bxid];
        }
    }
}


void bril::hfprocessor::HFProcessor::SubtractPedestal(interface::bril::shared::DatumHead& inheader){
    
    memset(pedestal,     0, sizeof(pedestal));
    
    pedBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfEtPedestalT::maxsize());
    pedBufRef->setDataSize(interface::bril::hfEtPedestalT::maxsize());
    
    interface::bril::shared::DatumHead* pedHeader = (interface::bril::shared::DatumHead*)(pedBufRef->getDataLocation());
    pedHeader->setTime(inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
    pedHeader->setResource(interface::bril::shared::DataSource::HF, 0, 0, interface::bril::shared::StorageType::FLOAT);
    pedHeader->setTotalsize(interface::bril::hfEtPedestalT::maxsize());

    debugStatement<<"Pedestal: ";

    int nSample=13;
    for (int i = 0; i < 4; i++){
        for (int j = i; j < 4*nSample; j+=4) {
            pedestal[i] +=  muHistPerBX[3500 + j]/nSample;
        }
        debugStatement<<" "<<i<<" "<<pedestal[i];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    for(int bxid=0; bxid<interface::bril::shared::MAX_NBX; bxid++){
        debugStatement<<" "<<bxid<<" "<<muHistPerBX[bxid];
        muHistPerBX[bxid]=(muHistPerBX[bxid] - pedestal[bxid % 4]);
        debugStatement<<" "<<pedestal[bxid % 4]<<" "<<muHistPerBX[bxid]<<"\n";
        for(int boardInd=0; boardInd<36; boardInd++){
            muHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid] - pedestal[bxid % 4];
        }
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // send off hf pedestal
    memcpy(pedHeader->payloadanchor, pedestal, (4)*sizeof(float));
    
    QueueStoreIt it = m_topicoutqueues.find( interface::bril::hfEtPedestalT::topicname() );
    if(it!=m_topicoutqueues.end()){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing the afterglow correction histogram to output queue");
        it->second->push(pedBufRef);
    } else if(pedBufRef){
        LOG4CPLUS_INFO(getApplicationLogger(),"release pedestal ref after assigning because topic not in queue's list");
        pedBufRef->release();
        pedBufRef=0;
    }
}


// Clear the fully-accumulated histograms in preparation
// to accumulate more
void bril::hfprocessor::HFProcessor::ClearCache(){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "ClearCache");
    
    memset(boardAvailableThisBlock  ,0,sizeof(boardAvailableThisBlock));
    memset(timeValidHist            ,0,sizeof(timeValidHist          ));
    memset(timeHist                 ,0,sizeof(timeHist            ));
    NBoardsCollected=0;
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "about to clear hfRawHist");
    for(std::map<unsigned int,toolbox::mem::Reference*>::iterator it=hfRawHist.begin();it!=hfRawHist.end();++it){
        if(it->second!=0){
            it->second->release();
            it->second=0;
        }
        delete it->second;
    }
    hfRawHist.clear();

    debugStatement<<"about to clear hfValidHist";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    for(std::map<unsigned int,toolbox::mem::Reference*>::iterator it=hfValidHist.begin();it!=hfValidHist.end();++it){
        if(it->second!=0){
            it->second->release();
            it->second=0;
        }
        delete it->second;
    }
    hfValidHist.clear();
    debugStatement<<"All cleared";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
}


bool bril::hfprocessor::HFProcessor::publishing(toolbox::task::WorkLoop* wl){
    QueueStoreIt it;
    for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
        std::string topicname = it->first;
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Is there data for "+topicname);
        if(it->second->empty()) continue;
        toolbox::mem::Reference* data = it->second->pop();
    
        std::pair<TopicStoreIt,TopicStoreIt > ret = m_out_topicTobuses.equal_range(topicname);
        for(TopicStoreIt topicit = ret.first; topicit!=ret.second;++topicit){
            if(data){ 
                std::string payloaddict;
                debugStatement<<"Found topic "<<topicname<<" for publishing";
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                if(topicname=="hfOcc1Agg"){
                    payloaddict=interface::bril::hfOcc1AggT::payloaddict();
                }else if(topicname=="hfoclumi"){
                    payloaddict=interface::bril::hfoclumiT::payloaddict();
                }else if(topicname=="hfEtSumAgg"){  
                    payloaddict=interface::bril::hfEtSumAggT::payloaddict();
                }else if(topicname=="hfetlumi"){
                    payloaddict=interface::bril::hfetlumiT::payloaddict();
                }else if(topicname=="bunchmask"){
                    payloaddict=interface::bril::bunchmaskT::payloaddict();
                }else if(topicname=="hfEtPedestal"){
                    payloaddict=interface::bril::hfEtPedestalT::payloaddict();
                }else if(topicname=="hfafterglowfrac"){
                    payloaddict=interface::bril::hfafterglowfracT::payloaddict();
                } 
                else{
                    LOG4CPLUS_ERROR(getApplicationLogger(),"Wrong topic name to publish!");
                }
                do_publish(topicit->second,topicname,payloaddict,data->duplicate());
            }
        }
        if(data) {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Releasing "+topicname+" after publishing");
            data->release();
            data=0;
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Released "+topicname+" after publishing");
        }
    }

    usleep(int(integration_period_secs*1.e6/8.)); //sleep 1/8 of the length of the integration period
    return true;
}

void bril::hfprocessor::HFProcessor::do_publish(const std::string& busname,const std::string& topicname,const std::string& pdict,toolbox::mem::Reference* bufRef){
    std::stringstream msg;
    try{
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" ){
            plist.setProperty("NOSTORE","1");
        }
        msg << "publish to "<<busname<<" , "<<topicname<<", run " << prevDataHeader.runnum<<" ls "<< prevDataHeader.lsnum <<" nb "<<prevDataHeader.nbnum; 
        LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
        debugStatement<<"Publish "<<topicname<<" to "<<busname;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        this->getEventingBus(busname).publish(topicname,bufRef,plist);
        debugStatement<<"getEventingBus.publish seemed to work";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
    }catch(xcept::Exception& e){
        msg<<"Failed to publish "<<topicname<<" to "<<busname;    
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){
            bufRef->release();
            bufRef = 0;
        }
        //XCEPT_DECLARE_NESTED(bril::hfprocessor::exception::Exception,myerrorobj,msg.str(),e);
        //this->notifyQualified("fatal",myerrorobj);
    }  
}

// When nibble-histogram receiving times out, processor the received data 
// and/or clear the stale data
void bril::hfprocessor::HFProcessor::timeExpired(toolbox::task::TimerEvent& e){
    //if(prevDataHeader.runnum==0) return;//cache is clean, do nothing
    //toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    //unsigned int nowsec = t.sec();
    //if( (nowsec-lastDataAcquired)>5 ){
    //    std::stringstream ss;
    //    ss<<"Found stale cache. run "<<prevDataHeader.runnum<<" ls "<<prevDataHeader.lsnum;
    //    LOG4CPLUS_DEBUG(getApplicationLogger(), "Full statistics, process the cache ");
    //    do_process(prevDataHeader);
    //    ClearCache();
    //}else{
    //    LOG4CPLUS_INFO(getApplicationLogger(), "Insufficient statistics collected, discard the cache ");
    //    prevDataHeader.runnum = 0;
    //    prevDataHeader.lsnum = 0;
    //    //prevDataHeader.cmslsnum = 0;
    //    prevDataHeader.nbnum = 0;
    //    ClearCache();
    //}
}

void bril::hfprocessor::HFProcessor::setMask() {
    
    for (unsigned int icol = 0; icol < 3564; icol++) {
        if ( m_iscolliding1[icol] && m_iscolliding2[icol] ) m_machine_mask[icol] = true;
    }
    
    memcpy(activeBXMask, m_machine_mask, sizeof(bool)*interface::bril::shared::MAX_NBX);
}

void bril::hfprocessor::HFProcessor::InitializeHFSBR()
{ 
  float sbr_oc[interface::bril::shared::MAX_NBX] = {1.0000000000, 0.0110200000, 0.0005661180, 0.0005600916, 0.0005541410, 0.0005482654, 0.0005424637, 0.0005367351, 0.0005310786, 0.0005254932, 0.0005199782, 0.0005145325, 0.0005091554, 0.0005038460, 0.0004986033, 0.0004934267, 0.0004883153, 0.0004832683, 0.0004782850, 0.0004733646, 0.0004685063, 0.0004637096, 0.0004589738, 0.0004542982, 0.0004496824, 0.0004451256, 0.0004406275, 0.0004361876, 0.0004318055, 0.0004274809, 0.0004232136, 0.0004190033, 0.0004148500, 0.0004107537, 0.0004067143, 0.0004027321, 0.0003988073, 0.0003949402, 0.0003911314, 0.0003873813, 0.0003836905, 0.0003800600, 0.0003764904, 0.0003729826, 0.0003695378, 0.0003661569, 0.0003628409, 0.0003595911, 0.0003564085, 0.0003532942, 0.0003502491, 0.0003472744, 0.0003443706, 0.0003415386, 0.0003387787, 0.0003360911, 0.0003334758, 0.0003309324, 0.0003284601, 0.0003260579, 0.0003237242, 0.0003214569, 0.0003192537, 0.0003171116, 0.0003150272, 0.0003129966, 0.0003110154, 0.0003090788, 0.0003071816, 0.0003053182, 0.0003034826, 0.0003016686, 0.0002998697, 0.0002980794, 0.0002962911, 0.0002944983, 0.0002926943, 0.0002908731, 0.0002890287, 0.0002871555, 0.0002852485, 0.0002833031, 0.0002813154, 0.0002792821, 0.0002772009, 0.0002750699, 0.0002728881, 0.0002706555, 0.0002683726, 0.0002660408, 0.0002636624, 0.0002612402, 0.0002587776, 0.0002562789, 0.0002537486, 0.0002511917, 0.0002486137, 0.0002460202, 0.0002434171, 0.0002408101, 0.0002382053, 0.0002356084, 0.0002330251, 0.0002304607, 0.0002279204, 0.0002254088, 0.0002229304, 0.0002204889, 0.0002180879, 0.0002157303, 0.0002134185, 0.0002111545, 0.0002089398, 0.0002067756, 0.0002046623, 0.0002026001, 0.0002005890, 0.0001986284, 0.0001967175, 0.0001948552, 0.0001930402, 0.0001912710, 0.0001895460, 0.0001878635, 0.0001862216, 0.0001846186, 0.0001830524, 0.0001815212, 0.0001800231, 0.0001785565, 0.0001771194, 0.0001757102, 0.0001743274, 0.0001729694, 0.0001716348, 0.0001703222, 0.0001690305, 0.0001677586, 0.0001665053, 0.0001652698, 0.0001640511, 0.0001628484, 0.0001616611, 0.0001604885, 0.0001593300, 0.0001581851, 0.0001570533, 0.0001559341, 0.0001548272, 0.0001537323, 0.0001526489, 0.0001515769, 0.0001505159, 0.0001494658, 0.0001484262, 0.0001473970, 0.0001463780, 0.0001453690, 0.0001443699, 0.0001433804, 0.0001424005, 0.0001414300, 0.0001404688, 0.0001395167, 0.0001385735, 0.0001376393, 0.0001367138, 0.0001357970, 0.0001348888, 0.0001339889, 0.0001330974, 0.0001322141, 0.0001313389, 0.0001304717, 0.0001296124, 0.0001287609, 0.0001279172, 0.0001270811, 0.0001262525, 0.0001254314, 0.0001246177, 0.0001238112, 0.0001230119, 0.0001222197, 0.0001214345, 0.0001206562, 0.0001198848, 0.0001191202, 0.0001183623, 0.0001176109, 0.0001007070, 0.0001006602, 0.0001006134, 0.0001005667, 0.0001005200, 0.0001004733, 0.0001004267, 0.0001003800, 0.0001003334, 0.0001002868, 0.0001002402, 0.0001001937, 0.0001001471, 0.0001001006, 0.0001000541, 0.0001000077, 0.0000999612, 0.0000999148, 0.0000998684, 0.0000998220, 0.0000997756, 0.0000997293, 0.0000996830, 0.0000996367, 0.0000995904, 0.0000995441, 0.0000994979, 0.0000994517, 0.0000994055, 0.0000993593, 0.0000993132, 0.0000992671, 0.0000992210, 0.0000991749, 0.0000991288, 0.0000990828, 0.0000990368, 0.0000989908, 0.0000989448, 0.0000988988, 0.0000988529, 0.0000988070, 0.0000987611, 0.0000987152, 0.0000986694, 0.0000986235, 0.0000985777, 0.0000985320, 0.0000984862, 0.0000984405, 0.0000983947, 0.0000983490, 0.0000983034, 0.0000982577, 0.0000982121, 0.0000981665, 0.0000981209, 0.0000980753, 0.0000980297, 0.0000979842, 0.0000979387, 0.0000978932, 0.0000978477, 0.0000978023, 0.0000977569, 0.0000977115, 0.0000976661, 0.0000976207, 0.0000975754, 0.0000975301, 0.0000974848, 0.0000974395, 0.0000973942, 0.0000973490, 0.0000973038, 0.0000972586, 0.0000972134, 0.0000971683, 0.0000971232, 0.0000970780, 0.0000970330, 0.0000969879, 0.0000969428, 0.0000968978, 0.0000968528, 0.0000968078, 0.0000967629, 0.0000967179, 0.0000966730, 0.0000966281, 0.0000965832, 0.0000965384, 0.0000964935, 0.0000964487, 0.0000964039, 0.0000963591, 0.0000963144, 0.0000962697, 0.0000962249, 0.0000961803, 0.0000961356, 0.0000960909, 0.0000960463, 0.0000960017, 0.0000959571, 0.0000959125, 0.0000958680, 0.0000958235, 0.0000957790, 0.0000957345, 0.0000956900, 0.0000956456, 0.0000956012, 0.0000955568, 0.0000955124, 0.0000954680, 0.0000954237, 0.0000953794, 0.0000953351, 0.0000952908, 0.0000952465, 0.0000952023, 0.0000951581, 0.0000951139, 0.0000950697, 0.0000950255, 0.0000949814, 0.0000949373, 0.0000948932, 0.0000948491, 0.0000948051, 0.0000947610, 0.0000947170, 0.0000946730, 0.0000946291, 0.0000945851, 0.0000945412, 0.0000944973, 0.0000944534, 0.0000944095, 0.0000943657, 0.0000943218, 0.0000942780, 0.0000942343, 0.0000941905, 0.0000941467, 0.0000941030, 0.0000940593, 0.0000940156, 0.0000939720, 0.0000939283, 0.0000938847, 0.0000938411, 0.0000937975, 0.0000937539, 0.0000937104, 0.0000936669, 0.0000936234, 0.0000935799, 0.0000935364, 0.0000934930, 0.0000934496, 0.0000934061, 0.0000933628, 0.0000933194, 0.0000932761, 0.0000932327, 0.0000931894, 0.0000931462, 0.0000931029, 0.0000930597, 0.0000930164, 0.0000929732, 0.0000929301, 0.0000928869, 0.0000928438, 0.0000928006, 0.0000927575, 0.0000927144, 0.0000926714, 0.0000926283, 0.0000925853, 0.0000925423, 0.0000924993, 0.0000924564, 0.0000924134, 0.0000923705, 0.0000923276, 0.0000922847, 0.0000922419, 0.0000921990, 0.0000921562, 0.0000921134, 0.0000920706, 0.0000920279, 0.0000919851, 0.0000919424, 0.0000918997, 0.0000918570, 0.0000918144, 0.0000917717, 0.0000917291, 0.0000916865, 0.0000916439, 0.0000916013, 0.0000915588, 0.0000915163, 0.0000914738, 0.0000914313, 0.0000913888, 0.0000913464, 0.0000913039, 0.0000912615, 0.0000912192, 0.0000911768, 0.0000911344, 0.0000910921, 0.0000910498, 0.0000910075, 0.0000909653, 0.0000909230, 0.0000908808, 0.0000908386, 0.0000907964, 0.0000907542, 0.0000907121, 0.0000906699, 0.0000906278, 0.0000905857, 0.0000905436, 0.0000905016, 0.0000904596, 0.0000904175, 0.0000903756, 0.0000903336, 0.0000902916, 0.0000902497, 0.0000902078, 0.0000901659, 0.0000901240, 0.0000900821, 0.0000900403, 0.0000899985, 0.0000899567, 0.0000899149, 0.0000898731, 0.0000898314, 0.0000897897, 0.0000897480, 0.0000897063, 0.0000896646, 0.0000896230, 0.0000895814, 0.0000895398, 0.0000894982, 0.0000894566, 0.0000894151, 0.0000893735, 0.0000893320, 0.0000892905, 0.0000892491, 0.0000892076, 0.0000891662, 0.0000891248, 0.0000890834, 0.0000890420, 0.0000890006, 0.0000889593, 0.0000889180, 0.0000888767, 0.0000888354, 0.0000887941, 0.0000887529, 0.0000887117, 0.0000886705, 0.0000886293, 0.0000885881, 0.0000885470, 0.0000885059, 0.0000884648, 0.0000884237, 0.0000883826, 0.0000883416, 0.0000883005, 0.0000882595, 0.0000882185, 0.0000881776, 0.0000881366, 0.0000880957, 0.0000880547, 0.0000880139, 0.0000879730, 0.0000879321, 0.0000878913, 0.0000878505, 0.0000878097, 0.0000877689, 0.0000877281, 0.0000876874, 0.0000876466, 0.0000876059, 0.0000875652, 0.0000875246, 0.0000874839, 0.0000874433, 0.0000874027, 0.0000873621, 0.0000873215, 0.0000872810, 0.0000872404, 0.0000871999, 0.0000871594, 0.0000871189, 0.0000870785, 0.0000870380, 0.0000869976, 0.0000869572, 0.0000869168, 0.0000868764, 0.0000868361, 0.0000867957, 0.0000867554, 0.0000867151, 0.0000866749, 0.0000866346, 0.0000865944, 0.0000865542, 0.0000865140, 0.0000864738, 0.0000864336, 0.0000863935, 0.0000863533, 0.0000863132, 0.0000862731, 0.0000862331, 0.0000861930, 0.0000861530, 0.0000861130, 0.0000860730, 0.0000860330, 0.0000859931, 0.0000859531, 0.0000859132, 0.0000858733, 0.0000858334, 0.0000857935, 0.0000857537, 0.0000857139, 0.0000856741, 0.0000856343, 0.0000855945, 0.0000855547, 0.0000855150, 0.0000854753, 0.0000854356, 0.0000853959, 0.0000853562, 0.0000853166, 0.0000852770, 0.0000852374, 0.0000851978, 0.0000851582, 0.0000851187, 0.0000850791, 0.0000850396, 0.0000850001, 0.0000849606, 0.0000849212, 0.0000848817, 0.0000848423, 0.0000848029, 0.0000847635, 0.0000847242, 0.0000846848, 0.0000846455, 0.0000846062, 0.0000845669, 0.0000845276, 0.0000844883, 0.0000844491, 0.0000844099, 0.0000843707, 0.0000843315, 0.0000842923, 0.0000842532, 0.0000842140, 0.0000841749, 0.0000841358, 0.0000840967, 0.0000840577, 0.0000840186, 0.0000839796, 0.0000839406, 0.0000839016, 0.0000838627, 0.0000838237, 0.0000837848, 0.0000837459, 0.0000837070, 0.0000836681, 0.0000836292, 0.0000835904, 0.0000835516, 0.0000835128, 0.0000834740, 0.0000834352, 0.0000833965, 0.0000833577, 0.0000833190, 0.0000832803, 0.0000832416, 0.0000832030, 0.0000831643, 0.0000831257, 0.0000830871, 0.0000830485, 0.0000830099, 0.0000829714, 0.0000829328, 0.0000828943, 0.0000828558, 0.0000828173, 0.0000827789, 0.0000827404, 0.0000827020, 0.0000826636, 0.0000826252, 0.0000825868, 0.0000825485, 0.0000825101, 0.0000824718, 0.0000824335, 0.0000823952, 0.0000823570, 0.0000823187, 0.0000822805, 0.0000822423, 0.0000822041, 0.0000821659, 0.0000821277, 0.0000820896, 0.0000820514, 0.0000820133, 0.0000819752, 0.0000819372, 0.0000818991, 0.0000818611, 0.0000818231, 0.0000817851, 0.0000817471, 0.0000817091, 0.0000816712, 0.0000816332, 0.0000815953, 0.0000815574, 0.0000815195, 0.0000814817, 0.0000814438, 0.0000814060, 0.0000813682, 0.0000813304, 0.0000812926, 0.0000812549, 0.0000812171, 0.0000811794, 0.0000811417, 0.0000811040, 0.0000810664, 0.0000810287, 0.0000809911, 0.0000809535, 0.0000809159, 0.0000808783, 0.0000808407, 0.0000808032, 0.0000807656, 0.0000807281, 0.0000806906, 0.0000806532, 0.0000806157, 0.0000805783, 0.0000805408, 0.0000805034, 0.0000804660, 0.0000804287, 0.0000803913, 0.0000803540, 0.0000803167, 0.0000802793, 0.0000802421, 0.0000802048, 0.0000801675, 0.0000801303, 0.0000800931, 0.0000800559, 0.0000800187, 0.0000799816, 0.0000799444, 0.0000799073, 0.0000798702, 0.0000798331, 0.0000797960, 0.0000797589, 0.0000797219, 0.0000796849, 0.0000796478, 0.0000796109, 0.0000795739, 0.0000795369, 0.0000795000, 0.0000794631, 0.0000794262, 0.0000793893, 0.0000793524, 0.0000793155, 0.0000792787, 0.0000792419, 0.0000792051, 0.0000791683, 0.0000791315, 0.0000790948, 0.0000790580, 0.0000790213, 0.0000789846, 0.0000789479, 0.0000789113, 0.0000788746, 0.0000788380, 0.0000788014, 0.0000787648, 0.0000787282, 0.0000786916, 0.0000786551, 0.0000786185, 0.0000785820, 0.0000785455, 0.0000785090, 0.0000784726, 0.0000784361, 0.0000783997, 0.0000783633, 0.0000783269, 0.0000782905, 0.0000782542, 0.0000782178, 0.0000781815, 0.0000781452, 0.0000781089, 0.0000780726, 0.0000780363, 0.0000780001, 0.0000779639, 0.0000779277, 0.0000778915, 0.0000778553, 0.0000778191, 0.0000777830, 0.0000777469, 0.0000777108, 0.0000776747, 0.0000776386, 0.0000776025, 0.0000775665, 0.0000775305, 0.0000774944, 0.0000774585, 0.0000774225, 0.0000773865, 0.0000773506, 0.0000773147, 0.0000772787, 0.0000772429, 0.0000772070, 0.0000771711, 0.0000771353, 0.0000770995, 0.0000770636, 0.0000770279, 0.0000769921, 0.0000769563, 0.0000769206, 0.0000768849, 0.0000768491, 0.0000768134, 0.0000767778, 0.0000767421, 0.0000767065, 0.0000766708, 0.0000766352, 0.0000765996, 0.0000765641, 0.0000765285, 0.0000764930, 0.0000764574, 0.0000764219, 0.0000763864, 0.0000763510, 0.0000763155, 0.0000762800, 0.0000762446, 0.0000762092, 0.0000761738, 0.0000761384, 0.0000761031, 0.0000760677, 0.0000760324, 0.0000759971, 0.0000759618, 0.0000759265, 0.0000758912, 0.0000758560, 0.0000758208, 0.0000757856, 0.0000757504, 0.0000757152, 0.0000756800, 0.0000756449, 0.0000756097, 0.0000755746, 0.0000755395, 0.0000755044, 0.0000754694, 0.0000754343, 0.0000753993, 0.0000753642, 0.0000753292, 0.0000752943, 0.0000752593, 0.0000752243, 0.0000751894, 0.0000751545, 0.0000751196, 0.0000750847, 0.0000750498, 0.0000750150, 0.0000749801, 0.0000749453, 0.0000749105, 0.0000748757, 0.0000748409, 0.0000748062, 0.0000747714, 0.0000747367, 0.0000747020, 0.0000746673, 0.0000746326, 0.0000745979, 0.0000745633, 0.0000745287, 0.0000744940, 0.0000744594, 0.0000744249, 0.0000743903, 0.0000743557, 0.0000743212, 0.0000742867, 0.0000742522, 0.0000742177, 0.0000741832, 0.0000741488, 0.0000741143, 0.0000740799, 0.0000740455, 0.0000740111, 0.0000739767, 0.0000739424, 0.0000739080, 0.0000738737, 0.0000738394, 0.0000738051, 0.0000737708, 0.0000737366, 0.0000737023, 0.0000736681, 0.0000736339, 0.0000735997, 0.0000735655, 0.0000735313, 0.0000734972, 0.0000734630, 0.0000734289, 0.0000733948, 0.0000733607, 0.0000733267, 0.0000732926, 0.0000732586, 0.0000732245, 0.0000731905, 0.0000731565, 0.0000731226, 0.0000730886, 0.0000730547, 0.0000730207, 0.0000729868, 0.0000729529, 0.0000729190, 0.0000728852, 0.0000728513, 0.0000728175, 0.0000727837, 0.0000727499, 0.0000727161, 0.0000726823, 0.0000726485, 0.0000726148, 0.0000725811, 0.0000725474, 0.0000725137, 0.0000724800, 0.0000724463, 0.0000724127, 0.0000723790, 0.0000723454, 0.0000723118, 0.0000722782, 0.0000722447, 0.0000722111, 0.0000721776, 0.0000721441, 0.0000721105, 0.0000720771, 0.0000720436, 0.0000720101, 0.0000719767, 0.0000719432, 0.0000719098, 0.0000718764, 0.0000718431, 0.0000718097, 0.0000717763, 0.0000717430, 0.0000717097, 0.0000716764, 0.0000716431, 0.0000716098, 0.0000715765, 0.0000715433, 0.0000715101, 0.0000714769, 0.0000714437, 0.0000714105, 0.0000713773, 0.0000713442, 0.0000713110, 0.0000712779, 0.0000712448, 0.0000712117, 0.0000711786, 0.0000711456, 0.0000711125, 0.0000710795, 0.0000710465, 0.0000710135, 0.0000709805, 0.0000709476, 0.0000709146, 0.0000708817, 0.0000708487, 0.0000708158, 0.0000707830, 0.0000707501, 0.0000707172, 0.0000706844, 0.0000706515, 0.0000706187, 0.0000705859, 0.0000705531, 0.0000705204, 0.0000704876, 0.0000704549, 0.0000704222, 0.0000703895, 0.0000703568, 0.0000703241, 0.0000702914, 0.0000702588, 0.0000702261, 0.0000701935, 0.0000701609, 0.0000701283, 0.0000700958, 0.0000700632, 0.0000700307, 0.0000699982, 0.0000699656, 0.0000699331, 0.0000699007, 0.0000698682, 0.0000698358, 0.0000698033, 0.0000697709, 0.0000697385, 0.0000697061, 0.0000696737, 0.0000696414, 0.0000696090, 0.0000695767, 0.0000695444, 0.0000695121, 0.0000694798, 0.0000694475, 0.0000694153, 0.0000693830, 0.0000693508, 0.0000693186, 0.0000692864, 0.0000692542, 0.0000692221, 0.0000691899, 0.0000691578, 0.0000691257, 0.0000690935, 0.0000690615, 0.0000690294, 0.0000689973, 0.0000689653, 0.0000689332, 0.0000689012, 0.0000688692, 0.0000688372, 0.0000688053, 0.0000687733, 0.0000687414, 0.0000687094, 0.0000686775, 0.0000686456, 0.0000686138, 0.0000685819, 0.0000685500, 0.0000685182, 0.0000684864, 0.0000684546, 0.0000684228, 0.0000683910, 0.0000683592, 0.0000683275, 0.0000682957, 0.0000682640, 0.0000682323, 0.0000682006, 0.0000681690, 0.0000681373, 0.0000681056, 0.0000680740, 0.0000680424, 0.0000680108, 0.0000679792, 0.0000679476, 0.0000679161, 0.0000678845, 0.0000678530, 0.0000678215, 0.0000677900, 0.0000677585, 0.0000677270, 0.0000676956, 0.0000676641, 0.0000676327, 0.0000676013, 0.0000675699, 0.0000675385, 0.0000675072, 0.0000674758, 0.0000674445, 0.0000674131, 0.0000673818, 0.0000673505, 0.0000673193, 0.0000672880, 0.0000672567, 0.0000672255, 0.0000671943, 0.0000671631, 0.0000671319, 0.0000671007, 0.0000670695, 0.0000670384, 0.0000670072, 0.0000669761, 0.0000669450, 0.0000669139, 0.0000668828, 0.0000668518, 0.0000668207, 0.0000667897, 0.0000667587, 0.0000667277, 0.0000666967, 0.0000666657, 0.0000666347, 0.0000666038, 0.0000665729, 0.0000665419, 0.0000665110, 0.0000664801, 0.0000664493, 0.0000664184, 0.0000663876, 0.0000663567, 0.0000663259, 0.0000662951, 0.0000662643, 0.0000662335, 0.0000662028, 0.0000661720, 0.0000661413, 0.0000661106, 0.0000660799, 0.0000660492, 0.0000660185, 0.0000659878, 0.0000659572, 0.0000659266, 0.0000658959, 0.0000658653, 0.0000658347, 0.0000658042, 0.0000657736, 0.0000657431, 0.0000657125, 0.0000656820, 0.0000656515, 0.0000656210, 0.0000655905, 0.0000655601, 0.0000655296, 0.0000654992, 0.0000654688, 0.0000654383, 0.0000654080, 0.0000653776, 0.0000653472, 0.0000653169, 0.0000652865, 0.0000652562, 0.0000652259, 0.0000651956, 0.0000651653, 0.0000651351, 0.0000651048, 0.0000650746, 0.0000650443, 0.0000650141, 0.0000649839, 0.0000649538, 0.0000649236, 0.0000648934, 0.0000648633, 0.0000648332, 0.0000648031, 0.0000647730, 0.0000647429, 0.0000647128, 0.0000646828, 0.0000646527, 0.0000646227, 0.0000645927, 0.0000645627, 0.0000645327, 0.0000645027, 0.0000644728, 0.0000644428, 0.0000644129, 0.0000643830, 0.0000643531, 0.0000643232, 0.0000642933, 0.0000642634, 0.0000642336, 0.0000642038, 0.0000641739, 0.0000641441, 0.0000641143, 0.0000640846, 0.0000640548, 0.0000640250, 0.0000639953, 0.0000639656, 0.0000639359, 0.0000639062, 0.0000638765, 0.0000638468, 0.0000638172, 0.0000637875, 0.0000637579, 0.0000637283, 0.0000636987, 0.0000636691, 0.0000636396, 0.0000636100, 0.0000635805, 0.0000635509, 0.0000635214, 0.0000634919, 0.0000634624, 0.0000634329, 0.0000634035, 0.0000633740, 0.0000633446, 0.0000633152, 0.0000632858, 0.0000632564, 0.0000632270, 0.0000631976, 0.0000631683, 0.0000631389, 0.0000631096, 0.0000630803, 0.0000630510, 0.0000630217, 0.0000629925, 0.0000629632, 0.0000629340, 0.0000629047, 0.0000628755, 0.0000628463, 0.0000628171, 0.0000627879, 0.0000627588, 0.0000627296, 0.0000627005, 0.0000626714, 0.0000626423, 0.0000626132, 0.0000625841, 0.0000625550, 0.0000625260, 0.0000624969, 0.0000624679, 0.0000624389, 0.0000624099, 0.0000623809, 0.0000623519, 0.0000623230, 0.0000622940, 0.0000622651, 0.0000622362, 0.0000622073, 0.0000621784, 0.0000621495, 0.0000621206, 0.0000620918, 0.0000620630, 0.0000620341, 0.0000620053, 0.0000619765, 0.0000619477, 0.0000619190, 0.0000618902, 0.0000618615, 0.0000618327, 0.0000618040, 0.0000617753, 0.0000617466, 0.0000617179, 0.0000616893, 0.0000616606, 0.0000616320, 0.0000616034, 0.0000615747, 0.0000615461, 0.0000615176, 0.0000614890, 0.0000614604, 0.0000614319, 0.0000614034, 0.0000613748, 0.0000613463, 0.0000613178, 0.0000612894, 0.0000612609, 0.0000612324, 0.0000612040, 0.0000611756, 0.0000611472, 0.0000611188, 0.0000610904, 0.0000610620, 0.0000610336, 0.0000610053, 0.0000609770, 0.0000609486, 0.0000609203, 0.0000608920, 0.0000608638, 0.0000608355, 0.0000608072, 0.0000607790, 0.0000607508, 0.0000607226, 0.0000606944, 0.0000606662, 0.0000606380, 0.0000606098, 0.0000605817, 0.0000605535, 0.0000605254, 0.0000604973, 0.0000604692, 0.0000604411, 0.0000604130, 0.0000603850, 0.0000603569, 0.0000603289, 0.0000603009, 0.0000602729, 0.0000602449, 0.0000602169, 0.0000601889, 0.0000601610, 0.0000601330, 0.0000601051, 0.0000600772, 0.0000600493, 0.0000600214, 0.0000599935, 0.0000599657, 0.0000599378, 0.0000599100, 0.0000598822, 0.0000598543, 0.0000598265, 0.0000597988, 0.0000597710, 0.0000597432, 0.0000597155, 0.0000596877, 0.0000596600, 0.0000596323, 0.0000596046, 0.0000595769, 0.0000595493, 0.0000595216, 0.0000594940, 0.0000594663, 0.0000594387, 0.0000594111, 0.0000593835, 0.0000593559, 0.0000593284, 0.0000593008, 0.0000592733, 0.0000592457, 0.0000592182, 0.0000591907, 0.0000591632, 0.0000591357, 0.0000591083, 0.0000590808, 0.0000590534, 0.0000590260, 0.0000589985, 0.0000589711, 0.0000589438, 0.0000589164, 0.0000588890, 0.0000588617, 0.0000588343, 0.0000588070, 0.0000587797, 0.0000587524, 0.0000587251, 0.0000586978, 0.0000586706, 0.0000586433, 0.0000586161, 0.0000585889, 0.0000585616, 0.0000585344, 0.0000585073, 0.0000584801, 0.0000584529, 0.0000584258, 0.0000583986, 0.0000583715, 0.0000583444, 0.0000583173, 0.0000582902, 0.0000582632, 0.0000582361, 0.0000582090, 0.0000581820, 0.0000581550, 0.0000581280, 0.0000581010, 0.0000580740, 0.0000580470, 0.0000580201, 0.0000579931, 0.0000579662, 0.0000579393, 0.0000579123, 0.0000578854, 0.0000578586, 0.0000578317, 0.0000578048, 0.0000577780, 0.0000577512, 0.0000577243, 0.0000576975, 0.0000576707, 0.0000576439, 0.0000576172, 0.0000575904, 0.0000575637, 0.0000575369, 0.0000575102, 0.0000574835, 0.0000574568, 0.0000574301, 0.0000574034, 0.0000573768, 0.0000573501, 0.0000573235, 0.0000572969, 0.0000572703, 0.0000572437, 0.0000572171, 0.0000571905, 0.0000571639, 0.0000571374, 0.0000571108, 0.0000570843, 0.0000570578, 0.0000570313, 0.0000570048, 0.0000569783, 0.0000569519, 0.0000569254, 0.0000568990, 0.0000568726, 0.0000568461, 0.0000568197, 0.0000567934, 0.0000567670, 0.0000567406, 0.0000567143, 0.0000566879, 0.0000566616, 0.0000566353, 0.0000566090, 0.0000565827, 0.0000565564, 0.0000565301, 0.0000565039, 0.0000564776, 0.0000564514, 0.0000564252, 0.0000563990, 0.0000563728, 0.0000563466, 0.0000563204, 0.0000562943, 0.0000562681, 0.0000562420, 0.0000562159, 0.0000561898, 0.0000561637, 0.0000561376, 0.0000561115, 0.0000560854, 0.0000560594, 0.0000560334, 0.0000560073, 0.0000559813, 0.0000559553, 0.0000559293, 0.0000559034, 0.0000558774, 0.0000558514, 0.0000558255, 0.0000557996, 0.0000557737, 0.0000557478, 0.0000557219, 0.0000556960, 0.0000556701, 0.0000556443, 0.0000556184, 0.0000555926, 0.0000555668, 0.0000555410, 0.0000555152, 0.0000554894, 0.0000554636, 0.0000554378, 0.0000554121, 0.0000553864, 0.0000553606, 0.0000553349, 0.0000553092, 0.0000552835, 0.0000552579, 0.0000552322, 0.0000552065, 0.0000551809, 0.0000551553, 0.0000551297, 0.0000551041, 0.0000550785, 0.0000550529, 0.0000550273, 0.0000550018, 0.0000549762, 0.0000549507, 0.0000549252, 0.0000548996, 0.0000548741, 0.0000548487, 0.0000548232, 0.0000547977, 0.0000547723, 0.0000547468, 0.0000547214, 0.0000546960, 0.0000546706, 0.0000546452, 0.0000546198, 0.0000545944, 0.0000545691, 0.0000545437, 0.0000545184, 0.0000544931, 0.0000544678, 0.0000544425, 0.0000544172, 0.0000543919, 0.0000543667, 0.0000543414, 0.0000543162, 0.0000542909, 0.0000542657, 0.0000542405, 0.0000542153, 0.0000541902, 0.0000541650, 0.0000541398, 0.0000541147, 0.0000540896, 0.0000540644, 0.0000540393, 0.0000540142, 0.0000539891, 0.0000539641, 0.0000539390, 0.0000539139, 0.0000538889, 0.0000538639, 0.0000538389, 0.0000538139, 0.0000537889, 0.0000537639, 0.0000537389, 0.0000537140, 0.0000536890, 0.0000536641, 0.0000536391, 0.0000536142, 0.0000535893, 0.0000535644, 0.0000535396, 0.0000535147, 0.0000534898, 0.0000534650, 0.0000534402, 0.0000534153, 0.0000533905, 0.0000533657, 0.0000533410, 0.0000533162, 0.0000532914, 0.0000532667, 0.0000532419, 0.0000532172, 0.0000531925, 0.0000531678, 0.0000531431, 0.0000531184, 0.0000530937, 0.0000530691, 0.0000530444, 0.0000530198, 0.0000529952, 0.0000529706, 0.0000529460, 0.0000529214, 0.0000528968, 0.0000528722, 0.0000528477, 0.0000528231, 0.0000527986, 0.0000527741, 0.0000527495, 0.0000527250, 0.0000527006, 0.0000526761, 0.0000526516, 0.0000526272, 0.0000526027, 0.0000525783, 0.0000525539, 0.0000525295, 0.0000525051, 0.0000524807, 0.0000524563, 0.0000524319, 0.0000524076, 0.0000523832, 0.0000523589, 0.0000523346, 0.0000523103, 0.0000522860, 0.0000522617, 0.0000522374, 0.0000522132, 0.0000521889, 0.0000521647, 0.0000521405, 0.0000521162, 0.0000520920, 0.0000520678, 0.0000520437, 0.0000520195, 0.0000519953, 0.0000519712, 0.0000519470, 0.0000519229, 0.0000518988, 0.0000518747, 0.0000518506, 0.0000518265, 0.0000518025, 0.0000517784, 0.0000517543, 0.0000517303, 0.0000517063, 0.0000516823, 0.0000516583, 0.0000516343, 0.0000516103, 0.0000515863, 0.0000515624, 0.0000515384, 0.0000515145, 0.0000514906, 0.0000514666, 0.0000514427, 0.0000514188, 0.0000513950, 0.0000513711, 0.0000513472, 0.0000513234, 0.0000512995, 0.0000512757, 0.0000512519, 0.0000512281, 0.0000512043, 0.0000511805, 0.0000511568, 0.0000511330, 0.0000511092, 0.0000510855, 0.0000510618, 0.0000510381, 0.0000510144, 0.0000509907, 0.0000509670, 0.0000509433, 0.0000509197, 0.0000508960, 0.0000508724, 0.0000508487, 0.0000508251, 0.0000508015, 0.0000507779, 0.0000507543, 0.0000507308, 0.0000507072, 0.0000506837, 0.0000506601, 0.0000506366, 0.0000506131, 0.0000505896, 0.0000505661, 0.0000505426, 0.0000505191, 0.0000504956, 0.0000504722, 0.0000504487, 0.0000504253, 0.0000504019, 0.0000503785, 0.0000503551, 0.0000503317, 0.0000503083, 0.0000502850, 0.0000502616, 0.0000502383, 0.0000502149, 0.0000501916, 0.0000501683, 0.0000501450, 0.0000501217, 0.0000500984, 0.0000500752, 0.0000500519, 0.0000500287, 0.0000500054, 0.0000499822, 0.0000499590, 0.0000499358, 0.0000499126, 0.0000498894, 0.0000498662, 0.0000498431, 0.0000498199, 0.0000497968, 0.0000497737, 0.0000497505, 0.0000497274, 0.0000497043, 0.0000496813, 0.0000496582, 0.0000496351, 0.0000496121, 0.0000495890, 0.0000495660, 0.0000495430, 0.0000495200, 0.0000494970, 0.0000494740, 0.0000494510, 0.0000494280, 0.0000494051, 0.0000493821, 0.0000493592, 0.0000493363, 0.0000493133, 0.0000492904, 0.0000492675, 0.0000492447, 0.0000492218, 0.0000491989, 0.0000491761, 0.0000491532, 0.0000491304, 0.0000491076, 0.0000490848, 0.0000490620, 0.0000490392, 0.0000490164, 0.0000489937, 0.0000489709, 0.0000489482, 0.0000489254, 0.0000489027, 0.0000488800, 0.0000488573, 0.0000488346, 0.0000488119, 0.0000487893, 0.0000487666, 0.0000487439, 0.0000487213, 0.0000486987, 0.0000486761, 0.0000486534, 0.0000486309, 0.0000486083, 0.0000485857, 0.0000485631, 0.0000485406, 0.0000485180, 0.0000484955, 0.0000484730, 0.0000484505, 0.0000484280, 0.0000484055, 0.0000483830, 0.0000483605, 0.0000483380, 0.0000483156, 0.0000482932, 0.0000482707, 0.0000482483, 0.0000482259, 0.0000482035, 0.0000481811, 0.0000481587, 0.0000481364, 0.0000481140, 0.0000480917, 0.0000480693, 0.0000480470, 0.0000480247, 0.0000480024, 0.0000479801, 0.0000479578, 0.0000479355, 0.0000479133, 0.0000478910, 0.0000478688, 0.0000478465, 0.0000478243, 0.0000478021, 0.0000477799, 0.0000477577, 0.0000477355, 0.0000477134, 0.0000476912, 0.0000476690, 0.0000476469, 0.0000476248, 0.0000476027, 0.0000475805, 0.0000475585, 0.0000475364, 0.0000475143, 0.0000474922, 0.0000474702, 0.0000474481, 0.0000474261, 0.0000474040, 0.0000473820, 0.0000473600, 0.0000473380, 0.0000473160, 0.0000472941, 0.0000472721, 0.0000472501, 0.0000472282, 0.0000472063, 0.0000471843, 0.0000471624, 0.0000471405, 0.0000471186, 0.0000470967, 0.0000470749, 0.0000470530, 0.0000470312, 0.0000470093, 0.0000469875, 0.0000469657, 0.0000469438, 0.0000469220, 0.0000469002, 0.0000468785, 0.0000468567, 0.0000468349, 0.0000468132, 0.0000467914, 0.0000467697, 0.0000467480, 0.0000467263, 0.0000467046, 0.0000466829, 0.0000466612, 0.0000466395, 0.0000466179, 0.0000465962, 0.0000465746, 0.0000465529, 0.0000465313, 0.0000465097, 0.0000464881, 0.0000464665, 0.0000464449, 0.0000464234, 0.0000464018, 0.0000463802, 0.0000463587, 0.0000463372, 0.0000463156, 0.0000462941, 0.0000462726, 0.0000462511, 0.0000462297, 0.0000462082, 0.0000461867, 0.0000461653, 0.0000461438, 0.0000461224, 0.0000461010, 0.0000460796, 0.0000460582, 0.0000460368, 0.0000460154, 0.0000459940, 0.0000459727, 0.0000459513, 0.0000459300, 0.0000459086, 0.0000458873, 0.0000458660, 0.0000458447, 0.0000458234, 0.0000458021, 0.0000457809, 0.0000457596, 0.0000457383, 0.0000457171, 0.0000456959, 0.0000456746, 0.0000456534, 0.0000456322, 0.0000456110, 0.0000455898, 0.0000455687, 0.0000455475, 0.0000455264, 0.0000455052, 0.0000454841, 0.0000454630, 0.0000454418, 0.0000454207, 0.0000453996, 0.0000453785, 0.0000453575, 0.0000453364, 0.0000453154, 0.0000452943, 0.0000452733, 0.0000452522, 0.0000452312, 0.0000452102, 0.0000451892, 0.0000451682, 0.0000451473, 0.0000451263, 0.0000451053, 0.0000450844, 0.0000450634, 0.0000450425, 0.0000450216, 0.0000450007, 0.0000449798, 0.0000449589, 0.0000449380, 0.0000449171, 0.0000448963, 0.0000448754, 0.0000448546, 0.0000448337, 0.0000448129, 0.0000447921, 0.0000447713, 0.0000447505, 0.0000447297, 0.0000447090, 0.0000446882, 0.0000446674, 0.0000446467, 0.0000446260, 0.0000446052, 0.0000445845, 0.0000445638, 0.0000445431, 0.0000445224, 0.0000445017, 0.0000444811, 0.0000444604, 0.0000444398, 0.0000444191, 0.0000443985, 0.0000443779, 0.0000443573, 0.0000443367, 0.0000443161, 0.0000442955, 0.0000442749, 0.0000442543, 0.0000442338, 0.0000442132, 0.0000441927, 0.0000441722, 0.0000441517, 0.0000441312, 0.0000441107, 0.0000440902, 0.0000440697, 0.0000440492, 0.0000440288, 0.0000440083, 0.0000439879, 0.0000439675, 0.0000439470, 0.0000439266, 0.0000439062, 0.0000438858, 0.0000438655, 0.0000438451, 0.0000438247, 0.0000438044, 0.0000437840, 0.0000437637, 0.0000437434, 0.0000437230, 0.0000437027, 0.0000436824, 0.0000436621, 0.0000436419, 0.0000436216, 0.0000436013, 0.0000435811, 0.0000435608, 0.0000435406, 0.0000435204, 0.0000435002, 0.0000434800, 0.0000434598, 0.0000434396, 0.0000434194, 0.0000433993, 0.0000433791, 0.0000433590, 0.0000433388, 0.0000433187, 0.0000432986, 0.0000432785, 0.0000432584, 0.0000432383, 0.0000432182, 0.0000431981, 0.0000431780, 0.0000431580, 0.0000431379, 0.0000431179, 0.0000430979, 0.0000430779, 0.0000430579, 0.0000430379, 0.0000430179, 0.0000429979, 0.0000429779, 0.0000429580, 0.0000429380, 0.0000429181, 0.0000428981, 0.0000428782, 0.0000428583, 0.0000428384, 0.0000428185, 0.0000427986, 0.0000427787, 0.0000427589, 0.0000427390, 0.0000427192, 0.0000426993, 0.0000426795, 0.0000426597, 0.0000426398, 0.0000426200, 0.0000426002, 0.0000425805, 0.0000425607, 0.0000425409, 0.0000425212, 0.0000425014, 0.0000424817, 0.0000424619, 0.0000424422, 0.0000424225, 0.0000424028, 0.0000423831, 0.0000423634, 0.0000423438, 0.0000423241, 0.0000423044, 0.0000422848, 0.0000422651, 0.0000422455, 0.0000422259, 0.0000422063, 0.0000421867, 0.0000421671, 0.0000421475, 0.0000421279, 0.0000421084, 0.0000420888, 0.0000420693, 0.0000420497, 0.0000420302, 0.0000420107, 0.0000419912, 0.0000419716, 0.0000419522, 0.0000419327, 0.0000419132, 0.0000418937, 0.0000418743, 0.0000418548, 0.0000418354, 0.0000418160, 0.0000417965, 0.0000417771, 0.0000417577, 0.0000417383, 0.0000417189, 0.0000416996, 0.0000416802, 0.0000416608, 0.0000416415, 0.0000416221, 0.0000416028, 0.0000415835, 0.0000415642, 0.0000415449, 0.0000415256, 0.0000415063, 0.0000414870, 0.0000414677, 0.0000414485, 0.0000414292, 0.0000414100, 0.0000413908, 0.0000413715, 0.0000413523, 0.0000413331, 0.0000413139, 0.0000412947, 0.0000412756, 0.0000412564, 0.0000412372, 0.0000412181, 0.0000411989, 0.0000411798, 0.0000411607, 0.0000411415, 0.0000411224, 0.0000411033, 0.0000410843, 0.0000410652, 0.0000410461, 0.0000410270, 0.0000410080, 0.0000409889, 0.0000409699, 0.0000409509, 0.0000409318, 0.0000409128, 0.0000408938, 0.0000408748, 0.0000408559, 0.0000408369, 0.0000408179, 0.0000407990, 0.0000407800, 0.0000407611, 0.0000407421, 0.0000407232, 0.0000407043, 0.0000406854, 0.0000406665, 0.0000406476, 0.0000406287, 0.0000406099, 0.0000405910, 0.0000405722, 0.0000405533, 0.0000405345, 0.0000405156, 0.0000404968, 0.0000404780, 0.0000404592, 0.0000404404, 0.0000404216, 0.0000404029, 0.0000403841, 0.0000403654, 0.0000403466, 0.0000403279, 0.0000403091, 0.0000402904, 0.0000402717, 0.0000402530, 0.0000402343, 0.0000402156, 0.0000401969, 0.0000401783, 0.0000401596, 0.0000401410, 0.0000401223, 0.0000401037, 0.0000400851, 0.0000400664, 0.0000400478, 0.0000400292, 0.0000400106, 0.0000399920, 0.0000399735, 0.0000399549, 0.0000399364, 0.0000399178, 0.0000398993, 0.0000398807, 0.0000398622, 0.0000398437, 0.0000398252, 0.0000398067, 0.0000397882, 0.0000397697, 0.0000397513, 0.0000397328, 0.0000397143, 0.0000396959, 0.0000396775, 0.0000396590, 0.0000396406, 0.0000396222, 0.0000396038, 0.0000395854, 0.0000395670, 0.0000395486, 0.0000395303, 0.0000395119, 0.0000394936, 0.0000394752, 0.0000394569, 0.0000394386, 0.0000394202, 0.0000394019, 0.0000393836, 0.0000393653, 0.0000393471, 0.0000393288, 0.0000393105, 0.0000392923, 0.0000392740, 0.0000392558, 0.0000392375, 0.0000392193, 0.0000392011, 0.0000391829, 0.0000391647, 0.0000391465, 0.0000391283, 0.0000391102, 0.0000390920, 0.0000390738, 0.0000390557, 0.0000390375, 0.0000390194, 0.0000390013, 0.0000389832, 0.0000389651, 0.0000389470, 0.0000389289, 0.0000389108, 0.0000388927, 0.0000388747, 0.0000388566, 0.0000388386, 0.0000388205, 0.0000388025, 0.0000387845, 0.0000387665, 0.0000387485, 0.0000387305, 0.0000387125, 0.0000386945, 0.0000386765, 0.0000386586, 0.0000386406, 0.0000386227, 0.0000386047, 0.0000385868, 0.0000385689, 0.0000385510, 0.0000385331, 0.0000385152, 0.0000384973, 0.0000384794, 0.0000384615, 0.0000384437, 0.0000384258, 0.0000384079, 0.0000383901, 0.0000383723, 0.0000383545, 0.0000383366, 0.0000383188, 0.0000383010, 0.0000382833, 0.0000382655, 0.0000382477, 0.0000382299, 0.0000382122, 0.0000381944, 0.0000381767, 0.0000381590, 0.0000381412, 0.0000381235, 0.0000381058, 0.0000380881, 0.0000380704, 0.0000380527, 0.0000380351, 0.0000380174, 0.0000379998, 0.0000379821, 0.0000379645, 0.0000379468, 0.0000379292, 0.0000379116, 0.0000378940, 0.0000378764, 0.0000378588, 0.0000378412, 0.0000378236, 0.0000378061, 0.0000377885, 0.0000377710, 0.0000377534, 0.0000377359, 0.0000377184, 0.0000377008, 0.0000376833, 0.0000376658, 0.0000376483, 0.0000376308, 0.0000376134, 0.0000375959, 0.0000375784, 0.0000375610, 0.0000375435, 0.0000375261, 0.0000375087, 0.0000374913, 0.0000374738, 0.0000374564, 0.0000374390, 0.0000374216, 0.0000374043, 0.0000373869, 0.0000373695, 0.0000373522, 0.0000373348, 0.0000373175, 0.0000373002, 0.0000372828, 0.0000372655, 0.0000372482, 0.0000372309, 0.0000372136, 0.0000371963, 0.0000371791, 0.0000371618, 0.0000371445, 0.0000371273, 0.0000371100, 0.0000370928, 0.0000370756, 0.0000370584, 0.0000370411, 0.0000370239, 0.0000370067, 0.0000369896, 0.0000369724, 0.0000369552, 0.0000369380, 0.0000369209, 0.0000369037, 0.0000368866, 0.0000368695, 0.0000368523, 0.0000368352, 0.0000368181, 0.0000368010, 0.0000367839, 0.0000367668, 0.0000367498, 0.0000367327, 0.0000367156, 0.0000366986, 0.0000366815, 0.0000366645, 0.0000366475, 0.0000366305, 0.0000366134, 0.0000365964, 0.0000365794, 0.0000365624, 0.0000365455, 0.0000365285, 0.0000365115, 0.0000364946, 0.0000364776, 0.0000364607, 0.0000364437, 0.0000364268, 0.0000364099, 0.0000363930, 0.0000363761, 0.0000363592, 0.0000363423, 0.0000363254, 0.0000363086, 0.0000362917, 0.0000362748, 0.0000362580, 0.0000362411, 0.0000362243, 0.0000362075, 0.0000361907, 0.0000361739, 0.0000361571, 0.0000361403, 0.0000361235, 0.0000361067, 0.0000360899, 0.0000360732, 0.0000360564, 0.0000360397, 0.0000360229, 0.0000360062, 0.0000359895, 0.0000359728, 0.0000359561, 0.0000359394, 0.0000359227, 0.0000359060, 0.0000358893, 0.0000358726, 0.0000358560, 0.0000358393, 0.0000358227, 0.0000358060, 0.0000357894, 0.0000357728, 0.0000357562, 0.0000357396, 0.0000357230, 0.0000357064, 0.0000356898, 0.0000356732, 0.0000356567, 0.0000356401, 0.0000356235, 0.0000356070, 0.0000355905, 0.0000355739, 0.0000355574, 0.0000355409, 0.0000355244, 0.0000355079, 0.0000354914, 0.0000354749, 0.0000354584, 0.0000354420, 0.0000354255, 0.0000354090, 0.0000353926, 0.0000353762, 0.0000353597, 0.0000353433, 0.0000353269, 0.0000353105, 0.0000352941, 0.0000352777, 0.0000352613, 0.0000352449, 0.0000352286, 0.0000352122, 0.0000351959, 0.0000351795, 0.0000351632, 0.0000351468, 0.0000351305, 0.0000351142, 0.0000350979, 0.0000350816, 0.0000350653, 0.0000350490, 0.0000350327, 0.0000350165, 0.0000350002, 0.0000349839, 0.0000349677, 0.0000349514, 0.0000349352, 0.0000349190, 0.0000349028, 0.0000348866, 0.0000348704, 0.0000348542, 0.0000348380, 0.0000348218, 0.0000348056, 0.0000347895, 0.0000347733, 0.0000347571, 0.0000347410, 0.0000347249, 0.0000347087, 0.0000346926, 0.0000346765, 0.0000346604, 0.0000346443, 0.0000346282, 0.0000346121, 0.0000345961, 0.0000345800, 0.0000345639, 0.0000345479, 0.0000345318, 0.0000345158, 0.0000344998, 0.0000344837, 0.0000344677, 0.0000344517, 0.0000344357, 0.0000344197, 0.0000344037, 0.0000343878, 0.0000343718, 0.0000343558, 0.0000343399, 0.0000343239, 0.0000343080, 0.0000342920, 0.0000342761, 0.0000342602, 0.0000342443, 0.0000342284, 0.0000342125, 0.0000341966, 0.0000341807, 0.0000341648, 0.0000341490, 0.0000341331, 0.0000341172, 0.0000341014, 0.0000340856, 0.0000340697, 0.0000340539, 0.0000340381, 0.0000340223, 0.0000340065, 0.0000339907, 0.0000339749, 0.0000339591, 0.0000339433, 0.0000339276, 0.0000339118, 0.0000338961, 0.0000338803, 0.0000338646, 0.0000338489, 0.0000338331, 0.0000338174, 0.0000338017, 0.0000337860, 0.0000337703, 0.0000337547, 0.0000337390, 0.0000337233, 0.0000337076, 0.0000336920, 0.0000336763, 0.0000336607, 0.0000336451, 0.0000336294, 0.0000336138, 0.0000335982, 0.0000335826, 0.0000335670, 0.0000335514, 0.0000335358, 0.0000335203, 0.0000335047, 0.0000334891, 0.0000334736, 0.0000334580, 0.0000334425, 0.0000334270, 0.0000334114, 0.0000333959, 0.0000333804, 0.0000333649, 0.0000333494, 0.0000333339, 0.0000333184, 0.0000333030, 0.0000332875, 0.0000332720, 0.0000332566, 0.0000332411, 0.0000332257, 0.0000332103, 0.0000331948, 0.0000331794, 0.0000331640, 0.0000331486, 0.0000331332, 0.0000331178, 0.0000331024, 0.0000330871, 0.0000330717, 0.0000330563, 0.0000330410, 0.0000330256, 0.0000330103, 0.0000329950, 0.0000329796, 0.0000329643, 0.0000329490, 0.0000329337, 0.0000329184, 0.0000329031, 0.0000328878, 0.0000328726, 0.0000328573, 0.0000328420, 0.0000328268, 0.0000328115, 0.0000327963, 0.0000327811, 0.0000327658, 0.0000327506, 0.0000327354, 0.0000327202, 0.0000327050, 0.0000326898, 0.0000326746, 0.0000326595, 0.0000326443, 0.0000326291, 0.0000326140, 0.0000325988, 0.0000325837, 0.0000325686, 0.0000325534, 0.0000325383, 0.0000325232, 0.0000325081, 0.0000324930, 0.0000324779, 0.0000324628, 0.0000324478, 0.0000324327, 0.0000324176, 0.0000324026, 0.0000323875, 0.0000323725, 0.0000323574, 0.0000323424, 0.0000323274, 0.0000323124, 0.0000322974, 0.0000322824, 0.0000322674, 0.0000322524, 0.0000322374, 0.0000322224, 0.0000322075, 0.0000321925, 0.0000321776, 0.0000321626, 0.0000321477, 0.0000321327, 0.0000321178, 0.0000321029, 0.0000320880, 0.0000320731, 0.0000320582, 0.0000320433, 0.0000320284, 0.0000320135, 0.0000319987, 0.0000319838, 0.0000319690, 0.0000319541, 0.0000319393, 0.0000319244, 0.0000319096, 0.0000318948, 0.0000318800, 0.0000318652, 0.0000318504, 0.0000318356, 0.0000318208, 0.0000318060, 0.0000317912, 0.0000317765, 0.0000317617, 0.0000317470, 0.0000317322, 0.0000317175, 0.0000317028, 0.0000316880, 0.0000316733, 0.0000316586, 0.0000316439, 0.0000316292, 0.0000316145, 0.0000315998, 0.0000315851, 0.0000315705, 0.0000315558, 0.0000315412, 0.0000315265, 0.0000315119, 0.0000314972, 0.0000314826, 0.0000314680, 0.0000314534, 0.0000314388, 0.0000314242, 0.0000314096, 0.0000313950, 0.0000313804, 0.0000313658, 0.0000313512, 0.0000313367, 0.0000313221, 0.0000313076, 0.0000312930, 0.0000312785, 0.0000312640, 0.0000312495, 0.0000312350, 0.0000312204, 0.0000312059, 0.0000311915, 0.0000311770, 0.0000311625, 0.0000311480, 0.0000311335, 0.0000311191, 0.0000311046, 0.0000310902, 0.0000310757, 0.0000310613, 0.0000310469, 0.0000310325, 0.0000310181, 0.0000310036, 0.0000309892, 0.0000309749, 0.0000309605, 0.0000309461, 0.0000309317, 0.0000309173, 0.0000309030, 0.0000308886, 0.0000308743, 0.0000308600, 0.0000308456, 0.0000308313, 0.0000308170, 0.0000308027, 0.0000307884, 0.0000307741, 0.0000307598, 0.0000307455, 0.0000307312, 0.0000307169, 0.0000307027, 0.0000306884, 0.0000306741, 0.0000306599, 0.0000306457, 0.0000306314, 0.0000306172, 0.0000306030, 0.0000305888, 0.0000305746, 0.0000305604, 0.0000305462, 0.0000305320, 0.0000305178, 0.0000305036, 0.0000304895, 0.0000304753, 0.0000304611, 0.0000304470, 0.0000304329, 0.0000304187, 0.0000304046, 0.0000303905, 0.0000303764, 0.0000303622, 0.0000303481, 0.0000303340, 0.0000303200, 0.0000303059, 0.0000302918, 0.0000302777, 0.0000302637, 0.0000302496, 0.0000302356, 0.0000302215, 0.0000302075, 0.0000301935, 0.0000301794, 0.0000301654, 0.0000301514, 0.0000301374, 0.0000301234, 0.0000301094, 0.0000300954, 0.0000300815, 0.0000300675, 0.0000300535, 0.0000300396, 0.0000300256, 0.0000300117, 0.0000299977, 0.0000299838, 0.0000299699, 0.0000299559, 0.0000299420, 0.0000299281, 0.0000299142, 0.0000299003, 0.0000298864, 0.0000298726, 0.0000298587, 0.0000298448, 0.0000298310, 0.0000298171, 0.0000298033, 0.0000297894, 0.0000297756, 0.0000297618, 0.0000297479, 0.0000297341, 0.0000297203, 0.0000297065, 0.0000296927, 0.0000296789, 0.0000296651, 0.0000296513, 0.0000296376, 0.0000296238, 0.0000296101, 0.0000295963, 0.0000295826, 0.0000295688, 0.0000295551, 0.0000295414, 0.0000295276, 0.0000295139, 0.0000295002, 0.0000294865, 0.0000294728, 0.0000294591, 0.0000294454, 0.0000294318, 0.0000294181, 0.0000294044, 0.0000293908, 0.0000293771, 0.0000293635, 0.0000293498, 0.0000293362, 0.0000293226, 0.0000293090, 0.0000292954, 0.0000292818, 0.0000292682, 0.0000292546, 0.0000292410, 0.0000292274, 0.0000292138, 0.0000292003, 0.0000291867, 0.0000291731, 0.0000291596, 0.0000291460, 0.0000291325, 0.0000291190, 0.0000291054, 0.0000290919, 0.0000290784, 0.0000290649, 0.0000290514, 0.0000290379, 0.0000290244, 0.0000290110, 0.0000289975, 0.0000289840, 0.0000289706, 0.0000289571, 0.0000289436, 0.0000289302, 0.0000289168, 0.0000289033, 0.0000288899, 0.0000288765, 0.0000288631, 0.0000288497, 0.0000288363, 0.0000288229, 0.0000288095, 0.0000287961, 0.0000287827, 0.0000287694, 0.0000287560, 0.0000287427, 0.0000287293, 0.0000287160, 0.0000287026, 0.0000286893, 0.0000286760, 0.0000286627, 0.0000286493, 0.0000286360, 0.0000286227, 0.0000286094, 0.0000285962, 0.0000285829, 0.0000285696, 0.0000285563, 0.0000285431, 0.0000285298, 0.0000285166, 0.0000285033, 0.0000284901, 0.0000284768, 0.0000284636, 0.0000284504, 0.0000284372, 0.0000284240, 0.0000284108, 0.0000283976, 0.0000283844, 0.0000283712, 0.0000283580, 0.0000283449, 0.0000283317, 0.0000283185, 0.0000283054, 0.0000282922, 0.0000282791, 0.0000282660, 0.0000282528, 0.0000282397, 0.0000282266, 0.0000282135, 0.0000282004, 0.0000281873, 0.0000281742, 0.0000281611, 0.0000281480, 0.0000281350, 0.0000281219, 0.0000281088, 0.0000280958, 0.0000280827, 0.0000280697, 0.0000280566, 0.0000280436, 0.0000280306, 0.0000280176, 0.0000280046, 0.0000279916, 0.0000279786, 0.0000279656, 0.0000279526, 0.0000279396, 0.0000279266, 0.0000279136, 0.0000279007, 0.0000278877, 0.0000278748, 0.0000278618, 0.0000278489, 0.0000278359, 0.0000278230, 0.0000278101, 0.0000277972, 0.0000277843, 0.0000277714, 0.0000277585, 0.0000277456, 0.0000277327, 0.0000277198, 0.0000277069, 0.0000276941, 0.0000276812, 0.0000276683, 0.0000276555, 0.0000276426, 0.0000276298, 0.0000276170, 0.0000276042, 0.0000275913, 0.0000275785, 0.0000275657, 0.0000275529, 0.0000275401, 0.0000275273, 0.0000275145, 0.0000275018, 0.0000274890, 0.0000274762, 0.0000274635, 0.0000274507, 0.0000274379, 0.0000274252, 0.0000274125, 0.0000273997, 0.0000273870, 0.0000273743, 0.0000273616, 0.0000273489, 0.0000273362, 0.0000273235, 0.0000273108, 0.0000272981, 0.0000272854, 0.0000272727, 0.0000272601, 0.0000272474, 0.0000272348, 0.0000272221, 0.0000272095, 0.0000271968, 0.0000271842, 0.0000271716, 0.0000271590, 0.0000271463, 0.0000271337, 0.0000271211, 0.0000271085, 0.0000270959, 0.0000270834, 0.0000270708, 0.0000270582, 0.0000270456, 0.0000270331, 0.0000270205, 0.0000270080, 0.0000269954, 0.0000269829, 0.0000269704, 0.0000269578, 0.0000269453, 0.0000269328, 0.0000269203, 0.0000269078, 0.0000268953, 0.0000268828, 0.0000268703, 0.0000268578, 0.0000268454, 0.0000268329, 0.0000268204, 0.0000268080, 0.0000267955, 0.0000267831, 0.0000267706, 0.0000267582, 0.0000267458, 0.0000267334, 0.0000267209, 0.0000267085, 0.0000266961, 0.0000266837, 0.0000266713, 0.0000266589, 0.0000266466, 0.0000266342, 0.0000266218, 0.0000266094, 0.0000265971, 0.0000265847, 0.0000265724, 0.0000265600, 0.0000265477, 0.0000265354, 0.0000265231, 0.0000265107, 0.0000264984, 0.0000264861, 0.0000264738, 0.0000264615, 0.0000264492, 0.0000264369, 0.0000264247, 0.0000264124, 0.0000264001, 0.0000263879, 0.0000263756, 0.0000263634, 0.0000263511, 0.0000263389, 0.0000263266, 0.0000263144, 0.0000263022, 0.0000262900, 0.0000262778, 0.0000262656, 0.0000262534, 0.0000262412, 0.0000262290, 0.0000262168, 0.0000262046, 0.0000261925, 0.0000261803, 0.0000261681, 0.0000261560, 0.0000261438, 0.0000261317, 0.0000261196, 0.0000261074, 0.0000260953, 0.0000260832, 0.0000260711, 0.0000260590, 0.0000260469, 0.0000260348, 0.0000260227, 0.0000260106, 0.0000259985, 0.0000259864, 0.0000259744, 0.0000259623, 0.0000259502, 0.0000259382, 0.0000259261, 0.0000259141, 0.0000259021, 0.0000258900, 0.0000258780, 0.0000258660, 0.0000258540, 0.0000258420, 0.0000258300, 0.0000258180, 0.0000258060, 0.0000257940, 0.0000257820, 0.0000257700, 0.0000257581, 0.0000257461, 0.0000257341, 0.0000257222, 0.0000257102, 0.0000256983, 0.0000256864, 0.0000256744, 0.0000256625, 0.0000256506, 0.0000256387, 0.0000256268, 0.0000256149, 0.0000256030, 0.0000255911, 0.0000255792, 0.0000255673, 0.0000255554, 0.0000255436, 0.0000255317, 0.0000255198, 0.0000255080, 0.0000254961, 0.0000254843, 0.0000254725, 0.0000254606, 0.0000254488, 0.0000254370, 0.0000254252, 0.0000254134, 0.0000254016, 0.0000253898, 0.0000253780, 0.0000253662, 0.0000253544, 0.0000253426, 0.0000253309, 0.0000253191, 0.0000253073, 0.0000252956, 0.0000252838, 0.0000252721, 0.0000252604, 0.0000252486, 0.0000252369, 0.0000252252, 0.0000252135, 0.0000252018, 0.0000251900, 0.0000251783, 0.0000251667, 0.0000251550, 0.0000251433, 0.0000251316, 0.0000251199, 0.0000251083, 0.0000250966, 0.0000250849, 0.0000250733, 0.0000250617, 0.0000250500, 0.0000250384, 0.0000250267, 0.0000250151, 0.0000250035, 0.0000249919, 0.0000249803, 0.0000249687, 0.0000249571, 0.0000249455, 0.0000249339, 0.0000249223, 0.0000249108, 0.0000248992, 0.0000248876, 0.0000248761, 0.0000248645, 0.0000248530, 0.0000248414, 0.0000248299, 0.0000248183, 0.0000248068, 0.0000247953, 0.0000247838, 0.0000247723, 0.0000247608, 0.0000247493, 0.0000247378, 0.0000247263, 0.0000247148, 0.0000247033, 0.0000246918, 0.0000246804, 0.0000246689, 0.0000246575, 0.0000246460, 0.0000246346, 0.0000246231, 0.0000246117, 0.0000246003, 0.0000245888, 0.0000245774, 0.0000245660, 0.0000245546, 0.0000245432, 0.0000245318, 0.0000245204, 0.0000245090, 0.0000244976, 0.0000244862, 0.0000244749, 0.0000244635, 0.0000244521, 0.0000244408, 0.0000244294, 0.0000244181, 0.0000244067, 0.0000243954, 0.0000243841, 0.0000243727, 0.0000243614, 0.0000243501, 0.0000243388, 0.0000243275, 0.0000243162, 0.0000243049, 0.0000242936, 0.0000242823, 0.0000242711, 0.0000242598, 0.0000242485, 0.0000242373, 0.0000242260, 0.0000242147, 0.0000242035, 0.0000241923, 0.0000241810, 0.0000241698, 0.0000241586, 0.0000241473, 0.0000241361, 0.0000241249, 0.0000241137, 0.0000241025, 0.0000240913, 0.0000240801, 0.0000240689, 0.0000240578, 0.0000240466, 0.0000240354, 0.0000240243, 0.0000240131, 0.0000240020, 0.0000239908, 0.0000239797, 0.0000239685, 0.0000239574, 0.0000239463, 0.0000239351, 0.0000239240, 0.0000239129, 0.0000239018, 0.0000238907, 0.0000238796, 0.0000238685, 0.0000238574, 0.0000238464, 0.0000238353, 0.0000238242, 0.0000238131, 0.0000238021, 0.0000237910, 0.0000237800, 0.0000237689, 0.0000237579, 0.0000237469, 0.0000237358, 0.0000237248, 0.0000237138, 0.0000237028, 0.0000236918, 0.0000236808, 0.0000236698, 0.0000236588, 0.0000236478, 0.0000236368, 0.0000236258, 0.0000236149, 0.0000236039, 0.0000235929, 0.0000235820, 0.0000235710, 0.0000235601, 0.0000235491, 0.0000235382, 0.0000235273, 0.0000235163, 0.0000235054, 0.0000234945, 0.0000234836, 0.0000234727, 0.0000234618, 0.0000234509, 0.0000234400, 0.0000234291, 0.0000234182, 0.0000234073, 0.0000233965, 0.0000233856, 0.0000233747, 0.0000233639, 0.0000233530, 0.0000233422, 0.0000233313, 0.0000233205, 0.0000233097, 0.0000232988, 0.0000232880, 0.0000232772, 0.0000232664, 0.0000232556, 0.0000232448, 0.0000232340, 0.0000232232, 0.0000232124, 0.0000232016, 0.0000231909, 0.0000231801, 0.0000231693, 0.0000231586, 0.0000231478, 0.0000231371, 0.0000231263, 0.0000231156, 0.0000231048, 0.0000230941, 0.0000230834, 0.0000230727, 0.0000230619, 0.0000230512, 0.0000230405, 0.0000230298, 0.0000230191, 0.0000230084, 0.0000229977, 0.0000229871, 0.0000229764, 0.0000229657, 0.0000229551, 0.0000229444, 0.0000229337, 0.0000229231, 0.0000229124, 0.0000229018, 0.0000228912, 0.0000228805, 0.0000228699, 0.0000228593, 0.0000228487, 0.0000228380, 0.0000228274, 0.0000228168, 0.0000228062, 0.0000227957, 0.0000227851, 0.0000227745, 0.0000227639, 0.0000227533, 0.0000227428, 0.0000227322, 0.0000227216, 0.0000227111, 0.0000227005, 0.0000226900, 0.0000226795, 0.0000226689, 0.0000226584, 0.0000226479, 0.0000226374, 0.0000226268, 0.0000226163, 0.0000226058, 0.0000225953, 0.0000225848, 0.0000225743, 0.0000225639, 0.0000225534, 0.0000225429, 0.0000225324, 0.0000225220, 0.0000225115, 0.0000225011, 0.0000224906, 0.0000224802, 0.0000224697, 0.0000224593, 0.0000224489, 0.0000224384, 0.0000224280, 0.0000224176, 0.0000224072, 0.0000223968, 0.0000223864, 0.0000223760, 0.0000223656, 0.0000223552, 0.0000223448, 0.0000223344, 0.0000223241, 0.0000223137, 0.0000223033, 0.0000222930, 0.0000222826, 0.0000222723, 0.0000222619, 0.0000222516, 0.0000222412, 0.0000222309, 0.0000222206, 0.0000222103, 0.0000222000, 0.0000221896, 0.0000221793, 0.0000221690, 0.0000221587, 0.0000221484, 0.0000221382, 0.0000221279, 0.0000221176, 0.0000221073, 0.0000220971, 0.0000220868, 0.0000220765, 0.0000220663, 0.0000220560, 0.0000220458, 0.0000220356, 0.0000220253, 0.0000220151, 0.0000220049, 0.0000219946, 0.0000219844, 0.0000219742, 0.0000219640, 0.0000219538, 0.0000219436, 0.0000219334, 0.0000219232, 0.0000219131, 0.0000219029, 0.0000218927, 0.0000218825, 0.0000218724, 0.0000218622, 0.0000218521, 0.0000218419, 0.0000218318, 0.0000218216, 0.0000218115, 0.0000218014, 0.0000217912, 0.0000217811, 0.0000217710, 0.0000217609, 0.0000217508, 0.0000217407, 0.0000217306, 0.0000217205, 0.0000217104, 0.0000217003, 0.0000216902, 0.0000216802, 0.0000216701, 0.0000216600, 0.0000216500, 0.0000216399, 0.0000216299, 0.0000216198, 0.0000216098, 0.0000215997, 0.0000215897, 0.0000215797, 0.0000215697, 0.0000215596, 0.0000215496, 0.0000215396, 0.0000215296, 0.0000215196, 0.0000215096, 0.0000214996, 0.0000214896, 0.0000214797, 0.0000214697, 0.0000214597, 0.0000214498, 0.0000214398, 0.0000214298, 0.0000214199, 0.0000214099, 0.0000214000, 0.0000213900, 0.0000213801, 0.0000213702, 0.0000213603, 0.0000213503, 0.0000213404, 0.0000213305, 0.0000213206, 0.0000213107, 0.0000213008, 0.0000212909, 0.0000212810, 0.0000212711, 0.0000212613, 0.0000212514, 0.0000212415, 0.0000212316, 0.0000212218, 0.0000212119, 0.0000212021, 0.0000211922, 0.0000211824, 0.0000211726, 0.0000211627, 0.0000211529, 0.0000211431, 0.0000211332, 0.0000211234, 0.0000211136, 0.0000211038, 0.0000210940, 0.0000210842, 0.0000210744, 0.0000210646, 0.0000210548, 0.0000210451, 0.0000210353, 0.0000210255, 0.0000210158};

  float sbr_et[interface::bril::shared::MAX_NBX] = {1.0, 0.02980003, 4.24787103e-03, 4.27978138e-04, 0.0010356194553775566, 0.0009626241116900353, 0.0008970334680314758, 0.0008380616834822833, 0.0007850073676633778, 0.0007372445431628781, 0.0006942145784101556, 0.0006554189867067844, 0.0006204129982767821, 0.0005887998221526117, 0.0005602255235978036, 0.0005343744506971256, 0.0005109651508240438, 0.0004897467240149576, 0.00047049556592270814, 0.00045301245806180423, 0.0004371199675605084, 0.0004226601226592312, 0.0004094923337931741, 0.00039749153331683674, 0.00038654650981078694, 0.00037655841549442016, 0.0003674394275856252, 0.0003591115465289963, 0.0003515055158848093, 0.00034455985035473376, 0.0003382199599377708, 0.0003324373595792956, 0.0003271689549132067, 0.0003223763958158554, 0.0003180254905026253, 0.0003140856738140342, 0.0003105295241668479, 0.00030733232439436695, 0.0003044716623750271, 0.00030192706795496827, 0.0002996796832125872, 0.00029771196359485894, 0.0002960074078793496, 0.0002945503152848077, 0.0002933255683691422, 0.00029231844061836494, 0.00029151442784549773, 0.0002908991026863147, 0.0002904579916010366, 0.0002901764738698165, 0.00029003970210745976, 0.0002900325438220228, 0.0002901395435058388, 0.0002903449046796468, 0.00029063249121475586, 0.00029098584713889533, 0.0002913882339932551, 0.0002918226846562138, 0.00029227207238863176, 0.0002927191936917514, 0.0002931468634071777, 0.0002935380203345325, 0.00029387584150145665, 0.0002941438630976729, 0.0002943261059844082, 0.0002944072036167066, 0.0002943725301725063, 0.0002942083266715648, 0.0002939018228913769, 0.00029344135294723906, 0.0002928164624998104, 0.00029201800568521663, 0.0002910382300283409, 0.0002898708477969957, 0.0002885110924798848, 0.0002869557593206264, 0.00028520322910895857, 0.00028325347471339736, 0.00028110805013149197, 0.00027877006212859336, 0.0002762441248277899, 0.00027353629789650705, 0.000270654009243536, 0.000267605963388642, 0.00026440203689056754, 0.00026105316241395145, 0.0002575712031779164, 0.0002539688196560738, 0.00025025933048756707, 0.0002464565696104867, 0.00024257474164240427, 0.000238628277508614, 0.00023463169225849178, 0.000230599446916513, 0.0002265458160899212, 0.00022248476290341823, 0.00021842982265664486, 0.00021439399640708738, 0.00021038965547408065, 0.00020642845764358908, 0.00020252127563322907, 0.00019867813815722878, 0.0001949081837161321, 0.0001912196270301473, 0.0001876197378417973, 0.0001841148316361468, 0.00018071027166801238, 0.00017741048154730043, 0.00017421896751745365, 0.00017113834946883274, 0.00016817039965905342, 0.00016531608806562772, 0.00016257563327202562, 0.00015994855778531222, 0.00015743374670030034, 0.00015502950865981492, 0.0001527336381110956, 0.00015054347792226955, 0.00014845598149781857, 0.00014646777361559862, 0.00014457520929783466, 0.00014277443012226582, 0.00014106141747504975, 0.00013943204234210266, 0.00013788211132842066, 0.0001364074086839796, 0.0001350037341986823, 0.00013366693690639484, 0.00013239294460852454, 0.00013117778929022577, 0.000130017628556797, 0.00012890876326399078, 0.0001278476515538347, 0.00012683091953737184, 0.00012585536888783358, 0.00012491798162264376, 0.00012401592236090746, 0.00012314653834531324, 0.0001223073575143821, 0.00012149608490345451, 0.0001207105976414557, 0.00011994893879603569, 0.00011920931030283623, 0.00011849006519604232, 0.00011778969933762892, 0.00011710684282235509, 0.00011644025121506638, 0.00011578879675665434, 0.00011515145965543616, 0.00011452731956204424, 0.00011391554730837163, 0.00011331539697487463, 0.00011272619833569752, 0.00011214734971772198, 0.00011157831129777821, 0.0001110185988518769, 0.00011046777796138505, 0.00010992545867351397, 0.00010939129060722684, 0.00010886495849061215, 0.0001083461781117996, 0.00010783469266250787, 0.00010733026945119374, 0.00010683269696141112, 0.00010634178223027661, 0.00010585734852177306, 0.0001053792332699114, 0.00010490728626742203, 0.00010444136807658536, 0.0001039813486399629, 0.0001035271060700953, 0.0001030785255986393, 0.00010263549866687359, 0.00010219792214098007, 0.0001017656976369678, 0.00010133873094153062, 0.00010091693151649393, 0.00010050021207580126, 0.00010008848822520403, 9.968167815594477e-05, 9.927970238476043e-05, 9.888248353347905e-05, 9.848994614234044e-05, 9.810201651194293e-05, 9.77186225694084e-05, 9.733969375496969e-05, 9.696516092572734e-05, 9.659495627379651e-05, 9.622901325648123e-05, 9.586726653647442e-05, 9.550965193039401e-05, 9.515610636423495e-05, 9.48065678345469e-05, 9.446097537434467e-05, 9.41192690229248e-05, 9.378138979890376e-05, 9.34472796759114e-05, 9.311688156047401e-05, 9.279013927170396e-05, 9.246699752248279e-05, 9.214740190188263e-05, 9.183129885861713e-05, 9.151863568535375e-05, 9.12093605037502e-05, 9.090342225010477e-05, 9.060077066153155e-05, 9.03013562625883e-05, 9.000513035229999e-05, 8.97120449915308e-05, 8.9422052990668e-05, 8.913510789758736e-05, 8.885116398587556e-05, 8.85701762432911e-05, 8.829210036044654e-05, 8.80168927197002e-05, 8.774451038424605e-05, 8.747491108739323e-05, 8.720805322202788e-05, 8.694389583025077e-05, 8.668239859318569e-05, 8.642352182095354e-05, 8.616722644280852e-05, 8.591347399743207e-05, 8.566222662338181e-05, 8.541344704969196e-05, 8.516709858662254e-05, 8.492314511655469e-05, 8.468155108502928e-05, 8.444228149192682e-05, 8.420530188278559e-05, 8.397057834025645e-05, 8.373807747569166e-05, 8.350776642086564e-05, 8.327961281982546e-05, 8.305358482086924e-05, 8.282965106865032e-05, 8.260778069640492e-05, 8.238794331830184e-05, 8.217010902191199e-05, 8.195424836079568e-05, 8.174033234720641e-05, 8.152833244490872e-05, 8.131822056210852e-05, 8.110996904449441e-05, 8.090355066838773e-05, 8.069893863400011e-05, 8.04961065587966e-05, 8.029502847096265e-05, 8.009567880297367e-05, 7.989803238526504e-05, 7.970206444000149e-05, 7.950775057494395e-05, 7.931506677741257e-05, 7.912398940834428e-05, 7.893449519644333e-05, 7.874656123242363e-05, 7.856016496334123e-05, 7.837528418701539e-05, 7.819189704653736e-05, 7.800998202486498e-05, 7.782951793950196e-05, 7.76504839372605e-05, 7.747285948910603e-05, 7.729662438508273e-05, 7.712175872931827e-05, 7.694824293510729e-05, 7.677605772007125e-05, 7.660518410139472e-05, 7.643560339113588e-05, 7.626729719161072e-05, 7.610024739084925e-05, 7.59344361581233e-05, 7.576984593954399e-05, 7.560645945372834e-05, 7.544425968753382e-05, 7.528322989185955e-05, 7.512335357751332e-05, 7.496461451114353e-05, 7.480699671123449e-05, 7.465048444416489e-05, 7.449506222032764e-05, 7.434071479031064e-05, 7.418742714113744e-05, 7.403518449256664e-05, 7.388397229344938e-05, 7.373377621814383e-05, 7.358458216298582e-05, 7.34363762428148e-05, 7.328914478755424e-05, 7.314287433884546e-05, 7.299755164673442e-05, 7.285316366641014e-05, 7.270969755499441e-05, 7.256714066838171e-05, 7.242548055812862e-05, 7.228470496839196e-05, 7.21448018329149e-05, 7.20057592720603e-05, 7.186756558989052e-05, 7.173020927129286e-05, 7.159367897915016e-05, 7.145796355155569e-05, 7.132305199907163e-05, 7.118893350203051e-05, 7.105559740787893e-05, 7.092303322856282e-05, 7.079123063795381e-05, 7.066017946931572e-05, 7.052986971281089e-05, 7.040029151304545e-05, 7.027143516665318e-05, 7.0143291119917e-05, 7.001584996642797e-05, 6.988910244478083e-05, 6.976303943630554e-05, 6.96376519628345e-05, 6.951293118450481e-05, 6.938886839759459e-05, 6.926545503239372e-05, 6.914268265110742e-05, 6.902054294579302e-05, 6.889902773632893e-05, 6.877812896841533e-05, 6.865783871160634e-05, 6.853814915737287e-05, 6.841905261719597e-05, 6.830054152068979e-05, 6.81826084137543e-05, 6.806524595675658e-05, 6.794844692274082e-05, 6.78322041956663e-05, 6.771651076867299e-05, 6.760135974237416e-05, 6.748674432317609e-05, 6.737265782162363e-05, 6.725909365077203e-05, 6.714604532458425e-05, 6.703350645635302e-05, 6.69214707571481e-05, 6.680993203428753e-05, 6.669888418983302e-05, 6.658832121910865e-05, 6.647823720924302e-05, 6.636862633773421e-05, 6.625948287103705e-05, 6.615080116317262e-05, 6.604257565435965e-05, 6.593480086966705e-05, 6.582747141768788e-05, 6.572058198923382e-05, 6.561412735605033e-05, 6.55081023695517e-05, 6.5402501959576e-05, 6.529732113315966e-05, 6.519255497333101e-05, 6.508819863792292e-05, 6.49842473584039e-05, 6.48806964387276e-05, 6.477754125420027e-05, 6.46747772503659e-05, 6.457239994190906e-05, 6.447040491157456e-05, 6.436878780910443e-05, 6.426754435019116e-05, 6.416667031544763e-05, 6.406616154939306e-05, 6.396601395945481e-05, 6.386622351498582e-05, 6.376678624629753e-05, 6.366769824370785e-05, 6.356895565660406e-05, 6.347055469252047e-05, 6.337249161623027e-05, 6.327476274885198e-05, 6.317736446696962e-05, 6.308029320176668e-05, 6.298354543817385e-05, 6.288711771402975e-05, 6.279100661925524e-05, 6.269520879504021e-05, 6.259972093304354e-05, 6.250453977460516e-05, 6.240966210997083e-05, 6.231508477752883e-05, 6.222080466305861e-05, 6.21268186989914e-05, 6.20331238636821e-05, 6.193971718069286e-05, 6.184659571808772e-05, 6.175375658773832e-05, 6.166119694464038e-05, 6.156891398624126e-05, 6.147690495177753e-05, 6.13851671216233e-05, 6.129369781664855e-05, 6.120249439758766e-05, 6.111155426441772e-05, 6.102087485574652e-05, 6.093045364821034e-05, 6.084028815588098e-05, 6.075037592968208e-05, 6.066071455681475e-05, 6.057130166019196e-05, 6.048213489788198e-05, 6.039321196256042e-05, 6.030453058097089e-05, 6.021608851339414e-05, 6.0127883553125414e-05, 6.0039913525960104e-05, 5.9952176289687395e-05, 5.986466973359181e-05, 5.977739177796262e-05, 5.9690340373610935e-05, 5.9603513501394263e-05, 5.9516909171748694e-05, 5.943052542422817e-05, 5.934436032705121e-05, 5.92584119766545e-05, 5.9172678497253724e-05, 5.908715804041105e-05, 5.900184878460948e-05, 5.8916748934833864e-05, 5.8831856722158534e-05, 5.87471704033412e-05, 5.8662688260423495e-05, 5.857840860033748e-05, 5.849432975451846e-05, 5.841045007852394e-05, 5.832676795165834e-05, 5.824328177660375e-05, 5.815998997905638e-05, 5.8076891007368795e-05, 5.79939833321976e-05, 5.791126544615682e-05, 5.7828735863476526e-05, 5.7746393119666966e-05, 5.766423577118784e-05, 5.758226239512281e-05, 5.750047158885912e-05, 5.7418861969772114e-05, 5.733743217491487e-05, 5.725618086071256e-05, 5.717510670266165e-05, 5.7094208395033766e-05, 5.7013484650584264e-05, 5.6932934200265306e-05, 5.685255579294344e-05, 5.6772348195121564e-05, 5.669231019066539e-05, 5.661244058053397e-05, 5.6532738182514586e-05, 5.645320183096181e-05, 5.6373830376540495e-05, 5.6294622685972954e-05, 5.6215577641790015e-05, 5.613669414208599e-05, 5.605797110027744e-05, 5.597940744486578e-05, 5.5901002119203595e-05, 5.582275408126452e-05, 5.5744662303416856e-05, 5.56667257722006e-05, 5.558894348810808e-05, 5.551131446536793e-05, 5.543383773173253e-05, 5.535651232826867e-05, 5.527933730915158e-05, 5.5202311741462164e-05, 5.5125434704987364e-05, 5.504870529202366e-05, 5.4972122607183725e-05, 5.4895685767206e-05, 5.481939390076727e-05, 5.4743246148298244e-05, 5.466724166180196e-05, 5.459137960467498e-05, 5.451565915153152e-05, 5.4440079488030226e-05, 5.436463981070368e-05, 5.428933932679057e-05, 5.4214177254070565e-05, 5.413915282070164e-05, 5.4064265265060015e-05, 5.3989513835582665e-05, 5.39148977906121e-05, 5.384041639824387e-05, 5.37660689361761e-05, 5.369185469156169e-05, 5.3617772960862646e-05, 5.354382304970677e-05, 5.3470004272746526e-05, 5.339631595352019e-05, 5.332275742431512e-05, 5.324932802603316e-05, 5.317602710805819e-05, 5.310285402812573e-05, 5.302980815219455e-05, 5.2956888854320346e-05, 5.288409551653134e-05, 5.2811427528705844e-05, 5.2738884288451785e-05, 5.266646520098803e-05, 5.2594169679027574e-05, 5.252199714266268e-05, 5.244994701925166e-05, 5.23780187433075e-05, 5.230621175638823e-05, 5.2234525506988966e-05, 5.216295945043572e-05, 5.209151304878072e-05, 5.20201857706996e-05, 5.194897709138995e-05, 5.1877886492471625e-05, 5.18069134618885e-05, 5.173605749381189e-05, 5.166531808854534e-05, 5.159469475243094e-05, 5.152418699775724e-05, 5.145379434266838e-05, 5.138351631107481e-05, 5.131335243256535e-05, 5.124330224232059e-05, 5.117336528102772e-05, 5.1103541094796636e-05, 5.1033829235077344e-05, 5.0964229258578696e-05, 5.089474072718841e-05, 5.0825363207894264e-05, 5.075609627270657e-05, 5.068693949858191e-05, 5.061789246734794e-05, 5.0548954765629504e-05, 5.048012598477576e-05, 5.041140572078859e-05, 5.034279357425205e-05, 5.027428915026288e-05, 5.020589205836223e-05, 5.01376019124683e-05, 5.006941833081013e-05, 5.0001340935862446e-05, 4.99333693542814e-05, 4.9865503216841445e-05, 4.979774215837314e-05, 4.973008581770188e-05, 4.96625338375877e-05, 4.9595085864665864e-05, 4.9527741549388574e-05, 4.946050054596738e-05, 4.9393362512316646e-05, 4.932632710999785e-05, 4.925939400416471e-05, 4.919256286350921e-05, 4.9125833360208506e-05, 4.9059205169872566e-05, 4.89926779714927e-05, 4.8926251447390875e-05, 4.88599252831698e-05, 4.879369916766381e-05, 4.8727572792890534e-05, 4.866154585400328e-05, 4.859561804924418e-05, 4.852978907989806e-05, 4.846405865024703e-05, 4.8398426467525844e-05, 4.833289224187779e-05, 4.826745568631149e-05, 4.820211651665818e-05, 4.813687445152979e-05, 4.807172921227758e-05, 4.8006680522951515e-05, 4.794172811026019e-05, 4.7876871703531386e-05, 4.781211103467336e-05, 4.774744583813657e-05, 4.768287585087608e-05, 4.761840081231458e-05, 4.755402046430593e-05, 4.748973455109925e-05, 4.7425542819303714e-05, 4.736144501785365e-05, 4.729744089797444e-05, 4.723353021314875e-05, 4.716971271908342e-05, 4.710598817367681e-05, 4.7042356336986605e-05, 4.697881697119829e-05, 4.6915369840593894e-05, 4.6852014711521394e-05, 4.678875135236452e-05, 4.672557953351305e-05, 4.666249902733353e-05, 4.659950960814051e-05, 4.6536611052168225e-05, 4.647380313754259e-05, 4.6411085644253855e-05, 4.6348458354129456e-05, 4.628592105080749e-05, 4.6223473519710395e-05, 4.616111554801926e-05, 4.6098846924648364e-05, 4.603666744022015e-05, 4.597457688704069e-05, 4.591257505907539e-05, 4.5850661751925125e-05, 4.578883676280282e-05, 4.572709989051025e-05, 4.566545093541535e-05, 4.560388969942975e-05, 4.5542415985986744e-05, 4.5481029600019607e-05, 4.541973034794014e-05, 4.535851803761767e-05, 4.529739247835834e-05, 4.52363534808847e-05, 4.5175400857315576e-05, 4.5114534421146404e-05, 4.5053753987229684e-05, 4.499305937175581e-05, 4.4932450392234294e-05, 4.487192686747511e-05, 4.481148861757046e-05, 4.475113546387672e-05, 4.469086722899681e-05, 4.4630683736762674e-05, 4.45705848122181e-05, 4.451057028160188e-05, 4.445063997233112e-05, 4.439079371298482e-05, 4.433103133328781e-05, 4.427135266409484e-05, 4.421175753737488e-05, 4.415224578619582e-05, 4.409281724470927e-05, 4.4033471748135595e-05, 4.397420913274932e-05, 4.3915029235864566e-05, 4.385593189582087e-05, 4.379691695196915e-05, 4.3737984244657894e-05, 4.367913361521956e-05, 4.362036490595724e-05, 4.3561677960131446e-05, 4.3503072621947145e-05, 4.344454873654101e-05, 4.338610614996882e-05, 4.332774470919312e-05, 4.3269464262070996e-05, 4.3211264657342106e-05, 4.315314574461686e-05, 4.309510737436478e-05, 4.303714939790303e-05, 4.2979271667385226e-05, 4.2921474035790226e-05, 4.2863756356911265e-05, 4.28061184853452e-05, 4.274856027648187e-05, 4.26910815864937e-05, 4.2633682272325425e-05, 4.2576362191683994e-05, 4.251912120302855e-05, 4.2461959165560696e-05, 4.240487593921482e-05, 4.2347871384648594e-05, 4.229094536323359e-05, 4.2234097737046086e-05, 4.2177328368858024e-05, 4.212063712212802e-05, 4.2064023860992605e-05, 4.200748845025757e-05, 4.195103075538941e-05, 4.189465064250694e-05, 4.183834797837306e-05, 4.178212263038656e-05, 4.172597446657413e-05, 4.166990335558248e-05, 4.161390916667058e-05, 4.155799176970193e-05, 4.1502151035137125e-05, 4.144638683402638e-05, 4.139069903800222e-05, 4.133508751927231e-05, 4.1279552150612384e-05, 4.1224092805359206e-05, 4.116870935740377e-05, 4.11134016811845e-05, 4.105816965168062e-05, 4.100301314440553e-05, 4.094793203540041e-05, 4.089292620122786e-05, 4.08379955189656e-05, 4.078313986620032e-05, 4.072835912102162e-05, 4.0673653162016e-05, 4.061902186826099e-05, 4.056446511931938e-05, 4.050998279523344e-05, 4.045557477651933e-05, 4.040124094416159e-05, 4.034698117960765e-05, 4.029279536476245e-05, 4.0238683381983174e-05, 4.0184645114074045e-05, 4.013068044428117e-05, 4.007678925628751e-05, 4.002297143420787e-05, 3.996922686258407e-05, 3.991555542638e-05, 3.9861957010976986e-05, 3.980843150216906e-05, 3.9754978786158324e-05, 3.9701598749550454e-05, 3.9648291279350185e-05, 3.959505626295695e-05, 3.9541893588160525e-05, 3.948880314313673e-05, 3.9435784816443256e-05, 3.938283849701552e-05, 3.9329964074162545e-05, 3.927716143756299e-05, 3.922443047726114e-05, 3.917177108366304e-05, 3.911918314753267e-05, 3.906666655998809e-05, 3.9014221212497785e-05, 3.8961846996876954e-05, 3.8909543805283915e-05, 3.8857311530216524e-05, 3.880515006450867e-05, 3.875305930132684e-05, 3.870103913416669e-05, 3.8649089456849686e-05, 3.859721016351985e-05, 3.854540114864043e-05, 3.849366230699076e-05, 3.844199353366307e-05, 3.839039472405938e-05, 3.8338865773888424e-05, 3.828740657916269e-05, 3.8236017036195355e-05, 3.818469704159742e-05, 3.81334464922748e-05, 3.8082265285425496e-05, 3.803115331853678e-05, 3.798011048938242e-05, 3.792913669602001e-05, 3.7878231836788244e-05, 3.78273958103043e-05, 3.777662851546123e-05, 3.77259298514254e-05, 3.7675299717634e-05, 3.762473801379247e-05, 3.7574244639872145e-05, 3.752381949610782e-05, 3.74734624829953e-05, 3.7423173501289145e-05, 3.737295245200031e-05, 3.732279923639391e-05, 3.7272713755986916e-05, 3.722269591254601e-05, 3.717274560808538e-05, 3.712286274486456e-05, 3.707304722538631e-05, 3.702329895239462e-05, 3.6973617828872535e-05, 3.692400375804019e-05, 3.687445664335285e-05, 3.682497638849887e-05, 3.6775562897397814e-05, 3.672621607419854e-05, 3.6676935823277275e-05, 3.662772204923584e-05, 3.657857465689976e-05, 3.6529493551316484e-05, 3.6480478637753594e-05, 3.6431529821697085e-05, 3.638264700884964e-05, 3.63338301051289e-05, 3.628507901666581e-05, 3.623639364980297e-05, 3.618777391109301e-05, 3.613921970729695e-05, 3.60907309453827e-05, 3.604230753252339e-05, 3.599394937609594e-05, 3.5945656383679474e-05, 3.589742846305386e-05, 3.5849265522198224e-05, 3.58011674692895e-05, 3.5753134212701026e-05, 3.5705165661001076e-05, 3.565726172295153e-05, 3.5609422307506474e-05, 3.5561647323810827e-05, 3.5513936681199066e-05, 3.546629028919386e-05, 3.5418708057504784e-05, 3.537118989602707e-05, 3.5323735714840294e-05, 3.5276345424207215e-05, 3.522901893457244e-05, 3.518175615656132e-05, 3.51345570009787e-05, 3.5087421378807753e-05, 3.504034920120883e-05, 3.4993340379518315e-05, 3.4946394825247516e-05, 3.4899512450081487e-05, 3.485269316587801e-05, 3.480593688466648e-05, 3.475924351864684e-05, 3.471261298018851e-05, 3.466604518182941e-05, 3.461954003627484e-05, 3.457309745639656e-05, 3.4526717355231756e-05, 3.448039964598203e-05, 3.443414424201251e-05, 3.4387951056850794e-05, 3.4341820004186096e-05, 3.4295750997868225e-05, 3.424974395190677e-05, 3.420379878047009e-05, 3.415791539788452e-05, 3.411209371863339e-05, 3.4066333657356236e-05, 3.402063512884789e-05, 3.397499804805763e-05, 3.392942233008841e-05, 3.3883907890195946e-05, 3.383845464378795e-05, 3.379306250642333e-05, 3.374773139381139e-05, 3.3702461221810984e-05, 3.3657251906429865e-05, 3.361210336382382e-05, 3.356701551029596e-05, 3.352198826229596e-05, 3.3477021536419354e-05, 3.343211524940675e-05, 3.338726931814319e-05, 3.3342483659657396e-05, 3.329775819112107e-05, 3.325309282984823e-05, 3.320848749329452e-05, 3.3163942099056543e-05, 3.3119456564871155e-05, 3.307503080861491e-05, 3.303066474830329e-05, 3.2986358302090165e-05, 3.2942111388267124e-05, 3.289792392526284e-05, 3.285379583164249e-05, 3.2809727026107086e-05, 3.2765717427492965e-05, 3.272176695477111e-05, 3.267787552704662e-05, 3.2634043063558124e-05, 3.2590269483677186e-05, 3.254655470690775e-05, 3.2502898652885594e-05, 3.245930124137777e-05, 3.241576239228208e-05, 3.237228202562649e-05, 3.232886006156867e-05, 3.228549642039538e-05, 3.2242191022522026e-05, 3.2198943788492134e-05, 3.2155754638976816e-05, 3.2112623494774284e-05, 3.206955027680935e-05, 3.202653490613296e-05, 3.1983577303921676e-05, 3.1940677391477216e-05, 3.189783509022599e-05, 3.185505032171863e-05, 3.1812323007629487e-05, 3.1769653069756246e-05, 3.1727040430019436e-05, 3.168448501046195e-05, 3.164198673324869e-05, 3.1599545520666034e-05, 3.15571612951215e-05, 3.151483397914322e-05, 3.147256349537961e-05, 3.143034976659887e-05, 3.138819271568866e-05, 3.134609226565561e-05, 3.130404833962496e-05, 3.1262060860840164e-05, 3.122012975266247e-05, 3.1178254938570544e-05, 3.113643634216008e-05, 3.1094673887143405e-05, 3.105296749734914e-05, 3.101131709672181e-05, 3.09697226093214e-05, 3.092818395932312e-05, 3.088670107101694e-05, 3.084527386880725e-05, 3.080390227721257e-05, 3.0762586220865085e-05, 3.07213256245104e-05, 3.068012041300714e-05, 3.0638970511326625e-05, 3.0597875844552515e-05, 3.055683633788051e-05, 3.0515851916618015e-05, 3.0474922506183737e-05, 3.0434048032107477e-05, 3.0393228420029733e-05, 3.0352463595701392e-05, 3.0311753484983427e-05, 3.027109801384658e-05, 3.0230497108371062e-05, 3.0189950694746233e-05, 3.0149458699270294e-05, 3.0109021048350023e-05, 3.0068637668500415e-05, 3.0028308486344448e-05, 2.9988033428612764e-05, 2.9947812422143373e-05, 2.9907645393881384e-05, 2.98675322708787e-05, 2.9827472980293773e-05, 2.9787467449391262e-05, 2.974751560554182e-05, 2.9707617376221803e-05, 2.9667772689012972e-05, 2.962798147160224e-05, 2.9588243651781434e-05, 2.954855915744696e-05, 2.9508927916599626e-05, 2.946934985734432e-05, 2.9429824907889777e-05, 2.9390352996548324e-05, 2.9350934051735617e-05, 2.93115680019704e-05, 2.9272254775874226e-05, 2.9232994302171272e-05, 2.9193786509688042e-05, 2.9154631327353127e-05, 2.9115528684196998e-05, 2.9076478509351725e-05, 2.9037480732050762e-05, 2.8998535281628713e-05, 2.8959642087521114e-05, 2.892080107926414e-05, 2.8882012186494465e-05, 2.8843275338948945e-05, 2.880459046646447e-05, 2.876595749897767e-05, 2.8727376366524753e-05, 2.8688846999241242e-05, 2.8650369327361776e-05, 2.861194328121988e-05, 2.857356879124776e-05, 2.853524578797609e-05, 2.8496974202033777e-05, 2.8458753964147792e-05, 2.8420585005142902e-05, 2.8382467255941516e-05, 2.8344400647563453e-05, 2.8306385111125735e-05, 2.8268420577842387e-05, 2.823050697902423e-05, 2.819264424607871e-05, 2.8154832310509642e-05, 2.8117071103917054e-05, 2.8079360557996996e-05, 2.8041700604541305e-05, 2.800409117543744e-05, 2.7966532202668287e-05, 2.7929023618311953e-05, 2.7891565354541598e-05, 2.7854157343625225e-05, 2.78167995179255e-05, 2.7779491809899554e-05, 2.7742234152098836e-05, 2.770502647716886e-05, 2.7667868717849105e-05, 2.7630760806972765e-05, 2.75937026774666e-05, 2.755669426235074e-05, 2.7519735494738523e-05, 2.7482826307836308e-05, 2.744596663494331e-05, 2.7409156409451388e-05, 2.7372395564844926e-05, 2.7335684034700615e-05, 2.729902175268729e-05, 2.7262408652565767e-05, 2.7225844668188676e-05, 2.7189329733500287e-05, 2.7152863782536316e-05, 2.711644674942381e-05, 2.7080078568380932e-05, 2.7043759173716806e-05, 2.7007488499831383e-05, 2.6971266481215227e-05, 2.6935093052449395e-05, 2.6898968148205257e-05, 2.6862891703244333e-05, 2.6826863652418126e-05, 2.679088393066798e-05, 2.6754952473024905e-05, 2.671906921460943e-05, 2.668323409063144e-05, 2.6647447036390023e-05, 2.661170798727331e-05, 2.657601687875831e-05, 2.6540373646410792e-05, 2.6504778225885083e-05, 2.646923055292396e-05, 2.6433730563358465e-05, 2.639827819310776e-05, 2.636287337817901e-05, 2.6327516054667166e-05, 2.629220615875488e-05, 2.6256943626712343e-05, 2.6221728394897097e-05, 2.6186560399753938e-05, 2.615143957781475e-05, 2.611636586569834e-05, 2.6081339200110325e-05, 2.604635951784297e-05, 2.6011426755775055e-05, 2.5976540850871715e-05, 2.5941701740184307e-05, 2.590690936085028e-05, 2.5872163650092998e-05, 2.583746454522165e-05, 2.5802811983631054e-05, 2.5768205902801575e-05, 2.573364624029894e-05, 2.5699132933774122e-05, 2.566466592096319e-05, 2.563024513968718e-05, 2.559587052785196e-05, 2.5561542023448092e-05, 2.5527259564550688e-05, 2.5493023089319286e-05, 2.5458832535997714e-05, 2.5424687842913933e-05, 2.5390588948479943e-05, 2.5356535791191625e-05, 2.5322528309628605e-05, 2.5288566442454135e-05, 2.5254650128414973e-05, 2.5220779306341192e-05, 2.518695391514613e-05, 2.515317389382621e-05, 2.5119439181460816e-05, 2.508574971721219e-05, 2.505210544032525e-05, 2.501850629012753e-05, 2.4984952206028985e-05, 2.495144312752192e-05, 2.4917978994180813e-05, 2.488455974566224e-05, 2.4851185321704712e-05, 2.4817855662128547e-05, 2.4784570706835775e-05, 2.475133039580998e-05, 2.4718134669116193e-05, 2.4684983466900785e-05, 2.4651876729391294e-05, 2.4618814396896353e-05, 2.458579640980553e-05, 2.4552822708589234e-05, 2.451989323379857e-05, 2.448700792606522e-05, 2.4454166726101356e-05, 2.4421369574699456e-05, 2.4388616412732243e-05, 2.4355907181152535e-05, 2.4323241820993115e-05, 2.429062027336665e-05, 2.4258042479465523e-05, 2.4225508380561765e-05, 2.419301791800689e-05, 2.416057103323182e-05, 2.4128167667746708e-05, 2.4095807763140893e-05, 2.406349126108273e-05, 2.4031218103319497e-05, 2.3998988231677262e-05, 2.396680158806078e-05, 2.3934658114453387e-05, 2.3902557752916846e-05, 2.3870500445591268e-05, 2.383848613469499e-05, 2.3806514762524455e-05, 2.3774586271454093e-05, 2.3742700603936218e-05, 2.37108577025009e-05, 2.3679057509755867e-05, 2.3647299968386386e-05, 2.3615585021155143e-05, 2.358391261090214e-05, 2.355228268054459e-05, 2.3520695173076776e-05, 2.3489150031569967e-05, 2.3457647199172292e-05, 2.3426186619108644e-05, 2.3394768234680552e-05, 2.336339198926608e-05, 2.333205782631971e-05, 2.3300765689372248e-05, 2.3269515522030684e-05, 2.323830726797811e-05, 2.3207140870973608e-05, 2.3176016274852125e-05, 2.3144933423524384e-05, 2.3113892260976764e-05, 2.3082892731271167e-05, 2.3051934778544975e-05, 2.302101834701088e-05, 2.2990143380956798e-05, 2.295930982474579e-05, 2.2928517622815888e-05, 2.2897766719680073e-05, 2.2867057059926082e-05, 2.283638858821637e-05, 2.2805761249287977e-05, 2.277517498795241e-05, 2.2744629749095564e-05, 2.27141254776776e-05, 2.2683662118732827e-05, 2.2653239617369642e-05, 2.2622857918770375e-05, 2.259251696819121e-05, 2.2562216710962084e-05, 2.2531957092486568e-05, 2.2501738058241774e-05, 2.247155955377825e-05, 2.244142152471987e-05, 2.2411323916763745e-05, 2.23812666756801e-05, 2.235124974731219e-05, 2.232127307757619e-05, 2.2291336612461087e-05, 2.226144029802859e-05, 2.2231584080413007e-05, 2.2201767905821182e-05, 2.217199172053235e-05, 2.2142255470898067e-05, 2.2112559103342096e-05, 2.2082902564360282e-05, 2.2053285800520515e-05, 2.202370875846257e-05, 2.199417138489803e-05, 2.196467362661019e-05, 2.1935215430453942e-05, 2.1905796743355683e-05, 2.1876417512313227e-05, 2.1847077684395693e-05, 2.1817777206743402e-05, 2.178851602656778e-05, 2.1759294091151286e-05, 2.1730111347847274e-05, 2.1700967744079898e-05, 2.1671863227344057e-05, 2.1642797745205242e-05, 2.161377124529948e-05, 2.1584783675333227e-05, 2.155583498308323e-05, 2.1526925116396503e-05, 2.1498054023190157e-05, 2.146922165145135e-05, 2.1440427949237186e-05, 2.141167286467459e-05, 2.138295634596024e-05, 2.1354278341360468e-05, 2.1325638799211136e-05, 2.1297037667917582e-05, 2.1268474895954498e-05, 2.1239950431865826e-05, 2.12114642242647e-05, 2.11830162218333e-05, 2.1154606373322814e-05, 2.1126234627553288e-05, 2.1097900933413557e-05, 2.106960523986117e-05, 2.1041347495922263e-05, 2.1013127650691473e-05, 2.098494565333186e-05, 2.0956801453074784e-05, 2.0928694999219847e-05, 2.090062624113476e-05, 2.0872595128255297e-05, 2.0844601610085153e-05, 2.0816645636195875e-05, 2.0788727156226788e-05, 2.0760846119884844e-05, 2.07330024769446e-05, 2.070519617724808e-05, 2.0677427170704685e-05, 2.0649695407291127e-05, 2.0622000837051318e-05, 2.0594343410096258e-05, 2.0566723076603993e-05, 2.0539139786819487e-05, 2.0511593491054533e-05, 2.0484084139687675e-05, 2.0456611683164114e-05, 2.04291760719956e-05, 2.0401777256760368e-05, 2.0374415188103016e-05, 2.034708981673446e-05, 2.0319801093431787e-05, 2.0292548969038208e-05, 2.0265333394462953e-05, 2.0238154320681185e-05, 2.0211011698733887e-05, 2.0183905479727808e-05, 2.0156835614835357e-05, 2.0129802055294515e-05, 2.010280475240873e-05, 2.0075843657546865e-05, 2.004891872214306e-05, 2.002202989769669e-05, 1.9995177135772248e-05, 1.9968360387999265e-05, 1.994157960607222e-05, 1.9914834741750456e-05, 1.9888125746858083e-05, 1.9861452573283898e-05, 1.9834815172981285e-05, 1.9808213497968163e-05, 1.978164750032684e-05, 1.9755117132203977e-05, 1.972862234581048e-05, 1.9702163093421403e-05, 1.9675739327375878e-05, 1.964935100007703e-05, 1.9622998063991865e-05, 1.959668047165122e-05, 1.957039817564964e-05, 1.9544151128645322e-05, 1.951793928336e-05, 1.9491762592578884e-05, 1.9465621009150564e-05, 1.9439514485986926e-05, 1.9413442976063056e-05, 1.9387406432417174e-05, 1.9361404808150515e-05, 1.933543805642729e-05, 1.930950613047457e-05, 1.92836089835822e-05, 1.9257746569102724e-05, 1.9231918840451305e-05, 1.920612575110563e-05, 1.918036725460582e-05, 1.9154643304554354e-05, 1.9128953854615994e-05, 1.9103298858517682e-05, 1.9077678270048474e-05, 1.9052092043059436e-05, 1.902654013146359e-05, 1.9001022489235778e-05, 1.897553907041264e-05, 1.895008982909249e-05, 1.892467471943525e-05, 1.8899293695662364e-05, 1.8873946712056704e-05, 1.8848633722962493e-05, 1.882335468278523e-05, 1.8798109545991607e-05, 1.8772898267109416e-05, 1.8747720800727463e-05, 1.8722577101495513e-05, 1.869746712412417e-05, 1.8672390823384818e-05, 1.8647348154109542e-05, 1.862233907119103e-05, 1.85973635295825e-05, 1.857242148429763e-05, 1.8547512890410458e-05, 1.8522637703055285e-05, 1.849779587742665e-05, 1.84729873687792e-05, 1.8448212132427618e-05, 1.842347012374655e-05, 1.8398761298170516e-05, 1.8374085611193865e-05, 1.834944301837061e-05, 1.832483347531445e-05, 1.8300256937698617e-05, 1.8275713361255822e-05, 1.8251202701778182e-05, 1.8226724915117122e-05, 1.8202279957183293e-05, 1.8177867783946527e-05, 1.8153488351435717e-05, 1.8129141615738754e-05, 1.810482753300245e-05, 1.8080546059432453e-05, 1.8056297151293173e-05, 1.8032080764907694e-05, 1.8007896856657705e-05, 1.7983745382983415e-05, 1.7959626300383478e-05, 1.7935539565414914e-05, 1.7911485134693023e-05, 1.788746296489131e-05, 1.7863473012741417e-05, 1.7839515235033034e-05, 1.7815589588613813e-05, 1.779169603038932e-05, 1.7767834517322922e-05, 1.7744005006435724e-05, 1.7720207454806497e-05, 1.7696441819571598e-05, 1.767270805792488e-05, 1.764900612711763e-05, 1.762533598445848e-05, 1.760169758731335e-05, 1.7578090893105342e-05, 1.7554515859314678e-05, 1.7530972443478633e-05, 1.7507460603191432e-05, 1.7483980296104205e-05, 1.7460531479924888e-05, 1.7437114112418153e-05, 1.7413728151405332e-05, 1.7390373554764332e-05, 1.7367050280429585e-05, 1.7343758286391943e-05, 1.7320497530698613e-05, 1.7297267971453086e-05, 1.7274069566815064e-05, 1.7250902275000356e-05, 1.722776605428084e-05, 1.7204660862984365e-05, 1.7181586659494693e-05, 1.71585434022514e-05, 1.7135531049749827e-05, 1.711254956054097e-05, 1.7089598893231453e-05, 1.706667900648341e-05, 1.704378985901444e-05, 1.7020931409597503e-05, 1.6998103617060884e-05, 1.6975306440288085e-05, 1.695253983821776e-05, 1.6929803769843648e-05, 1.6907098194214498e-05, 1.688442307043399e-05, 1.686177835766066e-05, 1.6839164015107837e-05, 1.681658000204355e-05, 1.6794026277790472e-05, 1.677150280172585e-05, 1.67490095332814e-05, 1.6726546431943285e-05, 1.670411345725199e-05, 1.6681710568802286e-05, 1.6659337726243124e-05, 1.663699488927761e-05, 1.6614682017662876e-05, 1.6592399071210048e-05, 1.6570146009784167e-05, 1.6547922793304097e-05, 1.652572938174247e-05, 1.650356573512561e-05, 1.6481431813533458e-05, 1.645932757709951e-05, 1.643725298601072e-05, 1.6415208000507468e-05, 1.639319258088345e-05, 1.6371206687485628e-05, 1.634925028071414e-05, 1.632732332102227e-05, 1.6305425768916316e-05, 1.6283557584955563e-05, 1.6261718729752206e-05, 1.623990916397127e-05, 1.6218128848330527e-05, 1.619637774360045e-05, 1.6174655810604137e-05, 1.6152963010217228e-05, 1.6131299303367838e-05, 1.61096646510365e-05, 1.6088059014256073e-05, 1.606648235411169e-05, 1.604493463174068e-05, 1.60234158083325e-05, 1.6001925845128663e-05, 1.5980464703422664e-05, 1.5959032344559926e-05, 1.5937628729937704e-05, 1.5916253821005048e-05, 1.58949075792627e-05, 1.5873589966263056e-05, 1.585230094361007e-05, 1.58310404729592e-05, 1.5809808516017344e-05, 1.578860503454274e-05, 1.5767429990344945e-05, 1.5746283345284723e-05, 1.5725165061273998e-05, 1.5704075100275778e-05, 1.5683013424304107e-05, 1.5661979995423947e-05, 1.5640974775751164e-05, 1.561999772745244e-05, 1.5599048812745178e-05, 1.5578127993897486e-05, 1.5557235233228062e-05, 1.553637049310615e-05, 1.5515533735951472e-05, 1.549472492423414e-05, 1.5473944020474624e-05, 1.5453190987243652e-05, 1.543246578716215e-05, 1.5411768382901193e-05, 1.5391098737181902e-05, 1.5370456812775422e-05, 1.534984257250282e-05, 1.5329255979235033e-05, 1.530869699589279e-05, 1.5288165585446562e-05, 1.5267661710916483e-05, 1.5247185335372276e-05, 1.5226736421933208e-05, 1.5206314933768015e-05, 1.518592083409482e-05, 1.5165554086181093e-05, 1.5145214653343563e-05, 1.5124902498948158e-05, 1.5104617586409945e-05, 1.508435987919306e-05, 1.5064129340810648e-05, 1.5043925934824775e-05, 1.50237496248464e-05, 1.5003600374535277e-05, 1.498347814759989e-05, 1.4963382907797418e-05, 1.4943314618933639e-05, 1.4923273244862882e-05, 1.4903258749487945e-05, 1.4883271096760061e-05, 1.4863310250678784e-05, 1.4843376175291971e-05, 1.4823468834695701e-05, 1.4803588193034196e-05, 1.4783734214499783e-05, 1.4763906863332801e-05, 1.4744106103821567e-05, 1.472433190030227e-05, 1.4704584217158959e-05, 1.4684863018823431e-05, 1.4665168269775207e-05, 1.4645499934541428e-05, 1.4625857977696835e-05, 1.4606242363863654e-05, 1.4586653057711584e-05, 1.4567090023957701e-05, 1.4547553227366397e-05, 1.4528042632749335e-05, 1.4508558204965358e-05, 1.448909990892046e-05, 1.4469667709567678e-05, 1.4450261571907076e-05, 1.4430881460985656e-05, 1.4411527341897287e-05, 1.439219917978267e-05, 1.437289693982925e-05, 1.4353620587271169e-05, 1.4334370087389186e-05, 1.431514540551064e-05, 1.429594650700936e-05, 1.4276773357305629e-05, 1.4257625921866098e-05, 1.423850416620374e-05, 1.4219408055877774e-05, 1.420033755649362e-05, 1.4181292633702826e-05, 1.4162273253203004e-05, 1.4143279380737776e-05, 1.4124310982096708e-05, 1.4105368023115251e-05, 1.4086450469674671e-05, 1.4067558287702e-05, 1.4048691443169964e-05, 1.4029849902096931e-05, 1.401103363054684e-05, 1.399224259462916e-05, 1.3973476760498783e-05, 1.3954736094356022e-05, 1.393602056244651e-05, 1.3917330131061154e-05, 1.3898664766536073e-05, 1.3880024435252527e-05, 1.3861409103636875e-05, 1.38428187381605e-05, 1.3824253305339762e-05, 1.3805712771735915e-05, 1.3787197103955059e-05, 1.3768706268648102e-05, 1.3750240232510658e-05, 1.3731798962283029e-05, 1.3713382424750096e-05, 1.3694990586741323e-05, 1.367662341513063e-05, 1.3658280876836384e-05, 1.3639962938821318e-05, 1.3621669568092459e-05, 1.3603400731701113e-05, 1.3585156396742748e-05, 1.3566936530356985e-05, 1.3548741099727496e-05, 1.3530570072081975e-05, 1.3512423414692085e-05, 1.3494301094873348e-05, 1.3476203079985165e-05, 1.3458129337430676e-05, 1.3440079834656769e-05, 1.3422054539153973e-05, 1.3404053418456413e-05, 1.3386076440141784e-05, 1.3368123571831232e-05, 1.3350194781189363e-05, 1.3332290035924123e-05, 1.331440930378677e-05, 1.3296552552571836e-05, 1.3278719750117015e-05, 1.3260910864303167e-05, 1.3243125863054201e-05, 1.3225364714337078e-05, 1.3207627386161688e-05, 1.3189913846580836e-05, 1.3172224063690194e-05, 1.3154558005628191e-05, 1.3136915640576021e-05, 1.3119296936757518e-05, 1.3101701862439167e-05, 1.3084130385929992e-05, 1.3066582475581519e-05, 1.304905809978774e-05, 1.3031557226985012e-05, 1.3014079825652044e-05, 1.2996625864309804e-05, 1.2979195311521495e-05, 1.2961788135892473e-05, 1.2944404306070196e-05, 1.2927043790744182e-05, 1.2909706558645928e-05, 1.289239257854889e-05, 1.2875101819268372e-05, 1.2857834249661539e-05, 1.2840589838627292e-05, 1.2823368555106258e-05, 1.2806170368080724e-05, 1.2788995246574566e-05, 1.2771843159653218e-05, 1.2754714076423595e-05, 1.2737607966034031e-05, 1.2720524797674266e-05, 1.270346454057534e-05, 1.2686427164009572e-05, 1.266941263729048e-05, 1.2652420929772753e-05, 1.2635452010852167e-05, 1.2618505849965545e-05, 1.2601582416590714e-05, 1.2584681680246415e-05, 1.2567803610492293e-05, 1.2550948176928798e-05, 1.2534115349197166e-05, 1.2517305096979341e-05, 1.2500517389997926e-05, 1.2483752198016145e-05, 1.2467009490837753e-05, 1.2450289238307033e-05, 1.2433591410308676e-05, 1.2416915976767802e-05, 1.240026290764983e-05, 1.238363217296048e-05, 1.2367023742745702e-05, 1.2350437587091603e-05, 1.2333873676124433e-05, 1.2317331980010486e-05, 1.2300812468956075e-05, 1.2284315113207481e-05, 1.2267839883050872e-05, 1.2251386748812287e-05, 1.2234955680857545e-05, 1.2218546649592226e-05, 1.2202159625461586e-05, 1.218579457895052e-05, 1.2169451480583526e-05, 1.2153130300924607e-05, 1.213683101057727e-05, 1.2120553580184425e-05, 1.2104297980428377e-05, 1.2088064182030733e-05, 1.2071852155752367e-05, 1.2055661872393387e-05, 1.203949330279304e-05, 1.20233464178297e-05, 1.2007221188420782e-05, 1.1991117585522725e-05, 1.1975035580130903e-05, 1.1958975143279592e-05, 1.1942936246041928e-05, 1.1926918859529828e-05, 1.1910922954893969e-05, 1.1894948503323696e-05, 1.1878995476047025e-05, 1.1863063844330535e-05, 1.1847153579479343e-05, 1.1831264652837067e-05, 1.1815397035785742e-05, 1.1799550699745798e-05, 1.178372561617598e-05, 1.1767921756573317e-05, 1.1752139092473076e-05, 1.1736377595448684e-05, 1.1720637237111704e-05, 1.1704917989111764e-05, 1.1689219823136522e-05, 1.1673542710911604e-05, 1.1657886624200555e-05, 1.1642251534804793e-05, 1.1626637414563552e-05, 1.1611044235353844e-05, 1.1595471969090375e-05, 1.1579920587725549e-05, 1.1564390063249363e-05, 1.1548880367689385e-05, 1.153339147311071e-05, 1.1517923351615877e-05, 1.1502475975344868e-05, 1.1487049316475003e-05, 1.1471643347220938e-05, 1.1456258039834578e-05, 1.1440893366605045e-05, 1.1425549299858638e-05, 1.1410225811958756e-05, 1.1394922875305877e-05, 1.1379640462337479e-05, 1.1364378545528025e-05, 1.1349137097388876e-05, 1.1333916090468262e-05, 1.1318715497351248e-05, 1.1303535290659642e-05, 1.1288375443051994e-05, 1.127323592722351e-05, 1.125811671590601e-05, 1.1243017781867903e-05, 1.1227939097914103e-05, 1.1212880636886014e-05, 1.1197842371661445e-05, 1.1182824275154602e-05, 1.1167826320315995e-05, 1.1152848480132424e-05, 1.1137890727626921e-05, 1.1122953035858687e-05, 1.1108035377923072e-05, 1.1093137726951487e-05, 1.1078260056111402e-05, 1.1063402338606258e-05, 1.1048564547675433e-05, 1.1033746656594211e-05, 1.1018948638673698e-05, 1.100417046726082e-05, 1.0989412115738214e-05, 1.0974673557524253e-05, 1.0959954766072934e-05, 1.0945255714873858e-05, 1.09305763774522e-05, 1.0915916727368619e-05, 1.0901276738219256e-05, 1.088665638363564e-05, 1.0872055637284678e-05, 1.08574744728686e-05, 1.0842912864124885e-05, 1.082837078482626e-05, 1.0813848208780605e-05, 1.0799345109830942e-05, 1.078486146185537e-05, 1.0770397238767007e-05, 1.0755952414513988e-05, 1.0741526963079361e-05, 1.0727120858481083e-05, 1.0712734074771941e-05, 1.0698366586039543e-05, 1.0684018366406232e-05, 1.0669689390029059e-05, 1.0655379631099748e-05, 1.064108906384462e-05, 1.0626817662524573e-05, 1.0612565401435014e-05, 1.0598332254905843e-05, 1.0584118197301375e-05, 1.0569923203020297e-05, 1.0555747246495654e-05, 1.0541590302194762e-05, 1.0527452344619192e-05, 1.0513333348304703e-05, 1.0499233287821219e-05, 1.0485152137772756e-05, 1.0471089872797391e-05, 1.0457046467567231e-05, 1.0443021896788327e-05, 1.0429016135200682e-05, 1.0415029157578153e-05, 1.0401060938728435e-05, 1.0387111453493018e-05, 1.0373180676747127e-05, 1.0359268583399692e-05, 1.0345375148393275e-05, 1.0331500346704068e-05, 1.0317644153341806e-05, 1.030380654334974e-05, 1.0289987491804615e-05, 1.0276186973816568e-05, 1.0262404964529144e-05, 1.0248641439119213e-05, 1.023489637279694e-05, 1.0221169740805739e-05, 1.0207461518422213e-05, 1.0193771680956146e-05, 1.018010020375041e-05, 1.0166447062180977e-05, 1.0152812231656808e-05, 1.013919568761988e-05, 1.0125597405545076e-05, 1.0112017360940187e-05, 1.0098455529345854e-05, 1.008491188633551e-05, 1.0071386407515362e-05, 1.0057879068524325e-05, 1.004438984503398e-05, 1.0030918712748554e-05, 1.0017465647404837e-05, 1.0004030624772183e-05, 9.990613620652417e-06, 9.977214610879845e-06, 9.963833571321165e-06, 9.950470477875442e-06, 9.937125306474074e-06, 9.92379803308073e-06, 9.910488633691326e-06, 9.897197084333954e-06, 9.883923361068876e-06, 9.870667439988452e-06, 9.857429297217093e-06, 9.844208908911257e-06, 9.83100625125936e-06, 9.817821300481765e-06, 9.804654032830714e-06, 9.791504424590317e-06, 9.778372452076476e-06, 9.765258091636855e-06, 9.752161319650857e-06, 9.739082112529544e-06, 9.726020446715633e-06, 9.712976298683417e-06, 9.69994964493876e-06, 9.686940462019018e-06, 9.673948726493019e-06, 9.660974414961024e-06, 9.648017504054666e-06, 9.63507797043693e-06, 9.622155790802092e-06, 9.609250941875678e-06, 9.596363400414448e-06, 9.583493143206315e-06, 9.57064014707034e-06, 9.557804388856659e-06, 9.544985845446468e-06, 9.532184493751959e-06, 9.519400310716289e-06, 9.50663327331355e-06, 9.493883358548697e-06, 9.481150543457547e-06, 9.468434805106692e-06, 9.455736120593504e-06, 9.44305446704605e-06, 9.430389821623086e-06, 9.417742161514002e-06, 9.405111463938765e-06, 9.392497706147921e-06, 9.379900865422495e-06, 9.36732091907401e-06, 9.3547578444444e-06, 9.342211618905989e-06, 9.329682219861463e-06, 9.317169624743788e-06, 9.304673811016227e-06, 9.29219475617224e-06, 9.279732437735497e-06, 9.26728683325979e-06, 9.254857920329028e-06, 9.242445676557187e-06, 9.230050079588247e-06, 9.217671107096199e-06, 9.205308736784951e-06, 9.192962946388326e-06, 9.180633713670012e-06, 9.168321016423509e-06, 9.156024832472113e-06, 9.143745139668847e-06, 9.131481915896455e-06, 9.119235139067326e-06, 9.107004787123481e-06, 9.094790838036525e-06, 9.082593269807605e-06, 9.070412060467368e-06, 9.058247188075932e-06, 9.046098630722838e-06, 9.03396636652701e-06, 9.021850373636713e-06, 9.009750630229538e-06, 8.99766711451232e-06, 8.985599804721137e-06, 8.973548679121248e-06, 8.96151371600707e-06, 8.949494893702125e-06, 8.937492190559001e-06, 8.925505584959334e-06, 8.913535055313738e-06, 8.901580580061794e-06, 8.889642137671994e-06, 8.877719706641697e-06, 8.865813265497122e-06, 8.853922792793267e-06, 8.842048267113909e-06, 8.83018966707153e-06, 8.818346971307315e-06, 8.806520158491077e-06, 8.794709207321243e-06, 8.782914096524814e-06, 8.771134804857313e-06, 8.759371311102766e-06, 8.747623594073637e-06, 8.735891632610823e-06, 8.724175405583587e-06, 8.712474891889534e-06, 8.70079007045458e-06, 8.689120920232885e-06, 8.67746742020686e-06, 8.66582954938708e-06, 8.654207286812288e-06, 8.64260061154933e-06, 8.631009502693125e-06, 8.619433939366643e-06, 8.607873900720832e-06, 8.596329365934624e-06, 8.584800314214854e-06, 8.573286724796267e-06, 8.561788576941437e-06, 8.55030584994076e-06, 8.538838523112408e-06, 8.527386575802282e-06, 8.515949987384001e-06, 8.504528737258827e-06, 8.493122804855657e-06, 8.48173216963098e-06, 8.470356811068832e-06, 8.45899670868077e-06, 8.447651842005822e-06, 8.436322190610466e-06, 8.425007734088575e-06, 8.413708452061398e-06, 8.402424324177512e-06, 8.39115533011278e-06, 8.379901449570345e-06, 8.368662662280548e-06, 8.357438948000931e-06, 8.346230286516174e-06, 8.33503665763807e-06, 8.323858041205497e-06, 8.312694417084357e-06, 8.301545765167577e-06, 8.290412065375025e-06, 8.279293297653522e-06, 8.268189441976767e-06, 8.257100478345324e-06, 8.246026386786587e-06, 8.23496714735472e-06, 8.223922740130657e-06, 8.212893145222028e-06, 8.201878342763148e-06, 8.19087831291499e-06, 8.179893035865107e-06, 8.16892249182765e-06, 8.157966661043285e-06, 8.1470255237792e-06, 8.136099060329023e-06, 8.125187251012826e-06, 8.11429007617708e-06, 8.103407516194599e-06, 8.092539551464537e-06, 8.08168616241232e-06, 8.07084732948964e-06, 8.060023033174396e-06, 8.049213253970674e-06, 8.038417972408712e-06, 8.027637169044846e-06, 8.016870824461509e-06, 8.006118919267153e-06, 7.995381434096262e-06, 7.984658349609275e-06, 7.973949646492565e-06, 7.963255305458428e-06, 7.952575307245005e-06, 7.941909632616288e-06, 7.931258262362055e-06, 7.92062117729786e-06, 7.909998358264974e-06, 7.899389786130368e-06, 7.88879544178668e-06, 7.878215306152159e-06, 7.867649360170665e-06, 7.857097584811603e-06, 7.846559961069896e-06, 7.83603646996597e-06, 7.825527092545692e-06, 7.815031809880365e-06, 7.804550603066663e-06, 7.794083453226624e-06, 7.783630341507597e-06, 7.773191249082214e-06, 7.762766157148366e-06, 7.752355046929155e-06, 7.741957899672867e-06, 7.731574696652935e-06, 7.721205419167912e-06, 7.710850048541428e-06, 7.70050856612216e-06, 7.69018095328381e-06, 7.679867191425042e-06, 7.66956726196949e-06, 7.659281146365682e-06, 7.649008826087038e-06, 7.638750282631822e-06, 7.628505497523108e-06, 7.61827445230876e-06, 7.608057128561378e-06, 7.5978535078782865e-06, 7.587663571881485e-06, 7.577487302217615e-06, 7.567324680557948e-06, 7.557175688598325e-06, 7.54704030805914e-06, 7.5369185206853005e-06, 7.526810308246206e-06, 7.516715652535692e-06, 7.5066345353720184e-06, 7.496566938597835e-06, 7.4865128440801325e-06, 7.4764722337102345e-06, 7.466445089403735e-06, 7.4564313931004955e-06, 7.446431126764595e-06, 7.436444272384293e-06, 7.426470811972024e-06, 7.416510727564326e-06, 7.406564001221847e-06, 7.39663061502928e-06, 7.386710551095356e-06, 7.376803791552797e-06, 7.36691031855828e-06, 7.357030114292431e-06, 7.347163160959754e-06, 7.33730944078864e-06, 7.3274689360312925e-06, 7.317641628963741e-06, 7.307827501885767e-06, 7.298026537120896e-06, 7.288238717016369e-06, 7.278464023943087e-06, 7.268702440295611e-06, 7.258953948492101e-06, 7.2492185309743006e-06, 7.239496170207508e-06, 7.229786848680526e-06, 7.2200905489056575e-06, 7.210407253418645e-06, 7.200736944778664e-06, 7.191079605568276e-06, 7.1814352183933986e-06, 7.17180376588329e-06, 7.162185230690488e-06, 7.152579595490813e-06, 7.142986842983307e-06, 7.133406955890226e-06, 7.123839916956989e-06, 7.114285708952158e-06, 7.104744314667413e-06, 7.0952157169175e-06, 7.085699898540228e-06, 7.076196842396408e-06, 7.066706531369854e-06, 7.05722894836732e-06, 7.04776407631849e-06, 7.038311898175948e-06, 7.0288723969151335e-06, 7.019445555534325e-06, 7.010031357054595e-06, 7.000629784519797e-06, 6.991240820996521e-06, 6.98186444957406e-06, 6.972500653364404e-06, 6.9631494155021725e-06, 6.953810719144623e-06, 6.944484547471589e-06, 6.935170883685466e-06, 6.925869711011182e-06, 6.916581012696152e-06, 6.907304772010275e-06, 6.898040972245872e-06, 6.888789596717686e-06, 6.8795506287628245e-06, 6.870324051740749e-06, 6.86110984903324e-06, 6.851908004044363e-06, 6.842718500200443e-06, 6.833541320950027e-06, 6.824376449763874e-06, 6.815223870134897e-06, 6.8060835655781495e-06, 6.796955519630806e-06, 6.787839715852105e-06, 6.778736137823346e-06, 6.76964476914784e-06, 6.760565593450899e-06, 6.751498594379783e-06, 6.742443755603692e-06, 6.7334010608137325e-06, 6.724370493722868e-06, 6.7153520380659246e-06, 6.706345677599531e-06, 6.697351396102098e-06, 6.6883691773738055e-06, 6.679399005236545e-06, 6.670440863533917e-06, 6.6614947361311834e-06, 6.6525606069152505e-06, 6.6436384597946305e-06, 6.634728278699414e-06, 6.625830047581256e-06, 6.61694375041332e-06, 6.608069371190278e-06, 6.599206893928253e-06, 6.5903563026648215e-06, 6.581517581458954e-06, 6.572690714391007e-06, 6.563875685562687e-06, 6.555072479097021e-06, 6.546281079138335e-06, 6.537501469852211e-06, 6.528733635425478e-06, 6.5199775600661645e-06, 6.511233228003477e-06, 6.502500623487786e-06, 6.49377973079057e-06, 6.485070534204413e-06, 6.476373018042955e-06, 6.467687166640887e-06, 6.4590129643538975e-06, 6.450350395558659e-06, 6.441699444652805e-06, 6.433060096054883e-06, 6.42443233420435e-06, 6.415816143561521e-06, 6.407211508607557e-06, 6.398618413844436e-06, 6.390036843794911e-06, 6.381466783002507e-06, 6.372908216031462e-06, 6.3643611274667335e-06, 6.355825501913938e-06, 6.347301323999342e-06, 6.338788578369841e-06, 6.330287249692904e-06, 6.321797322656582e-06, 6.313318781969443e-06, 6.304851612360582e-06, 6.29639579857956e-06, 6.287951325396396e-06, 6.279518177601539e-06, 6.27109634000583e-06, 6.262685797440487e-06, 6.254286534757065e-06, 6.245898536827446e-06, 6.237521788543788e-06, 6.2291562748185184e-06, 6.220801980584302e-06, 6.212458890794005e-06, 6.20412699042068e-06, 6.19580626445753e-06, 6.18749669791788e-06, 6.179198275835164e-06, 6.170910983262882e-06, 6.162634805274584e-06, 6.154369726963833e-06, 6.146115733444191e-06, 6.137872809849179e-06, 6.129640941332257e-06, 6.121420113066802e-06, 6.113210310246069e-06, 6.105011518083176e-06, 6.096823721811068e-06, 6.088646906682504e-06, 6.080481057970011e-06, 6.07232616096587e-06, 6.064182200982095e-06, 6.0560491633503886e-06, 6.047927033422135e-06, 6.039815796568356e-06, 6.031715438179703e-06, 6.023625943666412e-06, 6.015547298458287e-06, 6.007479488004682e-06, 5.999422497774453e-06, 5.991376313255957e-06, 5.983340919957001e-06, 5.975316303404842e-06, 5.9673024491461355e-06, 5.959299342746923e-06, 5.951306969792613e-06, 5.943325315887935e-06, 5.935354366656936e-06, 5.9273941077429336e-06, 5.919444524808502e-06, 5.911505603535451e-06, 5.903577329624784e-06, 5.895659688796692e-06, 5.8877526667905035e-06, 5.8798562493646894e-06, 5.8719704222968075e-06, 5.864095171383494e-06, 5.85623048244044e-06, 5.8483763413023495e-06, 5.840532733822936e-06, 5.8326996458748745e-06, 5.824877063349795e-06, 5.817064972158245e-06, 5.809263358229667e-06, 5.801472207512382e-06, 5.793691505973547e-06, 5.785921239599145e-06, 5.77816139439395e-06, 5.770411956381514e-06, 5.762672911604125e-06, 5.754944246122789e-06, 5.74722594601722e-06, 5.739517997385783e-06, 5.731820386345505e-06, 5.724133099032015e-06, 5.716456121599554e-06, 5.708789440220921e-06, 5.701133041087457e-06, 5.693486910409034e-06, 5.685851034414009e-06, 5.6782253993492135e-06, 5.6706099914799245e-06, 5.663004797089834e-06, 5.65540980248104e-06, 5.647824993973999e-06, 5.640250357907526e-06, 5.632685880638747e-06, 5.625131548543096e-06, 5.6175873480142705e-06, 5.610053265464219e-06, 5.602529287323119e-06, 5.5950154000393385e-06, 5.587511590079429e-06, 5.580017843928084e-06, 5.572534148088133e-06, 5.565060489080498e-06, 5.557596853444183e-06, 5.550143227736245e-06, 5.54269959853177e-06, 5.535265952423853e-06, 5.527842276023558e-06, 5.5204285559599205e-06, 5.513024778879901e-06, 5.5056309314483616e-06, 5.4982470003480666e-06, 5.490872972279623e-06, 5.483508833961489e-06, 5.476154572129925e-06, 5.468810173538981e-06, 5.461475624960479e-06, 5.454150913183976e-06, 5.4468360250167516e-06, 5.439530947283769e-06, 5.4322356668276755e-06, 5.424950170508751e-06, 5.417674445204904e-06, 5.410408477811645e-06, 5.403152255242053e-06, 5.395905764426764e-06, 5.3886689923139374e-06, 5.381441925869244e-06, 5.3742245520758275e-06, 5.367016857934294e-06, 5.359818830462686e-06, 5.352630456696448e-06, 5.3454517236884245e-06, 5.338282618508813e-06, 5.3311231282451615e-06, 5.3239732400023265e-06, 5.316832940902465e-06, 5.3097022180850035e-06, 5.302581058706615e-06, 5.295469449941203e-06, 5.288367378979863e-06, 5.281274833030882e-06, 5.274191799319694e-06, 5.267118265088863e-06, 5.260054217598073e-06, 5.252999644124085e-06, 5.2459545319607325e-06, 5.238918868418882e-06, 5.231892640826422e-06, 5.224875836528239e-06, 5.217868442886186e-06, 5.210870447279072e-06, 5.203881837102627e-06, 5.19690259976949e-06, 5.189932722709181e-06, 5.1829721933680735e-06, 5.176020999209388e-06, 5.169079127713147e-06, 5.162146566376175e-06, 5.155223302712056e-06, 5.148309324251128e-06, 5.1414046185404486e-06, 5.134509173143774e-06, 5.1276229756415485e-06, 5.120746013630863e-06, 5.113878274725451e-06, 5.107019746555649e-06, 5.100170416768394e-06, 5.093330273027183e-06, 5.086499303012056e-06, 5.079677494419585e-06, 5.072864834962834e-06, 5.0660613123713536e-06, 5.0592669143911455e-06, 5.052481628784643e-06, 5.045705443330705e-06, 5.0389383458245635e-06, 5.032180324077834e-06, 5.0254313659184695e-06, 5.018691459190754e-06, 5.011960591755268e-06, 5.005238751488875e-06, 4.9985259262847e-06, 4.991822104052103e-06, 4.985127272716661e-06, 4.978441420220142e-06, 4.97176453452049e-06, 4.965096603591797e-06, 4.95843761542428e-06, 4.951787558024272e-06, 4.945146419414182e-06, 4.9385141876324915e-06, 4.931890850733717e-06, 4.925276396788399e-06, 4.918670813883078e-06, 4.912074090120269e-06, 4.9054862136184515e-06, 4.8989071725120255e-06, 4.892336954951322e-06, 4.88577554910255e-06, 4.8792229431478e-06, 4.872679125285007e-06, 4.866144083727928e-06, 4.859617806706145e-06, 4.853100282465005e-06, 4.846591499265637e-06, 4.840091445384905e-06, 4.833600109115396e-06, 4.827117478765403e-06, 4.8206435426588915e-06, 4.814178289135496e-06, 4.807721706550482e-06, 4.801273783274736e-06, 4.7948345076947425e-06, 4.788403868212555e-06, 4.781981853245788e-06, 4.775568451227586e-06, 4.769163650606614e-06, 4.762767439847016e-06, 4.75637980742842e-06, 4.750000741845899e-06, 4.7436302316099525e-06, 4.7372682652464975e-06, 4.7309148312968325e-06, 4.7245699183176306e-06, 4.718233514880904e-06, 4.711905609574e-06, 4.705586190999567e-06, 4.699275247775536e-06, 4.692972768535115e-06, 4.6866787419267415e-06, 4.680393156614091e-06, 4.674116001276034e-06, 4.667847264606625e-06, 4.661586935315088e-06, 4.655335002125782e-06, 4.649091453778194e-06, 4.642856279026908e-06, 4.636629466641598e-06, 4.630411005406992e-06, 4.624200884122859e-06, 4.617999091603998e-06, 4.6118056166801985e-06, 4.60562044819624e-06, 4.599443575011855e-06, 4.593274986001724e-06, 4.587114670055446e-06, 4.580962616077515e-06, 4.574818812987317e-06, 4.568683249719087e-06, 4.56255591522191e-06, 4.556436798459689e-06, 4.550325888411127e-06, 4.544223174069711e-06, 4.538128644443683e-06, 4.532042288556036e-06, 4.525964095444477e-06, 4.519894054161423e-06, 4.513832153773963e-06, 4.507778383363862e-06, 4.501732732027515e-06, 4.495695188875948e-06, 4.48966574303479e-06, 4.48364438364425e-06, 4.477631099859112e-06, 4.471625880848694e-06, 4.465628715796843e-06, 4.459639593901916e-06, 4.4536585043767506e-06, 4.447685436448659e-06, 4.441720379359394e-06, 4.435763322365143e-06, 4.429814254736499e-06, 4.423873165758443e-06, 4.417940044730334e-06, 4.412014880965872e-06, 4.4060976637931e-06, 4.400188382554364e-06, 4.394287026606313e-06, 4.38839358531986e-06, 4.382508048080181e-06, 4.376630404286687e-06, 4.370760643353003e-06, 4.364898754706956e-06, 4.359044727790547e-06, 4.353198552059944e-06, 4.34736021698545e-06, 4.341529712051489e-06, 4.335707026756595e-06, 4.32989215061338e-06, 4.324085073148526e-06, 4.318285783902754e-06, 4.312494272430822e-06, 4.306710528301492e-06, 4.300934541097511e-06, 4.295166300415608e-06, 4.2894057958664545e-06, 4.283653017074663e-06, 4.277907953678758e-06, 4.272170595331155e-06, 4.26644093169816e-06, 4.260718952459925e-06, 4.255004647310454e-06, 4.24929800595756e-06, 4.243599018122876e-06, 4.237907673541804e-06, 4.232223961963518e-06, 4.226547873150948e-06, 4.22087939688074e-06, 4.215218522943263e-06, 4.209565241142569e-06, 4.203919541296394e-06, 4.198281413236123e-06, 4.192650846806778e-06, 4.1870278318670066e-06, 4.181412358289053e-06, 4.175804415958747e-06, 4.170203994775478e-06, 4.1646110846521915e-06, 4.159025675515352e-06, 4.153447757304936e-06, 4.147877319974418e-06, 4.142314353490737e-06, 4.1367588478343e-06, 4.131210792998941e-06, 4.125670178991916e-06, 4.120136995833889e-06, 4.1146112335589e-06, 4.109092882214365e-06, 4.103581931861033e-06, 4.098078372573e-06, 4.09258219443766e-06, 4.08709338755571e-06, 4.081611942041122e-06, 4.076137848021123e-06, 4.070671095636187e-06, 4.0652116750400026e-06, 4.059759576399476e-06, 4.054314789894691e-06, 4.048877305718903e-06, 4.043447114078525e-06, 4.038024205193098e-06, 4.032608569295286e-06, 4.0272001966308486e-06, 4.021799077458631e-06, 4.01640520205054e-06, 4.011018560691527e-06, 4.00563914367958e-06, 4.000266941325692e-06, 3.994901943953856e-06, 3.989544141901036e-06, 3.984193525517164e-06, 3.978850085165106e-06, 3.973513811220653e-06, 3.968184694072515e-06, 3.9628627241222765e-06, 3.9575478917844076e-06, 3.9522401874862285e-06, 3.946939601667897e-06, 3.941646124782395e-06, 3.936359747295506e-06, 3.931080459685804e-06, 3.925808252444628e-06, 3.920543116076073e-06, 3.915285041096971e-06, 3.910034018036864e-06, 3.904790037438009e-06, 3.8995530898553325e-06, 3.894323165856443e-06, 3.889100256021585e-06, 3.8838843509436485e-06, 3.878675441228133e-06, 3.873473517493137e-06, 3.8682785703693486e-06, 3.863090590500012e-06, 3.857909568540931e-06, 3.852735495160431e-06, 3.84756836103936e-06, 3.842408156871062e-06, 3.837254873361359e-06, 3.832108501228547e-06, 3.826969031203361e-06, 3.8218364540289735e-06, 3.816710760460969e-06, 3.8115919412673303e-06, 3.806479987228425e-06, 3.8013748891369796e-06, 3.7962766377980764e-06, 3.791185224029123e-06, 3.786100638659848e-06, 3.781022872532275e-06, 3.7759519165007074e-06, 3.7708877614317227e-06, 3.7658303982041404e-06, 3.760779817709017e-06, 3.7557360108496226e-06, 3.750698968541431e-06, 3.7456686817120966e-06, 3.7406451413014412e-06, 3.735628338261441e-06, 3.730618263556203e-06, 3.7256149081619585e-06, 3.7206182630670328e-06, 3.715628319271846e-06, 3.7106450677888826e-06, 3.7056684996426796e-06, 3.7006986058698185e-06, 3.6957353775188927e-06, 3.690778805650512e-06, 3.6858288813372644e-06, 3.680885595663719e-06, 3.675948939726398e-06, 3.6710189046337637e-06, 3.666095481506208e-06, 3.6611786614760255e-06, 3.6562684356874113e-06, 3.65136479529643e-06, 3.6464677314710114e-06, 3.6415772353909312e-06, 3.636693298247791e-06, 3.6318159112450114e-06, 3.6269450655978027e-06, 3.622080752533166e-06, 3.6172229632898617e-06, 3.6123716891184013e-06, 3.607526921281035e-06, 3.602688651051726e-06, 3.597856869716145e-06, 3.5930315685716476e-06, 3.5882127389272635e-06, 3.5834003721036755e-06, 3.5785944594332055e-06, 3.573794992259807e-06, 3.569001961939034e-06, 3.5642153598380413e-06, 3.5594351773355565e-06, 3.554661405821874e-06, 3.5498940366988323e-06, 3.545133061379801e-06, 3.5403784712896683e-06, 3.5356302578648194e-06, 3.530888412553131e-06, 3.526152926813942e-06, 3.521423792118051e-06, 3.516700999947693e-06, 3.5119845417965262e-06, 3.507274409169622e-06, 3.502570593583437e-06, 3.497873086565813e-06, 3.4931818796559507e-06, 3.488496964404397e-06, 3.483818332373034e-06, 3.479145975135058e-06, 3.47447988427497e-06, 3.469820051388552e-06, 3.465166468082866e-06, 3.4605191259762215e-06, 3.4558780166981734e-06, 3.451243131889505e-06, 3.4466144632022035e-06, 3.441992002299461e-06, 3.437375740855643e-06, 3.4327656705562865e-06, 3.428161783098076e-06, 3.4235640701888325e-06, 3.4189725235475002e-06, 3.414387134904127e-06, 3.409807895999855e-06, 3.405234798586899e-06, 3.4006678344285397e-06, 3.3961069952991018e-06, 3.3915522729839405e-06, 3.387003659279433e-06, 3.382461145992953e-06, 3.377924724942868e-06, 3.3733943879585137e-06, 3.368870126880184e-06, 3.36435193355912e-06, 3.3598397998574875e-06, 3.3553337176483712e-06, 3.350833678815749e-06, 3.3463396752544894e-06, 3.3418516988703285e-06, 3.337369741579856e-06, 3.3328937953105087e-06, 3.328423852000543e-06, 3.3239599035990346e-06, 3.319501942065849e-06, 3.3150499593716434e-06, 3.310603947497836e-06, 3.3061638984366013e-06, 3.3017298041908575e-06, 3.2973016567742426e-06, 3.292879448211111e-06, 3.2884631705365072e-06, 3.284052815796166e-06, 3.279648376046483e-06, 3.27524984335451e-06, 3.27085720979794e-06, 3.2664704674650885e-06, 3.262089608454884e-06, 3.2577146248768484e-06, 3.253345508851091e-06, 3.2489822525082834e-06, 3.244624847989655e-06, 3.240273287446976e-06, 3.2359275630425367e-06, 3.231587666949147e-06, 3.227253591350109e-06, 3.2229253284392066e-06, 3.2186028704207e-06, 3.2142862095092978e-06, 3.209975337930155e-06, 3.2056702479188493e-06, 3.201370931721376e-06, 3.1970773815941274e-06, 3.1927895898038804e-06, 3.188507548627787e-06, 3.1842312503533517e-06, 3.179960687278428e-06, 3.1756958517111937e-06, 3.1714367359701494e-06, 3.1671833323840918e-06, 3.162935633292106e-06, 3.158693631043557e-06, 3.1544573179980654e-06, 3.150226686525502e-06, 3.1460017290059668e-06, 3.1417824378297847e-06, 3.1375688053974826e-06, 3.133360824119778e-06, 3.1291584864175737e-06, 3.1249617847219287e-06, 3.1207707114740606e-06, 3.11658525912532e-06, 3.112405420137182e-06, 3.1082311869812343e-06, 3.104062552139157e-06, 3.0998995081027206e-06, 3.0957420473737575e-06, 3.0915901624641625e-06, 3.087443845895869e-06, 3.083303090200841e-06, 3.0791678879210604e-06, 3.0750382316085077e-06, 3.070914113825156e-06, 3.0667955271429503e-06, 3.0626824641438027e-06, 3.0585749174195704e-06, 3.0544728795720443e-06, 3.050376343212944e-06, 3.0462853009638913e-06, 3.0421997454564086e-06, 3.038119669331896e-06, 3.0340450652416284e-06, 3.0299759258467314e-06, 3.025912243818174e-06, 3.0218540118367576e-06, 3.0178012225930956e-06, 3.0137538687876096e-06, 3.009711943130505e-06, 3.0056754383417705e-06, 3.0016443471511533e-06, 2.997618662298152e-06, 2.993598376532006e-06, 2.989583482611675e-06, 2.985573973305833e-06, 2.9815698413928515e-06, 2.977571079660785e-06, 2.973577680907366e-06, 2.9695896379399795e-06, 2.965606943575663e-06, 2.9616295906410825e-06, 2.9576575719725298e-06, 2.9536908804158997e-06, 2.9497295088266828e-06, 2.9457734500699546e-06, 2.941822697020355e-06, 2.9378772425620857e-06, 2.933937079588885e-06, 2.9300022010040303e-06, 2.9260725997203097e-06, 2.9221482686600167e-06, 2.9182292007549434e-06, 2.9143153889463545e-06, 2.910406826184986e-06, 2.9065035054310254e-06, 2.902605419654104e-06, 2.8987125618332805e-06, 2.894824924957029e-06, 2.8909425020232307e-06, 2.887065286039152e-06, 2.883193270021445e-06, 2.8793264469961198e-06, 2.8754648099985468e-06, 2.8716083520734326e-06, 2.867757066274811e-06, 2.8639109456660377e-06, 2.8600699833197643e-06, 2.856234172317938e-06, 2.852403505751783e-06, 2.848577976721786e-06, 2.844757578337693e-06, 2.8409423037184847e-06, 2.837132145992377e-06, 2.8333270982967947e-06, 2.8295271537783728e-06, 2.8257323055929346e-06, 2.821942546905481e-06, 2.8181578708901844e-06, 2.8143782707303663e-06, 2.8106037396184958e-06, 2.8068342707561675e-06, 2.8030698573540968e-06, 2.799310492632103e-06, 2.7955561698190983e-06, 2.7918068821530786e-06, 2.7880626228811046e-06, 2.7843233852592986e-06, 2.7805891625528232e-06, 2.7768599480358774e-06, 2.773135734991677e-06, 2.7694165167124465e-06, 2.7657022864994107e-06, 2.7619930376627724e-06, 2.7582887635217125e-06, 2.7545894574043676e-06, 2.750895112647823e-06, 2.7472057225981026e-06, 2.743521280610151e-06, 2.739841780047828e-06, 2.7361672142838907e-06, 2.7324975766999865e-06, 2.7288328606866386e-06, 2.725173059643232e-06, 2.7215181669780094e-06, 2.7178681761080484e-06, 2.714223080459261e-06, 2.71058287346637e-06, 2.706947548572909e-06, 2.703317099231201e-06, 2.699691518902351e-06, 2.6960708010562372e-06, 2.6924549391714896e-06, 2.688843926735491e-06, 2.685237757244353e-06, 2.6816364242029142e-06, 2.678039921124722e-06, 2.674448241532023e-06, 2.670861378955753e-06, 2.6672793269355217e-06, 2.6637020790196067e-06, 2.6601296287649335e-06, 2.6565619697370746e-06, 2.6529990955102277e-06, 2.649440999667208e-06, 2.6458876757994414e-06, 2.642339117506945e-06, 2.638795318398322e-06, 2.6352562720907454e-06, 2.631721972209948e-06, 2.628192412390215e-06, 2.624667586274365e-06, 2.621147487513746e-06, 2.617632109768218e-06, 2.6141214467061457e-06, 2.6106154920043854e-06, 2.607114239348271e-06, 2.603617682431611e-06, 2.6001258149566654e-06, 2.5966386306341456e-06, 2.5931561231831935e-06, 2.589678286331379e-06, 2.5862051138146807e-06, 2.582736599377479e-06, 2.579272736772547e-06, 2.5758135197610316e-06, 2.5723589421124513e-06, 2.5689089976046773e-06, 2.5654636800239297e-06, 2.5620229831647575e-06, 2.5585869008300346e-06, 2.5551554268309474e-06, 2.55172855498698e-06, 2.5483062791259075e-06, 2.544888593083782e-06, 2.5414754907049224e-06, 2.5380669658419044e-06, 2.5346630123555465e-06, 2.531263624114903e-06, 2.527868794997248e-06, 2.524478518888071e-06, 2.5210927896810597e-06, 2.5177116012780886e-06, 2.514334947589217e-06, 2.5109628225326666e-06, 2.5075952200348194e-06, 2.5042321340301984e-06, 2.5008735584614674e-06, 2.4975194872794094e-06, 2.4941699144429213e-06, 2.4908248339190038e-06, 2.4874842396827463e-06, 2.4841481257173226e-06, 2.48081648601397e-06, 2.477489314571991e-06, 2.4741666053987313e-06, 2.4708483525095744e-06, 2.4675345499279334e-06, 2.4642251916852324e-06, 2.460920271820904e-06, 2.4576197843823716e-06, 2.4543237234250463e-06, 2.4510320830123077e-06, 2.447744857215498e-06, 2.444462040113914e-06, 2.441183625794788e-06, 2.437909608353288e-06, 2.4346399818924958e-06, 2.431374740523405e-06, 2.428113878364908e-06, 2.4248573895437814e-06, 2.4216052681946823e-06, 2.4183575084601316e-06, 2.415114104490508e-06, 2.4118750504440346e-06, 2.4086403404867677e-06, 2.4054099687925908e-06, 2.4021839295431985e-06, 2.398962216928091e-06, 2.395744825144559e-06, 2.392531748397678e-06, 2.3893229809002936e-06, 2.3861185168730125e-06, 2.3829183505441945e-06, 2.3797224761499384e-06, 2.376530887934075e-06, 2.3733435801481523e-06, 2.370160547051431e-06, 2.3669817829108693e-06, 2.363807282001114e-06, 2.360637038604492e-06, 2.3574710470109966e-06, 2.3543093015182815e-06, 2.351151796431645e-06, 2.3479985260640274e-06, 2.3448494847359923e-06, 2.3417046667757206e-06, 2.3385640665190023e-06, 2.3354276783092224e-06, 2.332295496497353e-06, 2.3291675154419425e-06, 2.3260437295091028e-06, 2.3229241330725073e-06, 2.3198087205133685e-06, 2.3166974862204414e-06, 2.31359042459e-06, 2.31048753002584e-06, 2.3073887969392576e-06, 2.3042942197490452e-06, 2.3012037928814838e-06, 2.2981175107703255e-06, 2.2950353678567913e-06, 2.291957358589553e-06, 2.2888834774247336e-06, 2.2858137188258853e-06, 2.282748077263988e-06, 2.2796865472174393e-06, 2.2766291231720373e-06, 2.273575799620981e-06, 2.270526571064849e-06, 2.2674814320116017e-06, 2.2644403769765598e-06, 2.2614034004824023e-06, 2.2583704970591554e-06, 2.2553416612441776e-06, 2.252316887582159e-06, 2.249296170625101e-06, 2.246279504932314e-06, 2.2432668850704065e-06, 2.24025830561327e-06, 2.2372537611420792e-06, 2.23425324624527e-06, 2.2312567555185414e-06, 2.2282642835648368e-06, 2.225275824994339e-06, 2.2222913744244606e-06, 2.2193109264798307e-06, 2.2163344757922914e-06, 2.2133620170008792e-06, 2.210393544751826e-06, 2.2074290536985397e-06, 2.2044685385016e-06, 2.2015119938287495e-06, 2.1985594143548792e-06, 2.1956107947620253e-06, 2.1926661297393522e-06, 2.1897254139831517e-06, 2.1867886421968247e-06, 2.1838558090908765e-06, 2.180926909382909e-06, 2.1780019377976046e-06, 2.1750808890667256e-06, 2.1721637579290946e-06, 2.169250539130595e-06, 2.166341227424154e-06, 2.163435817569735e-06, 2.160534304334333e-06, 2.1576366824919573e-06, 2.15474294682363e-06, 2.1518530921173685e-06, 2.1489671131681827e-06, 2.146085004778064e-06, 2.1432067617559726e-06, 2.1403323789178335e-06, 2.137461851086521e-06, 2.1345951730918578e-06, 2.1317323397705954e-06, 2.1288733459664123e-06, 2.1260181865299027e-06, 2.1231668563185664e-06, 2.1203193501968017e-06, 2.1174756630358913e-06, 2.1146357897140002e-06, 2.11179972511616e-06, 2.1089674641342613e-06, 2.10613900166705e-06, 2.1033143326201075e-06, 2.100493451905854e-06, 2.0976763544435275e-06, 2.0948630351591845e-06, 2.0920534889856834e-06, 2.0892477108626784e-06, 2.086445695736613e-06, 2.083647438560706e-06, 2.080852934294947e-06, 2.078062177906082e-06, 2.075275164367609e-06, 2.0724918886597686e-06, 2.069712345769531e-06, 2.066936530690593e-06, 2.0641644384233615e-06, 2.0613960639749532e-06, 2.058631402359178e-06, 2.0558704485965323e-06, 2.053113197714195e-06, 2.0503596447460095e-06, 2.0476097847324836e-06, 2.0448636127207736e-06, 2.0421211237646806e-06, 2.039382312924638e-06, 2.0366471752677025e-06, 2.0339157058675513e-06, 2.031187899804463e-06, 2.0284637521653196e-06, 2.0257432580435883e-06, 2.0230264125393186e-06, 2.0203132107591315e-06, 2.0176036478162096e-06, 2.014897718830292e-06, 2.0121954189276604e-06, 2.0094967432411347e-06, 2.006801686910061e-06, 2.004110245080307e-06, 2.0014224129042466e-06, 1.9987381855407572e-06, 1.996057558155211e-06, 1.9933805259194596e-06, 1.9907070840118345e-06, 1.9880372276171316e-06, 1.9853709519266026e-06, 1.982708252137954e-06, 1.9800491234553277e-06, 1.977393561089302e-06, 1.974741560256873e-06, 1.972093116181458e-06, 1.9694482240928756e-06, 1.9668068792273437e-06, 1.96416907682747e-06, 1.9615348121422412e-06, 1.9589040804270172e-06, 1.956276876943519e-06, 1.953653196959826e-06, 1.9510330357503604e-06, 1.9484163885958834e-06, 1.9458032507834866e-06, 1.94319361760658e-06, 1.940587484364889e-06, 1.9379848463644387e-06, 1.9353856989175545e-06, 1.9327900373428448e-06, 1.9301978569651966e-06, 1.9276091531157704e-06, 1.9250239211319847e-06, 1.922442156357514e-06, 1.9198638541422757e-06, 1.9172890098424235e-06, 1.9147176188203423e-06, 1.9121496764446322e-06, 1.909585178090109e-06, 1.907024119137789e-06, 1.9044664949748845e-06, 1.901912300994794e-06, 1.8993615325970926e-06, 1.8968141851875292e-06, 1.8942702541780092e-06, 1.8917297349865963e-06, 1.889192623037496e-06, 1.8866589137610521e-06, 1.8841286025937359e-06, 1.8816016849781417e-06, 1.8790781563629726e-06, 1.8765580122030381e-06, 1.8740412479592404e-06, 1.871527859098576e-06, 1.8690178410941137e-06, 1.8665111894249978e-06, 1.8640078995764337e-06, 1.861507967039681e-06, 1.8590113873120516e-06, 1.856518155896891e-06, 1.8540282683035768e-06, 1.8515417200475094e-06, 1.8490585066501019e-06, 1.8465786236387796e-06, 1.8441020665469602e-06, 1.8416288309140543e-06, 1.8391589122854534e-06, 1.836692306212527e-06, 1.8342290082526076e-06, 1.8317690139689863e-06, 1.8293123189309055e-06, 1.8268589187135478e-06, 1.824408808898035e-06, 1.821961985071411e-06, 1.8195184428266395e-06, 1.8170781777625942e-06, 1.8146411854840507e-06, 1.812207461601684e-06, 1.8097770017320503e-06, 1.807349801497587e-06, 1.8049258565266026e-06, 1.8025051624532662e-06, 1.800087714917608e-06, 1.7976735095655005e-06, 1.7952625420486577e-06, 1.7928548080246224e-06, 1.7904503031567679e-06, 1.7880490231142776e-06, 1.7856509635721456e-06, 1.783256120211166e-06, 1.7808644887179238e-06, 1.7784760647847947e-06, 1.776090844109926e-06, 1.7737088223972365e-06, 1.7713299953564068e-06, 1.7689543587028694e-06, 1.7665819081578083e-06, 1.764212639448141e-06, 1.761846548306518e-06, 1.7594836304713103e-06, 1.7571238816866108e-06, 1.7547672977022144e-06, 1.7524138742736182e-06, 1.750063607162011e-06, 1.7477164921342665e-06, 1.745372524962939e-06, 1.7430317014262481e-06, 1.740694017308078e-06, 1.738359468397966e-06, 1.736028050491096e-06, 1.7336997593882948e-06, 1.7313745908960173e-06, 1.7290525408263432e-06, 1.7267336049969695e-06, 1.7244177792312008e-06, 1.722105059357947e-06, 1.7197954412117094e-06, 1.7174889206325758e-06, 1.715185493466212e-06, 1.7128851555638597e-06, 1.7105879027823215e-06, 1.7082937309839564e-06, 1.7060026360366739e-06, 1.7037146138139235e-06, 1.7014296601946928e-06, 1.6991477710634935e-06, 1.6968689423103563e-06, 1.6945931698308258e-06, 1.692320449525949e-06, 1.6900507773022743e-06, 1.6877841490718364e-06, 1.685520560752154e-06, 1.683260008266219e-06, 1.6810024875424965e-06, 1.6787479945149075e-06, 1.6764965251228267e-06, 1.674248075311076e-06, 1.6720026410299142e-06, 1.6697602182350355e-06, 1.6675208028875541e-06, 1.6652843909540025e-06, 1.6630509784063224e-06, 1.6608205612218561e-06, 1.658593135383346e-06, 1.6563686968789177e-06, 1.6541472417020786e-06, 1.6519287658517095e-06, 1.6497132653320558e-06, 1.6475007361527266e-06, 1.645291174328678e-06, 1.6430845758802123e-06, 1.6408809368329677e-06, 1.6386802532179171e-06, 1.6364825210713519e-06, 1.634287736434881e-06, 1.632095895355423e-06, 1.629906993885195e-06, 1.6277210280817147e-06, 1.6255379940077822e-06, 1.623357887731479e-06, 1.6211807053261615e-06, 1.6190064428704493e-06, 1.6168350964482263e-06, 1.6146666621486235e-06, 1.6125011360660198e-06, 1.6103385143000295e-06, 1.6081787929555028e-06, 1.6060219681425095e-06, 1.6038680359763378e-06, 1.6017169925774857e-06, 1.5995688340716534e-06, 1.597423556589741e-06, 1.5952811562678333e-06, 1.5931416292471991e-06, 1.5910049716742819e-06, 1.5888711797006927e-06, 1.5867402494832069e-06, 1.5846121771837512e-06, 1.5824869589694004e-06, 1.5803645910123708e-06, 1.5782450694900097e-06, 1.576128390584796e-06, 1.5740145504843251e-06, 1.5719035453813054e-06, 1.569795371473551e-06, 1.567690024963979e-06, 1.5655875020605955e-06, 1.5634877989764932e-06, 1.5613909119298437e-06, 1.5592968371438892e-06, 1.5572055708469414e-06, 1.5551171092723663e-06, 1.5530314486585827e-06, 1.5509485852490548e-06, 1.548868515292283e-06, 1.5467912350418032e-06, 1.5447167407561725e-06, 1.5426450286989664e-06, 1.5405760951387706e-06, 1.538509936349179e-06, 1.5364465486087796e-06, 1.5343859282011522e-06, 1.5323280714148617e-06, 1.5302729745434483e-06, 1.5282206338854281e-06, 1.5261710457442768e-06, 1.5241242064284293e-06, 1.5220801122512718e-06, 1.520038759531133e-06, 1.5180001445912834e-06, 1.5159642637599207e-06, 1.5139311133701686e-06, 1.5119006897600682e-06, 1.5098729892725713e-06, 1.5078480082555372e-06, 1.5058257430617203e-06, 1.5038061900487678e-06, 1.50178934557921e-06, 1.4997752060204602e-06, 1.497763767744799e-06, 1.4957550271293743e-06, 1.4937489805561923e-06, 1.4917456244121107e-06, 1.4897449550888364e-06, 1.487746968982912e-06, 1.4857516624957142e-06, 1.483759032033446e-06, 1.4817690740071288e-06, 1.4797817848326016e-06, 1.477797160930506e-06, 1.4758151987262858e-06, 1.473835894650179e-06, 1.4718592451372093e-06, 1.4698852466271857e-06, 1.4679138955646883e-06, 1.4659451883990671e-06, 1.463979121584432e-06, 1.4620156915796534e-06, 1.4600548948483463e-06, 1.4580967278588702e-06, 1.4561411870843204e-06, 1.4541882690025224e-06, 1.4522379700960278e-06, 1.4502902868521033e-06, 1.4483452157627269e-06, 1.4464027533245821e-06, 1.444462896039049e-06, 1.4425256404122043e-06, 1.4405909829548066e-06, 1.4386589201822954e-06, 1.436729448614782e-06, 1.4348025647770494e-06, 1.4328782651985366e-06, 1.4309565464133388e-06, 1.4290374049601999e-06, 1.4271208373825046e-06, 1.4252068402282766e-06, 1.4232954100501662e-06, 1.4213865434054483e-06, 1.4194802368560145e-06, 1.4175764869683664e-06, 1.415675290313615e-06, 1.413776643467465e-06, 1.411880543010216e-06, 1.4099869855267532e-06, 1.4080959676065412e-06, 1.406207485843622e-06, 1.4043215368366024e-06, 1.4024381171886516e-06, 1.4005572235074938e-06, 1.3986788524054058e-06, 1.3968030004992052e-06, 1.3949296644102475e-06, 1.3930588407644193e-06, 1.3911905261921317e-06, 1.3893247173283188e-06, 1.3874614108124232e-06, 1.3856006032883972e-06, 1.3837422914046924e-06, 1.3818864718142555e-06, 1.3800331411745254e-06, 1.3781822961474197e-06, 1.376333933399335e-06, 1.3744880496011368e-06, 1.3726446414281597e-06, 1.3708037055601934e-06, 1.3689652386814815e-06, 1.3671292374807148e-06, 1.3652956986510242e-06, 1.3634646188899785e-06, 1.3616359948995726e-06, 1.3598098233862255e-06, 1.3579861010607734e-06, 1.3561648246384626e-06, 1.3543459908389487e-06, 1.3525295963862828e-06, 1.3507156380089106e-06, 1.348904112439666e-06, 1.347095016415763e-06, 1.345288346678796e-06, 1.3434840999747244e-06, 1.3416822730538742e-06, 1.3398828626709282e-06, 1.338085865584925e-06, 1.3362912785592462e-06, 1.3344990983616164e-06, 1.332709321764094e-06, 1.330921945543066e-06, 1.329136966479246e-06, 1.3273543813576622e-06, 1.3255741869676553e-06, 1.3237963801028719e-06, 1.322020957561258e-06, 1.3202479161450576e-06, 1.3184772526607994e-06, 1.3167089639192972e-06, 1.3149430467356394e-06, 1.313179497929191e-06, 1.3114183143235778e-06, 1.3096594927466879e-06, 1.307903030030663e-06, 1.3061489230118923e-06, 1.304397168531012e-06, 1.3026477634328912e-06, 1.3009007045666323e-06, 1.2991559887855633e-06, 1.2974136129472315e-06, 1.2956735739134015e-06, 1.2939358685500442e-06, 1.2922004937273346e-06, 1.2904674463196447e-06, 1.288736723205538e-06, 1.2870083212677676e-06, 1.2852822373932639e-06, 1.2835584684731335e-06, 1.2818370114026516e-06, 1.2801178630812602e-06, 1.278401020412557e-06, 1.276686480304293e-06, 1.2749742396683667e-06, 1.2732642954208167e-06, 1.2715566444818219e-06, 1.2698512837756875e-06, 1.2681482102308452e-06, 1.266447420779846e-06, 1.2647489123593543e-06, 1.2630526819101454e-06, 1.261358726377095e-06, 1.2596670427091763e-06, 1.2579776278594544e-06, 1.2562904787850834e-06, 1.2546055924472956e-06, 1.2529229658113992e-06, 1.251242595846773e-06, 1.2495644795268588e-06, 1.2478886138291614e-06, 1.246214995735235e-06, 1.2445436222306845e-06, 1.2428744903051561e-06, 1.2412075969523332e-06, 1.2395429391699338e-06, 1.2378805139597002e-06, 1.2362203183273957e-06, 1.2345623492828005e-06, 1.2329066038397027e-06, 1.2312530790159002e-06, 1.2296017718331859e-06, 1.2279526793173493e-06, 1.2263057984981664e-06, 1.2246611264094008e-06, 1.2230186600887912e-06, 1.2213783965780494e-06, 1.2197403329228559e-06, 1.2181044661728512e-06, 1.2164707933816366e-06, 1.2148393116067617e-06, 1.2132100179097232e-06, 1.2115829093559594e-06, 1.2099579830148421e-06, 1.208335235959678e-06, 1.2067146652676952e-06, 1.2050962680200426e-06, 1.2034800413017842e-06, 1.2018659822018917e-06, 1.200254087813245e-06, 1.198644355232619e-06, 1.1970367815606836e-06, 1.1954313639019963e-06, 1.1938280993650003e-06, 1.1922269850620146e-06, 1.1906280181092318e-06, 1.189031195626712e-06, 1.1874365147383766e-06, 1.1858439725720075e-06, 1.1842535662592356e-06, 1.1826652929355403e-06, 1.181079149740242e-06, 1.1794951338164971e-06, 1.1779132423112962e-06, 1.1763334723754539e-06, 1.1747558211636062e-06, 1.1731802858342046e-06, 1.1716068635495147e-06, 1.1700355514756047e-06, 1.1684663467823446e-06, 1.1668992466434003e-06, 1.165334248236227e-06, 1.1637713487420682e-06, 1.1622105453459455e-06, 1.160651835236656e-06, 1.1590952156067676e-06, 1.1575406836526122e-06, 1.1559882365742853e-06, 1.1544378715756338e-06, 1.1528895858642561e-06, 1.1513433766514957e-06, 1.1497992411524349e-06, 1.1482571765858943e-06, 1.1467171801744217e-06, 1.14517924914429e-06, 1.143643380725492e-06, 1.142109572151738e-06, 1.1405778206604457e-06, 1.1390481234927388e-06, 1.137520477893441e-06, 1.13599488111107e-06, 1.1344713303978366e-06, 1.1329498230096348e-06, 1.1314303562060383e-06, 1.1299129272502974e-06, 1.1283975334093313e-06, 1.126884171953728e-06, 1.1253728401577324e-06, 1.123863535299247e-06, 1.1223562546598234e-06, 1.120850995524662e-06, 1.1193477551826022e-06, 1.1178465309261195e-06, 1.1163473200513204e-06, 1.1148501198579378e-06, 1.113354927649328e-06, 1.1118617407324622e-06, 1.110370556417923e-06, 1.108881372019901e-06, 1.1073941848561869e-06, 1.105908992248173e-06, 1.1044257915208397e-06, 1.102944580002757e-06, 1.1014653550260771e-06, 1.09998811392653e-06, 1.0985128540434204e-06, 1.0970395727196204e-06, 1.0955682673015648e-06, 1.0940989351392476e-06, 1.0926315735862196e-06, 1.0911661799995777e-06, 1.0897027517399642e-06, 1.0882412861715613e-06, 1.086781780662086e-06, 1.0853242325827873e-06, 1.0838686393084377e-06, 1.0824149982173315e-06, 1.0809633066912786e-06, 1.0795135621156002e-06, 1.0780657618791259e-06, 1.0766199033741854e-06, 1.0751759839966064e-06, 1.0737340011457083e-06, 1.0722939522243009e-06, 1.0708558346386748e-06, 1.0694196457986003e-06, 1.0679853831173208e-06, 1.066553044011549e-06, 1.0651226259014643e-06, 1.0636941262107036e-06, 1.06226754236636e-06, 1.0608428717989768e-06, 1.0594201119425433e-06, 1.0579992602344925e-06, 1.0565803141156915e-06, 1.0551632710304404e-06, 1.053748128426467e-06, 1.0523348837549216e-06, 1.0509235344703745e-06, 1.0495140780308086e-06, 1.048106511897616e-06, 1.046700833535593e-06, 1.0452970404129381e-06, 1.043895130001243e-06, 1.042495099775492e-06, 1.0410969472140549e-06, 1.0397006697986826e-06, 1.0383062650145065e-06, 1.036913730350028e-06, 1.0355230632971175e-06, 1.0341342613510097e-06, 1.032747322010297e-06, 1.0313622427769296e-06, 1.0299790211562057e-06, 1.0285976546567696e-06, 1.0272181407906062e-06, 1.02584047707304e-06, 1.0244646610227252e-06, 1.0230906901616447e-06, 1.0217185620151046e-06, 1.0203482741117291e-06, 1.0189798239834604e-06, 1.017613209165547e-06, 1.0162484271965444e-06, 1.0148854756183092e-06, 1.013524351975994e-06, 1.0121650538180464e-06, 1.0108075786961991e-06, 1.0094519241654695e-06, 1.0080980877841533e-06, 1.0067460671138213e-06, 1.0053958597193163e-06, 1.004047463168745e-06, 1.0027008750334756e-06, 1.001356092888134e-06, 1.0000131143106003e-06, 9.98671936882001e-07, 9.973325581867076e-07, 9.95994975812331e-07, 9.946591873497173e-07, 9.933251903929453e-07, 9.91992982539319e-07, 9.906625613893647e-07, 9.893339245468271e-07, 9.880070696186642e-07, 9.866819942150455e-07, 9.853586959493434e-07, 9.84037172438132e-07, 9.827174213011817e-07, 9.813994401614546e-07, 9.80083226645103e-07, 9.787687783814605e-07, 9.774560930030415e-07, 9.76145168145534e-07, 9.748360014477996e-07, 9.735285905518644e-07, 9.722229331029177e-07, 9.709190267493064e-07, 9.69616869142531e-07, 9.683164579372447e-07, 9.670177907912422e-07, 9.65720865365462e-07, 9.644256793239786e-07, 9.631322303339988e-07, 9.618405160658605e-07, 9.605505341930237e-07, 9.592622823920694e-07, 9.579757583426936e-07, 9.566909597277072e-07, 9.55407884233026e-07, 9.541265295476702e-07, 9.528468933637596e-07, 9.515689733765081e-07, 9.502927672842232e-07, 9.490182727882971e-07, 9.477454875932055e-07, 9.46474409406503e-07, 9.452050359388172e-07, 9.439373649038496e-07, 9.426713940183643e-07, 9.414071210021898e-07, 9.401445435782116e-07, 9.388836594723687e-07, 9.376244664136526e-07, 9.36366962134098e-07, 9.351111443687819e-07, 9.338570108558185e-07, 9.326045593363576e-07, 9.313537875545767e-07, 9.301046932576786e-07, 9.288572741958883e-07, 9.276115281224466e-07, 9.263674527936106e-07, 9.251250459686438e-07, 9.238843054098159e-07, 9.226452288823977e-07, 9.21407814154656e-07, 9.201720589978539e-07, 9.189379611862403e-07, 9.177055184970505e-07, 9.164747287105001e-07, 9.152455896097838e-07, 9.140180989810676e-07, 9.127922546134868e-07, 9.115680542991421e-07, 9.103454958330947e-07, 9.09124577013365e-07, 9.079052956409244e-07, 9.066876495196944e-07, 9.05471636456542e-07, 9.042572542612741e-07, 9.030445007466381e-07, 9.01833373728312e-07, 9.006238710249042e-07, 8.994159904579489e-07, 8.982097298519008e-07, 8.970050870341353e-07, 8.958020598349387e-07, 8.946006460875084e-07, 8.93400843627947e-07, 8.922026502952612e-07, 8.91006063931354e-07, 8.898110823810234e-07, 8.886177034919579e-07, 8.874259251147318e-07, 8.862357451028045e-07, 8.850471613125118e-07, 8.838601716030655e-07, 8.826747738365481e-07, 8.814909658779091e-07, 8.803087455949634e-07, 8.791281108583836e-07, 8.779490595416984e-07, 8.767715895212881e-07, 8.755956986763827e-07, 8.744213848890551e-07, 8.73248646044219e-07, 8.720774800296245e-07, 8.709078847358541e-07, 8.697398580563219e-07, 8.685733978872647e-07, 8.674085021277413e-07, 8.662451686796288e-07, 8.650833954476168e-07, 8.639231803392077e-07, 8.627645212647081e-07};

  if (lumiAlgo.value_ == "occupancy") {
      memcpy(HFSBR,sbr_oc,sizeof(float)*interface::bril::shared::MAX_NBX);
  } else if (lumiAlgo.value_ == "etsum") {
      memcpy(HFSBR,sbr_et,sizeof(float)*interface::bril::shared::MAX_NBX);
  }
}

