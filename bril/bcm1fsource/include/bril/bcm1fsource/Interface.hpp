/*
 * Interface.h
 *
 *  Created on: 06.01.2011
 *      Author: marekp
 */

#ifndef INTERFACE_H_
#define INTERFACE_H_

// *** Interface

class Interface
{
public:
  // Destructor
  virtual ~Interface(){};
};

#endif /* INTERFACE_H_ */
