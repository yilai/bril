/***************************************************************************** 
 * File  ChangeableContainer.hpp
 * created on 13.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

// #ifndef TOOLS_DATASOURCE_HPP_
// #define TOOLS_DATASOURCE_HPP_

#ifndef TOOLS_DATASOURCE_HPP_
#define TOOLS_DATASOURCE_HPP_

#include <string.h>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/interprocess_recursive_mutex.hpp>

typedef boost::interprocess::interprocess_recursive_mutex DataSourceMutex;
typedef boost::interprocess::scoped_lock<boost::interprocess::interprocess_recursive_mutex> DataSourceLock;

template <class DATA>
class DataSource
{
private:

  unsigned int changeCounter;
  DATA data;
  // mutable DataSourceMutex& m_mutex;
  DataSourceMutex& m_mutex;
public:

  DataSource(DataSourceMutex& mutex_)
    : m_mutex(mutex_)
  {
  }

  // returns the data mutex
  DataSourceMutex& getMutex() const
  {
    return m_mutex;
  }

  unsigned int getChangeCounter() const
  {
    return changeCounter;
  }

  // overrides old data, if new data is different
  void setData(const DATA& newdata)
  {
    if (memcmp(&newdata,&data,sizeof(DATA))!=0) {
      data=newdata;
      changeCounter++;
    }
  }

  // overrides old data, if new data is different
  void setDataFast(const DATA& newdata)
  {
    data=newdata;
    changeCounter++;
  }

  // returns actual data
  const DATA& getData() const
  {
    return data;
  }

};

#endif /* TOOLS_DATASOURCE_HPP_ */
