/***************************************************************************** 
 * File  rhuclient.cpp
 * created on 13.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

#include "bril/bcm1fsource/rhuclient.hpp"

namespace rhu {

  IClient::~IClient()
  {
    // Make lib linker happy
  }

  void IClient::dummy()
  {
    // Make lib linker happy
  }

}
