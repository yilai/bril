#include "log4cplus/loggingmacros.h"
#include "bril/timesource/tcdsinfo.h"
#include "bril/timesource/TCDSHWReceiver.h"
#include "bril/timesource/exception/Exception.h"
#include "uhal/ConnectionManager.hpp"
#include "uhal/ProtocolIPbusCore.hpp"
#include "uhal/ProtocolTCP.hpp"
#include <stdint.h>
//#include <stdexcept>
#include "xcept/Exception.h"

bril::timesource::TCDSHWReceiver::TCDSHWReceiver( const std::string& connections,const std::string& ipbusname,log4cplus::Logger* logger, int fakeerrorinterval, int fakeerrorduration ):m_logger(logger),m_fakeerrorinterval(fakeerrorinterval),m_fakeerrorduration(fakeerrorduration){
  //uhal::setLogLevelTo( uhal::Warning() );
  uhal::setLogLevelTo( uhal::Error() );
  uhal::ConnectionManager ucm( connections );
  m_intf = std::make_shared<uhal::HwInterface>( ucm.getDevice( ipbusname ) );
  m_error_counter = 0;
  m_tcdsread_counter = 0;
  uint32_t timeoutperiod = m_intf->getTimeoutPeriod(); 
  std::stringstream ss;
  ss<<"uhal timeout period: "<<timeoutperiod;
  LOG4CPLUS_INFO( *m_logger,ss.str() );
  timeoutperiod = 300;
  ss.clear();ss.str("");
  ss<<"Setting uhal timeout period: "<<timeoutperiod;
  LOG4CPLUS_INFO( *m_logger,ss.str() );
  m_intf->setTimeoutPeriod( timeoutperiod );
}

void bril::timesource::TCDSHWReceiver::setup_board(){
    uhal::ValWord< uint32_t > board_id = m_intf->getNode( "system.board_id" ).read();
    m_intf->dispatch();
    if ( !board_id.valid() ) XCEPT_RAISE(bril::timesource::exception::SystemBoardError,"Cannot read system.board_id");
    m_intf->getNode( "glib.ctrl.xpoint1_s40" ).write( 1 );
    m_intf->getNode( "glib.ctrl.xpoint1_s41" ).write( 1 );
    m_intf->dispatch();
    //then timing controller module
    m_intf->getNode( "user.timecon.ctrl.bclk_sel" ).write( 2 );
    m_intf->getNode( "user.timecon.ctrl.orbit_sel" ).write( 2 );
    m_intf->getNode( "user.timecon.ctrl.mmcm_reset" ).write( 1 );
    m_intf->dispatch();
    usleep( 10000 ); // wait! (A.R.)
    m_intf->getNode( "user.timecon.ctrl.mmcm_reset" ).write( 0 );
    m_intf->dispatch();
}

uint32_t bril::timesource::TCDSHWReceiver::cdce_locked() const{
  //wait for the external CDCE to lock
  uhal::ValWord< uint32_t > rdy = m_intf->getNode( "glib.status.cdce_lock" ).read();
  m_intf->dispatch();
  return rdy.value();
}

uint32_t  bril::timesource::TCDSHWReceiver::tcds_ready() const{ 
  uhal::ValWord< uint32_t > rdy = m_intf->getNode( "user.timecon.stat.tcds_ready" ).read();
  m_intf->dispatch();
  return rdy.value();
}

bril::timesource::tcdsinfo_t bril::timesource::TCDSHWReceiver::read_tcds( int retry_on_bus_error ){   
  if( m_fakeerrorinterval!=0 && m_tcdsread_counter>m_fakeerrorinterval ){    
    ++m_error_counter;
    //std::cout<<" m_error_counter "<<m_error_counter<<" tcdsread_counter "<<m_tcdsread_counter<<std::endl;    
    if( m_error_counter>=m_fakeerrorduration ){
      m_error_counter = 0;
      if( m_tcdsread_counter>m_fakeerrorinterval ){
	m_tcdsread_counter = 0;
      }
    }else{
      //generate fake error read exception here
      XCEPT_RAISE(bril::timesource::exception::TCDSReadError,"fake TCDSReadError");
    }
  }

  tcdsinfo_t ret;  
  // A bus error is asserted if the read is attempted while the counters are being updated.
  // This is a transient condition and should not last more than a microsecond.
  while ( retry_on_bus_error ) {
    try{
      uhal::ValWord< uint32_t > ln = m_intf->getNode( "user.timecon.tcds_nb_num" ).read();
      uhal::ValWord< uint32_t > ls = m_intf->getNode( "user.timecon.tcds_ls_num" ).read();
      uhal::ValWord< uint32_t > run = m_intf->getNode( "user.timecon.tcds_run_num" ).read();
      uhal::ValWord< uint32_t > fill = m_intf->getNode( "user.timecon.tcds_fill_num" ).read();
      m_intf->dispatch();
      ret.lhc_fill = fill.value();
      ret.cms_run = run.value();
      ret.lumi_section = ls.value();
      ret.lumi_nibble = ln.value();
      retry_on_bus_error = 0;
    } catch ( const uhal::exception::TransactionLevelError &e ) {
        if ( --retry_on_bus_error == 0 ) {
            XCEPT_RAISE(bril::timesource::exception::TCDSReadError, "Failed to read TCDS information: not ready");
            if(m_logger){
                LOG4CPLUS_WARN( *m_logger, std::string("caught uhal::exception::TransactionLevelError: ")+e.what() );
            }else{
                std::cerr<<e.what()<<std::endl;
            }
        }
    } catch ( uhal::exception::NonValidatedMemory& e ) {
        if ( --retry_on_bus_error == 0 ) {
            XCEPT_RAISE(bril::timesource::exception::TCDSReadError, "Failed to read TCDS: non validated memory");
            if(m_logger){
                LOG4CPLUS_WARN( *m_logger, std::string("caught uhal::exception::NonValidatedMemory: ")+e.what() );
            }else{
                std::cerr<<e.what()<<std::endl;
            }
        }
    }catch ( const uhal::exception::IPbusCoreResponseCodeSet& e ){      
        if ( --retry_on_bus_error == 0 ){
            if(m_logger){
                LOG4CPLUS_WARN( *m_logger, std::string("caught uhal::exception::IPbusCoreResponseCodeSet: ")+e.what() );
            }else{
                std::cerr<<e.what()<<std::endl;
            }
            XCEPT_RAISE(bril::timesource::exception::TCDSReadError,e.what());
        }
    }catch( const uhal::exception::TcpConnectionFailure& e ){ //
        LOG4CPLUS_WARN( *m_logger, std::string("caught uhal::exception::TcpConnectionFailure: ")+e.what() );
        if ( --retry_on_bus_error == 0 ){
            XCEPT_RAISE(bril::timesource::exception::TCPConnectError,e.what());
        }
    }catch( std::exception& e ){// unknown error should crash
        if(m_logger){
            LOG4CPLUS_WARN( *m_logger,std::string("caught unknown exception ")+e.what() );
        }else{
            std::cerr<<e.what()<<std::endl;
        }
        if ( --retry_on_bus_error == 0 ){
            XCEPT_RAISE(bril::timesource::exception::UnknownError,e.what());
        }
    }
  }
  if( m_fakeerrorinterval!=0 ){
    ++m_tcdsread_counter;
  }
  return ret;
}

