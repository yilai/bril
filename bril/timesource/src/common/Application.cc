#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "eventing/api/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerFactory.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Properties.h"
#include "xdata/ItemGroupEvent.h"
#include "xdata/Table.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "bril/timesource/Application.h"
#include "bril/timesource/exception/Exception.h"
#include "bril/timesource/NBTimer.h"
#include "bril/timesource/RunStopEvent.h"
#include "bril/timesource/WebUtils.h"
#include "interface/bril/TCDSTopics.hh"

XDAQ_INSTANTIATOR_IMPL (bril::timesource::Application)

bril::timesource::Application::Application (xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this), m_applock(toolbox::BSem::FULL)
{
  xgi::framework::deferredbind(this,this,&bril::timesource::Application::Default,"Default");
  b2in::nub::bind(this, &bril::timesource::Application::onMessage);

  m_simSource = true;
  m_tcdsBus.value_ = "";
  m_tcdsTopic.value_ = "";
  m_swInitRun = 1;
  m_swInitFill = 1;
  m_swLSPerRun = 1;
  m_swNRuns = 1;
  m_swRunPerFill = 1;
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  std::string memPoolName = getApplicationDescriptor()->getClassName() + std::string("_memPool");
  toolbox::net::URN urn("toolbox-mem-pool",memPoolName);

  m_currentfill = 0;
  m_currentrun = 0;
  m_currentls = 0;
  m_currentnb = 0;
  m_cmson = false;
  m_deadfrac = 0.;
  m_ndeadfrac = 0;
  m_ncollidingbx = 0;
  m_norbs = 0;
  m_nbperls = 0;
  m_nruns = 0;
  m_runtimername = "timer_RUN";
  m_extratimername = "timer_EXTRA";
  m_runtimer = 0;
  m_extratimer = 0;
  m_swInitRun = 1;
  m_swInitFill = 1;
  m_swLSPerRun = 1;
  m_swRunPerFill = 1;
  m_instanceid = 0;
  if( getApplicationDescriptor()->hasInstanceNumber() ){
    m_instanceid = getApplicationDescriptor()->getInstance();
  }
  try{
    getApplicationInfoSpace()->fireItemAvailable("simSource",&m_simSource);
    getApplicationInfoSpace()->fireItemAvailable("tcdsBus",&m_tcdsBus);
    getApplicationInfoSpace()->fireItemAvailable("tcdsTopic",&m_tcdsTopic);
    getApplicationInfoSpace()->fireItemAvailable("outputBuses",&m_outputBuses);
    getApplicationInfoSpace()->fireItemAvailable("swInitRun",&m_swInitRun);
    getApplicationInfoSpace()->fireItemAvailable("swInitFill",&m_swInitFill);
    getApplicationInfoSpace()->fireItemAvailable("swLSPerRun",&m_swLSPerRun);
    getApplicationInfoSpace()->fireItemAvailable("swNRuns",&m_swNRuns);
    getApplicationInfoSpace()->fireItemAvailable("swRunPerFill",&m_swRunPerFill);
    getApplicationInfoSpace()->fireItemAvailable("swNBTimers",&m_swNBTimers);
    getApplicationInfoSpace()->fireItemAvailable("swNBTimerStrs",&m_swNBTimerStrs);
    getApplicationInfoSpace()->fireItemAvailable("startrun",&m_currentrun);
    getApplicationInfoSpace()->fireItemAvailable("currentfill",&m_currentfill);        
    getApplicationInfoSpace()->fireItemAvailable("movingWindowSize",&m_moving_window_size);        
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    m_memPool = m_poolFactory->createPool(urn,allocator);

    m_tcdswatcher = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_"+m_instanceid.toString()+"_tcdswatcher","waiting");
    m_as_tcdswatcher = toolbox::task::bind(this,&bril::timesource::Application::watching,"watching");
    
  }catch(xdata::exception::Exception& e){
    std::string msg = "Failed to setup infospace ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::timesource::exception::Application, msg, e);
  }catch(toolbox::mem::exception::Exception& e){
    std::string msg = "Failed to create memory pool ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::timesource::exception::Application, msg, e);
  }
 
  toolbox::net::URN monurn = createQualifiedInfoSpace( getApplicationDescriptor()->getClassName());
  m_monURN = monurn.toString();
  m_monInfoSpace = xdata::getInfoSpaceFactory()->get(m_monURN);
  m_lasttcdssec = 0;
  m_hastcds = false;
  m_fakenb4counts = 0;
  try{    
    m_monInfoSpace->fireItemAvailable("currentfill",&m_currentfill);
    m_monItemList.push_back("currentfill");
    m_monInfoSpace->fireItemAvailable("currentrun",&m_currentrun);
    m_monItemList.push_back("currentrun");
    m_monInfoSpace->fireItemAvailable("currentls",&m_currentls);
    m_monItemList.push_back("currentls");
    m_monInfoSpace->fireItemAvailable("currentnb",&m_currentnb);
    m_monItemList.push_back("currentnb");
    m_monInfoSpace->addGroupRetrieveListener(this);

    m_monstatus_table.insert(std::make_pair("currentfill",&m_currentfill));
    m_monstatus_table.insert(std::make_pair("currentrun",&m_currentrun));
    m_monstatus_table.insert(std::make_pair("currentls",&m_currentls));
    m_monstatus_table.insert(std::make_pair("currentnb",&m_currentnb));
    m_monstatus_table.insert(std::make_pair("deadfrac",&m_deadfrac));
    m_monstatus_table.insert(std::make_pair("ndeadfrac",&m_ndeadfrac));
  }catch(xcept::Exception& e){
    std::string msg = "Failed to put parameters into monInfoSpace ";
    LOG4CPLUS_FATAL(getApplicationLogger(),stdformat_exception_history(e));
    XCEPT_RETHROW(bril::timesource::exception::Application, msg, e);
  }

  m_moving_window_size = 12;
}

bril::timesource::Application::~Application (){
  for(std::vector< NBTimer* >::iterator it=m_nbtimerstore.begin();it!=m_nbtimerstore.end();++it){
    delete *it;
  }
  if(m_runtimer->isActive()){
    m_runtimer->stop();      
  }
  toolbox::task::getTimerFactory()->removeTimer(m_runtimername);
}

void bril::timesource::Application::Default (xgi::Input * in, xgi::Output * out){
  std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+ getApplicationDescriptor()->getURN();
  *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
  *out << cgicc::br();
  *out << "<p> Eventing Status</p>";
  *out <<busesToHTML();   
  m_monInfoSpace->lock();   
  m_monInfoSpace->fireItemGroupRetrieve(m_monItemList,this); 
  m_monInfoSpace->unlock();  
  *out << bril::timesource::WebUtils::mapToHTML("Configuration",m_monconfig_table,"",false);
  *out << bril::timesource::WebUtils::mapToHTML("Run Status",m_monstatus_table,"",false);  
}

void bril::timesource::Application::actionPerformed(toolbox::Event& e){
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received toolbox event " << e.type());
  if( e.type() == "eventing::api::BusReadyToPublish"){
    std::stringstream msg;
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    if( m_unreadybuses.find(busname)!=m_unreadybuses.end() ){
      m_unreadybuses.erase(busname);
    }
    if(m_unreadybuses.size()!=0) return;
    
    if(m_simSource){
      if(!m_runtimer){
	try{	
	  toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	  toolbox::TimeInterval delta(1,0);
	  start += delta;
	  toolbox::TimeInterval runduration( (double)m_swLSPerRun.value_*(s_lsduration) );
	  m_runtimer = toolbox::task::getTimerFactory()->createTimer(m_runtimername);
	  msg<<"Sim run duration "<<runduration;
	  LOG4CPLUS_INFO(this->getApplicationLogger(),msg.str());
	  m_runtimer->schedule(this,start,(void*)0, m_runtimername+"_firstrun");
	  m_runtimer->scheduleAtFixedRate(start,this,runduration,(void*)0, m_runtimername+"_run");	
	}catch(toolbox::task::exception::Exception& e){
	  msg<<"Failed to start timer "<<m_runtimername <<" "<< stdformat_exception_history(e);
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
	}
      }
    }else{      
      try{
	msg.str("");
	msg<<"subscribing to "<<m_tcdsTopic.value_;
	LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
	this->getEventingBus(m_tcdsBus.value_).subscribe(m_tcdsTopic.value_);
	// we started waiting for tcds from now, assuming we'll have it
	m_hastcds = true;
	m_lasttcdssec = toolbox::TimeVal::gettimeofday().sec();
      }catch(eventing::api::exception::Exception& e){
	msg<<"Failed to subscribe to "<<m_tcdsBus.value_<<" "<<stdformat_exception_history(e);
	LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
	XCEPT_RETHROW(bril::timesource::exception::Exception,msg.str(),e);
      }
      //start tcdswatch wl thread
      m_tcdswatcher->activate();
      //create tcds fallback timer            
      msg.str("");
      msg<<"starting tcdswatcher workloop";
      LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
      m_tcdswatcher->submit(m_as_tcdswatcher);
    }

  }
}

void bril::timesource::Application::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());
  std::stringstream msg;
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    if(m_simSource){      
      std::set<std::string> allbuses;
      size_t ntimers=m_swNBTimers.elements();
      for(size_t i=0;i<ntimers;++i){
	xdata::Properties* p=dynamic_cast<xdata::Properties*>(m_swNBTimers.elementAt(i));
	if(p){
	  std::string signalTopic = p->getProperty("signalTopic");
	  std::string nbFrequencyStr = p->getProperty("nbFrequency");
	  std::string busesStr = p->getProperty("buses");
	  if(busesStr.empty()){
	     LOG4CPLUS_WARN(getApplicationLogger(),"empty buses parameter");
	  }
	  std::set<std::string> buses = toolbox::parseTokenSet(busesStr,",");
	  allbuses.insert(buses.begin(),buses.end());
	  xdata::UnsignedInteger nbFrequency;
	  nbFrequency.fromString(nbFrequencyStr);
	  NBTimer* nbt = new NBTimer(const_cast<bril::timesource::Application*>(this),signalTopic,nbFrequency,buses,m_monInfoSpace);
	  std::string nbtimerStr = std::string("signalTopic:")+signalTopic+std::string(" nbFrequency:")+nbFrequency.toString()+std::string(" buses:")+busesStr;
	  xdata::String nbtimerString;
	  nbtimerString.fromString(nbtimerStr);
	  m_swNBTimerStrs.push_back(nbtimerString);
	  
	  m_dispatcher.addActionListener( nbt );
	  m_nbtimerstore.push_back(nbt);
	}
      }
      for(std::set<std::string>::iterator it=allbuses.begin(); it!=allbuses.end(); ++it){
	this->getEventingBus(*it).addActionListener(this);
      }
      
      m_monconfig_table.insert(std::make_pair("simSource",&m_simSource)); 
      m_monconfig_table.insert(std::make_pair("swNBTimerStrs",&m_swNBTimerStrs));
      m_monconfig_table.insert(std::make_pair("swInitRun",&m_swInitRun));
      m_monconfig_table.insert(std::make_pair("swInitFill",&m_swInitFill));
      m_monconfig_table.insert(std::make_pair("swLSPerRun",&m_swLSPerRun));
      m_monconfig_table.insert(std::make_pair("swNRuns",&m_swNRuns));
      m_monconfig_table.insert(std::make_pair("swRunPerFill",&m_swRunPerFill));
      m_currentfill = m_swInitFill.value_;
      m_currentrun = m_swInitRun.value_;   
    }else{
      
      m_outbuses = toolbox::parseTokenSet(m_outputBuses,",");
      m_unreadybuses = m_outbuses;
      m_monconfig_table.insert(std::make_pair("simSource",&m_simSource)); 
      m_monconfig_table.insert(std::make_pair("tcdsBus",&m_tcdsBus));
      m_monconfig_table.insert(std::make_pair("tcdsTopic",&m_tcdsTopic));
      m_monconfig_table.insert(std::make_pair("outputBuses",&m_outputBuses));
      
      this->getEventingBus(m_tcdsBus.value_).addActionListener(this); 
      for(std::set<std::string>::iterator it=m_outbuses.begin(); it!=m_outbuses.end(); ++it){
	if(*it!=m_tcdsBus.value_){
	  this->getEventingBus(*it).addActionListener(this);
	}
      }  
    }
  }
}

void bril::timesource::Application::timeExpired(toolbox::task::TimerEvent& e){
  std::string taskname = e.getTimerTask()->name;
  if(m_simSource){//sim mode
    m_applock.take();
    m_nruns++;
    if( m_nruns>m_swRunPerFill.value_ ){
      m_currentfill++;
    }
    m_applock.give();
    if(taskname == (m_runtimername+"_firstrun") ){
      LOG4CPLUS_INFO(getApplicationLogger(),"Sim first run start");
      do_runstart();//runtimer
    }else if(taskname == (m_runtimername+"_run") ){
      bril::timesource::RunStopEvent e;
      m_dispatcher.fireEvent(e);
      if((m_currentrun+1-m_swInitRun)>=m_swNRuns){
	//reset run number
	m_applock.take();
	m_currentrun = m_swInitRun;      // reset run number
	m_applock.give();
      }else{
	//increase run number
	m_applock.take();
	m_currentrun++; 
	m_applock.give();
      }
      LOG4CPLUS_INFO(getApplicationLogger(),"Sim run restart ");
      do_runstart();
    }   
  }else{//real mode tcds fallback timer
    m_applock.take(); 
    if( m_currentnb%4!=0 ){
      m_currentnb = (int(m_currentnb/4)+1)*4;
    }
    m_currentnb = m_currentnb+4;
    if(m_currentnb.value_>64){
      m_currentnb = 4;
      m_currentls = m_currentls+1;      
    }
    if( m_fakenb4counts == 0 ){
      m_currentnb = m_currentnb+4;//the first switch takes 2 * nb4
    }
    m_fakenb4counts++;
    m_cmson = false;
    m_deadfrac = 0.; 
    m_ndeadfrac = 0;
    m_applock.give();
    do_publish_timesignal("NB4",4);
    do_publish_tcds(false);
    LOG4CPLUS_INFO(getApplicationLogger(),"On fake timer ");
  }
}

void bril::timesource::Application::do_runstart(){
  if(m_simSource){
    try{
      getApplicationInfoSpace()->fireItemValueChanged("startrun");
    }catch(xdata::exception::Exception& e){
      std::string msg = "Failed to fire item valuechanged startrun ";
      LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    }
  }  
}

bool bril::timesource::Application::watching(toolbox::task::WorkLoop* wl){  
  std::stringstream msg;
  toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
  unsigned int nowsec = now.sec();
  if( m_hastcds&&((nowsec - m_lasttcdssec)>2) ){
    msg << "tcds stopped on sec "<<m_lasttcdssec<<" start fallback timer";
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    m_applock.take();
    m_hastcds = false;
    m_applock.give();
    try{
      if(!m_extratimer){	
	m_extratimer = toolbox::task::getTimerFactory()->createTimer(m_extratimername);      
      }
      toolbox::TimeInterval nb4duration = s_nbduration+s_nbduration+s_nbduration+s_nbduration;
      m_extratimer->scheduleAtFixedRate(now,this,nb4duration,(void*)0,m_extratimername);
    }catch(toolbox::task::exception::Exception& e){
      msg.str("");
      msg<<"Failed to start tcds fallback timer "<<m_extratimername <<" "<<stdformat_exception_history(e);
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
    }
  }
  ::sleep(2);  
  return true;
}

void bril::timesource::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist){
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    std::stringstream msg;
    if(topic == m_tcdsTopic.toString()){
      if(ref!=0){
	if( m_extratimer&&m_extratimer->isActive()&&!m_hastcds ){
	  m_extratimer->stop();
	  toolbox::task::getTimerFactory()->removeTimer(m_extratimername);
	  m_extratimer = 0;
	}
	m_applock.take();
	m_lasttcdssec = toolbox::TimeVal::gettimeofday().sec();
	m_hastcds = true;
	m_applock.give();

	//parse tcds message 
	xdata::Table table;
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
	xdata::exdr::Serializer serializer;
	try{
	  unsigned int previous_fill = m_currentfill.value_;
	  serializer.import(&table, &inBuffer);
	  xdata::Serializable* p = 0;
	  p =  table.getValueAt(0,"FillNumber");
	  if(p){
	    m_currentfill = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_;
	    p = 0;
	  }
	  p = table.getValueAt(0,"RunNumber");
	  if(p){
	    m_currentrun = dynamic_cast<xdata::UnsignedInteger32*>( p )->value_;
	    p = 0;
	  }
	  p = table.getValueAt(0,"SectionNumber");
	  if(p){
	    m_currentls = dynamic_cast<xdata::UnsignedInteger32*>( p )->value_ ;
	    p = 0;
	  }
	  p = table.getValueAt(0,"CMSRunActive");
	  if(p){
	    m_cmson = dynamic_cast<xdata::Boolean*>( p )->value_ ;	  
	    p = 0;
	  }
	  p = table.getValueAt(0,"NibbleNumber");
	  if(p){
	    m_currentnb = dynamic_cast<xdata::UnsignedInteger32*>( p )->value_ ;
	    p = 0;
	  }	  
	  p = table.getValueAt(0,"NumBXBeamActive");
	  unsigned int nbxbeamactive = 0;	  
	  if(p){
	    nbxbeamactive = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_ ;
	    unsigned int ncollidingbx = nbxbeamactive/4096;
	    p = 0;

	    if( m_currentfill.value_!=previous_fill ){//this is a new fill , clear the moving window and trust/publish the new value
	      m_ncollidingbx = ncollidingbx;

	      m_nbx_moving_window.clear();
	    }else{ //this is ongoing fill, push ncollidingbx into the moving window
	      m_nbx_moving_window.push_back(ncollidingbx);
	      if( m_nbx_moving_window.size()>m_moving_window_size.value_ ){  //if the queue is full, rotate the queue and check if all elements are equal. 
		m_nbx_moving_window.pop_front();
		if( std::equal( m_nbx_moving_window.begin()+1, m_nbx_moving_window.end(), m_nbx_moving_window.begin() ) ){
		  m_ncollidingbx = ncollidingbx; //if all elements in the window are equal, this value is stable enough to be trusted/published, else keep publishing the old value m_ncollidingbx 		  
		}else{
		  msg.clear(); msg.str("");	  
		  msg<<" fill "<<m_currentfill.value_<<" nbx moving window elements are not equal , FLIPPING ";
		  LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
		}
	      }  
	    }	    
	  }
	  p = table.getValueAt(0,"DeadtimeBeamActive");
	  if(p){
	    float dtbeam = dynamic_cast<xdata::Float*>(p)->value_/100. ;
	    if(m_ncollidingbx.value_>0 && m_cmson.value_){
	      m_deadfrac.value_ = m_deadfrac.value_ + dtbeam;
	      m_ndeadfrac.value_ = m_ndeadfrac.value_ + 1;
	    }
	    p = 0;	    
	  }	  
	  
	  p = table.getValueAt(0,"NumOrbits");
	  if(p){
	    m_norbs = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_ ;
	    p = 0;
	  }
	  p = table.getValueAt(0,"NumNibblesPerSection");
	  if(p){
	    m_nbperls = dynamic_cast<xdata::UnsignedInteger32*>(p)->value_ ;
	    p = 0;
	  }

	  msg<<"Received tcds msg: "<<m_currentfill.value_<<" "<<m_currentrun.value_<<" "<<m_currentls.value_<<" "<<m_currentnb.value_<<" deadfrac "<<m_deadfrac.value_<<" nbxbeamactive "<<nbxbeamactive<<" nbx "<<m_ncollidingbx.value_<<" norbits "<<m_norbs.value_<<" nbperls "<<m_nbperls.value_;
	  LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
	  msg.str("");	  
	  
	  if( m_currentnb.value_!=0 ){
	    do_publish_timesignal("NB1",1);
	    if( m_currentnb.value_%4==0 ){
	      do_publish_timesignal("NB4",4);	      
	      do_publish_tcds(false);
	    }
	    if( m_currentnb.value_==64 ){
	      do_publish_timesignal("NB64",64);
	    }
	  }	  
	  
	}catch (xdata::exception::Exception& err){
	  LOG4CPLUS_ERROR(getApplicationLogger(),"Failed to deserialize incoming tcds table"); 
	  if(ref!=0){
	    ref->release();
	    ref=0;
	  }
	}
      }
    }
  }
  if(ref!=0){
    ref->release();
    ref=0;
  }
}

void bril::timesource::Application::do_publish_timesignal(const std::string& topicname, unsigned int frequency){

  std::stringstream msg;
  toolbox::mem::Reference* bufRef = 0;
  toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
  size_t msgsize = sizeof(interface::bril::shared::DatumHead);
  try{
    bufRef = m_poolFactory->getFrame(m_memPool,msgsize);
    bufRef->setDataSize(msgsize);
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_currentfill.value_,m_currentrun.value_,m_currentls.value_,m_currentnb.value_,t.sec(),t.millisec());
    header->setResource(interface::bril::shared::DataSource::TCDS,0,0,0);
    header->setFrequency(frequency);
    header->setTotalsize(msgsize);
    for(std::set<std::string>::iterator it=m_outbuses.begin();it!=m_outbuses.end();++it){
      msg<<"Publishing "<<topicname<<" to "<<*it<<" : "<<m_currentfill.value_<<" "<<m_currentrun.value_<<" "<<m_currentls.value_<<" "<<m_currentnb.value_<<" "<<t.sec()<<"."<<t.millisec()<<" nbx "<<m_ncollidingbx.value_;
      LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
      this->getEventingBus(*it).publish(topicname, bufRef->duplicate(), plist);
      msg.str("");
    }
    if(bufRef){
      bufRef->release(); bufRef = 0;
    }
  }catch(xcept::Exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
  }
}

void bril::timesource::Application::do_publish_tcds(bool nostore){

  std::stringstream msg;
  toolbox::mem::Reference* bufRef = 0;
  toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
  size_t msgsize = interface::bril::tcdsT::maxsize();
  std::string payloaddict = interface::bril::tcdsT::payloaddict();
  std::string topicname = interface::bril::tcdsT::topicname();
  try{
    bufRef = m_poolFactory->getFrame(m_memPool,msgsize);
    bufRef->setDataSize(msgsize);
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION", interface::bril::shared::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT", payloaddict );
    if(nostore || m_ncollidingbx.value_<1){
       plist.setProperty("NOSTORE", "1");
    }
    interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    header->setTime(m_currentfill.value_,m_currentrun.value_,m_currentls.value_,m_currentnb.value_,t.sec(),t.millisec());
    header->setResource(interface::bril::shared::DataSource::TCDS,0,0,interface::bril::shared::StorageType::COMPOUND);
    header->setFrequency(4);
    header->setTotalsize(msgsize);
    interface::bril::shared::CompoundDataStreamer tc(payloaddict); 
    tc.insert_field( header->payloadanchor, "norb", &m_norbs.value_ );
    tc.insert_field( header->payloadanchor, "nbperls", &m_nbperls.value_ );
    tc.insert_field( header->payloadanchor, "cmson", &m_cmson.value_ );
    float mydeadfrac = 1.0;
    if( m_cmson.value_ && m_ncollidingbx.value_>0 && m_ndeadfrac.value_>0 ){
      mydeadfrac = m_deadfrac.value_/float(m_ndeadfrac.value_);
    }
    tc.insert_field( header->payloadanchor, "deadfrac", &mydeadfrac );    
    tc.insert_field( header->payloadanchor, "ncollidingbx", &m_ncollidingbx.value_ );    
    for(std::set<std::string>::iterator it=m_outbuses.begin();it!=m_outbuses.end();++it){
      msg<<"Publishing "<<topicname<<" to "<<*it<<" : "<<m_currentfill.value_<<" "<<m_currentrun.value_<<" "<<m_currentls.value_<<" "<<m_currentnb.value_<<" "<<t.sec()<<"."<<t.millisec()<<" cmson "<<m_cmson.value_<<" ncollidingbx "<<m_ncollidingbx.value_<<" avgdeadfrac "<<mydeadfrac<<" n "<<m_ndeadfrac.value_;
      LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
      this->getEventingBus(*it).publish(topicname, bufRef->duplicate(), plist);
      msg.str("");      
    }
    if(bufRef){
      bufRef->release(); bufRef = 0;
    }
    m_applock.take();
    m_deadfrac = 0.0;
    m_ndeadfrac = 0;    
    m_applock.give();
  }catch(xcept::Exception& e){
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
  }
}
