#ifndef _bril_timesource_tcdsinfo_h_
#define _bril_timesource_tcdsinfo_h_
#include <stdint.h>
#include <sstream>
namespace bril{
  namespace timesource{
    class tcdsinfo_t
    {
    public:
    tcdsinfo_t():lhc_fill(0),cms_run(0),lumi_section(0),lumi_nibble(0){}
    tcdsinfo_t(unsigned int f, unsigned int r, unsigned int s, unsigned int n):lhc_fill(f),cms_run(r),lumi_section(s),lumi_nibble(n){}  
      uint32_t lhc_fill;
      uint32_t cms_run;
      uint32_t lumi_section;
      uint32_t lumi_nibble;
      tcdsinfo_t& operator++() { //prefix ++
	if( this->lumi_nibble==64 ){
	  this->lumi_nibble = 1;
	  this->lumi_section++;
	}else{
	  this->lumi_nibble++;
	}
	return *this; 
      }
      tcdsinfo_t operator++(int) { //postfix ++
	tcdsinfo_t result(*this); //make a copy for result
	++(*this); // Now use the prefix version to do the work
	return result; // return the copy (the old) value.
      }   
      std::string toString()const{
	std::stringstream ss;
	ss << this->lhc_fill<<" "<<this->cms_run<<" "<<this->lumi_section<<" "<<this->lumi_nibble;
	return ss.str();
      }      
    };     
  }//ns timesource
}//ns bril

#endif
