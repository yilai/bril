#ifndef _bril_timesource_TCDSHWReceiver_h
#define _bril_timesource_TCDSHWReceiver_h
#include <string>
#include <utility>
#include "bril/timesource/tcdsinfo.h"
//#include "toolbox/mem/CountingPtr.h"
#include "uhal/HwInterface.hpp"
#include "log4cplus/logger.h"

namespace bril{
  namespace timesource{
    class TCDSHWReceiver
    {
      
    public:
      TCDSHWReceiver( const std::string& connections, 
		      const std::string& ipbusname, 
		      log4cplus::Logger* logger = 0,
		      int errorinterval = 0, 
		      int errorduration = 16);
      void setup_board();
      uint32_t cdce_locked() const ;
      uint32_t tcds_ready() const;
      tcdsinfo_t read_tcds( int retry_on_bus_error = 10 );
    private:
      //toolbox::mem::CountingPtr<uhal::HwInterface> m_intf;
      std::shared_ptr<uhal::HwInterface> m_intf;
      log4cplus::Logger* m_logger;
      //fake error parameters
      unsigned int m_fakeerrorinterval;
      unsigned int m_fakeerrorduration;
      unsigned int m_error_counter;
      unsigned int m_tcdsread_counter;
    };

  }}

#endif
