// $Id$
#ifndef _bril_timesource_NBTimer_h_
#define _bril_timesource_NBTimer_h_
#include "eventing/api/Member.h"
#include "xdata/ActionListener.h"
#include "xdata/InfoSpace.h"
#include "toolbox/task/TimerListener.h"
#include "xdata/UnsignedInteger32.h"
#include "toolbox/BSem.h"
#include "toolbox/lang/Class.h"
#include "xdaq/Application.h"
#include <string>
#include <set>
namespace toolbox{
  namespace task{
    class Timer;
  }
  namespace mem{
    class MemoryPoolFactory;
  }
}
namespace bril{
  namespace timesource{

    /**
     * nibble frequency timer
     */
    class NBTimer: public eventing::api::Member,public xdata::ActionListener, public toolbox::ActionListener, public toolbox::task::TimerListener{
    public:
      NBTimer(xdaq::Application* app,const std::string& signaltopic, unsigned int nbfrequency, const std::set<std::string>& buses, xdata::InfoSpace* monInfoSpace=0);
      // infospace callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // TimerListener callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      std::string name() const{return m_timername;}
      std::string signaltopic() const{return m_signaltopic;}
      unsigned int nbfrequency() const{return m_nbfrequency;}
      std::string buses() const{return m_busstr;}
    private:      
      void do_publish();
      void do_publish_faketcds();
      // initialize counters
      void initparams();

    private:
      xdaq::Application* m_app; // main app
      std::string m_signaltopic;
      std::string m_timername;
      toolbox::task::Timer* m_timer;
      unsigned int m_currentfill;
      unsigned int m_currentrun;
      unsigned int m_currentnb;
      unsigned int m_currentls;
      unsigned int m_currentcmsls;
      unsigned int m_nbfrequency;
      unsigned int m_nnbs;
      std::set<std::string> m_buses;
      std::string m_busstr;
      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;
      xdata::InfoSpace *m_monInfoSpace; // mon infospace 
    private:
      NBTimer(const NBTimer&);
      NBTimer& operator=(const NBTimer&);
    };

  }}//ns
#endif
