// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include <string>
#include <iostream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"

#include "bril/pltprocessor/Application.h"
#include "bril/pltprocessor/WebUtils.h"
#include "bril/pltprocessor/exception/Exception.h"
#include "interface/bril/PLTTopics.hh"
#include "interface/bril/BEAMTopics.hh"

#include "eventing/api/exception/Exception.h"

#include "xdata/Properties.h"
#include "xdata/TimeVal.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/TimeVal.h"

#include "b2in/nub/Method.h"
//#include "toolbox/regex.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Guard.h"
#include "toolbox/mem/AutoReference.h"

XDAQ_INSTANTIATOR_IMPL (bril::pltprocessor::Application)

using namespace interface::bril;

bril::pltprocessor::Application::Application (xdaq::ApplicationStub* s)
	: xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL)
{
	xgi::framework::deferredbind(this,this,&bril::pltprocessor::Application::Default, "Default");
	b2in::nub::bind(this, &bril::pltprocessor::Application::onMessage);
	m_poolFactory = toolbox::mem::getMemoryPoolFactory();
	m_classname = getApplicationDescriptor()->getClassName();
	m_instanceid = getApplicationDescriptor()->getInstance();
	
	std::string memPoolName = m_classname + m_instanceid.toString() + std::string("_memPool");
	toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
	toolbox::net::URN urn("toolbox-mem-pool",memPoolName);
	m_memPool = m_poolFactory->createPool(urn, allocator);
	std::string monnid("pltprocessorMon");
	std::string monurn = createQualifiedInfoSpace(monnid).toString();
	m_monInfoSpace = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));  
	try{
	  getApplicationInfoSpace()->fireItemAvailable("eventinginput",&m_datasources);
	  getApplicationInfoSpace()->fireItemAvailable("eventingoutput",&m_outputtopics);
	  getApplicationInfoSpace()->fireItemAvailable("calibtag",&m_calibtag);
	  getApplicationInfoSpace()->fireItemAvailable("calibzerotag",&m_calibzerotag);
	  getApplicationInfoSpace()->fireItemAvailable("sigmavis",&m_sigmavis);
	  getApplicationInfoSpace()->fireItemAvailable("accidentalspzero",&m_accidentalspzero);
	  getApplicationInfoSpace()->fireItemAvailable("accidentalspone",&m_accidentalspone);
	  getApplicationInfoSpace()->fireItemAvailable("activechannels", &m_activechannels);
	  getApplicationInfoSpace()->fireItemAvailable("excludechannels", &m_excludechannels);
	  getApplicationInfoSpace()->fireItemAvailable("usemask",&m_usemask);
	  getApplicationInfoSpace()->fireItemAvailable("pltdimensions",&m_pltdimensions);
	  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	  m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_publishing","waiting");

	  init_Flashlist();
	  declare_Flashlist();
	}
	catch(xcept::Exception & e){
	  XCEPT_RETHROW(xdaq::exception::Exception, "Failed to add Listener", e);
	}

	m_currentcha = 0;
	m_prevcha = 0;
	m_currentnib = 0;
	m_prevnib = 0;
	m_currentls = 0;
	m_currentrun = 0;
	m_prevls = 0;
	m_aggchannels = 0;
	m_aggnibbles = 0;

	m_vdmflag = false;

	for (unsigned int icol = 0; icol < NBX; icol++){
	  m_iscolliding1[icol] = false;
	  m_iscolliding2[icol] = false;
	}

	for (int ii = 0; ii < 16; ii++) {
	  m_automasks[ii] = true;
	  m_includechannel[ii] = true;
	}
	m_outtopicdicts.insert( std::make_pair(pltlumiT::topicname(),pltlumiT::payloaddict()) );
	m_outtopicdicts.insert( std::make_pair(pltlumizeroT::topicname(),pltlumizeroT::payloaddict()) );
	m_outtopicdicts.insert( std::make_pair(pltbkgAT::topicname(),pltbkgAT::payloaddict()) );
        m_outtopicdicts.insert( std::make_pair(pltbkgBT::topicname(),pltbkgBT::payloaddict()) );
	for (int i=0; i<16; ++i) {
	  std::string topicname = pltlumizeroT::topicname() + "_" + std::to_string(i);
	  m_outtopicdicts.insert(std::make_pair(topicname, pltlumizeroT::payloaddict()));
	}

	m_monRunStatus.insert(std::make_pair("current nibble", &m_currentnib));
	m_monRunStatus.insert(std::make_pair("current ls", &m_currentls));
	m_monRunStatus.insert(std::make_pair("current run", &m_currentrun));
	m_monRunStatus.insert(std::make_pair("current fill", &m_currentfill));
	
	m_monLumiStatus.insert(std::make_pair("Raw track lumi", &m_avgrawlumi));
	m_monLumiStatus.insert(std::make_pair("Calibrated track lumi", &m_avglumi));
	m_monLumiStatus.insert(std::make_pair("Raw ZC lumi", &m_avgrawzero));
	m_monLumiStatus.insert(std::make_pair("Calibrated ZC lumi", &m_avgzero));
	
	//flashlist monitoring. prepare pltprocessorMon infospace
	memset(m_tot_hit,0,16*sizeof(unsigned int));
	memset(m_tot_hit_zero,0,16*sizeof(unsigned int));
	
 }


bril::pltprocessor::Application::~Application ()
{
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    delete it->second;
  }
  m_topicoutqueues.clear();
}

void bril::pltprocessor::Application::actionPerformed(xdata::Event& e){
  std::stringstream msg;
  if(e.type() == "urn:xdaq-event:setDefaultValues"){
    size_t nsources = m_datasources.elements();
    try{
      for(size_t i=0;i<nsources;++i){
	xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
	xdata::String databus;
	xdata::String topicsStr;
	if(p){
	  databus = p->getProperty("bus");  	 
	  topicsStr = p->getProperty("topics");
	  std::set<std::string> topics = toolbox::parseTokenSet(topicsStr.value_,","); 	  
	  m_in_busTotopics.insert(std::make_pair(databus.value_,topics));
	}
      }
      subscribeall();
      size_t ntopics = m_outputtopics.elements();
      for(size_t i=0;i<ntopics;++i){
	xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_outputtopics.elementAt(i));
	if(p){
	  xdata::String topicname = p->getProperty("topic");
	  xdata::String outputbusStr = p->getProperty("buses");
	  std::set<std::string> outputbuses = toolbox::parseTokenSet(outputbusStr.value_,",");
	  for(std::set<std::string>::iterator it=outputbuses.begin(); it!=outputbuses.end(); ++it){
	    m_out_topicTobuses.insert(std::make_pair(topicname.value_,*it));
	  }
	  m_topicoutqueues.insert(std::make_pair(topicname.value_,new toolbox::squeue<toolbox::mem::Reference*>));
	  m_unreadybuses.insert(outputbuses.begin(),outputbuses.end());
	}
      }

      // Here we insert the per-channel outputs for pltlumizero (if present). This is a bit awkward but it's
      // better than adding 16 new entries to the XML (I hope).
      TopicStoreIt it = m_out_topicTobuses.find(pltlumizeroT::topicname());
      if (it != m_out_topicTobuses.end()) {
	// insert them with the same bus as the "parent" topic
	std::string& parentBus = it->second;
	for (int i=0; i<16; ++i)  {
	  std::string topicname = pltlumizeroT::topicname() + "_" + std::to_string(i);
	  m_topicoutqueues.insert(std::make_pair(topicname, new toolbox::squeue<toolbox::mem::Reference*>));
	  m_out_topicTobuses.insert(std::make_pair(topicname, parentBus));
	}
      }

      for(std::set<std::string>::iterator it=m_unreadybuses.begin(); it!=m_unreadybuses.end(); ++it){
	this->getEventingBus(*it).addActionListener(this);
      }
      // Parse the excluded channels string and read it into the array.
      std::string exclch = m_excludechannels;
      if (exclch.length() > 0) {
	size_t pos = 0; // position within FULL string that we're at so far
	size_t newpos = 0; // position within SUBSTRING that comma was found
	while (exclch[pos] == ',') pos++; // in case we started with commas for some reason

	while (newpos != std::string::npos) {
	  newpos = exclch.substr(pos).find(',');
	  int i = atoi(exclch.substr(pos).c_str());
	  if (i<0 || i>16) {
	    LOG4CPLUS_WARN(getApplicationLogger(), "Bad channel number " << i << " in excluded channel list");
	  } else {
	    LOG4CPLUS_INFO(getApplicationLogger(), "Readout channel " << i << " set to excluded from luminosity calculation");
	    m_includechannel[i] = false;
	  }
	  pos += newpos+1;
	  while (exclch[pos] == ',') pos++; // in case we have multiple commas in a row somehow
	  if (pos == exclch.length()) break; // in case somehow we managed to end the string with a comma
	}
      } // if there were any excluded channels specified to begin with

    }catch(xdata::exception::Exception& e){
      msg<<"Failed to parse application property";
      LOG4CPLUS_ERROR(getApplicationLogger(), msg.str());
      XCEPT_RETHROW(bril::pltprocessor::exception::Exception, msg.str(), e);
    }
  }
}

void bril::pltprocessor::Application::actionPerformed(toolbox::Event& e){
  if(e.type() == "eventing::api::BusReadyToPublish" ){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    std::stringstream msg;
    msg<< "event Bus '" << busname << "' is ready to publish";
    m_unreadybuses.erase(busname);
    if(m_unreadybuses.size()!=0) return; //wait until all buses are ready
    try{    
      toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::pltprocessor::Application::publishing,"publishing");
      m_publishing->activate();
      m_publishing->submit(as_publishing);
    }catch(toolbox::task::exception::Exception& e){
      msg<<"Failed to start publishing workloop "<<stdformat_exception_history(e);
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      XCEPT_RETHROW(bril::pltprocessor::exception::Exception,msg.str(),e);	
    }    
  }
}

void bril::pltprocessor::Application::subscribeall(){
  for(std::map<std::string, std::set<std::string> >::iterator bit=m_in_busTotopics.begin(); bit!=m_in_busTotopics.end(); ++bit ){
    std::string busname = bit->first; 
    for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){       
      LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*topicit); 
      try{
	this->getEventingBus(busname).subscribe(*topicit);
      }catch(eventing::api::exception::Exception& e){
	LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
	m_in_busTotopics[busname].erase(*topicit);
      }
    }
  }
}


/*
 * This is the default web page for this XDAQ application.
 */
void bril::pltprocessor::Application::Default (xgi::Input * in, xgi::Output * out)
{
  *out << busesToHTML();
  
  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::caption("input/output");
  *out << cgicc::tbody();
  
  *out << cgicc::tr();
  *out << cgicc::th("in_topics");
  *out << cgicc::td(plthistT::topicname()+","+plthistT::topicname() );
  *out << cgicc::tr();
  
  *out << cgicc::tr();
  *out << cgicc::th("out_topics");
  *out << cgicc::td( pltagghistT::topicname()+","+ pltagghistT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tbody();  
  *out << cgicc::table();
  *out << cgicc::br();

  *out << WebUtils::mapToHTML("Run Status", m_monRunStatus,"",true);
  *out << WebUtils::mapToHTML("Luminosity", m_monLumiStatus, "", true);
}

/*
 * This is the place where we receive messages.
 */
void bril::pltprocessor::Application::onMessage (toolbox::mem::Reference* ref, xdata::Properties & plist)
{
  // The eventing can send 'control' messages, so we need to check we are receiving a user message.
  std::stringstream msg;
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  toolbox::mem::AutoReference refguard(ref);
  if (action == "notify"&& ref!=0){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
    std::string v = plist.getProperty("DATA_VERSION");
    if(v.empty()){
      msg<<"Received data message without version header, do not process.";
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      return;
    }
    if(v!=interface::bril::shared::DATA_VERSION){
      msg.str("");
      msg<<"Mismatched data version, exiting";
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      return;
    }

    //First handle the topic sent if the number of active channels has changed.
/*
    if(topic == "NActiveChannels"){
      uint32_t NChan = 0;
      uint32_t* ChanLoc = (uint32_t*)ref->getDataLocation();
      memcpy(&NChan, ChanLoc, sizeof(uint32_t));
      //std::cout << "Setting N active channels to " << NChan << std::endl;
      m_activechannels = NChan;
    }
*/

    //    std::cout<<"received topic "<<topic<<std::endl;
    std::vector<std::string> substrings;
    interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
    if( topic == interface::bril::beamT::topicname()){
      std::string payloaddict = plist.getProperty("PAYLOAD_DICT");
      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
      char bstatus[28];
      tc.extract_field( bstatus , "status", inheader->payloadanchor );
      m_beamstatus = std::string(bstatus);
      DoBackground(inheader);
    }
    if (topic == interface::bril::vdmflagT::topicname()) {
      interface::bril::vdmflagT* me = (interface::bril::vdmflagT*)( ref->getDataLocation() );
      m_vdmflag = me->payload()[0];	  
      //std::cout << "vdm flag received and is " << m_vdmflag << std::endl;
    }

    if (topic == plthistT::topicname()){    
      interface::bril::shared::DatumHead* inheader = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
      toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();      
      unsigned int fill = inheader->fillnum;
      unsigned int run = inheader->runnum;
      unsigned int ls = inheader->lsnum;
      unsigned int channelId = inheader->getChannelID();//count from 1
      unsigned int nb = inheader->nbnum;
      msg<<"process on fill "<<fill<<" run "<<run<<" ls " << ls << " nibble "<< nb;
      LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
      m_currentfill = fill;
      m_currentrun = run;
      m_currentls = ls;
      m_currentnib = nb;
      m_prevcha = m_currentcha;
      m_currentcha = channelId;
      msg.str("");
      plthistT* dataptr=(plthistT*)ref->getDataLocation();      
      unsigned int tot = 0;
      //      for(int iiii = 0; iiii<3654; iiii++){
      //	tot+=dataptr->payload()[iiii];}
      //	if(channelId==0)      std::cout<<"accumulating run "<<run<<" ls " << ls << " nibble "<< nb << " channel " << channelId << " total " << tot << std::endl;
      LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
      if(m_currentcha <= m_prevcha){ //on start of nibble.  
	m_aggchannels = 1;
	m_prevnib = m_currentnib;	
	m_aggnibbles++;
      }else{
	m_aggchannels++;
      }
      
      //Reset buffers if current nibble == 1 (new LS)
      //Also do this if current ls == 1 and current nibble == 3.  This is becuase it seems at the start of a new run
      //It can take until up around nibble == 2 to begin receiving TCDS information.
/*      if((m_currentnib == 1 && m_aggchannels==1) || (m_currentls == 1 && m_aggchannels==1)){//on lumi section start, force reset local collector buffer
	//std::cout << "resetting here" << std::endl;
	memset(m_aggdata,0,sizeof(m_aggdata));
        memset(m_aggzero,0,sizeof(m_aggzero));
	memset(m_logzero,0,sizeof(m_logzero));
	memset(m_totdata,0,sizeof(m_totdata));
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Start of LS");
	m_prevls = m_currentls;
	m_currentrun = run;
	m_aggnibbles = 1;
      }
*/      

      for(unsigned int bx=0;bx<NBX;++bx){
	int ncount = dataptr->payload()[bx];
	m_aggdata[channelId][bx] += ncount;//aggregate over nibbles in corresponding channel and bx
	m_aggzero[channelId][bx] += (4096-ncount);
	tot += (4096-ncount);
	m_totdata[channelId] += ncount;
      }

      //if(channelId==1) std::cout<<"accumulating run "<<run<<" ls " << ls << " nibble "<< nb << " channel " << channelId << " tota\l " << tot << " aggnibbles " << m_aggnibbles << " channels " << m_activechannels[0]+m_activechannels[1] << " mask " << m_usemask << std::endl;

      //Automask channels which are extreme outliers.  Currently this is defined
      //as any channel which is off by a factor of 10 from at least 8 other channels.
      //If the PLT channels are all way off from one another then we have a serious 
      //problem anyway.


      if( nb%4 == 0 ){ //any channel on nb4 boundary, publish agghist
	toolbox::mem::Reference* agghistbufRef = 0;
	size_t pltaggsize = pltagghistT::maxsize();
	try{
	  agghistbufRef = m_poolFactory->getFrame(m_memPool,pltaggsize);
	  agghistbufRef->setDataSize(pltaggsize);
	  interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(agghistbufRef->getDataLocation());
	  header->setTime(fill,run, ls, nb, t.sec(), t.millisec());
	  header->setResource(interface::bril::shared::DataSource::PLT,0,channelId,interface::bril::shared::StorageType::UINT32);
	  header->setTotalsize(pltaggsize);
	  header->setFrequency(4);
	  memcpy(header->payloadanchor,m_aggdata[channelId],NBX*sizeof(uint32_t));

	  //flashlist monitoring begin
	  {//beg guard
	    toolbox::task::Guard<toolbox::BSem> g(m_applock);
	    for(size_t idx=0; idx<NBX; ++idx){
	      m_tot_hit[channelId] += m_aggdata[channelId][idx];	    
	    }	  
	  }//end guard
	  //flashlist monitoring end

	  QueueStoreIt it = m_topicoutqueues.find( pltagghistT::topicname() );
	  if(it!=m_topicoutqueues.end()){
	    it->second->push(agghistbufRef); //push agghist to publishing queue
	  }	 	  
	}catch(xcept::Exception& e){
	  std::string msg("Failed to process agghist data");
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	  // in case of error, must release all resources, including the incoming message buffer
	  if(agghistbufRef){
	    agghistbufRef->release();
	    agghistbufRef= 0;
	  }
	}
      
	// Now repeat publication for zero counting as well

	toolbox::mem::Reference* aggzerobufRef = 0;
        size_t pltaggzerosize = pltaggzeroT::maxsize();
        try{
          aggzerobufRef = m_poolFactory->getFrame(m_memPool,pltaggzerosize);
          aggzerobufRef->setDataSize(pltaggzerosize);
	  interface::bril::shared::DatumHead* header = (interface::bril::shared::DatumHead*)(aggzerobufRef->getDataLocation());
          header->setTime(fill,run, ls, nb, t.sec(), t.millisec());
          header->setResource(interface::bril::shared::DataSource::PLT,0,channelId,interface::bril::shared::StorageType::UINT32);
          header->setTotalsize(pltaggzerosize);
          header->setFrequency(4);
	  
	  for(unsigned int izero = 0; izero<NBX; izero++){
              m_logzero[channelId][izero] = -log((float)m_aggzero[channelId][izero] / (4096*m_aggnibbles));
//	if(channelId==1)      std::cout<<"accumulating run "<<run<<" ls " << ls << " nibble "<< nb << " channel " << channelId << " total " << m_logzero[channelId][izero] << std::endl;

	  }
	  
          memcpy(header->payloadanchor,m_aggzero[channelId],NBX*sizeof(uint32_t));

	  //flashlist monitoring begin
	  {//beg guard
	    toolbox::task::Guard<toolbox::BSem> g(m_applock);
	    for(size_t idx=0; idx<NBX; ++idx){
	      m_tot_hit_zero[channelId] += m_aggzero[channelId][idx];	    
	    }
	  }//end guard
	  //flashlist monitoring end

          QueueStoreIt it = m_topicoutqueues.find( pltaggzeroT::topicname() );
          if(it!=m_topicoutqueues.end()){
            it->second->push(aggzerobufRef); //push aggzero to publishing queue                                                          
          }
        }catch(xcept::Exception& e){
	  std::string msg("Failed to process aggzero data");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
          // in case of error, must release all resources, including the incoming message buffer                                         
          if(agghistbufRef){
            aggzerobufRef->release();
            aggzerobufRef= 0;
          }         
	}
      }
//	std::cout << "Aggregated channels" << m_aggchannels << std::endl;
      if( nb%4==0 && m_aggchannels==(m_activechannels[0]+m_activechannels[1])){ //on 4th nibble and collected 16 channels, publish result, reset local collector buffer.                                     
	automask(m_totdata);
	unsigned int maskedchannels = 0;
	for (int imask = 0; imask<16; imask++) {
	  if (!m_automasks[imask] || !m_includechannel[imask])
	    maskedchannels++;
	}
	LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("masking %i channels", maskedchannels));
	//If all the channels are masked then we have a real problem, or everything          
	//is zeroed out.  Either way the automasker is useless and we just ignore it.        
	if (maskedchannels >= (m_activechannels[0]+m_activechannels[1])) {
	  maskedchannels = 0;
	  for(int unmask = 0; unmask<16; unmask++) {
	    m_automasks[unmask] = true;
	    // always exclude the manually excluded channels however
	    if (!m_includechannel[unmask]) maskedchannels++;
	  }
	}

	LOG4CPLUS_DEBUG(getApplicationLogger(), "End of 4th nibble and end of 16 channels");	  
	toolbox::mem::Reference* lumibufRef = 0;
	size_t lumitotsize = pltlumiT::maxsize();
	//	float lumicalib = 11246./m_sigmavis/1E30/4096./m_aggnibbles;
	std::string lumicalibStr(m_calibtag);
	try{	 
	  lumibufRef = m_poolFactory->getFrame( m_memPool,lumitotsize );
	  lumibufRef->setDataSize(lumitotsize);
	  interface::bril::shared::DatumHead* lumiheader = (interface::bril::shared::DatumHead*)(lumibufRef->getDataLocation());
	  lumiheader->setTime(fill,run, ls, nb, t.sec(), t.millisec());
	  lumiheader->setResource(interface::bril::shared::DataSource::PLT,0,0,interface::bril::shared::StorageType::COMPOUND);
	  lumiheader->setTotalsize(lumitotsize);
	  lumiheader->setFrequency(4);
	  memset(m_rawbxlumi,0,sizeof(m_rawbxlumi));
	  for(size_t idx=0;idx<NBX;++idx) { //loop bx
	    if(!m_usemask || (m_iscolliding1[idx] && m_iscolliding2[idx])) {
	      for(int ichannel=0;ichannel<16;++ichannel) {
		if(m_automasks[ichannel] && m_includechannel[ichannel]) {
		  m_rawbxlumi[idx] += (float)m_aggdata[ichannel][idx];
		}
	      } 
	    }
	    m_rawbxlumi[idx] = m_rawbxlumi[idx]/(m_aggchannels-maskedchannels);//take average of collected
	  }

	  float avgraw = 0;
	  float avg = 0;
	  memset(m_bxlumi,0,sizeof(m_bxlumi));	  
          memset(m_bxlumi_chan,0,sizeof(m_bxlumi_chan));
	  for(size_t idx=0;idx<NBX;++idx){
	    for(int ichannel = 0; ichannel < 16; ++ichannel){
	      float lumicalib = 11246./(float)m_sigmavis[ichannel]/1E30/4096./m_aggnibbles;
	      m_bxlumi_chan[ichannel][idx] = m_aggdata[ichannel][idx]*lumicalib/(m_aggchannels-maskedchannels);
	      if(m_automasks[ichannel] && m_includechannel[ichannel]) {
		m_bxlumi[idx] += m_bxlumi_chan[ichannel][idx];//calibrate per bx
	      }
	    }
	    float accidentalrate = (1.0 - m_accidentalspone*m_bxlumi[idx] - m_accidentalspzero);
	    m_bxlumi[idx] = m_bxlumi[idx]*accidentalrate;
	    avgraw += m_rawbxlumi[idx];
	    avg += m_bxlumi[idx]; 
	  }	  
	  //std::cout << "Avg raw " << avgraw*16 << "Avg " << avg*64 << std::endl;
	  interface::bril::shared::CompoundDataStreamer tc(pltlumiT::payloaddict());  
	  tc.insert_field( lumiheader->payloadanchor, "calibtag" , lumicalibStr.c_str() );
	  tc.insert_field( lumiheader->payloadanchor, "avgraw", &avgraw );
	  tc.insert_field( lumiheader->payloadanchor, "avg", &avg );
	  tc.insert_field( lumiheader->payloadanchor, "bxraw", m_rawbxlumi );
	  tc.insert_field( lumiheader->payloadanchor, "bx", m_bxlumi );

	  m_avgrawlumi = avgraw;
	  m_avglumi = avg;

	  QueueStoreIt it = m_topicoutqueues.find(pltlumiT::topicname());
	  if(it!=m_topicoutqueues.end()){
	    it->second->push(lumibufRef);
	  }	  
	  memset(m_aggdata,0,sizeof(m_aggdata));	  
	}catch(xcept::Exception& e){
	  if(lumibufRef){
	    lumibufRef->release();
	    lumibufRef= 0;
	  }
	}

	toolbox::mem::Reference* lumizerobufRef = 0;
	toolbox::mem::Reference* lumizerochbufRef[16] = {0};
        size_t lumizerototsize = pltlumizeroT::maxsize();
	toolbox::mem::Reference* pltbkgAbufRef = 0;
        size_t pltbkgAtotsize = pltbkgAT::maxsize();
	toolbox::mem::Reference* pltbkgBbufRef = 0;
        size_t pltbkgBtotsize = pltbkgBT::maxsize();
        //float lumizerocalib = 11246/m_sigmavis/1E30;
	//	if(m_currentnib == 4 && m_currentls == 1)lumizerocalib*=2;//Same here as above.  First NB4 of a run is actually only an NB2
	std::string lumizerocalibStr(m_calibzerotag);
        try{
          lumizerobufRef = m_poolFactory->getFrame( m_memPool,lumizerototsize );
          lumizerobufRef->setDataSize(lumizerototsize);
	  interface::bril::shared::DatumHead* lumizeroheader = (interface::bril::shared::DatumHead*)(lumizerobufRef->getDataLocation());
          lumizeroheader->setTime(fill,run, ls, nb, t.sec(), t.millisec());
          lumizeroheader->setResource(interface::bril::shared::DataSource::PLT,0,0,interface::bril::shared::StorageType::COMPOUND);
          lumizeroheader->setTotalsize(lumizerototsize);
          lumizeroheader->setFrequency(4);

	  pltbkgAbufRef = m_poolFactory->getFrame( m_memPool,pltbkgAtotsize );
	  pltbkgBbufRef = m_poolFactory->getFrame( m_memPool,pltbkgBtotsize );

	  pltbkgAbufRef->setDataSize(pltbkgAtotsize);
	  pltbkgBbufRef->setDataSize(pltbkgBtotsize);

	  interface::bril::shared::DatumHead* pltbkgAheader = (interface::bril::shared::DatumHead*)(pltbkgAbufRef->getDataLocation());
	  interface::bril::shared::DatumHead* pltbkgBheader = (interface::bril::shared::DatumHead*)(pltbkgBbufRef->getDataLocation());

	  pltbkgAheader->setTime(fill,run, ls, nb, t.sec(), t.millisec());
          pltbkgAheader->setResource(interface::bril::shared::DataSource::PLT,0,0,interface::bril::shared::StorageType::COMPOUND);
          pltbkgAheader->setTotalsize(pltbkgAtotsize);
          pltbkgAheader->setFrequency(4);
	  pltbkgBheader->setTime(fill,run, ls, nb, t.sec(), t.millisec());
          pltbkgBheader->setResource(interface::bril::shared::DataSource::PLT,0,0,interface::bril::shared::StorageType::COMPOUND);
          pltbkgBheader->setTotalsize(pltbkgBtotsize);
          pltbkgBheader->setFrequency(4);

          memset(m_rawbxzero,0,sizeof(m_rawbxzero));
	  memset(m_chan_rawlumizero,0,sizeof(m_chan_rawlumizero));
          for(size_t idx=0;idx<NBX;++idx) { //loop bx
	    if(!m_usemask || (m_iscolliding1[idx] && m_iscolliding2[idx])) {
	      for(int ichannel=0;ichannel<16;++ichannel) {
		if(m_automasks[ichannel] && m_includechannel[ichannel]) {
		  m_rawbxzero[idx] += (float)m_logzero[ichannel][idx];
		}
		m_chan_rawlumizero[ichannel] += (float)m_logzero[ichannel][idx];
	      }
            }
            m_rawbxzero[idx] = m_rawbxzero[idx]/(m_aggchannels-maskedchannels);//take average of 16 channels. need to treat agg=0 here?
          }

	  float avgrawzero = 0;
          float avgzero = 0;
          memset(m_bxzero,0,sizeof(m_bxzero));
          memset(m_bxzero_chan,0,sizeof(m_bxzero_chan));
	  memset(m_chan_lumizero,0,sizeof(m_chan_lumizero));
	  memset(m_chan_bxzero,0,sizeof(m_chan_bxzero));

	  float m_pltbkgA1 = 0;
	  float	m_pltbkgB1 = 0;
          float	m_pltbkgA2 = 0;
          float	m_pltbkgB2 = 0;
	  float n_A1 = 0;
	  float n_A2 = 0;	
	  float n_B1 = 0;
	  float n_B2 = 0;
	  for(size_t idx=0;idx<NBX;++idx){
	    for(int ichannel = 0; ichannel < 16; ichannel++){
	      float lumicalibzero = 11246./m_sigmavis[ichannel]/1E30;
              m_bxzero_chan[ichannel][idx] = m_logzero[ichannel][idx]*lumicalibzero/(m_aggchannels-maskedchannels);
	      m_chan_bxzero[ichannel][idx] = m_logzero[ichannel][idx]*lumicalibzero;
	      if(m_automasks[ichannel] && m_includechannel[ichannel]) {
		m_bxzero[idx] += m_bxzero_chan[ichannel][idx];//calibrate per bx
	      }
	      m_chan_lumizero[ichannel] += m_logzero[ichannel][idx]*lumicalibzero;
	      float _ibeam1 = (m_bx1[idx]/1E11);
	      float _ibeam2 = (m_bx2[idx]/1E11);
	      if(_ibeam1==0)_ibeam1=1.;
	      if(_ibeam2==0)_ibeam2=1.;
	      float pltarea = m_pltdimensions[0]*m_pltdimensions[1];
	      float bkgcalib1 = 11246*(pltarea)/_ibeam1;
	      float bkgcalib2 = 11246*(pltarea)/_ibeam2;
	      if(ichannel < 8){
		if(m_iscolliding1[idx] && !m_iscolliding2[idx]){
		  m_pltbkgA1+=m_logzero[ichannel][idx]*bkgcalib1;
		  n_A1++;
		}
		else if(idx<3563 && !m_iscolliding1[idx] && m_iscolliding2[idx+1]){
		  m_pltbkgB1+=m_logzero[ichannel][idx]*bkgcalib1;
		  n_B1++;
		}
	      }
	      else if(ichannel >=8){
		if(m_iscolliding2[idx] && !m_iscolliding1[idx]){
                  m_pltbkgA2+=m_logzero[ichannel][idx]*bkgcalib2;
		  n_A2++;
		}
		else if(idx<3563 && !m_iscolliding2[idx] && m_iscolliding1[idx+1]){
                  m_pltbkgB2+=m_logzero[ichannel][idx]*bkgcalib2;
		  n_B2++;
		}
	      }
	    }
	    float accidentalrate = (1.0 - m_accidentalspone*m_bxzero[idx] - m_accidentalspzero);
            m_bxzero[idx] = m_bxzero[idx]*accidentalrate;
	    avgrawzero += m_rawbxzero[idx];
            avgzero += m_bxzero[idx];
	    
          }
	   if(n_A1>0)m_pltbkgA1 = (8/m_activechannels[0])*m_pltbkgA1/n_A1;
           if(n_A2>0)m_pltbkgA2 = (8/m_activechannels[1])*m_pltbkgA2/n_A2;
           if(n_B1>0)m_pltbkgB1 = (8/m_activechannels[0])*m_pltbkgB1/n_B1;
           if(n_B2>0)m_pltbkgB2 = (8/m_activechannels[1])*m_pltbkgB2/n_B2;

	  //std::cout<<"accumulating run "<<run<<" ls " << ls << " nibble "<< nb << " channel " << channelId <<	            "Avg raw " << avgrawzero << "Avg " << avgzero << std::endl;                                                       
	  //std::cout << m_activechannels[0] << " " << m_activechannels[1] << " " << n_A1 << " " << n_A2 << " " << n_B1 << " " << n_B2 << std::endl;
	  //std::cout << "publishing plt background with quantities " << m_pltbkgA1 << " " << m_pltbkgA2 << " " << m_pltbkgB1 << " " << m_pltbkgB2 << std::endl;

	   interface::bril::shared::CompoundDataStreamer tc(pltlumizeroT::payloaddict());
          tc.insert_field( lumizeroheader->payloadanchor, "calibtag" , lumizerocalibStr.c_str() );
          tc.insert_field( lumizeroheader->payloadanchor, "avgraw", &avgrawzero );
          tc.insert_field( lumizeroheader->payloadanchor, "avg", &avgzero );
          tc.insert_field( lumizeroheader->payloadanchor, "bxraw", m_rawbxzero );
          tc.insert_field( lumizeroheader->payloadanchor, "bx", m_bxzero );

	  m_avgrawzero = avgrawzero;
	  m_avgzero = avgzero;

          QueueStoreIt it = m_topicoutqueues.find(pltlumizeroT::topicname());
          if(it!=m_topicoutqueues.end()){
            it->second->push(lumizerobufRef);
          }

	  // Publish per-channel data if and only if we're in VdM scan mode.
	  if (m_vdmflag) {
	    for (int ich=0; ich<16; ++ich) {
	      std::string topicname = pltlumizeroT::topicname() + "_" + std::to_string(ich);
	      try {
		lumizerochbufRef[ich] = m_poolFactory->getFrame( m_memPool,lumizerototsize );
		lumizerochbufRef[ich]->setDataSize(lumizerototsize);
		interface::bril::shared::DatumHead* lumizerochheader = (interface::bril::shared::DatumHead*)(lumizerochbufRef[ich]->getDataLocation());
		lumizerochheader->setTime(fill,run, ls, nb, t.sec(), t.millisec());
		lumizerochheader->setResource(interface::bril::shared::DataSource::PLT,0,0,interface::bril::shared::StorageType::COMPOUND);
		lumizerochheader->setTotalsize(lumizerototsize);
		lumizerochheader->setFrequency(4);
		
		interface::bril::shared::CompoundDataStreamer tci(pltlumizeroT::payloaddict());
		tc.insert_field( lumizerochheader->payloadanchor, "calibtag" , lumizerocalibStr.c_str() );
		tc.insert_field( lumizerochheader->payloadanchor, "avgraw", &(m_chan_rawlumizero[ich]) );
		tc.insert_field( lumizerochheader->payloadanchor, "avg", &(m_chan_lumizero[ich]) );
		tc.insert_field( lumizerochheader->payloadanchor, "bxraw", m_logzero[ich] );
		tc.insert_field( lumizerochheader->payloadanchor, "bx", m_chan_bxzero[ich] );

		QueueStoreIt iti = m_topicoutqueues.find(topicname);
		if (iti != m_topicoutqueues.end()){
		  iti->second->push(lumizerochbufRef[ich]);
		}
	      } catch (xcept::Exception& e) {
		if(lumizerochbufRef[ich]){
		  lumizerochbufRef[ich]->release();
		  lumizerochbufRef[ich]= 0;
		}
	      } // try/catch
	    } // channel loop
	  } // if publishing for VdM

	  interface::bril::shared::CompoundDataStreamer tba(pltbkgAT::payloaddict());
	  tba.insert_field(pltbkgAheader->payloadanchor, "beam1bkg", &m_pltbkgA1);
	  tba.insert_field(pltbkgAheader->payloadanchor, "beam2bkg", &m_pltbkgA2);

	  interface::bril::shared::CompoundDataStreamer tbb(pltbkgBT::payloaddict());
          tba.insert_field(pltbkgBheader->payloadanchor, "beam1bkg", &m_pltbkgB1);
          tba.insert_field(pltbkgBheader->payloadanchor, "beam2bkg", &m_pltbkgB2);

	  QueueStoreIt it2 = m_topicoutqueues.find(pltbkgAT::topicname());
          if(it2!=m_topicoutqueues.end()){
            it2->second->push(pltbkgAbufRef);
          }
	  QueueStoreIt it3 = m_topicoutqueues.find(pltbkgBT::topicname());
          if(it3!=m_topicoutqueues.end()){
            it3->second->push(pltbkgBbufRef);
          }

          memset(m_logzero,0,sizeof(m_logzero));
          memset(m_aggzero,0,sizeof(m_aggzero));
	  memset(m_totdata,0,sizeof(m_totdata));
	  m_aggnibbles = 0; //Published everything, reset # of nibbles.
	}catch(xcept::Exception& e){
          if(lumizerobufRef){
            lumizerobufRef->release();
            lumizerobufRef= 0;
          }
        }
      }
    }
  }
}

void bril::pltprocessor::Application::do_publish(const std::string& busname,const std::string& topicname,toolbox::mem::Reference* bufRef){
  LOG4CPLUS_DEBUG(getApplicationLogger(),"do_publish "+topicname+" to "+busname);
  std::stringstream msg;
  //flashlist monitoring begin
  //fill and fire flashlist only together with lumi topics.
  
  if( topicname=="pltlumi"|| topicname=="pltlumizero" ){
    xdata::UnsignedInteger32* p_detid = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("detid") );
    if( topicname=="pltlumi" ){
      p_detid->value_ = 1;
    }else if ( topicname=="pltlumizero" ){
      p_detid->value_ = 0;
    };
    interface::bril::shared::DatumHead* d = (interface::bril::shared::DatumHead*)(bufRef->getDataLocation());
    xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("fill") );
    p_fillnum->value_ = d->fillnum;
    xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("run") );
    p_runnum->value_ = d->runnum;
    xdata::UnsignedInteger32* p_lsnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("ls") );
    p_lsnum->value_ = d->lsnum;
    xdata::UnsignedInteger32* p_nbnum = dynamic_cast< xdata::UnsignedInteger32* >( m_monInfoSpace->find("nb") );
    p_nbnum->value_ = d->nbnum;
    xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( m_monInfoSpace->find("timestamp") );
    p_timestamp->value_.sec( d->timestampsec ) ;
    p_timestamp->value_.usec( d->timestampmsec*1000 ) ;
    
    xdata::Vector<xdata::UnsignedInteger>* p_tot_hit_pertelescope = dynamic_cast< xdata::Vector< xdata::UnsignedInteger>* >( m_monInfoSpace->find("tot_hit_pertelescope") );
    {//beg guard
      toolbox::task::Guard<toolbox::BSem> g(m_applock);
      for(size_t cid=0;cid<16;++cid){
	xdata::UnsignedInteger v;	
	if( topicname=="pltlumi" ){	
	  v = m_tot_hit[cid];
	}else if( topicname=="pltlumizero" ){
	  v = m_tot_hit_zero[cid];
	}
	p_tot_hit_pertelescope->push_back(v);
      }
      LOG4CPLUS_INFO(getApplicationLogger(),"Firing plt flashlist for detid "+p_detid->toString() );
      m_monInfoSpace->fireItemGroupChanged( m_flashfields, this );  
      p_tot_hit_pertelescope->clear();
      if( topicname=="pltlumi" ){
	memset(m_tot_hit,0,16*sizeof(unsigned int));	
      }else if( topicname=="pltlumizero" ){
	memset(m_tot_hit_zero,0,16*sizeof(unsigned int));
      }
    }//end guard
  }  //flashlist monitoring end
  
  try{
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::shared::DATA_VERSION);
    if( m_beamstatus!="STABLE BEAMS" &&m_beamstatus!="SQUEEZE"&&m_beamstatus!="FLAT TOP"&&m_beamstatus!="ADJUST"){
      plist.setProperty("NOSTORE","1");
    }
    std::map<std::string,std::string>::iterator it=m_outtopicdicts.find(topicname);
    if( it!=m_outtopicdicts.end() ){
      plist.setProperty("PAYLOAD_DICT",it->second);
    }
    msg << "publish to "<<busname<<" , "<<topicname; 
    this->getEventingBus(busname).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<busname;    
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
    XCEPT_DECLARE_NESTED(bril::pltprocessor::exception::Exception,myerrorobj,msg.str(),e);
    this->notifyQualified("fatal",myerrorobj);
  }  
}
 

bool bril::pltprocessor::Application::publishing(toolbox::task::WorkLoop* wl){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    if(it->second->empty()) continue;
    std::string topicname = it->first;
    LOG4CPLUS_INFO(getApplicationLogger(),"Publishing "+topicname);
    toolbox::mem::Reference* data = it->second->pop();
    std::pair<TopicStoreIt,TopicStoreIt > ret = m_out_topicTobuses.equal_range(topicname);
    for(TopicStoreIt topicit = ret.first; topicit!=ret.second;++topicit){
      if(data) do_publish(topicit->second,topicname,data->duplicate());    
    }
    if(data) data->release();
  }
  usleep(5000);
  return true;
}


void bril::pltprocessor::Application::automask(float totdata[16]){
  for(int ii = 0; ii < 16; ii++) {
      m_automasks[ii] = true;
  }
  for(int ichan = 0; ichan < 16; ichan++){
//	    std::cout << "tot data " << ichan << " " << totdata[ichan] << std::endl;

    if(totdata[ichan] == 0) m_automasks[ichan] = false;
    else{
      int outly = 0;
      for(int jchan = 0; jchan < 16; jchan++){
	float ratio = totdata[ichan]/totdata[jchan];
	if((ratio > 10. || ratio < 0.1)) outly++;
      }
      if(outly > 8) m_automasks[ichan] = false;
    }
  }

}

void bril::pltprocessor::Application::DoBackground(interface::bril::shared::DatumHead* inheader){
  float TotBeamI1;
  float TotBeamI2;

  char _machinemode[50];
  char _beammode[50];

  interface::bril::shared::CompoundDataStreamer bt(interface::bril::beamT::payloaddict());
  bt.extract_field(&TotBeamI1, "intensity1", inheader->payloadanchor);
  bt.extract_field(&TotBeamI2, "intensity2", inheader->payloadanchor);
  bt.extract_field(m_bx1, "bxintensity1", inheader->payloadanchor);
  bt.extract_field(m_bx2, "bxintensity2", inheader->payloadanchor);
  bt.extract_field(m_iscolliding1, "bxconfig1", inheader->payloadanchor);
  bt.extract_field(m_iscolliding2, "bxconfig2", inheader->payloadanchor);
  bt.extract_field(_machinemode, "machinemode", inheader->payloadanchor);
  bt.extract_field(_beammode, "status", inheader->payloadanchor);

  //  std::cout << " Beam mode" << _beammode << std::endl;
}

void bril::pltprocessor::Application::init_Flashlist(){
  std::string fieldname;
  fieldname = "fill";
  m_flashfields.push_back(fieldname);
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "run";
  m_flashfields.push_back(fieldname);
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "ls";
  m_flashfields.push_back("ls");
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "nb";
  m_flashfields.push_back("nb");
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "timestamp";
  m_flashfields.push_back("timestamp");
  m_flashcontents.push_back( new xdata::TimeVal(toolbox::TimeVal()) );

  fieldname = "detid";
  m_flashfields.push_back("detid");
  m_flashcontents.push_back( new xdata::UnsignedInteger32(0) );

  fieldname = "tot_hit_pertelescope";
  m_flashfields.push_back("tot_hit_pertelescope");
  m_flashcontents.push_back( new xdata::Vector< xdata::UnsignedInteger > );

}

void bril::pltprocessor::Application::declare_Flashlist(){
  std::list<std::string>::const_iterator it = m_flashfields.begin();
  std::vector<xdata::Serializable*>::const_iterator valit = m_flashcontents.begin();
  for(; it!=m_flashfields.end() ; ++it, ++valit ){
    m_monInfoSpace->fireItemAvailable(*it,*valit);
  }       
}


