#include <initializer_list>
#include <iterator>
#include <unistd.h>

#include "b2in/nub/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Guard.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "xgi/framework/Method.h"
#include "xgi/framework/UIManager.h"

#include "bril/bhmprocessor/Application.h"
#include "bril/bhmprocessor/Algorithms.h"
#include "bril/bhmprocessor/exception/Exception.h"

#include "bril/webutils/WebUtils.h"

XDAQ_INSTANTIATOR_IMPL( bril::bhmprocessor::Application )

namespace
 {

  typedef toolbox::task::Guard<toolbox::BSem> SafeLocker;

  struct RefDestroyer  ////RAII exception safe guard object
   {
    toolbox::mem::Reference* m_ref;
    RefDestroyer( toolbox::mem::Reference* ref ) : m_ref( ref ){}
    ~RefDestroyer(){ if ( m_ref ) m_ref->release(); }
   };

 }

namespace bril
 {

  using namespace webutils;

  namespace bhmprocessor
   {

/**
 * Creates the Channel cache, by zeroing histograms for the good channels.
 * New and existing channels in the cache will be (created and) zeroed.
 * Existing channels that are not good anymore will be removed.
 *
 * @param good The list of valid channels to be used.
 */
    template <typename H, int Count>
    void Application::Cache<H, Count>::clear( const ChannelIdVector& good )
     {
      for ( auto it = m_cache.begin(); it != m_cache.end(); ++it )
        if ( std::find( good.begin(), good.end(), it->first ) == good.end() ) //not good anymore
          m_cache.erase( it );
      for ( auto gch = good.begin(); gch != good.end(); ++ gch )
        m_cache[*gch].clear();
     }

/**
 * Adds a histogram to the corresponding channel
 *
 * @param incoming histogram to be added
 * @param subbin TDC bin
 */
    template <typename H, int Count>
    template <typename T>
    bool Application::Cache<H, Count>::add( const interface::bril::shared::Datum<T>& incoming, unsigned int subbin )
     {
      bhm::ChannelId ch( incoming.getChannelID() );
      auto it = m_cache.find( ch );
      if ( it == m_cache.end() || subbin >= Count )
        return false;
      it->second.m_histogram[subbin] += incoming;
      it->second.m_nibbles[subbin] += incoming.publishnnb;
      it->second.m_quality[subbin] |= incoming.algoid;
      m_age = incoming.timestampsec;
      return true;
     }

/**
 * Checks that the cache has sufficient nibbles stored.
 *
 * @param nnb the required number of nibbles
 */
    template <typename H, int Count>
    bool Application::Cache<H, Count>::has_nibbles( unsigned int nnb )
     {
      for ( auto it = m_cache.begin(); it != m_cache.end(); ++it )
        for ( int n = 0; n != Count; ++n )
          if ( it->second.m_nibbles[n] < nnb )
            return false;
      return true;
     }

/**
 * @brief Constructor
 *
 * The constructor registers all the callbacks, initializes data structures and monitoring parameters.
 */
    Application::Application( xdaq::ApplicationStub* s ):
     xdaq::Application( s ),
     xgi::framework::UIManager( this ),
     eventing::api::Member( this ),
     m_applock( toolbox::BSem::FULL ),
     m_occupancy_sel( false ),
     m_amplitude_sel( false ),
     m_aggregate_algo( 0 ),
     m_amplitude_algo( 0 ),
     m_background_algo( 0 ),
     m_occupancy_chart_main( 0 ),
     m_timeseries_chart_main( 0 ),
     m_occupancy_chart_sbx1( 0 ),
     m_occupancy_chart_sbx2( 0 ),
     m_amplitude_chart_pn( 0 ),
     m_amplitude_chart_pf( 0 ),
     m_amplitude_chart_mn( 0 ),
     m_amplitude_chart_mf( 0 )
     {
      //prepare internal variables
      m_topic_functions[interface::bril::bhmamphistT::topicname()] = &Application::onMessageTopicAmplitude;
      m_topic_functions[interface::bril::beamT::topicname()] = &Application::onMessageTopicBeam;
      m_topic_functions[interface::bril::bhmocchistT::topicname()] = &Application::onMessageTopicOccupancy;

      try
       {
        std::string classname = getApplicationDescriptor()->getClassName();
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " initializing application: " << classname );
        toolbox::net::URN memurn( "toolbox-mem-pool", classname + "_mem" );
        m_pool_factory = toolbox::mem::getMemoryPoolFactory();
        m_mem_pool = m_pool_factory->createPool( memurn, new toolbox::mem::HeapAllocator() );
        setupConfiguration();
        setupMonitoring();
        //register callbacks
        xgi::framework::deferredbind( this, this, &Application::Default, "Default" ); //framework -> has standard page frame and stuff
        xgi::framework::deferredbind( this, this, &Application::expertPage, "expert" ); //framework -> has standard page frame and stuff
        xgi::deferredbind( this, this, &Application::requestData, "requestData" ); //naked data
        b2in::nub::bind( this, &Application::onMessage );
       }
      catch( xcept::Exception& e )
       {
        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Caught Exception\n" << stdformat_exception_history( e ) );
        notifyQualified( "error", e );
        throw; //Errors in the constructor are not recoverable
       }

     }

    Application::~Application()
     {
      m_waitcondition.signal();
      delete m_publish_wl;
     }

    void Application::setupAlgorithms()
     {
      delete m_aggregate_algo;
      delete m_amplitude_algo;
      delete m_background_algo;
      m_aggregate_algo = AlgorithmFactory::aggregate( m_aggregate_algo_prop );
      m_background_algo = AlgorithmFactory::background( m_background_algo_prop );
      m_amplitude_algo = AlgorithmFactory::amplitude( m_amplitude_algo_prop );
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Enabling " << m_aggregate_algo->desired_channels().size() << " channels for occupancy" );
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Enabling " << m_amplitude_algo->desired_channels().size() << " channels for amplitude" );
      frontOccupancy().clear( m_aggregate_algo->desired_channels() );
      frontAmplitude().clear( m_amplitude_algo->desired_channels() );
      backOccupancy().clear( m_aggregate_algo->desired_channels() );
      backAmplitude().clear( m_amplitude_algo->desired_channels() );
     }

    void Application::setupCharts()
     {
      std::string cb = getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestData";
      //The two charts for the main page, with BKGD4,5 vs BX and vs time
      WebChart::property_map smapocc;
      smapocc["Beam 1 (BKGD4)"] = WebChart::series_properties( "#4040FF" );
      smapocc["Beam 2 (BKGD5)"] = WebChart::series_properties( "#FF4040" );
      m_occupancy_chart_main = new WebChart( "bkgdbx", cb, "type: 'column', zoomType: 'xy', animation: false",
       "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.05, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
       "title: { text: 'Background numbers vs BX' }, tooltip: { enable : false },"
       "yAxis: { min: 0, title: { text: 'Hz/cm<sup>2</sup>/10<sup>11</sup>', useHTML: true } }", smapocc );
      m_timeseries_chart_main = new WebChart( "bkgdt", cb, "type: 'line', zoomType: 'xy', animation: false",
       "plotOptions: { line: { animation: false } }, title: { text: 'Background Numbers vs time' },"
       "yAxis: { min: 0, title: { text: 'Hz/cm<sup>2</sup>/10<sup>11</sup>', useHTML: true } }, xAxis: { type: 'datetime' }", smapocc );
      m_rate_history["Beam 1 (BKGD4)"] = std::deque<std::pair<unsigned long, float>>();
      m_rate_history["Beam 2 (BKGD5)"] = std::deque<std::pair<unsigned long, float>>();
      //The charts for the expert page: occupancy vs sbx
      WebChart::property_map smapsbx;
      smapsbx["TDC Bin 1"] = WebChart::series_properties( "#33cccc" );
      smapsbx["TDC Bin 2"] = WebChart::series_properties( "#ffcc00" );
      smapsbx["TDC Bin 3"] = WebChart::series_properties( "#ff00ff" );
      smapsbx["TDC Bin 4"] = WebChart::series_properties( "#333333" );
      m_occupancy_chart_sbx1 = new WebChart( "sbx1", cb, "type: 'column', zoomType: 'xy', animation: false",
       "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
       "title: { text: 'Beam 1' }, tooltip: { enable : false }", smapsbx );
      m_occupancy_chart_sbx2 = new WebChart( "sbx2", cb, "type: 'column', zoomType: 'xy', animation: false",
       "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
       "title: { text: 'Beam 2' }, tooltip: { enable : false }", smapsbx );
      //Amplitude
      const char* colors[10] = { "#E01010", "#E07010", "#E0B010", "#B0B010", "#70B010", "#70B070", "#10B070", "#10B0B0", "#1070E0", "#1010E0" };
      WebChart::property_map smapamp;
      for ( int i = 0; i != 10; ++i )
        smapamp[to_string( i + 1 )] = WebChart::series_properties( colors[i] );
      WebChart::type_map tmapamp;
      tmapamp["TDC bin 1"] = "0";
      tmapamp["TDC bin 2"] = "1";
      tmapamp["TDC bin 3"] = "2";
      tmapamp["TDC bin 4"] = "3";
      tmapamp["All TDC bins"] = "4";
      std::string opts = "type: 'line', zoomType: 'xy', animation: false";
      std::string extraopts = "plotOptions: { line: { animation: false } }, title: { text: '";
      m_amplitude_chart_pn = new WebChart( "amp_pn", cb, opts, extraopts + "Plus Near' }", smapamp );
      m_amplitude_chart_pf = new WebChart( "amp_pf", cb, opts, extraopts + "Plus Far' }", smapamp );
      m_amplitude_chart_mn = new WebChart( "amp_mn", cb, opts, extraopts + "Minus Near' }", smapamp );
      m_amplitude_chart_mf = new WebChart( "amp_mf", cb, opts, extraopts + "Minus Far' }", smapamp );
      m_amplitude_chart_pn->setTypes( tmapamp );
      m_amplitude_chart_pf->setTypes( tmapamp );
      m_amplitude_chart_mn->setTypes( tmapamp );
      m_amplitude_chart_mf->setTypes( tmapamp );
     }

    void Application::setupConfiguration()
     {
      auto infospace = getApplicationInfoSpace();
      infospace->fireItemAvailable( "eventinginput", &m_datasources );
      infospace->fireItemAvailable( "eventingoutput", &m_outputtopics );
      infospace->fireItemAvailable( "aggregatealgo", &m_aggregate_algo_prop );
      infospace->fireItemAvailable( "backgroundalgo", &m_background_algo_prop );
      infospace->fireItemAvailable( "amplitudealgo", &m_amplitude_algo_prop );
      infospace->fireItemAvailable( "reqnboccupancy", &m_required_nibbles_occupancy );
      infospace->fireItemAvailable( "reqnbamplitude", &m_required_nibbles_amplitude );
      infospace->fireItemAvailable( "montimerperiod", &m_mon_timer_period_sec );
      infospace->addListener( this, "urn:xdaq-event:setDefaultValues" );
      infospace->addItemChangedListener( "aggregatealgo", this );
      infospace->addItemChangedListener( "amplitudealgo", this );
      infospace->addItemChangedListener( "backgroundalgo", this );
      infospace->addItemChangedListener( "reqnboccupancy", this );
      infospace->addItemChangedListener( "reqnbamplitude", this );
     }

    void Application::setupEventing()
     {
      for ( size_t i = 0; i != m_datasources.size(); ++i )
       {
        std::string databus = m_datasources[i].getProperty( "bus" );
        std::string topics = m_datasources[i].getProperty( "topics" );
        auto topicset = toolbox::parseTokenSet( topics, "," );
        for ( auto it = topicset.begin(); it != topicset.end(); ++it )
         {
          getEventingBus( databus ).subscribe( *it );
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Subscribing to " << databus << "/" << *it );
         }
       }
      m_output_topics_map.clear();
      m_unready_buses.clear();
      for ( size_t i = 0; i != m_outputtopics.size(); ++i )
       {
        std::string topic = m_outputtopics[i].getProperty( "topic" );
        std::string buses = m_outputtopics[i].getProperty( "buses" );
        BusNameSet set = toolbox::parseTokenSet( buses, "," );
        m_output_topics_map[topic] = set;
        m_unready_buses.insert( set.begin(), set.end() );
       }
      for ( auto it = m_unready_buses.begin(); it != m_unready_buses.end(); ++it )
       {
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Waiting for bus \"" << *it << "\" to be ready." );
        getEventingBus( *it ).addActionListener( this );
       }
     }

    void Application::setupMonitoring()
     {
      m_mon_bxrate_b1.resize( interface::bril::shared::MAX_NBX );
      m_mon_bxrate_b2.resize( interface::bril::shared::MAX_NBX );
      std::string monurn = createQualifiedInfoSpace( "bhmprocessorMon" ).toString();
      m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
      const int nvars = 9;
      const char* names[nvars] = { "fill", "run", "ls", "nb", "timestamp", "totalrate_b1", "totalrate_b2", "bxrate_b1", "bxrate_b2" };
      xdata::Serializable* vars[nvars] = { &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_mon_totalrate_b1, &m_mon_totalrate_b2, &m_mon_bxrate_b1, &m_mon_bxrate_b2 };
      for ( int i = 0; i != nvars; ++i )
       {
        m_mon_varlist.push_back( names[i] );
        m_mon_vars.push_back( vars[i] );
        m_mon_infospace->fireItemAvailable( names[i], vars[i] );
       }
     }

    void Application::setupWorkloop()
     {
      m_publish_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( getApplicationDescriptor()->getURN() + "_publish", "waiting" );
      toolbox::task::ActionSignature* as_publishing = toolbox::task::bind( this, &Application::publishMain, "publish" );
      m_publish_wl->activate();
      m_publish_wl->submit( as_publishing );
     }

    void Application::Default( xgi::Input* in, xgi::Output* out )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ );
      using namespace cgicc;
      using std::endl;
      std::string configurl = "/urn:xdaq-application:service=hyperdaq/editProperties?lid=" + to_string( getApplicationDescriptor()->getLocalId() );
      std::string experturl = "/" + getApplicationDescriptor()->getURN() + "/expert";
      *out << "<script src=\"/bril/bhmprocessor/html/highcharts.js\"></script>";
      *out << h1() << "BHM Processor" << h1() << endl;
      *out << "<div style=\"float:left; width:100%;\">" << endl;
      *out << cgicc::div().set( "style", "float:left; width: 50%;" ) << endl;
        *out << h3() << "Eventing state" << h3() << endl;
        *out << busesToHTML() << br() << endl;
      *out << cgicc::div() << endl;
      *out << cgicc::div().set( "style", "margin-left: 50%;" ) << endl;
        *out << h3() << "Algorithm configuration - " << a( "CHANGE" ).set( "href", configurl ) << h3() << br() << endl;
        auto titles = { "" };
        auto algotypes = { "Aggregate Algorithm", "Background Algorithm", "Amplitude Algorithm" };
        auto algos = { m_aggregate_algo_prop.getProperty( "class" ), m_background_algo_prop.getProperty( "class" ), m_amplitude_algo_prop.getProperty( "class" ) };
        cgiVerticalTable( out, titles, algotypes, algos );
        *out << h4() << a( "Expert Page" ).set( "href", experturl ) << h4() << endl;
      *out << cgicc::div() << endl;
      *out << "</div>" << endl;
      *out << br() << hr() << endl;
      m_occupancy_chart_main->writeChart( out );
      m_timeseries_chart_main->writeChart( out );
     }

    void Application::expertPage( xgi::Input* in, xgi::Output* out )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ );
      using namespace cgicc;
      using std::endl;
      *out << "<script src=\"/bril/bhmprocessor/html/highcharts.js\"></script>";
      m_occupancy_chart_sbx1->writeChart( out );
      m_occupancy_chart_sbx2->writeChart( out );
      *out << cgicc::div().set( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
      *out << "<div style = \"float:left; width:50%;\">";
      m_amplitude_chart_pn->writeChart( out );
      *out << "</div> <div style = \"margin-left:50%;\">";
      m_amplitude_chart_pf->writeChart( out );
      *out << "</div>";
      *out << cgicc::div().set( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
      *out << "<div style = \"float:left; width:50%;\">";
      m_amplitude_chart_mn->writeChart( out );
      *out << "</div> <div style = \"margin-left:50%;\">";
      m_amplitude_chart_mf->writeChart( out );
      *out << "</div>";
     }

    void Application::requestData( xgi::Input* in, xgi::Output* out )
     {
      using namespace cgicc;
      try
       {
        SafeLocker locker( m_applock );
        WebChart::dataHeader( out );
        Cgicc cgi( in );
        auto bxmap = make_range_map( "Beam 1 (BKGD4)", m_bkgd_plus.m_perbx, "Beam 2 (BKGD5)", m_bkgd_minus.m_perbx );
        m_occupancy_chart_main->writeDataForQuery( cgi, out, bxmap );
        m_timeseries_chart_main->writeDataForQuery( cgi, out, m_rate_history );
        //I will treat the "expert" page charts differently: normally you would not need to examine cgi yourself, but since the data has to be produced from scratch, I "cheat"
        auto chart = cgi( "chart" );
        if ( chart == "sbx1" )
         {
          std::map<std::string, Histogram<uint16_t, 3564>> map;
          map["TDC Bin 1"] = m_aggr_plus.m_occupancy.resample<4>( 0 );
          map["TDC Bin 2"] = m_aggr_plus.m_occupancy.resample<4>( 1 );
          map["TDC Bin 3"] = m_aggr_plus.m_occupancy.resample<4>( 2 );
          map["TDC Bin 4"] = m_aggr_plus.m_occupancy.resample<4>( 3 );
          m_occupancy_chart_sbx1->writeDataForQuery( cgi, out,  map );
         }
        if ( chart == "sbx2" )
         {
          std::map<std::string, Histogram<uint16_t, 3564>> map;
          map["TDC Bin 1"] = m_aggr_minus.m_occupancy.resample<4>( 0 );
          map["TDC Bin 2"] = m_aggr_minus.m_occupancy.resample<4>( 1 );
          map["TDC Bin 3"] = m_aggr_minus.m_occupancy.resample<4>( 2 );
          map["TDC Bin 4"] = m_aggr_minus.m_occupancy.resample<4>( 3 );
          m_occupancy_chart_sbx2->writeDataForQuery( cgi, out,  map );
         }
        if ( chart.find( "amp_" ) == 0 )
         {
          std::map<std::string, Histogram<uint64_t, 256>> map;
          bhm::Beam beam = chart[4] == 'p' ? bhm::Beam1 : bhm::Beam2;
          bhm::Side side = chart[5] == 'n' ? bhm::Near : bhm::Far;
          for ( int i = 0; i != 10; ++i )
           {
            bhm::ChannelId id( beam, side, i );
            auto it = m_amplitude_output.find( id );
            if ( it == m_amplitude_output.end() )
              continue;
            auto type = cgi["type"]->getIntegerValue();
            if ( type >= 0 && type < 4 )
              map[to_string( i + 1 )] = it->second.resample<4>( type );
            else
              map[to_string( i + 1 )] = it->second.rebin<4>();
           }
          m_amplitude_chart_pn->writeDataForQuery( cgi, out, map );
          m_amplitude_chart_pf->writeDataForQuery( cgi, out, map );
          m_amplitude_chart_mn->writeDataForQuery( cgi, out, map );
          m_amplitude_chart_mf->writeDataForQuery( cgi, out, map );
         }
       }
      catch ( const std::exception& e )
       {
        XCEPT_RAISE( xgi::exception::Exception, e.what() );
       }
     }

    void Application::actionPerformed( xdata::Event& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received xdata event " << e.type() );
      try
       {
        if ( e.type() == "urn:xdaq-event:setDefaultValues" )
         {
          setupAlgorithms();
          setupEventing();
          setupCharts();
          //start monitoring timer
          toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer( "PeriodicDiagnostic" );
          toolbox::TimeInterval interval( m_mon_timer_period_sec, 0 );
          toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
          timer->scheduleAtFixedRate( start, this, interval,  0, "" );
         }
        else if ( e.type() == "ItemChangedEvent" )
         {
          std::string item = static_cast<xdata::ItemEvent&>( e ).itemName();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Parameter " << item << " changed." );
          SafeLocker locker( m_applock );
          if ( m_aggregate_algo && m_amplitude_algo && m_background_algo ) //don't do unless already initialized
            setupAlgorithms();
         }
       }
      catch ( xcept::Exception& e )
       {
        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Caught exception\n " << stdformat_exception_history( e ) );
        notifyQualified( "error", e );
       }
     }

    void Application::actionPerformed( toolbox::Event& e )
     {
      try
       {
        if( e.type() == "eventing::api::BusReadyToPublish" )
         {
          std::string busname = static_cast<eventing::api::Bus*>( e.originator() )->getBusName();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Bus \"" << busname << "\" is ready." );
          m_unready_buses.erase( busname );
          if ( !m_unready_buses.empty() )
            return; //wait until all buses are ready
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " All buses ready for publishing" );
          setupWorkloop();
         }
       }
      catch ( xcept::Exception& e )
       {
        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Caught exception\n " << stdformat_exception_history( e ) );
        notifyQualified( "error", e );
       }
     }

/**
 * @brief XDAQ Eventing receiver callback
 *
 * Receives the three types of data that are used by this application: the BHM amplitude and occupancy histograms, and the beam information.
 */
    void Application::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist ) 
     {
      try
       {
        RefDestroyer rd( ref );
        std::string action = plist.getProperty( "urn:b2in-eventing:action" );
        if ( action != "notify" )
          return;
        TopicName topic = plist.getProperty( "urn:b2in-eventing:topic" );
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Received data from topic " << topic );
        std::map<TopicName, TopicFunctionType>::iterator func = m_topic_functions.find( topic );
        if ( func == m_topic_functions.end() )
         {
          LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ": Unknown topic " << topic );
          return;
         }
        std::string version = plist.getProperty( "DATA_VERSION" );
        if( version.empty() || version != interface::bril::shared::DATA_VERSION )
         {
          LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ": Missing or wrong version header (\"" << version << "\"), skipping." );
          return;
         }
        TopicFunctionType fun = func->second;
        ( this->*fun )( ref, plist );
       }
      catch ( xcept::Exception& e )
       {
        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Caught exception " << stdformat_exception_history( e ) );
        notifyQualified( "error", e );
       }
     }

/**
 * This function produces monitoring data based on the amplitude histograms
 */
    void Application::onMessageTopicAmplitude( toolbox::mem::Reference* ref, xdata::Properties& )
     {
      interface::bril::bhmamphistT* indata = static_cast<interface::bril::bhmamphistT*>( ref->getDataLocation() );
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Received Channel " << int( indata->channelid ) << " fill " << indata->fillnum );
      updateTime( indata );
      unsigned int tdc = indata->getAlgoID() - 1;
      frontAmplitude().add( *indata, tdc );
      if ( frontAmplitude().has_nibbles( m_required_nibbles_amplitude ) )
       {
        swapAmplitude();
        m_amplitude_ready = true;
        m_waitcondition.signal(); //wake up processing thread
       }
     }

/**
 * Process the histogram cache, by computing background numbers based on the currently active algorithm.
 * This functions reduces individual channels data, accumulated over a LS or so, into global background numbers.
 */
    void Application::onMessageTopicOccupancy( toolbox::mem::Reference* ref, xdata::Properties& )
     {
      interface::bril::bhmocchistT* indata = static_cast<interface::bril::bhmocchistT*>( ref->getDataLocation() );
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Received Channel " << int( indata->channelid ) << " fill " << indata->fillnum );
      updateTime( indata );
      frontOccupancy().add( *indata );
      if ( frontOccupancy().has_nibbles( m_required_nibbles_occupancy ) )
       {
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " has occupancy.");
        swapOccupancy();
        m_occupancy_ready = true;
        m_waitcondition.signal();
       }
     }

/**
 * Unpacks all of the beam data
 */
    void Application::onMessageTopicBeam( toolbox::mem::Reference* ref, xdata::Properties& plist )
     {
      std::string payloaddict = plist.getProperty( "PAYLOAD_DICT" );
      if( payloaddict.empty() )
       {
        LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ": Received data has no dictionary, skipping." );
        return;
       }
      interface::bril::beamT* incoming = static_cast<interface::bril::beamT*>( ref->getDataLocation() );
      //updateTime( incoming ); FIXME beam takes counters from *sw* TCDS I think, better not mix it with the good one
      interface::bril::shared::CompoundDataStreamer cds( payloaddict );
      SafeLocker locker( m_applock );
      char status[30];
      status[29] = '\0';
      if ( cds.extract_field( m_beam_mask_1, "bxconfig1", incoming->payloadanchor ) &&
           cds.extract_field( m_beam_mask_2, "bxconfig2", incoming->payloadanchor ) &&
           cds.extract_field( m_beam_fbct_1, "bxintensity1", incoming->payloadanchor ) &&
           cds.extract_field( m_beam_fbct_2, "bxintensity2", incoming->payloadanchor ) &&
           cds.extract_field( &m_beam_intensity_1, "intensity1", incoming->payloadanchor ) &&
           cds.extract_field( &m_beam_intensity_2, "intensity2", incoming->payloadanchor ) &&
           cds.extract_field( m_beam_collidable, "collidable", incoming->payloadanchor ) &&
           cds.extract_field( status, "status", incoming->payloadanchor ) )
       {
        if ( status != m_beam_status )
         {
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Beam mode changed to " << status );
          //do something at change of beam mode...
          m_beam_status = status;
         }
        m_beam_age = incoming->timestampsec;
       }
      else
        LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ": Could not unpack information from Beam Data." );
     }

/**
 * Updates timing information to the most recent received header
 */
    void Application::updateTime( interface::bril::shared::DatumHead* hdr )
     {
      if ( !hdr->fillnum ) //sometimes the information is missing...
        return;
      SafeLocker locker( m_applock );
      m_mon_fill = hdr->fillnum;
      m_mon_run = hdr->runnum;
      m_mon_ls = hdr->lsnum;
      m_mon_nb = hdr->nbnum;
      m_mon_timestamp = toolbox::TimeVal( hdr->timestampsec, hdr->timestampmsec * 1000 );
     }

/**
 * Collects beam data in a nice format for the background algorithm.
 */
    Application::BeamData Application::beamData( bhm::Beam whichbeam ) const
     {
      BeamData d;
      if ( whichbeam == bhm::Beam1 )
       {
        d.m_mask_incoming = m_beam_mask_1;
        d.m_mask_outgoing = m_beam_mask_2;
        d.m_fbct_incoming = m_beam_fbct_1;
        d.m_fbct_outgoing = m_beam_fbct_2;
        d.m_intensity_incoming = m_beam_intensity_1;
        d.m_intensity_outgoing = m_beam_intensity_2;
       }
      else
       {
        d.m_mask_incoming = m_beam_mask_2;
        d.m_mask_outgoing = m_beam_mask_1;
        d.m_fbct_incoming = m_beam_fbct_2;
        d.m_fbct_outgoing = m_beam_fbct_1;
        d.m_intensity_incoming = m_beam_intensity_2;
        d.m_intensity_outgoing = m_beam_intensity_1;
       }
      return d;
     }

/**
 * Collects monitoring information, checks queue health, etc....
 *
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    void Application::timeExpired( toolbox::task::TimerEvent& e )
     {
//      toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
//      m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this ); this is now done in publishMain()
     }

/**
 * Workloop main function.
 *
 * The .wait() call blocks until the main thread calls .signal().
 *
 * @note This function executes in a different thread, all write access to member variables should be locked.
 *
 * @returns True to continue processing, false otherwise
 */
    bool Application::publishMain( toolbox::task::WorkLoop* wl )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      try
       {
        m_waitcondition.wait(); //maybe timedwait if we want a way out every now and then
        if ( m_amplitude_ready )
         {
          SafeLocker locker( m_applock );
          computeAmplitude();
          m_amplitude_ready = false;
          m_amplitude_chart_pn->newDataReady();
          m_amplitude_chart_pf->newDataReady();
          m_amplitude_chart_mn->newDataReady();
          m_amplitude_chart_mf->newDataReady();
         }
        if ( m_occupancy_ready )
         {
          SafeLocker locker( m_applock );
          computeBackgrounds();
          m_occupancy_ready = false;
          //update occupancy monitoring info:
          m_mon_totalrate_b1 = m_bkgd_plus.m_total;
          m_mon_totalrate_b2 = m_bkgd_minus.m_total;
          std::copy( m_bkgd_plus.m_perbx.begin(), m_bkgd_plus.m_perbx.end(), m_mon_bxrate_b1.begin() );
          std::copy( m_bkgd_minus.m_perbx.begin(), m_bkgd_minus.m_perbx.end(), m_mon_bxrate_b2.begin() );
          m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
          //update charts
          auto ms = static_cast<toolbox::TimeVal>( m_mon_timestamp ).sec() * 1000;
          auto& b1hist = m_rate_history["Beam 1 (BKGD4)"];
          b1hist.push_back( std::make_pair( ms, m_bkgd_plus.m_total ) );
          if ( b1hist.size() > 1000 )
            b1hist.pop_front();
          auto& b2hist = m_rate_history["Beam 2 (BKGD5)"];
          b2hist.push_back( std::make_pair( ms, m_bkgd_minus.m_total ) );
          if ( b2hist.size() > 1000 )
            b2hist.pop_front();
          m_occupancy_chart_main->newDataReady();
          m_timeseries_chart_main->newDataReady();
          m_occupancy_chart_sbx1->newDataReady();
          m_occupancy_chart_sbx2->newDataReady();
         }
       }
      catch ( xcept::Exception& e )
       {
        LOG4CPLUS_ERROR( getApplicationLogger(), __func__ << ": Caught exception " << stdformat_exception_history( e ) );
        notifyQualified( "error", e );
       }
      return true;
     }

/**
 * @note This function executes in a different thread, but it is protected by an external lock.
 */
    void Application::computeAmplitude()
     {
      //LOG4CPLUS_INFO( getApplicationLogger(), __func__ );
      m_amplitude_algo->compute( backAmplitude(), m_amplitude_output );
      //well do something with the result...
      for ( auto it = backAmplitude().begin(); it != backAmplitude().end(); ++it )
       {
        for ( int tdc = 0; tdc != 4; ++tdc )
         {
//          auto amphist = newPub<interface::bril::bhmampagghistT>( tdc + 1, it->first.linearized(), interface::bril::shared::StorageType::UINT32 );
//          for ( int bin = 0; bin != it->second.m_histogram[tdc].size(); ++bin )
//            amphist.second->payload()[bin] = it->second.m_histogram[tdc][bin];
//          doPublish( interface::bril::bhmampagghistT::topicname(), amphist.first );
         }
       }
      backAmplitude().clear( m_amplitude_algo->desired_channels() );
     }

/**
 * Computes background information and publishes to eventing.
 *
 * @note This function executes in a different thread, but it is protected by an external lock.
 */
    void Application::computeBackgrounds()
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ );
      //First step is to reduce the rates from single channels to per-beam.
      m_aggregate_algo->aggregate( backOccupancy(), m_aggr_plus, m_aggr_minus );
      if ( backOccupancy().age() > m_beam_age + 60 )
        LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ": Beam Data is more than one minute out of date; background calculation might be invalid." );
      backOccupancy().clear( m_aggregate_algo->desired_channels() );
      //Then compute background numbers and histograms
      m_background_algo->compute( m_aggr_plus, beamData( bhm::Beam1 ), m_bkgd_plus );
      m_background_algo->compute( m_aggr_minus, beamData( bhm::Beam2 ), m_bkgd_minus );
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " Zplus histo average counts/BX " << m_aggr_plus.m_occupancy.average() << " BKGD4 = " << m_bkgd_plus.m_total );
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " Zminus histo average counts/BX " << m_aggr_minus.m_occupancy.average() << " BKGD5 = " << m_bkgd_minus.m_total );
      //Finally publish the results
      auto bkgsummary = newPub<interface::bril::bhmbkgT>( 0, 0, interface::bril::shared::StorageType::COMPOUND );
      //"beam1:float:1 beam2:float:1 bxbeam1:float:3564 bxbeam2:float:3564 bxerror1:float:3564 bxerror2:float:3564"
      std::string pdict = interface::bril::bhmbkgT::payloaddict();
      interface::bril::shared::CompoundDataStreamer cds( pdict );
      cds.insert_field( bkgsummary.second->payloadanchor, "beam1", &m_bkgd_plus.m_total );
      cds.insert_field( bkgsummary.second->payloadanchor, "beam2", &m_bkgd_minus.m_total );
      cds.insert_field( bkgsummary.second->payloadanchor, "bxbeam1", m_bkgd_plus.m_perbx.begin() );
      cds.insert_field( bkgsummary.second->payloadanchor, "bxbeam2", m_bkgd_minus.m_perbx.begin() );
      cds.insert_field( bkgsummary.second->payloadanchor, "bxerror1", m_bkgd_plus.m_perbx_error.begin() );
      cds.insert_field( bkgsummary.second->payloadanchor, "bxerror2", m_bkgd_minus.m_perbx_error.begin() );
      doPublish( interface::bril::bhmbkgT::topicname(), bkgsummary.first, pdict );
     }

/**
 * @note This function executes in a different thread, but it is protected by an external lock.
 */
    template <typename T>
    std::pair<toolbox::mem::Reference*, T*> Application::newPub( unsigned int algoid, unsigned int channelid, unsigned int storage_type )
     {
      toolbox::mem::Reference* memref = 0;
      memref = m_pool_factory->getFrame( m_mem_pool, T::maxsize() );
      memref->setDataSize( T::maxsize() );
      T* datum = static_cast<T*>( memref->getDataLocation() );
      datum->setTime( m_mon_fill, m_mon_run, m_mon_ls, m_mon_nb, static_cast<toolbox::TimeVal>( m_mon_timestamp ).sec(), 0 );
      datum->setFrequency( 64 );
      datum->setTotalsize( T::maxsize() );
      datum->setResource( interface::bril::shared::DataSource::BHM, algoid, channelid, storage_type );
      return std::make_pair( memref, datum );
     }

/**
 * @note This function executes in a different thread, but it is protected by an external lock.
 */
    void Application::doPublish( const TopicName& topic, toolbox::mem::Reference* memref, const std::string& payloaddict )
     {
      RefDestroyer rd( memref );
      xdata::Properties plist;
      plist.setProperty( "DATA_VERSION", interface::bril::shared::DATA_VERSION );
      if ( !payloaddict.empty() )
        plist.setProperty( "PAYLOAD_DICT", payloaddict );
      auto iset = m_output_topics_map.find( topic );
      if ( iset == m_output_topics_map.end() )
        XCEPT_RAISE( exception::EventingError, "Topic is not in the list of known topics" );
      for ( auto it = iset->second.begin(); it != iset->second.end(); ++it )
       {
        BusName bus = *it;
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": publishing to " << bus << '/' << topic << " a message of size " << memref->getDataSize() );
        toolbox::mem::Reference* ref = memref->duplicate(); //Having to do this is truly *awful*, xdaq memory allocation *sucks* hardcore. The dupe reference is leaked, btw...
        getEventingBus( bus ).publish( topic, ref, plist );
       }
     }

   }

 }
