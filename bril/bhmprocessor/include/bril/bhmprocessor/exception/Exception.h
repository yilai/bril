#ifndef _bril_bhmprocessor_exception_Exception_h_
#define _bril_bhmprocessor_exception_Exception_h_

#include "xcept/Exception.h"

namespace bril
 {

  namespace bhmprocessor
   {

    namespace exception
     {

      class AlgorithmError : public xcept::Exception
       {

        public: 
        AlgorithmError( std::string name, std::string message, std::string module, int line, std::string function ) :
         xcept::Exception( name, message, module, line, function )
         {}

        AlgorithmError( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
         xcept::Exception( name, message, module, line, function, e )
         {}

       };

      class ConfigurationError : public xcept::Exception
       {

        public: 
        ConfigurationError( std::string name, std::string message, std::string module, int line, std::string function ) :
         xcept::Exception( name, message, module, line, function )
         {}

        ConfigurationError( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
         xcept::Exception( name, message, module, line, function, e )
         {}

       };

      class EventingError : public xcept::Exception
       {

        public: 
        EventingError( std::string name, std::string message, std::string module, int line, std::string function ) :
         xcept::Exception( name, message, module, line, function )
         {}

        EventingError( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
         xcept::Exception( name, message, module, line, function, e )
         {}

       };

      class UnknownError : public xcept::Exception
       {

        public: 
        UnknownError( std::string name, std::string message, std::string module, int line, std::string function ) :
         xcept::Exception( name, message, module, line, function )
         {}

        UnknownError( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
         xcept::Exception( name, message, module, line, function, e )
         {}

       };

     }

   }

 }

#endif

