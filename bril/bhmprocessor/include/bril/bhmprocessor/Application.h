/**
 * @brief BHM Processor xdaq Application.
 *
 * @author S. Orfanelli, N. Tosi \<nicko@cern.ch\>
 *
 * This Application processes histogram data for BHM, producing a background number and a per-BX
 * histogram of Machine Induced Background per beam.
 *
 */

#ifndef _bril_bhmprocessor_Application_h_
#define _bril_bhmprocessor_Application_h_

#include <list>
#include <map>
#include <set>
#include <string>

#include "b2in/nub/exception/Exception.h"

#include "bril/bhmprocessor/ChannelId.h"
#include "bril/bhmprocessor/Histogram.h"

#include "eventing/api/Member.h"

#include "interface/bril/BHMTopics.hh"
#include "interface/bril/BEAMTopics.hh"

#include "log4cplus/logger.h"

#include "toolbox/ActionListener.h"
#include "toolbox/BSem.h"
#include "toolbox/Condition.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "toolbox/task/TimerListener.h"

#include "xdaq/Application.h"

#include "xdata/ActionListener.h"
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xgi/Method.h"
#include "xgi/Output.h"

namespace toolbox
 {
  namespace task
   {
    class WorkLoop;
   }
 }

namespace bril
 {

  namespace webutils
   {
    class WebChart;
   }

  namespace bhmprocessor
   {

    class AggregateAlgorithm;

    class AmplitudeAlgorithm;

    class BackgroundAlgorithm;

    class Application : public xdaq::Application,
                        public xgi::framework::UIManager,
                        public eventing::api::Member,
                        public xdata::ActionListener,
                        public toolbox::ActionListener,
                        public toolbox::task::TimerListener
     {

      public:
      typedef std::vector<bhm::ChannelId> ChannelIdVector;

      /**
       * An histogram cache which allows simple integration over time.
       */
      template <typename H, int Count = 1>
      class Cache
       {

        public:
        typedef H histogram_type;

        struct Data
         {
          histogram_type m_histogram[Count];
          unsigned int m_nibbles[Count];
          unsigned int m_quality[Count];

          void clear()
           {
            for ( int n = 0; n != Count; ++n )
             {
              m_histogram[n].clear();
              m_nibbles[n] = 0;
              m_quality[n] = 0;
             }
           }
         };

        typedef std::map<bhm::ChannelId, Data> map_type;

        public:
        Cache( const ChannelIdVector& good = ChannelIdVector() )
         {
          clear( good );
         }

        void clear( const ChannelIdVector& );

        template <typename T>
        bool add( const interface::bril::shared::Datum<T>&, unsigned int = 0 );

        bool has_nibbles( unsigned int );

        typename map_type::size_type size() const
         {
          return m_cache.size();
         }

        typename map_type::const_iterator begin() const
         {
          return m_cache.begin();
         }

        typename map_type::const_iterator end() const
         {
          return m_cache.end();
         }

        unsigned int age() const
         {
          return m_age;
         }

        void swap( Cache<H, Count>& other )
         {
          m_cache.swap( other.m_cache );
          std::swap( m_age, other.m_age );
         }

        private:
        std::map<bhm::ChannelId, Data> m_cache;
        unsigned int m_age;

       };

      typedef Histogram<uint16_t, 14256> OccupancyHistogram;
      typedef Histogram<float, 3564> BXHistogram;
      typedef Histogram<uint32_t, 256> AmplitudeHistogram;
      typedef Histogram<uint64_t, 1024> AggregateAmplitudeHistogram;

      typedef Cache<OccupancyHistogram> OccupancyCache;
      typedef Cache<AmplitudeHistogram, 4> AmplitudeCache;

      struct AggregatedData
       {
        OccupancyHistogram m_occupancy;
        float m_exposure; //Area * time [s*cm^2]
        int m_quality;
       };

      struct BeamData
       {
        const bool* m_mask_incoming;
        const bool* m_mask_outgoing;
        const float* m_fbct_incoming;
        const float* m_fbct_outgoing;
        float m_intensity_incoming;
        float m_intensity_outgoing;
       };

      struct BackgroundOutput
       {
        BXHistogram m_perbx;
        BXHistogram m_perbx_error;
        float m_total;
        float m_total_error;
        int m_quality;
       };

      typedef std::map<bhm::ChannelId, AggregateAmplitudeHistogram> AmplitudeOutput;

      typedef std::string BusName;
      typedef std::string TopicName;
      typedef std::set<BusName> BusNameSet;
      typedef void ( Application::*TopicFunctionType )( toolbox::mem::Reference*, xdata::Properties& );

      public:
      XDAQ_INSTANTIATOR();

      Application( xdaq::ApplicationStub* );

      ~Application();

      virtual void Default( xgi::Input* in, xgi::Output* out ); // xgi(web) callback

      virtual void expertPage( xgi::Input* in, xgi::Output* out ); // xgi(web) callback

      virtual void requestData( xgi::Input* in, xgi::Output* out ); // xgi(web) callback

      virtual void actionPerformed( xdata::Event& ); // infospace event callback

      virtual void actionPerformed( toolbox::Event& ); // toolbox event callback

      virtual void onMessage( toolbox::mem::Reference*, xdata::Properties& ); // b2in message callback

      virtual void timeExpired( toolbox::task::TimerEvent& ); // timer callback

      private:
      Application( const Application& );

      Application& operator = ( const Application& );

      private:
      void setupAlgorithms();

      void setupCharts();

      void setupConfiguration();

      void setupEventing();

      void setupMonitoring();

      void setupWorkloop();

      void onMessageTopicAmplitude( toolbox::mem::Reference*, xdata::Properties& );

      void onMessageTopicOccupancy( toolbox::mem::Reference*, xdata::Properties& );

      void onMessageTopicBeam( toolbox::mem::Reference*, xdata::Properties& );

      void updateTime( interface::bril::shared::DatumHead* );

      BeamData beamData( bhm::Beam ) const;

      bool publishMain( toolbox::task::WorkLoop* );

      void computeAmplitude();

      void computeBackgrounds();

      void doPublish( const TopicName&, toolbox::mem::Reference*, const std::string& = std::string() );

      template <typename T>
      std::pair<toolbox::mem::Reference*, T*> newPub( unsigned int, unsigned int, unsigned int );

      OccupancyCache& frontOccupancy()
       {
        return m_occupancy_cache[m_occupancy_sel ? 1 : 0];
       }

      OccupancyCache& backOccupancy()
       {
        return m_occupancy_cache[m_occupancy_sel ? 0 : 1];
       }

      void swapOccupancy()
       {
//        m_applock.take();
        m_occupancy_sel = !m_occupancy_sel;
//        m_applock.give();
       }

      AmplitudeCache& frontAmplitude()
       {
        return m_amplitude_cache[m_amplitude_sel ? 1 : 0];
       }

      AmplitudeCache& backAmplitude()
       {
        return m_amplitude_cache[m_amplitude_sel ? 0 : 1];
       }

      void swapAmplitude()
       {
//        m_applock.take();
        m_amplitude_sel = !m_amplitude_sel;
//        m_applock.give();
       }

      private: //xdaq and miscellaneous stuff
      toolbox::BSem m_applock;
      std::map<TopicName, BusNameSet> m_output_topics_map;
      BusNameSet m_unready_buses;
      toolbox::mem::MemoryPoolFactory* m_pool_factory;
      toolbox::mem::Pool* m_mem_pool;
      toolbox::task::WorkLoop* m_publish_wl;
      std::map<TopicName, TopicFunctionType> m_topic_functions;

      private: //these members are available for external configuration (xml, soap, whatever...)
      xdata::Vector<xdata::Properties> m_datasources;
      xdata::Vector<xdata::Properties> m_outputtopics;
      xdata::Properties m_aggregate_algo_prop;
      xdata::Properties m_background_algo_prop;
      xdata::Properties m_amplitude_algo_prop;
      xdata::UnsignedInteger m_required_nibbles_occupancy;
      xdata::UnsignedInteger m_required_nibbles_amplitude;
      xdata::UnsignedInteger m_mon_timer_period_sec;

      private: //actual processing variables
      //The caches are double buffered to allow processing in a separate thread
      OccupancyCache m_occupancy_cache[2];
      AmplitudeCache m_amplitude_cache[2];
      bool m_occupancy_sel;
      bool m_amplitude_sel;

      toolbox::Condition m_waitcondition;
      bool m_occupancy_ready;
      bool m_amplitude_ready;

      AggregateAlgorithm* m_aggregate_algo;
      AmplitudeAlgorithm* m_amplitude_algo;
      BackgroundAlgorithm* m_background_algo;

      bool m_beam_mask_1[interface::bril::shared::MAX_NBX];
      bool m_beam_mask_2[interface::bril::shared::MAX_NBX];
      float m_beam_fbct_1[interface::bril::shared::MAX_NBX];
      float m_beam_fbct_2[interface::bril::shared::MAX_NBX];
      float m_beam_intensity_1;
      float m_beam_intensity_2;
      bool m_beam_collidable[interface::bril::shared::MAX_NBX];
      std::string m_beam_status;
      unsigned int m_beam_age;

      AggregatedData m_aggr_plus;
      AggregatedData m_aggr_minus;

      BackgroundOutput m_bkgd_plus;
      BackgroundOutput m_bkgd_minus;

      AmplitudeOutput m_amplitude_output;

      private: //monitoring variables
      xdata::InfoSpace* m_mon_infospace;
      std::list<std::string> m_mon_varlist;
      std::vector<xdata::Serializable*> m_mon_vars;
      xdata::String m_mon_lid;
      xdata::String m_mon_context;
      xdata::UnsignedInteger m_mon_fill;
      xdata::UnsignedInteger m_mon_run;
      xdata::UnsignedInteger m_mon_ls;
      xdata::UnsignedInteger m_mon_nb;
      xdata::TimeVal m_mon_timestamp;
      xdata::Float m_mon_totalrate_b1;
      xdata::Float m_mon_totalrate_b2;
      xdata::Vector<xdata::Float> m_mon_bxrate_b1;
      xdata::Vector<xdata::Float> m_mon_bxrate_b2;

      private:
      std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_rate_history;
      webutils::WebChart* m_occupancy_chart_main;
      webutils::WebChart* m_timeseries_chart_main;
      webutils::WebChart* m_occupancy_chart_sbx1;
      webutils::WebChart* m_occupancy_chart_sbx2;
      webutils::WebChart* m_amplitude_chart_pn;
      webutils::WebChart* m_amplitude_chart_pf;
      webutils::WebChart* m_amplitude_chart_mn;
      webutils::WebChart* m_amplitude_chart_mf;

    };

   }

 }

#endif

