// $Id$
#ifndef _bril_bhmprocessor_version_h_
#define _bril_bhmprocessor_version_h_
#include "config/PackageInfo.h"
#define BRIL_BRILBHMPROCESSOR_VERSION_MAJOR 4
#define BRIL_BRILBHMPROCESSOR_VERSION_MINOR 1
#define BRIL_BRILBHMPROCESSOR_VERSION_PATCH 0
#define BRILBHMPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRIL_BRILBHMPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_BRILBHMPROCESSOR_VERSION_MAJOR,BRIL_BRILBHMPROCESSOR_VERSION_MINOR,BRIL_BRILBHMPROCESSOR_VERSION_PATCH)
#ifndef BRIL_BRILBHMPROCESSOR_PREVIOUS_VERSIONS
#define BRIL_BRILBHMPROCESSOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_BRILBHMPROCESSOR_VERSION_MAJOR,BRIL_BRILBHMPROCESSOR_VERSION_MINOR,BRIL_BRILBHMPROCESSOR_VERSION_PATCH)
#else
#define BRIL_BRILBHMPROCESSOR_FULL_VERSION_LIST BRIL_BRILBHMPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_BRILBHMPROCESSOR_VERSION_MAJOR,BRIL_BRILBHMPROCESSOR_VERSION_MINOR,BRIL_BRILBHMPROCESSOR_VERSION_PATCH)
#endif
namespace brilbhmprocessor{
  const std::string project = "bril";
  const std::string package = "brilbhmprocessor";
  const std::string versions = BRIL_BRILBHMPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bhmprocessor";
  const std::string description = "collect and process histograms from bhmsource";
  const std::string authors = "Nicolo, Stella";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies ();
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
