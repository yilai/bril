#ifndef _pltslinkprocessor_exception_PLTAlarm_h_
#define _pltslinkprocessor_exception_PLTAlarm_h_

#include "bril/pltslinkprocessor/exception/Exception.h"

namespace bril {
    namespace pltslinkprocessor {
        namespace exception {
            class PLTAlarm: public bril::pltslinkprocessor::exception::Exception
            {
                public:
                PLTAlarm(std::string name, std::string message, 
                         std::string module, int line, std::string function):
                         bril::pltslinkprocessor::exception::Exception(name, message, module, line, function)
                {}

                PLTAlarm(std::string name, std::string message, 
                         std::string module, int line, std::string function, 
                         xcept::Exception& e):
                         bril::pltslinkprocessor::exception::Exception(name, message, module, line, function, e)
                {}
            };
        }
    }
}
#endif 
