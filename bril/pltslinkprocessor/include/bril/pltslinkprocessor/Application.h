// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_pltslinkprocessor_Application_h_
#define _bril_pltslinkprocessor_Application_h_

#include <string>
#include <fstream>
#include <iostream>

#include "xdaq/Application.h"

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"

//#include "root/TH2F.h"
//#include "root/TFile.h"

#include "xdata/ActionListener.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"
#include "xdata/TimeVal.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"

#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"

#include "toolbox/task/TimerListener.h"
#include "toolbox/ActionListener.h"
#include "toolbox/EventDispatcher.h"

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"

#include "bril/pltslinkprocessor/EventAnalyzer.h"

// sentinel alarm
#include "sentinel/utils/Alarm.h"

// Nico's stuff for webpages with charts
#include "bril/webutils/WebUtils.h"

namespace toolbox{
    namespace task{
        class WorkLoop;
    }
}

namespace bril
{
    namespace webutils{
        class WebChart;
    }
    namespace pltslinkprocessor
    {
        class Application : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener, public toolbox::ActionListener, public toolbox::task::TimerListener
        {
            public:

                XDAQ_INSTANTIATOR();

                Application (xdaq::ApplicationStub* s);
                ~Application ();

                void Default (xgi::Input * in, xgi::Output * out);
                void requestData (xgi::Input * in, xgi::Output * out);
                void onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist);
                virtual void actionPerformed(xdata::Event& e);
                virtual void actionPerformed(toolbox::Event& e);
                virtual void timeExpired(toolbox::task::TimerEvent& e);
                void automask(float totdata[16]);

            private:
                void zmqClient();
                void catchDropouts();
                void setupCharts();

                void initializeOutputFiles(unsigned runNumber, std::vector<unsigned> channels);

                bool publish_flash(toolbox::task::WorkLoop* wl);

                std::vector<int> vectorToHist(std::vector<float> &vecIn, unsigned int nBins, float minVal, float maxVal);

                xdata::InfoSpace* m_flashInfoSpace;
                std::string m_beamstatus;
                xdata::InfoSpace* m_sentinelInfoSpace;

                webutils::WebChart* m_error_chart_main;
                std::map<std::string, std::deque<std::pair<unsigned long, int>>> m_error_history; 

                std::map<std::pair<int,int>, webutils::WebChart*> m_pulse_hist;
                std::map<std::pair<int,int>, std::map<std::string, std::vector<int>>> m_pulse_vals;
                
                std::map<std::pair<int,int>, webutils::WebChart*> m_occ_chart;
                std::map<std::pair<int,int>, std::map<std::string, std::vector<webutils::heatmap_point<int, int, int>>>> m_occ_vals;

		std::vector<unsigned int> m_activeChannels;
		unsigned int m_nChannels = 0;
		
            protected:

                toolbox::mem::MemoryPoolFactory *m_poolFactory;
                const xdaq::ApplicationDescriptor *m_appDescriptor;
                std::string m_classname;
                xdata::UnsignedInteger32 m_instanceid;
                toolbox::mem::Pool* m_memPool;

                xdata::UnsignedInteger32 m_currentfill;
                xdata::UnsignedInteger32 m_currentnib;
                xdata::UnsignedInteger32 m_prevnib;	
                xdata::UnsignedInteger32 m_currentls;
                xdata::UnsignedInteger32 m_currentrun;
                xdata::UnsignedInteger32 m_prevls;

                int m_nibble;
                int m_fill;
                int m_run;
                xdata::UnsignedInteger32 m_fill_flash;
                xdata::UnsignedInteger32 m_run_flash;
                xdata::UnsignedInteger32 m_ls_flash;
                int m_ls;

                bool m_channelMask[16];

                unsigned int nPulseBins;
                float pulseMax;

                //for flashlist
                xdata::TimeVal m_timestamp;
                xdata::Float m_acc_avg;
                xdata::Vector<xdata::Float> m_acc_perchannel;
                xdata::Float m_eff_avg;
                xdata::Vector<xdata::Float> m_eff_perchannel;
                xdata::Vector<xdata::Float> m_rate_perchannel;
                xdata::Vector<xdata::Float> m_acc_sum;
                xdata::Vector<xdata::Float> m_eff_sum;
                xdata::Vector<xdata::Float> m_rate_sum;
                std::map<int, xdata::Integer> m_error_perchannel_map;
                std::map<int, xdata::Integer> m_error_sum_map;
                xdata::Vector<xdata::Integer> m_error_perchannel;
                xdata::Integer m_error_sum;
                unsigned int m_publish_count;
                
                xdata::String m_signalTopic;
                xdata::String m_bus;
                xdata::String m_workloopHost;
                xdata::String m_slinkHost;
		xdata::String m_activeChannelList;
                xdata::String m_gcFile;
                xdata::String m_alFile;
                xdata::String m_trackFile;
                xdata::String m_pixMask;
                xdata::String m_baseDir;
                xdata::Float m_calibVal;
                xdata::Boolean m_MakeLogs;
                xdata::Boolean m_AlwaysFlash;
                xdata::Integer m_err_thresh;
                
                // files for validation
		std::ofstream effFile;
		std::ofstream accFile;
		std::ofstream lumiFile;
		std::ofstream publishFile;
		std::ofstream flashFile;

                toolbox::task::WorkLoop* m_publish_flash;
                toolbox::task::ActionSignature* m_publish_flash_as;
        };
    }
}

#endif
