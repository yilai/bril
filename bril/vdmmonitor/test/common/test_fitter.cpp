#include "bril/vdmmonitor/Fitter.h"
#include "bril/vdmmonitor/RandGenerator.h"
#include <stdlib.h>     /* srand, rand */
#include <vector>
#include <iostream>
#include <numeric>


int main ()
{
  /* initialize random seed: */
  srand (time(NULL));

  const int nrolls=10000;  // number of experiments
  const int nstars=500000;    // maximum number of stars to distribute
  float p[24]={0};
  float one_shape[24]={0};
  float one_BC_fake[24]={0};
  std::vector<float> myvector_one_shape;
  std::vector<float> myvector_oneBC_shape;
  std::vector< std::vector <float> > All_shapes_X;
  std::vector< std::vector <float> > All_BCs_b1;
  std::vector< std::vector <float> > All_BCs_b2;
  std::vector<float> bxwidth;
  std::vector<float> bxpeak;
  std::vector<float> bxwidth_err;
  std::vector<float> bxpeak_err;
  float avgwidth;
  float avgpeak;
  float avgwidth_err;
  float avgpeak_err;
  //TF1 *gaus;
  //Double_t par[3];

  for (int bx=0; bx<3564; bx++) { //3564

    for (int i=0; i<nrolls; ++i) {
      float rand_number = bril::vdmmonitor::box_muller(0.0,0.3);
      // p[24] is just 24 bins, which bin of p will be filled is defined using rand_number
      if ((rand_number>=-12)&&(rand_number<12.0)) ++p[int(rand_number*10+12)];
    }

  std::cout << "normal_distribution ():" << std::endl;

  for (int i=0; i<24; ++i) {
    one_shape[i] = p[i]*nstars/(nrolls*100000);
    one_BC_fake[i] = 2.0;

    std::cout << one_shape[i] << std::endl;
    //to drow one shape with "*" stars
    //std::cout << p[i]*nstars/(nrolls*100000) << std::endl;
    //std::cout << i-12 << "-" << (i-12+1) << ": ";
    //std::cout << std::string(p[i]*nstars/nrolls,'*') << std::endl;
  }

  myvector_one_shape.assign (one_shape,one_shape+24);
  myvector_oneBC_shape.assign(one_BC_fake,one_BC_fake+24);

  All_shapes_X.push_back(myvector_one_shape);
  All_BCs_b1.push_back(myvector_oneBC_shape);
  All_BCs_b2.push_back(myvector_oneBC_shape);

  std::fill( p, p+24, 0 );

  }//end of bx loop

  bril::vdmmonitor::fit_vector(All_shapes_X, All_BCs_b1, All_BCs_b2, bxwidth, bxwidth_err, bxpeak, bxpeak_err, avgwidth, avgwidth_err, avgpeak, avgpeak_err);  
  std::cout << "After function with BC was called " << '\n';
 return 0;
}
