/**
 * @brief VDM scan xdaq Application.
 *
 * @author Zhen Xie
 *
 */

#ifndef _bril_vdmmonitor_VDMScan_h_
#define _bril_vdmmonitor_VDMScan_h_

#include <string>
#include <vector>
#include <array>
#include <map>
#include <fstream>

#include "eventing/api/Member.h"
#include "toolbox/ActionListener.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Properties.h"
#include "xgi/framework/UIManager.h"

namespace toolbox
 {
  namespace task
   {
     class WorkLoop;
     class ActionSignature;
   }
 }

namespace bril
{
  /**
     VDM scan on a single IP
   */
  namespace vdmmonitor
  {

    static const int MAXNBX=3564;
    
    struct BPMData{
    public:
    BPMData():Names(""),hPos(0.),vPos(0.),hErr(0.),vErr(0.){}
      std::string Names;
      float hPos;
      float vPos;
      float hErr;
      float vErr;
      void reset(){
	Names = "";
	hPos = 0.;
	vPos = 0.;
	hErr = 0.;
	vErr = 0.;
      }
    };
    
    struct VDMScanData{
    public:
    VDMScanData():plane("UNKNOWN"),step(0),progress(0),beam(0),stat("UNKNOWN"),nominal_separation(0.),read_nominal_B1sepPlane(0.),read_nominal_B1xingPlane(0.),read_nominal_B2sepPlane(0.),read_nominal_B2xingPlane(0.),set_nominal_B1sepPlane(0.),set_nominal_B1xingPlane(0.),set_nominal_B2sepPlane(0.),set_nominal_B2xingPlane(0.),nominal_separation_plane("UNKNOWN"),scanname("UNKNOWN"){
    }
      void reset(){
	plane = "UNKNOWN";
	step = 0;
	progress = 0;
	beam = 0;
	stat = "UNKNOWN";
	nominal_separation = 0.;
	read_nominal_B1sepPlane = 0.;
	read_nominal_B1xingPlane = 0.;
	read_nominal_B2sepPlane = 0.;
	read_nominal_B2xingPlane = 0.;
	set_nominal_B1sepPlane = 0.;
	set_nominal_B1xingPlane = 0.;
	set_nominal_B2sepPlane = 0.;
	set_nominal_B2xingPlane = 0.;
	nominal_separation_plane = "UNKNOWN";
	scanname = "UNKNOWN";
      }
      std::string plane;
      int step;
      int progress;
      int beam;
      std::string stat;
      float nominal_separation;
      float read_nominal_B1sepPlane;
      float read_nominal_B1xingPlane;
      float read_nominal_B2sepPlane;
      float read_nominal_B2xingPlane;
      float set_nominal_B1sepPlane;
      float set_nominal_B1xingPlane;
      float set_nominal_B2sepPlane;
      float set_nominal_B2xingPlane;   
      std::string nominal_separation_plane;
      std::string scanname;
    };
    
    class VDMScan : public xdaq::Application,
      public xgi::framework::UIManager,
      public eventing::api::Member,
      public xdata::ActionListener,
      public toolbox::ActionListener,
      public toolbox::EventDispatcher,
      public toolbox::task::TimerListener
      {
	
	XDAQ_INSTANTIATOR();

	VDMScan( xdaq::ApplicationStub* );
	
	virtual ~VDMScan();
	
	virtual void Default( xgi::Input* in, xgi::Output* out ); // xgi(web) callback
	
	virtual void actionPerformed( xdata::Event& ); // infospace event callback

	virtual void actionPerformed( toolbox::Event& ); // toolbox event callback
	
	virtual void onMessage( toolbox::mem::Reference*, xdata::Properties& ); // b2in message callback
	
	virtual void timeExpired( toolbox::task::TimerEvent& ); // timer callback
	
      private: 
	
	void writeVDMToCSV( std::ostream& ss );
	void writeVDMHeader( std::ostream& ss );
	void handleDipMessage( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist );
	void publishToEventing(const std::string& topicname);
	void publishToDIPEventing(const std::string& topicname);
	bool do_publishToEventing(toolbox::task::WorkLoop* wl);	
	bool do_publishToDIP(toolbox::task::WorkLoop* wl);	
	bool do_writeVDMToCSV(toolbox::task::WorkLoop* wl);    
	void do_dippublish_automaticscan( const std::string& diptopic );
	void do_publish_atlasbeam();      
	void do_publish_luminousregion();
	void do_publish_vdmscanflag();
	void do_publish_vdmscandata();
	void do_publish_bsrlfracBunched();
	void do_publish_bsrlqBunched();
	void do_publish_bstar();
	void do_publish_to_dipbus( const std::string& topicname, xdata::Table::Reference dipmessage);

	void do_processScanData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist );
	
	void do_processBPMData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist );
	
	void do_processAtlasData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist );
	
	void do_processBSRLData( const std::string& topic, toolbox::mem::Reference * ref, xdata::Properties & plist );
	
	void do_republish();

	void initCache();
	
	std::vector<std::string> gettruenames( const std::string& pattern );

	void cleanRepubCache();

	VDMScan( const VDMScan& );
	VDMScan& operator = ( const VDMScan& );      
	
      private: 
	
	toolbox::BSem m_applock;

	//configuration params
	xdata::String m_busName;
	xdata::String m_dipbusName;
	xdata::String m_diptopic;
	xdata::String m_signalTopic;
	xdata::String m_csvoutDir;
	xdata::String m_dipRoot;
	xdata::Boolean m_withcsv;

	//configured topics
	xdata::String m_vdmscanTopic;
	xdata::String m_bstarTopics;
	xdata::String m_luminousRegionTopics;
	xdata::String m_bpmTopics;
	xdata::String m_atlasTopics;
	xdata::String m_bsrlTopics;
	xdata::String m_outTopics;
	xdata::String m_outDIPTopics;
	xdata::String m_republishEvtTopics;

	//registry
	std::vector< std::string > m_dipsubs;
	std::vector< std::string > m_evtpubs;
	std::vector< std::string > m_dippubs;//no dippub outside ip5
	std::vector< std::string > m_repubs;
	//monitoring 
	
	toolbox::mem::MemoryPoolFactory* m_poolFactory;
	toolbox::mem::Pool* m_memPool;
	
	unsigned int m_fillnum;
	unsigned int m_runnum;
	unsigned int m_lsnum;
	unsigned int m_nbnum;
	unsigned int m_tssec;
	unsigned int m_tsmsec;
	bool m_evtcanpublish;
	bool m_dipcanpublish;
	bool m_canpublish;
	bool m_evtpublishsubmitted;
	bool m_dippublishsubmitted;
	std::string m_processuuid;
	std::string m_instanceidstr;
	//workloops
	toolbox::task::WorkLoop* m_wl_writecsv;
	toolbox::task::WorkLoop* m_wl_evtpublish;
	toolbox::task::WorkLoop* m_wl_dippublish;
	toolbox::task::ActionSignature* m_as_writecsv;
	toolbox::task::ActionSignature* m_as_evtpublish;
	toolbox::task::ActionSignature* m_as_dippublish;
	//
	//data caches
	//
	//vdm flags
	int m_ip;
	std::string m_ipstr;
	bool m_currentScanActive;
	bool m_previousScanActive;
	bool m_acqflag;
	bool m_bsrlfracBunched_canpublish;
	bool m_bsrlqBunched_canpublish;
	//vdm data
	VDMScanData m_scandatacache;
	//bpm data
	std::map<int,BPMData> m_l_bpmdatacache; //beam1,2
	std::map<int,BPMData> m_r_bpmdatacache; //beam1,2
      //atlas data , beam(to eventing), lumi(to csv)   
	std::map< int, float > m_atlasBeamIntensity; //beam1,2
	std::map< int, std::array<float,MAXNBX> > m_atlasBXIntensity; //beam1,2
	float m_atlas_totInst;
	//bsrl data      
	unsigned int m_bsrl_acqStamp;
	std::array<float,2> m_bsrl_post_fraction_ghost_bunched;
	std::array<float,2> m_bsrl_post_fraction_sat_bunched;
	float m_bsrl_post_q_bunched[2][3][11880];
	//luminous region
	std::array<float,2> m_luminous_region_tilt;
	std::array<float,3> m_luminous_region_centroid;
	std::array<float,3> m_luminous_region_size;
	//bstar data
	int m_bstar5;
	int m_ip5xingHmurad;
	std::ofstream m_vdmfile;
	std::string m_bsrlfracBunched_timerurn;
	std::string m_bsrlqBunched_timerurn;
	//republish cache
	std::map< std::string, std::pair<toolbox::mem::Reference*, xdata::Properties> > m_republishCache;
      }; //end class VDMScan
    
  } //end ns vdmmonitor
  
} //end ns bril

#endif

