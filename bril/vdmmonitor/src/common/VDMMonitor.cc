#include "cgicc/CgiDefs.h" 
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Guard.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/AutoReference.h"
#include "toolbox/task/TimerFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"

#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "bril/vdmmonitor/VDMMonitor.h"
#include "bril/vdmmonitor/Events.h"
#include "bril/vdmmonitor/exception/Exception.h"
#include "bril/vdmmonitor/Fitter.h"
#include "interface/bril/BEAMTopics.hh"
#include <cstdio>
#include <cmath>
#include <numeric>

XDAQ_INSTANTIATOR_IMPL(bril::vdmmonitor::VDMMonitor)

namespace bril {

  namespace vdmmonitor {

    VDMMonitor::VDMMonitor(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){

      xgi::framework::deferredbind(this,this,&bril::vdmmonitor::VDMMonitor::Default, "Default");
      
      b2in::nub::bind(this, &bril::vdmmonitor::VDMMonitor::onMessage);

      m_bus.fromString("brildata");//default
      m_vdmflagTopic.fromString("vdmflag");
      m_vdmscanTopic.fromString("vdmscan");
      m_beamTopic.fromString("beam");
      m_bunchlengthTopic.fromString("bunchlength");
      m_outtopic.fromString("vdmstep");
      m_maxIntervalSec = 300;//time(sec) threshold to declare scan off
      m_planeOffThresholdSec = 60;//time(sec) threshold to declare a plane off
      m_sleepBeforeSigma = 25000000; //25s
      //
      // register configuration parameters
      //
      getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
      getApplicationInfoSpace()->fireItemAvailable("vdmflagTopic",&m_vdmflagTopic);
      getApplicationInfoSpace()->fireItemAvailable("vdmscanTopic",&m_vdmscanTopic);
      getApplicationInfoSpace()->fireItemAvailable("beamTopic",&m_beamTopic);
      getApplicationInfoSpace()->fireItemAvailable("bunchlengthTopic",&m_bunchlengthTopic);
      getApplicationInfoSpace()->fireItemAvailable("lumiTopics",&m_lumiTopicStr);
      getApplicationInfoSpace()->fireItemAvailable("outTopic",&m_outtopic);
      getApplicationInfoSpace()->fireItemAvailable("maxIntervalSec",&m_maxIntervalSec);
      getApplicationInfoSpace()->fireItemAvailable("planeOffThresholdSec",&m_planeOffThresholdSec);
      getApplicationInfoSpace()->fireItemAvailable("sleepBeforeSigma",&m_sleepBeforeSigma);      
      getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	              
      m_fitting_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("VDMMonitor_fitting","waiting");  
      m_as_fitting = toolbox::task::bind(this,&bril::vdmmonitor::VDMMonitor::fitting,"fitting");
      m_fitting_wl->activate();   

      m_analyzing_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("VDMMonitor_analyzing","waiting");      
      m_as_analyzing = toolbox::task::bind(this,&bril::vdmmonitor::VDMMonitor::analyzing,"analyzing");
      m_analyzing_wl->activate();         

      //prepare vdmmon infospace
      std::string nid("vdmmon");
      std::string monurn = createQualifiedInfoSpace(nid).toString();
      m_IS_vdmmon = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));
      nid="vdmsigmamon";
      monurn = createQualifiedInfoSpace(nid).toString();
      m_IS_vdmsigmamon = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurn));

      init_shapeFlashlist();
      init_sigmaFlashlist();
      declareFlashlists();    
  
      m_canpublish = false;

      m_previousAcqflag = false;
      m_previousPlane = 0;
      m_vdmflag = false;
      m_stepon = false;
      m_planeoff = true;
      m_cmsoff = true;
      m_laststepoff_timesec = 0;
      m_fit_ongoing_flag = false;

      m_beammessage_counter = 0;
      
      //create timer for regular cms and plane off check
      m_timer = toolbox::task::getTimerFactory()->createTimer( "OFFCheck_" + getApplicationDescriptor()->getURN() );

      toolbox::net::URN memurn("toolbox-mem-pool","VDMMonitor_mem");
      m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
      try{
	toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
	m_memPool = m_poolFactory->createPool(memurn,allocator);
      }catch(xcept::Exception& e){
	std::stringstream msg;
	msg<<"Failed to setup memory pool "<<stdformat_exception_history(e);
	LOG4CPLUS_FATAL(getApplicationLogger(),msg.str());
      }
      
    }
    
    void VDMMonitor::declareFlashlists(){
      std::list<std::string>::const_iterator it = m_vdmshape_flashfields.begin();
      std::vector<xdata::Serializable*>::iterator valit = m_vdmshape_flashcontents.begin();
      for(;  it!=m_vdmshape_flashfields.end() && valit!=m_vdmshape_flashcontents.end(); ++it, ++valit){
	m_IS_vdmmon->fireItemAvailable(*it,*valit);	
      }
      
      it = m_vdmsigma_flashfields.begin();
      valit = m_vdmsigma_flashcontents.begin();
      for(; it!=m_vdmsigma_flashfields.end() && valit!=m_vdmsigma_flashcontents.end(); ++it, ++valit){
	m_IS_vdmsigmamon->fireItemAvailable(*it,*valit);
      }
    }
    
    void VDMMonitor::actionPerformed(xdata::Event& e){
      //LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
      if( e.type() == "urn:xdaq-event:setDefaultValues" ){        //configuration parsing finished callback
	this->getEventingBus(m_bus.value_).addActionListener(this);
	//subscribe to mandatory eventing topics
	this->getEventingBus(m_bus.value_).subscribe(m_vdmflagTopic.value_);
	this->getEventingBus(m_bus.value_).subscribe(m_vdmscanTopic.value_);
	this->getEventingBus(m_bus.value_).subscribe(m_beamTopic.value_);
	this->getEventingBus(m_bus.value_).subscribe(m_bunchlengthTopic.value_);
	addActionListener(this);
	
	//start cms/plane off timer
	toolbox::TimeInterval interval( 1, 0 ); // check cms off every sec
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday()+toolbox::TimeInterval(5,0);
	try{
	  m_timer->scheduleAtFixedRate(start,this,interval,0,"off_timer" );
	}catch( toolbox::task::exception::Exception& e ){
	  LOG4CPLUS_FATAL(getApplicationLogger()," "+stdformat_exception_history(e));
	}
	
	resetStepSumBeam();
	if( !m_lumiTopicStr.value_.empty() ){ //initialize luminometer caches, and subscribe to the topic
	  //prepare data caches and subscribe to all lumitopics     
	  m_lumitopics = toolbox::parseTokenSet(m_lumiTopicStr.value_,",");
	  for( std::set<std::string>::iterator it = m_lumitopics.begin(); it!=m_lumitopics.end(); ++it){
	    std::string lumitopicname = *it;	    
	    std::vector< ShapeData > shapes;
	    m_shapedata_cache.insert( std::make_pair(lumitopicname, shapes ) ) ;
	    m_sigmadata_cache.insert( std::make_pair(lumitopicname, SigmaData() ) ) ;
	    std::vector< std::vector< float > > steprates;  
	    m_stepsum_rate.insert( std::make_pair(lumitopicname, steprates) );

	    std::vector< StepData > steps;
	    m_stepcache_luminometer.insert( std::make_pair(lumitopicname, steps) );	    
	    this->getEventingBus(m_bus.value_).subscribe(*it);
	  }	  
	}// end loop over lumi topics		
      }
    }

    void VDMMonitor::clearAllCache(){
      resetStepSumBeam();
      resetStepSumRate();
      resetStepCache();
      resetShapeCache();
      resetSigmaCache();     
    }

    void VDMMonitor::resetStepSumBeam(){
      m_stepsum_beam1intensities.clear();
      m_stepsum_beam2intensities.clear();
      m_stepsum_beam1intensities.resize(3564,0);
      m_stepsum_beam2intensities.resize(3564,0);
      m_beammessage_counter = 0 ;
    }

    void VDMMonitor::resetStepSumRate(){

      for(std::map< std::string, std::vector< std::vector<float> > >::iterator it=m_stepsum_rate.begin(); it!=m_stepsum_rate.end(); ++it){
	it->second.clear();
      }
     
    }

    int VDMMonitor::planeNameToId( const std::string& name )const{
      if(name=="CROSSING"){
	return 1;
      }
      if(name=="SEPARATION"){
	return 2;
      }
      return 0;
    }

    std::string VDMMonitor::planeIdToString( int id ) const{
      std::string planename = "UNKNOWN";
      if( id==1 ) planename="X";
      if( id==2 ) planename="Y";
      return planename;
    }
    
    void VDMMonitor::actionPerformed( toolbox::Event& e ){
      //LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received toolbox event " + e.type());   
      if ( e.type() == "eventing::api::BusReadyToPublish" ){
	m_canpublish = true;
      }
      if( e.type() == "urn:bril-vdmmonitor-event:StepOn" ){
	LOG4CPLUS_INFO(getApplicationLogger(), "StepOn, enable data collection");
	m_stepon = true;	    
	if(m_planeoff) {
	  m_planeoff = false;//on first stepon switch off planeoff
	}

      }else if( e.type() == "urn:bril-vdmmonitor-event:StepOff" ){
	LOG4CPLUS_INFO(getApplicationLogger(), "StepOff, disable data collection");
	m_stepon = false; 
	toolbox::TimeVal now=toolbox::TimeVal::gettimeofday();
	{//start lock scope
	toolbox::task::Guard<toolbox::BSem> g(m_applock);
	std::map< std::string, std::vector< StepData > >::iterator it;
	for( it=m_stepcache_luminometer.begin(); it!=m_stepcache_luminometer.end(); ++it  ){
	  // copy current beam, lumi cache to each detectors' this step StepData
	  std::string topicname = it->first;
	  StepData thisstep;
	  thisstep.timestampsec = now.sec();
	  thisstep.plane = planeNameToId(m_scandata.plane);
	  thisstep.sep = m_scandata.nominal_separation; 
	  thisstep.beam1intensity.reserve( m_stepsum_beam1intensities.size() );
	  thisstep.beam2intensity.reserve( m_stepsum_beam2intensities.size() );
	  thisstep.rate.reserve( 3564 );
	  thisstep.rateerror.reserve( 3564 );

	  if( m_beammessage_counter!=0 ){
	    thisstep.n_beamintensity_messages = m_beammessage_counter;

	    for(std::vector< float >::iterator rIt=m_stepsum_beam1intensities.begin(); rIt!=m_stepsum_beam1intensities.end(); ++rIt){
	      //*rIt = *rIt/m_beammessage_counter; //b1 normalization
	      thisstep.beam1intensity.push_back(*rIt/m_beammessage_counter);
	    }

	    for(std::vector< float >::iterator rIt=m_stepsum_beam2intensities.begin(); rIt!=m_stepsum_beam2intensities.end(); ++rIt){
	      //*rIt = *rIt/m_beammessage_counter; //b2 normalization
	      thisstep.beam2intensity.push_back(*rIt/m_beammessage_counter);
	    }

	    //std::copy( m_stepsum_beam1intensities.begin(),  m_stepsum_beam1intensities.end() , back_inserter(thisstep.beam1intensity) );
	    //std::copy( m_stepsum_beam2intensities.begin(),  m_stepsum_beam2intensities.end() , back_inserter(thisstep.beam2intensity) );

	    size_t nratemsg=m_stepsum_rate[topicname].size();
	    if( nratemsg!=0 ){
	      thisstep.n_rate_messages = nratemsg;
	      std::vector< float > ratesum_result;
	      ratesum_result.resize(3564,0.0);
	      
	      for( std::vector< std::vector<float> >::iterator msgIt=m_stepsum_rate[topicname].begin(); msgIt!=m_stepsum_rate[topicname].end(); ++msgIt ){		
		size_t bxidx = 0;
		for( std::vector< float >::iterator bxIt = msgIt->begin(); bxIt != msgIt->end(); ++bxIt, ++bxidx ){
		  ratesum_result[bxidx] += *bxIt;
		}
	      }

	      for(std::vector< float >::iterator rIt=ratesum_result.begin(); rIt!=ratesum_result.end(); ++rIt){
		*rIt = *rIt/nratemsg; //rate average needed for std calculation		  
	      }
	      
	      std::vector< float > rateerror_result;  rateerror_result.resize(3564,0.0);
	      for( std::vector< std::vector<float> >::iterator msgIt=m_stepsum_rate[topicname].begin(); msgIt!=m_stepsum_rate[topicname].end(); ++msgIt ){
		size_t bxidx = 0;		
		for( std::vector< float >::iterator bxIt = msgIt->begin(); bxIt != msgIt->end(); ++bxIt, ++bxidx ){
		  float delta = ratesum_result[bxidx] - (*msgIt)[bxidx]; // prep. for rates std. calculation		 
		  delta = delta*delta;
		  rateerror_result[bxidx] += delta;
		}
	      }
	      
	      for(std::vector< float >::iterator rIt=rateerror_result.begin(); rIt!=rateerror_result.end(); ++rIt){
		*rIt = std::sqrt(*rIt/nratemsg); //rate error = std.		  
	      }

	      std::copy( ratesum_result.begin(),  ratesum_result.end() , back_inserter(thisstep.rate) );
	      std::copy( rateerror_result.begin(),  rateerror_result.end() , back_inserter(thisstep.rateerror) );
	      it->second.push_back(thisstep);
	      //publish vdmstep for this detector for this step to eventing	  
	      do_publishToEventing( topicname, std::string(m_scandata.plane), m_scandata.step, m_scandata.fillnum, m_scandata.runnum, thisstep );
	      
	    }else{//end if nratemsg!=0
	      LOG4CPLUS_ERROR(getApplicationLogger(), "No rate messages for "+topicname+" in current step");	  
	    }  
	  }else{
	    LOG4CPLUS_ERROR(getApplicationLogger(), "No beam messages in current step");
	  }//end if m_beammessage_counter!=0 	

	}//loop over topic
	
	// reset beam and rate cache after copying
	resetStepSumBeam();
	resetStepSumRate();
	}//end lock

	m_laststepoff_timesec = toolbox::TimeVal::gettimeofday().sec();	
	
      }else if( e.type() == "urn:bril-vdmmonitor-event:PlaneOff" ){
	LOG4CPLUS_INFO(getApplicationLogger(), "Current PlaneOff, launch shape fitting");
	m_planeoff = true;
	m_fitting_wl->submit( m_as_fitting );		
      }else if(  e.type() == "urn:bril-vdmmonitor-event:CMSOff" ){
	LOG4CPLUS_INFO(getApplicationLogger(), "CMSOff, launch sigma analyzing");
	m_cmsoff = true;
	m_previousPlane = 0; //reset all to initial states on CMSOffEvent
	m_planeoff = true;
	m_stepon = false;
	m_laststepoff_timesec = 0;
	m_analyzing_wl->submit( m_as_analyzing ) ;
      }      
    }

    void VDMMonitor::Default( xgi::Input* in, xgi::Output* out ){
    }
    
    void VDMMonitor::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist){
      toolbox::mem::AutoReference refguard(ref);
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if( action == "notify" && ref!=0 ){      
	std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	//std::string originator = plist.getProperty("ORIGINATOR");//zx: to remove	
	std::string payloaddict = plist.getProperty("PAYLOAD_DICT");

	LOG4CPLUS_DEBUG(getApplicationLogger(), "Received topic " +topic);
	
	if( topic == m_vdmflagTopic.value_ ){
	  std::stringstream ss;
	  interface::bril::vdmflagT* me = (interface::bril::vdmflagT*)( ref->getDataLocation() );
	  m_previousAcqflag = m_vdmflag;
	  m_vdmflag = me->payload()[0];//flag is acq flag
	  ss<<"current vdmflag "<< m_vdmflag<<" previous vdmflag "<<m_previousAcqflag;
	  LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());

	  m_cmsoff = false; //on receiving message turn cmsoff to false
	  if( m_vdmflag==false&&m_previousAcqflag==true ){  //fire STEP OFF event
	    bril::vdmmonitor::StepOffEvent myevent;
	    this->fireEvent( myevent ) ;
	  }else if( m_vdmflag==true&&m_previousAcqflag==false ){//fire STEP ON event
	    bril::vdmmonitor::StepOnEvent myevent;
	    this->fireEvent( myevent );	    
	  }
	  
	}else if( topic == m_vdmscanTopic.value_ ){
	  std::stringstream ss;
	  interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 	  
	  
	  //fillnum and runnum are in headed, so tc(payloaddict) is now needed
	  m_scandata.fillnum = thead->fillnum;
	  m_scandata.runnum = thead->runnum;

	  interface::bril::shared::CompoundDataStreamer tc(payloaddict);

	  tc.extract_field( &m_scandata.ip, "ip", thead->payloadanchor ); 
	  tc.extract_field( &m_scandata.step, "step", thead->payloadanchor );
	  tc.extract_field( &m_scandata.progress, "progress", thead->payloadanchor );
	  tc.extract_field( &m_scandata.beam, "beam", thead->payloadanchor );
	  tc.extract_field( m_scandata.plane, "nominal_sep_plane", thead->payloadanchor ); //nominal_sep_plane:str16:1
	  tc.extract_field( m_scandata.stat, "stat", thead->payloadanchor ); //stat:str16:1
	  tc.extract_field( &m_scandata.nominal_separation, "sep", thead->payloadanchor ); //sep:float:1
	  tc.extract_field( &m_scandata.read_nominal_B1sepPlane, "r_sepP1", thead->payloadanchor ); //r_sepP1:float:1 
	  tc.extract_field( &m_scandata.read_nominal_B1xingPlane, "r_xingP1", thead->payloadanchor );
	  tc.extract_field( &m_scandata.read_nominal_B2sepPlane, "r_sepP2", thead->payloadanchor );
	  tc.extract_field( &m_scandata.read_nominal_B2xingPlane, "r_xingP2", thead->payloadanchor );
	  tc.extract_field( &m_scandata.set_nominal_B1sepPlane, "s_sepP1", thead->payloadanchor );
	  tc.extract_field( &m_scandata.set_nominal_B1xingPlane, "s_xingP1", thead->payloadanchor );
	  tc.extract_field( &m_scandata.set_nominal_B2sepPlane, "s_sepP2", thead->payloadanchor );
	  tc.extract_field( &m_scandata.set_nominal_B2xingPlane, "s_xingP2", thead->payloadanchor );	
	  tc.extract_field( &m_scandata.read_beta_star, "bstar5", thead->payloadanchor ); //bstar5:int32:1
	  tc.extract_field( &m_scandata.read_crossing_angle, "xingHmurad", thead->payloadanchor ); //xingHmurad:int32:1	
	  
	  std::string currentplane = std::string(m_scandata.plane);
	  std::string currentstatus = std::string(m_scandata.stat);
	  int currentprogress = int(m_scandata.progress);

	  if (int(m_scandata.step)!=9999) {
	    ss<<" step "<<int(m_scandata.step)<<" progress "<<currentprogress<<" plane "<<currentplane << " status "<< currentstatus;
	    LOG4CPLUS_INFO(getApplicationLogger(), ss.str() );
	  }
	  
	  if( (m_cmsoff==false) && (m_planeoff==false) && (m_previousPlane!=planeNameToId(currentplane)) ){
	    LOG4CPLUS_DEBUG(getApplicationLogger(), "fire PlaneOffEvent due to plane change" );
	    bril::vdmmonitor::PlaneOffEvent myevent;
	    this->fireEvent( myevent );
	    m_planeoff = true;	    
	  }	   

	  m_previousPlane = planeNameToId(currentplane);//save current plane as previous

	}else if( topic == m_beamTopic.value_ ) {
	  //if collecting data, collect beam data
	    if( m_stepon ){
	      interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 	  
	      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
	      float b1[3564];
	      float b2[3564];
	      tc.extract_field( b1, "bxintensity1", thead->payloadanchor );
	      tc.extract_field( b2, "bxintensity2", thead->payloadanchor );
	      {//start lock
	      toolbox::task::Guard<toolbox::BSem> g(m_applock);     
	      for(size_t i=0;i<3564;++i){
		m_stepsum_beam1intensities[i] += b1[i];
		m_stepsum_beam2intensities[i] += b2[i];
	      }	      
	      m_beammessage_counter++;
	      }//end lock
	    }


	}else if( topic == m_bunchlengthTopic.value_ ) {
	  //if collecting data, collect bunchlength data
		 std::stringstream ss;
	     if( m_stepon ){
	      interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 	  
	      interface::bril::shared::CompoundDataStreamer tc(payloaddict);
	      tc.extract_field( m_scandata.b1_bxlength, "b1length", thead->payloadanchor ); // b1length:float:3564
	      tc.extract_field( m_scandata.b2_bxlength, "b2length", thead->payloadanchor ); // b2length:float:3564  

	    	//if (int(m_scandata.step)!=9999) {
	    	//ss<<"b1length[100] "<<float(m_scandata.b1_bxlength[100])<<" b1length[425] "<<float(m_scandata.b1_bxlength[425])<<" b1length[2240] "<<float(m_scandata.b1_bxlength[2240]) << "b2length[100] "<<float(m_scandata.b2_bxlength[100])<<" b2length[425] "<<float(m_scandata.b2_bxlength[425])<<" b2length[2240] "<<float(m_scandata.b2_bxlength[2240]);
	    	//LOG4CPLUS_INFO(getApplicationLogger(), ss.str() );
			//}
	    }

	}else if( m_lumitopics.find(topic)!=m_lumitopics.end() ){
	  // if collecting data, collect detector lumi data
	  if( m_stepon ){
	    interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation()); 	
	    interface::bril::shared::CompoundDataStreamer tc(payloaddict);
	    float rate[3564];	      
	    tc.extract_field( rate, "bxraw", thead->payloadanchor );
	    std::map< std::string, std::vector< std::vector< float > > >::iterator it = m_stepsum_rate.find(topic);	    	      
            {//start lock	  
              toolbox::task::Guard<toolbox::BSem> g(m_applock);
	      std::vector<float> rvector(rate, rate+3564);
	      it->second.push_back(rvector);
	    } //end lock
	  }//end if m_stepon
	}
      }// end action notify     
    }     
    
    bool VDMMonitor::isEmptyStepCache()const{
      int nonemptyluminometercount=0;
      if(m_stepcache_luminometer.size()==0) return true;
      for(std::map< std::string, std::vector< StepData > >::const_iterator it=m_stepcache_luminometer.begin(); it!=m_stepcache_luminometer.end();++it){
	if( it->second.size()>0 ){
	  ++nonemptyluminometercount;
	}
      }
      if(nonemptyluminometercount>0) return false;
      return true;
    }  

    bool VDMMonitor::isEmptyShapeCache()const{
      int nonemptyluminometershapecount=0;
      if(m_shapedata_cache.size()==0) return true;
      for(std::map< std::string, std::vector< ShapeData > >::const_iterator it=m_shapedata_cache.begin(); it!=m_shapedata_cache.end();++it){
	if( it->second.size()>0 ){
	  ++nonemptyluminometershapecount;
	}
      }
      if(nonemptyluminometershapecount>0) return false;
      return true;
    }      

    bool VDMMonitor::fitting( toolbox::task::WorkLoop* wl ){      
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering fitting");
      //
      // fit StepData cache to get ShapeData for the current scan plane
      //
      // push Shapedata into cache m_shapedata_cache
      // clear StepData cache after usage
      //
      //
      if( isEmptyStepCache() ){
	LOG4CPLUS_ERROR(getApplicationLogger(), "ERROR: NOTHING TO FIT");
	m_fit_ongoing_flag = false;
	return false;
      }
      m_fit_ongoing_flag = true; 

      std::stringstream ss;
      std::map< std::string, std::vector< StepData > >::iterator stepIt;
      for( stepIt=m_stepcache_luminometer.begin(); stepIt!=m_stepcache_luminometer.end(); ++stepIt ){ //loop over detectors
	std::string luminame = stepIt->first;
	if( stepIt->second.size()==0 ) {
	  LOG4CPLUS_INFO(getApplicationLogger(), "Skip fitting "+luminame);
	  continue;
	}
	std::vector< StepData > thisstep = stepIt->second;
	
	//input vectors
	std::vector< std::vector< float > > bxrates; //3564 of n steps
	std::vector< std::vector< float > > bxrateserr; //3564 of n steps
	std::vector< std::vector< float > > bxb1; //3564 of n steps
	std::vector< std::vector< float > > bxb2; //3564 of n steps

	std::vector< float > separations; // nominal separations of n steps

	//outputs 
	ShapeData thisshape;
	thisshape.plane = thisstep.front().plane;
	thisshape.fillnum = m_scandata.fillnum;
	thisshape.runnum = m_scandata.runnum;
	thisshape.crossing_angle = m_scandata.read_crossing_angle;
	thisshape.beta_star = m_scandata.read_beta_star;
	thisshape.timestampsec_begin = thisstep.front().timestampsec;
	thisshape.timestampsec_end = thisstep.back().timestampsec;
	// fill inputs
	for(std::vector<  StepData >::iterator sIt=thisstep.begin(); sIt!=thisstep.end(); ++sIt){
	  separations.push_back( sIt->sep );
	}
	for(size_t bx=0; bx<3564; ++bx){
	  std::vector<float> steprate;
	  std::vector<float> steprateerror;
	  std::vector<float> stepb1;
	  std::vector<float> stepb2;	  	  
	  std::vector< StepData >::iterator it;
	  for(it=thisstep.begin(); it!=thisstep.end(); ++it){
	    //float nbeammsg = it->n_beamintensity_messages;
	    //float nratemsg = it->n_beamintensity_messages;
	    steprate.push_back( it->rate[bx] );
	    steprateerror.push_back( it->rateerror[bx] );
	    stepb1.push_back( it->beam1intensity[bx] );
	    stepb2.push_back( it->beam2intensity[bx] );
	    //stepb1.push_back( it->beam1intensity[bx]/nbeammsg ); //already normalized wnhen copied to StepData
	    //stepb2.push_back( it->beam2intensity[bx]/nbeammsg );
	  }//end loop step
	  bxrates.push_back(steprate);
	  bxrateserr.push_back(steprateerror);
	  bxb1.push_back(stepb1);
	  bxb2.push_back(stepb2);
	}//end loop bx

	//std::cout << "bxrates.size() " << bxrates.size() << "bxrateserr.size() " << bxrateserr.size() <<'\n';

	if( separations.size() < 3 ){ // emit. scan is 7-9 points usually
	LOG4CPLUS_ERROR(getApplicationLogger(), "ERROR: NOT FITTING, LESS THAN 3 STEPS FOUND");
	resetStepCache();
	m_fit_ongoing_flag = false;
	return false;
      }

	bril::vdmmonitor::fit_vector(separations, bxrates, bxrateserr, bxb1,bxb2,thisshape.bxproductb1b2,thisshape.bxwidth,thisshape.bxwidth_err,thisshape.bxpeak,thisshape.bxpeak_err,thisshape.bxmean,thisshape.bxmean_err,thisshape.avgwidth,thisshape.avgwidth_err,thisshape.avgpeak,thisshape.avgpeak_err,thisshape.avgmean,thisshape.avgmean_err);
	ss<<"Shape for "<<luminame<<" : "<<thisshape.toString();
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());	
	
	m_shapedata_cache[luminame].push_back(thisshape); 


	//fill mon infospace for shapeflashlist
	//m_IS_vdmmon->lock();
	xdata::String* p_plane = dynamic_cast< xdata::String* >( m_IS_vdmmon->find("plane") );
	xdata::String* p_detectorname = dynamic_cast< xdata::String* >( m_IS_vdmmon->find("detectorname") );
	xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_IS_vdmmon->find("fillnum") );
	xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_IS_vdmmon->find("runnum") );
	xdata::TimeVal* p_timestamp_begin = dynamic_cast< xdata::TimeVal* >( m_IS_vdmmon->find("timestamp_begin") );
	xdata::TimeVal* p_timestamp_end = dynamic_cast< xdata::TimeVal* >( m_IS_vdmmon->find("timestamp_end") );
	xdata::Float* p_avgwidth = dynamic_cast< xdata::Float* >( m_IS_vdmmon->find("avgwidth") );	
	xdata::Float* p_avgwidth_err = dynamic_cast< xdata::Float* >( m_IS_vdmmon->find("avgwidth_err") );
	xdata::Float* p_avgpeak = dynamic_cast< xdata::Float* >( m_IS_vdmmon->find("avgpeak") );
	xdata::Float* p_avgpeak_err = dynamic_cast< xdata::Float* >( m_IS_vdmmon->find("avgpeak_err") );
	xdata::Float* p_avgmean = dynamic_cast< xdata::Float* >( m_IS_vdmmon->find("avgmean") );
	xdata::Float* p_avgmean_err = dynamic_cast< xdata::Float* >( m_IS_vdmmon->find("avgmean_err") );
	xdata::Vector< xdata::Float >* p_bxproductb1b2 = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxproductb1b2"));
	xdata::Vector< xdata::Float >* p_bxwidth = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxwidth"));
	xdata::Vector< xdata::Float >* p_bxwidth_err = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxwidth_err"));
	xdata::Vector< xdata::Float >* p_bxpeak = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxpeak") );
	xdata::Vector< xdata::Float >* p_bxpeak_err = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxpeak_err") );
	xdata::Vector< xdata::Float >* p_bxmean = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxmean") );
	xdata::Vector< xdata::Float >* p_bxmean_err = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmmon->find("bxmean_err") );
       
	p_plane->value_ = planeIdToString(thisshape.plane);
	p_detectorname->value_ =  luminame;
	p_fillnum->value_ =  thisshape.fillnum;
	p_runnum->value_ =  thisshape.runnum;
	p_timestamp_begin->value_.sec( thisshape.timestampsec_begin );
	p_timestamp_end->value_.sec( thisshape.timestampsec_end );
	p_avgwidth->value_ = thisshape.avgwidth;
	p_avgwidth_err->value_ = thisshape.avgwidth_err;
	p_avgpeak->value_ = thisshape.avgpeak;
	p_avgpeak_err->value_ = thisshape.avgpeak_err;
	p_avgmean->value_ = thisshape.avgmean;
	p_avgmean_err->value_ = thisshape.avgmean_err;
	p_bxproductb1b2->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxproductb1b2.begin(); it!=thisshape.bxproductb1b2.end(); ++it){
	    p_bxproductb1b2->push_back( xdata::Float(*it) );
	}
	p_bxwidth->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxwidth.begin(); it!=thisshape.bxwidth.end(); ++it){
	    p_bxwidth->push_back( xdata::Float(*it) );
	}
	p_bxwidth_err->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxwidth_err.begin(); it!=thisshape.bxwidth_err.end(); ++it){
	    p_bxwidth_err->push_back( xdata::Float(*it) );
	}
	p_bxpeak->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxpeak.begin(); it!=thisshape.bxpeak.end(); ++it){
	    p_bxpeak->push_back( xdata::Float(*it) );
	}
	p_bxpeak_err->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxpeak_err.begin(); it!=thisshape.bxpeak_err.end(); ++it){
	  p_bxpeak_err->push_back( xdata::Float(*it) );
	}
	p_bxmean->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxmean.begin(); it!=thisshape.bxmean.end(); ++it){
	    p_bxmean->push_back( xdata::Float(*it) );
	}
	p_bxmean_err->clear();
	for( std::vector<float>::const_iterator it = thisshape.bxmean_err.begin(); it!=thisshape.bxmean_err.end(); ++it){
	  p_bxmean_err->push_back( xdata::Float(*it) );
	}
	//fire mon infospace for shapeflashlist

	try{
	  ss.clear(); ss.str("");
	  ss<<"Firing VDMShape flashlist for "<<luminame<<" plane "<<thisshape.plane<<" avgpeak "<<thisshape.avgpeak << " beta star "<< thisshape.beta_star << " XAngle " << thisshape.crossing_angle;
	  LOG4CPLUS_INFO( getApplicationLogger(),ss.str() );
	  m_IS_vdmmon->fireItemGroupChanged(m_vdmshape_flashfields,this);      // fire flashlist vdmshape
	}catch(xdata::exception::Exception& e){
	  std::string msg("Failed to fire vdmshape flashlist ");
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
	  XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,msg,e);
	  notifyQualified("error",myerrorobj);
	}
	ss.clear(); ss.str("");
	ss<<"End VDMShape flashlist for "<<luminame;
	LOG4CPLUS_DEBUG( getApplicationLogger(),ss.str() );
	ss.clear(); ss.str("");
	//m_IS_vdmmon->unlock();
      }//end loop detector


      resetStepCache();
      m_fit_ongoing_flag = false;
      return false;
    }

    void VDMMonitor::resetStepCache(){
      std::map< std::string, std::vector< StepData > >::iterator it;
      for(it=m_stepcache_luminometer.begin(); it!=m_stepcache_luminometer.end(); ++it){
	it->second.clear();
      }
    }

    void VDMMonitor::resetShapeCache(){
      std::map< std::string, std::vector< ShapeData > >::iterator it;
      for(it=m_shapedata_cache.begin(); it!=m_shapedata_cache.end(); ++it){
	it->second.clear();
      }
    }
    
    void  VDMMonitor::resetSigmaCache(){
      std::map< std::string, SigmaData >::iterator it;
      for(it=m_sigmadata_cache.begin(); it!=m_sigmadata_cache.end(); ++it){
	SigmaData s;
	it->second = s; 
      }
    }
    
    bool VDMMonitor::analyzing( toolbox::task::WorkLoop* wl ){      
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering analyzing");
      usleep(m_sleepBeforeSigma.value_); //5000000 s
      //
      // analyze vector<ShapeData> to get SigmaData for the whole CMS scan
      //
      // push sigma into cache m_sigmadata_cache 
      // fire flashlists then clean all cache

      if (m_fit_ongoing_flag == true) {
	return true; // if fiting is ongoing analyzing wl statrts from the beginning, sleep, checks 
      }

      std::stringstream ss;

      std::map< std::string, std::vector< ShapeData > >::iterator shapeCacheIt=m_shapedata_cache.begin();
      for( ; shapeCacheIt!=m_shapedata_cache.end(); ++shapeCacheIt ){
	std::string luminame = shapeCacheIt->first;
	std::vector< ShapeData > shapes = shapeCacheIt->second;
	ss.clear(); ss.str("");
	ss<<"NUMBER OF SHAPES for "<<luminame<<shapes.size();
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());

	//std::cout << "BEFORE entered  debug shapes loop..."<< '\n';
	//debug looop for shapes
	std::vector< ShapeData >::iterator it1= shapes.begin();
	//std::cout<< " entered  debug shapes loop..."<< '\n';
	ShapeData xShape1;
	ShapeData yShape1;	
	
	int xcount1 = 0;
	int ycount1 = 0;
	for(; it1!=shapes.end(); ++it1){
	  //std::cout << " entered for it1!=shapes.end(); ..."<< '\n';
	  if(it1->plane==1){
	    xShape1 = *it1;
	    ++xcount1;
	    //std::cout << " One of the shapes is Crossing "<< '\n';
	    //std::cout << "### Crossing angle is "<< it1->crossing_angle << " and beta* is "<< it1->beta_star<< '\n';
	  }else if( it1->plane==2 ){
	    yShape1 = *it1;
	    ++ycount1;
	    //std::cout << " One of the shapes is Separation "<< '\n';
	  }else {
	    LOG4CPLUS_ERROR(getApplicationLogger(), "One of the shapes is not X and not Y");
	    //std::cout << " One of the shapes is not X and not Y "<< '\n';
	  }	  
	}// end loop shape


	if( shapes.size()!=2 ){
	  ss.clear();ss.str("");
	  ss<<"ERROR: "<<luminame<<" : scan pair not found, shapes size !=2, do nothing";
	  LOG4CPLUS_ERROR(getApplicationLogger(), ss.str());
	  continue;	  
	}
	
	std::vector< ShapeData >::iterator it= shapes.begin();
	ShapeData xShape;
	ShapeData yShape;
	SigmaData outputSigma;	
	
	int xcount = 0;
	int ycount = 0;

	for(; it!=shapes.end(); ++it){
	  
	  if(it->plane==1){
	    xShape = *it;
	    ++xcount;
	  }else if( it->plane==2 ){
	    yShape = *it;
	    ++ycount;
	  }	  
	}// end loop shape
	if(xcount!=1 || ycount!=1){
	  ss.clear();ss.str("");
	  ss<<"ERROR: "<<luminame<<" two shapes are not a X,Y pair, do nothing";
	  LOG4CPLUS_ERROR(getApplicationLogger(), ss.str());
	  continue;	  
	}

	outputSigma.fillnum = shapes.front().fillnum;
	outputSigma.runnum = shapes.front().runnum;
	outputSigma.timestampsec = shapes.back().timestampsec_end;

	float result = 0.0;
	float pileup_aver_xyshape = 0.0;
	float result_err = 0.0;
	int non_zero_fits = 0;
	float var_bx_emittX = 0.0;
	float var_bx_emittY = 0.0;
	float var_bx_emittX_err = 0.0;
	float var_bx_emittY_err = 0.0;
	float tmpvar_avgbxlength_sec = 0.0;
	float tmpvar_avgbxlength_m = 0.0;
	float var_longitudinal_term = 0.0;
	float gama = 6930.0; // gama=E/m_proton=6500GeV/0.938GeV


	for(size_t bx=0; bx<3564; ++bx){
	  if (xShape.bxwidth[bx]!=0 && yShape.bxwidth[bx]!=0 && xShape.bxpeak[bx]!=0 && yShape.bxpeak[bx]!=0 && xShape.bxwidth_err[bx]!=0 && yShape.bxwidth_err[bx]!=0 && xShape.bxpeak_err[bx]!=0 && yShape.bxpeak_err[bx]!=0){
	    result = M_PI*xShape.bxwidth[bx]*yShape.bxwidth[bx]*(xShape.bxpeak[bx]+yShape.bxpeak[bx]);
	    
	    // PILEUP = Peak_Rate*Sigma_inclusive/sigma_vis, where Sigma_inclusive = 80mb or as here sigma_vis is in barns, 0.08b, Peak_Rate=Fit_peak*bunch_current, as Fit_peak is nirmalized and we need un-normalized rate
	    pileup_aver_xyshape = (((xShape.bxpeak[bx]*xShape.bxproductb1b2[bx]*0.08)/result)+((yShape.bxpeak[bx]*yShape.bxproductb1b2[bx]*0.08)/result))/2;

	  // xsec =  pi * CapSigmaX[0] * CapSigmaY[0] * (peakX[0] + peakY[0])
	  // xsecErr = ( CapSigmaX[1]*CapSigmaX[1]/CapSigmaX[0]/CapSigmaX[0] +CapSigmaY[1]*CapSigmaY[1]/CapSigmaY[0]/CapSigmaY[0] + (peakX[1]*peakX[1] + peakY[1]*peakY[1])/(peakX[0]+peakY[0])/(peakX[0]+peakY[0]))
	  // xsecErr = math.sqrt(xsecErr) * xsec

	    result_err = std::sqrt(xShape.bxwidth_err[bx]*xShape.bxwidth_err[bx]/(xShape.bxwidth[bx]*xShape.bxwidth[bx]) + yShape.bxwidth_err[bx]*yShape.bxwidth_err[bx]/(yShape.bxwidth[bx]*yShape.bxwidth[bx]) + (xShape.bxpeak_err[bx]*xShape.bxpeak_err[bx] + yShape.bxpeak_err[bx]*yShape.bxpeak_err[bx])/((xShape.bxpeak[bx] + yShape.bxpeak[bx])*(xShape.bxpeak[bx] + yShape.bxpeak[bx])) )*result;
		
		// b1_bxlength[non_zero_fits] and NOT b1_bxlength[bx], as values in DIP are just dumped, they do not correcpond to filling scheme
		tmpvar_avgbxlength_sec = (m_scandata.b1_bxlength[non_zero_fits]+m_scandata.b2_bxlength[non_zero_fits])/2;
		tmpvar_avgbxlength_m = (tmpvar_avgbxlength_sec*300000000)/4;
		//crossing_angle*0.000001 -- as crossing_angle is in uRad
		var_longitudinal_term = 2*gama*tmpvar_avgbxlength_m*tmpvar_avgbxlength_m*xShape.crossing_angle*0.000001*xShape.crossing_angle*0.000001;

		//beta_star[cm]*0.01 to have it in [m], xShape.bxwidth[bx][mm]*0.001 to have it in [m]
		// emittance in X has +0.5 um correction factor based on Michi's simylation and paper
	    var_bx_emittX = (((((xShape.bxwidth[bx]*0.001*xShape.bxwidth[bx]*0.001*gama)-var_longitudinal_term)/(2*xShape.beta_star*0.01))*1000000)+0.5); // *1000000 to have it in um
	    var_bx_emittY = ((yShape.bxwidth[bx]*0.001*yShape.bxwidth[bx]*0.001*gama)/(2*yShape.beta_star*0.01))*1000000; // *1000000 to have it in um
	    //var_bx_emittX_err = (((xShape.bxwidth_err[bx]*0.001*xShape.bxwidth_err[bx]*0.001*gama)-var_longitudinal_term)/(2*xShape.beta_star*0.01))*1000000; // *1000000 to have it in um
	    var_bx_emittX_err = ((xShape.bxwidth_err[bx]*0.001*xShape.bxwidth_err[bx]*0.001*gama)/(2*xShape.beta_star*0.01))*1000000; // *1000000 to have it in um
	    var_bx_emittY_err = ((yShape.bxwidth_err[bx]*0.001*yShape.bxwidth_err[bx]*0.001*gama)/(2*yShape.beta_star*0.01))*1000000; // *1000000 to have it in um

	    ++non_zero_fits;

	  }else {
	    result=0.0;
	    result_err=0.0;
	    pileup_aver_xyshape = 0.0;
	    var_bx_emittX = 0.0;
	    var_longitudinal_term = 0.0;
	    var_bx_emittY = 0.0;
	    var_bx_emittX_err = 0.0;
	    var_bx_emittY_err = 0.0;

	  }
	  outputSigma.bxsigma[bx] = result;
	  outputSigma.bxsigmaerror[bx] = result_err;
	  outputSigma.pileup[bx] = pileup_aver_xyshape;
	  outputSigma.bxemittanceX[bx] = var_bx_emittX;
	  outputSigma.bxlongitudinal_term[bx] = var_longitudinal_term;
	  outputSigma.bxemittanceY[bx] = var_bx_emittY;
	  outputSigma.bxemittanceX_err[bx] = var_bx_emittX_err;
	  outputSigma.bxemittanceY_err[bx] = var_bx_emittY_err;
	}
	
	outputSigma.avgsigma = accumulate(outputSigma.bxsigma.begin(), outputSigma.bxsigma.end(), 0.0);
	//outputSigma.avgsigma = outputSigma.avgsigma/outputSigma.bxsigma.size();
	outputSigma.avgsigma = outputSigma.avgsigma/non_zero_fits;

	outputSigma.avgsigmaerror = accumulate(outputSigma.bxsigmaerror.begin(), outputSigma.bxsigmaerror.end(), 0.0);
	//outputSigma.avgsigmaerror = outputSigma.avgsigmaerror/outputSigma.bxsigmaerror.size();
	outputSigma.avgsigmaerror = outputSigma.avgsigmaerror/non_zero_fits;

	outputSigma.avgpileup = accumulate(outputSigma.pileup.begin(), outputSigma.pileup.end(), 0.0);
	outputSigma.avgpileup = outputSigma.avgpileup/non_zero_fits;

	outputSigma.avgemittanceX = accumulate(outputSigma.bxemittanceX.begin(), outputSigma.bxemittanceX.end(), 0.0); 
	outputSigma.avgemittanceX = outputSigma.avgemittanceX/non_zero_fits;

	outputSigma.avglongitudinal_term = accumulate(outputSigma.bxlongitudinal_term.begin(), outputSigma.bxlongitudinal_term.end(), 0.0); 
	outputSigma.avglongitudinal_term = outputSigma.avglongitudinal_term/non_zero_fits;

	outputSigma.avgemittanceY = accumulate(outputSigma.bxemittanceY.begin(), outputSigma.bxemittanceY.end(), 0.0);
	outputSigma.avgemittanceY = outputSigma.avgemittanceY/non_zero_fits;

	outputSigma.avgemittanceX_err = accumulate(outputSigma.bxemittanceX_err.begin(), outputSigma.bxemittanceX_err.end(), 0.0); 
	outputSigma.avgemittanceX_err = outputSigma.avgemittanceX_err/non_zero_fits;

	outputSigma.avgemittanceY_err = accumulate(outputSigma.bxemittanceY_err.begin(), outputSigma.bxemittanceY_err.end(), 0.0);
	outputSigma.avgemittanceY_err = outputSigma.avgemittanceY_err/non_zero_fits;	
	//std::cout << "*** Number of colliding bunches found in sigmaVis calculation part = "<< non_zero_fits <<'\n';

	ss.clear();ss.str("");
	ss << luminame<<" : "<<outputSigma.toString();
	LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
	m_sigmadata_cache[luminame] = outputSigma;
	
	do_publishResultToEventing(luminame,yShape,xShape,outputSigma);		

	//fill mon infospace for sigma flashlist       
	xdata::String* p_detectorname = dynamic_cast< xdata::String* >( m_IS_vdmsigmamon->find("detectorname") );
	xdata::UnsignedInteger32* p_fillnum = dynamic_cast< xdata::UnsignedInteger32* >( m_IS_vdmsigmamon->find("fillnum") );
	xdata::UnsignedInteger32* p_runnum = dynamic_cast< xdata::UnsignedInteger32* >( m_IS_vdmsigmamon->find("runnum") );
	xdata::TimeVal* p_timestamp = dynamic_cast< xdata::TimeVal* >( m_IS_vdmsigmamon->find("timestamp") );

	xdata::Float* p_avgsigma = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgsigma") );
	xdata::Float* p_avgpileup = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgpileup") );
	xdata::Float* p_avgemittanceX = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgemittanceX") );
	xdata::Float* p_avgemittanceY = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgemittanceY") );
	xdata::Float* p_avgemittanceX_err = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgemittanceX_err") );
	xdata::Float* p_avgemittanceY_err = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgemittanceY_err") );

	xdata::Float* p_avgsigmaerror = dynamic_cast< xdata::Float* >( m_IS_vdmsigmamon->find("avgsigmaerror") );
	xdata::Vector< xdata::Float >* p_bxsigma = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("bxsigma") );
	xdata::Vector< xdata::Float >* p_bxsigmaerror = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("bxsigmaerror") );
	xdata::Vector< xdata::Float >* p_pileup = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("pileup") );
	xdata::Vector< xdata::Float >* p_bxemittanceX = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("bxemittanceX") );
	xdata::Vector< xdata::Float >* p_bxemittanceY = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("bxemittanceY") );
	xdata::Vector< xdata::Float >* p_bxemittanceX_err = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("bxemittanceX_err") );
	xdata::Vector< xdata::Float >* p_bxemittanceY_err = dynamic_cast< xdata::Vector< xdata::Float >* >( m_IS_vdmsigmamon->find("bxemittanceY_err") );

	p_detectorname->value_ = luminame;
	p_fillnum->value_ = outputSigma.fillnum;
	p_runnum->value_ = outputSigma.runnum;
	p_timestamp->value_.sec( outputSigma.timestampsec );
	p_avgsigma->value_ = outputSigma.avgsigma;
	p_avgsigmaerror->value_ = outputSigma.avgsigmaerror;
	p_avgpileup->value_ = outputSigma.avgpileup;
	p_avgemittanceX->value_ = outputSigma.avgemittanceX;
	p_avgemittanceY->value_ = outputSigma.avgemittanceY;
	p_avgemittanceX_err->value_ = outputSigma.avgemittanceX_err;
	p_avgemittanceY_err->value_ = outputSigma.avgemittanceY_err;
	p_bxsigma->clear();
	p_bxsigmaerror->clear();
	p_pileup->clear();
	p_bxemittanceX->clear();
	p_bxemittanceY->clear();
	p_bxemittanceX_err->clear();
	p_bxemittanceY_err->clear();

	for( std::vector<float>::const_iterator it = outputSigma.bxsigma.begin(); it!=outputSigma.bxsigma.end(); ++it){
	  p_bxsigma->push_back( xdata::Float(*it) );
	}
	for( std::vector<float>::const_iterator it = outputSigma.bxsigmaerror.begin(); it!=outputSigma.bxsigmaerror.end(); ++it){
	  p_bxsigmaerror->push_back( xdata::Float(*it) );
	}
	for( std::vector<float>::const_iterator it = outputSigma.pileup.begin(); it!=outputSigma.pileup.end(); ++it){
	  p_pileup->push_back( xdata::Float(*it) );
	}
	for( std::vector<float>::const_iterator it = outputSigma.bxemittanceX.begin(); it!=outputSigma.bxemittanceX.end(); ++it){
	  p_bxemittanceX->push_back( xdata::Float(*it) );
	}
	for( std::vector<float>::const_iterator it = outputSigma.bxemittanceY.begin(); it!=outputSigma.bxemittanceY.end(); ++it){
	  p_bxemittanceY->push_back( xdata::Float(*it) );
	}	
	for( std::vector<float>::const_iterator it = outputSigma.bxemittanceX_err.begin(); it!=outputSigma.bxemittanceX_err.end(); ++it){
	  p_bxemittanceX_err->push_back( xdata::Float(*it) );
	}
	for( std::vector<float>::const_iterator it = outputSigma.bxemittanceY_err.begin(); it!=outputSigma.bxemittanceY_err.end(); ++it){
	  p_bxemittanceY_err->push_back( xdata::Float(*it) );
	}	

	try{
	  ss.clear(); ss.str("");
	  ss<<"Firing VDMSigma flashlist for "<<luminame<<" avgsigma "<<outputSigma.avgsigma;
	  LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
	  m_IS_vdmsigmamon->fireItemGroupChanged(m_vdmsigma_flashfields,this);      // fire flashlist vdmsigma
	}catch(xdata::exception::Exception& e){
	  std::string msg("Failed to fire vdmsigma flashlist ");
	  LOG4CPLUS_ERROR(getApplicationLogger(),msg+xcept::stdformat_exception_history(e));
	  XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,msg,e);
	  notifyQualified("error",myerrorobj);
	}
	ss.clear(); ss.str("");
	ss<<"End VDMSigma flashlst for "<<luminame;
	LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());	
	

      }//end loop detector
     
      clearAllCache(); 
      return false;
    }   

    
     void VDMMonitor::timeExpired( toolbox::task::TimerEvent& e ){
       std::string taskname = e.getTimerTask()->name;
       if( taskname=="off_timer" ){
	 toolbox::TimeVal now=toolbox::TimeVal::gettimeofday();
	 
	 std::stringstream ss;
	 ss<<"m_laststepoff_timesec "<<m_laststepoff_timesec<<" m_planeoff "<<m_planeoff<<" m_cmsoff "<<m_cmsoff<<" deltaT "<<(now.sec() - m_laststepoff_timesec );
	 LOG4CPLUS_DEBUG( getApplicationLogger(), ss.str());
	 
	 if( (m_laststepoff_timesec!=0) && (m_cmsoff==false) && (now.sec() - m_laststepoff_timesec )>m_maxIntervalSec.value_ ){
	   bril::vdmmonitor::CMSOffEvent myevent;
	   this->fireEvent( myevent );	   
	 }
	 if( (m_laststepoff_timesec!=0) && (m_stepon==false) && (m_planeoff==false) && (now.sec()-m_laststepoff_timesec)>m_planeOffThresholdSec.value_ ){
	   bril::vdmmonitor::PlaneOffEvent myevent;
	   LOG4CPLUS_DEBUG(getApplicationLogger(), "fire PlaneOffEvent due to timeout"); 
	   this->fireEvent( myevent );
	 }
       }
     }
    
    void VDMMonitor::do_publishResultToEventing( const std::string& provider, const ShapeData& yShape, const ShapeData& xShape, const SigmaData& sigma){
      std::stringstream ss;
      ss<<"Entering do_publishResultToEventing for "<<provider;
      LOG4CPLUS_INFO( this->getApplicationLogger(), ss.str() );
      if(!m_canpublish) return;
      std::string dictstr = interface::bril::vdmoutputT::payloaddict();
      size_t payloadsize = interface::bril::vdmoutputT::maxsize();
      xdata::Properties plist; 
      plist.setProperty( "DATA_VERSION", interface::bril::shared::DATA_VERSION );
      plist.setProperty( "PAYLOAD_DICT", dictstr ); 
      
      toolbox::mem::Reference* myref = m_poolFactory->getFrame(m_memPool,payloadsize);
      myref->setDataSize(payloadsize);
      interface::bril::vdmoutputT* payload = (interface::bril::vdmoutputT*)(myref->getDataLocation() );

      payload->setTime( sigma.fillnum, sigma.runnum, 0, 0, sigma.timestampsec, 0 );
      payload->setResource( interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND );
      payload->setTotalsize( payloadsize );

      interface::bril::shared::CompoundDataStreamer cds( dictstr ); 
      cds.insert_field( payload->payloadanchor,"provider",  provider.c_str() );
      cds.insert_field( payload->payloadanchor,"mean_sep", &yShape.bxmean[0] );
      cds.insert_field( payload->payloadanchor,"meanerr_sep", &yShape.bxmean_err[0] );
      cds.insert_field( payload->payloadanchor,"b1b2product_sep", &yShape.bxproductb1b2[0] );
      cds.insert_field( payload->payloadanchor,"sigma_sep", &yShape.bxwidth[0] );
      cds.insert_field( payload->payloadanchor,"sigmaerr_sep", &yShape.bxwidth_err[0] );
      cds.insert_field( payload->payloadanchor,"avgsigma_sep", &yShape.avgwidth );
      cds.insert_field( payload->payloadanchor,"avgsigmaerr_sep", &yShape.avgwidth_err );
      cds.insert_field( payload->payloadanchor,"mean_cross", &xShape.bxmean[0] );
      cds.insert_field( payload->payloadanchor,"meanerr_cross", &xShape.bxmean_err[0] );
      cds.insert_field( payload->payloadanchor,"b1b2product_cross", &xShape.bxproductb1b2[0] );
      cds.insert_field( payload->payloadanchor,"sigma_cross", &xShape.bxwidth[0] );
      cds.insert_field( payload->payloadanchor,"sigmaerr_cross", &xShape.bxwidth_err[0] );
      cds.insert_field( payload->payloadanchor,"avgsigma_cross", &xShape.avgwidth );
      cds.insert_field( payload->payloadanchor,"avgsigmaerr_cross", &xShape.avgwidth_err );
      cds.insert_field( payload->payloadanchor,"pileup", &sigma.pileup[0] );
      cds.insert_field( payload->payloadanchor,"avgpileup", &sigma.avgpileup );
      cds.insert_field( payload->payloadanchor,"bxemittanceX", &sigma.bxemittanceX[0] );
      cds.insert_field( payload->payloadanchor,"avgemittanceX", &sigma.avgemittanceX );
      cds.insert_field( payload->payloadanchor,"bxemittanceY", &sigma.bxemittanceY[0] );
      cds.insert_field( payload->payloadanchor,"avgemittanceY", &sigma.avgemittanceY );
      cds.insert_field( payload->payloadanchor,"bxemittanceX_err", &sigma.bxemittanceX_err[0] );
      cds.insert_field( payload->payloadanchor,"avgemittanceX_err", &sigma.avgemittanceX_err );
      cds.insert_field( payload->payloadanchor,"bxemittanceY_err", &sigma.bxemittanceY_err[0] );
      cds.insert_field( payload->payloadanchor,"avgemittanceY_err", &sigma.avgemittanceY_err );
      try{
	getEventingBus(m_bus.value_).publish(  interface::bril::vdmoutputT::topicname() ,  myref , plist); 
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      }catch(xcept::Exception& e){
	myref->release(); 
	LOG4CPLUS_ERROR( getApplicationLogger(),"Failed to publish "+ interface::bril::vdmoutputT::topicname() );
	LOG4CPLUS_ERROR( getApplicationLogger(),stdformat_exception_history(e) ); 
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,"Failed to publish "+interface::bril::vdmoutputT::topicname(),e);
	notifyQualified("error",myerrorobj);
      }	         
    }

    void VDMMonitor::do_publishToEventing( const std::string& provider, const std::string& planename, int stepid, int fillnum, int runnum, const StepData& thisstep ){
      std::stringstream ss;
      ss<<"Entering do_publishToEventing for "<<provider<<" plane "<<planename<<" step "<<stepid<<" fill "<<fillnum<<" run "<<runnum;
      LOG4CPLUS_DEBUG( this->getApplicationLogger(), ss.str() );
      if(!m_canpublish) return;
      std::string dictstr = interface::bril::vdmstepT::payloaddict();
      size_t payloadsize = interface::bril::vdmstepT::maxsize();
      xdata::Properties plist;    
      plist.setProperty( "DATA_VERSION", interface::bril::shared::DATA_VERSION );
      plist.setProperty( "PAYLOAD_DICT", dictstr );       

      toolbox::mem::Reference* myref = m_poolFactory->getFrame(m_memPool,payloadsize);
      myref->setDataSize(payloadsize);

      interface::bril::vdmoutputT* payload = (interface::bril::vdmoutputT*)(myref->getDataLocation() );
      
      payload->setTime( fillnum, runnum, 0, 0, thisstep.timestampsec, 0 );
      payload->setResource( interface::bril::shared::DataSource::DIP,0,0,interface::bril::shared::StorageType::COMPOUND );
      payload->setTotalsize( payloadsize );
      
      interface::bril::shared::CompoundDataStreamer cds( dictstr ); 
      cds.insert_field( payload->payloadanchor,"stepid",  &stepid );
      cds.insert_field( payload->payloadanchor,"plane", planename.c_str() );
      cds.insert_field( payload->payloadanchor,"separation", &thisstep.sep );
      cds.insert_field( payload->payloadanchor,"provider", provider.c_str() );

      if( thisstep.rate.size()==3564 ){	
	cds.insert_field( payload->payloadanchor,"rate", &thisstep.rate[0] );
      }else{
	ss.clear(); ss.str("");
	ss<<" thisstep.rate size is not 3564!! ";
	LOG4CPLUS_ERROR( this->getApplicationLogger(), ss.str() );
      }

      if( thisstep.rateerror.size()==3564 ){	
	cds.insert_field( payload->payloadanchor,"rateerror", &thisstep.rateerror[0] );
      }else{
	ss.clear(); ss.str("");
	ss<<" thisstep.rateerror size is not 3564!! ";
	LOG4CPLUS_ERROR( this->getApplicationLogger(), ss.str() );
      }
      
      if( thisstep.beam1intensity.size()==3564 ){
	cds.insert_field( payload->payloadanchor,"beam1intensity", &thisstep.beam1intensity[0] );
      }else{
	ss.clear(); ss.str("");
	ss<<" thisstep.beam1intensity size is not 3564!! ";
	LOG4CPLUS_ERROR( this->getApplicationLogger(), ss.str() );
      }

      if( thisstep.beam2intensity.size()==3564 ){
	cds.insert_field( payload->payloadanchor,"beam2intensity", &thisstep.beam2intensity[0] );
      }else{
	ss.clear(); ss.str("");
	ss<<" thisstep.beam2intensity size is not 3564!! ";
	LOG4CPLUS_ERROR( this->getApplicationLogger(), ss.str() );
      }

      try{
	getEventingBus(m_bus.value_).publish( m_outtopic.value_,  myref , plist); 
	LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      }catch(xcept::Exception& e){
	myref->release(); 
	LOG4CPLUS_ERROR( getApplicationLogger(),"Failed to publish "+ m_outtopic.value_ );
	LOG4CPLUS_ERROR( getApplicationLogger(),stdformat_exception_history(e) ); 
	XCEPT_DECLARE_NESTED(bril::vdmmonitor::exception::Exception,myerrorobj,"Failed to publish "+m_outtopic.value_,e);
	notifyQualified("error",myerrorobj);
      }	   
    }
    
    void VDMMonitor::init_shapeFlashlist(){      
      std::string fieldname;
      
      fieldname = "fillnum";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::UnsignedInteger32(0)  );      

      fieldname = "runnum";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::UnsignedInteger32(0)  );      

      fieldname = "timestamp_begin";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::TimeVal(toolbox::TimeVal())  );      

      fieldname = "timestamp_end";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::TimeVal(toolbox::TimeVal())  );      

      fieldname = "plane";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::String("")  );      

      fieldname = "detectorname";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::String("")  );      

      fieldname = "avgwidth";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Float(0)  );      

      fieldname = "avgwidth_err";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Float(0)  );      

      fieldname = "avgpeak";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Float(0)  );      
      
      fieldname = "avgpeak_err";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Float(0)  );    

      fieldname = "avgmean";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Float(0)  );      
      
      fieldname = "avgmean_err";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Float(0)  );     

      fieldname = "bxproductb1b2";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );  

      fieldname = "bxwidth";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );   

      fieldname = "bxwidth_err";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );   

      fieldname = "bxpeak";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Vector<xdata::Float>()  );  

      fieldname = "bxpeak_err";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Vector<xdata::Float>()  ); 

      fieldname = "bxmean";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Vector<xdata::Float>()  );  

      fieldname = "bxmean_err";
      m_vdmshape_flashfields.push_back( fieldname );
      m_vdmshape_flashcontents.push_back( new xdata::Vector<xdata::Float>()  );  
      
    }
   
    void VDMMonitor::init_sigmaFlashlist(){
      std::string fieldname;
      
      fieldname = "fillnum";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::UnsignedInteger32(0)  );      

      fieldname = "runnum";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::UnsignedInteger32(0)  );      

      fieldname = "timestamp";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::TimeVal(toolbox::TimeVal())  );      
      
      fieldname = "detectorname";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::String("")  );      

      fieldname = "avgsigma";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  );           

      fieldname = "avgsigmaerror";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  );      

      fieldname = "avgpileup";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  ); 

      fieldname = "avgemittanceX";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  ); 

      fieldname = "avgemittanceY";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  ); 
      
      fieldname = "avgemittanceX_err";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  ); 

      fieldname = "avgemittanceY_err";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Float(0)  ); 

      fieldname = "bxsigma";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );          

      fieldname = "bxsigmaerror";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back( new xdata::Vector<xdata::Float>()  );  

      fieldname = "pileup";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  ); 

      fieldname = "bxemittanceX";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );      

      fieldname = "bxemittanceY";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );  

      fieldname = "bxemittanceX_err";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  );      

      fieldname = "bxemittanceY_err";
      m_vdmsigma_flashfields.push_back( fieldname );
      m_vdmsigma_flashcontents.push_back(  new xdata::Vector<xdata::Float>()  ); 
    }

  } // end ns vdmmonitor
} // end ns bril 
