#include "bril/vdmmonitor/Events.h"

bril::vdmmonitor::StepOnEvent::StepOnEvent():toolbox::Event("urn:bril-vdmmonitor-event:StepOn",0){}

bril::vdmmonitor::StepOnEvent::~StepOnEvent(){}

bril::vdmmonitor::StepOffEvent::StepOffEvent():toolbox::Event("urn:bril-vdmmonitor-event:StepOff",0){}

bril::vdmmonitor::StepOffEvent::~StepOffEvent(){}

bril::vdmmonitor::PlaneOffEvent::PlaneOffEvent():toolbox::Event("urn:bril-vdmmonitor-event:PlaneOff",0){}

bril::vdmmonitor::PlaneOffEvent::~PlaneOffEvent(){}

bril::vdmmonitor::CMSOffEvent::CMSOffEvent():toolbox::Event("urn:bril-vdmmonitor-event:CMSOff",0){}

bril::vdmmonitor::CMSOffEvent::~CMSOffEvent(){}

bril::vdmmonitor::ScanActiveEvent::ScanActiveEvent(int ip):toolbox::Event("urn:bril-vdmmonitor-event:ScanActive",0),ip(ip){}

bril::vdmmonitor::ScanActiveEvent::~ScanActiveEvent(){}

bril::vdmmonitor::ScanOffEvent::ScanOffEvent(int ip):toolbox::Event("urn:bril-vdmmonitor-event:ScanOff",0),ip(ip){}

bril::vdmmonitor::ScanOffEvent::~ScanOffEvent(){}


