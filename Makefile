# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2018, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Project level Makefile
#
##

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES=\
	bril/extern/soci \
	bril/extern/hdf5 \
	bril/extern/bcm1f_ubcm \
	bril/extern/rhudaq \
	bril/extern/pydip \
	bril/extern/histograminterface \
	interface/bril \
	bril/webutils \
	bril/timesource \
	bril/bhmsource \
	bril/bhmprocessor \
	bril/bcm1fsource \
	bril/bcm1fprocessor \
	bril/bcm1futcasource \
	bril/bcm1futcaprocessor \
	bril/histogramsource/generic \
	bril/histogramsource/dtdemo \
	bril/histogramsource/ot \
	bril/histogramprocessor/generic \
	bril/histogramprocessor/dtdemo \
	bril/histogramprocessor/ot \
	bril/pltsource \
	bril/pltprocessor \
	bril/dipprocessor \
	bril/radmonprocessor \
	bril/pltslinkprocessor \
	bril/hfprocessor \
	bril/supervisor \
	bril/vdmmonitor \
	bril/bptxprocessor \
	bril/hfsource \
	bril/bcmlprocessor \
	bril/remusprocessor \
	bril/hardwareconfig \
	bril/lumistore
endif
export PACKAGES

ifndef BUILD_SUPPORT
BUILD_SUPPORT=build
endif
export BUILD_SUPPORT

ifndef MFDEFS_SUPPORT
MFDEFS_SUPPORT=1
endif
export MFDEFS_SUPPORT

ifndef PROJECT_NAME
PROJECT_NAME=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF]}')
endif
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

ifdef Set
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSet.${Set}
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules

