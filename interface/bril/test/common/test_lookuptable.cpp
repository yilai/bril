#include "bril/BCM1FTopics.hh"
#include <iostream>
/**
 * unit tests of some helpers defined in the interface header files
 */
//using namespace interface::bril;
int main(int argc, char** argv){
  std::cout<<interface::bril::shared::IdByName(interface::bril::shared::DataSource::lookuptable,"DIP")<<std::endl;
  std::cout<<interface::bril::shared::IdByName(interface::bril::BCM1FHistAlgos::lookuptable,"OC")<<std::endl;
  std::cout<<interface::bril::shared::NameById(interface::bril::BCM1FHistAlgos::lookuptable,1)<<std::endl;
  std::cout<<interface::bril::shared::NameById(interface::bril::BCM1FHistAlgos::lookuptable,2)<<std::endl;
  std::cout<<interface::bril::shared::StorageType::UINT32<<std::endl;
}
