#include "interface/bril/shared/CommonDataFormat.h"
#include "bril/BCM1FTopics.hh"
#include "interface/bril/shared/LUMITopics.hh"
#include "bril/TCDSTopics.hh"
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <string>
#include <string.h>
#include <vector>
/**
 * test different topic data types
 */
//using namespace interface::bril;
int main(int argc, char** argv){
  std::cout<<"  === data version: "<<interface::bril::shared::DATA_VERSION<<std::endl;
  std::cout<<"  === testing compound data === "<<std::endl;

  std::cout<<"headsize "<<sizeof(interface::bril::shared::DatumHead)<<std::endl;
  size_t totsize = interface::bril::shared::bestlumiT::maxsize();
  char *buffer=(char*)malloc(totsize); 
  if (buffer==NULL) exit (1);
  ((interface::bril::shared::DatumHead*)buffer)->setTime(2312,123450,1,1,1398344937,0);
  ((interface::bril::shared::DatumHead*)buffer)->setResource(interface::bril::shared::DataSource::LUMI,0,0,interface::bril::shared::StorageType::COMPOUND);
  ((interface::bril::shared::DatumHead*)buffer)->setTotalsize(totsize);
  ((interface::bril::shared::DatumHead*)buffer)->setFrequency(4);
  std::cout<<interface::bril::shared::bestlumiT::topicname()<<" datasize "<<((interface::bril::shared::DatumHead*)buffer)->totalsize()<<" payloadsize "<<((interface::bril::shared::DatumHead*)buffer)->payloadsize()<<std::endl;
  std::cout<<interface::bril::shared::bestlumiT::topicname()<<" maxsize "<<totsize<<" n "<<interface::bril::shared::bestlumiT::n()<<std::endl;
  free(buffer);

  std::cout<<"  === testing simple data === "<<std::endl;
  totsize = interface::bril::bcm1fhistT::maxsize();
  buffer=(char*)malloc(totsize); 
  if (buffer==NULL) exit (1);
  ((interface::bril::shared::DatumHead*)buffer)->setTime(2312,123450,1,1,1398344937,0);
  ((interface::bril::shared::DatumHead*)buffer)->setResource(interface::bril::shared::DataSource::BCM1F,3,0,interface::bril::shared::StorageType::UINT16);
  ((interface::bril::shared::DatumHead*)buffer)->setTotalsize(totsize);
  ((interface::bril::shared::DatumHead*)buffer)->setFrequency(4);
  std::cout<<interface::bril::bcm1fhistT::topicname()<<" datasize "<<((interface::bril::shared::DatumHead*)buffer)->totalsize()<<" payloadsize "<<((interface::bril::shared::DatumHead*)buffer)->payloadsize()<<std::endl;
  std::cout<<interface::bril::bcm1fhistT::topicname()<<" maxsize "<<totsize<<" n "<<interface::bril::bcm1fhistT::n()<<std::endl;
  std::cout<<"algo id "<<((interface::bril::shared::DatumHead*)buffer)->getAlgoID()<<std::endl;
  free(buffer);

  std::cout<<"  === testing signal data === "<<std::endl;
  totsize = interface::bril::NB1T::maxsize();
  buffer=(char*)malloc(totsize); 
  if (buffer==NULL) exit (1);
  ((interface::bril::shared::DatumHead*)buffer)->setTime(2312,123450,1,1,1398344937,0);
  ((interface::bril::shared::DatumHead*)buffer)->setResource(interface::bril::shared::DataSource::TCDS,0,0,0);
  ((interface::bril::shared::DatumHead*)buffer)->setTotalsize(totsize);
  ((interface::bril::shared::DatumHead*)buffer)->setFrequency(4);
  std::cout<<interface::bril::NB1T::topicname()<<" datasize "<<((interface::bril::shared::DatumHead*)buffer)->totalsize()<<" payloadsize "<<((interface::bril::shared::DatumHead*)buffer)->payloadsize()<<std::endl;
  std::cout<<interface::bril::NB1T::topicname()<<" maxsize "<<totsize<<" n "<<interface::bril::NB1T::n()<<std::endl;
  std::cout<<"NB1 maxsize "<<interface::bril::NB1T::maxsize()<<std::endl;
  std::cout<<"NB1 nitems "<<((interface::bril::shared::DatumHead*)buffer)->nitems()<<std::endl;
  std::cout<<"NB1 n "<<interface::bril::NB1T::n()<<std::endl;
  free(buffer);

}
