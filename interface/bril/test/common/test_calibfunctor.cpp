#include "bril/CalibData.h"
#include "bril/CalibFunctors.h"
#include <iostream>
#include <cstdlib>
#include <cassert>
/**
 * test different topic data types
 */
using namespace interface::bril;

int main(int argc, char** argv){
  size_t maxpower = 1;
  float coefs[maxpower+1];
  coefs[0] = 0.4;
  coefs[1] = 100.;
  std::cout<<"coefs[0] "<<coefs[0]<<" coefs[1] "<<coefs[1] <<std::endl;
  CalibData d;
  d.setCoefs(coefs,2);
  d.setValidity(1,1000);
  
  CalibFunctor* v = CalibFunctorFactory::createInstance("poly1d");
  float mylumi = 2.5;
  float mycorrectedlumi = 0;
  v->apply(d, mylumi,0,mycorrectedlumi);
  std::cout<<"coefs[0] "<<coefs[0]<<" coefs[1] "<<coefs[1] <<std::endl;
  std::cout<<"mylumi "<<mylumi<<" corrected "<<mycorrectedlumi<<std::endl;
  assert( mycorrectedlumi == (coefs[0]*mylumi+coefs[1]) );
  float mybxlumi[5];
  float mycorrectedbxlumi[5];
  for(size_t i=0; i<5; ++i){
    mybxlumi[i] = i;
    mycorrectedbxlumi[i] = 0;
  }
  v->apply_n(d,mybxlumi,0,mycorrectedbxlumi,5);
  for(size_t i=0; i<5; ++i){
    assert( mycorrectedbxlumi[i] == (coefs[0]*mybxlumi[i]+coefs[1]) );
  }
  delete v;
}
