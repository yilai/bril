#ifndef _interface_bril_HFTopics_hh_
#define _interface_bril_HFTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{
  //
  // HFSource output topics
  //
  DEFINE_SIMPLE_TOPIC(hfCMS1,unsigned int,interface::bril::shared::MAX_NBX,NULL,"hf CMS1 occupancy in one board","");
  DEFINE_SIMPLE_TOPIC(hfCMS2,unsigned int,interface::bril::shared::MAX_NBX,NULL,"hf CMS2 occupancy in one board","");
  DEFINE_SIMPLE_TOPIC(hfCMS_ET,unsigned int,interface::bril::shared::MAX_NBX,NULL,"hf CMS_ET occupancy in one board","");
  DEFINE_SIMPLE_TOPIC(hfCMS_VALID,unsigned int,interface::bril::shared::MAX_NBX,NULL,"hf CMS_VALID occupancy in one board","");
  DEFINE_SIMPLE_TOPIC(hfStatus,unsigned short,1,NULL,"status of board--0 is best","");
  
  //
  // HFProcessor output topics
  //
  DEFINE_SIMPLE_TOPIC(hfOcc1Agg,float,interface::bril::shared::MAX_NBX,NULL,"hf aggregate rate histogram","");
    
  DEFINE_SIMPLE_TOPIC(hfbxmask,unsigned short,interface::bril::shared::MAX_NBX,NULL,"hf bunch mask (0 or 1 per BX)","");

  DEFINE_COMPOUND_TOPIC(hflumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","hf inst lumi raw and calibrated","Hz/ub");

  DEFINE_COMPOUND_TOPIC(hfoclumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","hf inst lumi raw and calibrated from oc algo","Hz/ub");

  DEFINE_COMPOUND_TOPIC(hfetlumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","hf inst lumi raw and calibrated from et sum algo","Hz/ub");

  DEFINE_SIMPLE_TOPIC(hfEtSumAgg,float,interface::bril::shared::MAX_NBX,NULL,"hf aggregate EtSum histogram","");

  DEFINE_SIMPLE_TOPIC(hfafterglowfrac,float,interface::bril::shared::MAX_NBX,NULL,"aftergkiw correction fraction for HF","");

  DEFINE_SIMPLE_TOPIC(hfEtPedestal,float,4,NULL,"pedestal for HF ET; 4 values", "");

  }}//ns interface/bril

#endif
