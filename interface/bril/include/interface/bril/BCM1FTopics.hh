#ifndef _interface_bril_BCM1FTopics_hh_
#define _interface_bril_BCM1FTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{ 
  const unsigned int BCM1F_NCRATE = 6;   //number of crate
  const unsigned int BCM1F_NRU = 8;      //number of channel per crate
  const unsigned int BCM1F_BINPERBX = 4; //n bin per bunch
  const unsigned int BCM1F_NCHANNELS    =  48;
  #define BCM1FHistAlgos_ENUM(XX) XX(OC,=1) XX(COMB,) XX(TT,)
  DEFINE_LOOKUPTABLE(BCM1FHistAlgos,BCM1FHistAlgos_ENUM)

  #define BCM1FAggAlgos_ENUM(XX) XX(SUM,=1) XX(LUMIBIN,) 
  DEFINE_LOOKUPTABLE(BCM1FAggAlgos,BCM1FAggAlgos_ENUM)

  //
  // BCM1FSource topics
  //  
  DEFINE_SIMPLE_TOPIC(bcm1fhist,unsigned short,BCM1F_BINPERBX*interface::bril::shared::MAX_NBX,BCM1FHistAlgos::lookuptable,"bcm1f histograms","");

  //Storage processor topics
  DEFINE_COMPOUND_TOPIC(bcm1fhistls, "lshist:uint32:14256 nbcount:uint16:1","bcm1f histograms agg per ls","");
  DEFINE_COMPOUND_TOPIC(bcm1fhistnb4,"nb4hist:uint32:14256 nbcount:uint16:1","bcm1f histograms agg per nb4","");

  // BCM1FProcessor output topics
  //
  DEFINE_SIMPLE_TOPIC(bcm1fagghist,unsigned short,interface::bril::shared::MAX_NBX,BCM1FAggAlgos::lookuptable,"bcm1f aggregate histogram with bx per bin","");

  DEFINE_COMPOUND_TOPIC(bcm1fbkg,"plusz:float:1 minusz:float:1 nc1:float:1 nc2:float:1 lead1:float:1 lead2:float:1","bcm1f rate/current","");

  DEFINE_COMPOUND_TOPIC(bcm1fbkghistos,"bkgd1hist:float:3564 bkgd2hist:float:3564 bkgd1:float:1 bkgd2:float:1","bcm1f rate/current per bx","");

  DEFINE_COMPOUND_TOPIC(bcm1flumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","bcm1f inst lumi raw and calibrated","Hz/ub");

  DEFINE_COMPOUND_TOPIC(bcm1flumiperchannel, "calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","bcm1f lumi per channel","Hz/ub");

  DEFINE_COMPOUND_TOPIC(bcm1fscvdlumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","bcm1f sCVD inst lumi raw and calibrated","Hz/ub");

  DEFINE_COMPOUND_TOPIC(bcm1fpcvdlumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","bcm1f pCVD inst lumi raw and calibrated","Hz/ub");

  DEFINE_COMPOUND_TOPIC(bcm1fsilumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","bcm1fsi inst lumi raw and calibrated","Hz/ub");

 }}//ns interface/bril

#endif
