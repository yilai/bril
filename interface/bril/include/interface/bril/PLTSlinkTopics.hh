#ifndef _interface_bril_PLTSlinkTopics_hh_
#define _interface_bril_PLTSlinkTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{
 
 //
 // PLTProcessor output topics
 //

DEFINE_COMPOUND_TOPIC(pltslinklumi,"calibtag:str32:1 avgraw:float:1 avg:float:1", "plt inst lumi raw and calibrated from slink data", "Hz/ub");

DEFINE_COMPOUND_TOPIC(pltslinkrate,"acc_rate_total:float:1 acc_rate_perchannel:float:16 eff_total:float:1 eff_perchannel:float:16 pltzero_corr_sbil_perchannel:float:16 pltzero_raw_sbil_perchannel:float:16", "plt slink efficiency, rates", "");

  }}//ns interface/bril

#endif
