#ifndef _interface_bril_BPTXTopics_hh_
#define _interface_bril_BPTXTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{

#define BPTXScopeAlgos_ENUM(XX) XX(Scope1,=1) XX(Scope2,=2)
  DEFINE_LOOKUPTABLE(BPTXScopeAlgos,BPTXScopeAlgos_ENUM)
    
  // Everything is transmitted in ints in order to save memory (smaller size than floats)
  // To get back the physical values, consider this:
  //  * The intensity numbers are in units of 10^7 protions
  //  * The pulse lengths are in ps (multiply by 1000 to get ns)
  //  * The aplitudes are in mV

  DEFINE_COMPOUND_TOPIC(ScopeData,"nB1:uint16:1 nB2:uint16:1 nCol:uint16:1 BXID_B1:uint16:3564 BXID_B2:uint16:3564 TOT_INT_B1:float:1 TOT_INT_B2:float:1 INT_B1:uint16:3564 INT_B2:uint16:3564 AVG_LEN_B1:uint16:1 AVG_LEN_B2:uint16:1 LEN_B1:uint16:3564 LEN_B2:uint16:3564 AtoIcorB1:uint16:1 AtoIcorB2:uint16:1 PHASE_B1:int16:3564 PHASE_B2:int16:3564 AVG_PHASE_B1:int16:1 AVG_PHASE_B2:int16:1 DeltaT:float:1","BPTX Scope measurements","a(b)");

  DEFINE_SIMPLE_TOPIC(bptxDeltaT,float,1, BPTXScopeAlgos::lookuptable,"deltaT(B1-B2)","deltaT, ns");
  
}}

#endif
