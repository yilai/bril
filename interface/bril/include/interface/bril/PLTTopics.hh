#ifndef _interface_bril_PLTTopics_hh_
#define _interface_bril_PLTTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{
  const unsigned int PLT_NRU = 16;      //number of channel per crate
  
  //
  // PLTSource output topics
  //
  DEFINE_SIMPLE_TOPIC(plthist,unsigned int,interface::bril::shared::MAX_NBX,NULL,"plt rate histogram","");
  
  //
  // PLTProcessor output topics
  //
  DEFINE_SIMPLE_TOPIC(pltagghist,unsigned int,interface::bril::shared::MAX_NBX,NULL,"plt aggregate rate histogram","");

  DEFINE_SIMPLE_TOPIC(pltaggzero,unsigned int,interface::bril::shared::MAX_NBX,NULL,"plt zero counting histogram","");

  DEFINE_COMPOUND_TOPIC(pltlumi,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1","plt inst lumi raw and calibrated from track counting","Hz/ub");
  DEFINE_COMPOUND_TOPIC(pltlumizero,"calibtag:str32:1 avgraw:float:1 avg:float:1 bxraw:float:3564 bx:float:3564 maskhigh:uint32:1 masklow:uint32:1", "plt inst lumi raw and calibrated from zero counting", "Hz/ub");
  DEFINE_COMPOUND_TOPIC(pltbkgA, "beam1bkg:float:1 beam2bkg:float:1","plt bkg measurement from non-colliding bunches", "Hz/cm^2");
  DEFINE_COMPOUND_TOPIC(pltbkgB, "beam1bkg:float:1 beam2bkg:float:1","plt bkg measurement preceding colliding bunches", "Hz/cm^2");
  }}//ns interface/bril

#endif
