#ifndef _interface_bril_BCMTopics_hh_
#define _interface_bril_BCMTopics_hh_
#include "interface/bril/shared/CommonDataFormat.h"
namespace interface{ namespace bril{

  //
  // DipAnalyzer topics
  //  
  DEFINE_COMPOUND_TOPIC(bcm,"acq:uint32:576 acqts:uint32:3 lostframes:int32:12 ndumps:int32:6 blecsdump:bool:1 inabort:bool:48 pa1max:float:1","bcm acquisition and channel status","");  

  DEFINE_COMPOUND_TOPIC(bcmabortrate,"plusz:float:2 minusz:float:2","bcml maximum percentage of abort on plus minus z from RS1, RS12","percent");

  DEFINE_COMPOUND_TOPIC(blmrs,"rs91310ms:double:62 rs140us:double:62 rs8655ms:double:62","blmrs values","");   
  
  }}//ns interface/bril

#endif
